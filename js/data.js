
var formapago =[{
                    'codformapago': '1',
                    'nameformapago': 'TDC'
                },{
                    'codformapago': '2',
                    'nameformapago': 'Transferencia'
                },{
                    'codformapago': '3',
                    'nameformapago': 'Depósito por Taquilla'
                }]

var bancoo=[{
                    "bancoorigen": "102",
                    "ibp": "Banco de Venezuela S.A.C.A. Banco Universal"
                }, {
                    "bancoorigen": "114",
                    "ibp": "Bancaribe C.A. Banco Universal"
                },{
                    "bancoorigen": "115",
                    "ibp": "Banco Exterior C.A. Banco Universal"
                },{
                    "bancoorigen": "116",
                    "ibp": "Banco Occidental de Descuento, Banco Universal C.A"
                },{
                    "bancoorigen": "128",
                    "ibp": "Banco Caroní C.A. Banco Universal"
                },{
                    "bancoorigen": "138",
                    "ibp": "Banco Plaza, Banco Universal"
                },{
                    "bancoorigen": "151",
                    "ibp": "BFC Banco Fondo Común C.A. Banco Universal"
                },{
                    "bancoorigen": "156",
                    "ibp": "100% Banco, Banco Universal C.A."
                },{
                    "bancoorigen": "157",
                    "ibp": "Del Sur Banco Universal C.A."
                },{
                    "bancoorigen": "163",
                    "ibp": "Banco del Tesoro, C.A. Banco Universal"
                },{
                    "bancoorigen": "166",
                    "ibp": "Banco Agrícola de Venezuela, C.A. Banco Universal"
                },{
                    "bancoorigen": "168",
                    "ibp": "Bancrecer, S.A. Banco Micro financiero"
                },{
                    "bancoorigen": "169",
                    "ibp": "Mi Banco, Banco Micro financiero C.A."
                },{
                    "bancoorigen": "171",
                    "ibp": "Banco Activo, Banco Universal"
                },{
                    "bancoorigen": "172",
                    "ibp": "Bancamica, Banco Micro financiero C.A."
                },{
                    "bancoorigen": "174",
                    "ibp": "Banplus Banco Universal, C.A"
                },{
                    "bancoorigen": "175",
                    "ibp": "Banco Bicentenario del Pueblo de la Clase Obrera, Mujer y Comunas B.U."
                },{
                    "bancoorigen": "176",
                    "ibp": "Novo Banco, S.A. Sucursal Venezuela Banco Universal"
                },{
                    "bancoorigen": "190",
                    "ibp": "Citibank N.A."
                }]

var tipotarjeta =[ {
                        "codtarjeta": "1",
                        "ntarjeta": "Visa"
                    },{
                        "codtarjeta": "2",
                        "ntarjeta": "MasterCard"
                    },{
                        "codtarjeta": "3",
                        "ntarjeta": "Amex"
                    },{
                        "codtarjeta": "4",
                        "ntarjeta": "Dinner"
                  }]                

var marcapos =[{
                imarcapos: "1", 
                marcapos: "INGENICO"
            },
            {
                imarcapos: "2",
                marcapos: "INTELIPOS"
            },
            {
                imarcapos: "3", 
                marcapos: "VERIFONE"
            },
            {
                imarcapos: "4", 
                marcapos: "CASTLES"
            }]
