function redirectResultB(){
    alertify.error("No existen resultados para la búsqueda");
    window.setTimeout( 'redirectUrlB()', 3000 ); // 4 seconds
};

function redirectUrlB(){
    //open url that your need
     window.location.href ='?cargar';
};

function actualizarxLoteLlaves(source) {


  checkboxes = document.getElementsByName('checkserial');
  var idlote= document.getElementById('idlote').value;
  var usuariocarga= document.getElementById('usuario').value;
  var statusllaves= document.getElementById('statusllaves').value;

  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;

  }

            Swal.fire({
            title: 'Desea actualizar el lote completo?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarLotes.php",
                        type : "POST",
                        data : {"idlote":idlote,"usuariocarga":usuariocarga,"statusllaves":statusllaves},
                        success: function(data){
                            //console.log(data);
                            if(data==1){
                                 //transaccion exitosa
                        
                                    _Title = "¡Enhorabuena!";
                                    _Text = "El lote ha sido actualizado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1500,
                                        type : _Type,

                                        onBeforeOpen: function (){
                                            swal.showLoading()
                                        }
                                    }).then((result)=>{
                                        
                                        location.reload();  

                                    });
                               
                            }else
                            swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                                        _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{
                                        
                                        location.reload();  

                                    });
                }
            });
}



function actualizarxLoteApp(source) {


  checkboxes = document.getElementsByName('checkserial');
  var idlote= document.getElementById('idlote').value;
  var usuariocarga= document.getElementById('usuario').value;
  var statusapp= document.getElementById('statusapp').value;

  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;

  }
  
            Swal.fire({
            title: 'Desea actualizar el lote por completo?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarLotesApp.php",
                        type : "POST",
                        data : {"idlote":idlote,"usuariocarga":usuariocarga,"statusapp":statusapp},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El lote ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 3000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    location.reload();  

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                                        _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                        location.reload();  
                                    });
                }
            });

}

function actualizarxLoteTEM(source) {


  checkboxes = document.getElementsByName('checkserial');
  var idlote= document.getElementById('idlote').value;
  var usuariocarga= document.getElementById('usuario').value;
  var statustem= document.getElementById('statustem').value;

  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;

  }
  
            Swal.fire({
            title: 'Desea actualizar el lote por completo?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarLotesTEM.php",
                        type : "POST",
                        data : {"idlote":idlote,"usuariocarga":usuariocarga,"statustem":statustem},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El lote ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 3000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    location.reload();  

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                                        _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                        location.reload();  
                                    });
                }
            });

}

function busquedaporlotes(){
      if ($('#panel_lote').css('display') == 'none') {
          $('#panel_lote').css('display', 'block') ;
      }
      else {
          $('#panel_lote').css('display', 'none')  ;
      }
}

function busquedareportebanesco(){

    var variable=document.getElementById('idestatus').value;
    if(document.getElementById('idestatus').value != ""){

        window.location.href='?cargar=buscarreportebanesco&var='+variable
    }
    else
    {
        alertify.alert("DEBE SELECCIONAR UN ESTATUS!.");
        document.getElementById('idestatus').focus();
        return false;
    }   
}


function busquedalotes(){

    var variable=document.getElementById('idlote').value;
    if(document.getElementById('idlote').value != ""){

        window.location.href='?cargar=buscarreportexLote&var='+variable
    }
    else
    {
        alertify.alert("DEBE SELECCIONAR UN LOTE!.");
        document.getElementById('idlote').focus();
        return false;
    }   
}



    function buscarreporteinventario(){

    $.post( 'view/modulobanesco/inventario/buscarestatus.php').done( function(respuesta)
    {
        $( '#idestatus' ).html( respuesta );
    });

    $.post( 'view/modulobanesco/inventario/buscarlote.php').done( function(respuesta)
    {
        $( '#idlote' ).html( respuesta );
    });

}


function buscarloteinventario(){

    $.post( 'view/modulobanesco/inventario/buscarlote.php').done( function(respuesta)
    {
        $( '#idlote' ).html( respuesta );
    });

}

function buscarlotebanesco(){

    var variable=document.getElementById('idlote').value;
    if(document.getElementById('idlote').value != ""){

        window.location.href='?cargar=cargaappllaves&var='+variable
    }
    else
    {
        alertify.alert("DEBE SELECCIONAR UN NUMERO DE LOTE!.");
        document.getElementById('idlote').focus();
        return false;
    }   
}


function busquedareportebanescogeneral(){

    var variable=document.getElementById('clientes').value;

    if(document.getElementById('clientes').value != ""){

        window.location.href='?cargar=buscarreportegeneral&var='+variable
    }
    else
    {
        alertify.alert("DEBE SELECCIONAR UN ESTATUS!.");
        document.getElementById('clientes').focus();
        return false;
    }   
}



var AppReporteinventariobanesco = angular.module('AppReporteinventariobanesco', ['dx']);
AppReporteinventariobanesco.service('informacion', ['$http',
  function($http) {
    var idstatus=document.getElementById('idstatus').value;
    var vm=this;
    vm.fdatos = {idstatus: document.getElementById('idstatus').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    AppReporteinventariobanesco.controller('ControllerReporteinventariobanesco',['$scope','informacion',  function ControllerReporteinventariobanesco($scope,informacion) {
  informacion.postData('api/selectReporteInventarioBanescoAngular')
    .then(function(datos) {
        $scope.datos = datos.data

      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus Gestiones",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "id",
            caption: "N°",
            width: "auto"
        }, 
         {
            dataField: "iserialpos",
            caption: "Serial POS",
            width: "auto"
        },
         {
            dataField: "numerodelote",
            caption: "Número de Lote",
            width: "auto"
        },
{ 
            dataField: "fechagest",
            caption: "Fecha Carga",
            width: "auto"
        },

         {
            dataField: "usuariogest",
            caption: "Usuario Gestión",
            width: "auto"
        },
         {
            dataField: "estatusfinal",
            caption: "Estatus de Equipo",
            width: "auto"
        }       
        
      ],
       summary: {
            totalItems: [
            {
                column: "id",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };

      //var someUrl="?cargar=reportedetalladogestiones&var=";  
      //var url = "api/selectGestionMantenimiento.php";
      
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);




var AppReportebanescoxLote = angular.module('AppReportebanescoxLote', ['dx']);
    AppReportebanescoxLote.service('informacion', ['$http',
     function($http) {
        var vm=this;
        vm.fdatos = {idlote: document.getElementById('idlote').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);

    AppReportebanescoxLote.controller('ControllerReportebanescoxLote',['$scope','informacion',  function ControllerReportebanescoxLote($scope,informacion) {
  informacion.postData('api/selectReporteBanescoxLote')
    .then(function(datos) {

      $scope.datos = datos.data 
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus General",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "id",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
            dataField: "fechacarg",
            caption: "Fecha Carga al Sistema",
            width: "auto"
        },
        {
            dataField: "marcapo",
            caption: "Marca del Equipo",
            width: "auto"
        },

        {
            dataField: "seriales",
            caption: "Serial del Equipo",
            width: "auto"
        },

         {
            dataField: "status",
            caption: "Estatus",
            width: "auto"
        },
        {
            dataField: "usuariogest",
            caption: "Usuario Gestión",
            width: "auto"
        },
        {
            dataField: "fechagest",
            caption: "Fecha Gestión",
            width: "auto"
        }      
        
      ],
       summary: {
            totalItems: [
            {
                column: "id",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };

    },
    function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);




var AppreporteestatusgeneralBanesco = angular.module('AppreporteestatusgeneralBanesco', ['dx']);
AppreporteestatusgeneralBanesco.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {clientes: document.getElementById('clientes').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    AppreporteestatusgeneralBanesco.controller('ControllerreporteestatusgeneralBanesco',['$scope','informacion',  function ControllerreporteestatusgeneralBanesco($scope,informacion) {
  informacion.postData('api/selectReporteBanescoGeneral')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus General",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "nbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "rlegal",
            caption: "Representante Legal",
            width: "auto"
         },
        // {
        //     dataField: "clientevip",
        //     caption: "VIP",
        //     width: "auto"
        // },
         {
            dataField: "actividadeconomica",
            caption: "Actividad Económica",
            width: "auto"
        },
        {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "terminal",
            caption: "Nº Terminal",
            width: "auto"
        },
        {
            dataField: "ticket",
            caption: "N° Ticket",
            width: "auto"
        },
        {
            dataField: "statt",
            caption: "Estatus Laboratorio",
            width: "auto"
        },

        {
            dataField: "estatuscontac",
            caption: "Estatus Contacto",
            width: "auto"
        },
        {
            dataField: "estatuscliente",
            caption: "Estatus Cliente",
            width: "auto"
        },

        {
            dataField: "descequipo",
            caption: "Estatus del Equipo",
            width: "auto"
        },
        // {
        //     dataField: "desc_resultvisita",
        //     caption: "Estatus de la Visita",
        //     width: "auto"
        // },
        {
            dataField: "fechacarga",
            caption: "Fecha Carga al Sistema",
            width: "auto"
        }, 
        {
            dataField: "fecharecep",
            caption: "Fecha Recepcion",
            width: "auto"
        },
        {
            dataField: "fechaestion",
            caption: "Fecha Gestión",
            width: "auto"
        },
        {
            dataField: "idatepagos",
            caption: "Fecha Carga de Pagos",
            width: "auto"
        },          
        {
            dataField: "idatefactura",
            caption: "Fecha Carga Factura",
            width: "auto"
        },
        {
            dataField: "fechainstalacion",
            caption: "Fecha Instalacion",
            width: "auto"
        },
        {
            dataField: "fechapercance_eq",
            caption: "Fecha Percance",
            width: "auto"
        },
        {
            dataField: "fechadeclinacion",
            caption: "Fecha Declinacion",
            width: "auto"
        },
        // {
        //     dataField: "fechavisita",
        //     caption: "Fecha Visita",
        //     width: "auto"
        // },
         
        // {
        //     dataField: "oservicio",
        //     caption: "Orden de Servicio",
        //     width: "auto"
        // },
        // {
        //     dataField: "ifechaorden",
        //     caption: "Fecha Orden de Servicio",
        //     width: "auto"
        // },
        // {
        //   dataField: "migraestatus",
        //   caption:  "Estatus Cliente",
        //   width: "auto", 
        //   cellTemplate: function (container, options) {
        //     var x = options.value;
        //       if(x =='0' ||  x==null){
        //         $('<td>'+ 'Cliente Inteligensa' +'</td>')
        //         .css('color','#000000')
        //         .css('background','#ffffff')
        //         .css('text-align','center')
        //         .css('border-radius','0.2em')
        //         .css('padding','.3em')
        //         .appendTo(container); 
        //       } else if(x=='1'){
        //         $('<td>'+ 'Migración' +'</td>')
        //         .css('color','#000000')
        //         .css('background','#ffffff')
        //         .css('text-align','center')
        //         .css('border-radius','0.2em')
        //         .css('padding','.3em')
        //         .appendTo(container);  
        //       }else if(x=='2'){
        //         $('<td>'+ 'Comodato' +'</td>')
        //         .css('color','#000000')
        //         .css('background','#ffffff')
        //         .css('text-align','center')
        //         .css('border-radius','0.2em')
        //         .css('padding','.3em')
        //         .appendTo(container);  
        //       }
        //     } 
        //   },
        {
            dataField: "descperfil",
            caption: "Perfil del Aplicativo",
            width: "auto"
        },

        {
            dataField: "descripcionversion",
            caption: "Versión del Aplicativo",
            width: "auto"
        },

         {
            dataField: "ejec",
            caption: "Ejecutivo",
            width: "auto"
        },   
        {
            dataField: "region_ejec",
            caption: "Región Asignada",
            width: "auto"
        },     
       
         {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        },

        //  {
        //     dataField: "correo_corporativo",
        //     caption: "Correo Electronico Corporativo",
        //     width: "auto"
        // },
         {
            dataField: "correo_personal",
            caption: "Correo Electronico Personal",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
        //  {
        //     dataField: "tlf3",
        //     caption: "Teléfono 3",
        //     width: "auto"
        // },
        //  {
        //     dataField: "tlf4",
        //     caption: "Teléfono 4",
        //     width: "auto"
        // },
        {
            dataField: "cantidadpos",
            caption: "Cantidad Terminales",
            width: "auto"
        },
        {
            dataField: "tipoposs",
            caption: "Tipo de POS",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Modelo de POS",
            width: "auto"
        },
         {
            dataField: "factura",
            caption: "Nº Factura",
            width: "auto"
        },
         {
            dataField: "serialp",
            caption: "Serial POS",
            width: "auto"
        },
         {
            dataField: "serialsim",
            caption: "Serial SimCard",
            width: "auto"
        },
        {
            dataField: "serialmifi",
            caption: "Serial Mifi",
            width: "auto"
        },
         
        {
            dataField: "estado",
            caption: "Estado",
            width: "auto"
        }, 
        {
            dataField: "municipio",
            caption: "Municipio",
            width: "auto"
        }, 
         {
            dataField: "direccions",
            caption: "Direccion Fiscal.",
            width: "auto"
        }
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



function buscarreportebanesco(){

    var el_valor =document.getElementById('valor').value;
    var valor =13;
        
    $.post( 'view/modulobanesco/inventario/buscarbanco.php', { valor: el_valor} ).done( function(respuesta)
    {
        $( '#clientes' ).html( respuesta );
    });

 }
