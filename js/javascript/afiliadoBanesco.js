function generarestatusbanesco(){

	var el_valor =document.getElementById('valor').value;
	var valor =13;
	//var variable= el_valor+'/'+usuario;
	//alert (variable);
 	// Lista de bancos con registros en clientes potenciales comercializacion
 	// Lista de bancos con registros en clientes potenciales comercializacion
	$.post( 'view/ClientePotencial/recepcion/banescoestatus.php', { valor: el_valor} ).done( function(respuesta)
	{
		$( '#estatus' ).html( respuesta );
	});

	}


function buscarregistrobanesco(){

var variable=document.getElementById('estatus').value;
		//alert(reporteeje);
	if(document.getElementById('estatus').value != ""){

		window.location.href='?cargar=busquedaporestatus&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN ESTATUS A CONSULTAR!.");
		document.getElementById('estatus').focus();
		return false;
	}	
}

function validararchivoBanesco(){

	var	archivo=document.getElementById('archivo').value;
	var b = archivo.split('.');

	if(document.getElementById('archivo').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO .CSV A CARGAR.!");
		document.getElementById('archivo').focus();
		return false;
	}

	if(b[b.length-1] != 'csv')
	{
		alertify.alert('Error: El archivo debe ser .csv');
		document.getElementById('archivo').focus();
		return false;
	}

	else{
		document.formarchivo.submit();
	}
}




    var Appreportebanesco = angular.module('Appreportebanesco', ['dx']);
        Appreportebanesco.service('informacion', ['$http',
         function($http) {

            var estatus= document.getElementById('estatus').value;
            var vm=this;
            vm.fdatos = {estatus: document.getElementById('estatus').value};
            this.promiseCache = null;
            this.postData = function(url) {
               return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
            }
          }
    ]);

    Appreportebanesco.controller('Controllerreportebanesco',['$scope','informacion',  function Controllerreportebanesco($scope,informacion) {
  informacion.postData('api/selectReporteBanescoAngular')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      if (estatus.value=='0') { 
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Data Pendiente por Afiliado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "Consecutivo",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "coddocumento",
            caption: "Rif / CI",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "codbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "actividadeconomica",
            caption: "Actividad Económica",
            width: "auto"
        },
        {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        }, 
        {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
        {
            dataField: "",
            caption: "Motivo",
            width: "auto"
        },
        {
            dataField: "",
            caption: "N° Cuenta",
            width: "auto"
        }           
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    }
    if (estatus.value=='1') { 
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Data Afiliado Cargado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "Consecutivo",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "coddocumento",
            caption: "Rif / CI",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "codbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "ncuentabco",
            caption: "N° Cuenta Bancaria",
            width: "auto"
        },
        {
            dataField: "actividadeconomica",
            caption: "Actividad Económica",
            width: "auto"
        },
        {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        }, 
        {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
        
        {
            dataField: "desctipo",
            caption: "Tipo de Comunicación",
            width: "auto"
        },
        {
            dataField: "descmodelo",
            caption: "Modelo del Equipo",
            width: "auto"
        },
        {
            dataField: "serialp",
            caption: "Serial",
            width: "auto"
        },
        {
            dataField: "terminal",
            caption: "N° Terminal",
            width: "auto"
        }          
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    }
    if (estatus.value=='2') { 
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Data Rechazados ",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "Consecutivo",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "coddocumento",
            caption: "Rif / CI",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "codbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "actividadeconomica",
            caption: "Actividad Económica",
            width: "auto"
        },
        {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        }, 
        {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
        {
            dataField: "descrechazo",
            caption: "Motivo",
            width: "auto"
        }          
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    }
    if (estatus.value=='3') { 
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus General",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "Consecutivo",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "coddocumento",
            caption: "Rif / CI",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "codbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "actividadeconomica",
            caption: "Actividad Económica",
            width: "auto"
        },
        {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        }, 
        {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
        {
            dataField: "desc_afiliado",
            caption: "Motivo Desafiliación",
            width: "auto"
        }          
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    }

    },
    function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

