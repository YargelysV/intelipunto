$(document).ready(function(){
	$( "#editarrs" ).click(function() {
    	$('#razonsocialadm').css('display', 'inline-block');
  		$( "#razonsocialadm0" ).hide();
  		$( "#editarrs" ).hide();
  		$( "#guardarrs" ).show();
	});
	$( "#editarcl" ).click(function() {
    	$('#razoncladm').css('display', 'inline-block');
  		$( "#razoncladm0" ).hide();
  		$( "#editarcl" ).hide();
  		$( "#guardarcl" ).show();
	});
	$( "#editarrf" ).click(function() {
    	$('#rifadm').css('display', 'inline-block');
  		$( "#rifadm0" ).hide();
  		$( "#editarrf" ).hide();
  		$( "#guardarrf" ).show();
	});
	$( "#editarbco" ).click(function() {
    	$('select#bancoasignadogest').css('display', 'inline-block');
  		$( "#bco0" ).hide();
  		$( "#editarbco" ).hide();
  		$( "#guardarbco" ).show();
	});

	$( "#editarmodelo" ).click(function() {
		$('select#modelopos').css('display', 'inline-block');
		$( "#imodelo" ).hide();
		$( "#editarmodelo" ).hide();
		$( "#guardarmodelo" ).show();
	});

	$( "#guardarrs" ).click(function() {
		var razonsocial=$('#razonsocialadm').val();
		var idCliente=$('#idregistro').val();
		var userlogin=$('#usuario').val();
		$.ajax({
			url: "view/administracion/facturacionpos/EdiciondDeClientes.php",
			method: "POST",
			data: { razonsocial : razonsocial,
					idCliente : idCliente,
					userlogin: userlogin },
			success: function(e) {
				alertify
			  .success("Razón Social cambiada con éxito.");
			$( "#guardarrs" ).hide();
			$( "#razonsocialadm" ).hide();
  			$( "#editarrs" ).show();
  			$( "#razonsocialadm0" ).show();
  			$( "#razonsocialadm0" ).html('<b>'+razonsocial+'</b>');


			}
		});
	});
	$( "#guardarcl" ).click(function() {
		var razoncl=$('#razoncladm').val();
		var idCliente=$('#idregistro').val();
		var userlogin=$('#usuario').val();
		$.ajax({
			url: "view/administracion/facturacionpos/EdiciondDeClientes.php",
			method: "POST",
			data: { razoncl : razoncl,
					idCliente : idCliente,
					userlogin: userlogin },
			success: function(e) {
				alertify
			  .success("Razón Comercial cambiada con éxito.");
			$( "#guardarcl" ).hide();
			$( "#razoncladm" ).hide();
  			$( "#editarcl" ).show();
  			$( "#razoncladm0" ).show();
  			$( "#razoncladm0" ).html('<b>'+razoncl+'</b>');


			}
		});
	});
	$( "#guardarrf" ).click(function() {
		var rif=$('#rifadm').val();
		if (( /^[VJGEP][0-9]{9}/ ).test( rif )) {
			var idCliente=$('#idregistro').val();
			var userlogin=$('#usuario').val();
			var ibanco=$('#banco').val();
			$.ajax({
				url: "view/administracion/facturacionpos/EdiciondDeClientes.php",
				method: "POST",
				data: { rif : rif,
						idCliente : idCliente,
						userlogin: userlogin,
						ibanco: ibanco },
				success: function(e) {
					alertify
			  .success("RIF cambiado con éxito.");
				$( "#guardarrf" ).hide();
				$( "#rifadm" ).hide();
	  			$( "#editarrf" ).show();
	  			$( "#rifadm0" ).show();
	  			$( "#rifadm0" ).html('<b>'+rif+'</b>');
				}
			});
		}
		else{
			alertify
			  .error("Error, el RIF debe contener una letra y nueve digitos.");
		};	
	});

		$( "#guardarbco" ).click(function() {
		var banco=$('#bancoasignadogest').val();
			var idCliente=$('#idregistro').val();
			var userlogin=$('#usuario').val();
			//var ibanco=$('#banco').val();
			//alert(ibanco);
			$.ajax({
				url: "view/administracion/facturacionpos/EdiciondDeClientes.php",
				method: "POST",
				data: {
						idCliente : idCliente,
						userlogin: userlogin,
						banco: banco },
				success: function(e) {
					alertify.success("El banco ha sido modificado.");
					setTimeout(function(){ window.location.reload(0); }, 1000);
				$( "#guardarbco" ).hide();
				$( "#bancoasignadogest" ).show();
	  			$( "#editarbco" ).show();
				}
			});
	});


	$( "#guardarmodelo" ).click(function() {
	var modelopos=$('#modelopos').val();
		var idCliente=$('#idregistro').val();
		var userlogin=$('#usuario').val();
		//var ibanco=$('#banco').val();
		//alert(ibanco);
		$.ajax({
			url: "view/administracion/facturacionpos/EdiciondDeClientes.php",
			method: "POST",
			data: {
					idCliente : idCliente,
					userlogin: userlogin,
					modelopos: modelopos },
			success: function(e) {
				alertify.success("La Marca del POS ha sido modificado.");
				setTimeout(function(){ window.location.reload(0); }, 1000);
			$( "#guardarmodelo" ).hide();
			$( "#modelopos" ).show();
  			$( "#editarmodelo" ).show();
			}
		});
	});		
});
