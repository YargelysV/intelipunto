
    var f = new Date();
    var anio=f.getFullYear();
    var mes=f.getMonth()+1;
    var dia=f.getDate();
    var hora=f.getHours();
    var minuto=f.getMinutes();
    var segundo=f.getSeconds();
    var fechaactual= anio.toString()+mes.toString()+dia.toString()+hora.toString()+minuto.toString()+segundo.toString();
    

//prueba

var app = angular.module("appServitec",[]);  
app.controller("controllerServitec", function($scope,$http){  
    
    $scope.country = [] ;
    $scope.filterCountry= [] ;
    $http.get('api/selectSerialesPos.php').then(function(response){
    $scope.countryList = response.data;
    });
    $scope.complete = function(string, index){ 
      //alert(string);
       $scope.hidethis = false;  
     
    
       var output = [];  
       angular.forEach($scope.countryList, function(country){  
            if(country.toLowerCase().indexOf(string.toLowerCase()) >= 0)  
            {  
                 output.push(country);  
            }  
       });  
       if($scope.country[index]){
              $scope.filterCountry[index]= output;  
      }else{
    //  alert($scope.country[index]);
     }

    }  
    $scope.fillTextbox = function(string, index){  
       $scope.country = string;  
       $scope.hidethis = true;  
    }  
});     


var AppGestionComercial = angular.module('AppGestionComercial', ['dx']);
AppGestionComercial.service('informacion', ['$http',
  function($http) {
    var procesarinformacion= document.getElementById('procesarinformacion').value;
    var vm=this;
    vm.fdatos = { operacion: document.getElementById('operacion').value, 
                cliente: document.getElementById('cliente').value,
               fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);


AppGestionComercial.controller('GestionComercialController',['$scope','informacion',  function GestionComercialController($scope,informacion) {
  informacion.postData('api/select')
    .then(function(datos) {
      $scope.datos = datos.data
      
    //alert(procesarinformacion.value)
    if (procesarinformacion.value=='1'){ 

      $scope.gridOptions = {
      dataSource:$scope.datos,
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "registro", 
            caption: "Nº Registro",
            width: "auto"
        },
        {
            dataField: "fechaenvio",
            caption:  "Fecha de envío del archivo",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Nombre del Banco Adquiriente",
            width: "auto"
        },
        {
            dataField: "codigobanco", 
            caption:  "Prefijo del Banco",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Punto de Venta",
            width: "auto"
        },
        {
            dataField: "canpos", 
            caption:  "Cantidad de Punto de Venta",
            width: "auto"
        },
        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        },
        {
            dataField: "modelonegocio", 
            caption:  "Tipo módelo de negocio",
            width: "auto"
        },
        {
            dataField: "tipocliente", 
            caption:  "Tipo de Cliente",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social / Nombre del afiliado",
            width: "auto"
        },
        {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
       /* {
            dataField: "formapago", 
            caption: "Forma de pago",
            width: "auto"
        },
        {
            dataField: "transferencia", 
            caption: "Nº Autorización de la transferencia",
            width: "auto"
        },*/
        {
            dataField: "descestatus", 
            caption: "Estatus de Venta",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      
    };
   } 
    if (procesarinformacion.value=='2'){ 

      $scope.gridOptions = {
      dataSource:$scope.datos,
      
        "export": {
            enabled: true,
            fileName: "Clientes_Comercializacion_"+cliente.value+"."+fechaactual,
            allowExportSelectedData: false
      },
      
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "registro", 
            caption: "Nº Registro",
            width: "auto"
        },
        {
            dataField: "fechaenvio",
            caption:  "Fecha de envío del archivo",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Nombre del Banco Adquiriente",
            width: "auto"
        },
        {
            dataField: "codigobanco", 
            caption:  "Prefijo del Banco",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Punto de Venta",
            width: "auto"
        },
        {
            dataField: "canpos", 
            caption:  "Cantidad de Punto de Venta",
            width: "auto"
        },
        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        },
        {
            dataField: "modelonegocio", 
            caption:  "Tipo módelo de negocio",
            width: "auto"
        },
        {
            dataField: "tipocliente", 
            caption:  "Tipo de Cliente",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social / Nombre del afiliado",
            width: "auto"
        },
        {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
       /* {
            dataField: "formapago", 
            caption: "Forma de pago",
            width: "auto"
        },
        {
            dataField: "transferencia", 
            caption: "Nº Autorización de la transferencia",
            width: "auto"
        },*/
        {
            caption: "N° de Afiliación",
            width: "auto"
        },
        {
            dataField: "descestatus", 
            caption: "Estatus de Venta",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      
    };
   } 
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppClientesPotenciales = angular.module('AppClientesPotenciales', ['dx']);
AppClientesPotenciales.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fecha: document.getElementById('fecha').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppClientesPotenciales.controller('ClientesPotencialesController',['$scope','informacion',  function ClientesPotencialesController($scope,informacion) {
  informacion.postData('api/selectClientesPotenciales')
    .then(function(datos) {
      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Clientes",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {

          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalle </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            width: "auto"
        },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }
           
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppClientesPotencialesdetallada = angular.module('AppClientesPotencialesdetallada', ['dx']);
AppClientesPotencialesdetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppClientesPotencialesdetallada.controller('ClientesPotencialesdetalladaController',['$scope','informacion',  function ClientesPotencialesdetalladaController($scope,informacion) {
  informacion.postData('api/selectClientesPotencialesDetallada')
    .then(function(datos) {
      $scope.datos = datos.data
      var cliente= document.getElementById('cliente').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Clientes_"+cliente+'_'+fechadetalle.value+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de POS",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cant. de POS",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
        {
            dataField: "coddocumento", 
            caption:  "R.I.F/ C.I",
            width: "auto"
        },
         {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliación",
            width: "auto"
        },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        } ,
          {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo",
            width: "auto"
        } 
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


//Comercialización Masiva
var AppComercializacionMasiva = angular.module('AppComercializacionMasiva', ['dx']);
AppComercializacionMasiva.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value,
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value};

    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppComercializacionMasiva.controller('ComercializacionMasivaController',['$scope','informacion',  function ComercializacionMasivaController($scope,informacion) {
  informacion.postData('api/selectComercializacionMasiva')
    .then(function(datos) {

      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
       cliente= document.getElementById('cliente').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ComercializaciónMasiva_"+cliente+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
          {

          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalle </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        } ,
        {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }
          
      ],
      summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


//Comercialización Detallada
var AppComercializaciondetallada = angular.module('AppComercializaciondetallada', ['dx']);
AppComercializaciondetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value,
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppComercializaciondetallada.controller('ComercializaciondetalladaController',['$scope','informacion',  function ComercializaciondetalladaController($scope,informacion) {
  informacion.postData('api/selectComercializacionDetallada')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
      var pregistro= document.getElementById('pregistro').value;
      var codtipousuario= document.getElementById('codtipousuario').value;
       if (permisos!='W') {
       var mostrar=true;//cambiado a true para que siempre aparezca el boton editar.
      }
      else{
       var mostrar=true;
      }
       if (pregistro!='W') {
       var permisses=false;
      }
      else{
       var permisses=true;
      }

      var someUrl="?cargar=editar&var=";
      var Urlactive="?cargar=activaregistro&var=";
      $scope.datos = datos.data
      cliente= document.getElementById('cliente').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ComercializacionDetallada_"+cliente+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
        
      
      columns: [
        {
            dataField: "correlativo",
            caption: "N°",
            width: "auto",
            alignment: "center"
         
        },
         {

            dataField: "idestatus_contacto",
            caption:  "Estatus Contacto",
            width: "auto",
            cssClass: "enlace",
            alignment: "center",

              cellTemplate: function (container, options) {
              var x = options.value;
            
                if(x=='1'){
                $('<td>'+ 'Sin Contactar' +'</td>')
                .css('color','white') 
                .css('background','red')
                .css('text-align','center')
                .css('border-radius','0.2em')
                .css('padding','.3em')
                .appendTo(container);

                 } else if(x=='2'){
                 $('<td>'+ 'Contactado' +'</td>')
                 .css('color','#f1f1f1')
                 .css('background','orange')
                 .css('text-align','center')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 

                 }else if(x=='3'){
                 $('<td>'+ 'Venta Con Fondos'+'</td>')
                 .css('color','#f1f1f1')
                 .css('background','purple')
                 .css('text-align','center')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 

                 }else if(x=='4'){
                 $('<td>'+ 'Facturado'+'</td>')
                 .css('color','#f1f1f1')
                 .css('background','#04B4AE')
                 .css('text-align','center')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 

                 }else if(x=='5'){
                 $('<td>'+ 'Preventa'+'</td>')
                 .css('color','#f1f1f1')
                 .css('background','#04B4AE')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 

                 } 

            } 
        },
        {

            dataField: "desc_estatus_contacto",
            caption:  "Desc Estatus Contacto",
            width: "auto",
            visible: false
        },
          {

            dataField: "fechaedita",
            caption:  "Fecha Contacto",
            width: "auto"
          },
         {

            dataField: "activo",
            caption:  "Actividad",
            width: "auto",
            useIcons: true,
            enabled:false,
            cssClass: "enlace",
            alignment: "center",
            calculateCellValue: function(data) {
                return [
                data.enlaceactivo
              
                    ]
                      .join("/");
                      
               },  
            
               cellTemplate: function (container, options) {
               var x = options.value;
               enabled:false,
               l= x.split('/');
               var r="Editando...";
               if(l[1] =="1"){
                
                 if (permisses==false && codtipousuario!='A') {
                $('<td>'+ r +'</td>')
                .css('color','white') 
                .css('background','red')
                .css('border-radius','0.2em')
                .css('padding','.3em')
                .appendTo(container);

                 } else if (permisses= true && codtipousuario!='A') {
                  $('<td>'+ r +'</td>')
                  .css('color','white') 
                  .css('background','red')
                  .css('border-radius','0.2em')
                  .css('padding','.3em')
                  .appendTo(container);
                 } else{
                      $('<a>'+ r +'</a>')
                      .attr('href', Urlactive + options.value)
                      .css('color','white') 
                      .css('background','red')
                      .css('border-radius','0.2em')
                      .css('padding','.3em')
                      .appendTo(container);
                 }
                   
                 } else{
                      var r="Disponible";
                      $('<td>'+ r +'</td>')
                      .css('color','white') 
                      .css('background','#01DF3A')
                      .css('border-radius','0.2em')
                      .css('padding','.3em')
                      .appendTo(container);
                    
                    }  
            
             }    
               
        },
        {
            dataField: "usuarioactivo",
            caption: "usuario edita",
            width: "auto",
            visible :false

        },
      
         {
          
          dataField: 'enlaceafiliado',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
          ReadOnly: true,
          cssClass: "enlace",
          cellTemplate: function (container, options) {
             $('<a> Editar </a>')
               .attr('href', someUrl + options.value)
               .css('color','#f1f1f1')
               .css('background','blue')
               .css('border-radius','0.2em')
               .css('padding','.3em')
               .appendTo(container);
            }  
          
        },
        {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },
         {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
        {
            dataField: "razoncomercial", 
            caption:  "Razón Comercial",
            width: "auto"
        },
        {
            dataField: "personacontacto", 
            caption:  "Persona Contacto",
            width: "auto"
        },
        {
            dataField: "tlf1", 
            caption:  "Télefono",
            width: "auto"
        },
        {
            dataField: "telf2", 
            caption:  "Télefono 2",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
         {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliación",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
            
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },       
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },  
        {
            dataField: "usuarioedita",
            caption: "Eje. edita",
            width: "auto",
            visible :true

        },
           {
            dataField: "bcoasignado",
            caption: "Banco Asignado",
            width: "auto",
            visible :true

        },

     
      ],
      summary: {
            totalItems: [{
                column: "N°",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

//Comercialización Busqueda por Rif
var AppComercializacionRif = angular.module('AppComercializacionRif', ['dx']);
AppComercializacionRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { rif: document.getElementById('nrorif').value,
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value, 
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppComercializacionRif.controller('ComercializacionRifController',['$scope','informacion',  function ComercializacionRifController($scope,informacion) {
  informacion.postData('api/selectComercializacionRif')
    .then(function(datos) {

      var permisos= document.getElementById('permisos').value;
      var pregistro= document.getElementById('pregistro').value;
       var codtipousuario= document.getElementById('codtipousuario').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
       if (pregistro!='W') {
       var permisses=false;
      }
      else{
       var permisses=true;
      }

      var someUrl="?cargar=editar&var=";
      var Urlactive="?cargar=activaregistro&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
       filterRow: { visible: false },
              selection: {
          mode: "multiple"
      },
      
      "export": {
          enabled: true,
          fileName: "ComercializacionRif",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [

        {
            dataField: "correlativo",
            caption: "#",
            width: "auto"
        },

         {

            dataField: "idestatus_contacto",
            caption:  "Estatus Contacto",
            width: "auto",
            cssClass: "enlace",
            alignment: "center",
           
             cellTemplate: function (container, options) {
             var x = options.value;
            
               if(x=='1'){
               $('<td>'+ 'Sin Contactar' +'</td>')
               .css('color','white') 
               .css('background','red')
               .css('border-radius','0.2em')
               .css('padding','.3em')

               .appendTo(container);

                } else if(x=='2'){
                $('<td>'+ 'Contactado' +'</td>')
                .css('color','#f1f1f1')
                .css('background','orange')
                .css('border-radius','0.2em')
                .css('padding','.3em')
                .appendTo(container); 

                }else if(x=='3'){
                 $('<td>'+ 'Venta Con Fondos'+'</td>')
                 .css('color','#f1f1f1')
                 .css('background','purple')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 

                 }else if(x=='4'){
                 $('<td>'+ 'Facturado'+'</td>')
                 .css('color','#f1f1f1')
                 .css('background','#04B4AE')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 

                 } else if(x=='5'){
                 $('<td>'+ 'Preventa'+'</td>')
                 .css('color','#f1f1f1')
                 .css('background','#04B4AE')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 

                 } 


           }  

    },

        {

            dataField: "desc_estatus_contacto",
            caption:  "Desc Estatus Contacto",
            width: "auto",
            visible: false
        },

         {

            dataField: "activo",
            caption:  "Actividad",
            width: "auto",
            useIcons: true,
            cssClass: "enlace",
            alignment: "center",
            
            calculateCellValue: function(data) {
                return [
                data.enlaceactivo
              
                    ]
                      .join("/");
                      
               },  
            
               cellTemplate: function (container, options) {
               var x = options.value;
               
               l= x.split('/');
               var r="Editando...";
               if(l[1] =="1"){       
                 if(permisses==false && codtipousuario!='A'){
                 $('<td>'+ r +'</td>')
                .css('color','white') 
                .css('background','red')
                .css('border-radius','0.2em')
                .css('padding','.3em')
                 .appendTo(container);
                 

                  } else if(permisses==true && codtipousuario!='A'){
                  $('<td>'+ r +'</td>')
                  .css('color','white') 
                  .css('background','red')
                  .css('border-radius','0.2em')
                  .css('padding','.3em')
                  .appendTo(container); 
                  } else{
                    $('<a>'+ r +'</a>')
                 .attr('href', Urlactive +  options.value)
                 .css('color','white') 
                 .css('background','red')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container); 
                }
               
                      
                    }
                    else{
                      var r="Disponible";
                      $('<td>'+ r +'</td>')
                      .css('color','white') 
                      .css('background','#01DF3A')
                      .css('border-radius','0.2em')
                      .css('padding','.3em')
                      .appendTo(container);
                    
                    }  
            
             }    
               
        },
        {
            dataField: "usuarioactivo",
            caption: "usuario edita",
            width: "auto",
            visible :false

        },
        
          {

          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
          color:"red",
          cssClass: "enlace",
          alignment: "center",
           cellTemplate: function (container, options) {
             $('<a> Editar </a>')
               .attr('href', someUrl + options.value)
               .css('color','#f1f1f1')
               .css('background','blue')
               .css('border-radius','0.2em')
               .css('padding','.3em')
               .css('top','1em')
               .appendTo(container);
            }  
        },
        {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },
        {
            dataField: "razon",
            caption:  "Razón Social",
            width: "auto"
        }, 
        {
            dataField: "razoncomercial",
            caption:  "Razón Comercial",
            width: "auto"
        },
        {
            dataField: "personacontacto",
            caption:  "Persona Contacto",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption:  "Télefono",
            width: "auto"
        },        
        {
            dataField: "coddocumento",
            caption:  "N° documento",
            width: "auto"
        },
        {
            dataField: "afiliacion",
            caption:  "N° Afiliado",
            width: "auto"
        },    
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "tipopos",
            caption:  "Tipo POS",
            width: "auto"
        },
         {
            dataField: "cantidad", 
            caption:  "Cantidad",
            width: "auto"
            
        },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
        {
            dataField: "usuarioedita",
            caption: "Eje. Edita",
            width: "auto"
           
        },
    
      ],
    summary: {
            totalItems: [{
                column: "#",
                summaryType: "count"
            }]
        },
 
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


//Ejecutivos de ventas
var AppEjecutivoVentas = angular.module('AppEjecutivoVentas', ['dx']);

AppEjecutivoVentas.controller('EjecutivoVentasController', function EjecutivoVentasController($scope,$http) {
    var permisos=document.getElementById('permisos').value;
    
    var url = "api/EjecutivoVentas.php";
    var someUrl="?cargar=datobancoasignado&var=";
    if (permisos!='W') {
     var edit=false;
     var insert=false;
     var mostrar=false;
      }
      else{
        var edit=true;
        var insert=true;
        var mostrar=true;
      }
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "documento",
            loadUrl: url + "/Ejecutivos?a=1",
            insertUrl: url + "/InsertEj?a=2",
            updateUrl: url + "/UpdateEj?a=3",
            deleteUrl: url + "/DeleteEj?a=4"
        }), 
    
    searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },
      columns: [
        {
            dataField: "correlativo",
            caption: "Nro.",
            allowEditing: false, 
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          //visible: mostrar,
          width: "auto",

           cellTemplate: function (container, options) {
             $('<a> Bancos Asignados </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
         {
            dataField: "tipodoc",
            caption:  "Tipo Doc.",
            visible: mostrar,
            width: "100",
             validationRules: [{
            type: "required",
            message: "El Tipo Documento es Requerido"
            }],
            lookup: {
                    dataSource: [{
                        "tipodoc": "V",
                    }, {
                        "tipodoc": "E",
                    }, {
                        "tipodoc": "P",
                    }],
                    displayExpr: "tipodoc",
                    valueExpr: "tipodoc"
                }
        },
        {
            dataField: "documento",
            caption:  "Doc. Identidad",
            visible: mostrar,
            width: "auto",
           validationRules: [{
            type: "required",
            message: "El Documento es Requerido"
            }, {
                type: "pattern",
                pattern: /^\d*$/,
                message: "No puede usar letras."
            }, {
                type: "stringLength",
                min: 6,
                message: "Debe tener al menos 6 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 11 carácteres.",
                    max: 11
           }]
        },
        {
            dataField: "aliasvendedor", 
            caption:  "Alias Vendedor",
            visible: true,
            width: "auto"
        },
        {
            dataField: "nombre", 
            caption:  "Nombres",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El nombre es Requerido"
            }, {
                type: "pattern",
                pattern: /^[^0-9]+$/,
                message: "No puede usar Números en el nombre."
            }, {
                type: "stringLength",
                min: 2,
                message: "El nombre debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 50 carácteres.",
                    max: 50
           }]
        },
        {
            dataField: "apellido", 
            caption:  "Apellidos",
            width: "auto",
                 validationRules: [{
            type: "required",
            message: "El apellido es Requerido"
            }, {
                type: "pattern",
                pattern: /^[^0-9]+$/,
                message: "No puede usar Números en el apellido."
            }, {
                type: "stringLength",
                min: 2,
                message: "El apellido debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 50 carácteres.",
                    max: 50
           }]
        },
        {
            dataField: "vendedor", 
            caption:  "Codigo Vendedor",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El Codigo es Requerido"
            }, {
                type: "pattern",
                pattern: /^\d*$/,
                message: "No puede usar letras."
            }, {
                type: "stringLength",
                min: 1,
                message: "Debe tener al menos 1 caracter"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 3 carácteres.",
                    max: 3
           }]
        },
        
        {
            dataField: "ejecutivo", 
            caption:  "Tipo de Ejecutivo",
            width: 200,
            lookup: {
                    dataSource: [ {
                        "ejecutivo": "1",
                        "codejecutivos": "INTELIGENSA"
                    },{
                        "ejecutivo": "2",
                        "codejecutivos": "BANCO"
                    }],
                    valueExpr: "ejecutivo",
                    displayExpr: "codejecutivos"
                  }
        }         
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "cell",
          // allowAdding: insert,
            allowUpdating: edit
      },

    };
});




var AppEjecutivoBanco = angular.module('AppEjecutivoBanco', ['dx']);

AppEjecutivoBanco.controller('EjecutivoBancoController', function EjecutivoBancoController($scope,$http) {
  var documento=document.getElementById('documento').value;
  var vendedor=document.getElementById('vendedor').value;
  var banco=document.getElementById('banco').value;
  var permisos=document.getElementById('permisos').value;
    
    if (permisos!='W') {
      var agregar=false;
      var editar=false;
      }
      else{ 
      var agregar=true;
      var editar=true;
        }

   $http.get('api/selectipobanco.php?var='+documento,).then(function(response){
   $scope.banco = response.data;

   
    var Url = "api/selectEjecutivo.php";
  
      var applyFilterTypes = [{
            key: "auto",
            name: "Immediately"
        }, {
            key: "onClick",
            name: "On Button Click"
        }],
        dataGrid;
    
    $scope.filterRow = {
        visible: true,
        applyFilter: "auto"
    };
    
    $scope.headerFilter = {
        visible: true
    };
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
           key: "banco",
           loadUrl: Url + "/Ejecutivos?a=1&var=" +documento,
           insertUrl: Url + "/InsertEj?a=2&var=" +documento,
           deleteUrl: Url + "/DeleteEj?a=4&var=" +banco

        }), 
      columns: [
            {
            dataField: "correlativo", 
            caption:  "Consecutivo",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "vendedor", 
            caption:  "Codigo Vendedor",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "banco", 
            caption:  "Codigo Banco",
            allowEditing: false, 
            width: "auto"
        },
               {
            dataField: "banco", 
            caption:  "Nombre del Banco",
            width: "auto",
            lookup: {
                  dataSource: $scope.banco,
                  valueExpr: "banco",
                  displayExpr: "nombrebanco"
                }
              },
                    
      ],
      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
        editing: {
           mode: "cell",
           allowAdding: agregar,
           allowDeleting: editar

        },
    };
  }); 
});

var AppTecnicos = angular.module('AppTecnicos', ['dx']);

AppTecnicos.controller('TecnicosController', function TecnicosController($scope,$http) {
    
    var permisos=document.getElementById('permisos').value;
    var someUrl ="?cargar=habilitartecnico&var=";
    var url = "api/Tecnico.php";
     if (permisos!='W') {
     var edit=false;
     var insert=false;
     var elim=false;
      }
      else{
        var edit=true;
        var insert=true;
        var elim=true;
      }
   

    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "documento",
            loadUrl: url + "/Ejecutivos?a=1",
            insertUrl: url + "/InsertEj?a=2",
            updateUrl: url + "/UpdateEj?a=3"
            //deleteUrl: url + "/DeleteEj?a=4"
        }), 
    
    searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },  /*"export": {
          enabled: true,
          fileName: "Tecnicos",
          allowExportSelectedData: false
      },*/
      columns: [
        {
            dataField: "correlativo",
            caption: "Nro.",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "tipodoc",
            caption:  "Tipo Doc.",
            width: "100",
             validationRules: [{
            type: "required",
            message: "El Tipo Documento es Requerido"
            }],
            lookup: {
                    dataSource: [{
                        "tipodoc": "V",
                    }, {
                        "tipodoc": "E",
                    }, {
                        "tipodoc": "P",
                    }],
                    displayExpr: "tipodoc",
                    valueExpr: "tipodoc"
                }
        },
        {
            dataField: "documento",
            caption:  "Doc. Identidad",
            width: "auto",
           validationRules: [{
            type: "required",
            message: "El Documento es Requerido"
            }, {
                type: "pattern",
                pattern: /^\d*$/,
                message: "No puede usar letras."
            }, {
                type: "stringLength",
                min: 6,
                message: "Debe tener al menos 6 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 11 carácteres.",
                    max: 11
           }]
        },
        {
            dataField: "nombre", 
            caption:  "Nombres",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El nombre es Requerido"
            }, {
                type: "pattern",
                pattern: /^[^0-9]+$/,
                message: "No puede usar Números en el nombre."
            }, {
                type: "stringLength",
                min: 2,
                message: "El nombre debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 50 carácteres.",
                    max: 50
           }]
        },
        {
            dataField: "apellido", 
            caption:  "Apellidos",
            width: "auto",
                 validationRules: [{
            type: "required",
            message: "El apellido es Requerido"
            }, {
                type: "pattern",
                pattern: /^[^0-9]+$/,
                message: "No puede usar Números en el apellido."
            }, {
                type: "stringLength",
                min: 2,
                message: "El apellido debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 50 carácteres.",
                    max: 50
           }]
        },
        {
            dataField: "tecnico", 
            caption:  "Tecnico",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El Codigo es Requerido"
            }, {
                type: "pattern",
                pattern: /^\d*$/,
                message: "No puede usar letras."
            }, {
                type: "stringLength",
                min: 1,
                message: "Debe tener al menos 1 caracter"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 3 carácteres.",
                    max: 3
           }]
        },
        {
            dataField: "trabajo", 
            caption:  "Zona de Trabajo",
            width: "auto",
                validationRules: [{
            type: "required",
            message: "El Alias es Requerido"
            }, {
                type: "pattern",
                pattern: /^[^0-9]+$/,
                message: "No puede usar Números."
            }, {
                type: "stringLength",
                min: 2,
                message: "Debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 50 carácteres.",
                    max: 50
           }]
        },
        {
            dataField: "estatuss", 
            caption:  "Acción",
            width: "auto",
            allowEditing: false,
               calculateCellValue: function(data) {
   
                return [
                data.enlace
                    ]
                    .join("/");
                     },
              cellTemplate: function (container, options) {
              var x = options.value;
               l= x.split('/');
                if(l[1] =="1"){
               
                      $('<a>'+ 'Activo' +'</a>')
                      .attr('href', someUrl + options.value)
                      .appendTo(container);
                 
                 } else {
                     
                      $('<a>'+ 'Inactivo' +'</a>')
                       .attr('href', someUrl + options.value)
                      .appendTo(container);
                    
                    }  
               /* $('<a>'+l[1] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);*/


                 }
           
       }
          
      ],
        summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
       paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
        editing: {
           mode: "cell",
            allowAdding: insert,
            allowUpdating: edit
            //allowDeleting: elim
        },
        };
});



var AppGestionPos = angular.module('AppGestionPos', ['dx']);
AppGestionPos.controller('GestionPosController', function GestionPosController($scope,$http) {
      
       var permisos= document.getElementById('permisos').value;

       if (permisos!='W') {
     var edit=false;
     var insert=false;
  
      }
      else{
        var edit=true;
        var insert=true;
        
      }

      $scope.loaddata = function(){
      $http.get('api/selectmarcapos.php').then(function(response){
      $scope.marcapos = response.data;

      $http.get('api/selecttipopos.php').then(function(response){
      $scope.tipopos = response.data;
     //console.log($scope.formapago);

    var someUrl="?cargar=reportedetallado&var=";  
    var url = "api/selectGestionMantenimiento.php";
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "secuencial",
            loadUrl: url + "/Ejecutivos?a=1",
            insertUrl: url + "/InsertEj?a=2",
            updateUrl: url + "/UpdateEj?a=3"
        }), 


    searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },  "export": {
          enabled: true,
          fileName: "GestionPos",
          allowExportSelectedData: false
      },
      columns: [

         {
            dataField: "correlativoo",
            caption: "Nro.",
             allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "secuencial",
            caption: "Nro.",
            visible: false,
            allowEditing: false, 
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
     
        {
          dataField: "imarcapos", 
          caption:  "Marca de Pos",
          width: 200,
          lookup: {
                  dataSource: $scope.marcapos,
                  valueExpr: "imarcapos",
                  displayExpr: "marcapos"
                },
          validationRules: [{
          type: "required",
          message: "La marca del Pos es Requerida"
          }]
        },
        {
          dataField: "codtipo", 
          caption:  "Tipo de Pos",
          width: 200,
          lookup: {
                  dataSource: $scope.tipopos,
                  valueExpr: "codtipo",
                  displayExpr: "tipopos"
                },
          validationRules: [{
          type: "required",
          message: "El Tipo de POS es Requerido"
          }]
        },
        {
            dataField: "ivalordolar", 
            caption:  "Valor Dolar ($)",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El Valor es Requerido"
            }, {
                type: "pattern",
                pattern: /^[0-9]+([.][0-9]+)?$/,
                message: "No puede usar letras."
            }, {
                type: "stringLength",
                min: 1,
                message: "Debe tener al menos 1 caracter"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 7 carácteres.",
                    max: 7
           }]
        },
        {
            dataField: "ifactorconversion", 
            caption:  "Factor Conversion",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El Factor de conversion es Requerido"
            }, {
                type: "pattern",
                 pattern: /^[0-9]+([.][0-9]+)?$/,
                message: "No puede usar letras."
            }, {
                type: "stringLength",
                min: 1,
                message: "Debe tener al menos 1 caracter"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 10 carácteres.",
                    max: 10
           }]
        },
        {
            dataField: "ivalorbs", 
            caption:  "Valor (Bs)",
            width: "auto",
            type:"currency",
           allowEditing: false
        },
        {
            dataField: "ifecha", 
            caption:  "Fecha de Actualización",
            dataType: "date",
            width: "auto",
            allowEditing: false
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "cell",
            //allowAdding: insert,
            allowUpdating: edit
        
        },
    };
}); //FIN DEL marcapos
    }); //FIN DEL tipodepos
};
}); //FIN CONTROLER


//consulta del pos de todaas las fechas
var AppValorpos = angular.module('AppValorpos', ['dx']);
AppValorpos.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { marcapos: document.getElementById('marcapos').value,
                  tipopos: document.getElementById('tipopos').value,
                   fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppValorpos.controller('ValorposController',['$scope','informacion',  function ValorposController($scope,informacion) {
  informacion.postData('api/selectValorpos.php')
    .then(function(datos) {
      $scope.datos = datos.data
     // var fecha= document.getElementById('fecha').value;
      var someUrl="?cargar=reportedetalladopos&var=";  
    var url = "api/selectGestionMantenimiento.php";
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
    
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "icorrelativo",
            caption: "Consecutivo",
            width: "auto"
        },
           {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
          {
            dataField: "imarcapos", 
            caption:  "Marca de Pos",
            width: "auto"
        },
        {
            dataField: "codtipo", 
            caption:  "Tipo de POS",
            width: "auto"
        },
        
          {
            dataField: "ifecha", 
            caption:  "Fecha de Actualizacion",
            width: "auto"
        },
          {
            dataField: "cantidad", 
            caption:  "Cantidad de Actualizaciones",
            width: "auto"
        },

           
      ],
      summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


//consulta detallada por fechas


var AppValorposdetallado = angular.module('AppValorposdetallado', ['dx']);
AppValorposdetallado.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { marcapos: document.getElementById('marcapos').value,
                  tipopos: document.getElementById('tipopos').value,
                   fecha: document.getElementById('fecha').value,
                    secuencial: document.getElementById('secuencial').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppValorposdetallado.controller('ValorposdetalladoController',['$scope','informacion',  function ValorposdetalladoController($scope,informacion) {
  informacion.postData('api/selectValordetallado.php')
    .then(function(datos) {
      $scope.datos = datos.data
     // var fecha= document.getElementById('fecha').value;
     // var someUrl="?cargar=reportedetalladopos&var=";  
 
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
    
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "icorrelativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "imarcapos", 
            caption:  "Marca de Pos",
            width: "auto"
        },
        {
            dataField: "codtipo", 
            caption:  "Tipo de POS",
            width: "auto"
        },
        {
            dataField: "ivalordolar", 
            caption:  "Valor Dolar ($)",
            width: "auto"
        },
         {
            dataField: "ifactorconversion", 
            caption:  "Factor Conversion",
            width: "auto"
        },
         {
            dataField: "ivalorbs", 
            caption:  "Valor (Bs)",
            width: "auto"
        },
          {
            dataField: "ifecha", 
            dataType: "date",
            caption:  "Fecha de Actualizacion",
            width: "auto"
        }           
      ],
      summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppGestionComercialReporte = angular.module('AppGestionComercialReporte', ['dx']);
AppGestionComercialReporte.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { cliente: document.getElementById('cliente').value,
                  fecha: document.getElementById('fecha').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);


AppGestionComercialReporte.controller('GestionComercialReporteController',['$scope','informacion',  function GestionComercialReporteController($scope,informacion) {
  informacion.postData('api/SelectComercializacionReporte')
    .then(function(datos) {
      var someUrl="?cargar=reportedetallado&var=";
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      

              selection: {
          mode: "multiple"
      },
      "export": {
        enabled: true,
         fileName: "Clientes_Comercializacion_reporte"+cliente.value+"."+fechaactual,
         allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
       {
          dataField: 'enlacereporte',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "fechaenvio",
            caption:  "Fecha de envío del archivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion", 
            caption:  "Fecha recepción del archivo",
            width: "auto"
        },
        {
            dataField: "estatusafiliado", 
            caption:  "Estatus del Archivo",
            width: "auto"
        },
        {
            dataField: "registros", 
            caption:  "Registros (Afiliados)",
            width: "auto"
        },
        {
            dataField: "cantidadpos", 
            caption:  "Puntos de Ventas",
            width: "auto"
        }
          
      ],
      summary: {
            totalItems: [{
                column: "registros",
                summaryType: "sum"
            },
            {
                column: "cantidadpos",
                summaryType: "sum"
            },
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
       columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

var AppGestionComercialReporteDetallado = angular.module('AppGestionComercialReporteDetallado', ['dx']);
AppGestionComercialReporteDetallado.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { cliente: document.getElementById('cliente').value,
                  fechaenvio: document.getElementById('fechaenvio').value,
                  fecharecepcion: document.getElementById('fecharecepcion').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppGestionComercialReporteDetallado.controller('GestionComercialReporteDetalladoController',['$scope','informacion',  function GestionComercialReporteDetalladoController($scope,informacion) {
  informacion.postData('api/SelectComercializacionReporteDetallado')
    .then(function(datos) {
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
        enabled: true,
         fileName: "Clientes_Comercializacion_detallado"+cliente.value+"."+fechaactual,
         allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
       columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
          {
            dataField: "registro", 
            caption: "Nº Registro",
            width: "auto"
        },
        {
            dataField: "fechaenvio",
            caption:  "Fecha de envío del archivo",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Nombre del Banco Adquiriente",
            width: "auto"
        },
        {
            dataField: "cbanco", 
            caption:  "Prefijo del Banco",
            width: "auto"
        },
        {
            dataField: "estatusventa", 
            caption:  "Estatus de Venta",
            width: "auto"
        },
        {
            dataField: "tipopv", 
            caption:  "Tipo de Punto de Venta",
            width: "auto"
        },
        {
            dataField: "cantidadpos", 
            caption:  "Cantidad de Punto de Venta",
            width: "auto"
        },
        {
            dataField: "tipoline", 
            caption:  "Tipo de Línea",
            width: "auto"
        },
        {
            dataField: "tipomodelo", 
            caption:  "Tipo módelo de negocio",
            width: "auto"
        },
        {
            dataField: "tipoclient", 
            caption:  "Tipo de Cliente",
            width: "auto"
        },
        {
            dataField: "razon", 
            caption:  "Razón social / Nombre del afiliado",
            width: "auto"
        },
        {
            dataField: "documento", 
            caption:  "Rif / CI",
            width: "auto"
        },
       /* {
            dataField: "formapago", 
            caption: "Forma de pago",
            width: "auto"
        },
        {
            dataField: "transferencia", 
            caption: "Nº Autorización de la transferencia",
            width: "auto"
        },*/
        {
            dataField: "afiliado", 
            caption: "N° de Afiliación",
            width: "auto"
        }

      ],
      summary: {
            totalItems: [{
                column: "cantidadpos",
                summaryType: "sum"
            },
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
       columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppSerialesPos = angular.module('AppSerialesPos', ['dx']);
AppSerialesPos.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppSerialesPos.controller('SerialesPosController',['$scope','informacion',  function SerialesPosController($scope,informacion) {
  informacion.postData('api/SerialesPos')
    .then(function(datos) {
      $scope.datos = datos.data
      
     $scope.gridOptions = {
      dataSource:$scope.datos,
      groupPanel: {
          visible: true
      },
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Seriales_Pos"+fecha.value,
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
            dataField: "iproveedor",
            caption:  "Proveedor",
            width: "auto"
        },
         {
            dataField: "ifechacompra",
            caption:  "Fecha Compra",
            width: "auto"
        },
         {
            dataField: "imarcapos",
            caption:  "Marca POS",
            width: "auto"
        },
        {
            dataField: "tipopv",
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "serialpos",
            caption:  "Serial de Pos",
            width: "auto"
        },
        {
            dataField: "estatusequipos", 
            caption:  "Estatus del Equipo",
            width: "auto"
        }
       /*, {
            dataField: "nombrebanco", 
            caption:  "Banco Asignado",
            width: "auto"
        }*/
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },

      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppSerialesSimcard = angular.module('AppSerialesSimcard', ['dx']);
AppSerialesSimcard.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppSerialesSimcard.controller('SerialesSimcardController',['$scope','informacion',  function SerialesSimcardController($scope,informacion) {
  informacion.postData('api/Simcard')
    .then(function(datos) {
      $scope.datos = datos.data
      
     $scope.gridOptions = {
      dataSource:$scope.datos,
      groupPanel: {
          visible: true
      },
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Seriales_Simcards"+fecha.value,
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
            dataField: "iproveedor",
            caption:  "Proveedor",
            width: "auto"
        },
         {
            dataField: "ifechacompra",
            caption:  "Fecha Compra",
            width: "auto"
        },
         
        {
            dataField: "tiposim",
            caption:  "Tipo de SimCard",
            width: "auto"
        },
        {
            dataField: "serialsimc",
            caption:  "Serial de SimCard",
            width: "auto"
        },
        {
            dataField: "estatusequipos", 
            caption:  "Estatus de Equipo",
            width: "auto"
        }
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },

      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppSerialesMifi = angular.module('AppSerialesMifi', ['dx']);
AppSerialesMifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppSerialesMifi.controller('SerialesMifiController',['$scope','informacion',  function SerialesMifiController($scope,informacion) {
  informacion.postData('api/Mifi')
    .then(function(datos) {
      $scope.datos = datos.data
      
     $scope.gridOptions = {
      dataSource:$scope.datos,
      groupPanel: {
          visible: true
      },
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Seriales_Mifi"+fecha.value,
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
            dataField: "iproveedor",
            caption:  "Proveedor",
            width: "auto"
        },
         {
            dataField: "ifechacompra",
            caption:  "Fecha Compra",
            width: "auto"
        },
         
        {
            dataField: "tipomifi",
            caption:  "Tipo de MIFI",
            width: "auto"
        },
        {
            dataField: "serialmifii",
            caption:  "Serial de MIFI",
            width: "auto"
        },
        {
            dataField: "estatusequipos",
            caption:  "Estatus del Equipo",
            width: "auto"
        }
      ],
       summary: { 
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },

      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);






var AppClienteFacturacion = angular.module('AppClienteFacturacion', ['dx']);
AppClienteFacturacion.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { cliente: document.getElementById('cliente').value,
                  fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppClienteFacturacion.controller('ClientesFacturacionController',['$scope','informacion',  function ClientesFacturacionController($scope,informacion) {
  informacion.postData('api/ClientesFacturacion')
    .then(function(datos) {
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Clientes_Facturar"+cliente.value,
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "tipocliente",
            caption:  "TipoCliente",
            width: "auto"
        },
        {
            dataField: "razon",
            caption:  "Customer Name",
            width: "auto"
        },
        {
            dataField: "razon", 
            caption:  "Short Name",
            width: "auto"
        },
        {
            dataField: "razon", 
            caption:  "Statement Name",
            width: "auto"
        },
        {
            dataField: "destipo", 
            caption:  "Customer Class",
            width: "auto"
        },
        {
            dataField: "facturacion", 
            caption:  "Address Code",
            width: "auto"
        },
        {
            dataField: "representantelegal", 
            caption:  "Contact Person",
            width: "auto"
        },
        {
            dataField: "address1", 
            caption:  "Address 1",
            width: "auto"
        },
        {
            dataField: "address2", 
            caption:  "Address 2",
            width: "auto"
        },
        {
            dataField: "municipio", 
            caption:  "City",
            width: "auto"
        },
        {
            dataField: "estado", 
            caption:  "State",
            width: "auto"
        },
        {
            dataField: "codepostal", 
            caption:  "Zip",
            width: "auto"
        },
        {
            dataField: "countrycode", 
            caption:  "Country Code",
            width: "auto"
        },
        {
            dataField: "tlf1", 
            caption:  "Phone 1",
            width: "auto"
        },
        {
            dataField: "tlf2", 
            caption:  "Phone 2",
            width: "auto"
        },
         {
            dataField: "rif", 
            caption:  "User Defined 1",
            width: "auto"
        },
         {
            dataField: "rif", 
            caption:  "Tax Registration Number",
            width: "auto"
        }

      ],
       summary: {
            totalItems: [
            {
                column: "tipocliente",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

//Facturacion Masiva

var AppFacturacionMasiva = angular.module('AppFacturacionMasiva', ['dx']);
AppFacturacionMasiva.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value, 
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value };

    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppFacturacionMasiva.controller('FacturacionMasivaController',['$scope','informacion',  function FacturacionMasivaController($scope,informacion) {
  informacion.postData('api/selectGestionFacturacion')
    .then(function(datos) {

      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      cliente= document.getElementById('banco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "FacturaciónMasiva_"+cliente+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {

          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalle </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }
            
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

// facturacion masica de los mifi
var AppFacturacionMasivaMifi = angular.module('AppFacturacionMasivaMifi', ['dx']);
AppFacturacionMasivaMifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value, 
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value };

    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppFacturacionMasivaMifi.controller('FacturacionMasivaMifiController',['$scope','informacion',  function FacturacionMasivaMifiController($scope,informacion) {
  informacion.postData('api/selectFacturacionGestionMifi')
    .then(function(datos) {

      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      cliente= document.getElementById('banco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "FacturaciónMasiva_"+cliente+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {

          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalle </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }
            
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



//Facturación Detallada

var AppFacturaciondetallada = angular.module('AppFacturaciondetallada', ['dx']);
AppFacturaciondetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value,
                  masiva: document.getElementById('masiva').value,
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppFacturaciondetallada.controller('FacturaciondetalladaController',['$scope','informacion',  function  FacturaciondetalladaController($scope,informacion) {
  informacion.postData('api/SelectFacturacionDetallada')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }

      var someUrl="?cargar=facturarcliente&var=";
      $scope.datos = datos.data
       cliente= document.getElementById('banco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "FacturacionDetallada_"+cliente+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
          dataField: "estatuspago",
          caption: "Estatus Pago",
          width: "auto",
          cssClass: "enlace",

   cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Pago Confirmado' +'</td>')
     .css('color','white') 
     .css('background','#01DF3A')
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Pendiente Por Pagos' +'</td>')
      .css('color','#f1f1f1')
      .css('background','orange')
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="0"){

        $('<td>'+ 'Pendiente Por Confirmar' +'</td>')
        .css('color','#f1f1f1')
        .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

    }          
        },
        {

          dataField: 'enlaceafiliado',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Datos Facturación </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "idestatusequipo", 
            caption:  "Estatus del Equipo",
            width: "auto",
            cssClass: "enlace",

            cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Inteliservices' +'</td>')
     /*.css('color','white') 
     .css('background','#01DF3A')*/
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Verificar Serial.' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="3"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="4"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="5"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
      else if(x=="6"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
      else if(x=="8"){

        $('<td>'+ 'Entregado al Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="9"){

        $('<td>'+ 'Declinacion de Compra' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="10"){

        $('<td>'+ 'POS Instalado (Inteligensa.)' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="12"){

        $('<td>'+ 'POS Instalado Maracaibo' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="13"){

        $('<td>'+ 'Configuración Mifi' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="14"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="0"){

        $('<td>'+ 'Sin Parámetros' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
       else if(x==null){
      $('<td>'+ 'Por Asignar Serial' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } 

      else if(x=="16"){

        $('<td>'+ 'Revisar Mifi' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }

      else if(x=="20"){

        $('<td>'+ 'Gestión del Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }

      else if(x=="21"){

        $('<td>'+ 'Gestión Maracaibo' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      

    }  
        },
        {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
         {
            dataField: "nafiliacion",
            caption:  "N° Afiliado",
            width: "auto"
        },
         {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },  
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        }
           
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
     paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



    var AppFacturaciondetalladaMifi = angular.module('AppFacturaciondetalladaMifi', ['dx']);
AppFacturaciondetalladaMifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value,
                  masiva: document.getElementById('masiva').value,
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppFacturaciondetalladaMifi.controller('FacturaciondetalladaMifiController',['$scope','informacion',  function  FacturaciondetalladaMifiController($scope,informacion) {
  informacion.postData('api/SelectFacturacionDetalladaMifi')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }

      var someUrl="?cargar=facturarclientemifi&var=";
      $scope.datos = datos.data
       cliente= document.getElementById('banco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "FacturacionDetallada_"+cliente+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
          dataField: "estatuspago",
          caption: "Estatus Pago",
          width: "auto",
          cssClass: "enlace",

   cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Pago Confirmado' +'</td>')
     .css('color','white') 
     .css('background','#01DF3A')
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Pendiente Por Pagos' +'</td>')
      .css('color','#f1f1f1')
      .css('background','orange')
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="0"){

        $('<td>'+ 'Pendiente Por Confirmar' +'</td>')
        .css('color','#f1f1f1')
        .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

    }          
        },
        {

          dataField: 'enlaceafiliado',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Datos Facturación </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "idestatusequipo", 
            caption:  "Estatus del Equipo",
            width: "auto",
            cssClass: "enlace",

            cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Pendiente por Instalar' +'</td>')
     /*.css('color','white') 
     .css('background','#01DF3A')*/
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Revisión de Equipo' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="3"){

        $('<td>'+ 'Mifi Instalado' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

           else if(x=="4"){

        $('<td>'+ 'Declinación de Compra' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="5"){

        $('<td>'+ 'Gestionado por Inteligensa' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="0"){

        $('<td>'+ 'En proceso de Configuración' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
      
       else if(x==null){
      $('<td>'+ 'Por asignar serial mifi' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } 
    

    }  
        },
        {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
         /*{
            dataField: "nafiliacion",
            caption:  "N° Afiliado",
            width: "auto"
        },*/
         {
            dataField: "tipopos", 
            caption:  "Tipo de Equipo",
            width: "auto"
        },
       {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Equipo",
            width: "auto"
        }, 
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        }
           
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
     paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

//FACTURACION CLIENTE

var AppFacturacionCliente = angular.module('AppFacturacionCliente', ['dx']);

AppFacturacionCliente.controller('FacturacionClienteController', function DemoController($scope,$http) {
    
    var url = "api/FacturacionPos.php";
    var  vari= document.getElementById('var').value;

      var applyFilterTypes = [{
            key: "auto",
            name: "Immediately"
        }, {
            key: "onClick",
            name: "On Button Click"
        }],
        dataGrid;
    
    function getOrderDay(rowData) {
        return (new Date(rowData.OrderDate)).getDay();
    }
    
    $scope.filterRow = {
        visible: true,
        applyFilter: "auto"
    };
    
    $scope.headerFilter = {
        visible: true
    };

    $scope.currencyFormat = {
          format: " #,##0.##  Bs",
          value: 0.00
    };

    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "coddocumento",
            loadUrl: url + "/Ejecutivos?a=1&var="+vari,
            updateUrl: url + "/UpdateEj?a=3&var="+vari
        }), 
       "export": {
          enabled: true,
          fileName: "FacturacionCliente",
          allowExportSelectedData: false
      },
    
      columns: [
            {
            dataField: "coddocumento",
            caption:  "RIF / CI",
            allowEditing: false, 
            width: "auto"
            },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliado",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de POS",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de POS",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "fechafacturacion", 
            dataType: "date",
            caption:  "Fecha de Facturación",
            width: "auto",
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "nfactura", 
            caption:  "Nº de Factura",
            width: "auto"
        },
        {
            dataField: "formapago", 
            caption:  "Forma de Pago",
            width: 200,
            lookup: {
                    dataSource: [ {
                        "codformapago": "1",
                        "nameformapago": "TDC"
                    },{
                        "codformapago": "2",
                        "nameformapago": "Transferencia"
                    },{
                        "codformapago": "4",
                        "nameformapago": "Depósito por Taquilla"
                    }],
                    valueExpr: "codformapago",
                    displayExpr: "nameformapago"
                  }
        },
        {
            dataField: "ntransferencia", 
            caption:  "Nº de Transferencia",
            width: "auto"
        },
        {
            dataField: "bancoorigen", 
            caption:  "Banco Origen",
            width: 300,
            lookup: {
                    dataSource: [{
                        "bancoorigen": "102",
                        "ibp": "Banco de Venezuela S.A.C.A. Banco Universal"
                    }, {
                        "bancoorigen": "114",
                        "ibp": "Bancaribe C.A. Banco Universal"
                    },{
                        "bancoorigen": "115",
                        "ibp": "Banco Exterior C.A. Banco Universal"
                    },{
                        "bancoorigen": "116",
                        "ibp": "Banco Occidental de Descuento, Banco Universal C.A"
                    },{
                        "bancoorigen": "128",
                        "ibp": "Banco Caroní C.A. Banco Universal"
                    },{
                        "bancoorigen": "138",
                        "ibp": "Banco Plaza, Banco Universal"
                    },{
                        "bancoorigen": "151",
                        "ibp": "BFC Banco Fondo Común C.A. Banco Universal"
                    },{
                        "bancoorigen": "156",
                        "ibp": "100% Banco, Banco Universal C.A."
                    },{
                        "bancoorigen": "157",
                        "ibp": "Del Sur Banco Universal C.A."
                    },{
                        "bancoorigen": "163",
                        "ibp": "Banco del Tesoro, C.A. Banco Universal"
                    },{
                        "bancoorigen": "166",
                        "ibp": "Banco Agrícola de Venezuela, C.A. Banco Universal"
                    },{
                        "bancoorigen": "168",
                        "ibp": "Bancrecer, S.A. Banco Micro financiero"
                    },{
                        "bancoorigen": "169",
                        "ibp": "Mi Banco, Banco Micro financiero C.A."
                    },{
                        "bancoorigen": "171",
                        "ibp": "Banco Activo, Banco Universal"
                    },{
                        "bancoorigen": "172",
                        "ibp": "Bancamica, Banco Micro financiero C.A."
                    },{
                        "bancoorigen": "174",
                        "ibp": "Banplus Banco Universal, C.A"
                    },{
                        "bancoorigen": "175",
                        "ibp": "Banco Bicentenario del Pueblo de la Clase Obrera, Mujer y Comunas B.U."
                    },{
                        "bancoorigen": "176",
                        "ibp": "Novo Banco, S.A. Sucursal Venezuela Banco Universal"
                    },{
                        "bancoorigen": "190",
                        "ibp": "Citibank N.A."
                    }],
                    valueExpr: "bancoorigen",
                    displayExpr: "ibp"
                  }
        },
        {
            dataField: "costopos", 
            caption:  "Costo del POS (Bs)",
            width: "auto",
              validationRules: [
              {
                type: "pattern",
                pattern: /^\d*$/,
                message: "No puede usar letras."
            }, {
                type: "stringLength",
                min: 2,
                message: "Debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo maximo es de 50 carácteres.",
                    max: 50
           }],
                      
        },
        {
            dataField: "codtarjeta", 
            caption:  "Tipo de Tarjeta",
            width: "auto",
            lookup: {
                    dataSource: [ {
                        "codtarjeta": "1",
                        "ntarjeta": "Visa"
                    },{
                        "codtarjeta": "2",
                        "ntarjeta": "MasterCard"
                    },{
                        "codtarjeta": "3",
                        "ntarjeta": "Amex"
                    },{
                        "codtarjeta": "4",
                        "ntarjeta": "Dinner"
                    }],
                    valueExpr: "codtarjeta",
                    displayExpr: "ntarjeta"
                  }
        },
        {
            dataField: "fechapago", 
            dataType: "date",
            caption:  "Fecha de Pago",
            width: "auto",
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "horapago", 
            caption:  "Hora de Pago",
            dataType: "longTime",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "cell",
            allowUpdating: true
        },
    };
});




var AppReporteAdm = angular.module('AppReporteAdm', ['dx']);
AppReporteAdm.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteAdm.controller('ReporteAdmController',['$scope','informacion',  function ReporteAdmController($scope,informacion) {
  informacion.postData('api/selectReporteAdm')
    .then(function(datos) {
      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      

      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
              selection: {
          mode: "multiple"
      },
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }    
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);


var AppReporteAdmDetallada = angular.module('AppReporteAdmDetallada', ['dx']);
AppReporteAdmDetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteAdmDetallada.controller('ReporteAdmDetalladaController',['$scope','informacion',  function ReporteAdmDetalladaController($scope,informacion) {
  informacion.postData('api/selectReporteDetallado')
    .then(function(datos) {
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Administracion.Inventario_"+databanco+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepcion",
            width: "auto"
        },
       /* {
            dataField: "ejecutivoint", 
            caption:  "Nombre Ejecutivo Inteligensa",
            width: "auto"
        },*/
       /* {
            dataField: "ejecutivobanc", 
            caption:  "Nombre Ejecutivo Banco",
            width: "auto"
        },*/
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
        {
            dataField: "activofijo", 
            caption:  "N° de Activo Fijo",
            width: "auto"
        },
        {
            dataField: "fechacompra", 
            caption:  "Fecha de Compra POS",
            width: "auto"
        },
        {
            dataField: "marcapos", 
            caption:  "Modelo de Pos",
            width: "auto"
        },
        {
            dataField: "serialpos", 
            caption:  "N° de Serial",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },  
        {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
          {
            dataField: "coddocumento", 
            caption:  "RIF/CI",
            width: "auto"
        },
         {
            dataField: "rlegal", 
            caption:  "Persona Contacto",
            width: "auto"
        },
        
        {
            dataField: "direccion", 
            caption:  "Dirección Fiscal",
            width: "auto"
        },
         {
            dataField: "estado", 
            caption:  "Estado",
            width: "auto"
        },
         {
            dataField: "municipio", 
            caption:  "Municipio",
            width: "auto"
        },
         {
            dataField: "parroquia", 
            caption:  "Parroquia",
            width: "auto"
        },
              
        {
            dataField: "fechafacturacion", 
            caption:  "Fecha Facturación",
            width: "auto"
        },
          {
            dataField: "nfactura", 
            caption:  "N° de Fatura",
            width: "auto"
        },
       /*   {
            dataField: "nameformapago", 
            caption:  "Forma de Pago",
            width: "auto"
        },
          {
            dataField: "ntransferencia", 
            caption:  "N° de Transferencia",
            width: "auto"
        },
         {
            dataField: "nbancoorigen", 
            caption:  "Banco Origen",
            width: "auto"
        },  */  
        {
            dataField: "costopos", 
            caption:  "Monto",
            width: "auto"
        }
                
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppReportePOS = angular.module('AppReportePOS', ['dx']);
AppReportePOS.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value,};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReportePOS.controller('ReportePOSController',['$scope','informacion',  function ReportePOSController($scope,informacion) {
  informacion.postData('api/selectReportePos')
    .then(function(datos) {
      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

            selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechainstalacion",
            caption:  "Fecha de Instalación",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        } 
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);


var AppReporteAdmmDetallada = angular.module('AppReporteAdmmDetallada', ['dx']);
AppReporteAdmmDetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteAdmmDetallada.controller('ReporteAdmmDetalladaController',['$scope','informacion',  function ReporteAdmmDetalladaController($scope,informacion) {
  informacion.postData('api/selectReporteDetalladoPOS')
    .then(function(datos) {
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "MantenimientoPOS_"+databanco+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fechainstalacion",
            caption:  "Fecha de Instalación",
            width: "auto"
        },        
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
        {
            dataField: "marcapos", 
            caption:  "Marca de Pos",
            width: "auto"
        },
         {
            dataField: "serialpos", 
            caption:  "N° de Serial",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        },
        {
            dataField: "tipocliente", 
            caption:  "Tipo de Cliente",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },  
        {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "RIF/CI",
            width: "auto"
        },
        {
            dataField: "acteconomica", 
            caption:  "Actividad Económica de la Empresa",
            width: "auto"
        },
         
         {
            dataField: "rlegal", 
            caption:  "Nombre del Representate Legal",
            width: "auto"
        },
         {
            dataField: "correorl", 
            caption:  "Correo Electrónico",
            width: "auto"
        },
         {
            dataField: "telfrl", 
            caption:  "N° Telefónico Representante Legal",
            width: "auto"
        },
        {
            dataField: "telf1", 
            caption:  "Teléfono 1",
            width: "auto"
        },
        {
            dataField: "telf2", 
            caption:  "Teléfono 2",
            width: "auto"
        },
        {
            dataField: "ncuentapos", 
            caption:  "N° Cuenta Recaudadora POS",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección de Instalación",
            width: "auto"
        },
        {
            dataField: "estado", 
            caption:  "Estado",
            width: "auto"
        },
        {
            dataField: "municipio", 
            caption:  "Municipio",
            width: "auto"
        },
        {
            dataField: "parroquia", 
            caption:  "Parroquia",
            width: "auto"
        },
        {
            dataField: "horario", 
            caption:  "Horario del Comercio",
            width: "auto"
        },
        {
            dataField: "nafiliacion", 
            caption:  "N° Afiliación",
            width: "auto"
        },  
        {
            dataField: "fechafacturacion", 
            caption:  "Fecha Facturación",
            width: "auto"
        },
        {
            dataField: "nfactura", 
            caption:  "N° de Fatura",
            width: "auto"
        },
        {
            dataField: "nameformapago", 
            caption:  "Forma de Pago",
            width: "auto"
        },
        {
            dataField: "ntransferencia", 
            caption:  "N° de Transferencia",
            width: "auto"
        },
         {
            dataField: "nbancoorigen", 
            caption:  "Banco Origen",
            width: "auto"
        }                
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppReporteEjecutivo = angular.module('AppReporteEjecutivo', ['dx']);
AppReporteEjecutivo.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { clientes: document.getElementById('clientes').value, 
                  ejecutivo: document.getElementById('ejecutivo').value,
                  fechainicial: document.getElementById('fechainicial').value,
                      fechafinal: document.getElementById('fechafinal').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteEjecutivo.controller('ReporteEjecutivoController',['$scope','informacion',  function ReporteEjecutivoController($scope,informacion) {
  informacion.postData('api/selectReporteEjecutivo')
    .then(function(datos) {
      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

            selection: {
          mode: "multiple"
      },              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "nombre", 
            caption:  "Ejecutivo",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }    
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);




var AppReporteFacturas = angular.module('AppReporteFacturas', ['dx']);
AppReporteFacturas.service('informacion', ['$http',
  function($http) {
 
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url));
    }
  }
]);

AppReporteFacturas.controller('ReporteFacturasController',['$scope','informacion',  function ReporteFacturasController($scope,informacion) {
  informacion.postData('api/selectBusquedaTotalFactura')
    .then(function(datos) {
      $scope.datos = datos.data
      
     $scope.gridOptions = {
      dataSource:$scope.datos,
      groupPanel: {
          visible: true
      },
          selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteFactura",
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 450,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
            dataField: "nombrebanco",
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption:  "Razon Social",
            width: "auto"
        },
        {
            dataField: "factura",
            caption:  "Nª de Factura",
            width: "auto"
        },
         {
            dataField: "coddocumento",
            caption:  "RIF",
            width: "auto"
        },
         {
            dataField: "nafiliacion",
            caption:  "Nº de Afiliacion",
            width: "auto"
        },
          {
            dataField: "cantpos",
            caption:  "Cantidad de POS",
            width: "auto"
        },

          {
            dataField: "marcapos",
            caption:  "Marca de POS",
            width: "auto"
        },


         {
            dataField: "fechafactura",
            caption:  "Fecha de Factura",
            width: "auto"
        },

           {
            dataField: "direccion",
            caption:  "Direccion",
            width: "auto"
        },
        {
            dataField: "iserialpos",
            caption:  "Serial POS",
            width: "auto"
        },
        {
            dataField: "iserialsim",
            caption:  "Serial Sim Card",
            width: "auto"
        }, 
        {
            dataField: "iserialmifi",
            caption:  "Serial Mifi",
            width: "auto"
        },
        {
            dataField: "usuariofact",
            caption:  "Facturado por",
            width: "auto"
        },
        {
            dataField: "fechagestion",
            caption:  "Fecha Gestion",
            width: "auto"
        }

          
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            },
            {
                column: "cantpos",
                summaryType: "sum"
            } ]
        },

      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppReportexNumFactura = angular.module('AppReportexNumFactura', ['dx']);
AppReportexNumFactura.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nfactura: document.getElementById('nfactura').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReportexNumFactura.controller('ReportexNumFacturaController',['$scope','informacion',  function ReportexNumFacturaController($scope,informacion) {
  informacion.postData('api/selectBusquedaNumFactura')
    .then(function(datos) {
      $scope.datos = datos.data
      
     $scope.gridOptions = {
      dataSource:$scope.datos,
      groupPanel: {
          visible: true
      },
          selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteFactura",
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 450,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
        {
            dataField: "nombrebanco",
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption:  "Razon Social",
            width: "auto"
        },

         {
            dataField: "factura",
            caption:  "Nª de Factura",
            width: "auto"
        },
         {
            dataField: "coddocumento",
            caption:  "RIF",
            width: "auto"
        },
         {
            dataField: "nafiliacion",
            caption:  "Nº de Afiliacion",
            width: "auto"
        },
           {
            dataField: "cantpos",
            caption:  "Cantidad de POS",
            width: "auto"
        },

          {
            dataField: "marcapos",
            caption:  "Marca de POS",
            width: "auto"
        },

         {
            dataField: "fechafactura",
            caption:  "Fecha de Factura",
            width: "auto"
        },

           {
            dataField: "direccion",
            caption:  "Direccion",
            width: "auto"
        },
        {
            dataField: "serialpos",
            caption:  "Serial POS",
            width: "auto"
        },
        {
            dataField: "serialsim",
            caption:  "Serial Sim Card",
            width: "auto"
        }, 
        {
            dataField: "serialmifi",
            caption:  "Serial Mifi",
            width: "auto"
        }
          
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },

      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppReporteEjecutivos = angular.module('AppReporteEjecutivos', ['dx']);
AppReporteEjecutivos.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { clientes: document.getElementById('clientes').value,
                  estatus: document.getElementById('estatus').value,
                   documento: document.getElementById('documento').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteEjecutivos.controller('ReporteEjecutivosController',['$scope','informacion',  function ReporteEjecutivosController($scope,informacion) {
  informacion.postData('api/selectEjecutivoReporte')
    .then(function(datos) {
      $scope.datos = datos.data
      //databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "EjecutivosPOS_",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        
        {
            dataField: "ejecutivo", 
            caption:  "Nombre del Ejecutivo Inteligensa",
            width: "auto"
        },
        //  {
        //     dataField: "ejecutivobanc", 
        //     caption:  "Nombre del Ejecutivo Banco",
        //     width: "auto"
        // },
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
        {
            dataField: "nafiliacion", 
            caption:  "N° de Afiliación",
            width: "auto"
        },
        // {
        //     dataField: "marcapos", 
        //     caption:  "Marca de Pos",
        //     width: "auto"
        // },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },  
        {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
        {
            dataField: "coddocumento", 
            caption:  "RIF/CI",
            width: "auto"
        },
        {
            dataField: "estatusp", 
            caption:  "Estatus Cliente",
            width: "auto"
        }                 
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);




var AppReporteEjecutivoP = angular.module('AppReporteEjecutivoP', ['dx']);
AppReporteEjecutivoP.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  fechainicial: document.getElementById('fechainicial').value,
                  fechafinal: document.getElementById('fechafinal').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteEjecutivoP.controller('ReporteEjecutivoPController',['$scope','informacion',  function ReporteEjecutivoPController($scope,informacion) {
  informacion.postData('api/selectReporteEjecutivoDet')
    .then(function(datos) {
        var someUrl="?cargar=ejecutivo&var=";
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "EjecutivosPOS_"+databanco+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "ibp",
            caption:  "Nombre Banco",
            width: "auto"
        },
       
        {
            dataField: "ejecut", 
            caption:  "Ejecutivo",
            width: "auto"
        },
        {
            dataField: "fechac", 
            caption:  "Fecha Carga",
            width: "auto"
        },
        {
            dataField: "namearchivo", 
            caption:  "Nombre de Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad",
            width: "auto"
        },
        {
            dataField: "estatus", 
            caption:  "Estatus",
            width: "auto"
        }              
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppEjecutivoInd = angular.module('AppEjecutivoInd', ['dx']);
AppEjecutivoInd.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  fechainicial: document.getElementById('fechainicial').value,
                  fechafinal: document.getElementById('fechafinal').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppEjecutivoInd.controller('EjecutivoIndController',['$scope','informacion',  function EjecutivoIndController($scope,informacion) {
  informacion.postData('api/selectEjecutivosReporteInd')
    .then(function(datos) {
        var someUrl="?cargar=ejecutivo&var=";
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "EjecutivosPOS_"+databanco+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "ibp",
            caption:  "Nombre Banco",
            width: "auto"
        },
       
        {
            dataField: "ejecut", 
            caption:  "Ejecutivo",
            width: "auto"
        },
        {
            dataField: "fechac", 
            caption:  "Fecha Carga",
            width: "auto"
        },
        {
            dataField: "namearchivo", 
            caption:  "Nombre de Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad",
            width: "auto"
        }              
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);




var AppReporteEquiposF = angular.module('AppReporteEquiposF', ['dx']);
AppReporteEquiposF.service('informacion', ['$http',
  function($http) {
    var vm=this;
     vm.fdatos = { //clientes: document.getElementById('clientes').value, 
                    fechainicial: document.getElementById('fechainicial').value,
                  fechafinal: document.getElementById('fechafinal').value};
    //               };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteEquiposF.controller('ReporteEquiposFController',['$scope','informacion',  function ReporteEquiposFController($scope,informacion) {
  informacion.postData('api/selectReporteFecha')
    .then(function(datos) {
      var someUrl="?cargar=reporteequipos&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

            selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "ibp",
            caption:  "Banco",
            width: "auto"
        },
         {
            dataField: "marcapos",
            caption:  "Marca de Pos",
            width: "auto"
        },
        {
            dataField: "tipopos",
            caption:  "Tipo de Pos",
            width: "auto"
        },
        
        {
            dataField: "costopos", 
            caption:  "Monto Total",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Pos",
            width: "auto"
        } 
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);



var AppReporteEquipos = angular.module('AppReporteEquipos', ['dx']);
AppReporteEquipos.service('informacion', ['$http',
  function($http) {
    var vm=this;
     vm.fdatos = { clientes: document.getElementById('clientes').value,
                    fechainicial: document.getElementById('fechainicial').value,
                      fechafinal: document.getElementById('fechafinal').value};
    //               };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteEquipos.controller('ReporteEquiposController',['$scope','informacion',  function ReporteEquiposController($scope,informacion) {
  informacion.postData('api/selectReporteEquipos')
    .then(function(datos) {
      var someUrl="?cargar=reporteequipospos&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

            selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "ibp",
            caption:  "Banco",
            width: "auto"
        },
         {
            dataField: "marcapos",
            caption:  "Marca de Pos",
            width: "auto"
        },
        {
            dataField: "tipopos",
            caption:  "Tipo de Pos",
            width: "auto"
        },
        
        {
            dataField: "costopos", 
            caption:  "Monto Total",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },
        {
            dataField: "fecha", 
            caption:  "Fecha de Facturacion",
            width: "auto"
        }  
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);

var AppEquiposPOSS = angular.module('AppEquiposPOSS', ['dx']);
AppEquiposPOSS.service('informacion', ['$http',
  function($http) {
    var vm=this;
     vm.fdatos = {   clientes: document.getElementById('clientes').value,
                     fecha: document.getElementById('fecha').value,
                     marca: document.getElementById('marca').value,
                    tipo: document.getElementById('tipo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppEquiposPOSS.controller('EquiposPOSSController',['$scope','informacion',  function EquiposPOSSController($scope,informacion) {
  informacion.postData('api/selectEquipos')
    .then(function(datos) {
    //  var someUrl="?cargar=reporteequipospos&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

            selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        //  {
        //   dataField: 'enlace',
        //   caption:  "Acción",
        //   width: "auto",
        //    cellTemplate: function (container, options) {
        //      $('<a> Ver Detalles </a>')
        //        .attr('href', someUrl + options.value)
        //        .appendTo(container);
        //     }  
        // }, 
         {
            dataField: "razonsocial",
            caption:  "Razon Social",
            width: "auto"
        },
        {
            dataField: "nafiliacion",
            caption:  "Nº de Afiliacion",
            width: "auto"
        },      
        {
            dataField: "coddocumento", 
            caption:  "Doc. de Identidad",
            width: "auto"
        },
         {
            dataField: "marcapos", 
            caption:  "Marca de POS",
            width: "auto"
        },
         {
            dataField: "tipopos", 
            caption:  "Tipo de POS",
            width: "auto"
        },
         {
            dataField: "fechaf", 
            caption:  "Fecha de Facturacion",
            width: "auto"
        },
         {
            dataField: "costopos", 
            caption:  "Precio del POS",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);


/*
var AppEquiposPOSS = angular.module('AppEquiposPOSS', ['dx']);
AppEquiposPOSS.service('informacion', ['$http',
  function($http) {
    var vm=this;
     vm.fdatos = {   clientes: document.getElementById('clientes').value,
                     marca: document.getElementById('marca').value,
                    tipo: document.getElementById('tipo').value, 
                     fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppEquiposPOSS.controller('EquiposPOSSController',['$scope','informacion',  function EquiposPOSSController($scope,informacion) {
  informacion.postData('api/selectEquipos')
    .then(function(datos) {
    //  var someUrl="?cargar=reporteequipospos&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

            selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        //  {
        //   dataField: 'enlace',
        //   caption:  "Acción",
        //   width: "auto",
        //    cellTemplate: function (container, options) {
        //      $('<a> Ver Detalles </a>')
        //        .attr('href', someUrl + options.value)
        //        .appendTo(container);
        //     }  
        // }, 
         {
            dataField: "razonsocial",
            caption:  "Razon Social",
            width: "auto"
        },
        {
            dataField: "nafiliacion",
            caption:  "Nº de Afiliacion",
            width: "auto"
        },      
        {
            dataField: "coddocumento", 
            caption:  "Doc. de Identidad",
            width: "auto"
        },
         {
            dataField: "marcapos", 
            caption:  "Marca de POS",
            width: "auto"
        },
         {
            dataField: "tipopos", 
            caption:  "Tipo de POS",
            width: "auto"
        },
         {
            dataField: "fechaf", 
            caption:  "Fecha de Facturacion",
            width: "auto"
        },
         {
            dataField: "costopos", 
            caption:  "Precio del POS",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);*/


var AppReporteERP = angular.module('AppReporteERP', ['dx']);
AppReporteERP.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteERP.controller('ReporteERPController',['$scope','informacion',  function ReporteERPController($scope,informacion) {
  informacion.postData('api/selectReporteERP')
    .then(function(datos) {
      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },  
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }   
      ],
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);



var AppReporteERPDetallado = angular.module('AppReporteERPDetallado', ['dx']);
AppReporteERPDetallado.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteERPDetallado.controller('ReporteERPDetalladoController',['$scope','informacion',  function ReporteERPDetalladoController($scope,informacion) {
  informacion.postData('api/selectReporteDetalladoERP')
    .then(function(datos) {
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ERP_"+databanco+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Envio del Archivo",
            width: "auto"
        },
        
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
        {
            dataField: "activofijo", 
            caption:  "N° de Activo Fijo",
            width: "auto"
        },
        {
            dataField: "fechacompra", 
            caption:  "Fecha de Compra POS",
            width: "auto"
        },
        {
            dataField: "marcapos", 
            caption:  "Modelo de Pos",
            width: "auto"
        },
        {
            dataField: "serialpos", 
            caption:  "N° de Serial",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },  
        {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
          {
            dataField: "coddocumento", 
            caption:  "RIF/CI",
            width: "auto"
        },
         {
            dataField: "rlegal", 
            caption:  "Representante Legal",
            width: "auto"
        },
         {
            dataField: "nombre", 
            caption:  "Persona Contacto",
            width: "auto"
        },
         {
            dataField: "direccion", 
            caption:  "Dirección Fiscal",
            width: "auto"
        },
         {
            dataField: "estado", 
            caption:  "Estado",
            width: "auto"
        },
         {
            dataField: "municipio", 
            caption:  "Municipio",
            width: "auto"
        },
         {
            dataField: "parroquia", 
            caption:  "Parroquia",
            width: "auto"
        },
         {
            dataField: "d_codepostal", 
            caption:  "Código Postal",
            width: "auto"
        },
         {
            dataField: "telfrl", 
            caption:  "N° Telefonico",
            width: "auto"
        },
              
        {
            dataField: "fechafacturacion", 
            caption:  "Fecha Facturación",
            width: "auto"
        },
          {
            dataField: "nfactura", 
            caption:  "N° de Factura",
            width: "auto"
        },   
        {
            dataField: "costopos", 
            caption:  "Monto",
            width: "auto"
        }
                
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);





//Facturacion
var AppFactutaracion = angular.module('AppFactutaracion', ['dx']);
AppFactutaracion.controller('FacturacionController', function FacturacionController($scope,$http) {
    var vm=this;
    vm.fdatos = { rif: document.getElementById('rif').value,
                  banco: document.getElementById('banco').value,
                  fechacarga: document.getElementById('fechacarga').value,
                  namearchivo: document.getElementById('namearchivo').value,
                  idregistro: document.getElementById('idregistro').value };

        $http.get('api/selectformapago.php').then(function(response){
        $scope.formapago = response.data;
       // console.log($scope.formapago);
          $scope.tipoformapagovalidation={
              placeholder: 'Ingrese Forma de pago...',
              dataSource: $scope.formapago,
              valueExpr: 'codformapago',
              displayExpr: 'nameformapago'
              };


      $http.get('api/selecttipotarjeta.php').then(function(response){
      $scope.tipotarjeta = response.data;
      //console.log($scope.tipotarjeta);
          
        $scope.tipotarjetavalidation={
          placeholder: 'Ingrese Tipo de Tarjeta...',
          dataSource:  $scope.tipotarjeta,
          valueExpr: 'codtarjeta',
          displayExpr: 'ntarjeta'
        };
    

      $http.get('api/selecttipobanco.php').then(function(response){
      $scope.tipobanco = response.data;
      //console.log($scope.tipobanco);
        $scope.bancovalidation={
              placeholder: 'Ingrese Banco de Pago...',
              dataSource: $scope.tipobanco,
              valueExpr: 'bancoorigen',
              displayExpr: 'ibp'
              };

      
       $http.post('api/SelectFacturacion',vm.fdatos).then(function(datos) {
       var facturacion = datos.data  
       $scope.facturar=facturacion[0];
       // console.log($scope.facturar);
     

      var maxDate = new Date();
      var now = new Date();
     

         

      $scope.FFValidationRules = {
        validationRules: [{
            type: "required",
            message: "La fecha es requerida"
        }]
    }; 
         
    $scope.FacturaValidationRules = {
        validationRules: [{
            type: "required",
            message: "El Nº de Factura es requerida"
        },{
            type: "pattern",
            pattern: /^\d*$/,
            message: "No puede usar letras."
      }]
    };
     
      $scope.formapagoValidationRules = {
        validationRules: [{
            type: "required",
            message: "Forma de pago es requerida"
        }]
    };

      $scope.TransferenciaValidationRules = {
        validationRules: [{
            type: "required",
            message: "El Nº de Referencia es requerido"
        },{
            type: "pattern",
            pattern: /^\d*$/,
            message: "No puede usar letras."
      }]
    };

   

      $scope.bancoValidationRules = {
        validationRules: [{
            type: "required",
            message: "El Banco de Origen de la Transferencia es requerido"
        }]
    };
     
     $scope.dateBox = {
        dateFormat: {
            type: "date",
            value: now,
            message: "La Fecha es requerida"
        },
        timeFormat: 
        {
            type: "time",
            placeholder: 'Ingrese la hora de pago...'
        }
  };
 
    $scope.HoraValidationRules = {
        validationRules: [{
            type: "required",
            message: "La hora es requerida",
        }]
    }; 

   
         

    $scope.onFormSubmit = function(info) {
      DevExpress.ui.notify({
            message: "Datos Cargados Con éxito",
            position: {
                my: "center top",
                at: "center top"
            }
        }, "success", 5000);
        $http.post('api/updatefacturacion.php', info).then(function(response){
    });
    };
    
    $scope.submitButtonOptions = {
        itemType: 'button',
        text: 'Register',
        type: 'success',        
        useSubmitBehavior: true
    };

      }); 
      }); 
      }); 
      }); 
});   


//Facturación POR RIF O RAZON SOCIAL

var AppFacturacionRif = angular.module('AppFacturacionRif', ['dx']);
AppFacturacionRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value,
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppFacturacionRif.controller('FacturacionRifController',['$scope','informacion',  function FacturacionRifController($scope,informacion) {
  informacion.postData('api/SelectFacturacionRif')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }

      var someUrl="?cargar=facturarcliente&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
  {
          dataField: "estatuspago",
          caption: "Estatus Pago",
          width: "auto",
          cssClass: "enlace",

   cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Pago Confirmado' +'</td>')
     .css('color','white') 
     .css('background','#01DF3A')
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Pendiente Por Pagos' +'</td>')
      .css('color','#f1f1f1')
      .css('background','orange')
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="0"){

        $('<td>'+ 'Pendiente Por Confirmar' +'</td>')
        .css('color','#f1f1f1')
        .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

       }          
        },
        {

          dataField: 'enlaceafiliado',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Datos Facturación </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "idestatusequipo", 
            caption:  "Estatus del Equipo",
            width: "auto",
            cssClass: "enlace",

            cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Inteliservices' +'</td>')
     /*.css('color','white') 
     .css('background','#01DF3A')*/
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Verificar Serial' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="3"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="4"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="5"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
      else if(x=="6"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
      else if(x=="8"){

        $('<td>'+ 'Entregado al Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="9"){

        $('<td>'+ 'Declinacion de Compra' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="10"){

        $('<td>'+ 'POS Instalado (Inteligensa.)' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
       else if(x=="12"){

        $('<td>'+ 'POS Instalado (Maracaibo.)' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="13"){

        $('<td>'+ 'Configuración Mifi' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="14"){

        $('<td>'+ 'Inteliservices' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }

       else if(x=="16"){
      $('<td>'+ 'Revisar Mifi' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } 
      else if(x=="0"){

        $('<td>'+ 'Sin Parámetros' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
       else if(x==null){
      $('<td>'+ 'Por Asignar Serial' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      }

      else if(x=="20"){

        $('<td>'+ 'Gestión del Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }
      else if(x=="21"){

        $('<td>'+ 'Gestión Maracaibo' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
      

    }  
        },
        {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
        {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliación",
            width: "auto"
        },  
        {
            dataField: "ibp",
            caption:  "Banco",
            width: "auto"
        },
         {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        }           
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);




var AppFacturacionRifdeMifi = angular.module('AppFacturacionRifdeMifi', ['dx']);
AppFacturacionRifdeMifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value,
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value,
                  coordinador: document.getElementById('coordinador').value,
                  coordinadordos: document.getElementById('coordinadordos').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppFacturacionRifdeMifi.controller('FacturacionRifdeMifiController',['$scope','informacion',  function FacturacionRifdeMifiController($scope,informacion) {
  informacion.postData('api/selectFacturacionMifixRif')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }

      var someUrl="?cargar=facturarclientemifi&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
  {
          dataField: "estatuspago",
          caption: "Estatus Pago",
          width: "auto",
          cssClass: "enlace",

   cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Pago Confirmado' +'</td>')
     .css('color','white') 
     .css('background','#01DF3A')
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Pendiente Por Pagos' +'</td>')
      .css('color','#f1f1f1')
      .css('background','orange')
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="0"){

        $('<td>'+ 'Pendiente Por Confirmar' +'</td>')
        .css('color','#f1f1f1')
        .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

       }          
        },
        {

          dataField: 'enlaceafiliado',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Datos Facturación </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
        {
            dataField: "idestatusequipo", 
            caption:  "Estatus del Equipo",
            width: "auto",
            cssClass: "enlace",

                cellTemplate: function (container, options) {
   var x = options.value;
     if(x=='1'){
     $('<td>'+ 'Pendiente por Instalar' +'</td>')
     /*.css('color','white') 
     .css('background','#01DF3A')*/
     .css('text-align','center')
     .css('border-radius','0.2em')
     .css('padding','.3em')
     .appendTo(container);

      } else if(x=='2'){
      $('<td>'+ 'Revisión de Equipo' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } else if(x=="3"){

        $('<td>'+ 'Mifi Instalado' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="4"){

        $('<td>'+ 'Declinación de Compra' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="5"){

        $('<td>'+ 'Gestionado por Inteligensa' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

      else if(x=="0"){

        $('<td>'+ 'En proceso de Configuración' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
      
       else if(x==null){
      $('<td>'+ 'Por asignar serial mifi' +'</td>')
      /*.css('color','#f1f1f1')
      .css('background','orange')*/
      .css('text-align','center')
      .css('border-radius','0.2em')
      .css('padding','.3em')
      .appendTo(container); 

      } 
    

    } 
        },
        {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
       /* {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliación",
            width: "auto"
        },  */
        {
            dataField: "ibp",
            caption:  "Banco",
            width: "auto"
        },
         {
            dataField: "tipopos", 
            caption:  "Tipo de Equipo",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Equipos",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        }           
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


//Data SERVICIO TECNICO Detallada

var AppDataSTdetallada = angular.module('AppDataSTdetallada', ['dx']);
AppDataSTdetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppDataSTdetallada.controller('DataSTdetalladaController',['$scope','informacion',  function DataSTdetalladaController($scope,informacion) {
  informacion.postData('api/SelectDataSTDetallada')
    .then(function(datos) {
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioTecnico_"+databanco+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
        {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliación",
            width: "auto"
        },
         {
            dataField: "factura", 
            caption:  "Nº de Factura",
            width: "auto"
        },
          {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
          {
            dataField: "coddocumento", 
            caption:  "RIF/CI",
            width: "auto"
        },
         {
            dataField: "rlegal", 
            caption:  "Persona Contacto",
            width: "auto"
        },
          {
            dataField: "telfrl", 
            caption:  "N° Telefónico ",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección de Instalación",
            width: "auto"
        },
          {
            dataField: "telf1", 
            caption:  "Teléfono 1",
            width: "auto"
        },
         {
            dataField: "telf2", 
            caption:  "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },
        {
            dataField: "marcapos", 
            caption:  "Marca de Pos",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
       
        {
            dataField: "tipolinea", 
            caption:  "Tipo de Linea",
            width: "auto"
        },
         {
            dataField: "numeroterminal", 
            caption:  "N° terminal",
            width: "auto"
        },  
        {
            dataField: "serialpos", 
            caption:  "N° Serial de POS",
            width: "auto"
        },
          {
            dataField: "numerosim", 
            caption:  "N° Sim Card",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


// Data gestion tecnica


var AppSTGestiontecnica = angular.module('AppSTGestiontecnica', ['dx']);
AppSTGestiontecnica.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value, 
                  tipousuario: document.getElementById('tipousuario').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                  usuario: document.getElementById('usuario').value };

    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);


AppSTGestiontecnica.controller('STGestiontecnicaController',['$scope','informacion',  function STGestiontecnicaController($scope,informacion) {
  informacion.postData('api/selectGestionTecnica')
    .then(function(datos) {

      var someUrl="?cargar=buscarrecepcion&var=";
      $scope.datos = datos.data
      cliente= document.getElementById('banco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "FacturaciónMasiva_"+cliente+'_'+fechaactual,
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {

          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalle </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }
            
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppServicioTecMifi = angular.module('AppServicioTecMifi', ['dx']);
AppServicioTecMifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTecMifi.controller('ServicioTecnicoMifiController',['$scope','informacion',  function ServicioTecnicoMifiController($scope,informacion) {
  informacion.postData('api/selectServicioTecnicoMifi')
    .then(function(datos) {
      var someUrl="?cargar=buscarconfigmifi&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
            {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },   
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }  
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
     
    };
    },
    ); 
}]);







// servicio tecnico
var AppServicioTec = angular.module('AppServicioTec', ['dx']);
AppServicioTec.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTec.controller('ServicioTecController',['$scope','informacion',  function ServicioTecController($scope,informacion) {
  informacion.postData('api/selectServicioTecMasivo')
    .then(function(datos) {
      var someUrl="?cargar=buscarconfigpos&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
            {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },   
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }  
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
     
    };
    },
    ); 
}]);

var AppServicioInstalar = angular.module('AppServicioInstalar', ['dx']);
AppServicioInstalar.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioInstalar.controller('ServicioInstalarController',['$scope','informacion',  function ServicioInstalarController($scope,informacion) {
  informacion.postData('api/selectServicioInstalar')
    .then(function(datos) {
      var someUrl="?cargar=buscarInstPOS&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
        selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
            {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },   
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }  
      ],

      summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
    paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },

      showBorders: true,
    
    };
    },
    ); 
}]);


var AppServiciodetallada = angular.module('AppServiciodetallada', ['dx']);
AppServiciodetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServiciodetallada.controller('ServiciodetalladaController',['$scope','informacion',  function ServiciodetalladaController($scope,informacion) {
  informacion.postData('api/selectServicioDetallado')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=datosserviciosafiliado&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioDetallado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },     
         {
            dataField: "estatuss",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Logistica-Parametros' +'</td>')
         .css('color','white') 
         .css('background','#808080')
         .css('text-align','center')
         .css('border-radius','0.2em')
         .css('padding','.3em')
         .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'Verificar Serial' +'</td>')
          .css('color','#f1f1f1')
          .css('background','#000080')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container); 

          } 

          else if(x=='3'){

            $('<td>'+ 'Laboratorio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='4'){

            $('<td>'+ 'Logistica-Envio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#8C86B0')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } else if(x=='5'){

            $('<td>'+ 'Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#008080')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

            else if(x=='6'){

            $('<td>'+ 'Operaciones' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='8'){

            $('<td>'+ 'Entregado al Rosal' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='9'){

            $('<td>'+ 'Declinacion de Compra' +'</td>')
            //.css('color','#f1f1f1')
            //.css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='10'){

            $('<td>'+ 'POS Instalado (Inteligensa)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='0'){

            $('<td>'+ 'Sin Parámetros' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x==null){

            $('<td>'+ 'Por Asignar Serial' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }

          else if(x=='12'){

            $('<td>'+ 'POS Instalado (Maracaibo)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }
          else if(x=='13'){

            $('<td>'+ 'Configuracion MIFI' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }   
          else if(x=='14'){

            $('<td>'+ 'MIFI Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }  
          else if(x=='16'){

            $('<td>'+ 'Revisar Mifi' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }

          else if(x=="20"){

        $('<td>'+ 'Gestión del Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }

      else if(x=="21"){

        $('<td>'+ 'Gestión Maracaibo' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }  

        }          
            }, 
        /* {
            dataField: "desc_status",
            caption:  "Estatus de Servicio",
            width: "auto", 
        }, */

        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
        {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }  
      ],
        summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },

      showBorders: true,
     
    };
    },
    ); 
}]);

var AppServicioconfig = angular.module('AppServicioconfig', ['dx']);
AppServicioconfig.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioconfig.controller('ServicioConfigController',['$scope','informacion',  function ServicioConfigController($scope,informacion) {
  informacion.postData('api/selectServicioConfig')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=datoserialpos&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioDetallado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
         {
            dataField: "estatuservices",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Logistica-Parametros' +'</td>')
         .css('color','white') 
         .css('background','#808080')
         .css('text-align','center')
         .css('border-radius','0.2em')
         .css('padding','.3em')
         .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'Verificar Serial' +'</td>')
          .css('color','#f1f1f1')
          .css('background','#000080')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container); 

          } 

          else if(x=='3'){

            $('<td>'+ 'Laboratorio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='4'){

            $('<td>'+ 'Logistica-Envio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#8C86B0')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } else if(x=='5'){

            $('<td>'+ 'Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#008080')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

           } else if(x=='6'){

            $('<td>'+ 'Operaciones' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='8'){

            $('<td>'+ 'Entregado al Rosal' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='9'){

            $('<td>'+ 'Declinacion de Compra' +'</td>')
            //.css('color','#f1f1f1')
            //.css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='10'){

            $('<td>'+ 'POS Instalado (Inteligensa)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='0'){

            $('<td>'+ 'Sin Parámetros' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x==null){

            $('<td>'+ 'Por Asignar Serial' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }

          else if(x=='12'){

            $('<td>'+ 'POS Instalado (Maracaibo)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }  
           else if(x=='13'){

            $('<td>'+ 'Configuración MIFI' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }  
           else if(x=='14'){

            $('<td>'+ 'MIFI Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }  

           else if(x=='16'){

            $('<td>'+ 'Revisar Mifi' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }
            else if(x=="20"){

        $('<td>'+ 'Gestión del Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }

      else if(x=="21"){

        $('<td>'+ 'Gestión Maracaibo' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }  

        }          
            }, 
          
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            width: "auto"
        }, 

        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
        {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }
      ],

       summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
     
     
    };
    },
    ); 
}]);



    var AppServicioTecnicoMifiDetallado = angular.module('AppServicioTecnicoMifiDetallado', ['dx']);
AppServicioTecnicoMifiDetallado.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTecnicoMifiDetallado.controller('ServicioTecnicoMifiDetalladoController',['$scope','informacion',  function ServicioTecnicoMifiDetalladoController($scope,informacion) {
  informacion.postData('api/selectServicioDetalladoMifi')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=procesarconfigmifi&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioDetallado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },     
         {
            dataField: "estatuservices",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Pendiente por Instalar' +'</td>')
         .css('color','white') 
         .css('background','#808080')
         .css('text-align','center')
         .css('border-radius','0.2em')
         .css('padding','.3em')
         .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'Revisión de Equipo' +'</td>')
          .css('color','#f1f1f1')
          .css('background','#000080')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container); 

          } 

          else if(x=='3'){

            $('<td>'+ 'Mifi Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=="4"){

        $('<td>'+ 'Declinación de Compra' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 
            else if(x=='5'){

            $('<td>'+ 'Gestionado por Inteligensa' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#008080')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='0'){

            $('<td>'+ 'En proceso de configuración' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#008080')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 


        }          
            }, 
        /* {
            dataField: "desc_status",
            caption:  "Estatus de Servicio",
            width: "auto", 
        }, */

        {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Equipo",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }  
      ],
        summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },

      showBorders: true,
     
    };
    },
    ); 
}]);


var AppServicioMantRif = angular.module('AppServicioMantRif', ['dx']);
AppServicioMantRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value},

    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioMantRif.controller('ServicioMantRifController',['$scope','informacion',  function ServicioMantRifController($scope,informacion) {
  informacion.postData('api/selectServicioMantenimiento')
    .then(function(datos) {
       var permisos= document.getElementById('permisos').value;

       if (permisos!='W') {
     var mostrar=false;
  
      }
      else{
        var mostrar=true;
        
      }

        var someUrl="?cargar=recepcionmantenimiento&var=";
      $scope.datos = datos.data
       
      $scope.gridOptions = {
      dataSource:$scope.datos,

       groupPanel: {
          visible: true
      },  
       selection: {
          mode: "multiple"
      },     
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },

         {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Agregar Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "terminal", 
            caption:  "Nº de Terminal",
            width: "auto"
        },
        {
            dataField: "serial", 
            caption:  "Nº de Serial Pos",
            width: "auto"
        },
                
         {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
          {
            dataField: "marcapos", 
            caption:  "Modelo de Pos",
            width: "auto"
        },

        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
          {
            dataField: "fechainstalacion", 
            caption:  "Fecha de Instalación",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            visible: false,
            caption:  "Nombre del archivo",
            width: "auto"
        }          
      ],
    
      paging: { pageSize: 10 },
      showBorders: true,
        summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
     
    };
    },
    ); 
}]);



var AppMantenimientoServ = angular.module('AppMantenimientoServ', ['dx']);
AppMantenimientoServ.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { cliente: document.getElementById('cliente').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppMantenimientoServ.controller('AppMantenimientoServController',['$scope','informacion',  function AppMantenimientoServController($scope,informacion) {
  informacion.postData('api/selectServicioMantenimientoGeneral')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;

       if (permisos!='W') {
     var mostrar=false;
  
      }
      else{
        var mostrar=true;
        
      }
      var someUrl="?cargar=recepcionmantenimiento&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

        selection: {
          mode: "multiple"
      }, 
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "sec",
            caption:  "Consecutivo",
            width: "auto"
        },
         {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Agregar Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },  
         {
            dataField: "cliente",
            caption:  "ID",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption:  "Razón Social",
            width: "auto"
        },
        {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
         {
            dataField: "afiliado", 
            caption:  "Afiliado",
            width: "auto"
        },
        {
            dataField: "ibpbanco", 
            caption:  "Banco",
            width: "auto"
        },
       
          {
            dataField: "estatus", 
            caption:  "Estatus del Equipo",
            width: "auto"
        },
        {
            dataField: "marcapos", 
            caption:  "Marca de Equipo",
            width: "auto"
        },

        {
            dataField: "tipopos", 
            caption:  "Tipo de Comunicación",
            width: "auto"
        }
                  
      ],
      summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);



var AppServicioMantenimiento = angular.module('AppServicioMantenimiento', ['dx']);
AppServicioMantenimiento.controller('ServicioMantenimientoController', function ServicioMantenimientoController($scope,$http) {
    var vm=this;
    vm.fdatos = { nafiliacion: document.getElementById('nafiliacion').value,
                  nterminal: document.getElementById('nterminal').value};

        $http.get('api/selectformapago.php').then(function(response){
        $scope.formapago = response.data;
       // console.log($scope.formapago);
          $scope.tipoformapagovalidation={
               placeholder: 'Seleccione...',
              dataSource: $scope.formapago,
              valueExpr: 'codformapago',
              displayExpr: 'nameformapago'
              };


      $http.get('api/selecttipotarjeta.php').then(function(response){
      $scope.tipotarjeta = response.data;
      //console.log($scope.tipotarjeta);
          
        $scope.tipotarjetavalidation={
          placeholder: 'Ingrese Tipo de Tarjeta...',
          dataSource:  $scope.tipotarjeta,
          valueExpr: 'codtarjeta',
          displayExpr: 'ntarjeta'
        };
    

      $http.get('api/selecttipobanco.php').then(function(response){
      $scope.tipobanco = response.data;
      //console.log($scope.tipobanco);
        $scope.bancovalidation={
              placeholder: 'Ingrese Banco de Pago...',
              dataSource: $scope.tipobanco,
              valueExpr: 'bancoorigen',
              displayExpr: 'ibp'
              };

      
       $http.post('api/selectMantenimiento',vm.fdatos).then(function(datos) {
      var mantenimiento = datos.data  
       $scope.servicio=mantenimiento[0];
      

      var maxDate = new Date();
     var now = new Date();
        
      $scope.formapagoValidationRules = {
        validationRules: [{
            type: "required",
            message: "Forma de pago es requerida"
        }]
    };

     
      $scope.bancoValidationRules = {
        validationRules: [{
            type: "required",
            message: "El Banco de Origen de la Transferencia es requerido"
        }]
    };

        $scope.nombreValidationRules = {
        validationRules: [{
            type: "required",
            message: "El Nombre es requerido"
        }]
    };

      $scope.cedulaValidationRules = {
        validationRules: [{
            type: "required",
            message: "Debe insertar el Nº de Cédula"
        }]
    };
 
   

    $scope.onFormSubmit = function(info) {
      DevExpress.ui.notify({
            message: "Datos Cargados Con éxito",
            position: {
                my: "center top",
                at: "center top"
            }
        }, "success", 5000);
        $http.post('api/updatemantenimiento.php', info).then(function(response){
    });
    };
    
    $scope.submitButtonOptions = {
        text: 'Register',
        type: 'success',        
        useSubmitBehavior: true
    };

      }); 
      }); 
      }); 
      }); 
});   




var AppMantenimientoServRep = angular.module('AppMantenimientoServRep', ['dx']);
AppMantenimientoServRep.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { status: document.getElementById('status').value,
                  modalidad: document.getElementById('modalidad').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppMantenimientoServRep.controller('AppMantenimientoServRepController',['$scope','informacion',  function AppMantenimientoServRepController($scope,informacion) {
  informacion.postData('api/selectDomiciliacionReporte')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;

      var someUrl="?cargar=recepcionmantenimiento&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,

        selection: {
          mode: "multiple"
      }, 
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "sec",
            caption:  "Consecutivo",
            width: "auto"
        },
         {
            dataField: "cliente",
            caption:  "ID",
            width: "auto"
        },
        {
            dataField: "razon_social",
            caption:  "Razón Social",
            width: "auto"
        },
        {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
         {
            dataField: "descpago", 
            caption:  "Estatus de Domiciliación",
            width: "auto"
        },
        {
            dataField: "banco", 
            caption:  "Banco",
            width: "auto"
        },
       
          {
            dataField: "descmodalidad", 
            caption:  "Modalidad de Pago",
            width: "auto"
        }
                  
      ],
      summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      showBorders: true,
     
    };
    },
    ); 
}]);



var AppServicioTecRif = angular.module('AppServicioTecRif', ['dx']);
AppServicioTecRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTecRif.controller('ServicioTecRifController',['$scope','informacion',  function ServicioTecRifController($scope,informacion) {
  informacion.postData('api/selectServicioAfiliadoRif')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=datosserviciosafiliado&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [

        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        
         {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "estatuss",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

        cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Logistica-Parametros' +'</td>')
         .css('color','white') 
         .css('background','#808080')
         .css('text-align','center')
         .css('border-radius','0.2em')
         .css('padding','.3em')
         .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'Verificar Serial' +'</td>')
          .css('color','#f1f1f1')
          .css('background','#000080')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container); 

          } 

          else if(x=='3'){

            $('<td>'+ 'Laboratorio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='4'){

            $('<td>'+ 'Logistica-Envio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#8C86B0')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } else if(x=='5'){

            $('<td>'+ 'Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#008080')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

            else if(x=='6'){

            $('<td>'+ 'Operaciones' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='8'){

            $('<td>'+ 'Entregado al Rosal' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='9'){

            $('<td>'+ 'Declinacion de Compra' +'</td>')
            //.css('color','#f1f1f1')
            //.css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          
          else if(x=='10'){

            $('<td>'+ 'POS Instalado (Inteligensa)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='0'){

            $('<td>'+ 'Sin Parámetros' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x==null){

            $('<td>'+ 'Por Asignar Serial' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }

          else if(x=='12'){

            $('<td>'+ 'POS Instalado (Maracaibo)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='13'){

            $('<td>'+ 'Configuracion MIFI' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='14'){

            $('<td>'+ 'MIFI Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='16'){

            $('<td>'+ 'Revisar Mifi' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

            else if(x=="20"){

        $('<td>'+ 'Gestión del Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }

      else if(x=="21"){

        $('<td>'+ 'Gestión Maracaibo' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }  

        }          
             }, 

        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },

        {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
          {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }    
      ],

      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"

            },
              ],
          },
      paging: { pageSize: 10 },
      showBorders: true,
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
     
    };
    },
    ); 
}]);

var AppServicioTecRifgestion = angular.module('AppServicioTecRifgestion', ['dx']);
AppServicioTecRifgestion.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTecRifgestion.controller('ServicioTecRifgestionController',['$scope','informacion',  function ServicioTecRifgestionController($scope,informacion) {
  informacion.postData('api/selectServicioAfiliadoRifgestion')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=datoserialpos&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [

        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        
         {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "estatuservices",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

        cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Logistica-Parametros' +'</td>')
         .css('color','white') 
         .css('background','#808080')
         .css('text-align','center')
         .css('border-radius','0.2em')
         .css('padding','.3em')
         .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'Verificar Serial' +'</td>')
          .css('color','#f1f1f1')
          .css('background','#000080')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container); 

          } 

          else if(x=='3'){

            $('<td>'+ 'Laboratorio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='4'){

            $('<td>'+ 'Logistica-Envio' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#8C86B0')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } else if(x=='5'){

            $('<td>'+ 'Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#008080')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

            else if(x=='6'){

            $('<td>'+ 'Operaciones' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
       
          else if(x=='8'){

            $('<td>'+ 'Entregado al Rosal' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='9'){

            $('<td>'+ 'Declinacion de Compra' +'</td>')
            //.css('color','#f1f1f1')
            //.css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='10'){

            $('<td>'+ 'POS Instalado (Inteligensa)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='0'){

            $('<td>'+ 'Sin Parámetros' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x==null){

            $('<td>'+ 'Por Asignar Serial' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }

          else if(x=='12'){

            $('<td>'+ 'POS Instalado (Maracaibo)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='13'){

            $('<td>'+ 'Configuración MIFI' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='14'){

            $('<td>'+ 'MIFI Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='16'){

            $('<td>'+ 'Revisar Mifi' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }

            else if(x=="20"){

        $('<td>'+ 'Gestión del Rosal' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }

      else if(x=="21"){

        $('<td>'+ 'Gestión Maracaibo' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      }   

        }          
            }, 
        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
         {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
          {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }    
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"

            },
              ],
          },
                       
      paging: { pageSize: 10 },
      showBorders: true,
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
     
    };
    },
    ); 
}]);
//magallanes

var AppServicioTecMifiRif = angular.module('AppServicioTecMifiRif', ['dx']);
AppServicioTecMifiRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTecMifiRif.controller('ServicioTecRifgestionMifiController',['$scope','informacion',  function ServicioTecRifgestionMifiController($scope,informacion) {
  informacion.postData('api/selectServicioTecnicoRifMifi')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=procesarconfigmifi&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [

        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        
         {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "estatuservices",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

        cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Pendiente por Instalar' +'</td>')
         .css('color','white') 
         .css('background','#808080')
         .css('text-align','center')
         .css('border-radius','0.2em')
         .css('padding','.3em')
         .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'Revisión de Equipo' +'</td>')
          .css('color','#f1f1f1')
          .css('background','#000080')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container); 

          } 

          else if(x=='3'){

            $('<td>'+ 'Mifi Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=="4"){

        $('<td>'+ 'Declinación de Compra' +'</td>')
        // .css('color','#f1f1f1')
        // .css('background','red')
        .css('text-align','center')
        .css('border-radius','0.2em')
        .css('padding','.3em')
        .appendTo(container); 

      } 

          else if(x=='5'){

            $('<td>'+ 'Gestionado por Inteligensa' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#8C86B0')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } else if(x=='0'){

            $('<td>'+ 'En proceso de configuración' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#008080')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

        }          
            }, 
         {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
          {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Equipo",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }    
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"

            },
              ],
          },
                       
      paging: { pageSize: 10 },
      showBorders: true,
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
     
    };
    },
    ); 
}]);
var AppServicioTecnicoAfiliado = angular.module('AppServicioTecnicoAfiliado', ['dx']);

AppServicioTecnicoAfiliado.controller('ServicioTecnicoAfiliadoController', function ServicioTecnicoAfiliadoController($scope,$http) {
    
   var  afiliado= document.getElementById('nroafiliacion').value;
   $http.get('api/selectserialpos.php?afiliado='+afiliado).then(function(response){
   $scope.serialpos = response.data;


    var Url = "api/selectServicioTecnicoAfiliado.php";
    var someUrl="?cargar=datoserialpos&var=";
    var  vari= document.getElementById('var').value;
    var  codestatus= document.getElementById('codestatus').value;

    if (codestatus==5 || codestatus==10 || codestatus==12) {

      var edit=false;

    } else {

       var edit=true;
    }


      var applyFilterTypes = [{
            key: "auto",
            name: "Immediately"
        }, {
            key: "onClick",
            name: "On Button Click"
        }],
        dataGrid;
    
    function getOrderDay(rowData) {
        return (new Date(rowData.OrderDate)).getDay();
    }
    
    $scope.filterRow = {
        visible: true,
        applyFilter: "auto"
    };
    
    $scope.headerFilter = {
        visible: true
    };

    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "serialpo",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
          //  insertUrl: Url + "/InsertAcu?a=2&var="+vari,
    }), 

      columns: [
            {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "fechaen",
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto",
            Mask: "dd/MM/yyyy",  
            displayFormat: "dd/MM/yyyy",
            allowEditing: false,
            /*
            timeZone: "America/Caracas",
            validationRules: [{
            type: "required",
            message: "La fecha de entrega es requerida"
           
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }*/
        },
         {
            dataField: "numtermx", 
            caption:  "N° de Terminal",
            width: "auto",
               validationRules: [{
            type: "required",
            message: "El N° de Terminal es Requerido"
            }, {
                type: "stringLength",
                min: 1,
                message: "Debe tener al menos 1 caracter"
            },{
                    type: "stringLength",
                    message: "El largo máximo es de 100 carácteres.",
                    max: 10
           }]

        },

        {
            dataField: "serialpo", 
            caption:  "N° Serial de POS",
            allowEditing: false,
            width: "auto",
            // lookup: {
            //       dataSource: $scope.serialpos,
            //       valueExpr: "serialpo",
            //       displayExpr: "serialpo"
            //     },
              validationRules: [{
            type: "required",
            message: "El Serial de POS es Requerido"
            }, {
                type: "stringLength",
                min: 5,
                message: "Debe tener al menos 5 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo máximo es de 15 carácteres.",
                    max: 30
           }]
        },
         {
            dataField: "numsimx", 
            caption:  "N° Sim Card",
            allowEditing: false,
            width: "auto",
            
        },
        {
            dataField: "serialmifi", 
            caption:  "Serial Mifi",
            allowEditing: false,
            width: "auto",
            
        },
        {
            dataField: "numamexx", 
            caption:  "N° AMEX",
            width: "auto"
        },
        {
            dataField: "estatus", 
            caption:  "Estatus de Servicio",
            width: "auto"
        },
       /* {
            dataField: "nroafiliaci", 
            caption:  "N° de Afiliacion",
            width: "auto"
        },*/
        // {
          // dataField: 'enlace',
         //caption:  "Acción",
           //width: "auto",
           //allowEditing: false, 

        //    cellTemplate: function (container, options) {
         //     $('<a> Instalación </a>')
          //      .attr('href', someUrl + options.value)
           //     .css('color','white') 
            //    .css('background','blue')
             //  .css('border-radius','0.2em')
             //   .css('padding','.3em')
             //   .appendTo(container);
            // }  
         //}    
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
          mode : "row",
           allowUpdating: edit,
           // allowDeleting: true,
           /* allowAdding: true,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },   
            }  */ 
        },
    };
  }); 
});

var AppInventarioPos = angular.module('AppInventarioPos', ['dx']);
AppInventarioPos.service('informacion', ['$http',
  function($http) {
    var vm=this;
    //vm.fdatos = { fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppInventarioPos.controller('InventarioPosController',['$scope','informacion',  function InventarioPosController($scope,informacion) {
  informacion.postData('api/selectInventarioPos')
    .then(function(datos) {
      $scope.datos = datos.data
       var someUrl="?cargar=reportedetallado&var=";
    
     $scope.gridOptions = {
      dataSource:$scope.datos,
     
      groupPanel: {
          visible: true
      },
      selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Inventario_Pos",
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        }, 
         {

          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalle </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        
         {
            dataField: "ifechacompra",
            caption:  "Fecha Compra",
            width: "auto"
         },
          {
            dataField: "cantidad",
            caption:  "Cantidad de POS",
            width: 240
        },

        {
            dataField: "porasignar",
            caption:  "Disponibles",
            width: "auto"
        },
        {
            dataField: "asignados",
            caption:  "Asignados",
            width: "auto"
        }       

      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            },
             {
                column: "porasignar",
                summaryType: "sum"
            },
             {
                column: "asignados",
                summaryType: "sum"
            }]
        },

      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppInventarioPosDetallado = angular.module('AppInventarioPosDetallado', ['dx']);
AppInventarioPosDetallado.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppInventarioPosDetallado.controller('InventarioPosDetalladoController',['$scope','informacion',  function InventarioPosDetalladoController($scope,informacion) {
  informacion.postData('api/selectInventarioDetallado')
    .then(function(datos) {
      var someUrl="?cargar=reportegeneral&var=";
      var x="";
      $scope.datos = datos.data
      fecha= document.getElementById('fecha').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
    
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {

          dataField: 'enlacedetalle',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },    
        // {
        //     dataField: "ifechacompra",
        //     caption:  "Fecha de Compra",
        //     width: "auto"
        // },
        {
            dataField: "marca", 
            caption:  "Marca de Pos",
            width: 200
        },
        {
            dataField: "tipo", 
            caption:  "Tipo de Pos",
            width: 200
        },
         {
            dataField: "total", 
            caption:  "Cantidad",
            width: "auto"
        },
         
        {
            dataField: "porasignar", 
            caption:  "Disponibles",
            width: "auto",
           calculateCellValue: function(data) {
   
                return [
                data.enlacedisponibles,
                data.porasignar
                    ]
                    .join("/");
                     },
            cellTemplate: function (container, options) {
               var x = options.value;
              l= x.split('/');
              
              $('<a>'+l[3] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);
             }


        },
         {

          dataField: 'asignados',
          caption:  "Asignados",
          width: "auto",
          calculateCellValue: function(data) {
   
                return [
                data.enlaceasignados,
                data.asignados
                    ]
                    .join("/");
                     },
            cellTemplate: function (container, options) {
               var x = options.value;
              l= x.split('/');
              
              $('<a>'+l[3] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);
             }
        } 
      ],
      summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppInventarioPosGeneral = angular.module('AppInventarioPosGeneral', ['dx']);
AppInventarioPosGeneral.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { fecha: document.getElementById('fecha').value,
                  marcapos: document.getElementById('marcapos').value,
                   operacion: document.getElementById('operacion').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppInventarioPosGeneral.controller('InventarioPosGeneralController',['$scope','informacion',  function InventarioPosGeneralController($scope,informacion) {
  informacion.postData('api/selectInventarioGeneral')
    .then(function(datos) {
      $scope.datos = datos.data
     // var fecha= document.getElementById('fecha').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
    
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
          {
            dataField: "proveedor", 
            caption:  "Proveedor",
            width: "auto"
        },
        {
            dataField: "seriall", 
            caption:  "Serial de POS",
            width: "auto"
        },
        {
            dataField: "marcaposs", 
            caption:  "Marca de Pos",
            width: "auto"
        },
        {
            dataField: "simserial", 
            caption:  "Nº Simcards",
            width: "auto"
        },
        {
            dataField: "mifiserial", 
            caption:  "Serial Mifi",
            width: "auto"
        },
         {
            dataField: "afiliacion", 
            caption:  "Nº de Afiliacion",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "Rif",
            width: "auto"
        },
          {
            dataField: "razon", 
            caption:  "Razón Social",
            width: "auto"
        },
        {
            dataField: "terminal", 
            caption:  "Nº de Terminal",
            width: "auto"
        },
        {
            dataField: "bancoasig", 
            caption:  "Banco Asignado",
            width: "auto"
        },
    
           
      ],
      summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppCargaIndividual = angular.module('AppCargaIndividual', ['dx']);

AppCargaIndividual.controller('CargaIndividualController', function CargaIndividualController($scope,$http) {
    
     $http.get('api/selectmarcapos.php').then(function(response){
      $scope.marcapos = response.data;

      $http.get('api/selecttipopos.php').then(function(response){
      $scope.tipopos = response.data;

      $http.get('api/selecttipobanco.php').then(function(response){
      $scope.banco = response.data;

    var someUrl="?cargar=recepcionmantenimiento&var=";
    var url = "api/CargaIndividual.php";
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "nterminal",
            loadUrl: url + "/Ejecutivos?a=1",
            insertUrl: url + "/InsertEj?a=2",
            updateUrl: url + "/UpdateEj?a=3"
        }), 
    
    searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },  /*"export": {
          enabled: true,
          fileName: "Tecnicos",
          allowExportSelectedData: false
      },*/
      columns: [
        {
            dataField: "correlativo",
            caption: "Nro.",
            allowEditing: false, 
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Agregar Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        
        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliacion",
            width: "auto",
           validationRules: [{
            type: "required",
            message: "El N° de Afiliacion es Requerido"
            }, {
                type: "pattern",
                pattern: /^\d*$/,
                message: "No puede usar letras."
            }]
        },
        {
            dataField: "irazonsocial", 
            caption:  "Razon Social",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El nombre es Requerido"
            }]
        },
        {
            dataField: "cdocumento", 
            caption:  "N° de Documento",
            width: "auto",
                 validationRules: [{
            type: "required",
            message: "Debe ingresar Rif o N° de Documento"
            }]

        },
        {
            dataField: "nterminal", 
            caption:  "N° de Terminal",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El N° de Terminal es Requerido"
            }, {
                type: "pattern",
                pattern: /^\d*$/,
                message: "No puede usar letras."
            }]
        },
        {
            dataField: "serial", 
            caption:  "Serial del Pos",
            width: "auto",
                validationRules: [{
            type: "required",
            message: "Debe ingresar el Serial del POS"
            }]
        },
          {
          dataField: "codtipo", 
          caption:  "Tipo de Pos",
          width: 200,
          lookup: {
                  dataSource: $scope.tipopos,
                  valueExpr: "codtipo",
                  displayExpr: "tipopos"
                },
          validationRules: [{
          type: "required",
          message: "El Tipo de POS es Requerido"
          }]
        },
         {
          dataField: "imarcapos", 
          caption:  "Marca de Pos",
          width: 200,
          lookup: {
                  dataSource: $scope.marcapos,
                  valueExpr: "imarcapos",
                  displayExpr: "marcapos"
                },
          validationRules: [{
          type: "required",
          message: "La marca del Pos es Requerida"
          }]
        },
          {
          dataField: "bancoorigen", 
          caption:  "Banco",
          width: 200,
          lookup: {
                  dataSource: $scope.banco,
                  valueExpr: "bancoorigen",
                  displayExpr: "ibp"
                },
          validationRules: [{
          type: "required",
          message: "Debe seleccionar un banco"
          }]
        }

      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "cell",
            allowAdding: true,
            allowUpdating: true
        }
    };
      });
      });
      });
 });


//Servicio tecnico POR RIF O RAZON SOCIAL

var AppServicioTecnicoRif = angular.module('AppServicioTecnicoRif', ['dx']);
AppServicioTecnicoRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTecnicoRif.controller('ServicioTecnicoRifController',['$scope','informacion',  function ServicioTecnicoRifController($scope,informacion) {
  informacion.postData('api/SelectServicioTecnicoRif')
    .then(function(datos) {
      var someUrl="?cargar=datacliente&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
           {

          dataField: 'enlaceafiliado',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliado",
            width: "auto"
        },
        {
            dataField: "factura", 
            caption:  "Nº de Factura",
            width: "auto"
        },
         {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },
        {
            dataField: "serial", 
            caption:  "Serial Pos",
            width: "auto"
        },  
        {
            dataField: "terminal", 
            caption:  "Nº Terminal",
            width: "auto"
        },  
        {
            dataField: "sim", 
            caption:  "Nº Sim Card",
            width: "auto"
        },  
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        }      
           
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

//Data SERVICIO TECNICO Detallada RIF

var AppClienteSTRIF = angular.module('AppClienteSTRIF', ['dx']);
AppClienteSTRIF.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { rif: document.getElementById('rif').value, 
                  banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppClienteSTRIF.controller('ClienteSTRIFController',['$scope','informacion',  function ClienteSTRIFController($scope,informacion) {
  informacion.postData('api/SelectClienteRifST')
    .then(function(datos) {
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "ServicioTecnico_"+databanco+'_'+fechaactual,
          allowExportSelectedData: false
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
        {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliación",
            width: "auto"
        },
          {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
          {
            dataField: "coddocumento", 
            caption:  "RIF/CI",
            width: "auto"
        },
         {
            dataField: "rlegal", 
            caption:  "Persona Contacto",
            width: "auto"
        },
          {
            dataField: "telfrl", 
            caption:  "N° Telefónico ",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección de Instalación",
            width: "auto"
        },
          {
            dataField: "telf1", 
            caption:  "Teléfono 1",
            width: "auto"
        },
         {
            dataField: "telf2", 
            caption:  "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },
        {
            dataField: "marcapos", 
            caption:  "Marca de Pos",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
       
        {
            dataField: "tipolinea", 
            caption:  "Tipo de Linea",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


//Data Acuerdos de servicios banco y fecha

var AppAcuerdosServiciosBF = angular.module('AppAcuerdosServiciosBF', ['dx']);
AppAcuerdosServiciosBF.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppAcuerdosServiciosBF.service('datagrafico', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppAcuerdosServiciosBF.controller('AcuerdosServiciosBFController',['$scope','informacion',  function Acuerdos($scope,informacion) {
  informacion.postData('api/SelectAcuerdosServiciosBF')
    .then(function(datos) {
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      var someUrl="?cargar=buscarcliente&var=";
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "AcuerdoServicios_"+databanco+'_'+fechadetalle.value,
          allowExportSelectedData: false
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            width: "auto"
        },
        {
            dataField: "promedio",
            caption:  "Dias Instalación (Promedio)",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
          {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
          {
            dataField: "coddocumento", 
            caption:  "RIF/CI",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },
        {
            dataField: "tipolinea", 
            caption:  "Tipo de Linea",
            width: "auto"
        },
         {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliación",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre de Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección de Instalacion",
            width: "auto"
        },
         {

          dataField: 'enlaceafiliado',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }     
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
        paging: { pageSize: 5 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

//var types = ["line", "stackedline", "fullstackedline"];
var types = ["line"];


AppAcuerdosServiciosBF.controller('AcuerdosServiciosBFgraficoController',['$scope','datagrafico',  function AcuerdosServiciosBFgraficoController($scope,datagrafico) {
  datagrafico.postData('api/SelectAcuerdosServiciosBFgrafico')
    .then(function(datos) {
      $scope.datos1 = datos.data
      $scope.currentType = types[0];
      databanco= document.getElementById('databanco').value;
      $scope.chartOptions = {
        palette: "violet",
        dataSource: $scope.datos1,
        commonSeriesSettings: {
            argumentField: "registros"
        },
        bindingOptions: { 
            "commonSeriesSettings.type": "currentType"
        },
        margin: {
            bottom: 20
        },
        argumentAxis: {
            valueMarginsEnabled: false,
            discreteAxisDivisionMode: "crossLabels",
            grid: {
                visible: true
            }
        },
        series: [
            { valueField: "promedios",
              name: "Promedio de Dias",
              type: "bar",
              color: '#ffaa66'
            }
        ],
        legend: {
            verticalAlignment: "bottom",
            horizontalAlignment: "center",
            itemTextPosition: "bottom"
        },
        title: { 
            text: "Promedio de Dias de instalación de POS <br>"+" ("+databanco+") ",
            subtitle: {
                text: "Según su fecha de recepción: "+fechadetalle.value
            }
        },
        "export": {
            enabled: true,
            fileName: "AcuerdoServicios_"+databanco+'_'+fechadetalle.value,
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: arg.valueText
                };
            }
        }
    };
    
    $scope.typesOptions = {
        dataSource: types,
        bindingOptions: { 
            value: "currentType"
        }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


///AppAcuerdosServiciosRIF

var AppAcuerdosServiciosRIF = angular.module('AppAcuerdosServiciosRIF', ['dx']);
AppAcuerdosServiciosRIF.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { rif: document.getElementById('rif').value,
                  banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                   nombrearchivo: document.getElementById('nombrearchivo').value,
                  afiliado: document.getElementById('afiliado').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);


AppAcuerdosServiciosRIF.controller('AcuerdosServiciosRIFController',['$scope','informacion',  function Acuerdos($scope,informacion) {
  informacion.postData('api/SelectAcuerdosServiciosRIF')
    .then(function(datos) {
      $scope.datos = datos.data
      databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "AcuerdoServicio_"+databanco+'_'+rif.value+'_'+fechadetalle.value+'_'+afiliado.value,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            width: "auto"
        },
        {
            dataField: "fechainstalacion",
            caption:  "Fecha Instalación",
            width: "auto"
        },
        {
            dataField: "serialpos", 
            caption:  "Serial del POS",
            width: "auto"
        },
          {
            dataField: "promedio", 
            caption:  "Resultados de Gestión (Dias) ",
            width: "auto"
        },
          {
            dataField: "tipolinea", 
            caption:  "Tipo de Linea",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        }     
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
        paging: { pageSize: 5 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 50]
        },
      showBorders: true,
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

//var types = ["line", "stackedline", "fullstackedline"];
var types = ["line"];


AppAcuerdosServiciosRIF.controller('AcuerdosServiciosRIFgraficoController',['$scope','informacion',  function AcuerdosServiciosBFgraficoController($scope,informacion) {
  informacion.postData('api/SelectAcuerdosServiciosRIF')
    .then(function(datos) {
      $scope.datos1 = datos.data
      $scope.currentType = types[0];
      databanco= document.getElementById('databanco').value;
      $scope.chartOptions = {
        palette: "violet",
        dataSource: $scope.datos1,
        commonSeriesSettings: {
            argumentField: "serialpos"
        },
        bindingOptions: { 
            "commonSeriesSettings.type": "currentType"
        },
        margin: {
            bottom: 20
        },
        argumentAxis: {
            valueMarginsEnabled: false,
            discreteAxisDivisionMode: "crossLabels",
            grid: {
                visible: true
            }
        },
        series: [
            { valueField: "promedio",
              name: "Promedio de Dias",
              type: "bar",
              color: '#ffaa66'
            }
        ],
        legend: {
            verticalAlignment: "bottom",
            horizontalAlignment: "center",
            itemTextPosition: "bottom"
        },
        title: { 
            text: "Resultado de la Gestión  (Dias)",
            subtitle: {
                text: "(Por Seriales POS / Terminales) "
            }
        },
        "export": {
            enabled: true,
            fileName: "AcuerdoServicio_"+databanco+'_'+rif.value+'_'+fechadetalle.value+'_'+afiliado.value,
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: arg.valueText
                };
            }
        }
    };
    
    $scope.typesOptions = {
        dataSource: types,
        bindingOptions: { 
            value: "currentType"
        }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


// Acuerdos de Servicio  POR RIF O RAZON SOCIAL

var AppAcuerdoServicioRif = angular.module('AppAcuerdoServicioRif', ['dx']);
AppAcuerdoServicioRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppAcuerdoServicioRif.controller('AcuerdoServicioRifController',['$scope','informacion', 
 function AcuerdoServicioRifController($scope,informacion) {
  informacion.postData('api/SelectAcuerdoServicioRif')
    .then(function(datos) {
      var someUrl="?cargar=buscarcliente&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "idate", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantidadterminales", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            width: "auto"
        },
         {
            dataField: "coddocumento", 
            caption:  "Rif / CI",
            width: "auto"
        },
         {
            dataField: "nafiliacion", 
            caption:  "Nº Afiliado",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre Archivo",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
        {

          dataField: 'enlaceafiliado',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }     
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
        paging: { pageSize: 5 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 50]
        },
      showBorders: true,
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


// reporte general de mifi 


var Appreportegeneraldemifi = angular.module('Appreportegeneraldemifi', ['dx']);
Appreportegeneraldemifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreportegeneraldemifi.controller('Controllerreportegeneraldemifi',['$scope','informacion',  function Controllerreportegeneraldemifi($scope,informacion) {
  informacion.postData('api/selectReporteGeneraldeMifi')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte General de Mifi",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "nbanco",
            caption: "Banco",
            width: "auto"
        },

        {
            dataField: "estatuscontac",
            caption: "Estatus Contacto",
            width: "auto"
        },
        {
            dataField: "estatuscliente",
            caption: "Estatus Cliente",
            width: "auto"
        },

        {
            dataField: "descequipo",
            caption: "Estatus del Equipo",
            width: "auto"
        },
         {
            dataField: "fecharecep",
            caption: "Fecha Recepcion",
            width: "auto"
        },
               
         {
            dataField: "ejec",
            caption: "Ejecutivo",
            width: "auto"
        },     

        {
            dataField: "fechainstalacion",
            caption: "Fecha Instalacion",
            width: "auto"
        },
        
         {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "tlf3",
            caption: "Teléfono 3",
            width: "auto"
        },
         {
            dataField: "tlf4",
            caption: "Teléfono 4",
            width: "auto"
        },
        {
            dataField: "cantidadpos",
            caption: "Cantidad Terminales",
            width: "auto"
        },
        {
            dataField: "tipoposs",
            caption: "Tipo de Equipo",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Modelo de Equipo",
            width: "auto"
        },
         {
            dataField: "factura",
            caption: "Nº Factura",
            width: "auto"
        },
         {
            dataField: "serialmifi",
            caption: "Serial MIFI",
            width: "auto"
        },
         {
            dataField: "serialsim",
            caption: "Serial SimCard",
            width: "auto"
        },
        
         {
            dataField: "fechacarga",
            caption: "Fecha Carga",
            width: "auto"
        }, 
         {
            dataField: "direccions",
            caption: "Direccion Fiscal.",
            width: "auto"
        }
         
          
         
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);




// app reporte de mifi general

var Appreporteestatusgestiondemifi = angular.module('Appreporteestatusgestiondemifi', ['dx']);
Appreporteestatusgestiondemifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value, 
                 sitio: document.getElementById('sitio').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatusgestiondemifi.controller('Controllerreporteestatusgestiondemifi',['$scope','informacion',  function Controllerreporteestatusgestiondemifi($scope,informacion) {
  informacion.postData('api/selectReporteGestionMifi')
    .then(function(datos) {
    //  var tipousuario= document.getElementById('tipousuario').value;
     // var ejecutivo= document.getElementById('ejecutivo').value;
     // var usuario= document.getElementById('usuario').value;
      var banco= document.getElementById('banco').value;
      var sitio= document.getElementById('sitio').value;
      var someUrl="serviciotecnicomifi?cargar=procesarconfigmifi&var=";
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus",
          allowExportSelectedData: false
      },
      groupPanel: {
          visible: true
      },

       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },  
         {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
          {
            dataField: "estatus",
            caption: "Estatus Registro",
            width: "auto"
        },
        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        
      
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },
         {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono 1",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "tipopos",
            caption: "Tipo de Equipo",
            width: "auto"
        },
         {
            dataField: "marcapos",
            caption: "Modelo de Equipo",
            width: "auto"
        },
         {
            dataField: "fechac",
            caption: "Fecha de Carga al Sistema",
            width: "auto"
        },
        {
            dataField: "serialdemifi",
            caption: "Serial MIFI",
            width: "auto"
        },
        {
            dataField: "serialsim",
            caption: "Nº de SIM",
            width: "auto"
        },
          {
            dataField: "fechapos",
            caption: "Fecha de Carga de Serial",
            width: "auto"
        },
        
        {
            dataField: "fechainst",
            caption: "Fecha de Instalación",
            width: "auto"
        },{
            dataField: "direccionenvio",
            caption: "Direccion Instalación",
            width: "auto"
        },{
            dataField: "inftecnico",
            caption: "Tecnico",
            width: "auto"
        },
        {
            dataField: "observaciones",
            caption: "Observaciones",
            width: "auto"
        }
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


//serialpos
var AppServicioTecnicoGestionMifi = angular.module('AppServicioTecnicoGestionMifi', ['dx']);
  
AppServicioTecnicoGestionMifi.controller('ServicioTecnicoGestionMifiController', function ServicioTecnicoGestionMifiController($scope,$http) {
     var now = new Date();

    var  cestatus= document.getElementById('codestatus').value;
    $http.get('api/selectestatusinstmifi.php?cestatus='+cestatus).then(function(response){
    $scope.estatuspos = response.data;

    $http.get('api/selecttecnicos.php').then(function(response){
    $scope.tecnicos = response.data;

    var Url = "api/selectServicioTecnicoDetalladoMifi.php";
    var  vari= document.getElementById('var').value;
    var  serialmifi= document.getElementById('serialmifi').value;
    var  usuario= document.getElementById('tipousuario').value;
    var  area= document.getElementById('area').value;
    var  rowinstalado= document.getElementById('rowinstalado').value;
    var  codestatus= document.getElementById('codestatus').value;
    // var  idsecuencial= document.getElementById('idsecuencial').value;
    //alert(area);
    if ((usuario=='T' && area=='9') || (usuario=='A' && area=='1')) {
      var insert=true;
      var edit=true;
    }
     else {
      var insert=false;
      var edit=false;
    }
   
      var applyFilterTypes = [{
            key: "auto",
            name: "Immediately"
        }, {
            key: "onClick",
            name: "On Button Click"
        }],
        dataGrid;
    
    function getOrderDay(rowData) {
        return (new Date(rowData.OrderDate)).getDay();
    }

    function redirectrefrecar(){
    setTimeout(function(){ window.location.reload(0); }, 2000); 
  };

    
    $scope.filterRow = {
        visible: true,
        applyFilter: "auto"
    };
    
    $scope.headerFilter = {
        visible: true
    };
    if(rowinstalado==0){
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari,
         }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serialmifi+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            displayFormat: "dd/MM/yyyy",
            visible:false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del Mifi",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/mm/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "tecnico", 
            caption:  "Nombre del Técnico",
            width: "auto",
           /*  validationRules: [{
           // type: "required",
            message: "Debe Ingresar el Técnico"
            }],*/
             lookup: {
                    dataSource: $scope.tecnicos,
                    valueExpr: "tecnico",
                    displayExpr: "ntecnico"
                  }
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            value: now,
            message: "La fecha de la Gestión es requerida"
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "estatus", 
            caption:  "Status Instalación de Mifi",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },

         {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del Mifi",
            dataType: "date",
            width: "auto",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "popup",
           // allowUpdating: edit,
            allowAdding: insert,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }

        },
    };
   }//end if
   else{
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari
        }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serialmifi+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "ntecnico", 
            caption:  "Nombre del Técnico",
            width: "auto"
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },
        {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto"
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            allowEditing: true,
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true
    };
   }//end else 
    });// fin estatus pos 
    });// fin del tecnicos 
});

var AppClienteFacturacion = angular.module('AppClienteFacturacion', ['dx']);
AppClienteFacturacion.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { cliente: document.getElementById('cliente').value,
                  fecha: document.getElementById('fecha').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppClienteFacturacion.controller('ClientesFacturacionController',['$scope','informacion',  function ClientesFacturacionController($scope,informacion) {
  informacion.postData('api/ClientesFacturacion')
    .then(function(datos) {
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "Clientes_Facturar"+cliente.value,
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "tipocliente",
            caption:  "TipoCliente",
            width: "auto"
        },
        {
            dataField: "razon",
            caption:  "Customer Name",
            width: "auto"
        },
        {
            dataField: "razon", 
            caption:  "Short Name",
            width: "auto"
        },
        {
            dataField: "razon", 
            caption:  "Statement Name",
            width: "auto"
        },
        {
            dataField: "destipo", 
            caption:  "Customer Class",
            width: "auto"
        },
        {
            dataField: "facturacion", 
            caption:  "Address Code",
            width: "auto"
        },
        {
            dataField: "representantelegal", 
            caption:  "Contact Person",
            width: "auto"
        },
        {
            dataField: "address1", 
            caption:  "Address 1",
            width: "auto"
        },
        {
            dataField: "address2", 
            caption:  "Address 2",
            width: "auto"
        },
        {
            dataField: "municipio", 
            caption:  "City",
            width: "auto"
        },
        {
            dataField: "estado", 
            caption:  "State",
            width: "auto"
        },
        {
            dataField: "codepostal", 
            caption:  "Zip",
            width: "auto"
        },
        {
            dataField: "countrycode", 
            caption:  "Country Code",
            width: "auto"
        },
        {
            dataField: "tlf1", 
            caption:  "Phone 1",
            width: "auto"
        },
        {
            dataField: "tlf2", 
            caption:  "Phone 2",
            width: "auto"
        },
         {
            dataField: "rif", 
            caption:  "User Defined 1",
            width: "auto"
        },
         {
            dataField: "rif", 
            caption:  "Tax Registration Number",
            width: "auto"
        }

      ],
       summary: {
            totalItems: [
            {
                column: "tipocliente",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppPagos = angular.module('AppPagos', ['dx']);

AppPagos.controller('pagosController', function pagosController($scope,$http) {
    var now = new Date();

   $http.get('api/selectbanco.php').then(function(response){
   $scope.banco = response.data;
    var permisos=document.getElementById('permisos').value;
    var id_afiliado=document.getElementById('idregistro').value;
    var countpagos=document.getElementById('countpagos').value;
    var estatusconfirmacion = document.getElementById('estatusconfirmacion').value;
    var codarea = document.getElementById('estatusconfirmacion').value;
    var codtipousuario = document.getElementById('codtipousuario').value;
    var someUrl ="?cargar=confirmarPago&var=";
    var url = "api/crudPagos.php";
    var _gtienepago=0;

    var d = new Date();
    var horaactual=d.getHours()+':'+d.getMinutes();
    var edit_obser=false;
 
    if (estatusconfirmacion==1 && (codarea!=1 || codarea!=2)) {
        var edit=false;
        var insert=false;
        var elim=false;
    }
    if (permisos!='W') {
       var edit=false;
       var insert=false;
       var elim=false;
    }
    else{
        var edit=true;
        var insert=true;
        if (estatusconfirmacion!=1)
          var elim=true;

         if ((estatusconfirmacion==1 && (codtipousuario=="A")) || (estatusconfirmacion==0 && (codtipousuario=="A")) )
         var elim=true;
         else
         elim=false;

      }

    if (countpagos==0) {
     var edit=false;
     var elim=false;
     var cont=0;
      }

      if (codtipousuario=="A"){
        var aprobar=true;
        var edit_obser=true;
        } 
      else
        aprobar=false;
  /* if (estatusconfirmacion==1) {
      var insert=false;
       } */  
    $scope.gridOptions = {

        dataSource: DevExpress.data.AspNet.createStore({
            key: "referencia",
            loadUrl: url + "/viewPagos?a=1&var="+id_afiliado,
            insertUrl: url + "/InsertPago?a=2&id_registro="+id_afiliado,
            updateUrl: url + "/UpdatePago?a=3&id_registro="+id_afiliado,
            deleteUrl: url + "/DeleteEj?a=4&id_registro="+id_afiliado,
        }), 
    
    searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."

        },  /*"export": {
          enabled: true,
          fileName: "Tecnicos",
          allowExportSelectedData: false
      },*/

      columns: [
       
        {
            dataField: "fechapago",
            caption:  "Fecha pago",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La fecha del pago es requerida"
            }],      
        },
        {
            dataField: "horapago",
            caption:  "Hora pago",
            dataType: "time",
            // format:"shortTime",
            width: "auto",
            value:horaactual,
             validationRules: [{
               type: "pattern",
               pattern:/((1[0-2]|0?[1-9]):([0-5][0-9])?([AaPp][Mm]))/,
               message: "Verificar formato de hora ejemplo: 10:30AM."
           }] 
        },      
        {
            dataField: "bancoorigen",
            caption:  "Banco origen",
            width: "auto",
            lookup: {
                    dataSource: $scope.banco,
                    valueExpr: "banco",
                    displayExpr: "ibp"
                  },
          
        },
        {
            dataField: "bancodestino", 
            caption:  "Banco destino",
            width: "auto",
            lookup: {
                    dataSource: $scope.banco,
                    valueExpr: "banco",
                    displayExpr: "ibp"
                  },
  
        },
        {
            dataField: "formapago", 
            caption:  "Forma pago",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La forma de pago es requerida"
            }],
            lookup: {
                    dataSource: [ {
                        "codformapago": "1",
                        "nameformapago": "TDC"
                    },{
                        "codformapago": "2",
                        "nameformapago": "Transferencia"
                    },{
                        "codformapago": "3",
                        "nameformapago": "Efectivo"
                    },{
                        "codformapago": "4",
                        "nameformapago": "Depósito por Taquilla"
                    }],
                    valueExpr: "codformapago",
                    displayExpr: "nameformapago"
                  }

        },
        {
            dataField: "tipomoneda", 
            caption:  "Moneda",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El tipo de moneda es requerida"
            }],
            lookup: {
                    dataSource: [ {
                        "codmoneda": "1",
                        "namemoneda": "Bs.S"
                    },{
                        "codmoneda": "2",
                        "namemoneda": "USD"
                    }],
                    valueExpr: "codmoneda",
                    displayExpr: "namemoneda"
                  }
        },
        {
            dataField: "montousd",  
            caption:  "Monto USD",
            width: "auto",
            format: "currency",
            validationRules: [
            {
                type: "pattern",
                pattern: /^(\d{1}\.)?(\d+\.?)+(,\d{2})?$/,
                message: "No puede usar letras."
            }
            ],
        },
        {
            dataField: "factorconversion",  
            caption:  "Factor Conversión",
            width: "auto",
            format: "currency",
            validationRules: [
            {
                type: "pattern",
                pattern: /^(\d{1}\.)?(\d+\.?)+(,\d{2})?$/,
                message: "No puede usar letras."
            }
            ],
          
        },
        {
            dataField: "monto",  
            caption:  "Monto",
            width: "auto",
            /*format: "currency",*/
            validationRules: [
            {
                type: "pattern",
                pattern: /^(\d{1}\.)?(\d+\.?)+(,\d{2})?$/,
                message: "No puede usar letras."
            }
            ],
          
        },
        {
            dataField: "referencia", 
            caption:  "Referencia",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La referencia es requerida",
            type: "pattern",
            pattern: /^[a-zA-Z0-9\+\-\+\.\\+\:\ ]*$/,
           // /[0-9\+\-\ ]/
            message: "No puede usar /."
            }],
            
            
        },
        {
            dataField: "nombredepositante", 
            caption:  "Depositante",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El nombre es del depositante es requerido"
            }, {
                type: "stringLength",
                min: 2,
                message: "El nombre debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo máximo es de 200 carácteres.",
                    max: 200
           }]
            
        },
        {
            dataField: "imagenpago", 
            caption:  "Capture",
            width: "auto",
            visible: false,
            allowEditing: false 
            
        },
        {
            dataField: "observacioncomer", 
            caption:  "Observación Comercial",
            width: "auto"
            
        },
        {
            dataField: "observacionadm", 
            caption:  "Observación Administración",
            value:"",
                width: "auto",
            allowEditing: edit_obser
            
        },
        {
            dataField: "estatus", 
            caption:  "Estatus",
            width: "auto",
            lookup: {
                   dataSource: [ {
                       "codestatus": "0",
                       "descestatus": "Por aprobar"
                   },{
                       "codestatus": "1",
                       "descestatus": "Aprobado"
                   }],
                   valueExpr: "codestatus",
                   displayExpr: "descestatus"
                 },
                   editorOptions: {
                disabled: true
                
            }
            
        },

        {
            dataField: "fechacarga", 
            caption:  "fecha carga",
            width: "auto",
            allowEditing : false,
            
        },

        {
            dataField: "idafiliado",
            caption: "Registro",
            visible: false,
            width: "auto",
            editorOptions: {
                disabled: true
                
            }
        },

        {
            dataField: "enlace", 
            caption:  "Confirmación",
            width: "auto",
            visible: aprobar,
            allowEditing: false,
              editorOptions: {
              disabled: true
                
            },
               calculateCellValue: function(data) {
   
                return [
                data.enlace
                    ]
                    .join("/");
                     },
              cellTemplate: function (container, options) {
              var x = options.value;
               l= x.split('/');
                if(l[2] =="1"){
               
                      $('<a>'+ 'Aprobado' +'</a>')
                      .attr('href', someUrl + options.value)
                      .appendTo(container);
                 
                 } else if (x=="") {
                    //if x is null then this register haven't a saved  payment 
    
                  //console.log(_gtienepago); 
                      $('<b>'+ 'registre pago' +'</b>')
                       .appendTo(container);
                    
                    }  else{
                        $('<a>'+ 'Por aprobar' +'</a>')
                       .attr('href', someUrl + options.value)
                      .appendTo(container);

                    }

               /* $('<a>'+l[1] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);*/

                 }
           
       }
         
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
            mode: "popup",
            allowAdding: insert,
            allowUpdating: edit,
            allowDeleting: elim,
             popup: {
                title: "Agregar datos de Pago",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }
        },
  summary: {
            totalItems: [
                 {
                column: "monto",
                summaryType: "sum",
                valueFormat: ""
            }]
        }

    };
   });//API select bank´s 
});



var AppPagosMifi = angular.module('AppPagosMifi', ['dx']);

AppPagosMifi.controller('MifipagosController', function MifipagosController($scope,$http) {
    var now = new Date();

   $http.get('api/selectbanco.php').then(function(response){
   $scope.banco = response.data;
    var permisos=document.getElementById('permisos').value;
    var id_afiliado=document.getElementById('idregistro').value;
    var countpagos=document.getElementById('countpagos').value;
    var estatusconfirmacion = document.getElementById('estatusconfirmacion').value;
    var codarea = document.getElementById('estatusconfirmacion').value;
    var codtipousuario = document.getElementById('codtipousuario').value;
    var someUrl ="?cargar=confirmarpagosmifi&var=";
    var url = "api/FacturacionMifi.php";
    var _gtienepago=0;

    var d = new Date();
    var horaactual=d.getHours()+':'+d.getMinutes();
    var edit_obser=false;
 
    if (estatusconfirmacion==1 && (codarea!=1 || codarea!=2)) {
        var edit=false;
        var insert=false;
        var elim=false;
    }
    if (permisos!='W') {
       var edit=false;
       var insert=false;
       var elim=false;
    }
    else{
        var edit=true;
        var insert=true;
        if (estatusconfirmacion!=1)
          var elim=true;

         if ((estatusconfirmacion==1 && (codtipousuario=="A")) || (estatusconfirmacion==0 && (codtipousuario=="A")) )
         var elim=true;
         else
         elim=false;

      }

    if (countpagos==0) {
     var edit=false;
     var elim=false;
     var cont=0;
      }

      if (codtipousuario=="A"){
        var aprobar=true;
        var edit_obser=true;
        } 
      else
        aprobar=false;
  /* if (estatusconfirmacion==1) {
      var insert=false;
       } */  
    $scope.gridOptions = {

        dataSource: DevExpress.data.AspNet.createStore({
            key: "referencia",
            loadUrl: url + "/viewPagos?a=1&var="+id_afiliado,
            insertUrl: url + "/InsertPago?a=2&id_registro="+id_afiliado,
            updateUrl: url + "/UpdatePago?a=3&id_registro="+id_afiliado,
            deleteUrl: url + "/DeleteEj?a=4&id_registro="+id_afiliado,
        }), 
    
    searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."

        },  /*"export": {
          enabled: true,
          fileName: "Tecnicos",
          allowExportSelectedData: false
      },*/

      columns: [
       
        {
            dataField: "fechapago",
            caption:  "Fecha pago",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La fecha del pago es requerida"
            }],      
        },
        {
            dataField: "horapago",
            caption:  "Hora pago",
            dataType: "time",
            // format:"shortTime",
            width: "auto",
            value:horaactual,
             validationRules: [{
               type: "pattern",
               pattern:/((1[0-2]|0?[1-9]):([0-5][0-9])?([AaPp][Mm]))/,
               message: "Verificar formato de hora ejemplo: 10:30AM."
           }] 
        },      
        {
            dataField: "bancoorigen",
            caption:  "Banco origen",
            width: "auto",
            lookup: {
                    dataSource: $scope.banco,
                    valueExpr: "banco",
                    displayExpr: "ibp"
                  },
          
        },
        {
            dataField: "bancodestino", 
            caption:  "Banco destino",
            width: "auto",
            lookup: {
                    dataSource: $scope.banco,
                    valueExpr: "banco",
                    displayExpr: "ibp"
                  },
  
        },
        {
            dataField: "formapago", 
            caption:  "Forma pago",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La forma de pago es requerida"
            }],
            lookup: {
                    dataSource: [ {
                        "codformapago": "1",
                        "nameformapago": "TDC"
                    },{
                        "codformapago": "2",
                        "nameformapago": "Transferencia"
                    },{
                        "codformapago": "3",
                        "nameformapago": "Efectivo"
                    },{
                        "codformapago": "4",
                        "nameformapago": "Depósito por Taquilla"
                    }],
                    valueExpr: "codformapago",
                    displayExpr: "nameformapago"
                  }

        },
        {
            dataField: "tipomoneda", 
            caption:  "Moneda",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El tipo de moneda es requerida"
            }],
            lookup: {
                    dataSource: [ {
                        "codmoneda": "1",
                        "namemoneda": "Bs.S"
                    },{
                        "codmoneda": "2",
                        "namemoneda": "USD"
                    }],
                    valueExpr: "codmoneda",
                    displayExpr: "namemoneda"
                  }
        },
        {
            dataField: "montousd",  
            caption:  "Monto USD",
            width: "auto",
            format: "currency",
            validationRules: [
            {
                type: "pattern",
                pattern: /^(\d{1}\.)?(\d+\.?)+(,\d{2})?$/,
                message: "No puede usar letras."
            }
            ],
        },
        {
            dataField: "factorconversion",  
            caption:  "Factor Conversión",
            width: "auto",
            format: "currency",
            validationRules: [
            {
                type: "pattern",
                pattern: /^(\d{1}\.)?(\d+\.?)+(,\d{2})?$/,
                message: "No puede usar letras."
            }
            ],
          
        },
        {
            dataField: "monto",  
            caption:  "Monto",
            width: "auto",
            /*format: "currency",*/
            validationRules: [
            {
                type: "pattern",
                pattern: /^(\d{1}\.)?(\d+\.?)+(,\d{2})?$/,
                message: "No puede usar letras."
            }
            ],
          
        },
        {
            dataField: "referencia", 
            caption:  "Referencia",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La referencia es requerida"
            }],
            
            
        },
        {
            dataField: "nombredepositante", 
            caption:  "Depositante",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "El nombre es del depositante es requerido"
            }, {
                type: "stringLength",
                min: 2,
                message: "El nombre debe tener al menos 2 carácteres"
            },{
                    type: "stringLength",
                    message: "El largo máximo es de 200 carácteres.",
                    max: 200
           }]
            
        },
        {
            dataField: "imagenpago", 
            caption:  "Capture",
            width: "auto",
            visible: false,
            allowEditing: false 
            
        },
        {
            dataField: "observacioncomer", 
            caption:  "Observación Comercial",
            width: "auto"
            
        },
        {
            dataField: "observacionadm", 
            caption:  "Observación Administración",
            value:"",
                width: "auto",
            allowEditing: edit_obser
            
        },
        {
            dataField: "estatus", 
            caption:  "Estatus",
            width: "auto",
            lookup: {
                   dataSource: [ {
                       "codestatus": "0",
                       "descestatus": "Por aprobar"
                   },{
                       "codestatus": "1",
                       "descestatus": "Aprobado"
                   }],
                   valueExpr: "codestatus",
                   displayExpr: "descestatus"
                 },
                   editorOptions: {
                disabled: true
                
            }
            
        },

        {
            dataField: "fechacarga", 
            caption:  "fecha carga",
            width: "auto",
            allowEditing : false,
            
        },

        {
            dataField: "idafiliado",
            caption: "Registro",
            visible: false,
            width: "auto",
            editorOptions: {
                disabled: true
                
            }
        },

        {
            dataField: "enlace", 
            caption:  "Confirmación",
            width: "auto",
            visible: aprobar,
            allowEditing: false,
              editorOptions: {
              disabled: true
                
            },
               calculateCellValue: function(data) {
   
                return [
                data.enlace
                    ]
                    .join("/");
                     },
              cellTemplate: function (container, options) {
              var x = options.value;
               l= x.split('/');
                if(l[2] =="1"){
               
                      $('<a>'+ 'Aprobado' +'</a>')
                      .attr('href', someUrl + options.value)
                      .appendTo(container);
                 
                 } else if (x=="") {
                    //if x is null then this register haven't a saved  payment 
    
                  //console.log(_gtienepago); 
                      $('<b>'+ 'registre pago' +'</b>')
                       .appendTo(container);
                    
                    }  else{
                        $('<a>'+ 'Por aprobar' +'</a>')
                       .attr('href', someUrl + options.value)
                      .appendTo(container);

                    }

               /* $('<a>'+l[1] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);*/

                 }
           
       }
         
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
            mode: "popup",
            allowAdding: insert,
            allowUpdating: edit,
            allowDeleting: elim,
             popup: {
                title: "Agregar datos de Pago",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }
        },
  summary: {
            totalItems: [
                 {
                column: "monto",
                summaryType: "sum",
                valueFormat: ""
            }]
        }

    };
   });//API select bank´s 
});


    //Comercializacion Reporte estatus gestion 
    var appResultadoGestion = angular.module('appResultadoGestion', ['dx']);
    appResultadoGestion.service('informacion', ['$http',
      function($http) {
        var vm=this;
        vm.fdatos = { operacion: 1 , idregistro: document.getElementById('id_registro').value,namearchivo: document.getElementById('namearch').value };
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
appResultadoGestion.controller('ComercializacionResultadoGestionController',['$scope','informacion',  function ComercializacionResultadoGestionController($scope,informacion) {
  informacion.postData('api/selectComercializacionResultadoGestion')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ComercializacionEstatusGestion",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: 50
        },
        {
            dataField: "g_coddocumento",
            caption:  "N° documento",
            width: "auto"
        },

         {
            dataField: "descestatus",
            caption:  "Estatus",
            width: "auto"
        },
        {
            dataField: "ejecutivobanco", 
            caption:  "Eje. Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "ejecutivointeligensa", 
            caption:  "Eje. Inteligensa",
            width: "auto"
        },
         {
            dataField: "usuariogest", 
            caption:  "Eje. Gestión",
            width: "auto"
        },
        {
            dataField: "g_observacion", 
            caption:  "Observaciones",
            width: "auto"
        },
         {
            dataField: "fechagestion",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto"
        },
        
      ],
      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },
    function (error){
      console.log(error, 'El afiliado no tiene gestiones guardadas.');
  }); 
}]);

/*REPORTE DIARIO POR BANCO */
var AppReporteDiarioXBanco= angular.module('AppReporteDiarioXBanco', ['dx']);
AppReporteDiarioXBanco.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { clientes: document.getElementById('clientes').value
                 /* estatus: document.getElementById('estatus').value,
                   documento: document.getElementById('documento').value*/};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteDiarioXBanco.controller('ReporteDiarioController',['$scope','informacion',  function ReporteDiarioController($scope,informacion) {
  informacion.postData('api/selectReporteDiarioBanco')
    .then(function(datos) {
      $scope.datos = datos.data
      //databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
         
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "N°",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Nombre Banco",
            width: "auto"
        },
       /* {
            dataField: "ejecutivo", 
            caption:  "Nombre del Ejecutivo Inteligensa",
            width: "auto"
        },*/
        //  {
        //     dataField: "ejecutivobanc", 
        //     caption:  "Nombre del Ejecutivo Banco",
        //     width: "auto"
        // },
      
        {
            dataField: "carteratotal", 
            caption:  "Cartera De POS",
            width: "auto"
        },
        // {
        //     dataField: "marcapos", 
        //     caption:  "Marca de Pos",
        //     width: "auto"
        // },
        {
            dataField: "tipopos", 
            caption:  "Modelo",
            width: "auto"
        },
        {
            dataField: "estatusventa", 
            caption:  "Cantidad Ventas Con Fondos",
            width: "auto"
        },
        {
            dataField: "cantfacturados", 
            caption:  "Cantidad Facturados",
            width: "auto"
        },
      /*  {
            dataField: "montousdtransf", 
            caption:  "Monto USD Transferencia",
            valueFormat:"currency",
            precision:2,
            width: "auto" 
        },*/
          /*  {
            dataField: "montousdefect", 
            caption:  "Monto USD Efectivo",
            valueFormat:"currency",
            precision:2,
            width: "auto" 
        },*/
      /*  {
            dataField: "montobs", 
            caption:  "Monto Bs.S",
            width: "auto"
        }, 
            */     
      ],
      summary: {
            totalItems: [
            {
                column: "carteratotal",
                summaryType: "sum"

            },
            {
                column: "estatusventa",
                summaryType: "sum"

            },
            {
                column: "cantfacturados",
                summaryType: "sum"

            },  
            // {
            //     column: "montousd",
            //     summaryType: "sum",
            //     valueFormat: "currency",
            //      precision: 2
            // },
            // {
            //     column: "montobs",
            //     summaryType: "sum",
            //     /*valueFormat: "currency",*/
            //      precision: 2
            // },


            ],

        },

     paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');

      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE BANCO.xlsx');
        });
      });
      e.cancel = true;
    },
    };
     function customizeHeader(worksheet){

 

    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 11; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 8);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
     worksheet.getColumn(2).width = 35;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };
    worksheet.getRow(7).getCell(8).font =  {  bold: true };

    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    
    worksheet.getColumn(1).values = [  "","REPORTE DE VENTAS POR BANCO"];
    
  
  };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

var AppReporteXdias= angular.module('AppReporteXdias', ['dx']);
AppReporteXdias.service('informacion', ['$http',
  function($http) {
    var vm=this;
     var fechainicial= document.getElementById('fechainicial').value;
     var clientes= document.getElementById('clientes').value;
     var ejecutivo= document.getElementById('ejecutivo').value;
     var fechafinal= document.getElementById('fechafinal').value;
     var tiporeporte= document.getElementById('tiporeporte').value;
     var nombrebanco= document.getElementById('nombrebanco').value;


    vm.fdatos = { clientes: document.getElementById('clientes').value,
                  ejecutivo: document.getElementById('ejecutivo').value,
                   fechainicial: document.getElementById('fechainicial').value,
                    fechafinal: document.getElementById('fechafinal').value,
                      tiporeporte: document.getElementById('tiporeporte').value,
                        nombrebanco: document.getElementById('nombrebanco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }

    //NOMBRE AL EXPORTAR
   if ((clientes!='') && (ejecutivo!='') && (fechainicial!='')) {
    if (tiporeporte=='2') {
          if (clientes=='0') {
           nombredescarga='ReporteAdministracion_TodosLosBancos'+'_'+ejecutivo+'_'+fechainicial+'_Hasta_'+fechafinal;
         } 
           else if (ejecutivo=='0') {
           nombredescarga='ReporteAdministracion_'+nombrebanco+'_TodosLosEjecutivos_'+fechainicial+'_Hasta_'+fechafinal;
   
         }
          else if ((clientes=='0') && (ejecutivo=='0')) {
          nombredescarga='ReporteAdministracion_TodosLosBancos'+'_TodosLosEjecutivos_'+fechainicial+'_Hasta_'+fechafinal;
   
         }
         else {
         nombredescarga='ReporteAdministracion_'+nombrebanco+'_'+ejecutivo+'_'+fechainicial+'_Hasta_'+fechafinal;
         }}
    if (tiporeporte=='1') {
          if (clientes=='0') {
           nombredescarga='ReporteVentas_TodosLosBancos'+'_'+ejecutivo+'_'+fechainicial+'_Hasta_'+fechafinal;
         } 
           else if (ejecutivo=='0') {
           nombredescarga='ReporteVentas_'+nombrebanco+'_TodosLosEjecutivos_'+fechainicial+'_Hasta_'+fechafinal;
   
         }
          else if ((clientes=='0') && (ejecutivo=='0')) {
          nombredescarga='ReporteVentas_TodosLosBancos'+'_TodosLosEjecutivos_'+fechainicial+'_Hasta_'+fechafinal;
   
         }
         else {
         nombredescarga='ReporteVentas_'+nombrebanco+'_'+ejecutivo+'_'+fechainicial+'_Hasta_'+fechafinal;
         }}
  
     }
   else if ((clientes!='') && (ejecutivo=='') && (fechainicial!='')) {
      if (tiporeporte=='1') {
          if (clientes=='0') {
           nombredescarga='ReporteVentas_TodosLosBancos'+'_'+fechainicial+'_Hasta_'+fechafinal;
         } else {
           nombredescarga='ReporteVentas_'+nombrebanco+'_'+fechainicial+'_Hasta_'+fechafinal;
    }}
     if (tiporeporte=='2') {
          if (clientes=='0') {
           nombredescarga='ReporteAdministracion_TodosLosBancos'+'_'+fechainicial+'_Hasta_'+fechafinal;
         } else {
           nombredescarga='ReporteAdministracion_'+nombrebanco+'_'+fechainicial+'_Hasta_'+fechafinal;
    }}
   }

   else if ((clientes=='') && (ejecutivo!='') && (fechainicial!='')) {
      if (tiporeporte=='1') {
          if (ejecutivo=='0') {
           nombredescarga='ReporteVentas'+'_TodosLosEjecutivos_'+fechainicial+'_Hasta_'+fechafinal;
         } else {
           nombredescarga='ReporteVentas_'+ejecutivo+'_'+fechainicial+'_Hasta_'+fechafinal;
    }}
     if (tiporeporte=='2') {
          if (ejecutivo=='0') {
           nombredescarga='ReporteAdministracion'+'_TodosLosEjecutivos_'+fechainicial+'_Hasta_'+fechafinal;
         } else {
           nombredescarga='ReporteAdministracion_'+ejecutivo+'_'+fechainicial+'_Hasta_'+fechafinal;
    }}
   }

  else if ((clientes=='') && (ejecutivo!='') && (fechainicial=='')) {
      
          if (ejecutivo=='0') {
           nombredescarga='ReporteVentas_TodosLosEjecutivos';
         } else {
           nombredescarga='ReporteVentas_'+ejecutivo;
       }
   }
   else if ((clientes!='') && (ejecutivo!='') && (fechainicial=='')) {
           if (clientes=='0') {
           nombredescarga='ReporteVentas_TodosLosBancos'+'_'+ejecutivo;
         } 
           else if (ejecutivo=='0') {
           nombredescarga='ReporteVentas_'+nombrebanco+'_TodosLosEjecutivos';
   
         }
          else if ((clientes=='0') && (ejecutivo=='0')) {
          nombredescarga='ReporteVentas_TodosLosBancos'+'_TodosLosEjecutivos';
   
         }
         else {
         nombredescarga='ReporteVentas_'+nombrebanco+'_'+ejecutivo;
         }
   }
  else if ((clientes=='') && (ejecutivo=='') && (fechainicial!='')) {
    if (tiporeporte=='1') {
       
      nombredescarga='ReporteVentas_'+fechainicial+'_Hasta_'+fechafinal;
         }
    if (tiporeporte=='2') {
      nombredescarga='ReporteAdministracion_'+fechainicial+'_Hasta_'+fechafinal;
        }
    else {
      nombredescarga='ReporteEjecutivo_'+fechainicial+'_Hasta_'+fechafinal;
    }
  
     }
  }
]);

AppReporteXdias.controller('ReportexdiasController',['$scope','informacion',  function ReportexdiasController($scope,informacion) {
  informacion.postData('api/selectReportexdias')
    .then(function(datos) {
      var someUrl="?cargar=reportexdiasdetalles&var=";
      var fechainicial=document.getElementById('fechainicial').value;
      var fechafinal= document.getElementById('fechafinal').value;
       var tiporeporte= document.getElementById('tiporeporte').value;
      $scope.datos = datos.data
      //databanco= document.getElementById('databanco').value;
      if (fechainicial.value!='') {
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
       
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        /*{
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },*/
        {
            dataField: "correlativo", 
            caption:  "Nº",
            width: "auto"
        },{
           dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalle </a>')
               .attr('href', someUrl + options.value+'/'+fechainicial+'/'+fechafinal+'/'+tiporeporte)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "nombrebanco", 
            caption:  "Nombre Banco",
            width: "auto"
        },
       /* {
            dataField: "ejecutivo", 
            caption:  "Nombre del Ejecutivo Inteligensa",
            width: "auto"
        },*/
        //  {
        //     dataField: "ejecutivobanc", 
        //     caption:  "Nombre del Ejecutivo Banco",
        //     width: "auto"
        // },
      
        {
            dataField: "vendedor", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },

        {
            dataField: "cantidad", 
            caption:  "Cartera De POS",
            width: "auto"
        },
        // {
        //     dataField: "marcapos", 
        //     caption:  "Marca de Pos",
        //     width: "auto"
        // },
        {
            dataField: "tipopos", 
            caption:  "Modelo",
            width: "auto"
        },
        {
            dataField: "ventas_con_fondos", 
            caption:  "Cantidad de Ventas con fondos",
            width: "auto"
        },
        {
            dataField: "facturado", 
            caption:  "Cantidad de POS Facturados",
            width: "auto"
        },  
        // {
        //     dataField: "montousdtranf", 
        //     caption:  "Monto USD Transferencia",
        //     width: "auto"
        // },  
            // {
        //     dataField: "montousdefect", 
        //     caption:  "Monto USD Transferencia",
        //     width: "auto"
        // },  
        //  {
        //     dataField: "montobs", 
        //     caption:  "Monto BS",
        //     width: "auto"
        // },    
        // //      {
        //     dataField: "sinfacturar", 
        //     caption:  "Sin facturar",
        //     width: "auto"
        // },    
              
                     
      ],
       summary: {
            totalItems: [
             {
                column: "cantidad",
                summaryType: "sum"
            },
            {
                column: "ventas_con_fondos",
                summaryType: "sum"
            },
            {
                column: "facturado",
                summaryType: "sum"
            },
            // {
            //     column: "montousd",
            //     summaryType: "sum",
            //     valueFormat:"currency",
            //     precision:2
            // },
            // {
            //     column: "montobs",
            //     summaryType: "sum",
            //     precision:2
            // },

            ]
        },
    paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');

      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), nombredescarga+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
  };

  function customizeHeader(worksheet){

    var fechainicial= document.getElementById('fechainicial').value;
    var clientes= document.getElementById('clientes').value;
    var ejecutivo= document.getElementById('ejecutivo').value;
    var fechafinal= document.getElementById('fechafinal').value;
    var tiporeporte= document.getElementById('tiporeporte').value;
    var nombrebanco= document.getElementById('nombrebanco').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 11; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getRow(1).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    if (tiporeporte=='1') {
    worksheet.getColumn(1).values = [  "REPORTE DE VENTAS",fechainicial+' Hasta '+fechafinal];
    }
  else if (tiporeporte=='2') {
    worksheet.getColumn(1).values = [  "REPORTE DE ADMINISTRACION",fechainicial+' Hasta '+fechafinal];
  }
  };

    }//corchete del if

    else {

var someUrl="?cargar=reportexdiasdetalles&var=";
       $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
 
        {
            dataField: "correlativo", 
            caption:  "Nº",
            width: "auto"
        },{
           dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalle </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "nombrebanco", 
            caption:  "Nombre Banco",
            width: "auto"
        },
       /* {
            dataField: "ejecutivo", 
            caption:  "Nombre del Ejecutivo Inteligensa",
            width: "auto"
        },*/
        //  {
        //     dataField: "ejecutivobanc", 
        //     caption:  "Nombre del Ejecutivo Banco",
        //     width: "auto"
        // },
      
        {
            dataField: "vendedor", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },
    
        {
            dataField: "cantidad", 
            caption:  "Cartera POS",
            width: "auto"
        },

      
        // {
        //     dataField: "marcapos", 
        //     caption:  "Marca de Pos",
        //     width: "auto"
        // },
        {
            dataField: "tipopos", 
            caption:  "Modelo",
            width: "auto"
        },
        {
            dataField: "ventas_con_fondos", 
            caption:  "Cantidad de Ventas con fondos",
            width: "auto"
        },
        {
            dataField: "facturado", 
            caption:  "Cantidad de POS Facturados",
            width: "auto"
        },  
        // {
        //     dataField: "montousd", 
        //     caption:  "Monto USD",
        //     width: "auto"
        // },  
        //  {
        //     dataField: "montobs", 
        //     caption:  "Monto BS",
        //     width: "auto"
        // },    
        //      {
        //     dataField: "sinfacturar", 
        //     caption:  "Sin facturar",
        //     width: "auto"
        // },    
              
                     
      ],
       summary: {
            totalItems: [ 
            {
                column: "cantidad", 
                summaryType: "sum"
            },
            {
                column: "ventas_con_fondos", 
                summaryType: "sum"
            },
            {
                column: "facturado",
                summaryType: "sum"
            },
            // {
            //     column: "montousd",
            //     summaryType: "sum",
            //     valueFormat:"currency",
            //     precision:2
            // },
            // {
            //     column: "montobs",
            //     summaryType: "sum",
            //     precision:2
            // },

            ]
        },
       paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
        onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');

      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), nombredescarga+'.xlsx');
        });
      });
      e.cancel = true;
    },

    };
      function customizeHeader(worksheet){

    var fechainicial= document.getElementById('fechainicial').value;
    var clientes= document.getElementById('clientes').value;
    var ejecutivo= document.getElementById('ejecutivo').value;
    var fechafinal= document.getElementById('fechafinal').value;
    var tiporeporte= document.getElementById('tiporeporte').value;
    var nombrebanco= document.getElementById('nombrebanco').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 5; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 5; columnIndex < 11; columnIndex++){
      worksheet.getColumn(columnIndex).width = 10;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(2).width = 35;
     worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 14 };
    worksheet.getRow(7).getCell(1).font = {  bold: true  };
    worksheet.getRow(7).getCell(2).font = {  bold: true  };
    worksheet.getRow(7).getCell(3).font = {  bold: true  };
    worksheet.getRow(7).getCell(4).font = {  bold: true  };
    worksheet.getRow(7).getCell(5).font = {  bold: true  };
    worksheet.getRow(7).getCell(6).font = {  bold: true  };
    worksheet.getRow(7).getCell(7).font = {  bold: true  };
    worksheet.getRow(7).getCell(8).font = {  bold: true  };
    worksheet.getRow(7).getCell(9).font = {  bold: true  };
    worksheet.getRow(7).getCell(10).font ={  bold: true  };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    if (tiporeporte=='1') {
    worksheet.getColumn(1).values = [ '' ,"REPORTE DE VENTAS POR EJECUTIVO"];
    }
  // else if (tiporeporte=='2') {
  //   worksheet.getColumn(1).values = [  "REPORTE DE ADMINISTRACION",fechainicial+' Hasta '+fechafinal];
  // }
  };
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


/*REPORTE GESTIONES DE EJECUTIVOS */
var Appreportetrabajo= angular.module('Appreportetrabajo', ['dx']);
Appreportetrabajo.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {fechainicial: document.getElementById('fechainicial').value,
                    fechafinal: document.getElementById('fechafinal').value,
                      tiporeporte: document.getElementById('tiporeporte').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

Appreportetrabajo.controller('reportetrabajoController',['$scope','informacion',  function reportetrabajoController($scope,informacion) {
  informacion.postData('api/SelectreportexGestiones')
    .then(function(datos) {
      $scope.datos = datos.data
      //databanco= document.getElementById('databanco').value;
      var fechainicial= document.getElementById('fechainicial').value;
      var fechafinal= document.getElementById('fechafinal').value;
      var tiporeporte= document.getElementById('tiporeporte').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
     
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Nº",
            width: "auto"
        },
        {
            dataField: "nomejecutivo",
            caption:  "Usuario",
            width: "auto"
        },
        {
            dataField: "nombresapellido",
            caption:  "Ejecutivo Gestión",
            width: "auto"
        },
        {
            dataField: "cantidad",
            caption:  "Gestiones realizadas",
            width: "auto"
        }   
      ],
      

       paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
       onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');

      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'Reporte Gestiones por Ejecutivo.xlsx');
        });
      });
      e.cancel = true;
    },


    };

   function customizeHeader(worksheet){

    var fechainicial= document.getElementById('fechainicial').value;
    var ejecutivo= document.getElementById('ejecutivo').value;
    var fechafinal= document.getElementById('fechafinal').value;
    var tiporeporte= document.getElementById('tiporeporte').value;
  
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 11; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 4);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 20;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 14 };
    worksheet.getRow(7).getCell(1).font = {  bold: true };
    worksheet.getRow(7).getCell(2).font = {  bold: true };
    worksheet.getRow(7).getCell(3).font = {  bold: true };
    worksheet.getRow(7).getCell(4).font = {  bold: true };
  

    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    if (tiporeporte=='3') {
    worksheet.getColumn(1).values = [ "","REPORTE GESTIONES POR EJECUTIVO",fechainicial+" hasta "+fechafinal];
    }
  // else if (tiporeporte=='2') {
  //   worksheet.getColumn(1).values = [  "REPORTE DE ADMINISTRACION",fechainicial+' Hasta '+fechafinal];
  // }
  };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

/*REPORTE CONEXIONES DE EJECUTIVOS */
var Appreporteconexiones= angular.module('Appreporteconexiones', ['dx']);
Appreporteconexiones.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);
  
Appreporteconexiones.controller('reporteConexionesController',['$scope','informacion',  function reporteConexionesController($scope,informacion) {
  informacion.postData('api/selectReporteConex')
    .then(function(datos) {
      $scope.datos = datos.data
      //databanco= document.getElementById('databanco').value;
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
        
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Nº",
            width: "auto"
        },
        {
            dataField: "usuario",
            caption:  "Usuario",
            width: "auto"
        },
        {
            dataField: "nombreejecutivo",
            caption:  "Ejecutivo",
            width: "auto"
        },
         {
            dataField: "fecha1",
            caption:  "Conexión Uno",
            width: "auto"
        },{
            dataField: "fecha2",
            caption:  "Conexión Dos",
            width: "auto"
        },{
            dataField: "fecha3",
            caption:  "Conexión Tres",
            width: "auto"
        },{
            dataField: "conexiones",
            caption:  "Historial de conexiones",
            width: "auto"
        }
      ],
      

      paging: { pageSize: 10 },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
        onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');

      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE DE CONEXIONES.xlsx');
        });
      });
      e.cancel = true;
    },
    };
    function customizeHeader(worksheet){
  
   
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 8);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE DE CONEXIONES","desde el 15/05/2019"];
 
  };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);
//Reporte por Gerencia
var AppReporteGrupal = angular.module('AppReporteGrupal', ['dx']);
AppReporteGrupal.service('informacion', ['$http',
  function($http) {
    var vm=this;
        fechainicial= document.getElementById('fechainicial').value;
        fechafinal= document.getElementById('fechafinal').value;
    vm.fdatos = { gerente: document.getElementById('gerente').value, 
                  fechainicial: document.getElementById('fechainicial').value,
                  fechafinal: document.getElementById('fechafinal').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteGrupal.controller('ReporteGrupalController',['$scope','informacion',  function ReporteGrupalController($scope,informacion) {
  informacion.postData('api/selectreporteGrupal')
    .then(function(datos) {
      var someUrl="?cargar=reportegrupaldetalles&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
      selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
         "export": {
          enabled: true,
          // fileName: "Reporte Ventas Por Gerencia",
          allowExportSelectedData: true
      },
      columns: [
        
        {
            dataField: "correlativo",
            caption: "N°",
            width: "auto"
        },{

          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalle </a>')
               .attr('href', someUrl + options.value+'/'+fechainicial+'/'+fechafinal)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "nombrebanco", 
            caption:  "Nombre Banco",
            width: "auto"
        },
      
        {
            dataField: "cantidad", 
            caption:  "Cartera De POS",
            width: "auto"
        },
        
        {
            dataField: "tipopos", 
            caption:  "Modelo",
            width: "auto"
        },
        {
            dataField: "ventas_con_fondos", 
            caption:  "Cantidad Ventas Con Fondos",
            width: "auto"
        },
        {
            dataField: "facturado", 
            caption:  "Cantidad Facturados",
            width: "auto"
        },
        {
            dataField: "montousd", 
            caption:  "Monto USD",
            valueFormat:"currency",
            precision:2,
            width: "auto"
        },
        {
            dataField: "montobs", 
            caption:  "Monto Bs.S",
            width: "auto"
        }, 
      ],
        summary: {
            totalItems: [
            {
                column: "cantidad",
                summaryType: "sum"
            },
            {
                column: "ventas_con_fondos",
                summaryType: "sum"
            },
            {
                column: "facturado",
                summaryType: "sum"
            },
            {
                column: "montousd",
                summaryType: "sum"
            },
              {
                column: "montobs",
                summaryType: "sum"
            }
            ]
        },
       paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,

        onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var vendedor=document.getElementById('vendedor').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE GERENCIA '+vendedor+'.xlsx');
        });
      });
      e.cancel = true;
    },
    };
    },
    ); 
    function customizeHeader(worksheet){
    var vendedor=document.getElementById('vendedor').value;
   
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 9);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE GERENCIAL",vendedor];
 
  };
}]);



//Reporte por Gerencia detallado
var AppReporteGrupaldetalles = angular.module('AppReporteGrupaldetalles', ['dx']);
AppReporteGrupaldetalles.service('informacion', ['$http',
  function($http) {
    var vm=this;
  
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  tipopos: document.getElementById('tipopos').value, 
                  fechainicial: document.getElementById('fechainicial').value,
                  fechafinal: document.getElementById('fechafinal').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteGrupaldetalles.controller('ReporteGrupaldetallesController',['$scope','informacion',  function ReporteGrupaldetallesController($scope,informacion) {
 informacion.postData('api/SelectReporteGrupalDetallado')
    .then(function(datos) {
      
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
      selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
         "export": {
          enabled: true,
          
          allowExportSelectedData: true
      },
      columns: [
        
        {
            dataField: "correlativo",
            caption: "N°",
            width: "auto"
        }, 
        {
            dataField: "nombrebanco", 
            caption:  "Nombre Banco",
            width: "auto"
        },
         {
            dataField: "ejecutivoasig", 
            caption:  "Ejecutivo Asignado",
            width: "auto"
        },  {
            dataField: "usuaedit", 
            caption:  "Usuario Gestión",
            width: "auto"
        },
      
        {
            dataField: "cantidad", 
            caption:  "Cartera De POS",
            width: "auto"
        },
        
        {
            dataField: "tipopos", 
            caption:  "Modelo",
            width: "auto"
        },
        {
            dataField: "ventas_con_fondos", 
            caption:  "Cantidad Ventas Con Fondos",
            width: "auto"
        },
        {
            dataField: "facturado", 
            caption:  "Cantidad Facturados",
            width: "auto"
        },
        {
            dataField: "montousd", 
            caption:  "Monto USD",
            valueFormat:"currency",
            precision:2,
            width: "auto"
        },
        {
            dataField: "montobs", 
            caption:  "Monto Bs.S",
            width: "auto"
        }, 
      ],
        summary: {
            totalItems: [
            {
                column: "cantidad",
                summaryType: "sum"
            },
            {
                column: "ventas_con_fondos",
                summaryType: "sum"
            },
            {
                column: "facturado",
                summaryType: "sum"
            },
            {
                column: "montousd",
                summaryType: "sum"
            },
              {
                column: "montobs",
                summaryType: "sum"
            }
            ]
        },
       paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
       onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var nombrebanco= document.getElementById('nombrebanco').value;
      var tipopos= document.getElementById('tipopos').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE GERENCIAL DETALLADO '+nombrebanco+'/ '+tipopos+'.xlsx');
        });
      });
      e.cancel = true;
    },
     
    };
    },
    ); 
    function customizeHeader(worksheet){
  var nombrebanco= document.getElementById('nombrebanco').value;
  var tipopos= document.getElementById('tipopos').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE GERENCIAL DETALLADO", nombrebanco+'/ '+tipopos];
 
  };
}]);

//Reporte Ventas POS
var AppReportexPOS = angular.module('AppReportexPOS', ['dx']);
AppReportexPOS.service('informacion', ['$http',
  function($http) {
    var vm=this;
       fechainicial= document.getElementById('fechainicial').value;
       fechafinal= document.getElementById('fechafinal').value;
    vm.fdatos = { POS: document.getElementById('POS').value, 
                  fechainicial: document.getElementById('fechainicial').value,
                  fechafinal: document.getElementById('fechafinal').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReportexPOS.controller('ReportexPOSController',['$scope','informacion',  function ReportexPOSController($scope,informacion) {
  informacion.postData('api/SelectReporteVentasTipospos')
    .then(function(datos) {
      var someUrl="?cargar=reportexposdetalles&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
      selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
         "export": {
          enabled: true,
          fileName: "ReportePOS",
          allowExportSelectedData: true
      },
      columns: [
        
        {
            dataField: "correlativo",
            caption: "N°",
            width: "auto"
        },{

          dataField: 'marcapos',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalle </a>')
               .attr('href', someUrl + options.value+'/'+fechainicial+'/'+fechafinal)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "tipopos", 
            caption:  "POS",
            width: "auto"
        },  
        {
            dataField: "marcapos", 
            caption:  "Modelo",
            width: "auto"
        },
      
        {
            dataField: "cantidad", 
            caption:  "Cartera De POS",
            width: "auto"
        },
        
      
        {
            dataField: "ventas_con_fondos", 
            caption:  "Cantidad Ventas Con Fondos",
            width: "auto"
        },
        {
            dataField: "facturado", 
            caption:  "Cantidad Facturados",
            width: "auto"
        },
        {
            dataField: "montousd", 
            caption:  "Monto USD",
            valueFormat:"currency",
            precision:2,
            width: "auto"
        },
        {
            dataField: "montobs", 
            caption:  "Monto Bs.S",
            width: "auto"
        }, 
      ],
        summary: {
            totalItems: [
            {
                column: "cantidad",
                summaryType: "sum"
            },
            {
                column: "ventas_con_fondos",
                summaryType: "sum"
            },
            {
                column: "facturado",
                summaryType: "sum"
            },
            {
                column: "montousd",
                summaryType: "sum"
            },
              {
                column: "montobs",
                summaryType: "sum"
            }
            ]
        },
       paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
       onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
   
      var tipopos= document.getElementById('tipopos').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE VENTAS POS '+tipopos+'.xlsx');
        });
      });
      e.cancel = true;
    },
     
    };
    },
    ); 
    function customizeHeader(worksheet){
 
  var tipopos= document.getElementById('tipopos').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 9);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE POS", tipopos];
 
  };
}]);


//Reporte Ventas x dias detallado

var AppReporteXdiasdetallado = angular.module('AppReporteXdiasdetallado', ['dx']);

AppReporteXdiasdetallado.controller('ReportexdiasDetalladoController', function ReportexdiasDetalladoController($scope,$http) {

    var  banco= document.getElementById('banco').value
    var  usuarioeje= document.getElementById('usuarioeje').value
    var  tipopos= document.getElementById('tipopos').value
    var  fechainicial= document.getElementById('fechainicial').value
    var  fechafinal= document.getElementById('fechafinal').value
    var  tiporeporte= document.getElementById('tiporeporte').value
    var  idcliente= document.getElementById('idcliente').value
    var url = "api/SelectReportexDiasDetallado.php";
      
    $scope.gridOptions = {

        dataSource: DevExpress.data.AspNet.createStore({
            key: "id",
            loadUrl: url + "/Reporte?a=1&var="+banco+'/'+usuarioeje+'/'+tipopos+'/'+fechainicial+'/'+fechafinal+'/'+tiporeporte,
            updateUrl: url + "/UpdateRep?a=2&var="+idcliente,

        }),   
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
         "export": {
          enabled: true,
          fileName: "reporteDetalladoPOS",
          allowExportSelectedData: true
      },
      columns: [
        
        {
            dataField: "correlativo",
            caption: "N°",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "id", 
            caption:  "ID",
            allowEditing: false, 
            width: "auto"
        },{
            dataField: "nombrebanco", 
            caption:  "Nombre Banco",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "fechacomisiones",
            caption:  "Fecha pago",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La fecha del pago es requerida"
            }],      
        },
         {
            dataField: "confcomisiones", 
            caption:  "Confirmación",
            width: "auto",
            validationRules: [{
            type: "required",
            message: "La forma de pago es requerida"
            }],
            lookup: {
                    dataSource: [ 
                    {
                        "codpago": "0",
                        "namepago": "POR DEFINIR"
                    },{
                        "codpago": "1",
                        "namepago": "PAGADO"
                    },{
                        "codpago": "2",
                        "namepago": "SIN PAGO"
                    }],
                    valueExpr: "codpago",
                    displayExpr: "namepago"
                  }

        },
        {
            dataField: "razonsocial", 
            caption:  "Razón Social",
            allowEditing: false, 
            width: "auto"
        },{
            dataField: "coddocumento", 
            caption:  "RIF",
            allowEditing: false,
            width: "auto"
        },{
            dataField: "afiliado", 
            caption:  "Número de Afiliación",
            allowEditing: false,
            width: "auto"
        },{
            dataField: "ejecutivogestion", 
            caption:  "Ejecutivo Asignado",
            allowEditing: false,
            width: "auto"
        },{
            dataField: "gestionador", 
            caption:  "Ejecutivo Gestión",
            allowEditing: false,
            width: "auto"
        },
        ,{
            dataField: "vendedorfinal", 
            caption:  "Vendedor",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fechaedicion", 
            caption:  "Fecha Última Gestión",
            allowEditing: false,
            width: "auto"
        },
          
        {
            dataField: "tipo", 
            caption:  "Tipos POS",
            allowEditing: false,
            width: "auto"
        },  
        {
            dataField: "modelo", 
            caption:  "Modelo",
            allowEditing: false,
            width: "auto"
        },{
            dataField: "cantidad", 
            caption:  "Cartera De POS",
            allowEditing: false,
            width: "auto"
        },
    
        {
            dataField: "ventas_con_fondos", 
            caption:  "Cantidad Ventas Con Fondos",
            allowEditing: false,
            width: "auto"
        },{
            dataField: "fecha_ventasconfondos", 
            caption:  "Fecha Cierre de Venta",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "facturado", 
            caption:  "Cantidad Facturados",
            allowEditing: false,
            width: "auto"
        },{
            dataField: "fechafactura", 
            caption:  "Fecha Facturación",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "numerofactura", 
            caption:  "Número Factura",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "montousdtransf", 
            caption:  "Monto USD Transferencia",
            valueFormat:"currency",
            precision:2,
            allowEditing: false,
            width: "auto"
        },
          {
            dataField: "montousdefec", 
            caption:  "Monto USD Efectivo",
            valueFormat:"currency",
            precision:2,
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "montobs", 
            caption:  "Monto Bs.S",
            allowEditing: false,
            width: "auto"
        }, 
      ],
      summary: {
            totalItems: [
             {
                column: "correlativo",
                summaryType: "count"
            },
            {
                column: "cantidad",
                summaryType: "sum"
            },
            {
                column: "ventas_con_fondos",
                summaryType: "sum"
            },
            {
                column: "facturado",
                summaryType: "sum"
            },
            {
                column: "montousd",
                summaryType: "sum"
            },
              {
                column: "montobs",
                summaryType: "sum"
            }
            ]
        },
    paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
        showBorders: true,
        editing: {
           mode: "cell",
            allowUpdating: true
            //allowDeleting: elim
        },
         };
});


//Reporte Ventas POS detallado
var AppReportexPOSdetallado = angular.module('AppReportexPOSdetallado', ['dx']);
AppReportexPOSdetallado.service('informacion', ['$http',
  function($http) {
    var vm=this;
            var   fechainicial= document.getElementById('fechainicial').value;
            var   fechafinal= document.getElementById('fechafinal').value;
    vm.fdatos = { marcapos: document.getElementById('marcapos').value, 
                  fechainicial: document.getElementById('fechainicial').value,
                  fechafinal: document.getElementById('fechafinal').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReportexPOSdetallado.controller('ReportexPOSDetalladoController',['$scope','informacion',  function ReportexPOSDetalladoController($scope,informacion) {
  informacion.postData('api/SelectReportexPOSdetallado')
    .then(function(datos) {
      var someUrl="?cargar=reportexposdetalles&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
      selection: {
          mode: "multiple"
      },  
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
         "export": {
          enabled: true,
          fileName: "reporteDetalladoPOS",
          allowExportSelectedData: true
      },
      columns: [
        
        {
            dataField: "correlativo",
            caption: "N°",
            width: "auto"
        },{
            dataField: "nombrebanco", 
            caption:  "Nombre Banco",
            width: "auto"
        },
          {
            dataField: "cantidad", 
            caption:  "Cartera De POS",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "POS",
            width: "auto"
        },  
        {
            dataField: "marcapos", 
            caption:  "Modelo",
            width: "auto"
        },
    
        {
            dataField: "ventas_con_fondos", 
            caption:  "Cantidad Ventas Con Fondos",
            width: "auto"
        },
        {
            dataField: "facturado", 
            caption:  "Cantidad Facturados",
            width: "auto"
        },
        {
            dataField: "montousd", 
            caption:  "Monto USD",
            valueFormat:"currency",
            precision:2,
            width: "auto"
        },
        {
            dataField: "montobs", 
            caption:  "Monto Bs.S",
            width: "auto"
        }, 
      ],
      summary: {
            totalItems: [
            {
                column: "cantidad",
                summaryType: "sum"
            },
            {
                column: "ventas_con_fondos",
                summaryType: "sum"
            },
            {
                column: "facturado",
                summaryType: "sum"
            },
            {
                column: "montousd",
                summaryType: "sum"
            },
              {
                column: "montobs",
                summaryType: "sum"
            }
            ]
        },
      paging: { pageSize: 10 },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
        onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
   
      var marcapos= document.getElementById('marcapos').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE VENTAS POS '+marcapos+'.xlsx');
        });
      });
      e.cancel = true;
    },
    };
    },
    ); 
     function customizeHeader(worksheet){
 
  var marcapos= document.getElementById('marcapos').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 9);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE POS", marcapos];
 
  };
}]);

var Appreporteestatus = angular.module('Appreporteestatus', ['dx']);
Appreporteestatus.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatus.controller('Controllerreporteestatus',['$scope','informacion',  function Controllerreporteestatus($scope,informacion) {
  informacion.postData('api/selectReporteEstatus')
    .then(function(datos) {
      var tipousuario= document.getElementById('tipousuario').value;
      var busqueda= document.getElementById('banco').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus",
          allowExportSelectedData: false
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "correlativo",
            caption: "N°",
            width: "auto"
        }, {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + '/' + '/' + busqueda)
               .appendTo(container);
            }  
        },  {
            dataField: "fechacarga",
            caption: "Fecha Carga Sistema",
            width: "auto"
        },
        {
            dataField: "bancocodigo",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },{
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },
        {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },{
            dataField: "cantidad",
            caption: "Cantidad Terminales",
            width: "auto"
        },{
            dataField: "estatuscliente",
            caption: "Estatus",
            width: "auto"
        }
      ],
       summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            },
            {
                column: "cantidad",
                summaryType: "sum"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var Appreporteestatusgeneral = angular.module('Appreporteestatusgeneral', ['dx']);
Appreporteestatusgeneral.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value,
                   region: document.getElementById('region').value,
                     modalidad: document.getElementById('modalidad').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatusgeneral.controller('Controllerreporteestatusgeneral',['$scope','informacion',  function Controllerreporteestatusgeneral($scope,informacion) {
  informacion.postData('api/selectReporteEstatusGeneral')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus General",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "nbanco",
            caption: "Banco",
            width: "auto"
        },
        {
          dataField: "desc_switch",
          caption: "Switcher Bancario",
          width: "auto"
      },
        {
            dataField: "rlegal",
            caption: "Representante Legal",
            width: "auto"
        },
         {
            dataField: "actividadeconomica",
            caption: "Actividad Económica",
            width: "auto"
        },

        {
            dataField: "desc_expediente",
            caption: "Estatus Expediente",
            width: "auto"
        },

        {
            dataField: "estatuscontac",
            caption: "Estatus Contacto",
            width: "auto"
        },
        {
            dataField: "estatuscliente",
            caption: "Estatus Cliente",
            width: "auto"
        },

        {
            dataField: "descequipo",
            caption: "Estatus del Equipo",
            width: "auto"
        },
        {
            dataField: "modalidad",
            caption: "Modalidad de Venta",
            width: "auto"
        },
        {
            dataField: "desc_resultvisita",
            caption: "Estatus de la Visita",
            width: "auto"
        },
        {
            dataField: "fechacarga",
            caption: "Fecha Carga al Sistema",
            width: "auto"
        }, 
        {
            dataField: "fechaestion",
            caption: "Fecha Gestión",
            width: "auto"
        },
        {
            dataField: "idatepagos",
            caption: "Fecha Carga de Pagos",
            width: "auto"
        },          
        {
            dataField: "idatefactura",
            caption: "Fecha Carga Factura",
            width: "auto"
        },
        {
            dataField: "fechaparametros",
            caption: "Fecha Carga Parametros",
            width: "auto"
        },
        {
            dataField: "fechainstalacion",
            caption: "Fecha Instalacion",
            width: "auto"
        },
        {
            dataField: "fechapercance_eq",
            caption: "Fecha Percance",
            width: "auto"
        },
        {
            dataField: "activapercance",
            caption: "Fecha Activación Percance",
            width: "auto"
        },
        {
            dataField: "fechadeclinacion",
            caption: "Fecha Declinacion",
            width: "auto"
        },
        {
            dataField: "fechavisita",
            caption: "Fecha Visita",
            width: "auto"
        },
         {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "oservicio",
            caption: "Orden de Servicio",
            width: "auto"
        },
        {
            dataField: "ifechaorden",
            caption: "Fecha Orden de Servicio",
            width: "auto"
        },
        // {
        //   dataField: "migraestatus",
        //   caption:  "Estatus Cliente",
        //   width: "auto", 
        //   cellTemplate: function (container, options) {
        //     var x = options.value;
        //       if(x =='0' ||  x==null){
        //         $('<td>'+ 'Cliente Inteligensa' +'</td>')
        //         .css('color','#000000')
        //         .css('background','#ffffff')
        //         .css('text-align','center')
        //         .css('border-radius','0.2em')
        //         .css('padding','.3em')
        //         .appendTo(container); 
        //       } else if(x=='1'){
        //         $('<td>'+ 'Migración' +'</td>')
        //         .css('color','#000000')
        //         .css('background','#ffffff')
        //         .css('text-align','center')
        //         .css('border-radius','0.2em')
        //         .css('padding','.3em')
        //         .appendTo(container);  
        //       }else if(x=='2'){
        //         $('<td>'+ 'Comodato' +'</td>')
        //         .css('color','#000000')
        //         .css('background','#ffffff')
        //         .css('text-align','center')
        //         .css('border-radius','0.2em')
        //         .css('padding','.3em')
        //         .appendTo(container);  
        //       }
        //     } 
        //   },
         {
            dataField: "terminal",
            caption: "Nº Terminal",
            width: "auto"
        },

        {
            dataField: "descperfil",
            caption: "Perfil del Aplicativo",
            width: "auto"
        },

        {
            dataField: "descripcionversion",
            caption: "Versión del Aplicativo",
            width: "auto"
        },

         {
            dataField: "ejec",
            caption: "Ejecutivo",
            width: "auto"
        },   
        {
            dataField: "region_ejec",
            caption: "Región Asignada",
            width: "auto"
        },     
       
         {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
        {
            dataField: "cantidadpos",
            caption: "Cantidad Terminales",
            width: "auto"
        },
        {
            dataField: "tipoposs",
            caption: "Tipo de POS",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Modelo de POS",
            width: "auto"
        },
         {
            dataField: "factura",
            caption: "Nº Factura",
            width: "auto"
        },
         {
            dataField: "serialp",
            caption: "Serial POS",
            width: "auto"
        },
        {
            dataField: "serialcompleto",
            caption: "Serial POS Completo",
            width: "auto"
        },
         {
            dataField: "serialsim",
            caption: "Serial SimCard",
            width: "auto"
        },
        {
            dataField: "serialmifi",
            caption: "Serial Mifi",
            width: "auto"
        },
         
        {
            dataField: "estado",
            caption: "Estado",
            width: "auto"
        }, 
        {
            dataField: "municipio",
            caption: "Municipio",
            width: "auto"
        }, 
         {
            dataField: "direccions",
            caption: "Direccion Fiscal.",
            width: "auto"
        },
         {
            dataField: "direccion_insst",
            caption: "Direccion Instalación.",
            width: "auto"
        },
        {
            dataField: "bancoanterior",
            caption: "Banco Anterior.",
            width: "auto"
        },
        {
            dataField: "razonsocialanterior",
            caption: "Razón Social anterior.",
            width: "auto"
        },
        {
            dataField: "id_adm",
            caption: "cod_adm",
            width: "auto"
        },
                
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);




// reporte de registros domiciliados 


var Appreportedomiciliaciongeneral = angular.module('Appreportedomiciliaciongeneral', ['dx']);
Appreportedomiciliaciongeneral.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreportedomiciliaciongeneral.controller('Controllerreportedomiciliaciongeneral',['$scope','informacion',  function Controllerreportedomiciliaciongeneral($scope,informacion) {
  informacion.postData('api/selectReporteDomiciliacionGeneral')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus General",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "nbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "rlegal",
            caption: "Representante Legal",
            width: "auto"
        },
         {
            dataField: "actividadeconomica",
            caption: "Actividad Económica",
            width: "auto"
        },
        {
            dataField: "rifbanco",
            caption: "RIF Bancario",
            width: "auto"
        },

        {
            dataField: "estatuscontac",
            caption: "Estatus Contacto",
            width: "auto"
        },
        {
            dataField: "estatuscliente",
            caption: "Estatus Cliente",
            width: "auto"
        },

        {
            dataField: "descequipo",
            caption: "Estatus del Equipo",
            width: "auto"
        },
        {
            dataField: "desc_resultvisita",
            caption: "Estatus de la Visita",
            width: "auto"
        },
        {
            dataField: "fechacarga",
            caption: "Fecha Carga al Sistema",
            width: "auto"
        }, 
        {
            dataField: "fecharecep",
            caption: "Fecha Recepcion",
            width: "auto"
        },
        {
            dataField: "fechaestion",
            caption: "Fecha Gestión",
            width: "auto"
        },
        {
            dataField: "idatepagos",
            caption: "Fecha Carga de Pagos",
            width: "auto"
        },          
        {
            dataField: "idatefactura",
            caption: "Fecha Carga Factura",
            width: "auto"
        },
        {
            dataField: "fechainstalacion",
            caption: "Fecha Instalacion",
            width: "auto"
        },
        {
            dataField: "fechapercance_eq",
            caption: "Fecha Percance",
            width: "auto"
        },
        {
            dataField: "fechadeclinacion",
            caption: "Fecha Declinacion",
            width: "auto"
        },
        {
            dataField: "fechavisita",
            caption: "Fecha Visita",
            width: "auto"
        },
         {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "oservicio",
            caption: "Orden de Servicio",
            width: "auto"
        },
        {
            dataField: "ifechaorden",
            caption: "Fecha Orden de Servicio",
            width: "auto"
        },
         {
            dataField: "terminal",
            caption: "Nº Terminal",
            width: "auto"
        },

        {
            dataField: "descperfil",
            caption: "Perfil del Aplicativo",
            width: "auto"
        },

        {
            dataField: "descripcionversion",
            caption: "Versión del Aplicativo",
            width: "auto"
        },

         {
            dataField: "ejec",
            caption: "Ejecutivo",
            width: "auto"
        },     
       
         {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        },

         {
            dataField: "correo_corporativo",
            caption: "Correo Electronico Corporativo",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
        {
            dataField: "cantidadpos",
            caption: "Cantidad Terminales",
            width: "auto"
        },
        {
            dataField: "tipoposs",
            caption: "Tipo de POS",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Modelo de POS",
            width: "auto"
        },
         {
            dataField: "factura",
            caption: "Nº Factura",
            width: "auto"
        },
         {
            dataField: "serialp",
            caption: "Serial POS",
            width: "auto"
        },
         {
            dataField: "serialsim",
            caption: "Serial SimCard",
            width: "auto"
        },
        {
            dataField: "serialmifi",
            caption: "Serial Mifi",
            width: "auto"
        },
         
        {
            dataField: "estado",
            caption: "Estado",
            width: "auto"
        }, 
        {
            dataField: "municipio",
            caption: "Municipio",
            width: "auto"
        }, 
         {
            dataField: "direccions",
            caption: "Direccion Fiscal.",
            width: "auto"
        }
                
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


// reporte general de registros de mifi 


var Appreporteestatussustituciones = angular.module('Appreporteestatussustituciones', ['dx']);
Appreporteestatussustituciones.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatussustituciones.controller('Controllerreporteestatussustituciones',['$scope','informacion',  function Controllerreporteestatussustituciones($scope,informacion) {
  informacion.postData('api/selectReporteEstatusSustitucion')
    .then(function(datos) {

      $scope.datos = datos.data
      var someUrl="?cargar=reportedetalladosustitucion&var=";  
      //var url = "api/selectGestionMantenimiento.php";
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus Sustitución",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
            {
            dataField: "enlace",
            caption: "Acción",
            width: "auto",

              cellTemplate: function (container, options) {
              var x = options.value;
               l= x.split('/');
                if(l[3] =="1" || l[3] === 1){
               
                      $('<a>'+ 'Revisar Sustitucion' +'</a>')
                      .attr('href', someUrl + options.value)
                      .appendTo(container);
                 
                 } else {
                     
                      $('<td>'+ 'No posee Sustitucion' +'</td>')
                       .attr('href', someUrl + options.value)
                      .appendTo(container);
                    
                    }  
               /* $('<a>'+l[1] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);*/


                 }
        },
         {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "razonsocial",
            caption: "Razón Social",
            width: "auto"
        },

        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "estatus",
            caption: "Estatus del Equipo",
            width: "auto"
        },
         {
            dataField: "serial",
            caption: "Serial Actual del Equipo",
            width: "auto"
        },
         {
            dataField: "serialmifi",
            caption: "Mifi Actual",
            width: "auto"
        },

        {
            dataField: "sim",
            caption: "Simcard Actual",
            width: "auto"
        }        
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);





//Reporte por Gerencia detallado
var AppReporteDetalladoSustitucion = angular.module('AppReporteDetalladoSustitucion', ['dx']);
AppReporteDetalladoSustitucion.service('informacion', ['$http',
  function($http) {
    var vm=this;
  
    vm.fdatos = { cliente: document.getElementById('cliente').value, 
                  secuencial: document.getElementById('secuencial').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteDetalladoSustitucion.controller('ReporteDetalladoSustitucionController',['$scope','informacion',  function ReporteDetalladoSustitucionController($scope,informacion) {
 informacion.postData('api/SelectReporteDetalladoSustitucion')
    .then(function(datos) {
      
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
      selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
         "export": {
          enabled: true,
          
          allowExportSelectedData: true
      },
      columns: [
        
        {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
        {
            dataField: "descripcion", 
            caption:  "Información del Serial",
            width: "auto"
        },

        {
            dataField: "cliente", 
            caption:  "ID",
            width: "auto"
        },
         {
            dataField: "nbanco", 
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "documento", 
            caption:  "Rif",
            width: "auto"
        },
         {
            dataField: "razonsoc", 
            caption:  "Razon Social",
            width: "auto"
        }, 

        {
            dataField: "factura", 
            caption:  "Nº Factura",
            width: "auto"
        },
             
        {
            dataField: "serialpos", 
            caption:  "Serial del Equipo",
            width: "auto"
        },
        {
            dataField: "simcard", 
            caption:  "Serial Simcard",
            width: "auto"
        },
        {
            dataField: "mifiserial", 
            caption:  "Serial Mifi",
            width: "auto"
        }

        
      ],

       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var Appreportegeneraldemifi = angular.module('Appreportegeneraldemifi', ['dx']);
Appreportegeneraldemifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreportegeneraldemifi.controller('Controllerreportegeneraldemifi',['$scope','informacion',  function Controllerreportegeneraldemifi($scope,informacion) {
  informacion.postData('api/selectReporteGeneraldeMifi')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte General de Mifi",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },

        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },

         {
            dataField: "nbanco",
            caption: "Banco",
            width: "auto"
        },

        {
            dataField: "estatuscontac",
            caption: "Estatus Contacto",
            width: "auto"
        },
        {
            dataField: "estatuscliente",
            caption: "Estatus Cliente",
            width: "auto"
        },

        {
            dataField: "descequipo",
            caption: "Estatus del Equipo",
            width: "auto"
        },
         {
            dataField: "fecharecep",
            caption: "Fecha Recepcion",
            width: "auto"
        },
               
         {
            dataField: "ejec",
            caption: "Ejecutivo",
            width: "auto"
        },     

        {
            dataField: "fechainstalacion",
            caption: "Fecha Instalacion",
            width: "auto"
        },
        
         {
            dataField: "correo",
            caption: "Correo Electronico",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "tlf3",
            caption: "Teléfono 3",
            width: "auto"
        },
         {
            dataField: "tlf4",
            caption: "Teléfono 4",
            width: "auto"
        },
        {
            dataField: "cantidadpos",
            caption: "Cantidad Terminales",
            width: "auto"
        },
        {
            dataField: "tipoposs",
            caption: "Tipo de Equipo",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Modelo de Equipo",
            width: "auto"
        },
         {
            dataField: "factura",
            caption: "Nº Factura",
            width: "auto"
        },
         {
            dataField: "serialmifi",
            caption: "Serial MIFI",
            width: "auto"
        },
         {
            dataField: "serialsim",
            caption: "Serial SimCard",
            width: "auto"
        },
        
         {
            dataField: "fechacarga",
            caption: "Fecha Carga",
            width: "auto"
        }, 
         {
            dataField: "direccions",
            caption: "Direccion Fiscal.",
            width: "auto"
        }
         
          
         
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



/* APP CAPTACION CLIENTE */
var AppCaptacion = angular.module('AppCaptacion', ['dx']);

AppCaptacion.controller('CaptacionController', function CaptacionController($scope,$http) {
    var now = new Date();
    var url = "api/crudCaptacion.php";  
       $http.get('api/selectbanco.php').then(function(response){
       $scope.banco = response.data;
       $http.get('api/selectMedioContacto.php').then(function(response){
       $scope.medio = response.data;
       var usuario=document.getElementById('usuario').value;
       // $scope.showColon = false;

    $scope.gridOptions = {

        dataSource: DevExpress.data.AspNet.createStore({
            key: "oid_consecutivo",
            loadUrl: url + "/viewRegistro?a=1&usuario="+usuario,
            updateUrl: url + "/UpdateRegistro?a=3&id_registro="
            // insertUrl: url + "/InsertPago?a=2&id_registro="+id_afiliado,

            // deleteUrl: url + "/DeleteEj?a=4&id_registro="+id_afiliado,
        }), 
    
    searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."



        },  
             selection: {
          mode: "multiple"
      },

      groupPanel: {
          visible: true
      },
        "export": {
          enabled: true,
          fileName: "Data_captacion_clientes",
          allowExportSelectedData: false
      },

      columns: [
        {
                type: "buttons",
                width: 40,
                buttons: ["edit", {
                    visible: function(e) {
                        return !e.row.isEditing;
                    }
                  
                }]
            },
        {
            dataField: "ofecharegistro", 
            caption:  "Fecha",
            width: "auto"
          
            
        },  {
            dataField: "onombre", 
            caption:  "Nombre",
            validationRules: [{
                type: "required",
                message: "El nombre es requerido "
            }],
            width: "auto"
          
            
        },
        {
            dataField: "oapellido", 
            caption:  "Apellido",
            validationRules: [{
                type: "required",
                message: "El apellido es requerido "
            }],
            width: "auto"
            
        },
        {
            dataField: "ocoddocumento", 
            caption:  "RIF ó Cédula",
            validationRules: [{
                type: "required",
                message: "El Documento es requerido "
            }],
             width: "auto"

        },
         {
            dataField: "onafiliado", 
            caption:  "Número Afiliación",
             width: "auto"

        },
        {
            dataField: "otlf1", 
            caption:  "Teléfono",
                  validationRules: [{ type: "required" }, {
                    type: "pattern",
                    message: 'El télefono debe tener el siguiente formato "(0412)-555-5555 ".',
                    pattern: /^\(\d{4}\)-\d{3}-\d{4}$/i 
                }],
             width: "auto"

        },
        {
            dataField: "orazonsocial", 
            caption:  "Razón Social",
            validationRules: [{
                type: "required",
                message: "La razón social es requerida "
            }],
             width: "auto"

        },
        {
            dataField: "obancointeres", 
            caption:  "Banco Interés",
            width: "auto"

        },
        {
            dataField: "oejecutivo", 
            caption:  "Ejecutivo Asignado",
            width: "auto",
            editorOptions: {
                   disabled: true
                  },
            cellTemplate: function (container, options) {
              var x = options.value;
              //console.log(x);
                if(x !=null) {
              
                $('<div class="center"><td><b>'+ x +'</b></td></div>')
                   .css('color','white') 
                   .css('background','#01DF3A')
                   .css('text-align','center')
                   .css('border-radius','0.2em')
                   .css('padding','.3em')
                   .appendTo(container);
              
              }else{
        
              $('<div class="center"><td><b>'+ "POR ASIGNAR" +'</b></td></div>')
                 .css('color','white') 
                 .css('background','RED')
                 .css('text-align','center')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container);
              }
         //console.log( $scope.showColon);
             }

            
        },
        {
            dataField: "ocodigobanco", 
            caption:  "Banco Asignado",
            width: "auto",
            lookup: {
                    dataSource: $scope.banco,
                    valueExpr: "banco",
                    displayExpr: "ibp"
                  },
                  validationRules: [{
                      type: "required",
                      message: "El banco es requerido"
                  }],
                   editorOptions: {
                   disabled: false
                  }  
  
        },
        
         {
            dataField: "ocantposasignados", 
            caption:  "Cantidad de POS",
            validationRules: [{
                type: "required",
                message: "La cantidad de POS es requerida "
            }],
            width: "auto"
            
        },
         {
            dataField: "ocorreorepresentantelegal", 
            caption:  "Correo",
              validationRules: [{ 
                type: "required",
                 message: "El campo Correo es requerido "
               }, { type: "email" }],
            width: "auto"
            
        },
          {
            dataField: "omediocontacto", 
            caption:  "Medio Contacto",
            width: "auto",
            validationRules: [{
                type: "required",
                message: "Medio de contacto es requerido "
            }],
            lookup: {
                    dataSource: $scope.medio,
                    valueExpr: "codigomedioc",
                    displayExpr: "mediocontacto"
                  },
            editorOptions: {
                   disabled: false
                  }
            
        },
        {
            dataField: "odireccion_instalacion", 
            caption:  "Dirección",
            validationRules: [{
                type: "required",
                message: "La dirección es requerida "
            }],
            width: "auto"
            
        },
        {
            dataField: "otipocliente", 
            caption:  "Tipo Cliente",
            width: "auto",
            validationRules: [{
                type: "required",
                message: "El campo tipo de cliente es requerido"
            }],
            lookup: {
                   dataSource: [ {
                       "codestatus": "Comercial",
                       "descestatus": "Comercial"
                   },{
                       "codestatus": "Corporativo",
                       "descestatus": "Corporativo"
                   }],
                   valueExpr: "codestatus",
                   displayExpr: "descestatus"
                 },
                   editorOptions: {
                disabled: false
                
            }
            
        },
        {
            dataField: "ousuario", 
            caption:  "Usuario",
            width: "auto",
            visible:false,
              editorOptions: {
                disabled: true
                
            }
            
        },
        {
            dataField: "oestatuscontacto",
            caption: "Estatus",
            visible: false,
            width: "auto",
            editorOptions: {
                disabled: true
                
            }
        },
          {
            dataField: "oid_consecutivo",
            caption:  "Correlativo",
            width: "auto",
            visible:false,
            editorOptions: {
            disabled: true    
            }

        }
   
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
            mode: "popup",
            allowAdding: false,
            allowUpdating: true ,
            allowDeleting: false,
            useIcons: true,
             popup: {
                title: "Captación Cliente - Registro",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }
        },
  summary: {
            totalItems: [
                 {
                column: "oid_consecutivo",
                summaryType: "count",
                valueFormat: ""
            }]
        }

    };

  });//API select bancos
 });
});



// /* APP asignacion de cliente a ejecutivo CLIENTE */

var AppAsignacionEjecutivo= angular.module('AppAsignacionEjecutivo', ['dx']);

AppAsignacionEjecutivo.controller('AsignacionController', function AsignacionController($scope,$http) {
    var now = new Date();
    var url = "api/crudAsignacion.php";  
    $http.get('api/selectbanco.php').then(function(response){ 
    $scope.banco = response.data;
    $http.get('api/selectMedioContacto.php').then(function(response){
    $scope.medio = response.data;
    $http.get('api/SelectEjecutivoVentas.php').then(function(response){
    $scope.ejecutivos = response.data;
    var banco=document.getElementById('banco').value;
    // var id_afiliado=document.getElementById('idregistro').value;
    $scope.gridOptions = {

        dataSource: DevExpress.data.AspNet.createStore({
            key: "oid_consecutivo",
            loadUrl: url + "/viewRegistro?a=1&ocodigobanco="+banco,
            updateUrl: url + "/UpdateRegistro?a=3&ocodigobanco="+banco
            // insertUrl: url + "/InsertPago?a=2&id_registro="+id_afiliado,

            // deleteUrl: url + "/DeleteEj?a=4&id_registro="+id_afiliado,
        }), 
    
     searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."

        },  

        "export": {
          enabled: true,
          fileName: "Data_regsitros",
          allowExportSelectedData: false
      },

      columns: [

           {
                type: "buttons",
                width: 40,
                buttons: ["edit", {
                    visible: function(e) {
                        return !e.row.isEditing;
                    }
                  
                }]
            },
        {
            dataField: "correlativo", 
            caption:  "Nº",
            width: "auto",
            editorOptions: {
                    disabled: true
                
            }
            
        },
          {
            dataField: "onombre", 
            caption:  "Nombre",
            width: "auto",
            editorOptions: {
                    disabled: true
                
            }
            
        },
        {
            dataField: "oapellido", 
            caption:  "Apellido",
            width: "auto",
            editorOptions: {
                    disabled: true
                
            }
            
        },
        {
            dataField: "ocoddocumento", 
            caption:  "RIF ó Cédula",
            width: "auto",
            editorOptions: {
                    disabled: true
                
            }

        },
         {
            dataField: "onafiliado", 
            caption:  "Número Afiliado",
            width: "auto",
            editorOptions: {
                    disabled: true
                
            }

        },
        {
            dataField: "oejecutivo", 
            caption:  "Asignar Ejecutivo",
            width: "auto",
            lookup: {
                    dataSource: $scope.ejecutivos,
                    valueExpr: "aliasvendedor",
                    displayExpr: "aliasvendedor"
                  },
            editorOptions: {
                    disabled: false
                
            },
            cellTemplate: function (container, options) {
              var x = options.value;
              //console.log(x);
                if(x !=null) {
                $('<div class="center"><td><b>'+ x +'</b></td></div>')
                   .css('color','white') 
                   .css('background','#01DF3A')
                   .css('text-align','center')
                   .css('border-radius','0.2em')
                   .css('padding','.3em')
                   .appendTo(container);
              
              }else{
              $('<div class="center"><td><b>'+ "POR ASIGNAR" +'</b></td></div>')
                 .css('color','white') 
                 .css('background','RED')
                 .css('text-align','center')
                 .css('border-radius','0.2em')
                 .css('padding','.3em')
                 .appendTo(container);
              }
         
             }
        },
        {
            dataField: "otlf1", 
            caption:  "Teléfono",
            width: "auto",
            editorOptions: {
                    disabled: true
                
            }

        },
        {
            dataField: "orazonsocial", 
            caption:  "Razón Social",
            width: "auto",
            editorOptions: {
                     disabled: true
                 
            }

        },
           {
            dataField: "omediocontacto", 
            caption:  "Medio Contacto",
            width: "auto",
                lookup: {
                    dataSource: $scope.medio,
                    valueExpr: "codigomedioc",
                    displayExpr: "mediocontacto"
                  },
            editorOptions: {
                disabled: true
                  }
            
        },
        {
            dataField: "obancointeres", 
            caption:  "Banco Interés",
            width: "auto",
            editorOptions: {
                     disabled: true
                 
            }

        },
        {
            dataField: "ocodigobanco", 
            caption:  "Banco Asignado",
            width: "auto",
            lookup: {
                    dataSource: $scope.banco,
                    valueExpr: "banco",
                    displayExpr: "ibp"
                  },
            editorOptions: {
                disabled: true
                
            }
  
        },
        {
            dataField: "ousuario", 
            caption:  "Usuario",
            width: "auto",
            editorOptions: {
                disabled: true
                
            }
            
        },
        {
            dataField: "ocantposasignados", 
            caption:  "Cantidad de POS",
            width: "auto",
            editorOptions: {
                     disabled: true
                 
            }
            
        },
         {
            dataField: "ocorreorepresentantelegal", 
            caption:  "Correo",
            width: "auto",
            editorOptions: {
                     disabled: true
                 
            }
            
        },
            {
            dataField: "odireccion_instalacion", 
            caption:  "Dirección",
            width: "auto",
            editorOptions: {
                     disabled: true
                 
            }
            
        },
        {
            dataField: "otipocliente", 
            caption:  "Tipo Cliente",
            width: "auto",
            lookup: {
                   dataSource: [ {
                       "codestatus": "Comercial",
                       "descestatus": "Comercial"
                   },{
                       "codestatus": "Corporativo",
                       "descestatus": "Corporativo"
                   }],
                   valueExpr: "codestatus",
                   displayExpr: "descestatus"
                 },
                   editorOptions: {
                disabled: true
                
            }
            
        },
        {
            dataField: "oestatuscontacto",
            caption: "Estatus",
            visible: false,
            width: "auto",
            editorOptions: {
                disabled: true
                
            }
        },
          {
            dataField: "oid_consecutivo",
            caption:  "Correlativo",
            width: "auto",
            editorOptions: {
            disabled: true    
            }

        }
   
      ], 
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 30]
        },
      showBorders: true,
        editing: {
            mode: "popup",
            allowAdding: false,
            allowUpdating: true,
            allowDeleting: false,
            useIcons: true,
             popup: {
                title: "Asignación Cliente - Registro",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }
        },
  summary: {
            totalItems: [
                 {
                column: "correlativo",
                summaryType: "count",
                 }]

          }

    };

    });//cierra select ejecutivo
  });//cierra select medio contacto
});//cierra select banco

});


// app reporte de mifi general

var Appreporteestatusgestiondemifi = angular.module('Appreporteestatusgestiondemifi', ['dx']);
Appreporteestatusgestiondemifi.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value, 
                 sitio: document.getElementById('sitio').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatusgestiondemifi.controller('Controllerreporteestatusgestiondemifi',['$scope','informacion',  function Controllerreporteestatusgestiondemifi($scope,informacion) {
  informacion.postData('api/selectReporteGestionMifi')
    .then(function(datos) {
    //  var tipousuario= document.getElementById('tipousuario').value;
     // var ejecutivo= document.getElementById('ejecutivo').value;
     // var usuario= document.getElementById('usuario').value;
      var banco= document.getElementById('banco').value;
      var sitio= document.getElementById('sitio').value;
      var someUrl="serviciotecnicomifi?cargar=procesarconfigmifi&var=";
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus",
          allowExportSelectedData: false
      },
      groupPanel: {
          visible: true
      },

       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },  
         {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
          {
            dataField: "estatus",
            caption: "Estatus Registro",
            width: "auto"
        },
        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        
      
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },
         {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono 1",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "tipopos",
            caption: "Tipo de Equipo",
            width: "auto"
        },
         {
            dataField: "marcapos",
            caption: "Modelo de Equipo",
            width: "auto"
        },
         {
            dataField: "fechac",
            caption: "Fecha de Carga al Sistema",
            width: "auto"
        },
        {
            dataField: "serialdemifi",
            caption: "Serial MIFI",
            width: "auto"
        },
        {
            dataField: "serialsim",
            caption: "Nº de SIM",
            width: "auto"
        },
          {
            dataField: "fechapos",
            caption: "Fecha de Carga de Serial",
            width: "auto"
        },
        
        {
            dataField: "fechainst",
            caption: "Fecha de Instalación",
            width: "auto"
        },{
            dataField: "direccionenvio",
            caption: "Direccion Instalación",
            width: "auto"
        },{
            dataField: "inftecnico",
            caption: "Tecnico",
            width: "auto"
        },
        {
            dataField: "observaciones",
            caption: "Observaciones",
            width: "auto"
        }
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

// app resultado de la gestion

var Appreporteestatusserviciogestion = angular.module('Appreporteestatusserviciogestion', ['dx']);
Appreporteestatusserviciogestion.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {region: document.getElementById('region').value, 
                banco: document.getElementById('banco').value, 
                 sitio: document.getElementById('sitio').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatusserviciogestion.controller('Controllerreporteestatusserviciogestion',['$scope','informacion',  function Controllerreporteestatusserviciogestion($scope,informacion) {
  informacion.postData('api/selectreporteserviciotecnicogestion')
    .then(function(datos) {
    //  var tipousuario= document.getElementById('tipousuario').value;
     // var ejecutivo= document.getElementById('ejecutivo').value;
     // var usuario= document.getElementById('usuario').value;
      var region= document.getElementById('region').value;
      var banco= document.getElementById('banco').value;
      var sitio= document.getElementById('sitio').value;
      var someUrl="gestionarServicoTecnico?cargar=datoserialpos&var=";
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus",
          allowExportSelectedData: false
      },
      groupPanel: {
          visible: true
      },

       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value  + '/' + sitio)
               .appendTo(container);
            }  
        },  
         {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
          {
            dataField: "estatus",
            caption: "Estatus Registro",
            width: "auto"
        },
        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "afiliado",
            caption: "Afiliado",
            width: "auto"
        },
      
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },
         {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono 1",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "tipopos",
            caption: "Tipo de Equipo",
            width: "auto"
        },
         {
            dataField: "marcapos",
            caption: "Modelo de Equipo",
            width: "auto"
        },
         {
            dataField: "fechacarga",
            caption: "Fecha de Carga al Sistema",
            width: "auto"
        },
        {
            dataField: "serial",
            caption: "Serial POS",
            width: "auto"
        },
          {
            dataField: "fechapos",
            caption: "Fecha de Carga de Serial",
            width: "auto"
        },{

            dataField: "terminal",
            caption: "Nº de Terminal",
            width: "auto"
        },
        {
            dataField: "fechaparametros",
            caption: "Fecha de Asignacion de Parámetros",
            width: "auto"
        },
        {
            dataField: "sim",
            caption: "Nº de SIM",
            width: "auto"
        },
        {
            dataField: "serialmifi",
            caption: "Serial Mifi",
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption: "Fecha de Instalación",
            width: "auto"
        },{
            dataField: "direccionenvio",
            caption: "Direccion Instalación",
            width: "auto"
        },{
            dataField: "inftecnico",
            caption: "Tecnico",
            width: "auto"
        },
        {
            dataField: "observaciones",
            caption: "Observaciones",
            width: "auto"
        }
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

    var AppReportePosParametrizados = angular.module('AppReportePosParametrizados', ['dx']);
AppReportePosParametrizados.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value,
                  estatus: document.getElementById('estatus').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReportePosParametrizados.controller('ReportePosParametrizadosController',['$scope','informacion',  function ReportePosParametrizadosController($scope,informacion) {
  informacion.postData('api/selectreporteposparametrizado')
    .then(function(datos) {
     // var permisos= document.getElementById('permisos').value;
      //  if (permisos!='W') {
      //  var mostrar=false;
      // }
      // else{
      //  var mostrar=true;
      // }
      var banco= document.getElementById('banco').value;
      var estatus= document.getElementById('estatus').value;
      var someUrl="ConfiguracionPOS?cargar=datosserviciosafiliado&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioDetallado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "consecutivo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "id_registro_cliente",
            caption: "ID",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value+'/reporte/'+banco+'/'+estatus)
               .appendTo(container);
            }  
        },
        //        {
        //     dataField: "estatus", 
        //     caption:  "Estatus",
        //     width: "auto"
        // }, 
        {
            dataField: "estatus",
            caption:  "Estatus de Equipo",
            width: "auto"
        },
        {
            dataField: "nombre_banco",
            caption:  "Banco",
            width: "auto"
        },

        {
            dataField: "razon_social",
            caption:  "Razon Social",
            width: "auto"
        },
        {
            dataField: "documento",
            caption:  "Documento",
            width: "auto"
        },
        {
            dataField: "factura",
            caption:  "Nº Factura",
            width: "auto"
        },
          {
            dataField: "cantidad",
            caption:  "Cantidad de POS",
            width: "auto"
        },
        {
            dataField: "afiliacion", 
            caption:  "Afiliado",
            width: "auto"
        },
        {
            dataField: "serialpos", 
            caption:  "Serial POS",
            width: "auto"
        },
        {
            dataField: "serialsim", 
            caption:  "Serial Simcard",
            width: "auto"
        },
        {
            dataField: "serialmifi", 
            caption:  "Serial Mifi",
            width: "auto"
        },
        {
            dataField: "observacion", 
            caption:  "Observaciones",
            width: "auto"
        },
        {
            dataField: "usuariogestion", 
            caption:  "Usuario Gestión",
            width: "auto"
        }
      
      ],
      summary: {
            totalItems: [
            {
                column: "consecutivo",
                summaryType: "count"
            },
            {
                column: "cantidad",
                summaryType: "sum"
            }
            ]
          },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,

     
    };
    },
    ); 
}]);
//Reporte Captación
    var appReporteCaptacion = angular.module('appReporteCaptacion', ['dx']);
    appReporteCaptacion.service('informacion', ['$http',
      function($http) {
        var vm=this;
        vm.fdatos = { 
          clientes: document.getElementById('clientes').value,
          ejecutivo: document.getElementById('ejecutivo').value, 
          fechainicial: document.getElementById('fechainicial').value,
          fechafinal: document.getElementById('fechafinal').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
appReporteCaptacion.controller('ReporteCaptacionController',['$scope','informacion',  function ReporteCaptacionController($scope,informacion) {
  informacion.postData('api/selectReporteCaptacionEjecutivos')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteCaptacionEjecutivo",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "id_consecutivo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha",
            width: "auto"
        },
        {
            dataField: "usuario", 
            caption:  "Usuario de Carga",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "nombre",
            caption:  "Nombre",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "coddocumento", 
            caption:  "RIF",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de POS",
            dataType: "date",
            width: "auto"
        },
        ,
        {
            dataField: "ibp", 
            caption:  "Banco Asignado",
            dataType: "date",
            width: "auto"
        },
      ],
      summary: {
            totalItems: [
            {
                column: "id_consecutivo",
                summaryType: "count"
            },
           ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },
    function (error){
      console.log(error, 'El afiliado no tiene gestiones guardadas.');
  }); 
}]);




var AppServicioTecnicoSerial = angular.module('AppServicioTecnicoSerial', ['dx']);
  
AppServicioTecnicoSerial.controller('ServicioTecnicoSerialController', function ServicioTecnicoSerialController($scope,$http) {
     var now = new Date();

    var  cestatus= document.getElementById('codestatus').value;
    $http.get('api/selectestatuspos.php?cestatus='+cestatus).then(function(response){
    $scope.estatuspos = response.data;

    $http.get('api/selecttecnicos.php').then(function(response){
    $scope.tecnicos = response.data;

    var Url = "api/selectServicioTecnicoSerial.php";
    var  vari= document.getElementById('var').value;
    var  serial= document.getElementById('serial').value;
    var  afiliado= document.getElementById('nroafiliacion').value;
    var  usuario= document.getElementById('tipousuario').value;
    var  area= document.getElementById('area').value;
    var  rowinstalado= document.getElementById('rowinstalado').value;
    var  codestatus= document.getElementById('codestatus').value;
    // var  idsecuencial= document.getElementById('idsecuencial').value;
    //alert(area);
    if ((usuario=='T' && area=='9' && codestatus!=21 && (rowinstalado==0 || rowinstalado!=0 && codestatus==13 || codestatus==1 || (codestatus!=5 && codestatus!=7 && codestatus!=8  && codestatus!=9 && codestatus!=10  && codestatus!=12  && codestatus!=14  && codestatus!=16  && codestatus!=2 ) )) || (usuario=='A' && area=='1' && (rowinstalado==0 || rowinstalado!=0 && codestatus==13 || codestatus==1 || (codestatus!=5 && codestatus!=7 && codestatus!=8  && codestatus!=9 && codestatus!=10  && codestatus!=12  && codestatus!=14  && codestatus!=16  && codestatus!=2 && codestatus!=22)))) {
      var insert=true;
      var edit=true;
    }
     else {
      var insert=false;
      var edit=false;
    }
   
      var applyFilterTypes = [{
            key: "auto",
            name: "Immediately"
        }, {
            key: "onClick",
            name: "On Button Click"
        }],
        dataGrid;
    
    function getOrderDay(rowData) {
        return (new Date(rowData.OrderDate)).getDay();
    }

    function redirectrefrecar(){
    setTimeout(function(){ window.location.reload(0); }, 2000); 
  };

    
    $scope.filterRow = {
        visible: true,
        applyFilter: "auto"
    };
    
    $scope.headerFilter = {
        visible: true
    };
  if(rowinstalado==0 && codestatus!=1 ){
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari,
         }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            displayFormat: "dd/MM/yyyy",
            visible:false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false,
            width: "auto"
        },
        
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/mm/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "tecnico", 
            caption:  "Nombre del Técnico",
            width: "auto",
             validationRules: [{
            type: "required",
            message: "Debe Ingresar el Técnico"
            }],
             lookup: {
                    dataSource: $scope.tecnicos,
                    valueExpr: "tecnico",
                    displayExpr: "ntecnico"
                  }
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            value: now,
            message: "La fecha de la Gestión es requerida"
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },

         {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
           {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logistica",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
           {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "popup",
           // allowUpdating: edit,
            allowAdding: insert,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }

        },
    };
   }//end if
   else{
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari
        }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "ntecnico", 
            caption:  "Nombre del Técnico",
            width: "auto"
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },
        {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
         {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logisitca",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto"
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            allowEditing: true,
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true

    };
   }//end else 
   if (codestatus==1 && rowinstalado==0) {

     $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari,
         }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            displayFormat: "dd/MM/yyyy",
            visible:false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/mm/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "tecnico", 
            caption:  "Nombre del Técnico",
            width: "auto",
             validationRules: [{
            type: "required",
            message: "Debe Ingresar el Técnico"
            }],
             lookup: {
                    dataSource: $scope.tecnicos,
                    valueExpr: "tecnico",
                    displayExpr: "ntecnico"
                  }
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            value: now,
            message: "La fecha de la Gestión es requerida"
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },

         {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
           {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logistica",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
           {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "popup",
           // allowUpdating: edit,
            allowAdding: insert,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }

        },
    };

   } else {

 $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari,
         }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            displayFormat: "dd/MM/yyyy",
            visible:false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/mm/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "ntecnico", 
            caption:  "Nombre del Técnico",
            allowEditing: false,
            width: "auto",
           
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            value: now,
            message: "La fecha de la Gestión es requerida"
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },

         {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
           {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logistica",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
           {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "popup",
           // allowUpdating: edit,
            allowAdding: insert,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }

        },
    };


   }
    });// fin estatus pos 
    });// fin del tecnicos 
});


var AppServicioconfigElRosal = angular.module('AppServicioconfigElRosal', ['dx']);
AppServicioconfigElRosal.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioconfigElRosal.controller('ServicioconfigElRosalController',['$scope','informacion',  function ServicioconfigElRosalController($scope,informacion) {
  informacion.postData('api/selectServicioElRosal')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=procesargestion&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioDetallado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
         {
            dataField: "estatuservices",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='8'){
          $('<td>'+ 'Entregado al Rosal' +'</td>')
          .css('color','#f1f1f1')
          .css('background','#000080')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container); 

          } 
          else if(x=='9'){

            $('<td>'+ 'Declinacion de Compra' +'</td>')
            //.css('color','#f1f1f1')
            //.css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='20'){

            $('<td>'+ 'Gestión del Rosal' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='21'){

            $('<td>'+ 'Gestión de Maracaibo' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#8C86B0')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } else if(x=='10'){

            $('<td>'+ 'POS Instalado (Inteligensa)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
         

          else if(x=='12'){

            $('<td>'+ 'POS Instalado (Maracaibo)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }  
        }          
            }, 
          
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            width: "auto"
        }, 

        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
        {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }
      ],

       summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
     
     
    };
    },
    ); 
}]);



var AppServicioInstalarElRosal = angular.module('AppServicioInstalarElRosal', ['dx']);
AppServicioInstalarElRosal.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value, 
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioInstalarElRosal.controller('ServicioInstalarElRosalController',['$scope','informacion',  function ServicioInstalarElRosalController($scope,informacion) {
  informacion.postData('api/selectServicioInstalarElRosal')
    .then(function(datos) {
      var someUrl="?cargar=buscargestiondetallada&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
        selection: {
          mode: "multiple"
      },
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
            {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },   
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }  
      ],

      summary: {
            totalItems: [{
                column: "correlativo",
                summaryType: "count"
            }]
        },
    paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },

      showBorders: true,
    
    };
    },
    ); 
}]);



var AppServicioTecRifgestionRosal = angular.module('AppServicioTecRifgestionRosal', ['dx']);
AppServicioTecRifgestionRosal.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { nrorif: document.getElementById('nrorif').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppServicioTecRifgestionRosal.controller('ServicioTecRifgestionRosalController',['$scope','informacion',  function ServicioTecRifgestionRosalController($scope,informacion) {
  informacion.postData('api/selectServicioAfiliadoRifgestionrosal')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=false;
      }
      else{
       var mostrar=true;
      }
      var someUrl="?cargar=procesargestion&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [

        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        
         {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        }, 
        {
            dataField: "estatuservices",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

        cellTemplate: function (container, options) {
       var x = options.value;
          if(x=='20'){

            $('<td>'+ 'Gestión del Rosal' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#33A2FF')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='21'){

            $('<td>'+ 'Gestión Maracaibo' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#8C86B0')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          
          else if(x=='8'){

            $('<td>'+ 'Entregado al Rosal' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='9'){

            $('<td>'+ 'Declinacion de Compra' +'</td>')
            //.css('color','#f1f1f1')
            //.css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
      
          else if(x=='10'){

            $('<td>'+ 'POS Instalado (Inteligensa)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x==null){

            $('<td>'+ 'Por Asignar Serial' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }

          else if(x=='12'){

            $('<td>'+ 'POS Instalado (Maracaibo)' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } /*
          else if(x=='13'){

            $('<td>'+ 'Configuración MIFI' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='14'){

            $('<td>'+ 'MIFI Instalado' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
          else if(x=='16'){

            $('<td>'+ 'Revisar Mifi' +'</td>')
            .css('color','#f1f1f1')
            .css('background','#2BE787')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 
*/
        }          
            }, 
        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
         {
            dataField: "factura",
            caption:  "N° de Factura",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
          {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }    
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"

            },
              ],
          },
                       
      paging: { pageSize: 10 },
      showBorders: true,
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
     
    };
    },
    ); 
}]);


 // Reporte de gestion rosal y maracaibo   
var Appreporteestatusserviciogestionrosal = angular.module('Appreporteestatusserviciogestionrosal', ['dx']);
Appreporteestatusserviciogestionrosal.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value, 
                 sitio: document.getElementById('sitio').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatusserviciogestionrosal.controller('Controllerreporteestatusserviciogestionrosal',['$scope','informacion',  function Controllerreporteestatusserviciogestionrosal($scope,informacion) {
  informacion.postData('api/selectreporteserviciotecnicogestionrosal')
    .then(function(datos) {
    //  var tipousuario= document.getElementById('tipousuario').value;
     // var ejecutivo= document.getElementById('ejecutivo').value;
     // var usuario= document.getElementById('usuario').value;
      var banco= document.getElementById('banco').value;
      var sitio= document.getElementById('sitio').value;
      var someUrl="serviciotecnicorosal?cargar=procesargestion&var=";
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus",
          allowExportSelectedData: false
      },
      groupPanel: {
          visible: true
      },

       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value  + '/' + sitio)
               .appendTo(container);
            }  
        },  
         {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
          {
            dataField: "estatus",
            caption: "Estatus Registro",
            width: "auto"
        },
        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "afiliado",
            caption: "Afiliado",
            width: "auto"
        },
      
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },
         {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },
         {
            dataField: "tlf1",
            caption: "Teléfono 1",
            width: "auto"
        },
         {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
         {
            dataField: "tipopos",
            caption: "Tipo de Equipo",
            width: "auto"
        },
         {
            dataField: "marcapos",
            caption: "Modelo de Equipo",
            width: "auto"
        },
         {
            dataField: "fechacarga",
            caption: "Fecha de Carga al Sistema",
            width: "auto"
        },
        {
            dataField: "serial",
            caption: "Serial POS",
            width: "auto"
        },
          {
            dataField: "fechapos",
            caption: "Fecha de Carga de Serial",
            width: "auto"
        },{

            dataField: "terminal",
            caption: "Nº de Terminal",
            width: "auto"
        },
        {
            dataField: "fechaparametros",
            caption: "Fecha de Asignacion de Parámetros",
            width: "auto"
        },
        {
            dataField: "sim",
            caption: "Nº de SIM",
            width: "auto"
        },
        {
            dataField: "serialmifi",
            caption: "Serial Mifi",
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption: "Fecha de Instalación",
            width: "auto"
        },{
            dataField: "direccionenvio",
            caption: "Direccion Instalación",
            width: "auto"
        },{
            dataField: "inftecnico",
            caption: "Tecnico",
            width: "auto"
        },
        {
            dataField: "observaciones",
            caption: "Observaciones",
            width: "auto"
        }
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

    

var AppProcesarGestionRosal = angular.module('AppProcesarGestionRosal', ['dx']);
  
AppProcesarGestionRosal.controller('ProcesarGestionRosalController', function ProcesarGestionRosalController($scope,$http) {
     var now = new Date();

    var  cestatus= document.getElementById('codestatus').value;
    $http.get('api/selectestatuspos.php?cestatus='+cestatus).then(function(response){
    $scope.estatuspos = response.data;

    $http.get('api/selecttecnicos.php').then(function(response){
    $scope.tecnicos = response.data;

    var Url = "api/selectServicioTecnicoSerial.php";
    var  vari= document.getElementById('var').value;
    var  serial= document.getElementById('serial').value;
    var  afiliado= document.getElementById('nroafiliacion').value;
    var  usuario= document.getElementById('tipousuario').value;
    var  area= document.getElementById('area').value;
    var  rowinstalado= document.getElementById('rowinstalado').value;
    var  codestatus= document.getElementById('codestatus').value;
    // var  idsecuencial= document.getElementById('idsecuencial').value;
    //alert(area);
     if (((rowinstalado==0 || rowinstalado!=0 && codestatus==13 || codestatus==1 || (codestatus!=5 && codestatus!=7 && codestatus!=8  && codestatus!=9 && codestatus!=10  && codestatus!=12  && codestatus!=14  && codestatus!=16) )) ||  (rowinstalado==0 || rowinstalado!=0 && codestatus==13 || codestatus==1 || (codestatus!=5 && codestatus!=7 && codestatus!=8  && codestatus!=9 && codestatus!=10  && codestatus!=12  && codestatus!=14  && codestatus!=16))) {
      var insert=true;
      var edit=true;
    }
     else {
      var insert=false;
      var edit=false;
    }
   
      var applyFilterTypes = [{
            key: "auto",
            name: "Immediately"
        }, {
            key: "onClick",
            name: "On Button Click"
        }],
        dataGrid;
    
    function getOrderDay(rowData) {
        return (new Date(rowData.OrderDate)).getDay();
    }

    function redirectrefrecar(){
    setTimeout(function(){ window.location.reload(0); }, 2000); 
  };

    
    $scope.filterRow = {
        visible: true,
        applyFilter: "auto"
    };
    
    $scope.headerFilter = {
        visible: true
    };
  if(rowinstalado==0 && codestatus!=1 ){
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari,
         }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            displayFormat: "dd/MM/yyyy",
            visible:false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false,
            width: "auto"
        },
        
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/mm/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "tecnico", 
            caption:  "Nombre del Técnico",
            width: "auto",
             validationRules: [{
            type: "required",
            message: "Debe Ingresar el Técnico"
            }],
             lookup: {
                    dataSource: $scope.tecnicos,
                    valueExpr: "tecnico",
                    displayExpr: "ntecnico"
                  }
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            value: now,
            message: "La fecha de la Gestión es requerida"
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },

         {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
           {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logistica",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
           {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "popup",
           // allowUpdating: edit,
            allowAdding: insert,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }

        },
    };
   }//end if
   else{
    $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari
        }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false, 
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "ntecnico", 
            caption:  "Nombre del Técnico",
            width: "auto"
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },
        {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
         {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logisitca",
            dataType: "date",
            width: "auto"
        },
         {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto"
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            allowEditing: true,
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true

    };
   }//end else 
   if (codestatus==1 && rowinstalado==0) {

     $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari,
         }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            displayFormat: "dd/MM/yyyy",
            visible:false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/mm/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "tecnico", 
            caption:  "Nombre del Técnico",
            width: "auto",
             validationRules: [{
            type: "required",
            message: "Debe Ingresar el Técnico"
            }],
             lookup: {
                    dataSource: $scope.tecnicos,
                    valueExpr: "tecnico",
                    displayExpr: "ntecnico"
                  }
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            value: now,
            message: "La fecha de la Gestión es requerida"
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },

         {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
           {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logistica",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
           {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "popup",
           // allowUpdating: edit,
            allowAdding: insert,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }

        },
    };

   } else {

 $scope.gridOptions = {   
        dataSource: DevExpress.data.AspNet.createStore({
            key: "fechacarga",
            loadUrl: Url + "/Acuerdos?a=1&var="+vari,
            updateUrl: Url + "/UpdateAcu?a=3&var="+vari,
            insertUrl: Url + "/InsertAcu?a=2&var="+vari,
         }), 
         "export": {
          enabled: true,
          fileName: "EstatusInstalacionPOS_"+serial+'_'+afiliado+'_'+fechaactual,
          allowExportSelectedData: false
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "secuencial", 
            caption:  "NRO",
            allowEditing: false, 
            width: "auto"
        },
         {
            dataField: "fechacarga", 
            caption:  "Fecha Carga Sistema",
            allowEditing: false, 
            displayFormat: "dd/MM/yyyy",
            visible:false, 
            width: "auto"
        },
        {
            dataField: "serialpo", 
            caption:  "Serial del POS",
            allowEditing: false,
            width: "auto"
        },
        {
            dataField: "fecharecepalma",
            caption:  "Fecha Recepción (Almacen)",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/mm/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "ntecnico", 
            caption:  "Nombre del Técnico",
            allowEditing: false,
            width: "auto",
           
        },
         {
             dataField: "fechagest",
            caption:  "Fecha de Gestión",
            dataType: "date",
            width: "auto",
            validationRules: [{
            type: "required",
            value: now,
            message: "La fecha de la Gestión es requerida"
            }],
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
            dataField: "estatus", 
            caption:  "Status Instalación de POS",
            width: "auto",
            visible: false,
             validationRules: [{
            type: "required",
            message: "El estatus de instalación es requerido"
            }],
             lookup: {
                    dataSource: $scope.estatuspos,
                    valueExpr: "estatus",
                    displayExpr: "descestatus"
                  }
        },

         {
            dataField: "estatusinst", 
            caption:  "Ubicacion del equipo",
            allowEditing: false,
            width: "auto"
        },
           {
            dataField: "fechalog",
            caption:  "Fecha de Entrega a Logistica",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
          
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
           {
            dataField: "fechaenv",
            caption:  "Fecha de Envio",
            dataType: "date",
            width: "auto",
            displayFormat: "dd/MM/yyyy",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
        {
            dataField: "fechainst",
            caption:  "Fecha de Instalación del POS",
            dataType: "date",
            width: "auto",
            
             calculateFilterExpression: function(value, selectedFilterOperations, target) {
                if(target === "headerFilter" && value === "weekends") {
                    return [[getOrderDay, "=", 0], "or", [getOrderDay, "=", 6]];
                }
                return this.defaultCalculateFilterExpression.apply(this, arguments);
            },
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: "weekends"
                        });                        
                        return results;
                    };
                }
            }
        },
         {
             dataField: "observacion",
            caption:  "Observaciones",
            dataType: "text",
            width: "auto",
          }
      ],
      paging: { pageSize: 10 },
      showBorders: true,
        editing: {
           mode: "popup",
           // allowUpdating: edit,
            allowAdding: insert,
             popup: {
                title: "Agregar datos de instalación",
                showTitle: true,
                width: 950,
                height: 550,
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },     

            }

        },
    };


   }
    });// fin estatus pos 
    });// fin del tecnicos 
});
  var appReporteDomiciliacion = angular.module('appReporteDomiciliacion', ['dx']);
    appReporteDomiciliacion.service('informacion', ['$http',
      function($http) {
        var vm=this;
        vm.fdatos = { 
          cliente: document.getElementById('clientes').value,
          tipoBusq: document.getElementById('estatus').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
appReporteDomiciliacion.controller('ReporteReporteController',['$scope','informacion',  function ReporteReporteController($scope,informacion) {
  informacion.postData('api/selectReporteDomiciliacion')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteCaptacionEjecutivo",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "consecutivo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "id_consecutivo",
            caption: "Id",
            width: "auto"
        },
        {
            dataField: "banco",
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "documento", 
            caption:  "RIF",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razon Social",
            width: "auto"
        },
        {
            dataField: "nafiliacion", 
            caption:  "Afiliacion",
            width: "auto"
        },
        {
            dataField: "tlf1", 
            caption:  "Telefono 1",
            width: "auto"
        },
        {
            dataField: "tlf2", 
            caption:  "Telefono 2",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "POS",
            width: "auto"
        },
        {
            dataField: "serialpos", 
            caption:  "Serial POS",
            width: "auto"
        }, 
        {
            dataField: "numterminal", 
            caption:  "Numero Terminal",
            width: "auto"
        },
        {
            dataField: "serialmifi", 
            caption:  "Serial Mifi",
            width: "auto"
        },
        {
            dataField: "fehcainst", 
            caption:  "Fecha Instalacion",
            width: "auto"
        },
        {
            dataField: "estatus", 
            caption:  "Estatus",
            width: "auto"
        }
      ],
      summary: {
            totalItems: [
            {
                column: "id_consecutivo",
                summaryType: "count"
            },
           ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },
    function (error){
      console.log(error, 'El afiliado no tiene gestiones guardadas.');
  }); 
}]);

var AppEncuestaNueva = angular.module('AppEncuestaNueva', ['dx']);
AppEncuestaNueva.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value,
                  fecha: document.getElementById('fecha').value,
                  origen: document.getElementById('origen').value };
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);


AppEncuestaNueva.controller('EncuestaController',['$scope','informacion',  function EncuestaController($scope,informacion) {
  informacion.postData('api/selectencuestafecha')
    .then(function(datos) {
      var someUrl="?cargar=buscarbancoencuesta&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
            {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },   
         {
            dataField: "ibp",
            caption:  "Banco",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fecharecepcion",
            caption:  "Fecha de Recepción",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "fechacarga", 
            caption:  "Fecha de Carga",
            dataType: "date",
            width: "auto"
        },
        // {
        //     dataField: "usuario", 
        //     caption:  "Usuario",
        //     width: "auto"
        // },
        {
            dataField: "namearchivo", 
            caption:  "Nombre del Archivo",
            width: "auto"
        },
        {
            dataField: "cantidad", 
            caption:  "Cantidad de Registros",
            width: "auto"
        }  
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
     
    };
    },
    ); 
}]);

var AppEncuestaBanco = angular.module('AppEncuestaBanco', ['dx']);
AppEncuestaBanco.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppEncuestaBanco.controller('EncuestaBancoController',['$scope','informacion',  function EncuestaBancoController($scope,informacion) {
  informacion.postData('api/SelectEncuestaBanco')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
      var pregistro= document.getElementById('pregistro').value;
      var codtipousuario= document.getElementById('codtipousuario').value;
      var retorno=document.getElementById('adminvolver').value;
       if (permisos!='W') {
       var mostrar=true;//cambiado a true para que siempre aparezca el boton editar.
      }
      else{
       var mostrar=true;
      }
       if (pregistro!='W') {
       var permisses=false;
      }
      else{
       var permisses=true;
      }
      
      var someUrl="?cargar=DatosEncuesta&var=";
      var Urlactive="?cargar=activarencuesta&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioDetallado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value+'/'+retorno)
               .appendTo(container);
            }  
        },     
         {
            dataField: "estatuss",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Encuestado' +'</td>')
            .css('color','white') 
            .css('background','#01DF3A')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'No Encuestado' +'</td>')
          .css('color','#f1f1f1')
          .css('background','orange')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container);
          } 

          else if(x=='3'){

            $('<td>'+ 'Llamar Luego' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='4'){

            $('<td>'+ 'Encuesta Rechazada' +'</td>')
            .css('color','#f1f1f1')
            .css('background','red')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }   

        }          
            }, 
   {

            dataField: "activo",
            caption:  "Actividad",
            width: "auto",
            useIcons: true,
            enabled:false,
            cssClass: "enlace",
            alignment: "center",
            calculateCellValue: function(data) {
                return [
                data.enlaceactivo
              
                    ]
                      .join("/");
                      
               },  
            
               cellTemplate: function (container, options) {
               var x = options.value;
               enabled:false,
               l= x.split('/');
               var r="Editando...";
               if(l[1] =="1"){
                
                 if (permisses==false && codtipousuario!='A') {
                $('<td>'+ r +'</td>')
                .css('color','white') 
                .css('background','red')
                .css('border-radius','0.2em')
                .css('padding','.3em')
                .appendTo(container);

                 } else if (permisses= true && codtipousuario!='A') {
                  $('<td>'+ r +'</td>')
                  .css('color','white') 
                  .css('background','red')
                  .css('border-radius','0.2em')
                  .css('padding','.3em')
                  .appendTo(container);
                 } else{
                      $('<a>'+ r +'</a>')
                      .attr('href', Urlactive + options.value)
                      .css('color','white') 
                      .css('background','red')
                      .css('border-radius','0.2em')
                      .css('padding','.3em')
                      .appendTo(container);
                 }
                   
                 } else{
                      var r="Disponible";
                      $('<td>'+ r +'</td>')
                      .css('color','white') 
                      .css('background','#01DF3A')
                      .css('border-radius','0.2em')
                      .css('padding','.3em')
                      .appendTo(container);
                    
                    }  
            
             }    
               
        },
           

        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }  
      ],
        summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },

      showBorders: true,
     
    };
    },
    ); 
}]);

var AppEncuestadetallada = angular.module('AppEncuestadetallada', ['dx']);
AppEncuestadetallada.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = { banco: document.getElementById('banco').value,
                  fechadetalle: document.getElementById('fechadetalle').value,
                  namearchivo: document.getElementById('namearchivo').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppEncuestadetallada.controller('EncuestadetalladaController',['$scope','informacion',  function EncuestadetalladaController($scope,informacion) {
  informacion.postData('api/Selectencuestadetallado')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
      var retorno=document.getElementById('adminvolver').value;
      if (permisos!='W') {
       var mostrar=true;
      }
      else{
       var mostrar=false;
      }
      var someUrl="?cargar=DatosEncuesta&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ServicioDetallado",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value+'/'+retorno)
               .appendTo(container);
            }  
        },     
        {
            dataField: "estatuss",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Encuestado' +'</td>')
            .css('color','white') 
            .css('background','#01DF3A')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'No Encuestado' +'</td>')
          .css('color','#f1f1f1')
          .css('background','orange')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container);
          } 

          else if(x=='3'){

            $('<td>'+ 'Llamar Luego' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='4'){

            $('<td>'+ 'Encuesta Rechazada' +'</td>')
            .css('color','#f1f1f1')
            .css('background','red')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }   

        }          
            }, 
        /* {
            dataField: "desc_status",
            caption:  "Estatus de Servicio",
            width: "auto", 
        }, */

        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },
        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }  
      ],
        summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },

      showBorders: true,
     
    };
    },
    ); 
}]);

var AppEncuestaRif = angular.module('AppEncuestaRif', ['dx']);
AppEncuestaRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
  
    vm.fdatos = { nrorif: document.getElementById('nrorif').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppEncuestaRif.controller('EncuestRifController',['$scope','informacion',  function EncuestRifController($scope,informacion) {
  informacion.postData('api/SelectEncuestaRif')
    .then(function(datos) {
      var permisos= document.getElementById('permisos').value;
       if (permisos!='W') {
       var mostrar=true;
      }
      else{
       var mostrar=false;
      }
      var someUrl="?cargar=DatosEncuesta&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [

        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        
        {
          dataField: 'enlace',
          caption:  "Acción",
          visible: mostrar,
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Gestionar </a>')
               .attr('href', someUrl + options.value+'/'+3)
               .appendTo(container);
            }  
        },     
        {
            dataField: "estatuss",
            caption:  "Estatus de Servicio",
            width: "auto", 
              cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Encuestado' +'</td>')
            .css('color','white') 
            .css('background','#01DF3A')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'No Encuestado' +'</td>')
          .css('color','#f1f1f1')
          .css('background','orange')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container);
          } 

          else if(x=='3'){

            $('<td>'+ 'Llamar Luego' +'</td>')
            .css('color','#f1f1f1')
            .css('background','blue')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          } 

          else if(x=='4'){

            $('<td>'+ 'Encuesta Rechazada' +'</td>')
            .css('color','#f1f1f1')
            .css('background','red')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container); 

          }   

        }          
            },

        {
            dataField: "nafiliacion",
            caption:  "N° de Afiliación",
            width: "auto"
        },

        {
            dataField: "razonsocial", 
            caption:  "Razón social",
            width: "auto"
        },
         {
            dataField: "documento", 
            caption:  "RIF / CI",
            width: "auto"
        },
        {
            dataField: "ibp", 
            caption:  "Banco",
            width: "auto"
        },
          {
            dataField: "direccion", 
            caption:  "Dirección",
            width: "auto"
        },
         {
            dataField: "namearchivo", 
            caption:  "Nombre del archivo",
            width: "auto"
        },
        {
            dataField: "tipopos", 
            caption:  "Tipo de Pos",
            width: "auto"
        },
        {
            dataField: "cantterminalesasig", 
            caption:  "Cantidad de Pos",
            width: "auto"
        },

        {
            dataField: "tipolinea", 
            caption:  "Tipo de Línea",
            width: "auto"
        }    
      ],

      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"

            },
              ],
          },
      paging: { pageSize: 10 },
      showBorders: true,
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
     
    };
    },
    ); 
}]);

var AppReporteEncuestaPreguntaa = angular.module('AppReporteEncuestaPreguntaa', ['dx']);
   AppReporteEncuestaPreguntaa.service('informacion', ['$http',
    function($http) {
       var vm=this;
       vm.fdatos = { 
         clientes: document.getElementById('clientes').value,
         fechainicial: document.getElementById('fechainicial').value,
         fechafinal: document.getElementById('fechafinal').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
AppReporteEncuestaPreguntaa.controller('ReporteEncuestaPreguntaa',['$scope','informacion',  function ReporteEncuestaPreguntaa($scope,informacion) {
  informacion.postData('api/selectReporteEncuestaPreguntasa')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      groupPanel: {
          visible: true
      },

     "export": {
          enabled: true,
          fileName: "ReporteEncuestaPregunta",
          allowExportSelectedData: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
         {
            dataField: "banco",
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "preguntas",
            caption:  "Preguntas",
            width: "auto"
        },
        {
            dataField: "si", 
            caption:  "Respuesta SI ",
            alignment: "center",
            width: "auto"
        },
         {
            dataField: "porcentaje",
            caption:  "Porcentaje",
            width: "auto"
        },
        {
            dataField: "nou", 
            caption:  "Respuesta NO",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajeno", 
            caption:  "Porcentaje",
            width: "auto"
        },
        {
            dataField: "nc", 
            caption:  "No sabe/No contesta",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajenc", 
            caption:  "Porcentaje",
            width: "auto"
        }
      ],
      
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
      showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var total= document.getElementById('total').value;
      var nclientes= document.getElementById('nclientes').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA DETALLADO '+nclientes+'/ '+total+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var total= document.getElementById('total').value;
    var nclientes= document.getElementById('nclientes').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA DETALLADO", nclientes+'/'+'Total Comercios Consultados: '+ total];
 
  }; 
}]);


  var AppReporteEncuestaPreguntab = angular.module('AppReporteEncuestaPreguntab', ['dx']);
    AppReporteEncuestaPreguntab.service('informacion', ['$http',
      function($http) {
        var vm=this;
        vm.fdatos = { 
          clientes: document.getElementById('clientes').value,
          fechainicial: document.getElementById('fechainicial').value,
          fechafinal: document.getElementById('fechafinal').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
  ]);
AppReporteEncuestaPreguntab.controller('ReporteEncuestaPreguntab',['$scope','informacion',  function ReporteEncuestaPreguntab($scope,informacion) {
  informacion.postData('api/selectReporteEncuestaPreguntasb')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
     "export": {
          enabled: true,
          fileName: "ReporteEncuestaPregunta",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "nbanco",
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "preguntas",
            caption:  "Preguntas",
            width: "auto"
        },
        {
            dataField: "excelente", 
            caption:  "Excelente",
            alignment: "center",
            width: "auto"
        },
         {
            dataField: "porcentajeexc",
            caption:  "Porcentaje",
            width: "auto"
        },
        {
            dataField: "bueno", 
            caption:  "Bueno",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajebueno", 
            caption:  "Porcentaje",
            width: "auto"
        },
        {
            dataField: "regular", 
            caption:  "Regular",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajreg", 
            caption:  "Porcentaje",
            width: "auto"
        },
       {
            dataField: "mal", 
            caption:  "Mal",
            width: "auto"
        },
        {
            dataField: "porcentajemal", 
            caption:  "Porcentaje",
            width: "auto"
        },
        {
            dataField: "nc", 
            caption:  "No sabe/No contesta",
            width: "auto"
        },
        {
            dataField: "porcentajenc", 
            caption:  "Porcentaje",
            width: "auto"
        }
      ],
      summary: {
            totalItems: [
            {
                column: "totalc",
                summaryType: "count"
            },
           ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
      showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var total= document.getElementById('total').value;
      var nclientes= document.getElementById('nclientes').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA DETALLADO '+nclientes+'/ '+total+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var total= document.getElementById('total').value;
    var nclientes= document.getElementById('nclientes').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA DETALLADO", nclientes+'/'+'Total Comercios Consultados: '+ total];
 
  }; 
}]);

var AppReporteEncuestaPreguntac = angular.module('AppReporteEncuestaPreguntac', ['dx']);
    AppReporteEncuestaPreguntac.service('informacion', ['$http',
      function($http) {
        var vm=this;
        vm.fdatos = { 
          clientes: document.getElementById('clientes').value,
          fechainicial: document.getElementById('fechainicial').value,
          fechafinal: document.getElementById('fechafinal').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
  ]);
AppReporteEncuestaPreguntac.controller('ReporteEncuestaPreguntac',['$scope','informacion',  function ReporteEncuestaPreguntac($scope,informacion) {
  informacion.postData('api/selectReporteEncuestaPreguntasc')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
     "export": {
          enabled: true,
          fileName: "ReporteCaptacionEjecutivo",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },
      columns: [
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto"
        },
        {
            dataField: "nbanco",
            caption:  "Banco",
            width: "auto"
        },
        {
            dataField: "preguntas",
            caption:  "Preguntas",
            width: "auto"
        },
        {
            dataField: "recomendacion", 
            caption:  "Recomendación",
            alignment: "center",
            width: "auto"
        },
         {
            dataField: "porcentajerec",
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "redes", 
            caption:  "Redes",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajerd", 
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "vallas", 
            caption:  "Vallas",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajevll", 
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
       {
            dataField: "internet", 
            caption:  "Internet",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajeinter", 
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "nc", 
            caption:  "No sabe/No contesta",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "porcentajenc", 
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        }
      ],
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
    showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var total= document.getElementById('total').value;
      var nclientes= document.getElementById('nclientes').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA DETALLADO '+nclientes+'/ '+total+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var total= document.getElementById('total').value;
    var nclientes= document.getElementById('nclientes').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA DETALLADO", nclientes+'/'+'Total Comercios Consultados: '+ total];
 
  }; 
}]);

var AppReporteEncuestaEstados = angular.module('AppReporteEncuestaEstados', ['dx']);
   AppReporteEncuestaEstados.service('informacion', ['$http',
    function($http) {
       var vm=this;
       vm.fdatos = { 
         clientes: document.getElementById('clientes').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
AppReporteEncuestaEstados.controller('ReporteEncuestaEstados',['$scope','informacion',  function ReporteEncuestaEstados($scope,informacion) {
  informacion.postData('api/SelectReporteEncuestaEstados')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteEncuestaPregunta",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        
        {
            dataField: "contencuestado",
            caption:  "N° Encuestado",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "encuestado", 
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"        
            
        },
         {
            dataField: "contnoencuestado",
            caption:  "N° NO Encuestado",
            alignment: "center",
            width: "auto"
        },
         {
            dataField: "noencuestado",
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "contllamarluego",
            caption:  "N° Llamar Luego",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "llamarluego",
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "contencuestarechazada",
            caption:  "N° Encuesta Rechazada",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "encuestarechazada",
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        }
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            },
           ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
    showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var total= document.getElementById('total').value;
      var nclientes= document.getElementById('nbanco').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA ESTATUS '+nclientes+'/ '+total+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var total= document.getElementById('total').value;
    var nclientes= document.getElementById('nbanco').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA ESTATUS", nclientes+'/'+'Total Comercios Consultados: '+ total];
 
  }; 
}]);
var AppReporteEncuestaEstadosTB = angular.module('AppReporteEncuestaEstadosTB', ['dx']);
   AppReporteEncuestaEstadosTB.service('informacion', ['$http',
    function($http) {
       var vm=this;
       vm.fdatos = { 
         cliente: document.getElementById('cliente').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
AppReporteEncuestaEstadosTB.controller('ReporteEncuestaEstadosTB',['$scope','informacion',  function ReporteEncuestaEstadosTB($scope,informacion) {
  informacion.postData('api/SelectReporteEncuestaEstadosTB')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteEncuestaEstatus",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "nbanco",
            caption:  "Banco",
            width: "auto"
        },

        {
            dataField: "contencuestado",
            caption:  "N° Encuestado",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "encuestado", 
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"        
            
        },
         {
            dataField: "contnoencuestado",
            caption:  "N° NO Encuestado",
            alignment: "center",
            width: "auto"
        },
         {
            dataField: "noencuestado",
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "contllamarluego",
            caption:  "N° Llamar Luego",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "llamarluego",
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "contencuestarechazada",
            caption:  "N° Encuesta Rechazada",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "encuestarechazada",
            caption:  "Porcentaje",
            alignment: "center",
            width: "auto"
        }
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            },
           ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
    showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var total= document.getElementById('totaltb').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA ESTATUS TODOS LOS BANCOS / '+total+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var total= document.getElementById('totaltb').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA ESTATUS", 'TODOS LOS BANCOS/'+'Total Comercios Consultados: '+ total];
 
  }; 
}]);


var AppReporteEncuestaRegistro = angular.module('AppReporteEncuestaRegistro', ['dx']);
   AppReporteEncuestaRegistro.service('informacion', ['$http',
    function($http) {
       var vm=this;
       vm.fdatos = { 
        clientes: document.getElementById('clientes').value,
        fechainicial: document.getElementById('fechainicial').value,
        fechafinal: document.getElementById('fechafinal').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
AppReporteEncuestaRegistro.controller('ReporteEncuestaRegistros',['$scope','informacion',  function ReporteEncuestaRegistros($scope,informacion) {
  informacion.postData('api/SelectReporteEncuestaRegistros')
    .then(function(datos) {
      var someUrl="?cargar=DatosEncuestaReporte&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteEncuestaPregunta",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        {
          dataField: 'idcliente',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },   
        {
            dataField: "idcliente", 
            caption:  "ID",
            alignment: "center",
            width: "auto"        
            
        },
         {
            dataField: "rifcliente", 
            caption:  "RIF",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "rsocial", 
            caption:  "RAZON SOCIAL",
            width: "auto"
        },
         {
            dataField: "rlegal",
            caption:  "Representante Legal",
            width: "auto"
        },
        {
            dataField: "pcontac", 
            caption:  "Persona Contacto",
            alignment: "center",
            width: "auto"
        },
        {
            dataField: "estatus", 
            caption:  "Estatus",
            width: "auto"
        },
        {
            dataField: "ejecutivo", 
            caption:  "Ejecutivo",
            width: "auto"
        },
        {
            dataField: "fecha", 
            caption:  "Fecha",
            width: "auto"
        }
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            },
           ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      },
    showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var nclientes= document.getElementById('nclientes').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA DETALLADO '+nclientes+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var nclientes= document.getElementById('nclientes').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA DETALLADO", nclientes];
 
  }; 
}]);

var AppReporteEncuestaRegistrodet = angular.module('AppReporteEncuestaRegistrodet', ['dx']);
AppReporteEncuestaRegistrodet.service('informacion', ['$http',
  function($http) {
    var vm=this;
  
    vm.fdatos = { 
        clientes: document.getElementById('clientes').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteEncuestaRegistrodet.controller('ReporteEncuestaRegistrosdet',['$scope','informacion',  function ReporteEncuestaRegistrosdet($scope,informacion) {
  informacion.postData('api/SelectReporteEncuestaRegistrosdetalle')
    .then(function(datos) {
      
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 

        "export": {
          enabled: true,
           fileName: "ReporteEncuestaPreguntaDetalle",
          allowExportSelectedData: true
      },
      columns: [

        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        {
            dataField: "pregunta1", 
            caption:  "Preguntas",
            width: "auto"
        },
          {
            dataField: "respuesta1", 
            caption:  "Respuestas",
            width: "auto"
        },
         {
            dataField: "respuesta2", 
            caption:  "Respuestas",
            width: "auto"
        }
          
      ],

      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"

            },
          ],
        },
      paging: { pageSize: 13 },
      showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var nombrebanco= document.getElementById('nbanco').value;
      var rlegal= document.getElementById('rif').value;
      var fecha= document.getElementById('fechaenc').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA DETALLADO '+nombrebanco+'/ '+rlegal+'/ '+fecha+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var nombrebanco= document.getElementById('nbanco').value;
    var rlegal= document.getElementById('rlegal').value;
    var fecha= document.getElementById('fechaenc').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA DETALLADO", nombrebanco+'/ '+rlegal+'/ '+fecha];
 
  }; 
}]);


var AppReporteEncuestaRegistroRif = angular.module('AppReporteEncuestaRegistroRif', ['dx']);
AppReporteEncuestaRegistroRif.service('informacion', ['$http',
  function($http) {
    var vm=this;
  
    vm.fdatos = { 
        rif: document.getElementById('nrorif').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

AppReporteEncuestaRegistroRif.controller('ReporteEncuestaRegistrosRif',['$scope','informacion',  function ReporteEncuestaRegistrosRif($scope,informacion) {
  informacion.postData('api/SelectReporteEncuestaRegistrosRif')
    .then(function(datos) {
      
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },

        "export": {
          enabled: true,
          
          allowExportSelectedData: true
      }, 
      columns: [

        {
            dataField: "correlativo",
            caption:  "Consecutivo",
            width: "auto"
        },
        {
            dataField: "pregunta1", 
            caption:  "Preguntas",
            width: "auto"
        },
          {
            dataField: "respuesta1", 
            caption:  "Respuestas",
            width: "auto"
        },
         {
            dataField: "respuesta2", 
            caption:  "Respuestas",
            width: "auto"
        }
          
      ],

      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"

            },
              ],
          },
      paging: { pageSize: 13 },
      showBorders: true,
      onExporting: function(e) {
      var workbook = new ExcelJS.Workbook();    
      var worksheet = workbook.addWorksheet('Main sheet');
      var nombrebanco= document.getElementById('nbanco').value;
      var rsocial= document.getElementById('rsocial').value;
      var rif= document.getElementById('nrorif').value;
      var fecha= document.getElementById('fechaenc').value;
      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet: worksheet,
        topLeftCell: { row: 7, column: 1 },
        customizeCell: function(options) {
          var gridCell = options.gridCell;
          var excelCell = options.cell;
        }
      }).then(function(dataGridRange) {  
        customizeHeader(worksheet);
        return Promise.resolve();
      }).then(function() {
        workbook.xlsx.writeBuffer().then(function(buffer) {
          saveAs(new Blob([buffer], { type: "application/octet-stream" }), 'REPORTE ENCUESTA DETALLADO '+nombrebanco+'/ '+rsocial+'/ '+fecha+'.xlsx');
        });
      });
      e.cancel = true;
    },
 
    };
    },
    );
    function customizeHeader(worksheet){
    var nombrebanco= document.getElementById('nbanco').value;
    var rsocial= document.getElementById('rsocial').value;
    var rif= document.getElementById('nrorif').value;
    var fecha= document.getElementById('fechaenc').value;
    var generalStyles = { 

      font: { bold: true },
      fill: { type: 'pattern', pattern:'solid', fgColor:{argb:'D3D3D3'}, bgColor:{argb:'D3D3D3'}},
      alignment: { horizontal: 'center' }
    };
    for(var columnIndex = 2; columnIndex < 4; columnIndex++){
      worksheet.getColumn(columnIndex).width = 15;
    }
    for(var columnIndex = 4; columnIndex < 7; columnIndex++){
      worksheet.getColumn(columnIndex).width = 20;
    }
    for(var rowIndex = 1; rowIndex < 6; rowIndex++){
      worksheet.mergeCells(rowIndex, 1, rowIndex, 10);
      //worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
     // Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
    }
    worksheet.getRow(2).height = 20;
    worksheet.getColumn(3).width = 25;
    worksheet.getRow(2).getCell(1).font = {  bold: true, size: 16 };
    worksheet.getRow(7).getCell(1).font =  {  bold: true };
    worksheet.getRow(7).getCell(2).font =  {  bold: true };
    worksheet.getRow(7).getCell(3).font =  {  bold: true };
    worksheet.getRow(7).getCell(4).font =  {  bold: true };
    worksheet.getRow(7).getCell(5).font =  {  bold: true };
    worksheet.getRow(7).getCell(6).font =  {  bold: true };
    worksheet.getRow(7).getCell(7).font =  {  bold: true };     
    worksheet.getRow(7).getCell(8).font =  {  bold: true };
    worksheet.getRow(7).getCell(9).font =  {  bold: true };
    worksheet.getRow(7).getCell(10).font = {  bold: true };
    worksheet.getRow(1).getCell(3).numFmt = "d mmmm yyyy";                 
    //worksheet.getRow(1).getCell(3).font = {  bold: true, size: 16 };
    worksheet.getColumn(1).values = [  "","REPORTE ENCUESTA DETALLADO", nombrebanco+'/ '+rsocial+'/ '+rif+'/ '+fecha];
 
  }; 
}]);


 var AppPersonasRef = angular.module('AppPersonasRef', ['dx']);
AppPersonasRef.service('informacion', ['$http',
  function($http) {
    var vm=this;
    
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);


AppPersonasRef.controller('PersonasRef',['$scope','informacion',  function PersonasRef($scope,informacion) {
  informacion.postData('api/Selectbuscarperaonasref')
    .then(function(datos) {
      var someUrl="CaptacionCliente?cargar=iniciocaptacioncliente&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        },

        "export": {
          enabled: true,
          
          allowExportSelectedData: true
      },  
      columns: [
        
        {
            dataField: "correlativo",
            caption: "Consecutivo",
            width: "auto",
            alignment: "center"
        },
            {
          dataField: 'idclientes',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Detalles </a>')
               .attr('href', someUrl + options.value)
               .appendTo(container);
            }  
        },   
        {
            dataField: "estatus",
            caption:  "Estatus",
            width: "auto", 
             // cssClass: "enlace",

       cellTemplate: function (container, options) {
       var x = options.value;
         if(x=='1'){
         $('<td>'+ 'Contactado' +'</td>')
            .css('color','white') 
            .css('background','#01DF3A')
            .css('text-align','center')
            .css('border-radius','0.2em')
            .css('padding','.3em')
            .appendTo(container);

          } else if(x=='2'){
          $('<td>'+ 'No Contactado' +'</td>')
          .css('color','#f1f1f1')
          .css('background','orange')
          .css('text-align','center')
          .css('border-radius','0.2em')
          .css('padding','.3em')
          .appendTo(container);
          } 

        }          
            },
         {
            dataField: "nombreref",
            caption:  "Nombre REFERIDO",
            dataType: "date",
            width: "auto"
        },
        {
            dataField: "numeroref", 
            caption:  "Teléfono REFERIDO",
            width: "auto"
        },
        {
            dataField: "razonsocialenc", 
            caption:  "Razón Social(CLIENTE)",
            width: "auto"
        }, 
        {
            dataField: "rifencuestado", 
            caption:  "RIF (CLIENTE)",
            width: "auto"
        },
        {
            dataField: "nombreenc", 
            caption:  "Representante Legal(CLIENTE)",
            width: "auto"
        }, 
        {
            dataField: "tlfenc2", 
            caption:  "Teléfono (CLIENTE)",
            width: "auto"
        } 
      
      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            }]
        },
      paging: { pageSize: 10 },
       pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },
    ); 
}]);

var AppReporteHistorialLlamadas = angular.module('AppReporteHistorialLlamadas', ['dx']);
   AppReporteHistorialLlamadas.service('informacion', ['$http',
    function($http) {
       var vm=this;
       vm.fdatos = { 
         idcliente: document.getElementById('idcliente').value};
        this.promiseCache = null;
        this.postData = function(url) {
           return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
        }
      }
    ]);
AppReporteHistorialLlamadas.controller('ReporteHistorialLlamadas',['$scope','informacion',  function ReporteHistorialLlamadas($scope,informacion) {
  informacion.postData('api/SelectReporteHistorialLlamadas')
    .then(function(datos) {
      // var someUrl="?cargar=editar&var=";
      $scope.datos = datos.data
      
      $scope.gridOptions = {
      dataSource:$scope.datos,
              selection: {
          mode: "multiple"
      },
      "export": {
          enabled: true,
          fileName: "ReporteHistorialLlamadas",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "rlegal",
            caption:  "Representante Legal",
            alignment: "center",
            width: "200px"
        },
        {
            dataField: "persona_contacto",
            caption:  "Persona Contacto",
            alignment: "center",
            width: "200px"
        },
        {
            dataField: "usuario_contacto", 
            caption:  "Ejecutivo",
            alignment: "center",
            width: "155px"        
            
        },
         {
            dataField: "fecha_contacto",
            caption:  "Fecha llamada",
            alignment: "center",
            width: "125px"
        },
        {
            dataField: "hora_contacto",
            caption:  "Hora llamada",
            alignment: "center",
            width: "125px"
        },
        {
            dataField: "descripcion_estatus",
            caption:  "Estatus",
            alignment: "center",
            width: "155px"
        }

      ],
      summary: {
            totalItems: [
            {
                column: "correlativo",
                summaryType: "count"
            },
           ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },
    function (error){
      console.log(error, 'Cliente sin llamadas realizadas');
  }); 
}]);


var Appreporteestatusgestiones = angular.module('Appreporteestatusgestiones', ['dx']);
Appreporteestatusgestiones.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatusgestiones.controller('Controllerreporteestatusgestiones',['$scope','informacion',  function Controllerreporteestatusgestiones($scope,informacion) {
  informacion.postData('api/selectReporteGestiones')
    .then(function(datos) {

      $scope.datos = datos.data
      var someUrl="?cargar=reportedetalladogestiones&var=";  
      //var url = "api/selectGestionMantenimiento.php";
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus Gestiones",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
            {
            dataField: "enlace",
            caption: "Acción",
            width: "auto",

              cellTemplate: function (container, options) {
              var x = options.value;
               l= x.split('/');
                      $('<a>'+ 'Revisar Gestiones' +'</a>')
                      .attr('href', someUrl + options.value)
                      .appendTo(container);
               /* $('<a>'+l[1] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);*/


                 }
        },
         {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "razonsocial",
            caption: "Razón Social",
            width: "auto"
        },

        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "estatus",
            caption: "Estatus del Equipo",
            width: "auto"
        },
         {
            dataField: "serial",
            caption: "Serial Actual del Equipo",
            width: "auto"
        },
         {
            dataField: "serialmifi",
            caption: "Mifi Actual",
            width: "auto"
        },

        {
            dataField: "sim",
            caption: "Simcard Actual",
            width: "auto"
        }        
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var Appreporteestatusgestionesrif = angular.module('Appreporteestatusgestionesrif', ['dx']);
Appreporteestatusgestionesrif.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {nrif: document.getElementById('nrif').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteestatusgestionesrif.controller('Controllerreporteestatusgestionesrif',['$scope','informacion',  function Controllerreporteestatusgestionesrif($scope,informacion) {
  informacion.postData('api/selectReporteGestionesRIF')
    .then(function(datos) {

      $scope.datos = datos.data
      var someUrl="?cargar=reportedetalladogestiones&var=";  
      //var url = "api/selectGestionMantenimiento.php";
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus Gestiones",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
            {
            dataField: "enlace",
            caption: "Acción",
            width: "auto",

              cellTemplate: function (container, options) {
              var x = options.value;
               l= x.split('/');
                      $('<a>'+ 'Revisar Gestiones' +'</a>')
                      .attr('href', someUrl + options.value)
                      .appendTo(container);
               /* $('<a>'+l[1] +'</a>')
                .attr('href', someUrl + options.value)
                .appendTo(container);*/


                 }
        },
         {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },

         {
            dataField: "razonsocial",
            caption: "Razón Social",
            width: "auto"
        },

        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "estatus",
            caption: "Estatus del Equipo",
            width: "auto"
        },
         {
            dataField: "serial",
            caption: "Serial Actual del Equipo",
            width: "auto"
        },
         {
            dataField: "serialmifi",
            caption: "Mifi Actual",
            width: "auto"
        },

        {
            dataField: "sim",
            caption: "Simcard Actual",
            width: "auto"
        }        
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);


var AppClientesProfit = angular.module('AppClientesProfit', ['dx']);
AppClientesProfit.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {clientes: document.getElementById('clientes').value,
                 fechainicial: document.getElementById('fechainicial').value,
                 fechafinal: document.getElementById('fechafinal').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    AppClientesProfit.controller('ControllerClientesProfit',['$scope','informacion',  function ControllerClientesProfit($scope,informacion) {
  informacion.postData('api/SelectReporteClientesProfit')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Data Clientes Profit",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "documento",
            caption: "co_cli",
            width: "auto"
        },

         {
            dataField: "razonsocial",
            caption: "cli_des",
            width: "auto"
        },

        {
            dataField: "direccionenvio",
            caption: "direc1",
            width: "auto"
        },

         {
            dataField: "tlf1",
            caption: "telefonos",
            width: "auto"
        },
        {
            dataField: "representante",
            caption: "respons",
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption: "fecha_reg",
            width: "auto"
        },
         {
            dataField: "documento2",
            caption: "rif",
            width: "auto"
        },

        {
            dataField: "correo",
            caption: "email",
            width: "auto"
 }
        
      ],
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var AppPlantillaProfit = angular.module('AppPlantillaProfit', ['dx']);
AppPlantillaProfit.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {clientes: document.getElementById('clientes').value,
                 fechainicial: document.getElementById('fechainicial').value,
                 fechafinal: document.getElementById('fechafinal').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    AppPlantillaProfit.controller('ControllerPlantillaProfit',['$scope','informacion',  function ControllerPlantillaProfit($scope,informacion) {
  informacion.postData('api/SelectReportePlantillaProfit')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Data Plantilla Profit",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [

        {
            dataField: "afiliado_term",
            caption: "descrip",
            width: "auto"
        },

         {
            dataField: "documento",
            caption: "co_cli",
            width: "auto"
        },

        {
            dataField: "codbanco",
            caption: "co_tran",
            width: "auto"
        },

         {
            dataField: "fechainst1",
            caption: "fec_emis",
            width: "auto"
        },
        {
            dataField: "fechainst2",
            caption: "fec_venc",
            width: "auto"
        },
        {
            dataField: "fechainst3",
            caption: "fec_reg",
            width: "auto"
        },
         {
            dataField: "cuenta_banco",
            caption: "Nº Cuenta",
            width: "auto"
        },
        

        {
            dataField: "serialcomplet",
            caption: "campo3",
            width: "auto"
        },
        {
            dataField: "modalidad",
            caption: "codigo",
            width: "auto"
        },
        /*{
            dataField: "cod_servicio",
            caption: "codigo",
            width: "auto"
        },*/
        {
            dataField: "monto",
            caption: "total_bruto",
            width: "auto"
        },
        {
            dataField: "iva",
            caption: "monto_imp",
            width: "auto"
        },
        {
            dataField: "total_neto",
            caption: "total_neto",
            width: "auto"
        },
        {
            dataField: "id_adm",
            caption: "ID",
            width: "auto"
        }
                
        
      ],
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var Appreporteinstalados = angular.module('Appreporteinstalados', ['dx']);
Appreporteinstalados.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value,
                 fechainicial: document.getElementById('fechainicial').value,
                 fechafinal: document.getElementById('fechafinal').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteinstalados.controller('Controllerreporteinstalados',['$scope','informacion',  function Controllerreporteinstalados($scope,informacion) {
  informacion.postData('api/selectReporteEstatusInstalados')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Equipos Instalados",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
        {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },
        {
            dataField: "documento",
            caption: "RIF",
            width: "auto"
        },
        {
            dataField: "afiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "representante",
            caption: "Representante Legal",
            width: "auto"
        },
        {
            dataField: "correo",
            caption: "Correo Electrónico",
            width: "auto"
        },

        {
            dataField: "tlf1",
            caption: "Teléfono 1",
            width: "auto"
        },
        {
            dataField: "tlf2",
            caption: "Teléfono 2",
            width: "auto"
        },
        {
            dataField: "ejecut",
            caption: "Ejecutivo",
            width: "auto"
        },

        {
            dataField: "tipopos",
            caption: "Tipo de Equipo",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Módelo de Equipo",
            width: "auto"
        },

         {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },         
        
        {
            dataField: "factura",
            caption: "N° Factura",
            width: "auto"
        }, 
        {
            dataField: "serial",
            caption: "Serial del Equipo",
            width: "auto"
        },
        {
            dataField: "sim",
            caption: "Serial SIM",
            width: "auto"
        },
        {
            dataField: "serialmifi",
            caption: "Serial MIFI",
            width: "auto"
        },
        {
            dataField: "terminal",
            caption: "N° Terminal",
            width: "auto"
        },
        {
            dataField: "estatus",
            caption: "Estatus del Equipo",
            width: "auto"
        },          
        {
            dataField: "fechainst",
            caption: "Fecha Instalación",
            width: "auto"
        },
        {
            dataField: "direccionenvio",
            caption: "Dirección",
            width: "auto"
        },
                
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);



var Appreportevisitatecnica = angular.module('Appreportevisitatecnica', ['dx']);
Appreportevisitatecnica.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreportevisitatecnica.controller('Controllerreportevisitatecnica',['$scope','informacion',  function Controllerreportevisitatecnica($scope,informacion) {
  informacion.postData('api/selectReporteVisitaTecnica')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Equipos Instalados",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
        {
            dataField: "cliente",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "descresult",
            caption: "Resultado de la Visita",
            width: "auto"
        },
        {
            dataField: "desc_visita",
            caption: "Estatus de Visita",
            width: "auto"
        },

         {
            dataField: "razonsocial",
            caption: "Razón Social",
            width: "auto"
        },
        {
            dataField: "documento",
            caption: "RIF ó Cédula",
            width: "auto"
        },
         {
            dataField: "afiliado",
            caption: "Afiliado",
            width: "auto"
        },

        {
            dataField: "representante",
            caption: "Representante Legal",
            width: "auto"
        },
        {
            dataField: "correo",
            caption: "Correo Electrónico",
            width: "auto"
        },
        {
            dataField: "tlf1",
            caption: "Teléfono Principal",
            width: "auto"
        },

        {
            dataField: "ejecut",
            caption: "Ejecutivo",
            width: "auto"
        },
        {
            dataField: "tipopos",
            caption: "Tipo de Equipo",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Módelo de Equipo",
            width: "auto"
        },
        {
            dataField: "ibpbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "factura",
            caption: "N° Factura",
            width: "auto"
        }, 
         {
            dataField: "serial",
            caption: "Serial del Equipo",
            width: "auto"
        },
         {
            dataField: "sim",
            caption: "Serial del Equipo",
            width: "auto"
        },
         {
            dataField: "serialmifi",
            caption: "Serial del Equipo",
            width: "auto"
        },
        {
            dataField: "terminal",
            caption: "Nº Terminal",
            width: "auto"
        },
        {
            dataField: "estatus",
            caption: "Estatus del Equipo",
            width: "auto"
        },
        {
            dataField: "fechainst",
            caption: "Fecha Instalación",
            width: "auto"
        },       
       
        {
            dataField: "fechagest",
            caption: "Fecha Gestión",
            width: "auto"
        },
        {
            dataField: "fechavisit",
            caption: "Fecha Visita",
            width: "auto"
        },    
        {
            dataField: "desc_zona",
            caption: "Tipo Zona",
            width: "auto"
        },      
        {
            dataField: "tecniconame",
            caption: "Nombre del Técnico",
            width: "auto"
        },  
        {
            dataField: "estado",
            caption: "Estado",
            width: "auto"
        },      
        
        {
            dataField: "idsecuencial",
            caption: "idsecuencial",
            width: "auto"
        },
                
        
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

var Appreporteriesgomedido = angular.module('Appreporteriesgomedido', ['dx']);
Appreporteriesgomedido.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {regiones: document.getElementById('regiones').value,
    banco: document.getElementById('banco').value,
    usuario: document.getElementById('usuario').value,
    ejecutivoin: document.getElementById('ejecutivoin').value,
    tipousuario: document.getElementById('tipousuario').value,
    coordinador: document.getElementById('coordinador').value,
    coordinadordos: document.getElementById('coordinadordos').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreporteriesgomedido.controller('Controllerreporteriesgomedido',['$scope','informacion',  function Controllerreporteriesgomedido($scope,informacion) {
  informacion.postData('api/selectReporteRiesgoMedido')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;*/
      var someUrl="riesgomedido?cargar=mostrarinfopagos&var=";
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Riesgo Medido"
          //allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
        {
            dataField: "consecutivo",
            caption: "ID",
            width: "auto"
        },
        {
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },
        {
            dataField: "cantposconf",
            caption: "Cantidad de POS Confirmados",
            width: "auto"
        },
         {
            dataField: "nbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "ejec",
            caption: "Ejecutivo",
            width: "auto"
        },
         {
            dataField: "modeloposs",
            caption: "Modelo POS",
            width: "auto"
        },

        {
            dataField: "montorecibido",
            caption: "Monto Inicial",
            width: "auto"
        },
        {
            dataField: "valordolar",
            caption: "Precio del Equipo",
            width: "auto"
        },

        {
            dataField: "riesgomedido",
            caption: "Riesgo Medido",
            width: "auto"
        },
        {
            dataField: "modalidad",
            caption: "Modalidad de Venta",
            width: "auto"
        },
        {
            dataField: "fechacarga",
            caption: "Fecha Carga al Sistema",
            width: "auto"
        },
        {
            dataField: "negociacion",
            caption: "Negociación",
            width: "auto"
        }, 
        {
            dataField: "coordaprobado",
            caption: "Aprobado por",
            width: "auto"
        },
        {
            dataField: "fechaparobacion",
            caption: "Fecha de Aprobación",
            width: "auto"
        },
        {
            dataField: "region_ejec",
            caption: "Cobro Central",
            width: "auto"
        },          
        {
            dataField: "montopagado",
            caption: "Monto Pagado",
            width: "auto"
        },
        {
            dataField: "fechapago",
            caption: "Fecha de Pago",
            width: "auto"
        },
        {
            dataField: "saldodeudor",
            caption: "Saldo Deudor",
            width: "auto"
        },
        {
            dataField: "observacionpago",
            caption: "Observación",
            width: "auto"
        },
        {
          dataField: 'consecutivo',
          caption:  "Información Pagos",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ver Cuotas</a>')
               .attr('href', someUrl+options.value)
               .appendTo(container);
            }  
        },
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

var Appreportecargaparametros = angular.module('Appreportecargaparametros', ['dx']);
Appreportecargaparametros.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {banco: document.getElementById('banco').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appreportecargaparametros.controller('Controllerreportecargaparametros',['$scope','informacion',  function Controllerreportecargaparametros($scope,informacion) {
  informacion.postData('api/SelectReporteCargaParametros')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus General",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "fechainstalacion",
            caption: "Fecha",
            width: "auto"
        },
        {
            dataField: "factura",
            caption: "N° Factura",
            width: "auto"
        },
          {
            dataField: "sec",
            caption: "N°",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "nbanco",
            caption: "Banco",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Modelo POS",
            width: "auto"
        },
        {
            dataField: "tipopos",
            caption: "Tipo de Comunicacion",
            width: "auto"
        },
        {
            dataField: "tipolinea",
            caption: "Operadora",
            width: "auto"
        },
        {
            dataField: "serialsustitucion",
            caption: "Serial Saliente",
            width: "auto"
        },
        {
            dataField: "serialp",
            caption: "Serial Entrante",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption: "Nombre del Comercio",
            width: "auto"
        },
        {
            dataField: "numafiliado",
            caption: "Afiliado",
            width: "auto"
        },
        {
            dataField: "terminal",
            caption: "N° term",
            width: "auto"
        },
        {
            dataField: "coddocumento",
            caption: "RIF",
            width: "auto"
        },
        {
            dataField: "aplicativo",
            caption: "Versión Aplicativo",
            width: "auto"
        },
        {
            dataField: "tipogestion",
            caption: "Tipo de Gestión",
            width: "auto"
        },
         

        {
            dataField: "observaciones",
            caption: "Observaciones",
            width: "auto"
        },
        {
            dataField: "estatusequipo",
            caption: "Estatus del Equipo",
            width: "auto"
        },
        {
            dataField: "ejecregion",
            caption: "Región Asignada",
            width: "auto"
        },
        {
            dataField: "fechaparametros",
            caption: "Fecha Carga Parametros",
            width: "auto"
        },
        {
            dataField: "estatusparametrizacion",
            caption: "Estatus Parametrización",
            width: "auto"
        },
        {
            dataField: "modventa",
            caption: "Planes de Financiamiento",
            width: "auto"
        },
      ],
       summary: {
            totalItems: [
            {
                column: "sec",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);

var Appvisualizarcuotas = angular.module('Appvisualizarcuotas', ['dx']);
Appvisualizarcuotas.service('informacion', ['$http',
  function($http) {
    var vm=this;
    vm.fdatos = {id_afiliado: document.getElementById('id_afiliado').value};
    this.promiseCache = null;
    this.postData = function(url) {
       return this.promiseCache || (this.promiseCache = $http.post(url, vm.fdatos));
    }
  }
]);

    Appvisualizarcuotas.controller('Controllervisualizarcuotas',['$scope','informacion',  function Controllervisualizarcuotas($scope,informacion) {
  informacion.postData('api/selectVistaPagos')
    .then(function(datos) {
   /*   var tipousuario= document.getElementById('tipousuario').value;
      var ejecutivo= document.getElementById('ejecutivo').value;
      var usuario= document.getElementById('usuario').value;
      var someUrl="facturacionpos?cargar=facturarcliente&var=";*/
      $scope.datos = datos.data
      $scope.gridOptions = {
      dataSource:$scope.datos,
      "export": {
          enabled: true,
          fileName: "Reporte Estatus General",
          allowExportSelectedData: true
      },
      groupPanel: {
          visible: true
      },
       selection: {
          mode: "multiple"
      },
       searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Buscar..."
        }, 
      columns: [
        {
            dataField: "correlativo",
            caption: "N° Pago",
            width: "auto"
        },
          {
            dataField: "id",
            caption: "ID",
            width: "auto"
        }, 
       /* {
          dataField: 'enlace',
          caption:  "Acción",
          width: "auto",
           cellTemplate: function (container, options) {
             $('<a> Ir a Registro</a>')
               .attr('href', someUrl + options.value + '/' + tipousuario + '/' + ejecutivo + '/' + usuario + '/' + 'R')
               .appendTo(container);
            }  
        },*/ 
        {
            dataField: "rif",
            caption: "RIF",
            width: "auto"
        },
        {
            dataField: "marcapos",
            caption: "Modelo POS",
            width: "auto"
        },
        {
            dataField: "razonsocial",
            caption: "Razon Social",
            width: "auto"
        },
        {
            dataField: "fechapago",
            caption: "Fecha Pago",
            width: "auto"
        },
        {
            dataField: "estatus",
            caption: "Estatus Aprobación",
            width: "auto"
        },
        {
            dataField: "aprobacion",
            caption: "Estatus Confirmación",
            width: "auto"
        },
        {
            dataField: "nombrebancorigen",
            caption: "Banco Origen",
            width: "auto"
        },
        {
            dataField: "nombrebancdestino",
            caption: "Banco Destino",
            width: "auto"
        },
        {
            dataField: "",
            caption: "Forma Pago",
            width: "auto"
        },
        {
            dataField: "tipomoneda",
            caption: "Moneda",
            width: "auto"
        },
        {
            dataField: "montousd",
            caption: "Monto USD",
            width: "auto"
        },
         
        {
            dataField: "factorconversion",
            caption: "Factor de conversión",
            width: "auto"
        },
        {
            dataField: "monto",
            caption: "Monto Bs",
            width: "auto"
        },
        {
            dataField: "referencia",
            caption: "Referencia",
            width: "auto"
        },
        {
            dataField: "nombredepositante",
            caption: "Nombre Depositante",
            width: "auto"
        },
        {
            dataField: "observacioncomer",
            caption: "Observación Comercialización",
            width: "auto"
        },
        {
            dataField: "observacionadm",
            caption: "Observación Administración",
            width: "auto"
        },
        
      ],
       summary: {
            totalItems: [
            {
                column: "id",
                summaryType: "count"
            }
            ]
          },
      paging: { pageSize: 10 },
      pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50]
        },
      showBorders: true,
      columnChooser: {
          enabled: true,
          height: 180,
          width: 400,
          emptyPanelText: 'Ocultar columnas'
      }
    };
    },function (error){
      console.log(error, 'No puede obtener datos.');
  }); 
}]);