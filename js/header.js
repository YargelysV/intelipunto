
function motivomostrar(){
	document.getElementById('motivo').style.display='block';
	$('#motivos').prop("required", true);
}

function motivoocultar(){
	document.getElementById('motivo').style.display='none';
	$('#motivos').removeAttr("required");
}

function eliminar(usuario){

	alertify.set({ labels: {
		ok: "Eliminar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Eliminar al usuario: "+usuario+"?", function (e) {
		if (e) {
			$.post('view/usuario/eliminar.php',{postusuario:usuario},
				function(data)
				{
//ajaxget();
alertify.success(data);
listausuario('',1,10);
ocultar();
});
//   alertify.success("Usuario eliminado con &Eacute;xito");
} else {
	alertify.error("No se ha eliminado el usuario");
	return false;
}
})
	return false;
}

function reiniciar(usuario){

	alertify.set({ labels: {
		ok: "Reiniciar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Reiniciar la clave del usuario: "+usuario+"?", function (e) {
		if (e) {
			$.post('view/usuario/reiniciar.php',{postusuario:usuario},
				function(data)
				{
					alertify.success(data);
				});
		} else {
			alertify.error("No se ha reiniciado la clave al usuario "+usuario);
			return false;
		}
	})
	return false;
}

function desbloquear(usuario){

	alertify.set({ labels: {
		ok: "Desbloquear",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Desbloquear este usuario: "+usuario+"?", function (e) {
		if (e) {
			$.post('view/usuario/desbloquear.php',{postusuario:usuario},
				function(data)
				{
					alertify.success(data);
					setTimeout(function(){ window.location.reload(0); }, 2000); 
				});
		} else {
			alertify.error("No se ha desbloqueado el usuario "+usuario);
			return false;
		}
	})
	return false;
}
//actualizar un bloque en especifico a traves de ajax
function ajaxget(){
	var conexion;
	if(window.XMLHttpRequest)
	{
		conexion=new XMLHttpRequest();
	}
	else
	{
		conexion=new ActiveXObject("Microsoft.XMLHTTP");
	}
	conexion.onreadystatechange=function()
	{
		if (conexion.readyState==4 && conexion.status==200)
		{
			document.getElementById("recepcion").innerHTML=conexion.responseText;
		}
	}
	conexion.open("GET","home.php",true);
	conexion.send();
}

function soloLetras(e) {
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toString();
letras = "áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere mostrar.
especiales = [8, 37, 39, 46, 6, 32]; //Es la validacion del KeyCodes, que teclas recibe el campo de texto.
tecla_especial = false
for(var i in especiales) {
	if(key == especiales[i]) {
		tecla_especial = true;
		break;
	}
}

if(letras.indexOf(tecla) == -1 && !tecla_especial){
	return false;
}
}

function soloLetras1(e) {
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toString(); 
letras = "áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere mostrar.
especiales = [8, 37, 39, 46, 6,32]; //Es la validacion del KeyCodes, que teclas recibe el campo de texto.
tecla_especial = false
for(var i in especiales) {
	if(key == especiales[i]) {
		tecla_especial = true;
		break;
	}
}

if(letras.indexOf(tecla) == -1 && !tecla_especial){
	return false;
}
}


function soloNumeros(e)
{
	var keynum = window.event ? window.event.keyCode : e.which;
	if ((keynum == 8) || (keynum == 46))
		return true;

	return /\d/.test(String.fromCharCode(keynum));
}

function soloNumeros1(e)
{
	var keynum = window.event ? window.event.keyCode : e.which;
	if ((keynum == 8))
		return true;

	return /\d/.test(String.fromCharCode(keynum));
}

function ocultar(){
	document.getElementById('oculto').style.display = 'none';
}

function ocultarcliente(){
	document.getElementById('ocultocliente').style.display = 'none';
}

function ocultarclientedetalle(){
	document.getElementById('ocultoclientedetalle').style.display = 'none';
}	

function ocultarmod(){
	document.getElementById('ocultomod').style.display = 'none';
}


function validararchivo(){

	var	archivo=document.getElementById('archivo').value;
	var b = archivo.split('.');

	if(document.getElementById('archivo').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO .CSV A CARGAR.!");
		document.getElementById('archivo').focus();
		return false;
	}

	if(b[b.length-1] != 'csv')
	{
		alertify.alert('Error: El archivo debe ser .csv');
		document.getElementById('archivo').focus();
		return false;
	}

	else{
		document.formarchivo.submit();
	}
}

function cargar(){

	alertify.set({ labels: {
		ok: "Cargar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Cargar la informaci&oacute;n?", function (e) {
		if (e) {

			$.post('?cargar=transferencia2',
				function(data)
				{
					ocultarcargar();
					alertify.success('Archivo guardado con &eacute;xito');
					setTimeout(function(){ window.location.reload(1); }, 900);
//	alertify.success(data);

});
		} else {
			alertify.error("No se ha cargado la informaci&oacute;n");
			return false;
		}
	})
	return false;
}


function cargarduplicado(){

	alertify.set({ labels: {
		ok: "Cargar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Cargar la informaci&oacute;n?", function (e) {
		if (e) {

			$.post('?cargar=transferencia2',
				function(data)
				{
					
					alertify.success('Archivo guardado con &eacute;xito');
					setTimeout(function(){ window.location.reload(1); }, 1000);
//	alertify.success(data);

});
		} else {
			alertify.error("No se ha cargado la informaci&oacute;n");
			return false;
		}
	})
	return false;
}

function eliminarduplicadoscargar(){

	alertify.set({ labels: {
		ok: "Cargar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Eliminar los Registros Duplicados y Cargar la Informaci&oacute;n?", function (e) {
		if (e) {

			$.post('?cargar=transferencia3',
				function(data)
				{
					// ocultarcargar();
					alertify.success('Archivo guardado con &eacute;xito');
					setTimeout(function(){ window.location.reload(1); }, 5000);
//	alertify.success(data);

});
		} else {
			alertify.error("No se ha cargado la informaci&oacute;n");
			return false;
		}
	})
	return false;
}

function ocultarcargar(){
	document.getElementById('ocultocargar').style.display = 'none';
}

function mostrarcargar(){
	document.getElementById('ocultocargar').style.display = 'block';}

function restaurar(){
	 if ($('#restaurarocultar').css('display') == 'none') {
          $('#restaurarocultar').css('display', 'block')
          $('#eliminarocultar').css('display', 'none') ;
      }
     
      else {
          $('#restaurarocultar').css('display', 'none')  ;
      }
 }   

 function eliminar_archivo_ClientePotencial(){
	 if ($('#eliminarocultar').css('display') == 'none') {
          $('#eliminarocultar').css('display', 'block')
          $('#restaurarocultar').css('display', 'none') ;
      }
      
      else {
          $('#eliminarocultar').css('display', 'none')  ;
      }
 } 

function errores(){
document.getElementById('errores').style.display = 'block';}


function eliminararchivoseriales(){

	alertify.set({ labels: {
		ok: "Ok",
		cancel: "Cancelar"
		}
	});

alertify.set({ buttonReverse: true });

	if(document.getElementById('fecha').value == "")
	{
		alertify.alert("DEBE SELECCIONAR LA FECHA A ELIMINAR!.");
		document.getElementById('fecha').focus();
		return false;
	}	

alertify.confirm("&#191;Desea Eliminar al archivo?", function (e) {
	if (e) {
		var variable=document.getElementById('fecha').value;
		
		$.post('view/Serie/Seriales/eliminararchivo.php',{postvariable:variable},
		function(data)
		{
		//ajaxget();
		alertify.success(data);
		setTimeout(function(){ window.location.reload(1); }, 2000);
	});
	} else {
		alertify.error("No se ha eliminado el archivo");
		return false;
	}
})
return false;
}

			function eliminararchivocliente(){

				alertify.set({ labels: {
					ok: "Ok",
					cancel: "Cancelar"
				}
			});

				alertify.set({ buttonReverse: true });

				alertify.confirm("&#191;Desea Eliminar al archivo?", function (e) {
					if (e) {

						$.post('view/Administracion/facturacion/eliminararchivo.php',
							function(data)
							{
//ajaxget();
alertify.success(data);
setTimeout(function(){ window.location.reload(1); }, 2000);
});
//alertify.success("Usuario eliminado con &Eacute;xito");
} else {
	alertify.error("No se ha eliminado el archivo");
	return false;
}
})
				return false;
			}


			function eliminararchivoclientePotencial(tipousuario, login){

				alertify.set({ labels: {
					ok: "Ok",
					cancel: "Cancelar"
				}
			});

				alertify.set({ buttonReverse: true });

				if(document.getElementById('clienteaeliminar').value == "")
				{
					alertify.alert("DEBE SELECCIONAR EL BANCO A ELIMINAR!");
					document.getElementById('clienteaeliminar').focus();
					return false;
				}
				if(document.getElementById('fecharaeliminar').value == "")
				{
					alertify.alert("DEBE SELECCIONAR LA FECHA A ELIMINAR!");
					document.getElementById('fecharaeliminar').focus();
					return false;
				}	
				if(document.getElementById('archivoaeliminar').value == "")
				{
					alertify.alert("DEBE SELECCIONAR EL ARCHIVO!");
					document.getElementById('archivoaeliminar').focus();
					return false;
				}

				alertify.confirm("&#191;Desea Eliminar al archivo?", function (e) {
					if (e) {
						var variable=document.getElementById('clienteaeliminar').value+'/'+document.getElementById('fecharaeliminar').value+'/'+document.getElementById('archivoaeliminar').value+'/'+tipousuario+'/'+login;
						$.post('view/ClientePotencial/recepcion/eliminararchivo.php',{postvariable:variable},
							function(data)
							{
//ajaxget();
alertify.success(data);
setTimeout(function(){ window.location.reload(1); }, 2000);
});
//alertify.success("Usuario eliminado con &Eacute;xito");
} else {
	alertify.error("No se ha eliminado el archivo");
	return false;
}
})
				return false;
			}

			function puntos(donde,caracter){
				pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/
				valor = donde.value
				largo = valor.length
				crtr = true
				if(isNaN(caracter) || pat.test(caracter) == true){
					if (pat.test(caracter)==true){
						caracter = '"\"' + caracter
					}
					carcter = new RegExp(caracter,"g")
					valor = valor.replace(carcter,"")
					donde.value = valor
					crtr = false
				}
				else{
					var nums = new Array()
					cont = 0
					for(m=0;m<largo;m++){
						if(valor.charAt(m) == "." || valor.charAt(m) == " ")
							{continue;}
						else{
							nums[cont] = valor.charAt(m)
							cont++
						}
					}
				}
				var cad1="",cad2="",tres=0
				if(largo > 3 && crtr == true){
					for (k=nums.length-1;k>=0;k--){
						cad1 = nums[k]
						cad2 = cad1 + cad2
						tres++
						if((tres%3) == 0){
							if(k!=0){
								cad2 = "." + cad2
							}
						}
					}
					donde.value = cad2
				}
			}
			function validarfechas(){

				var fechadesde=document.getElementById('fechadesde').value;
				var fechahasta=document.getElementById('fechahasta').value;

				var fechad = new Date(fechadesde);
				var aniodesde = fechad.getUTCFullYear();

				var fechah = new Date(fechahasta);
				var aniohasta = fechah.getUTCFullYear();


				if ((aniodesde != aniohasta) && (!isNaN(aniodesde)) && (!isNaN(aniohasta)))
				{
					alertify.error("Debe elegir un inventario del mismo a&ntilde;o");
					document.getElementById('buscar').disabled=true;
				}
				else if((Date.parse(fechadesde)) > (Date.parse(fechahasta))){
					alertify.error("La fecha inicial no puede ser mayor que la fecha final");
					document.getElementById('buscar').disabled=true;
				}
				else
				{
					document.getElementById('buscar').disabled=false;	
				}
			}

			function actualizarmodulo(valor){


				var modulo;
				var nmodulo;
				var usuarioadm;
				var usuario;
				var estatus;
				var codtipoacceso;

				modulo=document.getElementById('modulo'+[valor]).value;
				nmodulo=document.getElementById('nmodulo'+[valor]).value;
				usuarioadm=document.getElementById('usuarioadmi').value;
				usuario=document.getElementById('usuario').value;
				estatus=document.getElementById('activo'+[valor]).value;
				codtipoacceso=document.getElementById('acceso'+[valor]).value;

				var variable=usuario+'/'+modulo+'/'+codtipoacceso+'/'+estatus+'/'+usuarioadm;

				$.post('view/usuario/editarestatusmodulo.php',{postvariable:variable},
					function(data)
					{
//	alertify.success(data);
window.location.reload();

});

				return false;
			}

			function actualizartipoacceso(valor){

				var modulo;
				var nmodulo;
				var usuarioadm;
				var usuario;
				var estatus;
				var codtipoacceso;

				modulo=document.getElementById('modulo'+[valor]).value;
				nmodulo=document.getElementById('nmodulo'+[valor]).value;
				usuarioadm=document.getElementById('usuarioadmi').value;
				usuario=document.getElementById('usuario').value;
				estatus=1;
				codtipoacceso=document.getElementById('acceso'+[valor]).value;

				var variable=usuario+'/'+modulo+'/'+codtipoacceso+'/'+estatus+'/'+usuarioadm;
//alert (variable);
$.post('view/usuario/editartipoacceso.php',{postvariable:variable},
	function(data)
	{
//	alertify.success(data);
window.location.reload();

});

return false;
}

function mostraracceso(valor){
	// alert(valor);
	document.getElementById('accesotipo'+[valor]).style.display='block';
}

function ocultaracceso(valor){
	document.getElementById('accesotipo'+[valor]).style.display='none';
}

function validarhoras(cantidad, valor) { 

	var horadesde = document.getElementById('horainicial'+[valor]).value;
	var horahasta = document.getElementById('horafinal'+[valor]).value;
	horadesde = horadesde.split(":"); 
	horahasta = horahasta.split(":"); 
    

// Obtener horas y minutos (hora 1) 
var hh1 = parseInt(horadesde[0],10); 
var mm1 = parseInt(horadesde[1],10); 
var ss1 = parseInt(horadesde[2],10); 

// Obtener horas y minutos (hora 2) 
var hh2 = parseInt(horahasta[0],10); 
var mm2 = parseInt(horahasta[1],10); 
var ss2 = parseInt(horahasta[2],10); 
// Comparar 
// if (hh1<hh2 || (hh1==hh2 && mm1<mm2)) 

if (hh1>hh2 || (hh1==hh2 && mm1>mm2) || (hh1==hh2 && mm1==mm2 && ss1>ss2)) 
{
//alertify.error ("La hora inicial no puede ser Mayor a la hora final"); 
document.getElementById('produccion').disabled=true;
return false;
}

//    alert ("sHora1 MAYOR sHora2"); 
else
{
	document.getElementById('produccion').disabled=false;
	return false; 
}
} 

function buscarpreparacion(){

	window.location.href='?cargar=buscarpreparacion'
}

function buscarpreparacioncliente(){
	var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if(((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")) 
	|| ((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarclientes&var='+variable
	}
}

function buscarclientepotencial(){
	var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
	
}

function buscaracuerdoservicio(){
	var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") || (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	/*if(((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}*/
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
	
}


function NuevoEjecutivo(){

	var operacion =document.getElementById('operacion').value;
	var codvendedor=document.getElementById('codvendedor').value;
	var aliasvendedor=document.getElementById('aliasvendedor').value;
	var coddocumento=document.getElementById('coddocumento').value;
	var tipodoc=document.getElementById('tipodoc').value;
	var nombres=document.getElementById('nombres').value;
	var apellidos=document.getElementById('apellidos').value;
	var usuario =document.getElementById('usuario').value;
	var documento;


	if (coddocumento=='')
	{
		alertify.alert("Debe Ingresar numero de cedula de ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (coddocumento.length<7)
	{
		alertify.alert('El numero de documento de identificacion debe tener minimo 7 caracteres');
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (codvendedor=='')
	{
		alertify.alert("Debe Ingresar el codigo del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (aliasvendedor=='')
	{
		alertify.alert("Debe Ingresar el alias del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (nombres=='')
	{
		alertify.alert("Debe los nombres del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (apellidos=='')
	{
		alertify.alert("Debe los apellidos del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}
	documento=tipodoc+'-'+coddocumento;

	var variable= operacion+'/'+codvendedor+'/'+aliasvendedor+'/'+documento+'/'+nombres+'/'+apellidos+'/'+usuario;

	alertify.set({ labels: {
		ok: "Ok",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Ingresar nuevo ejecutivo de ventas?", function (e) {
		if (e) {

			$.post('view/ejecutivo/nuevaCargaEjecutivo.php',{postvariable:variable},
				function(data)
				{
//ajaxget();
alertify.success(data);
setTimeout(function(){ window.location.reload(1); }, 3000);
});
		} else {
			alertify.error("No se agrego el ejecutivo de ventas");
			return false;
		}
	})
	return false;
}



function EditarEjecutivo(){

	var operacion =document.getElementById('operacion').value;
	var codvendedor=document.getElementById('codvendedor').value;
	var aliasvendedor=document.getElementById('aliasvendedor').value;
	var coddocumento=document.getElementById('coddocumento').value;
	var tipodoc=document.getElementById('tipodoc').value;
	var nombres=document.getElementById('nombres').value;
	var apellidos=document.getElementById('apellidos').value;
	var usuario =document.getElementById('usuario').value;
	var documento;


	if (codvendedor=='')
	{
		alertify.alert("Debe Ingresar el codigo del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (aliasvendedor=='')
	{
		alertify.alert("Debe Ingresar el alias del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (nombres=='')
	{
		alertify.alert("Debe los nombres del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (apellidos=='' )
	{
		alertify.alert("Debe los apellidos del ejecutivo de ventas");
		document.getElementById('ejecutivo').focus();
		return false;
	}
	documento=tipodoc+'-'+coddocumento;
	var variable= operacion+'/'+codvendedor+'/'+aliasvendedor+'/'+documento+'/'+nombres+'/'+apellidos+'/'+usuario;



	alertify.set({ labels: {
		ok: "Ok",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea modificar Ejecutivo de ventas?", function (e) {
		if (e) {

			$.post('view/ejecutivo/EditarEjecutivoExistente.php',{postvariable:variable},
				function(data)
				{
//ajaxget();
alertify.success(data);
setTimeout(function(){ window.location.reload(1); }, 2000);
});
		} else {
			alertify.error("No se modifico ejecutivo de ventas");
			return false;
		}
	})
	return false;
}


function eliminarej(ejecutivo){
	var operacion =document.getElementById('operacion').value;
	var variable= operacion+'/'+ejecutivo;

	alertify.set({ labels: {
		ok: "Eliminar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Eliminar Ejecutivo: "+ejecutivo+"?", function (e) {
		if (e) {
			$.post('view/ejecutivo/EliminarEjecutivo.php',{postejecutivo:variable},
				function(data)
				{
					alertify.success(data);
					setTimeout(function(){ window.location.reload(1); }, 2000);
				});
		} else {
			alertify.error("No se ha eliminado el ejecutivo");
			return false;
		}
	})
	return false;
}


// servicio tecnico 2018
function listaservitec(){

	/*var value = $('#busqueda').val();
  	var value_without_space = $.trim(value);
 	var busqueda = $('#busqueda').val(value_without_space);*/

	if(document.getElementById('busqueda').value == "")
	{
		alertify.alert("DEBE INGRESAR EL N° DE AFILIACIÓN RIF/CI O RAZÓN SOCIAL!.");
		document.getElementById('busqueda').focus();
		return false;
	}
	
	else {
		var busqueda =document.getElementById('busqueda').value;
		window.location.href='?cargar=buscarrifservicio&var='+busqueda

	}
}

function buscarrifgestionxmifi(){

	/*var value = $('#busqueda').val();
  	var value_without_space = $.trim(value);
 	var busqueda = $('#busqueda').val(value_without_space);*/

	if(document.getElementById('busqueda').value == "")
	{
		alertify.alert("DEBE INGRESAR EL N° DE AFILIACIÓN RIF/CI O RAZÓN SOCIAL!.");
		document.getElementById('busqueda').focus();
		return false;
	}
	
	else {
		var busqueda =document.getElementById('busqueda').value;
		window.location.href='?cargar=buscarrifmifiservicio&var='+busqueda

	}
}


function listaservitecgestion(){

	var busqueda =document.getElementById('busqueda').value;
	var usuario =document.getElementById('usuario').value;
	/*var value = $('#busqueda').val();
  	var value_without_space = $.trim(value);
 	var busqueda = $('#busqueda').val(value_without_space);*/

	if(document.getElementById('busqueda').value == "")
	{
		alertify.alert("DEBE INGRESAR EL N° DE AFILIACIÓN RIF/CI O RAZÓN SOCIAL!.");
		document.getElementById('busqueda').focus();
		return false;
	}
	
	else {
		var busqueda =document.getElementById('busqueda').value;
    var usuario =document.getElementById('usuario').value;

		//alert(variable1);
		window.location.href='?cargar=buscarrifserviciogestion&var='+busqueda+'/'+usuario

	}
}



function guardarserial(correlativo){

	var serialposasignado;
	var numterminal;
	var numsim;
	var fechaent;
	var tipopos;

    var totalserial=document.getElementById('totalserial').value;
	
	var usuario=document.getElementById('usuario').value;
	var nroafiliacion=document.getElementById('nroafiliacion').value;
    var namearchivo=document.getElementById('namearchivo').value;
	
	//alert (correlativo);
	
	//var	correlativo=document.getElementById('correlativo').value;

	fechaent=document.getElementById('fechaent'+correlativo).value;
	numterminal=document.getElementById('numterminal'+correlativo).value;
	serialposasignado=document.getElementById('serialpos'+correlativo).value;
	tipopos=document.getElementById('tipopos'+correlativo).value;
	numsim=document.getElementById('numsim'+correlativo).value;
	
	if (fechaent=='')
		{
		alertify.alert("Debe Seleccionar la fecha");
		document.getElementById('fechaent'+[correlativo]).focus();
		return false;
		}

	if (numterminal=='')
		{
		alertify.alert("Debe Ingresar el número de terminal");
		document.getElementById('numterminal'+[correlativo]).focus();
		return false;
		}

	if (serialposasignado=='')
		{
		alertify.alert("Debe Ingresar el número de serial del POS");
		document.getElementById('serialpos'+[correlativo]).focus();
		return false;
		}

	if ((tipopos=='GPRS') && (numsim==''))
		{
		alertify.alert("Debe Ingresar el número de SIM CARD");
		document.getElementById('numsim'+[correlativo]).focus();
		return false;
		}
			
	var variable=correlativo+'/'+fechaent+'/'+numterminal+'/'+serialposasignado+'/'+numsim+'/'+nroafiliacion+'/'+namearchivo+'/'+usuario;
	//alert(variable);
	
		alertify.set({ labels: {
            ok: "Procesar",
            cancel: "Cancelar"
        }
        });

        alertify.set({ buttonReverse: true });

		alertify.confirm("&#191;Desea Procesar la informacion?", function (e) {

		if (e) {
			$.post('view/ServicioTecnico/asignarserial.php',{postvariable:variable},
			function(data)
			{
				//if (data==="Procesado con éxito"){
				document.getElementById('gestionar'+[correlativo]).disabled=false;
				//}
				//document.getElementById('serialpos'+[correlativo]).readonly=true;
				alertify.success(data);
				//setTimeout(function(){ window.location.reload(0); }, 3000); 
				//$("#myModal").load("#myModal");

			});
		} else {
				 alertify.error("No se ha procesado la información");
                return false;
            	}
			})
        return false;
}


function guardardatosins(){
	


	var serialposasignado=document.getElementById('serialpos').value;
	var namearchivo=document.getElementById('namearchivo').value;
	var fecharecpos=document.getElementById('fecharecpos').value;
	var coditecnico=document.getElementById('tecnicos').value;
	var fechagestion=document.getElementById('fechagestion').value;
	var estatuspos=document.getElementById('statuspos').value;
	var fechainspos=document.getElementById('fechainspos').value;
	var usuariocarg=document.getElementById('usuariocarga').value;
	var nroafiliacion=document.getElementById('nroafiliacion').value;

	var variable=serialposasignado+'/'+namearchivo+'/'+fecharecpos+'/'+coditecnico+'/'+fechagestion+'/'+estatuspos+'/'+fechainspos+'/'+usuariocarg+'/'+nroafiliacion;
	
	$.post('view/ServicioTecnico/agregardatosins.php',{postvariable:variable},
		function(data)
		{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 

		});


	return false;

}




/*function ModificarServiTec(){

	var fechaent=document.getElementById('fechaent').value;
	var fecharec=document.getElementById('fecharec').value;
	var serial=document.getElementById('serial').value;
	var fechains=document.getElementById('fechains').value;
	var estatus=document.getElementById('estatus').value;
	var nroafiliacion=document.getElementById('nroafiliacion').value;




	if (serial=='')
	{
		alertify.alert("Debe ingresar el serial del POS");
		document.getElementById('ServicioTecnico').focus();
		return false;
	}


	if (estatus=='4')
	{
		if (fechains=='')
		{
			alertify.alert("Debe ingresar una fecha de instalacion del POS");
			document.getElementById('ServicioTecnico').focus();
			return false;
		}
	}


	var variable= fechaent+'/'+fecharec+'/'+serial+'/'+fechains+'/'+estatus+'/'+nroafiliacion;

	alertify.set({ labels: {
		ok: "Ok",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea los detalles de instalacion de POS a nuestra base de datos?", function (e) {
		if (e){

			$.post('view/ServicioTecnico/Agregardatosins.php',{postvariable:variable},
				function(data)
				{
//ajaxget();
alertify.success(data);
setTimeout(function(){ window.location.reload(1); }, 2000);
});
		} else {
			alertify.error("No se modifico ejecutivo de ventas");
			return false;
		}
	})

}*/

function EditarTecnico(){

	var operacion =document.getElementById('operacion').value;
	var codtecnicoserv=document.getElementById('codvendedor').value;
	var aliastecnico=document.getElementById('aliasvendedor').value;
	var coddocumento=document.getElementById('coddocumento').value;
	var tipodoc=document.getElementById('tipodoc').value;
	var nombres=document.getElementById('nombres').value;
	var apellidos=document.getElementById('apellidos').value;
	var usuario =document.getElementById('usuario').value;
	var documento;


	if (codtecnicoserv=='')
	{
		alertify.alert("Debe Ingresar el codigo del tecnico");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (aliastecnico=='')
	{
		alertify.alert("Debe Ingresar el alias del tecnico");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (nombres=='')
	{
		alertify.alert("Debe los nombres del tecnico");
		document.getElementById('ejecutivo').focus();
		return false;
	}

	if (apellidos=='' )
	{
		alertify.alert("Debe los apellidos del tecnico");
		document.getElementById('ejecutivo').focus();
		return false;
	}
	documento=tipodoc+'-'+coddocumento;
	var variable= operacion+'/'+codtecnicoserv+'/'+aliastecnico+'/'+documento+'/'+nombres+'/'+apellidos+'/'+usuario;



	alertify.set({ labels: {
		ok: "Ok",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea modificar datos del tecnico", function (e) {
		if (e) {

			$.post('view/ejecutivo/EditarTecnicoExistente.php',{postvariable:variable},
				function(data)
				{
//ajaxget();
alertify.success(data);
setTimeout(function(){ window.location.reload(1); }, 2000);
});
		} else {
			alertify.error("No se modificaron datos del tecnico");
			return false;
		}
	})
	return false;
}


function restaurararchivo(){

	var usuario =document.getElementById('usuario').value;
	alertify.set({ labels: {
		ok: "Ok",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	if(document.getElementById('clientesrestaurar').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL BANCO A RESTAURAR!");
		document.getElementById('clientesrestaurar').focus();
		return false;
	}
	if(document.getElementById('fecharestaurar').value == "")
	{
		alertify.alert("DEBE SELECCIONAR LA FECHA A RESTAURAR!");
		document.getElementById('fecharestaurar').focus();
		return false;
	}	
	if(document.getElementById('nombrearchivo').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO RESTAURAR!");
		document.getElementById('nombrearchivo').focus();
		return false;
	}	

	alertify.confirm("&#191;Desea Restaurar al archivo?", function (e) {
		if (e) {
			var variable=document.getElementById('clientesrestaurar').value+'/'+document.getElementById('fecharestaurar').value+'/'+usuario+'/'+document.getElementById('nombrearchivo').value;
// alert (variable);
$.post('view/ClientePotencial/recepcion/restaurararchivo.php',{postvariable:variable},
	function(data)
	{
//ajaxget();
alertify.success(data);
setTimeout(function(){ window.location.reload(1); }, 3000);
});
//alertify.success("Usuario eliminado con &Eacute;xito");
} else {
	alertify.error("No se ha restaurado el archivo");
	return false;
}
})
	return false;
}

// funciones juniors

function buscarcomercializacioncliente(){
	var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value+'/'+document.getElementById('tipousuario').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('usuario').value+'/'+document.getElementById('coordinador').value+'/'+document.getElementById('coordinadordos').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))
	
	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
}

// busqueda por rif o razon social
function buscarclienteRif(){
 var value = $('#nrorif').val();
    var value_without_space2 = $.trim(value);
	var value_without_space=value_without_space2.toUpperCase();
  var rif =$('#nrorif').val(value_without_space);

	if(document.getElementById('nrorif').value == "")
	{   
		 // .ajs-message.ajs-custom { color: #31708f;  background-color: #d9edf7;  border-color: #31708f; }
		alertify.set('notifier','position', 'bottom-right');
		alertify.alert("El campo no puede estar vacio.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('nrorif').focus();
		document.getElementById('nrorif').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('nrorif').value+'/'+document.getElementById('tipousuario').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('usuario').value+'/'+document.getElementById('coordinador').value+'/'+document.getElementById('coordinadordos').value;
		window.location.href='?cargar=buscarrif&var='+variable
	}
}

//

//

function buscarclienteRifvisita(){
 var value = $('#nrorif').val();
    var value_without_space2 = $.trim(value);
	var value_without_space=value_without_space2.toUpperCase();
  var rif =$('#nrorif').val(value_without_space);

	if(document.getElementById('nrorif').value == "")
	{   
		 // .ajs-message.ajs-custom { color: #31708f;  background-color: #d9edf7;  border-color: #31708f; }
		alertify.set('notifier','position', 'bottom-right');
		alertify.alert("El campo no puede estar vacio.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('nrorif').focus();
		document.getElementById('nrorif').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('nrorif').value;
		window.location.href='?cargar=buscarregistrovisitaxrif&var='+variable
	}
}


function crearrperfil(){
 var value = $('#perfilgeo').val();
    var value_without_space = $.trim(value);
  var rif =$('#perfilgeo').val(value_without_space);

	if(document.getElementById('perfilgeo').value == "")
	{   
		 // .ajs-message.ajs-custom { color: #31708f;  background-color: #d9edf7;  border-color: #31708f; }
		alertify.set('notifier','position', 'bottom-right');
		alertify.alert("El campo no puede estar vacio.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('perfilgeo').focus();
		document.getElementById('perfilgeo').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('perfilgeo').value;
		window.location.href='?cargar=buscarrif&var='+variable
	}
}

function buscarclientecomercial(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;
var operacion=document.getElementById('valor').value;

	// var cliente=document.getElementById('clientes').value;
	// var operacion=document.getElementById('valor').value;
	// var fecha=document.getElementById('fecha').value;

if((document.getElementById('clientes').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL BANCO!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))
	
	{	
		alertify.alert("PARA GENERAR EXCEL SOLO DEBE SELECCIONAR EL BANCO!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	
	{	
		alertify.alert("PARA GENERAR EXCEL SOLO DEBE SELECCIONAR EL BANCO!.");
		document.getElementById('clientes').focus();
		return false;
	}
	else{
	var variable=document.getElementById('clientes').value;
	window.location.href='?cargar=buscargestion&operacion&var='+operacion+'&var='+variable
	}
	

	}
function buscarreporte(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarreporte&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarreporte&var='+variable
	}
}

function buscarreporteadministracion(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
}


function buscarreporteerp(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
}



function buscarreportepos(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
}


function buscarejecutivos(){

var variable=document.getElementById('clientes').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
var variable1=document.getElementById('clientes').value+'/'+document.getElementById('ejecutivo').value;
		
	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	{
		alertify.alert("DEBE SELECCIONAR UN EJECUTIVO!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		
		window.location.href='?cargar=buscarcliente&var='+variable1
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	{
		window.location.href='?cargar=reporteeje&var='+variable
	}
		
	}

	function buscarreportediario(){
var reporteeje=document.getElementById('tiporeporte').value;
var variable=document.getElementById('clientes').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value+'/'+document.getElementById('tiporeporte').value;
var variable1=document.getElementById('clientes').value+'/'+document.getElementById('ejecutivo').value;
		
	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	{ if (reporteeje=='3') {
		window.location.href='?cargar=reportetrabajo&var='+variable
	}
		else{
		window.location.href='?cargar=reportexdias&var='+variable
	}}
	/*Busqueda solo por banco */
	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		var variable1= document.getElementById('clientes').value;
		window.location.href='?cargar=reportexbanco&var='+variable1
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=reportexdias&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
		
		window.location.href='?cargar=reportexdias&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
	
		window.location.href='?cargar=reportexdias&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value =="") && (document.getElementById('fechafinal').value =="")){

		window.location.href='?cargar=reportexdias&var='+variable
	}


		if((document.getElementById('clientes').value == "")  && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		window.location.href='?cargar=reportexdias&var='+variable
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value =="") && (document.getElementById('fechafinal').value =="")){

	window.location.href='?cargar=reportexdias&var='+variable
	}
	

	// if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	
	// {
	// 	window.location.href='?cargar=buscarrecepcion&var='+variable
	// }

	
	
	// if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	// {
	// 	window.location.href='?cargar=buscarrecepcion&var='+variable
	// }

	// if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	// {
	// 	window.location.href='?cargar=reporteeje&var='+variable
	// }
		
	}

function buscarreportegerencial(){

var variable1=document.getElementById('clientes').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value == "")){

		window.location.href='?cargar=reportegrupal&var='+variable
	}
		
	/*Busqueda solo por banco */
	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value != ""))
	
	{
		
		window.location.href='?cargar=reportegrupal&var='+variable1
	}


	

	
	}

	function buscarreportegerencialdetallado(){

var variable1=document.getElementById('POS').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
var variable=document.getElementById('POS').value;
		//alert(reporteeje);
	if((document.getElementById('POS').value != "") && (document.getElementById('fechainicial').value == "")){

		window.location.href='?cargar=reportetipopos&var='+variable
	}
		
	/*Busqueda solo por banco */
	if((document.getElementById('POS').value != "") && (document.getElementById('fechainicial').value != ""))
	
	{
		
		window.location.href='?cargar=reportetipopos&var='+variable1
	}


	

	
	}

	// recuerdame
function buscarreporteestatus(){

var clientes=document.getElementById('clientes').value;

var regiones=document.getElementById('regiones').value;


var modalidad=document.getElementById('modalidad');

// var variable=document.getElementById('clientes').value+'/'+document.getElementById('modalidad').value;

if (modalidad==null) {

	window.location.href='?cargar=generarreporteestatus&var='+clientes+'/'+regiones
	
} else { 
	var modalidad=document.getElementById('modalidad').value;

var variable=document.getElementById('clientes').value+'/'+regiones+'/'+document.getElementById('modalidad').value;

//alert(variable);
if((document.getElementById('clientes').value != "") && (document.getElementById('regiones').value != "")){

			window.location.href='?cargar=generarreporteestatus&var='+variable
		}
		else
		{
			alertify.alert("DEBE SELECCIONAR UNA REGION Y UN BANCO A CONSULTAR!.");
			document.getElementById('clientes').focus();
			return false;
		} 
	}	
}


function buscarreportedomiciliacion (){

	var variable=document.getElementById('clientes').value;
			//alert(reporteeje);
		if(document.getElementById('clientes').value != ""){
	
			window.location.href='?cargar=generarreporte&var='+variable
		}
		else
		{
			alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
			document.getElementById('clientes').focus();
			return false;
		}	
	}


function buscarreporteequipos(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=generarreporteequipos&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}

function buscarreporteequiposinstalados(){

	var variable=document.getElementById('clientes').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
		//alert(reporteeje);
	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == "")){

		alertify.alert("DEBE SELECCIONAR UN MÉTODO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == "")){

		window.location.href='?cargar=generarreporteequiposinstalados&var='+variable

	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != "")){

		window.location.href='?cargar=generarreporteequiposinstalados&var='+variable

	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != "")){

		window.location.href='?cargar=generarreporteequiposinstalados&var='+variable

	}

}

function buscarreportepercance(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=generarreportepercance&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}


function buscarreporteestatusgestion(){
//var variable=document.getElementById('clientes').value+'/'+document.getElementById('almacen').value+'/'+document.getElementById('region').value;
var variable1=document.getElementById('region').value+'/'+document.getElementById('clientes').value+'/'+document.getElementById('almacen').value;
		
	if(document.getElementById('region').value == "")
	{
		alertify.alert("DEBE SELECCIONAR REGIÓN!.");
		document.getElementById('region').focus();
		return false;
	}
	if((document.getElementById('region').value == "") && (document.getElementById('clientes').value == "") && (document.getElementById('almacen').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('region').value != "") && (document.getElementById('clientes').value == "") && (document.getElementById('almacen').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR BANCO O ALMACEN.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('region').value != "") && (document.getElementById('clientes').value != "") && (document.getElementById('almacen').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('almacen').value != "") && (document.getElementById('region').value != "")))
	{
		window.location.href='?cargar=generarreporteestatus&var='+variable1
	}
	
	if((document.getElementById('region').value != "") && (document.getElementById('clientes').value != "") && (document.getElementById('almacen').value != ""))
	{
		window.location.href='?cargar=generarreporteestatus&var='+variable1
	}
}


function buscarreportegestiondemifi(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('almacen').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('almacen').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('almacen').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('almacen').value != "")))
	{
		window.location.href='?cargar=generarreporteestatusmifi&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('almacen').value != ""))
	{
		window.location.href='?cargar=generarreporteestatusmifi&var='+variable
	}
}

function buscarreportegeneralmifi(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=reportegeneraldemifis&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}

function buscarclientesprofit(){

	var variable=document.getElementById('clientes').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;


		//alert(reporteeje);
	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == "")){

		alertify.alert("DEBE SELECCIONAR UN MÉTODO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == "")){

		window.location.href='?cargar=busquedaclientes&var='+variable

	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != "")){

		window.location.href='?cargar=busquedaclientes&var='+variable

	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != "")){

		window.location.href='?cargar=busquedaclientes&var='+variable

	}

}


function buscarplantillaprofit(){

	var variable=document.getElementById('clientes').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;


		//alert(reporteeje);
	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == "")){

		alertify.alert("DEBE SELECCIONAR UN MÉTODO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == "")){

		window.location.href='?cargar=busquedaplantilla&var='+variable

	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != "")){

		window.location.href='?cargar=busquedaplantilla&var='+variable

	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != "")){

		window.location.href='?cargar=busquedaplantilla&var='+variable

	}

}

function comerdatosbasicos(){

	coddocumento=document.getElementById('coddocumento').value;
	nombre=document.getElementById('nombre').value;
	apellido=document.getElementById('apellido').value;
	institutouorganismo=document.getElementById('institutouorganismo').value;
	rcomercialofantasia=document.getElementById('rcomercialofantasia').value;
	namearch=document.getElementById('namearch').value;
	if((nombre||apellido||institutouorganismorcomercialofantasia)==""){
    alertify.error("Error,los campos estan vacios.");
    

	} else{ 
	var variable=coddocumento+'/'+nombre+'/'+apellido+'/'+institutouorganismo+'/'+rcomercialofantasia+'/'+namearch;
	$.post('view/comercializacion/gestion/datosbasicos.php',{postvariable:variable},
		function(data)
		{
			modified=false;
			alertify.success(data);
//setTimeout(function(){ window.location.reload(0); }, 4000); 

});
	}

	return false;
}



function reporteequipos(){


var variable=document.getElementById('clientes').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
var variable1=document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
		

	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	
	{
		window.location.href='?cargar=buscarreporteequipos&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA FECHA A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		window.location.href='?cargar=buscarreporteequipos&var='+document.getElementById('clientes').value
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value != "") && (document.getElementById('fechafinal').value != ""))
	{
		window.location.href='?cargar=buscarreporteequipos&var='+variable
	}

		if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA FECHA A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		window.location.href='?cargar=buscarreporteequipos&var='+document.getElementById('clientes').value
	}

		
	}


function comerdatoscontacto(){

	coddocumento=document.getElementById('coddocumento').value;
	tlfmovil1=document.getElementById('tlfmovil1').value;
	tlfmovil2=document.getElementById('tlfmovil2').value;
	email1=document.getElementById('email1').value;
	email2=document.getElementById('email2').value;
	email3=document.getElementById('email3').value;
    namearch=document.getElementById('namearch').value;
    nombre=document.getElementById('nombre').value;
    apellido=document.getElementById('apellido').value;
    usuario=document.getElementById('usuario').value;
    cliente=document.getElementById('id_prefijobanco').value;
	direccioninstalacion=document.getElementById('id_registro').value;
	tlfmovil3=document.getElementById('tlfmovil3').value;
	tlfmovil4=document.getElementById('tlfmovil4').value;
    contador=0;
    iteraciones=3;

    // if((document.getElementById('tlfmovil1').value== "") &&( document.getElementById('tlfmovil2').value=="") ) {
    //     alertify.error('Debe agregar por lo menos un n&uacute;mero de tel&eacute;fono movil');
    //     $('#tlfmovil1').input().css("border-color","red" );

    //       return false;
    // } 
    
  //    if((email1||email2||email3) == ""){
 	// alertify.error('Debe agregar por lo menos una dirección de correo');
 	// document.getElementById('email1').focus();
  //   $('#email1').input().css("border-color","red" );
  //   return false;
 	// }


//     else{ 

     
// 				for (init =0; init < iteraciones; init++)
// 				{
// 					    contador++;
// 					    var corp =0;
// 					    var per =0;
// 					    var com =0;

// 						texto=document.getElementById('email'+[contador]).value;
//  					 	var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

// 						   if(texto.value == "") {
// 						      document.getElementById('email'+[contador]).style = 'border-color:none;';

// 						      // document.getElementById('updatoscontacto').disabled=false;
// 						   }
// 						   else if (texto!="" && !regex.test(texto)) {
// 						      document.getElementById("resultcorreo").innerHTML = "Correo invalido";
// 						      document.getElementById('email'+[contador]).style = 'border-color:red;';  
// 						       alertify.error(texto+' Dirección de Correo incorrecta');   
// 						       if(texto.id=='email1'){ corp =1;  }
// 						       if (texto.id=='email2') { per=1; }
// 						       if (texto.id=='email3') { com=1; }
// 						       correosvalidos=1;
// 						       return false;

// 						      }

// 						   else if (regex.test(texto) || (corp ==0) || (per ==0)  ||(com ==0)   ){
// 						      // document.getElementById('updatoscontacto').disabled=false;
// 						      document.getElementById("resultcorreo").innerHTML = "";
// 						      document.getElementById('email'+[contador]).style = 'border-color:green;';

// 							correosvalidos=0

// 						  }
					                    
// }
				 
	var variable=coddocumento+'/'+tlfmovil1+'/'+tlfmovil2+'/'+email1+'/'+email2+'/'+email3+'/'+namearch+'/'+nombre+'/'+apellido+'/'+direccioninstalacion+'/'+usuario+'/'+cliente+'/'+tlfmovil3+'/'+tlfmovil4;


	$.post('view/comercializacion/gestion/datoscontacto.php',{postvariable:variable}).done(function(data)
		{


			if(data.estado=="ok"){
alertify.success('Datos guardados con &eacute;xito');
           modified=false;
           $('#tlfmovil1').html(data.tlf_movil1);
           $('#tlfm1').val(data.tlf_movil1);// span en Datos Básicos
		   $('#tlfmovil2').html(data.tlf_movil2);
		   $('#tlfmovil3').val(data.telf1);
		   $('#tlfmovil4').val(data.telf2);
           $('#email1').val(data.email_corporativo);
           $('#email2').val(data.email_comercial);
           $('#email3').val(data.email_personal);
           $('#nombre').val(data.nombre);
           var pcontacto= data.nombre+' '+data.apellido;
           $('#personacontacto').html(pcontacto).toString();
// $('#apellido').val(data.apellido).toString();
}
           
			
		});
    //}
	
	return false;


}

function equiposcostos(){

	coddocumento=document.getElementById('coddocumento').value;
    namearch=document.getElementById('namearch').value;
   	direccioninstalacion=document.getElementById('id_registro').value;
    usuario=document.getElementById('usuario').value;
    tipopos=document.getElementById('tipopos').value;
    codmodelopos=document.getElementById('modelopos').value;
    codmarcapos=document.getElementById('marcapos').value;
    _cantposasignados=document.getElementById('cantpos').value;
	cantposaprobados=document.getElementById('cantposaprobados').value;
	tipolinea=document.getElementById('tipolinea').value;
	/*tipolinea= $("#tipolinea option:selected").text();
	tlinea=tipolinea.trim();*/

    if(document.getElementById('tipopos').value== 0 ) {
        alertify.error('Debe seleccionar el tipo de POS');
        $('#tipopos').select().css("border-color","red" );
          return false;
    } 

    if(document.getElementById('modelopos').value== 0 ) {
        alertify.error('Debe seleccionar el modelo del POS');
        $('#modelopos').select().css("border-color","red" );

          return false;
    } 
    //alert(codmodelopos);

    if(document.getElementById('modelopos').value== 1 && document.getElementById('marcapos').value==0) {
        alertify.error('Debe seleccionar la marca del POS');
        $('#marcapos').select().css("border-color","red" );

          return false;
    } 
    
/*     if(document.getElementById('tipolinea').value== 0 && document.getElementById('tipopos').value!='MIFI')  {
        alertify.error('Debe seleccionar un tipo de linea');
        $('#tipolinea').select().css("border-color","red" );

          return false;
    } */

/*    if(document.getElementById('tipolinea').value== 0 )  {
       alertify.error('Debe seleccionar un tipo de linea');
        $('#tipolinea').select().css("border-color","red" );

          return false;
    } */
    if(document.getElementById('cantposaprobados').value== 0 ) {
        alertify.error('Cantidad de POS aprobados debe ser mayor a cero');
        $('#cantposaprobados').select().css("border-color","red" );

          return false;
    }
    // if (cantposaprobados>_cantposasignados) {
    // 	  alertify.error('Cantidad de "POS Asignados" no puede ser mayor a cantidad de "POS Aprobados"');
    // } 
  
    else{ 

    var variable=coddocumento+'/'+namearch+'/'+usuario+'/'+direccioninstalacion+'/'+codmodelopos+'/'+codmarcapos+'/'+cantposaprobados+'/'+tipolinea+'/'+tipopos;
	  //alert(variable);
	$.post('view/comercializacion/gestion/equiposcostos.php',{postvariable:variable}).done(function(data)
		{
			modified=false; 
			alertify.success(data); 
		    setTimeout(function(){ window.location.reload(0); }, 2000); 
			
		});
    }

	return false;
}

function equiposcostos1(){

	coddocumento=document.getElementById('coddocumento').value;
    namearch=document.getElementById('namearch').value;
   	direccioninstalacion=document.getElementById('id_registro').value;
    usuario=document.getElementById('usuario').value;
    tipopos=document.getElementById('tipopos').value;
    codmarcapos=document.getElementById('marcapos').value;
    _cantposasignados=document.getElementById('cantpos').value;
	cantposaprobados=document.getElementById('cantposaprobados').value;
	tipolinea= $("#tipolinea option:selected").text();
	tlinea=tipolinea.trim();

    if(document.getElementById('tipopos').value== 0 ) {
        alertify.error('Debe seleccionar el tipo de POS');
        $('#tipopos').select().css("border-color","red" );
          return false;
    } 

    if(document.getElementById('marcapos').value== 0 ) {
        alertify.error('Debe seleccionar marca del POS');
        $('#marcapos').select().css("border-color","red" );

          return false;
    } 
    
    if(document.getElementById('tipolinea').value== 0 ) {
        alertify.error('Debe seleccionar un tipo de linea');
        $('#tipolinea').select().css("border-color","red" );

          return false;
    } 
    if(document.getElementById('cantposaprobados').value== 0 ) {
        alertify.error('Cantidad de POS aprobados debe ser mayor a cero');
        $('#cantposaprobados').select().css("border-color","red" );

          return false;
    }
    // if (cantposaprobados>_cantposasignados) {
    // 	  alertify.error('Cantidad de "POS Asignados" no puede ser mayor a cantidad de "POS Aprobados"');
    // } 
  
    else{ 

    var variable=coddocumento+'/'+namearch+'/'+usuario+'/'+direccioninstalacion+'/'+codmarcapos+'/'+cantposaprobados+'/'+tlinea+'/'+tipopos;
	 //alert(variable);
	$.post('view/comercializacion/gestion/equipomodificado.php',{postvariable:variable}).done(function(data)
		{
			modified=false; 
			alertify.success(data); 
		    //setTimeout(function(){ window.location.reload(0); }, 2000); 
			
		});
    }

	return false;
}



function indicainteres(){

	coddocumento=document.getElementById('coddocumento').value;
    namearch=document.getElementById('namearch').value;
    usuario=document.getElementById('usuario').value;
   	id_registro=document.getElementById('id_registro').value;
   	interes=document.getElementById('interes').value;
   	nafiliacion=document.getElementById('nafiliado').value;
    ejebanco=document.getElementById('ejebanco').value;
    ejeinteligensa=document.getElementById('vendedoresinteligensa').value;
	if((interes) == ""){
 	alertify.error('Debe indicar si esta interesado');
    document.getElementById('interes').focus();
    $('#interes').select().css("border-color","red");
    return	false;
 	}
 	else if (ejebanco==""){
        alertify.error('No tiene ejecutivo por banco asignado, comuniquese con el administrador del sistema');
        $('#nombrejecutivo').input().css("border-color","red" );
        return false;
	}

   
    else{ 

    var variable=coddocumento+'/'+namearch+'/'+usuario+'/'+id_registro+'/'+interes+'/'+nafiliacion+'/'+ejebanco+'/'+ejeinteligensa;
	$.post('view/comercializacion/gestion/indicainteres.php',{postvariable:variable}).done(function(data)
	{	
			modified=false; 
			alertify.success(data); 
			setTimeout(function(){ window.location.reload(0); }, 4000); 

			
		});
    }

	return false;
}



function comerdatosempresa(){

  var arrTodo = new Array();
  var arrValores = $('input[name="dias"]:checked').map(function() {
    arrTodo.push(this.id, this.checked);
     chk = $(this).is(':checked') ? 1 : 0;
    $(this).val(chk);
    return this.value;

  }).get();
 
    lun=document.getElementById('checklunes').value;
    mar=document.getElementById('checkmartes').value;
    mie=document.getElementById('checkmiercoles').value;
    jue=document.getElementById('checkjueves').value;
    vie=document.getElementById('checkviernes').value;
    sab=document.getElementById('checksabado').value;
    dom=document.getElementById('checkdomingo').value;
	coddocumento=document.getElementById('coddocumento').value;
	txtlunvie=document.getElementById('horas_lun_vier').value;
	txtsabdom=document.getElementById('horas_sab_dom').value;
	namearch=document.getElementById('namearch').value;
	usuario=document.getElementById('usuario').value;
	acteconomica=document.getElementById('edit_actividad').value;
	categoria=document.getElementById('edit_categoria').value;
	subcategoria=document.getElementById('edit_subcategoria').value;
	id_registro=document.getElementById('id_registro').value;

	if((acteconomica)==0){
 	alertify.error('El campo actividad economica no puede estar vacio');
    document.getElementById('acteconomica').focus();
    $('#edit_actividad').input().css("border-color","red");
 	}
    else {
    document.getElementsByClassName("form-group col-xs-12 divcheck")[0].style.border = 'none';
	var variable=coddocumento+'/'+namearch+'/'+lun+'/'+mar+'/'+mie+'/'+jue+'/'+vie+'/'+sab+'/'+dom+'/'+usuario+'/'+txtlunvie+'/'+txtsabdom+'/'+acteconomica+'/'+id_registro+'/'+categoria+'/'+subcategoria;
	//alert(acteconomica);
	$.post('view/comercializacion/gestion/datoscaracteristicaempresa.php',{postvariable:variable},
		function(data)
		{
			modified=false;
			alertify.success(data);
//setTimeout(function(){ window.location.reload(0); }, 4000); 

       });
       }

	return false;
	
}


function caracteristicasdepago(){
	var cantidadterminales =document.getElementById('cantposaprobados').value; 
	//alert (cantidadterminales);
	var contador=0;
	estatuspos1=document.getElementById('estatuspos1').value;
	ejebanco=document.getElementById('ejebanco').value;
	coddocumento=document.getElementById('coddocumento').value;
	cliente=document.getElementById('id_prefijobanco').value;
	namearchivo=document.getElementById('namearch').value;
	modventa=document.getElementById('modoventa').value;
	usuario=document.getElementById('usuario').value;
	totalprecio=document.getElementById('totalprecio').value;	
  nafiliacion=document.getElementById('nafiliado').value;
  totalaprobados=document.getElementById('totalaprobados').value;
  id_registro=document.getElementById('id_r').value;
  fechaipago=document.getElementById('fechaipago').value;
  anticipomonto=document.getElementById('anticipomonto').value;
  descuentomonto=document.getElementById('descuentomonto').value;
  inicialmonto=document.getElementById('inicialmonto').value;
  coordaprobacion=document.getElementById('coordaprobacion').value;
  observacionpago=document.getElementById('observacionpago').value;
  montocomision=document.getElementById('montocomision').value;
  precioactual=document.getElementById('costobs_facturacion').value;
  //precioanterior=document.getElementById('preciopos').value;
  montorecompra=document.getElementById('montorecompra').value;
  serialresguardo1=document.getElementById('serialresguardo1').value;

  //alert(precioactual+'/'+precioanterior);
	
    //alert(equiprecompra);
    if (modventa=="0") {
	    alertify.error('Debe indicar la modalidad de venta');
	    document.getElementById('modoventa').focus();
	    $('#modoventa').select().css("border-color","red");
	    return	false;	 	
    } 

    if(coordaprobacion=="0" && modventa=="4"){
	 		alertify.error('Debe indicar el coordinador que aprobó la venta');
	    document.getElementById('coordaprobacion').focus();
	    $('#coordaprobacion').select().css("border-color","red");
	    return	false;
 		}
    
    if(fechaipago==""){
	 	alertify.error('Debe indicar la fecha');
	    document.getElementById('fechaipago').focus();
	    $('#fechaipago').select().css("border-color","red");
	    return	false;
 		}

	if(anticipomonto=='' && modventa=="6"){
		alertify.error('El monto de anticipo no debe estar en blanco');
	    document.getElementById('anticipomonto').focus();
	    $('#anticipomonto').select().css("border-color","red");
	    return	false;	
	}
//alert(modventa)
 	//  if (modventa=="6") {
 	//  	for (var i = 0; i < cantidadterminales; i++) {
	// 		contador++;
	// 			//var serialresguardo1 =document.getElementById("serial"+[contador]).value;
	// 			//var montorecompra =document.getElementById("montorecompra"+[contador]).value;
	// 			//var fecharecompra =document.getElementById("fecharecompra"+[contador]).value;

	// 			var variable=coddocumento+'/'+cliente+'/'+namearchivo+'/'+usuario+'/'+nafiliacion+'/'+totalprecio+'/'+ejebanco+'/'+estatuspos1+'/'+id_registro+'/'+totalaprobados+'/'+modventa+'/'+fechaipago+'/'+anticipomonto+'/'+descuentomonto+'/'+inicialmonto+'/'+coordaprobacion+'/'+observacionpago+'/'+montocomision+'/'+montorecompra+'/'+serialresguardo1;
	// 			//alert(variable);
	// 			$.post('view/comercializacion/gestion/caracteristicasdepago.php',{postvariable:variable},
	// 					function(data)
	// 					{
	// 						modified=false;
	// 						alertify.success(data);
	// 		          setTimeout(function(){ window.location.reload(0); }, 3000); 
	// 		          $('#caracteristicaActualizar').prop('disabled',true);
	// 		          $('#estatuspos1').select().css("border-color","none");
			         

	// 		});
				
	// 			//return false;
				
	// 		}
			
		//}else{
			//montorecompra="";
		//	serialresguardo1="";
			var variable=coddocumento+'/'+cliente+'/'+namearchivo+'/'+usuario+'/'+nafiliacion+'/'+totalprecio+'/'+ejebanco+'/'+estatuspos1+'/'+id_registro+'/'+totalaprobados+'/'+modventa+'/'+fechaipago+'/'+anticipomonto+'/'+descuentomonto+'/'+inicialmonto+'/'+coordaprobacion+'/'+observacionpago+'/'+montocomision+'/'+montorecompra+'/'+serialresguardo1+'/'+precioactual;
			//alert(variable);
				$.post('view/comercializacion/gestion/caracteristicasdepago.php',{postvariable:variable},
						function(data)
						{
							modified=false;
							alertify.success(data);
			          setTimeout(function(){ window.location.reload(0); }, 3000); 
			          $('#caracteristicaActualizar').prop('disabled',true);
			          $('#estatuspos1').select().css("border-color","none");
			         

			});
				
				return false;
		//}
}

function validapreciopos (){

	  precioactual=document.getElementById('costobs_facturacion').value;
  	precioanterior=document.getElementById('preciopos').value;
  	cliente_venta=document.getElementById('clienteventa').value;
  	registro=document.getElementById('id_registro').value;
  	area_usuario=document.getElementById('codarea').value;
  	usuario=document.getElementById('usuario').value;
  	saldo=document.getElementById('saldo').value;
  	//console.log(usuario);

  	if (area_usuario==1 || (usuario=='NBRAVO' || usuario=='RMARINO')) {
 			if(precioactual!= precioanterior && cliente_venta==registro && saldo!=0){

 			document.getElementById("resultprecio").innerHTML = "El precio actual no coincide con el registrado, debe modificarlo.";
 				$('#caracteristicaActualizar').prop('disabled',false);
 			return false;
 			}
 			
		}
}



function dirfiscal(){

    coddocumento=document.getElementById('coddocumento').value;
 	codetipodir1=document.getElementById('codetipodir1').value;
    d_calle_av1=document.getElementById('d_calle_av1').value;
	d_localidad1=document.getElementById('d_municipio1').value;
	d_sector1=document.getElementById('d_parroquia1').value;
	d_urbanizacion1=document.getElementById('d_urbanizacion1').value;
	d_nlocal1=document.getElementById('d_nlocal1').value;
	d_estado1=document.getElementById('d_estado1').value;
    d_codepostal1=document.getElementById('d_codepostal1').value;
	d_ptoref1=document.getElementById('d_ptoref1').value;
	m=document.getElementById('fechacarga').value;
	n=document.getElementById('prefijo').value;
	namearch=document.getElementById('namearch').value;
	usuario=document.getElementById('usuario').value;
	id_registro=document.getElementById('id_registro').value;

if ((d_calle_av1||d_localidad1||d_estado1||d_urbanizacion1||d_nlocal1||d_estado1 ||d_codepostal1||d_ptoref1)=="") {
	 alertify.error("Error,los campos estan vacios.");
     return false;
}
else if(d_estado1==""){
	alertify.error('Error, Debe seleccionar un Estado');
 	$('#edit_estado').focus().select();
	$('#edit_estado').select().css("border-color","red" );
	return false;
     }
else if(d_localidad1==""){
	alertify.error('Error, debe seleccionar un Municipio');
 	$('#edit_localidad').focus().select();
	$('#edit_localidad').select().css("border-color","red");
	return false;
     }
else if(d_sector1==""){
	alertify.error('Error, debe seleccionar una Parroquia');
 	$('#edit_sector').focus();
	$('#edit_sector').css("border-color","red");
	return false;
     }

else{
	
	var variable=coddocumento+'|-|SEPARATOR|-|'+codetipodir1+'|-|SEPARATOR|-|'+d_calle_av1+'|-|SEPARATOR|-|'+d_localidad1+'|-|SEPARATOR|-|'+d_sector1+'|-|SEPARATOR|-|'+d_urbanizacion1+'|-|SEPARATOR|-|'+d_nlocal1+'|-|SEPARATOR|-|'+ d_estado1+'|-|SEPARATOR|-|'+d_codepostal1+'|-|SEPARATOR|-|'+d_ptoref1+'|-|SEPARATOR|-|'+m+'|-|SEPARATOR|-|'+n+'|-|SEPARATOR|-|'+namearch+'|-|SEPARATOR|-|'+usuario+'|-|SEPARATOR|-|'+id_registro;
			  $.post('view/comercializacion/gestion/direcciones.php',{postvariable:variable},
			   function(data)
			   {
			   	modified=false;
				alertify.success(data);
				//parent.document.location = parent.document.location;
			   setTimeout(function(){ window.location.reload(0); }, 4000); 

			   });

	}		  
        return false;
}

function dircomercial(){

	a=document.getElementById('coddocumento').value;
	b=document.getElementById('codetipodir2').value;
	c=document.getElementById('d_calle_av2').value;
	d=document.getElementById('d_localidad2').value;
	e=document.getElementById('d_sector2').value;
	h=document.getElementById('d_urbanizacion2').value;
	i=document.getElementById('d_nlocal2').value;
	j=document.getElementById('d_estado2').value;
	k=document.getElementById('d_codepostal2').value;
	l=document.getElementById('d_ptoref2').value;
	m=document.getElementById('fechacarga').value;
	n=document.getElementById('prefijo').value;
	namearch=document.getElementById('namearch').value;
	if(j==0){
	alertify.error('Debe seleccionar un Estado');
 	$('#d_estado2').focus().select();
	$('#d_estado2').select().css("border-color","red" );
	return false;
     }

	if ((c||d||l||e||h||i||j||k)=="") {
	 alertify.error("Error,los campos estan vacios.");
        return false;
}else{

	var variable=a+'/'+b+'/'+c+'/'+d+'/'+e+'/'+h+'/'+i+'/'+ j+'/'+k+'/'+l+'/'+m+'/'+n+'/'+namearch;
	$.post('view/comercializacion/gestion/direcciones.php',{postvariable:variable},
		function(data)
		{
			modified=false;
			alertify.success(data);
		//	setTimeout(function(){ window.location.reload(0); }, 4000); 

		});
	}
	return false;
}

function direvento(){
    
	a=document.getElementById('coddocumento').value;
	b=document.getElementById('codetipodir3').value;
	c=document.getElementById('d_calle_av3').value;
	d=document.getElementById('d_localidad3').value;
	e=document.getElementById('d_sector3').value;
	h=document.getElementById('d_urbanizacion3').value;
	i=document.getElementById('d_nlocal3').value;
	j=document.getElementById('d_estado3').value;
	k=document.getElementById('d_codepostal3').value;
	l=document.getElementById('d_ptoref3').value;
	m=document.getElementById('fechacarga').value;
	n=document.getElementById('prefijo').value;
	namearch=document.getElementById('namearch').value;
     if(j==0){
	alertify.error('Debe seleccionar un Estado');
 	$('#d_estado3').focus().select();
	$('#d_estado3').select().css("border-color","red" );
	return false;
     }

	if ((c||d||l||e||h||i||j||k)=="") {

	 alertify.error("Error,los campos estan vacios.");
        return false;
		}else{
	var variable=a+'/'+b+'/'+c+'/'+d+'/'+e+'/'+h+'/'+i+'/'+ j+'/'+k+'/'+l+'/'+m+'/'+n+'/'+namearch;

	$.post('view/comercializacion/gestion/direcciones.php',{postvariable:variable},
		function(data)
		{
			modified=false;
			alertify.success(data);

		//	setTimeout(function(){ window.location.reload(0); }, 4000); 

		});
	}
	return false;
}
function dirinstall(){

	coddocumento=document.getElementById('coddocumento').value;
	codetipodir4=document.getElementById('codetipodir4').value;
	d_calle_av1=document.getElementById('edit_calleav').value;
	d_localidad1=document.getElementById('edit_localidad').value;
	d_sector1=document.getElementById('edit_sector').value;
	d_urbanizacion1=document.getElementById('edit_urbanizacion').value;
	d_nlocal1=document.getElementById('edit_nlocal').value;
	d_estado1=document.getElementById('edit_estado').value;
	d_codepostal1=document.getElementById('edit_codigopostal').value;
	d_ptoref1=document.getElementById('edit_ptoref').value;
	m=document.getElementById('fechacarga').value;
	n=document.getElementById('prefijo').value;
	namearch=document.getElementById('namearch').value;
	usuario=document.getElementById('usuario').value;
	id_registro=document.getElementById('id_registro').value;
	returnadm=document.getElementById('returnadm').value;
	
if ((d_calle_av1||d_localidad1||d_urbanizacion1||d_sector1||d_nlocal1||d_codepostal1||d_ptoref1)=="") {
	 alertify.error("Error, todos los campos estan vacios.");
      return false;
}
else if(d_estado1==0){
	alertify.error('Error, Debe seleccionar un Estado');
 	$('#edit_estado').focus().select();
	$('#edit_estado').select().css("border-color","red" );
	return false;
     }
else if(d_localidad1==""){
	alertify.error('Error, Debe seleccionar un Municipio');
 	$('#edit_localidad').focus().select();
	$('#edit_localidad').select().css("border-color","red");
	return false;
     }
else if(d_sector1==""){
	alertify.error('Error, Debe seleccionar una Parroquia');
 	$('#edit_sector').focus().select();
	$('#edit_sector').select().css("border-color","red");
	return false;
     }else if((d_calle_av1&&d_localidad1&&d_urbanizacion1&&d_sector1&&d_nlocal1&&d_codepostal1&&d_ptoref1)==""){
	alertify.error('Error, Debe colocar la dirección completa');
	return false;
     }  

else{
	var variable=coddocumento+'/-/-/-'+codetipodir4+'/-/-/-'+d_calle_av1+'/-/-/-'+d_localidad1+'/-/-/-'+d_sector1+'/-/-/-'+d_urbanizacion1+'/-/-/-'+d_nlocal1+'/-/-/-'+ d_estado1+'/-/-/-'+d_codepostal1+'/-/-/-'+d_ptoref1+'/-/-/-'+m+'/-/-/-'+n+'/-/-/-'+namearch+'/-/-/-'+usuario+'/-/-/-'+id_registro;
	$.post('view/comercializacion/gestion/direccioninstalacion.php',{postvariable:variable},
		function(data)
		{
			 modified=false;
			 alertify.success(data);
		     setTimeout(function(){ window.location.reload(0); }, 4000); 

		});
	}
	return false;
}

//guarda gestion
function agregargestion(){
	gest_nafiliado=document.getElementById('afiliacion').value;
	gest_ejebanco=document.getElementById('ejebanco').value;
	gest_ejecutivointeligensa=document.getElementById('vendedoresinteligensa1').value;
	gest_estatuspos=document.getElementById('estatuspos').value;
	gest_observacion=document.getElementById('gest_observacion').value;
	gest_coddocumento=document.getElementById('coddocumento').value;
	gest_namearch=document.getElementById('namearch').value;
	gest_usuario=document.getElementById('usuario').value;
	id_registro=document.getElementById('id_registro').value;
	estatusmigra=document.getElementById('clientemigra');
	comodato=document.getElementById('clientecomodato');
	estatusvip=document.getElementById('clientevip');

	//alert(id_registro);

	if(estatusmigra== null || estatusmigra===null){
		clientemigra=0;
	}else
		if (document.getElementById('clientemigra').checked){
			clientemigra=document.getElementById('clientemigra').value;
		}else
			clientemigra=0;
			
	if(comodato== null || comodato===null){
		clientecomodato=0;
	}else
		if (document.getElementById('clientecomodato').checked){
			clientecomodato=document.getElementById('clientecomodato').value;
		}else
			clientecomodato=0;

	if(comodato== null || comodato===null){
		clientecomodato=0;
	}else
		if (document.getElementById('clientecomodato').checked){
			clientecomodato=document.getElementById('clientecomodato').value;
		}else
			clientecomodato=0;

	if(estatusvip== null || estatusvip===null){
		clientvip=0;
	}else
		if (document.getElementById('clientevip').checked){
			clientvip=document.getElementById('clientevip').value;
		}else
			clientvip=0;
	//alert(clientvip);
	if (gest_ejebanco=="") {
		alertify.error('Error el afiliado debe tener un ejecutivo por banco asignado.');
		// $('#nombrejecutivo').focus();
		// $('#nombrejecutivo').input().css("border-color","red" );
		 document.getElementById('nombrejecutivo').style = 'border-color:red;'
		return false;
	}
	else if(gest_estatuspos=="0"){
	    alertify.error('Debe seleccionar una opción');
		$('#estatuspos').focus().select();
		$('#estatuspos').select().css("border-color","red" );
		return false;

	}else{


	var variable=gest_nafiliado+'/'+gest_ejebanco+'/'+gest_ejecutivointeligensa+'/'+gest_estatuspos+'/'+gest_observacion+'/'+gest_coddocumento+'/'+gest_namearch+'/'+gest_usuario+'/'+id_registro+'/'+clientemigra+'/'+clientecomodato+'/'+clientvip;
		$.post('view/comercializacion/gestion/guardargestion.php',{postvariable:variable},
		function(data)
		{
             
			// console.log(data);
			
			if(data.valor==1){
			// alertify.success(data);
			alertify.success("Gesti&oacute;n guardada con &eacute;xito");
			setTimeout(function(){ window.location.reload(0); }, 5000); 
			}
			 if (data.valor==0) {
			alertify.error("Error, No se pudo guardar la Gesti&oacute;n");

			}
			if (data.valor==2){

				alertify.error("Error, primero debe seleccionar un equipo");
			}

		});
}

	return false;
}


function asignarRifBanco() {

	id_registro=document.getElementById('idregistro').value;
	bancorif=document.getElementById('idrif').value;

	var variable=id_registro+'/'+bancorif;
	//alert(cantidadposs);
	$.post('view/Administracion/facturacionpos/guardarRifBanco.php',{postvariable:variable},

		function(data)
		{
	//setTimeout(function(){ window.location.reload(0); }, 900);		

})

}



function eliminargestion(coddocumento,nrogestion,nombrearchivo,count){
	
	var estatus=document.getElementById('estatuspos'+[count]).value;
    var observaciones=document.getElementById('g_observacion'+[count]).value;
    var usuario=document.getElementById("usuario").value;
    
	alertify.set({ labels: {
		ok: "Editar",
		cancel: "Cancelar"
	}
});

	var variable =coddocumento+'/'+nrogestion+'/'+nombrearchivo+'/'+estatus+'/'+observaciones+'/'+usuario;
	alertify.set({ buttonReverse: true });
	alertify.confirm("&#191;Desea editar la gestión: "+count+"?", function (e) {
		if (e) {
			$.post('view/comercializacion/gestion/eliminar.php',{postvariable:variable},
				function(data)
				{
					if (data==1) {
						alertify.success("Gestión editada con &Eacute;xito!.");
					}
					else if(data==0){
						alertify.error("Solo puede editar gestiones cargadas el mismo d&iacute;a, Consulte con el administrador del Sistema!.");
					}
					setTimeout(function(){ window.location.reload(0); }, 5000);
				});
} else {
	alertify.error("No se ha editado la gestión");
	return false;
}
});
	return false;
}


function validararchivotxt(){

	var	archivo=document.getElementById('archivo').value;
	var b = archivo.split('.');

	if(document.getElementById('archivo').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO .Txt A CARGAR.!");
		document.getElementById('archivo').focus();
		return false;
	}

	if(b[b.length-1] != 'TXT')
	{
		alertify.alert('Error: El archivo debe ser .txt');
		document.getElementById('archivo').focus();
		return false;
	}

	else{
		document.formarchivo.submit();
	}
}

function buscarpreparaciontxt(){

	window.location.href='?cargar=buscar'
}
 


//validar rango de horas
function validarhorashorario() { 
var horadesde;
var horahasta;

var a = document.querySelectorAll("input");
for(var b in a){
 var c = a[b];
if(typeof c=="object"){
  c.onchange = function (){
 //    console.log(this);
     horadesde=this;


  }
 }
}      
  // var horadesde = document.getElementById('lunh1').value;
  // var horahasta = document.getElementById('lunh2').value;

   horadesde = horadesde.split(":"); 
   horahasta = horahasta.split(":"); 
   // alert(res+x);

                // Obtener horas y minutos (hora 1) 
                var hh1 = parseInt(horadesde[0],10); 
                var mm1 = parseInt(horadesde[1],10); 
                var ss1 = parseInt(horadesde[2],10); 
                // Obtener horas y minutos (hora 2) 
                var hh2 = parseInt(horahasta[0],10); 
                var mm2 = parseInt(horahasta[1],10); 
                var ss2 = parseInt(horahasta[2],10); 
                // Comparar 
                // if (hh1<hh2 || (hh1==hh2 && mm1<mm2)) 
   
                if (hh1>hh2 || (hh1==hh2 && mm1>mm2) || (hh1==hh2 && mm1==mm2 && ss1>ss2)) 
                {
                alertify.error ("La hora final no puede ser Mayor a la hora inicial"); 
               // document.getElementById('produccion').disabled=true;
                return false;
                }

                //    alert ("sHora1 MAYOR sHora2"); 
                else
                {

                	alertify.success ("Rango de horas es validado."); 
                	return false; 
                }    
}

function modales(){
  // Add minus icon for collapse element which is open by default
  $(".collapse.in").each(function(){
    $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
  });

  // Toggle plus minus icon on show hide of collapse element
  $(".collapse").on('show.bs.collapse', function(){
    $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hide.bs.collapse', function(){
    $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });
 
  }

correosvalidos=0
function validarEmail(elemento){
  // document.getElementById('updatoscontacto').disabled=true;
  var texto = document.getElementById(elemento.id).value;
  var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  var corp =0;
  var per =0;
  var com =0;
   if(document.getElementById(elemento.id).value == "") {
      document.getElementById(elemento.id).style = 'border-color:none;';

      // document.getElementById('updatoscontacto').disabled=false;
   }
   else if (!regex.test(texto)) {
      document.getElementById("resultcorreo").innerHTML = "Correo invalido";
      // document.getElementById('updatoscontacto').disabled=true;
      document.getElementById(elemento.id).style = 'border-color:red;';     
       if(elemento.id=='email1'){ corp =1;  }
       if (elemento.id=='email2') { per=1; }
       if (elemento.id=='email3') { com=1; }
       correosvalidos=1;

      }

   else if (regex.test(texto) || (corp ==0) || (per ==0)  ||(com ==0)   ){
      // document.getElementById('updatoscontacto').disabled=false;
      document.getElementById("resultcorreo").innerHTML = "";
      document.getElementById(elemento.id).style = 'border-color:green;';

	correosvalidos=0

  }


}


//search for Rif or razon social
function redirectResult(){
 	alertify.error("No existen resultados para la búsqueda");
    window.setTimeout( 'redirectUrl()', 3000 ); // 4 seconds
};


function resultadoBanesco(){
 	
		alertify.error("El registro debe poseer Afiliación para continuar el proceso");
    window.setTimeout( 'redirectUrl()', 5000 ); // 4 seconds
};

function rechazoBanesco(){
 	
		alertify.error("El registro se encuentra Declinado, no puede continuar con el proceso");
    window.setTimeout( 'redirectUrl()', 5000 ); // 4 seconds
};

function redirectUrl(){
    //open url that your need
     window.location.href ='?cargar';
};

function disabledBtn(){
     $('input[type="submit"]').attr('disabled','disabled');
     $('input[type="text"]').keypress(function(){
            if($(this).val() != ''){
               $('input[type="submit"]').removeAttr('disabled');
            }
     });
 }

var modified;

function modificaciones(){
  
  $('#estatuspos').change(function()
	{
		var estatuspos = $(this).val();
		//alert(selectperfil);
		if(estatuspos==1 || estatuspos===1){
			$('.ocultarmigracion').removeClass('hidden');
		}
			
	});
}

  function validarCampos(id) {
	alertify.set({ labels: {
		ok: "Aceptar",
		cancel: "Cancelar"
	}
	});
  //alert(id);
  if (modified)
{
      alertify.confirm("Usted ha efectuado cambios, desea guardarlos?", function (e) {
        if (e) {
         // alertify.success("Usted ha decidido salir del formulario sin guardar.");
          alertify.custom = alertify.extend("custom");
          alertify.custom("Usted ha decidido salir del formulario sin guardar.");
             $('#basicmodal').modal('hide');
        } else {
          alertify.error("Aseguresé de guardar antes de salir.");
        }
      });
  }
      return false;
}




function SalirPermanecer(id) {
	if (modified==true){
			alertify.set({ labels: { ok: "Salir", cancel: "Permanecer" } });
			alertify.confirm("Usted esta intentando salir sin antes haber guardado los campos modificados", function (e) {
		
				if (e) {
					
					alertify.error("Usted decidió salir sin guardar los campos modificados");
					if(id.value==2){
						modified=false;
						$('#contactmodal').modal('hide');
						
					}
					if (id.value==3) {
						modified=false;
						$('#caracteristicasmodal').modal('hide');
						// $('#modalprueba').modal('toggle');
						
					}
					if (id.value==6) {
						modified=false;

						$('#directionstmodal').modal('hide');
											}
					if (id.value==7) {
						modified=false;
						$('#equiposmodal').modal('hide');
						
					}
					
				} else {
					alertify.success("Le sugerimos guardar antes de salir del formulario");
				}
		
			});
			return false;
		  }//end modified
		  	else{
  		
            if(id.value==2){
            	$('#contactmodal').modal('hide');
            	modified=false;
            }
            if (id.value==3) {
            	$('#caracteristicasmodal').modal('hide');
            	modified=false;
            }
            if (id.value==6) {
            	$('#directionstmodal').modal('hide');
            	modified=false;
            }
            if (id.value==7) {
            	$('#equiposmodal').modal('hide');
            	modified=false;
            }
            if (id.value==8) {
            	$('#factura').hide();
            	$('#devolucion').hide();
            	$('#devoluciontotal').hide();
            	$('#percances').hide();
            	document.getElementById("check").checked = false;
            	document.getElementById("checkdev").checked = false;
            	document.getElementById("devoluciontotal").checked = false;
            	document.getElementById("checkpercances").checked = false;
            	document.getElementById('guardarfacturacion').disabled=false;
            }
            if (id.value==9) {
            	$('#facturamifi').hide();
            	$('#devolucionmifi').hide();
            	$('#devoluciontotalmifi').hide();
            	document.getElementById("checkmifi").checked = false;
            	document.getElementById("checkdevmifi").checked = false;
            	document.getElementById("checkdevtotalmifi").checked = false;
            	document.getElementById('guardarfacturacionmifi').disabled=false;
            }
						if(id.value==10){
            	$('#modalencuesta').modal('hide');
            	modified=false;
            }

  	}
  return false;
}

function AnteriorPermanecer(id) {
	if (modified==true){
			alertify.set({ labels: { ok: "Ir", cancel: "Permanecer" } });
			alertify.confirm("Hay cambios sin guardar en el formulario ¿Desea ir al anterior de todas formas?", function (e) {
		
				if (e) {
			alertify.error("Usted accedio al formulario sin guardar el anterior");
			if (id.value==3){ 
			modified==false;
            $('#contactmodal').modal('show');
            $('#caracteristicasmodal').modal('hide');
            } 
            if (id.value==6){ 
            modified==false;
            $('#caracteristicasmodal').modal('show');
            $('#directionstmodal').modal('hide');            
            }
            if (id.value==7){ 
            modified==false;
            $('#directionstmodal').modal('show');
            $('#equiposmodal').modal('hide');
           }	
		 } else {
					alertify.success("Le sugerimos guardar antes de salir del formulario");
				}
		
			});
			return false;
		  }//end modified
		  	else{
  		
           if (id.value==3){ 
           	modified=false;
            $('#contactmodal').modal('show');
            $('#caracteristicasmodal').modal('hide');
            } 
            if (id.value==6){ 
            modified=false;	
            $('#caracteristicasmodal').modal('show');
            $('#directionstmodal').modal('hide');
            }
            if (id.value==7){ 
            modified=false;
            $('#directionstmodal').modal('show');
            $('#equiposmodal').modal('hide');
           }

  	}
  return false;
}

function SiguientePermanecer(id) {
	if (modified==true){
			alertify.set({ labels: { ok: "Ir", cancel: "Permanecer" } });
			alertify.confirm("Hay cambios sin guardar en el formulario ¿Desea ir al siguiente formulario de todas formas?", function (e) {
		
			if (e) {	
			alertify.error("Usted accedio al formulario sin guardar el anterior");
			if (id.value==2){
			modified==false;	
            $('#contactmodal').modal('hide');
            $('#caracteristicasmodal').modal('show');
            } 
            if (id.value==3){
            modified==false; 
            $('#caracteristicasmodal').modal('hide');
            $('#directionstmodal').modal('show');
            }
            if (id.value==6){
            modified==false; 
            $('#directionstmodal').modal('hide');
            $('#equiposmodal').modal('show');
           }
					
			} else {
					alertify.success("Le sugerimos guardar antes de salir del formulario");
				}
		
			});
			return false;
		  }//end modified
		  	else { 
  		
           if (id.value==2){ 
           	modified=false;
            $('#contactmodal').modal('hide');
            $('#caracteristicasmodal').modal('show');
            } 
            if (id.value==3){
            modified=false; 
            $('#caracteristicasmodal').modal('hide');
            $('#directionstmodal').modal('show');  
            }
            if (id.value==6){
            modified=false; 
            $('#directionstmodal').modal('hide');
            $('#equiposmodal').modal('show');
            
           }

  	}
  return false;
}


function gralControlModal(id){

alertify.set({ labels: {
		ok: "Guardar",
		cancel: "Salir"
	}
	});
  //alert(id.id+'--valor: '+id.value+'--modificado: '+modified);
  if (modified==true){
  	 
if (id.id=='cerrar'){
      alertify.confirm("Usted ha efectuado cambios, desea guardarlos?", function (e) {
        if (e) {
        	//modified=false;
            // alertify.success("Usted ha decidido salir del formulario sin guardar.");
            alertify.custom = alertify.extend("custom");
            // if (id.value==1){ 
            // $('#basicmodal').modal('hide');
            // } 
            if (id.value==2){ 
    		if((document.getElementById('tlfmovil1').value== "") &&( document.getElementById('tlfmovil2').value=="") ) {
     		alertify.error('Debe agregar por lo menos un n&uacute;mero de tel&eacute;fono movil');
    		 return false;
  		  	} 
    		else if((document.getElementById('emailcorpo').value == "")&&(document.getElementById('emailcomer').value=="")&&(document.getElementById('emailperson').value =="")){
 			alertify.error('Debe agregar por lo menos una dirección de correo');
    		return false;
 			}
 			else if (document.getElementById('nombre').value==""){
 			alertify.error('Debe agregar el nombre de la persona de contacto');
    		return false;
 			}
 			else if(document.getElementById('nombre').value == ""){
 				alertify.error('Debe agregar el apellido de la persona de contacto');
 			return false; 
    		}
   		 else{
            $(document).ready(comerdatoscontacto);
            $('#contactmodal').modal('hide');
          	}
            } //finaliza cerrar 2
            //caracteristicas de la empresa
            if (id.value==3){
            if(document.getElementById('acteconomica').value ==""){
            alertify.error("Error,el campo actividad económica esta vacio.");

            }
            
         else{
            $(document).ready(comerdatosempresa);		
            $('#characteristmodal').modal('hide');
            }
            }//end cerrar 3
            if (id.value==6){
                coddocumento=document.getElementById('coddocumento').value;
             	codetipodir1=document.getElementById('codetipodir1').value;
                d_calle_av1=document.getElementById('d_calle_av1').value;
            	d_localidad1=document.getElementById('d_localidad1').value;
            	d_sector1=document.getElementById('d_sector1').value;
            	d_urbanizacion1=document.getElementById('d_urbanizacion1').value;
            	d_nlocal1=document.getElementById('d_nlocal1').value;
            	d_estado1=document.getElementById('d_estado1').value;
                d_codepostal1=document.getElementById('d_codepostal1').value;
            	d_ptoref1=document.getElementById('d_ptoref1').value;
            	m=document.getElementById('fechacarga').value;
            	n=document.getElementById('prefijo').value;
            	namearch=document.getElementById('namearch').value;

            if ((d_calle_av1||d_localidad1||d_estado1||d_urbanizacion1||d_nlocal1||d_estado1 ||d_codepostal1||d_ptoref1)=="") {
            	 alertify.error("Error,los campos estan vacios.");
                 return false;
            }
            else if(d_localidad1==""){
            	alertify.error('Error, debe seleccionar un Municipio');
             	$('#edit_localidad').focus().input();
            	$('#edit_localidad').input().css("border-color","red");
            	return false;
                 }
            else if(d_sector1==""){
            	alertify.error('Error,debe seleccionar una Parroquia');
             	$('#edit_sector').focus().input();
            	$('#edit_sector').input().css("border-color","red");
            	return false;
                 }
            else if(d_estado1==0){
            	alertify.error('Error, Debe seleccionar un Estado');
             	$('#edit_estado').focus().select();
            	$('#edit_estado').select().css("border-color","red" );
            	return false;
                 }
             else{    		
            $(document).ready(dirfiscal);				
            $('#directionstmodal').modal('hide');
            }
            }// finaliza cerrar 6

            if (id.value==7){ 
            $('#equiposmodal').modal('hide');
            }
            //alertify.custom("Usted ha guardado correctamente antes de cerrar."); 
            //pendiente por validación
        } else {
          alertify.error("Usted ha salido del formulario sin guardar los cambios realizados.");
          $('.modal').modal('hide');
        }
      });
  }

 if( id.id=='siguiente')
{
      alertify.confirm("Usted ha efectuado cambios, desea guardarlos?", function (e) {
        if (e) {
         // alertify.success("Usted ha decidido salir del formulario sin guardar.");
         	 modified=false;
         	 alertify.custom = alertify.extend("custom");
         
            if (id.value==2){ 
    		if((document.getElementById('tlfmovil1').value== "") &&( document.getElementById('tlfmovil2').value=="") ) {
     		alertify.error('Debe agregar por lo menos un n&uacute;mero de tel&eacute;fono movil');
    		 return false;
  		  	} 
    		else if((document.getElementById('emailcorpo').value == "")&&(document.getElementById('emailcomer').value=="")&&(document.getElementById('emailperson').value =="")){
 			alertify.error('Debe agregar por lo menos una dirección de correo');
    		return false;
 			}
 			else if((document.getElementById('interes').value=="")){
 			alertify.error('Debe indicar interes');
 			$('#interes').focus().select();
 			$('#interes').select().css("border-color","red" );
    		return false;
 			}
   		 else{
            $(document).ready(comerdatoscontacto);
            $('#contactmodal').modal('hide');
            $('#characteristmodal').modal('show');
          	}
            } //finaliza siguiente 2
            //caracteristicas de la empresa
            if (id.value==3){
            if(document.getElementById('cactividad').value ==""){
            alertify.error("Error,el campo Ciclo de Actividad esta vacio.");

            }
            else if (document.getElementById('fcaja').value =="") {
            alertify.error("Error,el campo Flujo de Caja esta vacio.");

            }
            else if (document.getElementById('rinventario').value ==""){
            alertify.error("Error,el campo Rotación de Inventario esta vacio.");
            return false;
            }
           else{
           	$(document).ready(comerdatosempresa);	
            $('#characteristmodal').modal('hide');
            $('#directionstmodal').modal('show');
           }
           } // finaliza siguiente 3
            if (id.value==6){ 
                coddocumento=document.getElementById('coddocumento').value;
             	codetipodir1=document.getElementById('codetipodir1').value;
                d_calle_av1=document.getElementById('d_calle_av1').value;
            	d_localidad1=document.getElementById('d_localidad1').value;
            	d_sector1=document.getElementById('d_sector1').value;
            	d_urbanizacion1=document.getElementById('d_urbanizacion1').value;
            	d_nlocal1=document.getElementById('d_nlocal1').value;
            	d_estado1=document.getElementById('d_estado1').value;
                d_codepostal1=document.getElementById('d_codepostal1').value;
            	d_ptoref1=document.getElementById('d_ptoref1').value;
            	m=document.getElementById('fechacarga').value;
            	n=document.getElementById('prefijo').value;
            	namearch=document.getElementById('namearch').value;

            if ((d_calle_av1||d_localidad1||d_estado1||d_urbanizacion1||d_nlocal1||d_estado1 ||d_codepostal1||d_ptoref1)=="") {
            	 alertify.error("Error,los campos estan vacios.");
                 return false;
            }
            else if(d_localidad1==""){
            	alertify.error('Error, el campo Ciudad no puede estar vacio');
             	$('#edit_localidad').focus().input();
            	$('#edit_localidad').input().css("border-color","red");
            	return false;
                 }
            else if(d_sector1==""){
            	alertify.error('Error, el campo Sector, no puede estar vacio');
             	$('#edit_sector').focus().input();
            	$('#edit_sector').input().css("border-color","red");
            	return false;
                 }
            else if(d_estado1==0){
            	alertify.error('Error, Debe seleccionar un Estado');
             	$('#edit_estado').focus().select();
            	$('#edit_estado').select().css("border-color","red" );
            	return false;
                 }
             else{
            $(document).ready(dirfiscal);
            $('#directionstmodal').modal('hide');
            $('#equiposmodal').modal('show');
            }
            }
           // alertify.custom("Usted ha decidido ir al siguiente formulario sin guardar el anterior.");

        } else {
         alertify.error("Usted accedió al siguiente formulario sin guardar el anterior.");
            if (id.value==2	) {
            $('#contactmodal').modal('hide');
            $('#characteristmodal').modal('show');
            modified=false;
            }
            else if (id.value ==3) {
          	$('#characteristmodal').modal('hide');
            $('#directionstmodal').modal('show');
            modified=false;
            }
            else if (id.value ==6) {
            $('#directionstmodal').modal('hide');
            $('#equiposmodal').modal('show');
            modified=false;
            }
        }
      });
  }
   if( id.id=='anterior')
{
      alertify.confirm("Puede haber cambios sin guardar en el formulario ¿Desea ir al anterior de todas formas?", function (e) {
        if (e) {
         // alertify.success("Usted ha decidido salir del formulario sin guardar.");
         	 modified=false;
         	 alertify.custom = alertify.extend("custom");
            if (id.value==2){ 
            $('#basicmodal').modal('hide');
            $('#contactmodal').modal('show');
            } 
            if (id.value==3){
            if(document.getElementById('cactividad').value ==""){
            alertify.error("Error,el campo Ciclo de Actividad esta vacio.");

            }
            else if (document.getElementById('fcaja').value =="") {
            alertify.error("Error,el campo Flujo de Caja esta vacio.");

            }
            else if (document.getElementById('rinventario').value ==""){
            alertify.error("Error,el campo Rotación de Inventario esta vacio.");
            return false;
            }
           else{
           	$(document).ready(comerdatoscontacto);	
            $('#contactmodal').modal('hide');
 
           }
           } 
           // direccion fiscal
            if (id.value==6){
            coddocumento=document.getElementById('coddocumento').value;
         	codetipodir1=document.getElementById('codetipodir1').value;
            d_calle_av1=document.getElementById('d_calle_av1').value;
        	d_localidad1=document.getElementById('d_localidad1').value;
        	d_sector1=document.getElementById('d_sector1').value;
        	d_urbanizacion1=document.getElementById('d_urbanizacion1').value;
        	d_nlocal1=document.getElementById('d_nlocal1').value;
        	d_estado1=document.getElementById('d_estado1').value;
            d_codepostal1=document.getElementById('d_codepostal1').value;
        	d_ptoref1=document.getElementById('d_ptoref1').value;
        	m=document.getElementById('fechacarga').value;
        	n=document.getElementById('prefijo').value;
        	namearch=document.getElementById('namearch').value;

        if ((d_calle_av1||d_localidad1||d_estado1||d_urbanizacion1||d_nlocal1||d_estado1 ||d_codepostal1||d_ptoref1)=="") {
        	 alertify.error("Error,los campos estan vacios.");
             return false;
        }
        else if(d_localidad1==""){
        	alertify.error('Error, el campo Ciudad no puede estar vacio');
         	$('#edit_localidad').focus().input();
        	$('#edit_localidad').input().css("border-color","red");
        	return false;
             }
        else if(d_sector1==""){
        	alertify.error('Error, el campo Sector, no puede estar vacio');
         	$('#edit_sector').focus().input();
        	$('#edit_sector').input().css("border-color","red");
        	return false;
             }
        else if(d_estado1==0){
        	alertify.error('Error, Debe seleccionar un Estado');
         	$('#edit_estado').focus().select();
        	$('#edit_estado').select().css("border-color","red" );
        	return false;
             }
         else{
        $(document).ready(dirfiscal);
        $('#directionstmodal').modal('hide');
        $('#characteristmodal').modal('show');
        }     
        }
            if (id.value==7){ 
            $('#directionstmodal').modal('show');
            $('#equiposmodal').modal('hide');
            }
            //alertify.custom("Usted ha decidido ir al siguiente formulario sin guardar el anterior.");

        } else {
          alertify.error("Aseguresé de guardar antes de ir al siguiente formulario.");
        }
      });
  }
 }//modified
  else if (id.id=='cerrar'){  
            if (id.value==2){ 
            $('#contactmodal').modal('hide');
            } 
            if (id.value==3){ 
            $('#characteristmodal').modal('hide');
            }
            if (id.value==6){ 
            $('#directionstmodal').modal('hide');
            }
            if (id.value==7){ 
            $('#equiposmodal').modal('hide');
            }
        
  	}
  	else if(id.id=='siguiente'){
            if (id.value==2){ 
            $('#contactmodal').modal('hide');
            $('#characteristmodal').modal('show');
            } 
            if (id.value==3){ 
            $('#characteristmodal').modal('hide');
            $('#directionstmodal').modal('show');
            }
            if (id.value==6){ 
            $('#directionstmodal').modal('hide');
            $('#equiposmodal').modal('show');
            }

  	}
  	else{
  		
            if (id.value==3){ 
            $('#contactmodal').modal('show');
            $('#characteristmodal').modal('hide');
            } 
            // if (id.value==4){ 
            // $('#characteristmodal').modal('show');
            // $('#dirinstallmodal').modal('hide');
            // }
            // if (id.value==5){ 
            // $('#dirinstallmodal').modal('show');
            // $('#representmodal').modal('hide');
            // }
            if (id.value==6){ 
            $('#characteristmodal').modal('show');
            $('#directionstmodal').modal('hide');
            }
            if (id.value==7){ 
            $('#directionstmodal').modal('show');
            $('#equiposmodal').modal('hide');
           }

  	}
      return false;
}

function linkdirfiscal(){
            $('#directionstmodal').modal('show');
            $('#myModal').modal('hide');
           
}



function procesarclientesbanco(){
	var operacion =document.getElementById('operacion').value;
	var namecliente =document.getElementById('namecliente').value;
	var cliente =document.getElementById('cliente').value;
	var fecha =document.getElementById('fecha').value;
	var usuario =document.getElementById('usuario').value;
	var variable= operacion+'/'+cliente+'/'+fecha+'/'+usuario;
	
	alertify.set({ labels: {
		ok: "Procesar",
		cancel: "Cancelar"
	}
	});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Procesar la Información del Banco: "+namecliente+"?", function (e) {
		if (e) {
			$.post('view/Reportes/afiliado/procesarinformacion.php',{postinformacion:variable},
				function(data)
				{
					alertify.success(data);
					setTimeout(function(){ window.location.reload(1); }, 1000);
				});
		} else {
			alertify.error("No se ha procesado la información del Banco:"+namecliente);
			return false;
		}
	})
	return false;
}

function buscarseriales(){

	if(document.getElementById('fecha').value == "")
	{
		alertify.alert("DEBE SELECCIONAR LA FECHA A CONSULTAR!.");
		document.getElementById('fecha').focus();
		return false;

		
	}	
	else{
	var variable=document.getElementById('fecha').value;
	window.location.href='?cargar=buscarpreparacion&var='+variable
	}
}

function buscarserialsimcard(){

	if(document.getElementById('fecha').value == "")
	{
		alertify.alert("DEBE SELECCIONAR LA FECHA A CONSULTAR!.");
		document.getElementById('fecha').focus();
		return false;

		
	}	
	else{
	var variable=document.getElementById('fecha').value;
	window.location.href='?cargar=buscarpreparacion&var='+variable
	}
}

function buscarserialesmifi(){

	if(document.getElementById('fecha').value == "")
	{
		alertify.alert("DEBE SELECCIONAR LA FECHA A CONSULTAR!.");
		document.getElementById('fecha').focus();
		return false;

		
	}	
	else{
	var variable=document.getElementById('fecha').value;
	window.location.href='?cargar=buscarpreparacion&var='+variable
	}
}




function reporteseriales(){

	if(document.getElementById('fecha').value == "")
	{
		alertify.alert("DEBE SELECCIONAR LA FECHA A CONSULTAR!.");
		document.getElementById('fecha').focus();
		return false;

		
	}	
	else{
	var variable=document.getElementById('fecha').value;
	window.location.href='?cargar=buscarinventario&var='+variable
	}
}

function mostrarBusquedaRif(){
      if ($('#panelbusquedarif').css('display') == 'none') {
          $('#panelbusquedarif').css('display', 'block') ;
          $('#panelbusquedafecha').css('display', 'none')  ;
      }
      else {
          $('#panelbusquedarif').css('display', 'none')  ;
      }
}

function mostrarBusquedaFecha(){
      if ($('#panelbusquedafecha').css('display') == 'none') {
          $('#panelbusquedafecha').css('display', 'block') ;
          $('#panelbusquedarif').css('display', 'none') 
      }
      else {
          $('#panelbusquedafecha').css('display', 'none')  ;
      }
}

function crearperfil(){
      if ($('#crearperfilgeo').css('display') == 'none') {
          $('#crearperfilgeo').css('display', 'block') ;
      }
      else {
          $('#crearperfilgeo').css('display', 'none')  ;
      }
}

function mantenimientofechas(){

	var el_valor =document.getElementById('valor').value;
 	// Lista de Clientes
	$.post( 'view/ServicioMantenimiento/fechamantenimiento.php', { valor: el_valor} ).done( function(respuesta)
	{
		$( '#fecha' ).html( respuesta );
	});

};

function mantenimiento(){

	window.location.href='?cargar=cargaindividual'
   
}

function buscarmantenimiento(){

	if(document.getElementById('clientes').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;

		
	}	
	else{
	var variable=document.getElementById('clientes').value;
	window.location.href='?cargar=buscarmantenimiento&var='+variable
	}
}


function generarreportedomic(){

	variable=document.getElementById('status').value+'/'+document.getElementById('modalidad').value;

	if((document.getElementById('status').value == "") || (document.getElementById('modalidad').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCIÓN!.");
		return false;		
	}	

	else{
	//alert(variable)	
	window.location.href='?cargar=generarreporte&var='+variable
	}
}



function buscarconfigPOS(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))
	
	{
		window.location.href='?cargar=buscarservitec&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscarconfigpos&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarconfigpos&var='+variable
	}
}

function buscarservitec(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value+'/'+document.getElementById('region').value;
//alert(variable);

	if(document.getElementById('region').value == "")
	{
		alertify.alert("DEBE SELECCIONAR REGIÓN!.");
		document.getElementById('region').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") && (document.getElementById('region').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('region').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('region').value != ""))
	{
		alertify.alert("DEBE SELECCIONAR BANCO.");
		document.getElementById('region').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "") && (document.getElementById('region').value != ""))

	{
		window.location.href='?cargar=buscarInstalar&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == "") && (document.getElementById('region').value != ""))
	{
		window.location.href='?cargar=buscarInstPOS&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != "") && (document.getElementById('region').value != ""))
	{
		window.location.href='?cargar=buscarInstPOS&var='+variable
	}
}


function buscarperfiles(){
	var perfil=document.getElementById('perfiles').value;
	var dir=document.getElementById('dir').value;
	var variable=perfil+'/'+dir;
	if(document.getElementById('perfiles').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL PERFIL A CONSULTAR!.");
		document.getElementById('perfiles').focus();
		return false;

		
	}	
	else{
	
	window.location.href='?cargar=gestionmantenimiento&var='+variable;
	}
}

function mostrarRif(){
      if ($('#serviciobusquedarif').css('display') == 'none') {
          $('#serviciobusquedarif').css('display', 'block') ;
      }
      else {
          $('#serviciobusquedarif').css('display', 'none')  ;
      }
}

function buscargestiontecnica(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != "")))
	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
}

// copiar esto adaptado a repoorte facturacion 
function buscarfacturacioncliente(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value+'/'+document.getElementById('tipousuario').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('usuario').value+'/'+document.getElementById('coordinador').value+'/'+document.getElementById('coordinadordos').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))

	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
}
// A

function buscarfacturacionclientereporte(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('estatus').value;
//alert(variable);
if((document.getElementById('clientes').value == "") && (document.getElementById('estatus').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}

if((document.getElementById('clientes').value != "") && (document.getElementById('estatus').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('estatus').value != ""))
	{
		window.location.href='?cargar=generarreporteestatusfactura&var='+variable
	}
}


// busqueda por rif o razon social
function buscarclienteRifFacturacion(){
	if(document.getElementById('nrorif').value == "")
	{   
		 // .ajs-message.ajs-custom { color: #31708f;  background-color: #d9edf7;  border-color: #31708f; }
		alertify.set('notifier','position', 'bottom-right');
		alertify.alert("Debe ingresar la información a buscar.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('nrorif').focus();
		document.getElementById('nrorif').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('nrorif').value+'/'+document.getElementById('tipousuario').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('usuario').value+'/'+document.getElementById('coordinador').value+'/'+document.getElementById('coordinadordos').value;
		window.location.href='?cargar=buscarrif&var='+variable
	}
}

function buscarfacturacionclientemifi(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value+'/'+document.getElementById('tipousuario').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('usuario').value+'/'+document.getElementById('coordinador').value+'/'+document.getElementById('coordinadordos').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))

	{
		window.location.href='?cargar=buscarcliente&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarrecepcion&var='+variable
	}
}

function buscarclienteFacturacionRifMifi(){
	if(document.getElementById('nrorif').value == "")
	{   
		 // .ajs-message.ajs-custom { color: #31708f;  background-color: #d9edf7;  border-color: #31708f; }
		alertify.set('notifier','position', 'bottom-right');
		alertify.alert("Debe ingresar la información a buscar.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('nrorif').focus();
		document.getElementById('nrorif').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('idserial').value+'/'+document.getElementById('tipousuario').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('usuario').value+'/'+document.getElementById('coordinador').value+'/'+document.getElementById('coordinadordos').value;
		window.location.href='?cargar=buscarmifixrif&var='+variable
	}
}



function buscarclienteRifGestionTecnica(){
	if(document.getElementById('nrorif').value == "")
	{   
		 // .ajs-message.ajs-custom { color: #31708f;  background-color: #d9edf7;  border-color: #31708f; }
		alertify.set('notifier','position', 'bottom-right');
		alertify.alert("Debe ingresar la información a buscar.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('nrorif').focus();
		document.getElementById('nrorif').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('nrorif').value+'/'+document.getElementById('tipousuario').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('usuario').value;
		window.location.href='?cargar=buscarrif&var='+variable
	}
}
function redirectrefrecar(){
 	setTimeout(function(){ window.location.reload(0); }, 2000); 
};

//search for Rif or razon social
function redirectResultacuerdo(retornar){
 	alertify.error("Debes Ingresar primero el número de Serial de POS");
    window.setTimeout( 'redirectUrlAcuerdo("'+retornar+'")', 4000 ); // 4 seconds
};
    
function redirectUrlAcuerdo(retornar){
    //open url that your need
     window.location.href ='?cargar=datosserviciosafiliado&var='+retornar;
};

function generararchivo(callback){
var nombre =document.getElementById('nombre').value;
var cliente =document.getElementById('cliente').value;
var variable= nombre;

//var variable1= cliente+'.Mac';
//alert(nombre);
	$.post('view/Administracion/preparacioncliente/buscarpreparacion.php',{postvariable:variable},
	function(data)
	{
	//alert(data);
	//setTimeout(function(){  downloadLink(data); }, 4000);
	var capa = document.getElementById("descarga");
        capa.click();
         callback(cliente);
       //alert(variable1);
      // $.post('view/Administracion/preparacioncliente/eliminararchivo.php',{postvariable:cliente});
	});

}

function eliminararchivo(cliente){
 alertify.alert("Información procesada con Éxito.", function () {
	setTimeout($.post('view/Administracion/preparacioncliente/eliminararchivo.php',{postvariable:cliente}), 2000);
    });
}

function multiplicarPrecioxCantPos(){
  m1 = document.getElementById("costobs_facturacion").value;
  $("#accordionequipos_costos").collapse('toggle');

  valorpunto=parseInt(m1);
  m2 = document.getElementById("totalaprobados").value;
  cantidad= parseInt(m2);

if (m2==0 || m2==""){

      alertify.alert("<b style='color:red;'>No tiene POS Seleccionado.</b><b> Debe seleecionar Equipo y Cantidad </b>", function () {
         

   		setTimeout(function(){ $('#equiposmodal').modal('hide'); }, 0); // cerrar
			setTimeout(function(){  $('#contactmodal').modal('show'); }, 100); // abrir
			setTimeout(function(){$('#accordionequipos_costos').collapse('show'); }, 2000);

      });
}
 
  total = (parseInt(valorpunto) * parseInt(cantidad));
  // alert(total);

  	document.getElementById("totalprecio").value = total;

}
function validarzonas(valor){

	//var preciozona=document.getElementById('preciozona');
	checklist=document.getElementById('check_list'+[valor]);
	//alert(checklist);
	//alert(valor);
		 if( checklist.checked ) 
		 {
			document.getElementById('preciozona'+[valor]).disabled=false;
			document.getElementById('buttoneditar'+[valor]).disabled=false;
		 }
		 else
		 {
			document.getElementById('preciozona'+[valor]).disabled=true;
			document.getElementById('buttoneditar'+[valor]).disabled=true;
		 }
			
		//  var checkboxes = document.getElementById("form1").checkbox;
		
		  var cont = 0; 
		  
		  for (var x=1; x < 4; x++)
		  {
		  	 checklist1=document.getElementById('check_list'+[x]);
		 // 	 alert(checklist1);

		 if(checklist1.checked) {
		 cont = cont + 1;
		     if(cont>1){
		     //	alert ("El número de checkbox pulsados es " + cont);	
		    	document.getElementById('buttoneditar'+[1]).disabled=true;
		        document.getElementById('buttoneditar'+[2]).disabled=true;
		        document.getElementById('buttoneditar'+[3]).disabled=true;
		  		}
		  		else
		  		{
		  			document.getElementById('buttoneditar'+[x]).disabled=false;	
		  		}
		  }
		
		 }

}
/*	
$(document).ready(function() {
var checklist = 1;
        $("input:checkbox").click(function() {
            checklist = $("input:checkbox:checked").length;
            if (checklist > 1) {
                $("input:button").show();
            } else {
                $("input:button").hide();
             
            }   
     })
  })
  */    


function editarValorZona(count){
	var tipozona=document.getElementById('tipozona'+count).value;
	var preciozona=document.getElementById('preciozona'+count).value;
	var cod_perfil=document.getElementById('perfil').value;

	var variable=tipozona+'/'+preciozona+'/'+cod_perfil;
	$.post('view/Gestion/Mantenimiento/preciozona.php',{postvariable:variable},
	function(data)
	{
		alertify.success(data);
		setTimeout(function(){ window.location.reload(0); }, 3000); 
	});

}

function buscarafiliado(){


	if(document.getElementById('busqueda').value == "")
	{
		alertify.alert("DEBE INGRESAR EL N° DE AFILIACIÓN RIF/CI O RAZÓN SOCIAL!.");
		document.getElementById('busqueda').focus();
		return false;
	}
	
	else {
		var busqueda =document.getElementById('busqueda').value;
		window.location.href='?cargar=buscarrif&var='+busqueda

	}
}


function editaractividad(usuario){


	alertify.set({ labels: {
		ok: "Offline",
		cancel: "Cancelar"
	}
});
	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea inactivar el siguiente usuario: "+usuario+"?", function (e) {
		if (e) {
			$.post('view/usuario/editaractividad.php',{postusuario:usuario},
				function(data)
				{
					alertify.success(data);
					setTimeout(function(){ window.location.reload(0); }); 
				});
		} else {
			alertify.error("Se ha cancelado el proceso");
			setTimeout(function(){ window.location.reload(0); }); 
			return false;
		}
	})
	return false;
}


function editarZonas(){

	var fila=document.getElementById('fila').value;
	var tipozona;
	var preciozona; 
	var cod_perfil;	
	var checklist;
	var contador=0;

		
			 
				for (paso =0; paso < fila; paso++)
				{
					contador++;
						checklist=document.getElementById('check_list'+[contador]);

						if(checklist.checked) 
							{
							
							tipozona=document.getElementById('tipozona'+[contador]).value;
							preciozona=document.getElementById('preciozona'+[contador]).value;
							cod_perfil=document.getElementById('perfil').value;
							
							
				var variable=tipozona+'/'+preciozona+'/'+cod_perfil;
				//alert (variable);
				$.post('view/Gestion/Mantenimiento/preciozona.php',{postvariable:variable},
				function(data)
			{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 3000); 
			});
		}
							
}	
			

}

function eliminarbancos(count){

	var codigobanco=document.getElementById('banco'+count).value;
	var variable=codigobanco;

	alertify.set({ labels: {
		ok: "Eliminar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });
	alertify.confirm("&#191;Desea Eliminar el siguiente banco?", function (e) {
		if (e) {
			$.post('view/Gestion/Mantenimiento/eliminarbanco.php',{postvariable:variable},
			function(data)
			{
		alertify.success(data);
		setTimeout(function(){ window.location.reload(0); }, 3000); 
			});

		} else {
			alertify.error("No se ha eliminado el banco");
			return false;
		}
})
	return false;
}



function guardarbanco(){

	cod_perfil=document.getElementById('perfil').value;
	codigobanco=document.getElementById('bancos').value;
    var variable=cod_perfil+'/'+codigobanco;
   	

	alertify.set({ buttonReverse: true });
	alertify.confirm("&#191;Desea agregar el siguiente banco?", function (e) {
		if (e) {
			$.post('view/Gestion/Mantenimiento/guardarbancos.php',{postvariable:variable},
			function(data)
			{
		alertify.success(data);
		setTimeout(function(){ window.location.reload(0); }, 3000); 
			});

		} else {
			alertify.error("El banco no pudo ser guardado");
			return false;
		}
})
	return false;
		
    }

function showlistbank(){

	document.getElementById('bancos').style.display="block";
	document.getElementById('aggbanco').style.display="none";
	document.getElementById('guardar').style.display="block";

}
function crearperfil(){

	//var cod_perfil=document.getElementById('cod_perfil').value;
	var desc_perfil=document.getElementById('desc_perfil').value;


	var variable=desc_perfil;
	$.post('view/Gestion/Mantenimiento/perfilgeografico.php',{postvariable:variable},
	function(data)
	{
		alertify.success(data);
	setTimeout(function(){ window.location.reload(0); }, 3000); 
	});

}

//guarda interes mantenimiento
function interesMantenimiento(){
    coddocumento=document.getElementById('coddocumento').value;
	namearch=document.getElementById('namearch').value;
	usuario=document.getElementById('usuario').value;
	direccioninstalacion=document.getElementById('id_registro').value;
	selectinteres=document.getElementById('selectmantenimiento').value;

 if(selectinteres=="0"){
	    alertify.error('Debe seleccionar una opción');
		$('#selectmantenimiento').focus().select();
		$('#selectmantenimiento').select().css("border-color","red" );
		return false;

	}else{


	var variable=coddocumento+'/'+namearch+'/'+usuario+'/'+direccioninstalacion+'/'+selectinteres;
		// alert(variable);
		$.post('view/comercializacion/gestion/guardainteresmantenimiento.php',{postvariable:variable},
		function(data)
		{
			modified=false;
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 

		});
}

	return false;
}

function habilitaregistro()
{
  if (document.getElementById("estatus").checked === true) { 
    document.getElementById("estatus").disabled=false;
  }

  if (  document.getElementById("estatus").checked = true) {

    alertify.set({ labels: {
      ok: "Habilitar",
      cancel: "Cancelar"
    }
  });

    alertify.set({ buttonReverse: true });
    	idregistro=document.getElementById('idregistro').value;
    alertify.confirm("&#191;Desea habilitar el registro afiliado ?", function (e) {
      if (e) {
        $.post('view/comercializacion/gestion/reloadregistro.php',{postvariable:idregistro},
          function(data)
          {
            alertify.success(data);

            setTimeout(function(){ window.location.reload(0); }, 1000);
          });
      } else {
        alertify.error("No se ha habilitado el registro afiliado.");
        setTimeout(function(){ window.location.reload(0); }, 1000);
        return false;
      }
    })
    return false;
  }
}

function habilitaregistroencuesta()
{
  if (document.getElementById("estatus").checked === true) { 
    document.getElementById("estatus").disabled=false;
  }

  if (  document.getElementById("estatus").checked = true) {

    alertify.set({ labels: {
      ok: "Habilitar",
      cancel: "Cancelar"
    }
  });

    alertify.set({ buttonReverse: true });
    	idregistro=document.getElementById('idregistro').value;
    	//alert(idregistro);
    alertify.confirm("&#191;Desea habilitar el registro afiliado ?", function (e) {
      if (e) {
        $.post('view/Encuesta/reloadregistroencuesta.php',{postvariable:idregistro},
          function(data)
          {
            alertify.success(data);

            setTimeout(function(){ window.location.reload(0); }, 1000);
          });
      } else {
        alertify.error("No se ha habilitado el registro afiliado.");
        setTimeout(function(){ window.location.reload(0); }, 1000);
        return false;
      }
    })
    return false;
  }
}

/*Para indicar que el afiliado fue contactado*/
function afiliadocontactado()
{
  if (document.getElementById("estatus").checked === true) { 
    document.getElementById("estatus").disabled=false;
  }

  if (  document.getElementById("estatus").checked = true) {

    alertify.set({ labels: {
      ok: "Habilitar",
      cancel: "Cancelar"
    }
  });

alertify.set({ buttonReverse: true });
id_registro=document.getElementById('id_registro').value;
usuario=document.getElementById('usuario').value;
    alertify.confirm("&#191;Desea indicar que el afliado fue contactado ?", function (e) {
      if (e) {
        $.post('view/comercializacion/gestion/indicacontacto.php',{postvariable:id_registro+'/'+usuario},
          function(data)
          {
            alertify.success(data);
            setTimeout(function(){ window.location.reload(0); }, 1000);
          });
      } else {
        alertify.error("Acción cancelada.");
        setTimeout(function(){ window.location.reload(0); }, 1000);
        return false;
      }
    })
    return false;
  }
}

function habilitartecnico()
{
var equis="";
  if (document.getElementById("estatus").checked == true) {
    
    document.getElementById("estatus").disabled=false;
  }
  if (document.getElementById("estatus").value == 0) {
    
    var equis="Habilitar";

  }else{
var equis="Inhabilitar";

  }

  if (  document.getElementById("estatus").checked = true) {

    alertify.set({ labels: {

      ok: equis,
      cancel: "Cancelar"
    }
  });

    alertify.set({ buttonReverse: true });
    	documento=document.getElementById('documento').value;
    	estatus=document.getElementById('estatus').value;
    
    alertify.confirm("&#191;Desea "+ equis +" el siguiente tecnico?", function (e) {
      if (e) {
      	var variable=documento+'/'+estatus;
        $.post('view/ServicioTecnico/estatustecnico.php',{postvariable:variable},
          function(data)
          {
            alertify.success(data);

           setTimeout(function(){ window.location.reload(0); }, 600);
          });
      } else {
        alertify.error("No se ha" +equis+ "el tecnico.");
        setTimeout(function(){ window.location.reload(0); }, 600);
        return false;
      }
    })
    return false;
  }
}



function validarfechasreporte(){

 var fechainicial=document.getElementById('fechainicial').value;
 var fechafinal=document.getElementById('fechafinal').value;
 
 var fechad = new Date(fechainicial);
 var aniodesde = fechad.getUTCFullYear();

 var fechah = new Date(fechafinal);
 var aniohasta = fechah.getUTCFullYear();

//alert (Date.parse(fechadesde));
//alert (Date.parse(fechahasta));

if ((isNaN(aniodesde)) || (isNaN(aniohasta)))
{
	document.getElementById('Buscar').disabled=true;
}
else if((Date.parse(fechainicial)) > (Date.parse(fechafinal))){
	//alertify.error("La fecha inicial no puede ser mayor que la fecha final");
	document.getElementById('Buscar').disabled=true;
	}
	else
	{
	document.getElementById('Buscar').disabled=false;	
	}
	//Para habilitar el tipo de reporte
	if ((fechainicial!='') && (fechafinal!='')) {
	$('#tiporeporte').removeAttr('disabled');
	}
	else{
		 $('#tiporeporte').val("1");
       document.getElementById('tiporeporte').disabled=true;
       }
}


function validarfechasR(){

 var fechainicial=document.getElementById('fechainicial').value;
 var fechafinal=document.getElementById('fechafinal').value;

 
 var fechad = new Date(fechainicial);
 var aniodesde = fechad.getUTCFullYear();

 var fechah = new Date(fechafinal);
 var aniohasta = fechah.getUTCFullYear();
//alert (fechainicial);
//alert (Date.parse(fechadesde));
//alert (Date.parse(fechahasta));

if ((isNaN(aniodesde)) || (isNaN(aniohasta)))
{
	document.getElementById('Buscar').disabled=true;
}
else if((Date.parse(fechainicial)) > (Date.parse(fechafinal))){
	//alertify.error("La fecha inicial no puede ser mayor que la fecha final");
	document.getElementById('Buscar').disabled=true;
	}	
	else
	{
	document.getElementById('Buscar').disabled=false;	
	}
	
}




function guardarnafiliado(){
	


	var numeroafiliado=document.getElementById('numeroafiliado').value;
	var idregistro=document.getElementById('id_registro').value;

	if (numeroafiliado=="") {
		alertify.error('Error el campo no puede estar vacio. Ingrese el número de afiliado');
		document.getElementById('numeroafiliado').style.borderColor="red";
		document.getElementById('numeroafiliado').focus('');
		return false;
	}

	var variable=idregistro+'/'+numeroafiliado;
	//alert(variable);
	$.post('view/comercializacion/gestion/guardarnafiliado.php',{postvariable:variable},
		function(data)
		{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 


		});


	return false;

}


function guardarnafiliadoserviciotecnico(){
	

		//alert(idcliente);
	var numeroafiliado=document.getElementById('numeroafiliado').value;
	var idcliente=document.getElementById('idcliente').value;

	if (numeroafiliado=="") {
		alertify.error('Error el campo no puede estar vacio. Ingrese el número de afiliado');
		document.getElementById('numeroafiliado').style.borderColor="red";
		document.getElementById('numeroafiliado').focus('');
		return false;
	}

	var variable=idcliente+'/'+numeroafiliado;

	$.post('view/ServicioTecnico/guardarafiliacionservicio.php',{postvariable:variable},
		function(data)
		{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 


		});


	return false;

}



function guardarbancoasignado(){
	

	var usuario=document.getElementById('usuario').value;	
	var banco=document.getElementById('bancoasignado').value;
	var idregistro=document.getElementById('id_registro').value;
	if (banco=="") {
		alertify.error('Error el campo no puede estar vacio. Ingrese el banco a asignar');
		document.getElementById('bancoasignado').style.borderColor="red";
		document.getElementById('bancoasignado').focus('');
		return false;
	}

		var variable=idregistro+'/'+banco+'/'+usuario;
//	alert(variable);
	$.post('view/comercializacion/gestion/guardarbanco.php',{postvariable:variable},
		function(data)
		{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 


		});


	return false;

}
function agendarllamada(){
	


	var nextcall=document.getElementById('nextcall').value;
	var idregistro=document.getElementById('id_registro').value;
	var numeroafiliado=document.getElementById('numeroafiliado').value;
	if (nextcall=="") {
		alertify.error('Error, debe indicar fecha de la proxima llamada');
		document.getElementById('nextcall').style.borderColor="red";
	
		return false;
	}

	var variable=idregistro+'/'+nextcall+'/'+numeroafiliado;
	
	$.post('view/comercializacion/gestion/agendallamada.php',{postvariable:variable},
		function(data)
		{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 


		});


	return false;

}


function confirmPayment()
{
var equis="";
  if (document.getElementById("estatus").checked == true) {
    
    document.getElementById("estatus").disabled=false;
  }
  if (document.getElementById("estatus").value == 0) {
    
    var equis="Aprobar";

  }else{
var equis="Rechazar";

  }

  if (  document.getElementById("estatus").checked = true) {
    alertify.set({ labels: {

      ok: equis,
      cancel: "Cancelar"
    }
  });

    alertify.set({ buttonReverse: true });
        usuario=document.getElementById('usuario').value;
    	documento=document.getElementById('documento').value;
    	estatus=document.getElementById('estatus').value;
    if (estatus==0) {
    	var newStatus=1;
    }
    if (estatus==1) {
    	var newStatus=0;
    }
    alertify.confirm("&#191;Desea "+ equis +" el siguiente pago?", function (e) {
      if (e) {
      	var variable=documento+'/'+newStatus+'/'+usuario;
        $.post('view/Administracion/facturacionpos/updateStatusPago.php',{postvariable:variable},
          function(data)
          {
            alertify.success(data);
           setTimeout(function(){ window.location.reload(0); }, 2000);
          });
      } else {
      	if (equis=="Aprobar") {
      		var verbo="Aprobado";
      	}else{
      		var verbo="Rechazar";
      	}
        alertify.error("No se ha " +verbo+ " el pago.");
        setTimeout(function(){ window.location.reload(0); }, 2000);
        return false;
      }
    })
    return false;
  }
}


function confirmAllPayments(){
	var usuario =document.getElementById('usuario').value;
	var idregistro=document.getElementById('idregistro').value;
if (idregistro=="") {
		alertify.error('Error, id del registro es null');
		document.getElementById('idregistro').style.borderColor="red";
	
		return false;
	}
	var estatus=0;
	var variable=idregistro+'/'+estatus+'/'+usuario;
	$.post('view/Administracion/facturacionpos/consultaestatuspagos.php',{postvariable:variable},
		function(data)
		{

if (data!=0) { alertify.error("Error, aun tiene pagos sin confirmar,");
		//setTimeout(function(){ window.location.reload(0); }, 2000);
	
 }else{
 	 alertify.success('Pagos Confirmados');
     	setTimeout(function(){ window.location.reload(0); }, 2000);
		 fetch('http://192.168.1.6:3000/enviarnotificacion',{
			method: 'POST',
			headers: {
				'Access-Control-Allow-Origin': '*'
			},
			//body:JSON.stringify(datos)
		}) 
 	}

});

	return false;

}


function confirmarAllpagosMifi(){
	var usuario =document.getElementById('usuario').value;
	var idregistro=document.getElementById('idregistro').value;
if (idregistro=="") {
		alertify.error('Error, id del registro es null');
		document.getElementById('idregistro').style.borderColor="red";
	
		return false;
	}
	var estatus=0;
	var variable=idregistro+'/'+estatus+'/'+usuario;
	$.post('view/Administracion/facturacionmifi/consultaestatuspagosmifi.php',{postvariable:variable},
		function(data)
		{

if (data!=0) { alertify.error("Error, aun tiene pagos sin confirmar,");
		//setTimeout(function(){ window.location.reload(0); }, 2000);
	
 }else{
 	 alertify.success('Pagos Confirmados');
     	setTimeout(function(){ window.location.reload(0); }, 2000);
 	}

});

	return false;

}


function confirmarpagomifi()
{
var equis="";
  if (document.getElementById("estatus").checked == true) {
    
    document.getElementById("estatus").disabled=false;
  }
  if (document.getElementById("estatus").value == 0) {
    
    var equis="Aprobar";

  }else{
var equis="Rechazar";

  }

  if (  document.getElementById("estatus").checked = true) {
    alertify.set({ labels: {

      ok: equis,
      cancel: "Cancelar"
    }
  });

    alertify.set({ buttonReverse: true });
        usuario=document.getElementById('usuario').value;
    	documento=document.getElementById('documento').value;
    	estatus=document.getElementById('estatus').value;
    if (estatus==0) {
    	var newStatus=1;
    }
    if (estatus==1) {
    	var newStatus=0;
    }
    alertify.confirm("&#191;Desea "+ equis +" el siguiente pago?", function (e) {
      if (e) {
      	var variable=documento+'/'+newStatus+'/'+usuario;
        $.post('view/Administracion/facturacionmifi/updatestatuspagomifi.php',{postvariable:variable},
          function(data)
          {
            alertify.success(data);
           setTimeout(function(){ window.location.reload(0); }, 2000);
          });
      } else {
      	if (equis=="Aprobar") {
      		var verbo="Aprobado";
      	}else{
      		var verbo="Rechazar";
      	}
        alertify.error("No se ha " +verbo+ " el pago.");
        setTimeout(function(){ window.location.reload(0); }, 2000);
        return false;
      }
    })
    return false;
  }
}




function guardarFacturacion(){

	var cantidadterminales =document.getElementById('cantidadterminales').value; 
	var contador=0;
	var idregistro=document.getElementById('idregistro').value;
	var numerofactura =document.getElementById('numerofactura').value;
	var ffactura =document.getElementById('ffactura').value;
	var montofactura =document.getElementById('montofactura').value;
	var usuario =document.getElementById('usuario').value;
	var marcapos =document.getElementById('marcapos').value;
	var idprefijo =document.getElementById('idprefijo').value;
	var idcuenta =document.getElementById('idcuenta').value;
	var point=montofactura;
	var amountnopoint = point.toString().replace(/\./g,''); 
	var amountfinal = amountnopoint.replace(/,/g, ".");
	var cuentabanco=idprefijo+idcuenta;	

	//alert(idprefijo)
	console.log(idcuenta.length);

	//if (idcuenta.length < 16)

		var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if(dd < 10)
          dd = '0' + dd;
        if(mm < 10)
          mm = '0' + mm;
        today = yyyy + '-' + mm + '-' + dd;


	    var resta = new Date();
	    var dd = resta.getDate();
        var mm = resta.getMonth() - 5;
        var yyyy = resta.getFullYear();


        if(dd < 10)
          dd = '0' + dd;
        if(mm < 10)
          mm = '0' + mm;
	  		resta= yyyy + '-' + mm + '-' + dd;


	if (idregistro=="") {
	alertify.error('Error, id del registro no puede estar vacio');
	document.getElementById('idregistro').style.borderColor="red";
	return false;
	}
	if (numerofactura==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('numerofactura').style.borderColor="red";
    return false;
	}
	if (idcuenta=="" && idprefijo!='0100'){
	alertify.error('Error, el número de cuenta es requerido');
    document.getElementById('idcuenta').style.borderColor="red";
    return false;
	}
	if ((idcuenta.length < 16) && (idprefijo!='0100')){
	alertify.error('Error, debe colocar los 16 caracteres');
    document.getElementById('idcuenta').style.borderColor="red";
    return false;
	}
	if (ffactura==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('ffactura').style.borderColor="red";
    return false;
	}



	//if ((ffactura > today) || (ffactura < resta)) {

			if (ffactura > today) 
			{
			document.getElementById("errorfechafact").style.removeProperty("display");
	    document.getElementById("errorfechafact").innerHTML="La fecha no debe ser mayor a la actual";
	    return false;
			}

		/*	if (ffactura < resta) 

			{
			document.getElementById("errorfechafact").style.removeProperty("display");
	    document.getElementById("errorfechafact").innerHTML="La fecha no debe ser menor a 6 meses";
	    return false;

			}*/
	//}

	if (montofactura=="0"){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('montofactura').style.borderColor="red";
    return false;
	}

//alert(cantidadterminales);
for (var i = 0; i < cantidadterminales; i++) {
contador++;
	var serial =document.getElementById("serial"+[contador]).value;
	var simcard =document.getElementById("simcard"+[contador]).value;
	var serialmifi =document.getElementById("serialmifi"+[contador]).value;
	var codigodestino =document.getElementById("codigodestino"+[contador]).value;
	var consecutivo =document.getElementById("consecutivo"+[contador]).value;
	var idestatus =document.getElementById("idestatus"+[contador]).value;
	//var fechains =document.getElementById('fechainss'+[contador]);
/*	if (fechains==null){
		var fechainss='0000-00-00';
		} else if  (fechains!==null){
			var fechainss =document.getElementById("fechainss"+[contador]).value;
		}*/

/*	var idgestion =document.getElementById("idgestion"+[contador]);
	if (idgestion==null){
		var idgestion=0;
		} else if  (fechains!==null){
			var idgestion =document.getElementById("idgestion"+[contador]).value;
		}*/
	//alert(serial+'/'+simcard+'/'+serialmifi+'/'+codigodestino+'/'+consecutivo);

	var variable=idregistro+'/'+numerofactura+'/'+ffactura+'/'+amountfinal+'/'+serial+'/'+simcard+'/'+serialmifi+'/'+codigodestino+'/'+usuario+'/'+consecutivo+'/'+idestatus+'/'+cuentabanco;
	//alert(variable)
	$.post('view/Administracion/facturacionpos/guardafacturacion.php',{postvariable:variable},
		function(data)
		{

});
	
}
	alertify.success("Factura guardada con exito");
	setTimeout(function(){ window.location.reload(0); }, 7000);		
	
	}



	function guardarFacturacionMifi(){

	var cantidadterminales =document.getElementById('cantidadterminales').value; 
	var contador=0;
	var idregistro=document.getElementById('idregistro').value;
	var numerofactura =document.getElementById('numerofactura').value;
	var ffactura =document.getElementById('ffactura').value;
	var montofactura =document.getElementById('montofactura').value;
	var usuario =document.getElementById('usuario').value;
	
	var point=montofactura;
	var amountnopoint = point.toString().replace(/\./g,''); 
	var amountfinal = amountnopoint.replace(/,/g, ".");	

		if (idregistro=="") {
		alertify.error('Error, id del registro no puede estar vacio');
		document.getElementById('idregistro').style.borderColor="red";
		return false;
	}
	if (numerofactura==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('numerofactura').style.borderColor="red";
    return false;
	}
	if (ffactura==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('ffactura').style.borderColor="red";
    return false;
	}


	if (montofactura=="0"){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('montofactura').style.borderColor="red";
    return false;
	}


//alert(cantidadterminales);
for (var i = 0; i < cantidadterminales; i++) {
contador++;
	
	var mifiserial =document.getElementById("mifiserial"+[contador]).value;
	var codigodestino =document.getElementById("codigodestino"+[contador]).value;
	var consecutivo =document.getElementById("consecutivo"+[contador]).value;
	var simserial =document.getElementById("simserial"+[contador]).value;

	var variable=idregistro+'/'+numerofactura+'/'+ffactura+'/'+amountfinal+'/'+mifiserial+'/'+codigodestino+'/'+usuario+'/'+consecutivo+'/'+simserial;
	$.post('view/Administracion/facturacionmifi/guardarfacturacionmifi.php',{postvariable:variable},

		function(data)
		{

});
	
}
	alertify.success("Factura guardada con exito");
	setTimeout(function(){ window.location.reload(0); }, 30000);		
	
	}

function guardarFacturaAnulada(){
	
	var idregistro=document.getElementById('idregistro').value;
	var numfactura =document.getElementById('numfactura').value;
	var datefactura =document.getElementById('datefactura').value;
	var bsfactura =document.getElementById('bsfactura').value;
/*	var serial =document.getElementById('serial').value;
	var simcard =document.getElementById('simcard').value;
	var serialm =document.getElementById('serialm').value;*/
	var usuario =document.getElementById('usuario').value;

// var str = montofactura;
var point=bsfactura;
var amountnopoint = point.toString().replace(/\./g,''); 
var amountfinal = amountnopoint.replace(/,/g, ".");
//var Newmonto = res.toString().replace(/\./g,''); 
	if (idregistro=="") {
		alertify.error('Error, id del registro no puede estar vacio');
		document.getElementById('idregistro').style.borderColor="red";
		return false;
	}
	if (numfactura==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('numfactura').style.borderColor="red";
    return false;
	}
	if (datefactura==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('datefactura').style.borderColor="red";
    return false;
	}
	if (bsfactura==""){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('bsfactura').style.borderColor="red";
    return false;
	}
	/*if (serial==""){
	alertify.error('Error, el campo serial es requerido');
    document.getElementById('serialp').style.borderColor="red";
    return false;
	}
	*/
	//alert (variable);
	var variable=idregistro+'/'+numfactura+'/'+datefactura+'/'+amountfinal+'/'+usuario;
	$.post('view/Administracion/facturacionpos/guardaranulacion.php',{postvariable:variable},

		function(data)
		{
	//alert(variable);
	alertify.success(data);
	setTimeout(function(){ window.location.reload(0); }, 3000);		

});

	return false;
 
}


function guardarFacturaAnuladadeMifi(){
	
	var idregistro=document.getElementById('idregistro').value;
	var numfactura =document.getElementById('numfactura').value;
	var datefactura =document.getElementById('datefactura').value;
	var bsfactura =document.getElementById('bsfactura').value;
/*	var serial =document.getElementById('serial').value;
	var simcard =document.getElementById('simcard').value;
	var serialm =document.getElementById('serialm').value;*/
	var usuario =document.getElementById('usuario').value;

// var str = montofactura;
var point=bsfactura;
var amountnopoint = point.toString().replace(/\./g,''); 
var amountfinal = amountnopoint.replace(/,/g, ".");
//var Newmonto = res.toString().replace(/\./g,''); 
	if (idregistro=="") {
		alertify.error('Error, id del registro no puede estar vacio');
		document.getElementById('idregistro').style.borderColor="red";
		return false;
	}
	if (numfactura==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('numfactura').style.borderColor="red";
    return false;
	}
	if (datefactura==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('datefactura').style.borderColor="red";
    return false;
	}
	if (bsfactura==""){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('bsfactura').style.borderColor="red";
    return false;
	}
	/*if (serial==""){
	alertify.error('Error, el campo serial es requerido');
    document.getElementById('serialp').style.borderColor="red";
    return false;
	}
	*/
	//alert (variable);
	var variable=idregistro+'/'+numfactura+'/'+datefactura+'/'+amountfinal+'/'+usuario;
	$.post('view/Administracion/facturacionmifi/guardaranulaciondemifi.php',{postvariable:variable},

		function(data)
		{
	//alert(variable);
	alertify.success(data);
	setTimeout(function(){ window.location.reload(0); }, 3000);		

});

	return false;
 
}



function guardarDevolucionFactura(){
	
	var cantidadterminales= document.getElementById('cantidadterminales').value;
	var contador=0;
	var idregistro=document.getElementById('idregistro').value;
	var facturadev =document.getElementById('facturadev').value;
	var fechadev =document.getElementById('fechadev').value;
	var montodev =document.getElementById('montodev').value;
	//var descmotivo =document.getElementById('descmotivo').value;

	var usuario =document.getElementById('usuario').value;

// var str = montofactura;
var point=montodev;
var amountnopoint = point.toString().replace(/\./g,''); 
var amountfinal = amountnopoint.replace(/,/g, ".");
//var Newmonto = res.toString().replace(/\./g,''); 
	if (idregistro=="") {
		alertify.error('Error, id del registro no puede estar vacio');
		document.getElementById('idregistro').style.borderColor="red";
		return false;
	}
	if (facturadev==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('facturadev').style.borderColor="red";
    return false;
	}
	if (fechadev==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('fechadev').style.borderColor="red";
    return false;
	}
	if (montodev=="0"){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('montodev').style.borderColor="red";
    return false;
	}

	for (var i = 0; i < cantidadterminales; i++) {
contador++;

var serialdev =document.getElementById("serialdev"+[contador]).value;
var simdev =document.getElementById("simdev"+[contador]).value;
var serialmdev =document.getElementById("serialmdev"+[contador]).value;
var codigomotivo =document.getElementById("codigomotivo"+[contador]).value;
var observacion =document.getElementById("observacion"+[contador]).value;
var consecutivo =document.getElementById("consecutivo"+[contador]).value;
var id_estatus =document.getElementById("id_estatus"+[contador]).value;
var idgestiones =document.getElementById("idgestiones"+[contador]).value;
//alert(serialdev+'/'+simdev+'/'+serialmdev+'/'+codigomotivo+'/'+consecutivo+'/'+id_estatus+'/'+idgestiones);
//alert(codigomotivo) ;

var variable=idregistro+'/'+facturadev+'/'+fechadev+'/'+amountfinal+'/'+serialdev+'/'+simdev+'/'+serialmdev+'/'+codigomotivo+'/'+usuario+'/'+consecutivo+'/'+observacion+'/'+id_estatus+'/'+idgestiones;
		$.post('view/Administracion/facturacionpos/guardardevolucion.php',{postvariable:variable},

		function(data)
		{
			if (data==1) {
				alertify.success("Devolución Guardada con exito");
				fetch('http://localhost:5000/enviarnotificacionrecompra',{
					method: 'POST',
					headers: {
						'Access-Control-Allow-Origin': '*'
					},
						//body:JSON.stringify(datos)
				})

			}else{
	    		alertify.error('La devolución no pudo ser registrado');	
	    	}
		});

		}

		
			//setTimeout(function(){ window.location.reload(0); }, 4000);	
//alert (variable);

 
}


function guardarDevolucionFacturadeMifi(){
	
	var cantidadterminales= document.getElementById('cantidadterminales').value;
	var contador=0;
	var idregistro=document.getElementById('idregistro').value;
	var facturadev =document.getElementById('facturadev').value;
	var fechadev =document.getElementById('fechadev').value;
	var montodev =document.getElementById('montodev').value;
	//var descmotivo =document.getElementById('descmotivo').value;

	var usuario =document.getElementById('usuario').value;

// var str = montofactura;
var point=montodev;
var amountnopoint = point.toString().replace(/\./g,''); 
var amountfinal = amountnopoint.replace(/,/g, ".");
//var Newmonto = res.toString().replace(/\./g,''); 
	if (idregistro=="") {
		alertify.error('Error, id del registro no puede estar vacio');
		document.getElementById('idregistro').style.borderColor="red";
		return false;
	}
	if (facturadev==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('facturadev').style.borderColor="red";
    return false;
	}
	if (fechadev==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('fechadev').style.borderColor="red";
    return false;
	}
	if (montodev=="0"){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('montodev').style.borderColor="red";
    return false;
	}

	for (var i = 0; i < cantidadterminales; i++) {
			contador++;

	var serialmifidev =document.getElementById("serialmifidev"+[contador]).value;
	var devsimcard =document.getElementById("devsimcard"+[contador]).value;
	var codigomotivo =document.getElementById("codigomotivo"+[contador]).value;
	var observacion =document.getElementById("observacion"+[contador]).value;
	var consecutivo =document.getElementById("consecutivo"+[contador]).value;
	

	//alert(serialdev+'/'+simdev+'/'+serialmdev+'/'+codigomotivo+'/'+consecutivo);
			
	//alert (variable);
	var variable=idregistro+'/'+facturadev+'/'+fechadev+'/'+amountfinal+'/'+serialmifidev+'/'+devsimcard+'/'+codigomotivo+'/'+usuario+'/'+consecutivo+'/'+observacion;
	$.post('view/Administracion/facturacionmifi/guardardevoluciondemifi.php',{postvariable:variable},

		function(data)
		{
	//alert(variable);
	

});

}

alertify.success("Devolución Guardada con exito");
	setTimeout(function(){ window.location.reload(0); }, 3000000);		
 
}

function guardarDevolucionTotal(){
	
	var cantidadterminales= document.getElementById('cantidadterminales').value;
	var contador=0;
	var idregistro=document.getElementById('idregistro').value;
	var facturadev =document.getElementById('facturadev').value;
	var fechadev =document.getElementById('fechadev').value;
	var montodev =document.getElementById('montodev').value;
	var usuario =document.getElementById('usuario').value;

// var str = montofactura;
var point=montodev;
var amountnopoint = point.toString().replace(/\./g,''); 
var amountfinal = amountnopoint.replace(/,/g, ".");
//var Newmonto = res.toString().replace(/\./g,''); 
	if (idregistro=="") {
		alertify.error('Error, id del registro no puede estar vacio');
		document.getElementById('idregistro').style.borderColor="red";
		return false;
	}
	if (facturadev==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('facturadev').style.borderColor="red";
    return false;
	}
	if (fechadev==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('fechadev').style.borderColor="red";
    return false;
	}
	if (montodev=="0"){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('montodev').style.borderColor="red";
    return false;
	}

	for (var i = 0; i < cantidadterminales; i++) {
			contador++;

	var serialdevtotal =document.getElementById("serialdevtotal"+[contador]).value;
	var simdevtotal =document.getElementById("simdevtotal"+[contador]).value;
	var serialmdevtotal =document.getElementById("serialmdevtotal"+[contador]).value;
	var codigomotivodev =document.getElementById("codigomotivodev"+[contador]).value;
	var consecutivo =document.getElementById("consecutivo"+[contador]).value;

	var variable=idregistro+'/'+facturadev+'/'+fechadev+'/'+amountfinal+'/'+serialdevtotal+'/'+simdevtotal+'/'+serialmdevtotal+'/'+codigomotivodev+'/'+usuario+'/'+consecutivo;
	$.post('view/Administracion/facturacionpos/guardardevoluciontotal.php',{postvariable:variable},

		function(data)
		{ 
		});

}

alertify.success("Devolución Guardada con exito");
	setTimeout(function(){ window.location.reload(0); }, 3000);		 
}



function autocompletarserialdevtotal(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('serialdevtotal'+count).value;
		var texto = document.getElementById('validarserialdevtotal'+count).value;
		
		if (busqueda.length>=7) {
	
	$.post('view/Administracion/facturacionpos/buscarserialdevolucion.php',{postvariable:busqueda},
			function(data)
			{
			if (data==0) { 
				// esto no hace nada
				 document.getElementById('validarserialdevtotal'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('serialdevtotal'+count).style = 'border-color:red;'; 

		 }	else  {
				//datatype: 'json'
				
				//$('#lista_id'+count).show();
				$('#lista_id'+count).html(data);
				document.getElementById('validarserialdevtotal'+count).innerHTML = "";
     			/*document.getElementById('serialdev'+count).style = 'border-color:control;'; 
     			$('#btnguardardevolucion').removeAttr('disabled');*/
		}
			}) 
	} if (busqueda.length<=6) {
		 	//$('#lista_id'+count).show();
				document.getElementById('serialdevtotal'+count).style = 'border-color:red;'; 
				 document.getElementById('caracteresposdevtotal'+count).innerHTML = "El serial debe tener 15 dígitos";
				 $('#btnguardardevoluciontotal').prop('disabled',true);	

		 } if (busqueda.length>=25) {
		 	//$('#lista_id'+count).show();
		 		document.getElementById('serialdevtotal'+count).style = 'border-color:red;'; 
				 $('#btnguardardevoluciontotal').prop('disabled',true);	

		 }

		 else {
		 	document.getElementById('serialdevtotal'+count).style = 'border-color:control;';
		 	document.getElementById('caracteresposdevtotal'+count).innerHTML = "";

		 	$('#btnguardardevoluciontotal').removeAttr('disabled');

		 }
}



function autocompletarsimdevtotal(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('simdevtotal'+count).value;
		var texto = document.getElementById('validarsimdevtotal'+count).value;
		if (busqueda.length>=7) {
	
	//console.log(busqueda);
	//Contamos el valor del input mediante una condicional
	$.post('view/Administracion/facturacionpos/buscarsimcarddevolucion.php',{postvariable:busqueda},
			function(data)
			{
			if (data==0) { 

				 document.getElementById('validarsimdevtotal'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('simdevtotal'+count).style = 'border-color:red;'; 
				$('#btnguardardevoluciontotal').prop('disabled',true);

		/*		alertify.error("Error, Serial no existe");
			setTimeout(function(){ window.location.reload(0); }, 2000);
		*/ }	else  {
				//datatype: 'json'
				
				//$('#lista_id_sim'+count).show();
				$('#lista_id_sim'+count).html(data);
				document.getElementById('validarsimdevtotal'+count).innerHTML = "";
     			document.getElementById('simdevtotal'+count).style = 'border-color:control;'; 
     			$('#btnguardardevoluciontotal').removeAttr('disabled');
		}
			}) 
	} 
}

function autocompletarmifidevtotal(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('serialmdevtotal'+count).value;
		var texto = document.getElementById('validarmifidevtotal'+count).value;
		if (busqueda.length>=7) {
	
	//console.log(busqueda);
	//Contamos el valor del input mediante una condicional
	$.post('view/Administracion/facturacionpos/buscarmifidevolucion.php',{postvariable:busqueda},
			function(data)
			{
			if (data==0) { 

				 document.getElementById('validarmifidevtotal'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('serialmdevtotal'+count).style = 'border-color:red;';
				 $('#btnguardardevoluciontotal').prop('disabled',true);

				
		/*		alertify.error("Error, Serial no existe");
			setTimeout(function(){ window.location.reload(0); }, 2000);
		*/ }	else  {
				//datatype: 'json'
				
				//$('#lista_id_mifi'+count).show();
				$('#lista_id_mifi'+count).html(data);
				document.getElementById('validarmifidev'+count).innerHTML = "";
     			document.getElementById('serialmdev'+count).style = 'border-color:control;';
     			 $('#btnguardardevoluciontotal').removeAttr('disabled'); 
		}
			}) 
	} 
}

function buscarfacturapos(){
	if(document.getElementById('factura').value == "")
	{   
	
		window.location.href='?cargar=busquedatotalfactura'
	}
	else{
		var variable=document.getElementById('factura').value;
		window.location.href='?cargar=busquedaxnumfactura&var='+variable
	}
}

function  RegistroCliente(){
	//var operacion =document.getElementById('operacion').value;
	var nombre=document.getElementById('nombre').value;
	var apellido=document.getElementById('apellido').value;
	var tipodoc=document.getElementById('tipodoc').value;
	var rif=document.getElementById('rif').value;
	var telf=document.getElementById('telf').value;
	var razon_social=document.getElementById('Razon_social').value;
	var bancosinteres=document.getElementById('bancosinteres').value;
	var bancos=document.getElementById('bancos').value;
	var tipopos=document.getElementById('tipopos').value;
	var cantidadpos=document.getElementById('cantidadpos').value;
	var Correo=document.getElementById('Correo').value;
	var Direccion=document.getElementById('Direccion').value;
	var medio_contacto=document.getElementById('medio_contacto').value;
	var tipo_cliente=document.getElementById('tipo_cliente').value;
	var usuario=document.getElementById('usuario').value;
	var bancoxdefecto=document.getElementById('bancoxdefecto').value;
	var n_afiliacion=document.getElementById('n_afiliacion').value;
    var banco1="";
    var banco2="";
    var documento="";
    var _tamCampoTlf=telf.length;
    
    if ((rif=="") && (tipodoc!="")) {
      alertify.error('Debe ingresar el número de documento.');
      return false;
    }
     if ((rif!="") && (tipodoc=="")) {
      alertify.error('Debe seleccionar tipo de documento.');
      return false;
    }
    if ((rif!="")&&(tipodoc!="")) {
     documento=tipodoc+rif;
    }
   

    if ((bancos!="") && ((nombre=="")||(apellido=="")||(rif=="")||(telf=="")||(razon_social=="")||(tipopos=="")||(cantidadpos=="")||(Correo=="")||(Direccion=="")||(medio_contacto==""))) {
     alertify.error('Error, debe llenar todos los campos');
	return false;

    }

    if (bancos!="") {
    	if (bancos=="0108" || bancos=="0105"){
  			if(rif.length<9){
  				alertify.error('Error, El RIF para este banco debe contener 9 digitos');
  	 			return false;
  			}
    	}
    	if (bancoxdefecto!="") {
    		if (bancoxdefecto=="997") {
            banco1=bancoxdefecto;
            banco2=bancos;
    		} 
    		if(bancoxdefecto=="999"){
             banco1=bancoxdefecto;
             banco2=bancos;
    		}
    	}else{
    		banco1=bancos;
    		banco2=bancos;

    	}
    }

    
if (bancos=="" && bancoxdefecto!="" ) {
     banco1=bancoxdefecto;
}
 if (telf!='' && _tamCampoTlf<15) {
    	 alertify.error('Error, Número de télefono incompleto');
    	 return false;
    }
 if (Correo!="" && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(Correo))){
      alertify.error("La dirección de email es incorrecta.");
      return false;
   }
   if(rif.length<9){
  				alertify.error('Error, El RIF debe contener 9 digitos');
  	 			return false;
  	}

if ((nombre=="") || (apellido=="") || (documento=="") || (telf=="")) {
	 alertify.error("Error, los campos estan vacios.Debe ingresar por lo menos Nombre, Apellido , RIF y Télefono");
     return false;    
}else{
	   //document.getElementById("guardarcaptacion").disabled = true; 
	   var cargar;
	   var variable=nombre+'_/-/'+apellido+'_/-/'+banco1+'_/-/'+documento+'_/-/'+telf+'_/-/'+bancosinteres+'_/-/'+usuario+'_/-/'+razon_social+'_/-/'+tipopos+'_/-/'+cantidadpos+'_/-/'+Correo+'_/-/'+Direccion+'_/-/'+medio_contacto+'_/-/'+tipo_cliente+'_/-/'+banco2+'_/-/'+n_afiliacion+'_/-/'+rif;
		 $.post('view/CaptacionCliente/validadorduplicidad.php',{postvariable:variable},
			   function(r)
			  { /*SI LA VARIABLE "r" ES IGUAL DISTINTO DE CERO SIGNIFICA QUE EL RIF, 
			  RAZON SOCIAL O AFILIADO EXISTE EN ALGUN REGISTRO DE LA TABLA TBL_PROSPECTOS*/
			  	if (r!=0) {
			  		alertify.confirm('Este registro ya existe en nuestro sistema, ¿desea crear un duplicado?', function (e) {
			  			if (e) {
			  				 guardarcaptacion(variable);
			  			} else{
			  				alertify.error("No se ha cargado la informaci&oacute;n");
			  			}
			  		})
			  	}/*SI LA VARIABLE "r" ES IGUAL A CERO NO EXISTE EN TBL_PROSPECTO Y PUEDE CARGAR LA INFORMACION*/
			  	else if (r==0) {
			  guardarcaptacion(variable);
			  	}
			  });
	   



	   
	
	}		  
        return false;
}

function guardarcaptacion(variable){
		 $.post('view/CaptacionCliente/guardarcaptacion.php',{postvariable:variable},
			   function(data)
			  {
			  	modified=false;
			alertify.success(data);
			cargar=0;
			   setTimeout(function(){ window.location.reload(0); }, 3000); 
			  });
			  		return false;
}

// BUSQUEDA PARA EL GERENTE EN MODULO CAPTACION CLIENTE -
function buscarclientesporasignar(){
	var variable=document.getElementById('clientes').value;
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	
	if(document.getElementById('clientes').value != "")
	{
		window.location.href='?cargar=registrosxasignar&var='+variable
	}
}

function buscarreporteparametrizacion(){

var variable=document.getElementById('clientes').value+'/'+document.getElementById('estatus').value;
		//alert(reporteeje);
		if((document.getElementById('clientes').value == "") && (document.getElementById('estatus').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('estatus').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('estatus').value != "")))
	{
		window.location.href='?cargar=porparametrizar&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('estatus').value != ""))
	{
		window.location.href='?cargar=porparametrizar&var='+variable
	}
		
}

function buscarreportecaptacion(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('ejecutivo').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
var variable1=document.getElementById('clientes').value+'/'+document.getElementById('ejecutivo').value;

	/*Busqueda solo por banco */
	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		var variable1= document.getElementById('clientes').value;
		window.location.href='?cargar=reportexejecutivo&var='+variable1
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=reportexejecutivo&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
		
		window.location.href='?cargar=reportexejecutivo&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
	
		window.location.href='?cargar=reportexejecutivo&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value =="") && (document.getElementById('fechafinal').value =="")){

		window.location.href='?cargar=reportexejecutivo&var='+variable
	}


		if((document.getElementById('clientes').value == "")  && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value != "") && (document.getElementById('fechainicial').value =="") && (document.getElementById('fechafinal').value =="")){

	window.location.href='?cargar=reportexejecutivo&var='+variable
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('ejecutivo').value == "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=reportexejecutivo&var='+variable
	}
	
		
	}
	function validarfechasreportecap(){

 var fechainicial=document.getElementById('fechainicial').value;
 var fechafinal=document.getElementById('fechafinal').value;
 
 var fechad = new Date(fechainicial);
 var aniodesde = fechad.getUTCFullYear();

 var fechah = new Date(fechafinal);
 var aniohasta = fechah.getUTCFullYear();

//alert (Date.parse(fechadesde));
//alert (Date.parse(fechahasta));

if ((isNaN(aniodesde)) || (isNaN(aniohasta)))
{
	document.getElementById('Buscar').disabled=true;
}
else if((Date.parse(fechainicial)) > (Date.parse(fechafinal))){
	//alertify.error("La fecha inicial no puede ser mayor que la fecha final");
	document.getElementById('Buscar').disabled=true;
	}
	else
	{
	document.getElementById('Buscar').disabled=false;	
	}
	
	
}

function eliminarduplicadosbd(){

	alertify.set({ labels: {
		ok: "Cargar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Eliminar los Registros Duplicados y Cargar la Informaci&oacute;n?", function (e) {
		if (e) {

			$.post('?cargar=transferencia4',
				function(data)
				{
					// ocultarcargar();
					alertify.success('Archivo guardado con &eacute;xito');
					setTimeout(function(){ window.location.reload(1); }, 5000);
//	alertify.success(data);

});
		} else {
			alertify.error("No se ha cargado la informaci&oacute;n");
			return false;
		}
	})
	return false;
}

function eliminarduplicados2cargar(){

	alertify.set({ labels: {
		ok: "Cargar",
		cancel: "Cancelar"
	}
});

	alertify.set({ buttonReverse: true });

	alertify.confirm("&#191;Desea Eliminar los Registros Duplicados y Cargar la Informaci&oacute;n?", function (e) {
		if (e) {

			$.post('?cargar=transferencia5',
				function(data)
				{
					// ocultarcargar();
					alertify.success('Archivo guardado con &eacute;xito');
					setTimeout(function(){ window.location.reload(1); }, 5000);
//	alertify.success(data);

});
		} else {
			alertify.error("No se ha cargado la informaci&oacute;n");
			return false;
		}
	})
	return false;
}






function autocompletar(count) {
		
		var texto = document.getElementById('validarserial'+count).value;
		var busqueda = document.getElementById('serial'+count).value;
		var marcapos = document.getElementById('marcapos').value;
		var banco = document.getElementById('banco').value;

		var busqmarca= busqueda.substring(5,7);  
		var marca_registro= marcapos.substring(1,3);  
		var variable=busqueda+'/'+banco;
		// console.log(marca_registro);
		// console.log(busqmarca);

		// if (marca_registro===busqmarca) {
		// 	console.log('bien')
		// } else {
		// 	console.log('error')
		// }
		if (busqueda.length>=7) {

	$.post('view/Administracion/facturacionpos/buscarserial.php',{postvariable:variable},
			function(data)
			{
				const array = JSON.parse(data);

			if (array.status!=0) { 

				 document.getElementById('validarserial'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('serial'+count).style = 'border-color:red;'; 
				 $('#guardarfacturacion').prop('disabled',true);	

		 }  else if (array.status==0 && marcapos!=array.marca){

		 		alertify.error("Error, el serial debe coincidir con el módelo del Equipo");
		 		document.getElementById('serial'+count).style = 'border-color:red;'; 
		 		//document.getElementById('validarserial'+count).innerHTML = "";
				$('#guardarfacturacion').prop('disabled',true);
				//console.log()
		 }

		 else  {
				
				//$('#lista_id'+count).show();
				$('#lista_id'+count).html(data);
				document.getElementById('validarserial'+count).innerHTML = "";
     			 // document.getElementById('serial'+count).style = 'border-color:control;';   12208CT31034448  "021827333301051915586102"
     			//$('#guardarfacturacion').removeAttr('disabled');
		}
			}) 

	}  if (busqueda.length<=6) {
		 	//$('#lista_id'+count).show();
				document.getElementById('serial'+count).style = 'border-color:red;'; 
				 document.getElementById('caracterespos'+count).innerHTML = "El serial está incompleto";
				 $('#guardarfacturacion').prop('disabled',true);	

		 } if (busqueda.length>=25) {
		 	//$('#lista_id'+count).show();
		 		document.getElementById('serial'+count).style = 'border-color:red;'; 
				 $('#guardarfacturacion').prop('disabled',true);	

		 } else {
		 	document.getElementById('serial'+count).style = 'border-color:control;';
		 	document.getElementById('caracterespos'+count).innerHTML = "";

		 	$('#guardarfacturacion').removeAttr('disabled');

		 }
}

function autocompletarsimcard(count) {
	var autosimcard = document.getElementById('validarsimcard'+count).value;
	var busqueda = document.getElementById('simcard'+count).value;
if (busqueda.length>=7) {

$.post('view/Administracion/facturacionpos/buscarsimcard.php',{postvariable:busqueda},
		function(data)
		{
			if (data==0) { 

			 document.getElementById('validarsimcard'+count).innerHTML = "Error, Serial no existe";
			 document.getElementById('simcard'+count).style = 'border-color:red;';
			 $('#guardarfacturacion').prop('disabled',true);

		 }	
		 	else
		 		{
				//$('#lista_id_sim'+count).show();
				$('#lista_id_sim'+count).html(data);
				document.getElementById('validarsimcard'+count).innerHTML = "";
     			document.getElementById('simcard'+count).style = 'border-color:control;'; 
     			$('#guardarfacturacion').removeAttr('disabled');
	}

}) 
	} 
}

function autocompletarmifi(count) {

	var automifi = document.getElementById('validarmifi'+count).value;
	var busqueda = document.getElementById('serialmifi'+count).value;
if (busqueda.length>=7) {

$.post('view/Administracion/facturacionpos/buscarmifi.php',{postvariable:busqueda},
		function(data)
		{
			if (data==0) { 

				document.getElementById('validarmifi'+count).innerHTML = "Error, Serial no existe";
				document.getElementById('serialmifi'+count).style = 'border-color:red;'; 
				$('#guardarfacturacion').prop('disabled',true);
				
		 }	else{

				//$('#lista_id_mifi'+count).show();
				$('#lista_id_mifi'+count).html(data);
				document.getElementById('validarmifi'+count).innerHTML = "";
     			document.getElementById('serialmifi'+count).style = 'border-color:control;'; 
     			$('#guardarfacturacion').removeAttr('disabled');
	}

}) 
	} 
}


function autocompletarserialdev(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('serialdev'+count).value;
		var texto = document.getElementById('validarserialdev'+count).value;
		var marcapos = document.getElementById('marcapos').value;
		var banco = document.getElementById('banco').value;
		var variable=busqueda+'/'+banco;
		if (busqueda.length>=7) {
	
	//console.log(busqueda);
	//Contamos el valor del input mediante una condicional
	$.post('view/Administracion/facturacionpos/buscarserial.php',{postvariable:variable},
			function(data)
			{

				const array = JSON.parse(data);

			if (array.status!=0) { 

				 document.getElementById('validarserialdev'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('serialdev'+count).style = 'border-color:red;'; 
				 $('#btnguardardevolucion').prop('disabled',true);	

		 }  else if (array.status==0 && marcapos!=array.marca){

		 		alertify.error("Error, el serial debe coincidir con el módelo del Equipo");
		 		document.getElementById('serialdev'+count).style = 'border-color:red;'; 
		 		document.getElementById('validarserialdev'+count).innerHTML = "";
				$('#btnguardardevolucion').prop('disabled',true);
		 }

		 else  {
				
				$('#lista_id'+count).html(data);
				document.getElementById('validarserialdev'+count).innerHTML = "";
		}
			}) 

	}  if (busqueda.length<=7) {

		 	//$('#lista_id'+count).show();
				document.getElementById('serialdev'+count).style = 'border-color:red;'; 
				 document.getElementById('caracteresposdev'+count).innerHTML = "El serial está incompleto";
				 $('#btnguardardevolucion').prop('disabled',true);	

		 } else {
		 	document.getElementById('serialdev'+count).style = 'border-color:control;';
		 	document.getElementById('caracteresposdev'+count).innerHTML = "";

		 	$('#btnguardardevolucion').removeAttr('disabled');

		 }
}

function autocompletarsimdev(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('simdev'+count).value;
		var texto = document.getElementById('validarsimdev'+count).value;
		if (busqueda.length>=7) {
	
	//console.log(busqueda);
	//Contamos el valor del input mediante una condicional
	$.post('view/Administracion/facturacionpos/buscarsimcard.php',{postvariable:busqueda},
			function(data)
			{
			if (data==0) { 

				 document.getElementById('validarsimdev'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('simdev'+count).style = 'border-color:red;'; 
				$('#btnguardardevolucion').prop('disabled',true);

		/*		alertify.error("Error, Serial no existe");
			setTimeout(function(){ window.location.reload(0); }, 2000);
		*/ }	else  {
				//datatype: 'json'
				
				//$('#lista_id_sim'+count).show();
				$('#lista_id_sim'+count).html(data);
				document.getElementById('validarsimdev'+count).innerHTML = "";
     			document.getElementById('simdev'+count).style = 'border-color:control;'; 
     			$('#btnguardardevolucion').removeAttr('disabled');
		}
			}) 
	} 
}

function autocompletarmifidev(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('serialmdev'+count).value;
		var texto = document.getElementById('validarmifidev'+count).value;
		if (busqueda.length>=7) {
	
	//console.log(busqueda);
	//Contamos el valor del input mediante una condicional
	$.post('view/Administracion/facturacionpos/buscarmifi.php',{postvariable:busqueda},
			function(data)
			{
			if (data==0) { 

				 document.getElementById('validarmifidev'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('serialmdev'+count).style = 'border-color:red;';
				 $('#btnguardardevolucion').prop('disabled',true);

				
		/*		alertify.error("Error, Serial no existe");
			setTimeout(function(){ window.location.reload(0); }, 2000);
		*/ }	else  {
				//datatype: 'json'
				
				//$('#lista_id_mifi'+count).show();
				$('#lista_id_mifi'+count).html(data);
				document.getElementById('validarmifidev'+count).innerHTML = "";
     			document.getElementById('serialmdev'+count).style = 'border-color:control;';
     			 $('#btnguardardevolucion').removeAttr('disabled'); 
		}
			}) 
	} 
}



function autoserialmifi(count) {

	var autoserial = document.getElementById('validargestionmifi'+count).value;
	var busqueda = document.getElementById('mifiserial'+count).value;
if (busqueda.length>=7) {

$.post('view/Administracion/facturacionpos/buscarmifi.php',{postvariable:busqueda},
		function(data)
		{
			if (data==0) { 

				document.getElementById('validargestionmifi'+count).innerHTML = "Error, Serial no existe";
				document.getElementById('mifiserial'+count).style = 'border-color:red;'; 
				$('#guardarfacturacionmifi').prop('disabled',true);
				
		 }	else{

				//$('#lista_id_mifi'+count).show();
				$('#listaidmifi'+count).html(data);
				document.getElementById('validargestionmifi'+count).innerHTML = "";
     			document.getElementById('mifiserial'+count).style = 'border-color:control;'; 
     			$('#guardarfacturacionmifi').removeAttr('disabled');
	}

}) 
	} 
}

function completarserialsimcard(count) {

	var autoserialsim = document.getElementById('validargestiondesim'+count).value;
	var busqueda = document.getElementById('simserial'+count).value;
if (busqueda.length>=7) {

$.post('view/Administracion/facturacionpos/buscarsimcard.php',{postvariable:busqueda},
		function(data)
		{
			if (data==0) { 

				document.getElementById('validargestiondesim'+count).innerHTML = "Error, Serial no existe";
				document.getElementById('simserial'+count).style = 'border-color:red;'; 
				$('#guardarfacturacionmifi').prop('disabled',true);
				
		 }	else{

				//$('#lista_id_mifi'+count).show();
				$('#listaid_numsim'+count).html(data);
				document.getElementById('validargestiondesim'+count).innerHTML = "";
     			document.getElementById('simserial'+count).style = 'border-color:control;'; 
     			$('#guardarfacturacionmifi').removeAttr('disabled');
	}

}) 
	} 
}


function autocompletarmifixdev(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('serialmifidev'+count).value;
		var texto = document.getElementById('validarmifixdev'+count).value;
		if (busqueda.length>=7) {
	
	//console.log(busqueda);
	//Contamos el valor del input mediante una condicional
	$.post('view/Administracion/facturacionpos/buscarmifi.php',{postvariable:busqueda},
			function(data)
			{
			if (data==0) { 

				 document.getElementById('validarmifixdev'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('serialmifidev'+count).style = 'border-color:red;';
				 $('#btnguardardevolucionsolomifi').prop('disabled',true);

				
		/*		alertify.error("Error, Serial no existe");
			setTimeout(function(){ window.location.reload(0); }, 2000);
		*/ }	else  {
				//datatype: 'json'
				
				//$('#lista_id_mifi'+count).show();
				$('#lista_id_mifi_xdev'+count).html(data);
				document.getElementById('validarmifidev'+count).innerHTML = "";
     			document.getElementById('serialmifidev'+count).style = 'border-color:control;';
     			 $('#btnguardardevolucionsolomifi').removeAttr('disabled'); 
		}
			}) 
	} 
}



function autocompletarsimcardxdev(count) {
	//var minimo_letras = 0; // minimo letras visibles en el autocompletar
		var busqueda = document.getElementById('devsimcard'+count).value;
		var texto = document.getElementById('validarsimcardxdev'+count).value;
		if (busqueda.length>=7) {
	
	//console.log(busqueda);
	//Contamos el valor del input mediante una condicional
	$.post('view/Administracion/facturacionpos/buscarsimcard.php',{postvariable:busqueda},
			function(data)
			{
			if (data==0) { 

				 document.getElementById('validarsimcardxdev'+count).innerHTML = "Error, Serial no existe";
				 document.getElementById('devsimcard'+count).style = 'border-color:red;';
				 $('#btnguardardevolucionsolomifi').prop('disabled',true);

				
		/*		alertify.error("Error, Serial no existe");
			setTimeout(function(){ window.location.reload(0); }, 2000);
		*/ }	else  {
				//datatype: 'json'
				
				//$('#lista_id_mifi'+count).show();
				$('#lista_id_simcard_xdev'+count).html(data);
				document.getElementById('validarsimcardxdev'+count).innerHTML = "";
     			document.getElementById('devsimcard'+count).style = 'border-color:control;';
     			 $('#btnguardardevolucionsolomifi').removeAttr('disabled'); 
		}
			}) 
	} 
}


// Funcion Mostrar valores
function set_item(opciones) {
	// Cambiar el valor del formulario input
	$('#serial').val(opciones);
	// ocultar lista de proposiciones
	$('#lista_id').hide();
}

function factanulada() {
       var fac = document.getElementById("factura");
       var check = document.getElementById("check");
       var checkdev = document.getElementById("devolucion");
       var equiposdevtotal = document.getElementById("devoluciontotal"); 
       var percances = document.getElementById("percances"); 

        if (check.checked) {
            fac.style.display='block';
            checkdev.style.display='none';
            equiposdevtotal.style.display='none';
            percances.style.display='none';
            document.getElementById('guardarfacturacion').disabled=true;
        }
        else {
            fac.style.display='none';
        }
    }


    function factanuladamifi() {
       var facmifi = document.getElementById("facturamifi");
       var checkmifi = document.getElementById("checkmifi");
       var checkdevmifi = document.getElementById("devolucionmifi");
       var equiposdevtotalmifi = document.getElementById("devoluciontotalmifi"); 

        if (checkmifi.checked) {
            facmifi.style.display='block';
            checkdevmifi.style.display='none';
            equiposdevtotalmifi.style.display='none';
            document.getElementById('guardarfacturacionmifi').disabled=true;
        }
        else {
            facmifi.style.display='none';
        }
    }


function devequipos() {

	   var fac = document.getElementById("factura");
       var equivosdev = document.getElementById("devolucion");
       var equiposdevtotal = document.getElementById("devoluciontotal");
       var checkdev = document.getElementById("checkdev");
       var percances = document.getElementById("percances"); 

        if (checkdev.checked) {
            equivosdev.style.display='block';
            equiposdevtotal.style.display='none';
            fac.style.display='none';
            percances.style.display='none';
            document.getElementById('guardarfacturacion').disabled=true;
        }
        else {
            equivosdev.style.display='none';
        }
    }

    function devequiposmifi() {

	   var facmifi = document.getElementById("facturamifi");
       var equivosdevmifi = document.getElementById("devolucionmifi");
       var equiposdevtotalmifi = document.getElementById("devoluciontotalmifi");
       var checkdevmifi = document.getElementById("checkdevmifi");

        if (checkdevmifi.checked) {
            equivosdevmifi.style.display='block';
            equiposdevtotalmifi.style.display='none';
            facmifi.style.display='none';
            document.getElementById('guardarfacturacionmifi').disabled=true;
        }
        else {
            equivosdevmifi.style.display='none';
        }
    }

    function devtotalequipos() {
       var fac = document.getElementById("factura");
       var checkdev = document.getElementById("devolucion");
       var equiposdevtotal = document.getElementById("devoluciontotal");
	   	var checkdevtotal = document.getElementById("checkdevtotal");
	   	var percances = document.getElementById("percances");

        if (checkdevtotal.checked) {
            equiposdevtotal.style.display='block';	
            fac.style.display='none';
            checkdev.style.display='none';
            percances.style.display='none';
            document.getElementById('guardarfacturacion').disabled=true;
           
        }
        else {
            equiposdevtotal.style.display='none';
        }
    }

    function percances() {

    	var checkdev = document.getElementById("devolucion");
    	var fac = document.getElementById("factura");
      var checkperc = document.getElementById("devoluciontotal");
      var percances = document.getElementById("percances");
	   	var checkpercances = document.getElementById("checkpercances");

        if (checkpercances.checked) {
            percances.style.display='block';	
            fac.style.display='none';
            checkdev.style.display='none';
            checkperc.style.display='none';
            document.getElementById('guardarfacturacion').disabled=true;
           
        }
        else {
            percances.style.display='none';
        }
    }

    function devtotalequiposmifi() {
	       var facmifi = document.getElementById("facturamifi");
	       var checkdevmifi = document.getElementById("devolucionmifi");
	       var equiposdevtotalmifi = document.getElementById("devoluciontotalmifi");
		   	 var checkdevtotalmifi = document.getElementById("checkdevtotalmifi");

        if (checkdevtotalmifi.checked) {
            equiposdevtotalmifi.style.display='block';	
            facmifi.style.display='none';
            checkdevmifi.style.display='none';
            document.getElementById('guardarfacturacionmifi').disabled=true;
           
        }
        else {
            equiposdevtotalmifi.style.display='none';
        }
    }


    function ver_imgfactura($id) {

	  var idregistro = document.getElementById("idregistro").value;
	  console.log(idregistro);
	  if (idregistro!="") {
	  	window.open('verfactura?cargar=ver_factura&id='+idregistro,'_blank');
			//window.open.target = "_blank";
			//window.location.href='?cargar=ver_factura&id='+idregistro

	}else{
		alertify.error('Error');
	}
    }


function ver_imgfacturaanterior($id) {

	  var idregistro = document.getElementById("idregistro").value;
	  
	  if (idregistro!="") {
	  	window.open('verfactura?cargar=factura_anterior&id='+idregistro,'_blank');
			//window.open.target = "_blank";
			//window.location.href='?cargar=ver_factura&id='+idregistro

	}else{
		alertify.error('Error');
	}
    }

//que es esto??????????????????????????
function validararhivoUbii(){
	
	var	archivo1=document.getElementById('archivoUbii').value;

	var	tamano1=document.getElementById('archivoUbii');
	
		
	if((document.getElementById('archivoUbii').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO A CARGAR.!");
		document.getElementById('archivoUbii').focus();
		return false;
	}


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoUbii').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoUbii').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoUbii2').value;

		var	tamano1=document.getElementById('archivoUbii2');
	
	
		var extensionesValidas = " .jpeg, .jpg, .pdf";
		var pesoPermitido = 15000;
	
		if (tamano1.files && tamano1.files[0]) {
	
		  var pesoFichero1 = tamano1.files[0].size/15000;
	
				if(pesoFichero1 > pesoPermitido) {
			alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
			document.getElementById('archivoUbii2').focus();
			return false;
		   }
		  }
	
		
		  //valida fotos
		var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
		var extensionValida1 = extensionesValidas.indexOf(extension1);
		
	
	
		if(extensionValida1 < 0) {
	
			alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
			document.getElementById('archivoUbii2').focus();
			return false;
		}
	
		else{
			var	archivo1=document.getElementById('archivoUbii3').value;

	var	tamano1=document.getElementById('archivoUbii3');


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoUbii3').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoUbii3').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoUbii4').value;

	var	tamano1=document.getElementById('archivoUbii4');


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoUbii4').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoUbii4').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoUbii5').value;

		var	tamano1=document.getElementById('archivoUbii5');
	
	
		var extensionesValidas = " .jpeg, .jpg, .pdf";
		var pesoPermitido = 15000;
	
		if (tamano1.files && tamano1.files[0]) {
	
		  var pesoFichero1 = tamano1.files[0].size/15000;
	
				if(pesoFichero1 > pesoPermitido) {
			alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
			document.getElementById('archivoUbii5').focus();
			return false;
		   }
		  }
	
		
		  //valida fotos
		var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
		var extensionValida1 = extensionesValidas.indexOf(extension1);
		
	
	
		if(extensionValida1 < 0) {
	
			alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
			document.getElementById('archivoUbii5').focus();
			return false;
		}
	
		else{
			var	archivo1=document.getElementById('archivoUbii6').value;

	var	tamano1=document.getElementById('archivoUbii6');


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoUbii6').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoUbii6').focus();
		return false;
	}

	else{
		document.subir.submit();
	}
		}
	}
	}
		}
	}
}

function validararhivoUcomp(){
	
	var	archivo1=document.getElementById('archivoUc').value;

	var	tamano1=document.getElementById('archivoUc');
	
		
	if((document.getElementById('archivoUc').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO A CARGAR.!");
		document.getElementById('archivoUc').focus();
		return false;
	}


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 50000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/50000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoUc').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoUc').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoU2c').value;

	var	tamano1=document.getElementById('archivoU2c');

	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 50000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/50000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoU2c').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoU2c').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoU3c').value;

		var	tamano1=document.getElementById('archivoU3c');
	
		var extensionesValidas = " .jpeg, .jpg, .pdf";
		var pesoPermitido = 50000;
	
		if (tamano1.files && tamano1.files[0]) {
	
		  var pesoFichero1 = tamano1.files[0].size/50000;
	
				if(pesoFichero1 > pesoPermitido) {
			alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
			document.getElementById('archivoU3c').focus();
			return false;
		   }
		  }
	
		
		  //valida fotos
		var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
		var extensionValida1 = extensionesValidas.indexOf(extension1);
		
	
	
		if(extensionValida1 < 0) {
	
			alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
			document.getElementById('archivoU3c').focus();
			return false;
		}
	
		else{
			document.subircU.submit();
		}
	}

	}
}
// 

// paquete e validadores de documentos, esta formula se debe emplear cada que se quieran evaluar independientemente los 6 documentos de carga de exp
function validararhivoIp(){
	
	var	archivo1=document.getElementById('archivoIp').value;

	var	tamano1=document.getElementById('archivoIp');
	
		
	if((document.getElementById('archivoIp').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO A CARGAR.!");
		document.getElementById('archivoIp').focus();
		return false;
	}


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoIp').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoIp').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoIp2').value;

		var	tamano1=document.getElementById('archivoIp2');
	
	
		var extensionesValidas = " .jpeg, .jpg, .pdf";
		var pesoPermitido = 15000;
	
		if (tamano1.files && tamano1.files[0]) {
	
		  var pesoFichero1 = tamano1.files[0].size/15000;
	
				if(pesoFichero1 > pesoPermitido) {
			alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
			document.getElementById('archivoIp2').focus();
			return false;
		   }
		  }
	
		
		  //valida fotos
		var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
		var extensionValida1 = extensionesValidas.indexOf(extension1);
		
	
	
		if(extensionValida1 < 0) {
	
			alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
			document.getElementById('archivoIp2').focus();
			return false;
		}
	
		else{
			var	archivo1=document.getElementById('archivoIp3').value;

	var	tamano1=document.getElementById('archivoIp3');


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoIp3').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoIp3').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoIp4').value;

	var	tamano1=document.getElementById('archivoIp4');


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoIp4').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoIp4').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivoIp5').value;

		var	tamano1=document.getElementById('archivoIp5');
	
	
		var extensionesValidas = " .jpeg, .jpg, .pdf";
		var pesoPermitido = 15000;
	
		if (tamano1.files && tamano1.files[0]) {
	
		  var pesoFichero1 = tamano1.files[0].size/15000;
	
				if(pesoFichero1 > pesoPermitido) {
			alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
			document.getElementById('archivoIp5').focus();
			return false;
		   }
		  }
	
		
		  //valida fotos
		var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
		var extensionValida1 = extensionesValidas.indexOf(extension1);
		
	
	
		if(extensionValida1 < 0) {
	
			alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
			document.getElementById('archivoIp5').focus();
			return false;
		}
	
		else{
			var	archivo1=document.getElementById('archivoIp6').value;

	var	tamano1=document.getElementById('archivoIp6');


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 15000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/15000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivoIp6').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivoIp6').focus();
		return false;
	}

	else{
		document.subirIp.submit();
	}
		}
	}
	}
		}
	}
}
//  aqui cierran los validadores de docs

    function validararhivocompIp(){
	
	var	archivo1=document.getElementById('archivocIp').value;

	var	tamano1=document.getElementById('archivocIp');
	
		
	if((document.getElementById('archivocIp').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO A CARGAR.!");
		document.getElementById('archivocIp').focus();
		return false;
	}


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 50000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/50000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivocIp').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivocIp').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivo2c').value;

	var	tamano1=document.getElementById('archivo2c');
	
		
	if((document.getElementById('archivocIp').value == ""))
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO A CARGAR.!");
		document.getElementById('archivocIp').focus();
		return false;
	}


	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 50000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/50000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivocIp').focus();
		return false;
	   }
	  }

	
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	


	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
		document.getElementById('archivocIp').focus();
		return false;
	}

	else{
		var	archivo1=document.getElementById('archivo3c').value;

		var	tamano1=document.getElementById('archivo3c');
		
			
		if((document.getElementById('archivocIp').value == ""))
		{
			alertify.alert("DEBE SELECCIONAR EL ARCHIVO A CARGAR.!");
			document.getElementById('archivocIp').focus();
			return false;
		}
	
	
		var extensionesValidas = " .jpeg, .jpg, .pdf";
		var pesoPermitido = 50000;
	
		if (tamano1.files && tamano1.files[0]) {
	
		  var pesoFichero1 = tamano1.files[0].size/50000;
	
				if(pesoFichero1 > pesoPermitido) {
			alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
			document.getElementById('archivocIp').focus();
			return false;
		   }
		  }
	
		
		  //valida fotos
		var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
		var extensionValida1 = extensionesValidas.indexOf(extension1);
		
	
	
		if(extensionValida1 < 0) {
	
			alertify.alert('La extensión no es válida, Su Factura tiene de extensión: .'+ extension1);
			document.getElementById('archivocIp').focus();
			return false;
		}
	
		else{
			document.subircIp.submit();
		}
	}

	}
}
// 



function buscarconfiguracionmifi(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))
	
	{
		window.location.href='?cargar=buscarserviciomifi&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscarconfigmifi&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarconfigmifi&var='+variable
	}
}  

function guardarDeclinacionCompra(){
	
	var cantidadterminales= document.getElementById('cantidadterminales').value;
	var contador=0;
	var idregistro=document.getElementById('idregistro').value;
	var facturadev =document.getElementById('facturadev').value;
	var fechadev =document.getElementById('fechadev').value;
	var montodev =document.getElementById('montodev').value;

	//var descmotivo =document.getElementById('descmotivo').value;

	var usuario =document.getElementById('usuario').value;

// var str = montofactura;
var point=montodev;
var amountnopoint = point.toString().replace(/\./g,''); 
var amountfinal = amountnopoint.replace(/,/g, ".");
//var Newmonto = res.toString().replace(/\./g,''); 
	if (idregistro=="") {
		alertify.error('Error, id del registro no puede estar vacio');
		document.getElementById('idregistro').style.borderColor="red";
		return false;
	}
	if (facturadev==""){
	alertify.error('Error, el número de factura es un campo requerido');
    document.getElementById('facturadev').style.borderColor="red";
    return false;
	}
	if (fechadev==""){
	alertify.error('Error, el campo fecha es requerido');
    document.getElementById('fechadev').style.borderColor="red";
    return false;
	}
	if (montodev=="0"){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('montodev').style.borderColor="red";
    return false;
	}

	for (var i = 0; i < cantidadterminales; i++) {
			contador++;

	//var serialdevtotal =document.getElementById("serialdevtotal"+[contador]).value;
	var serialmifidec =document.getElementById("serialmifidec"+[contador]).value;
	var decsimcard =document.getElementById("decsimcard"+[contador]).value;
	var codigomotivodev =document.getElementById("codigomotivodev"+[contador]).value;
	var consecutivo =document.getElementById("consecutivo"+[contador]).value;
	alert(serialmifidec+'/'+decsimcard+'/'+codigomotivodev+'/'+consecutivo);
	//alert(serialdev);		
	//alert (variable);
	var variable=idregistro+'/'+facturadev+'/'+fechadev+'/'+amountfinal+'/'+serialmifidec+'/'+decsimcard+'/'+codigomotivodev+'/'+usuario+'/'+consecutivo;
	$.post('view/Administracion/facturacionmifi/guardardeclinacionmifi.php',{postvariable:variable},

		function(data)
		{
	//alert(variable);
	

});

}

alertify.success("Devolución Guardada con exito");
	setTimeout(function(){ window.location.reload(0); }, 3000000);		
 
}

    function ver_imgfacturademifi($id) {

	  var idregistro = document.getElementById("idregistro").value;
	  console.log(idregistro);
	  if (idregistro!="") {
	  	window.open('verfactura_mifi?cargar=ver_factura_mifi&id='+idregistro,'_blank');
			//window.open.target = "_blank";
			//window.location.href='?cargar=ver_factura&id='+idregistro

	}else{
		alertify.error('Error');
	}
    }


function ver_imgfacturaanteriordemifi($id) {

	  var idregistro = document.getElementById("idregistro").value;
	  
	  if (idregistro!="") {
	  	window.open('verfactura_mifi?cargar=factura_anterior_mifi&id='+idregistro,'_blank');
			//window.open.target = "_blank";
			//window.location.href='?cargar=ver_factura&id='+idregistro

	}else{
		alertify.error('Error');
	}
    }


function buscarservitecrosal(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))

	{
		window.location.href='?cargar=buscargestion&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscargestiondetallada&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscargestiondetallada&var='+variable
	}
}



function gestionrosalrif(){

	/*var value = $('#busqueda').val();
  	var value_without_space = $.trim(value);
 	var busqueda = $('#busqueda').val(value_without_space);*/

	if(document.getElementById('busqueda').value == "")
	{
		alertify.alert("DEBE INGRESAR EL N° DE AFILIACIÓN RIF/CI O RAZÓN SOCIAL!.");
		document.getElementById('busqueda').focus();
		return false;
	}
	
	else {
		var busqueda =document.getElementById('busqueda').value;
		window.location.href='?cargar=buscargestionxrif&var='+busqueda

	}
}

function buscarreporteestatusgestiondelrosal(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('almacen').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('almacen').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if(((document.getElementById('clientes').value != "") && (document.getElementById('almacen').value == ""))
	|| ((document.getElementById('clientes').value == "") && (document.getElementById('almacen').value != "")))
	{
		window.location.href='?cargar=generarreportegestionrosal&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('almacen').value != ""))
	{
		window.location.href='?cargar=generarreportegestionrosal&var='+variable
	}
}



function actualizaciondatos(){
	
	var idregistro=document.getElementById('id_registro').value;
	var serialp=document.getElementById('serialp').value;
	var usuario=document.getElementById('usuario').value;

	var variable=idregistro+'/'+serialp+'/'+usuario;
		//alert(variable);
	$.post('view/comercializacion/gestion/modificardatos.php',{postvariable:variable},
		function(data)
		{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 
		});

	return false;
}


function buscarreportesustitucion(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=generarreportesustituciones&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}

function buscarclienteencuesta(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fecha').value;

	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	if((document.getElementById('clientes').value == "") && (document.getElementById('fecha').value != ""))
	
	{
		window.location.href='?cargar=buscarfechaencuesta&var='+variable
	}
	
	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value == ""))
	{
		window.location.href='?cargar=buscarbancoencuesta&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fecha').value != ""))
	{
		window.location.href='?cargar=buscarbancfechaencuesta&var='+variable
	}
}

function listaencuesta(){

	/*var value = $('#busqueda').val();
  	var value_without_space = $.trim(value);
 	var busqueda = $('#busqueda').val(value_without_space);*/

	if(document.getElementById('busqueda').value == "")
	{
		alertify.alert("DEBE INGRESAR EL N° DE AFILIACIÓN RIF/CI O RAZÓN SOCIAL!.");
		document.getElementById('busqueda').focus();
		return false;
	}
	
	else {
		var busqueda =document.getElementById('busqueda').value;
		window.location.href='?cargar=buscarrifencuesta&var='+busqueda

	}
}

function buscarpreguntaencuesta(){

var variable=document.getElementById('clientes').value+'/'+ document.getElementById('detalle').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
var variable1=document.getElementById('clientes').value+'/'+ document.getElementById('detalle').value
	//alert(variable1);
	if(document.getElementById('detalle').value == "")
	
	{
		alertify.alert("DEBE SELECCIONAR UN DETALLE A CONSULTAR!.");
		document.getElementById('detalle').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	}
	if(document.getElementById('clientes').value == "")
	
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO PARA CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	}
var detallepregt=document.getElementById('detalle').value
if (detallepregt==1){
	/*Busqueda solo por banco */
	if((document.getElementById('clientes').value != "") && (document.getElementById('detalle').value !== "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		var variable1= document.getElementById('clientes').value;
		window.location.href='?cargar=buscarpreguntasa&var='+variable1
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=buscarpreguntasa&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
		
		window.location.href='?cargar=buscarpreguntasa&var='+variable
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	}
	//if((document.getElementById('clientes').value !== "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value ="") && (document.getElementById('fechafinal').value ="")){

	//window.location.href='?cargar=buscarpreguntas&var='+variable1
	//}
	
	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value == "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=buscarpreguntasa&var='+variable
	}
} else
	if (detallepregt==2){
	/*Busqueda solo por banco */
	if((document.getElementById('clientes').value != "") && (document.getElementById('detalle').value !== "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		var variable1= document.getElementById('clientes').value;
		window.location.href='?cargar=buscarpreguntasb&var='+variable1
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=buscarpreguntasb&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
		
		window.location.href='?cargar=buscarpreguntasb&var='+variable
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		window.location.href='?cargar=reportexejecutivo&var='+variable
	}
	
	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value == "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=buscarpreguntasb&var='+variable
	}
}else
	if (detallepregt==3){
	/*Busqueda solo por banco */
	if((document.getElementById('clientes').value != "") && (document.getElementById('detalle').value !== "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		var variable1= document.getElementById('clientes').value;
		window.location.href='?cargar=buscarpreguntasc&var='+variable1
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=buscarpreguntasc&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
		
		window.location.href='?cargar=buscarpreguntasc&var='+variable
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		window.location.href='?cargar=reportexejecutivo&var='+variable
	}
	
	if((document.getElementById('clientes').value == "") && (document.getElementById('detalle').value == "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=buscarpreguntasc&var='+variable
	}
}
}
function buscarestatusencuesta(){
var variable=document.getElementById('clientes').value

	if(document.getElementById('clientes').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	
	if(document.getElementById('clientes').value != "")
	{
		window.location.href='?cargar=buscarestado&var='+variable
	}
}

function buscarregistroencuesta(){

var variable=document.getElementById('clientes').value+'/'+document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;
var variable1=document.getElementById('clientes').value
	//alert(variable1);
	/*Busqueda solo por banco */
	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		var variable1= document.getElementById('clientes').value;
		window.location.href='?cargar=buscarregistros&var='+variable1
	}

	if((document.getElementById('clientes').value == "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){

	window.location.href='?cargar=buscarregistros&var='+variable
	}

	if((document.getElementById('clientes').value != "") && (document.getElementById('fechainicial').value !="") && (document.getElementById('fechafinal').value !="")){
		
		window.location.href='?cargar=buscarregistros&var='+variable
	}

	if((document.getElementById('clientes').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO PARA CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	}
	
}

function listaregistrosencuesta(){

	/*var value = $('#busqueda').val();
  	var value_without_space = $.trim(value);
 	var busqueda = $('#busqueda').val(value_without_space);*/

	if(document.getElementById('busqueda').value == "")
	{
		alertify.alert("DEBE INGRESAR EL N° DE AFILIACIÓN RIF/CI O RAZÓN SOCIAL!.");
		document.getElementById('busqueda').focus();
		return false;
	}
	
	else {
		var busqueda =document.getElementById('busqueda').value;
		window.location.href='?cargar=buscarregistrosrif&var='+busqueda

	}
}

function enviarcaptacion(){

	var variable=document.getElementById('nombreref').value+'/'+document.getElementById('apellidoref').value+'/'+document.getElementById('telefono').value;
	window.location.href='CaptacionCliente?cargar=iniciocaptacioncliente&var='+variable;
}


function personarefcpntactado()
{
  if (document.getElementById("estatus").checked === true) { 
    document.getElementById("estatus").disabled=false;
  }

  if (  document.getElementById("estatus").checked = true) {

    alertify.set({ labels: {
      ok: "Habilitar",
      cancel: "Cancelar"
    }
  });

alertify.set({ buttonReverse: true });
id_registro=document.getElementById('idregistro').value;
usuario=document.getElementById('usuario').value;
    alertify.confirm("&#191;Desea indicar que el referido fue contactado ?", function (e) {
      if (e) {
        $.post('view/Encuesta/personarefcontactada.php',{postvariable:id_registro+'/'+usuario},
          function(data)
          {
            alertify.success(data);
            setTimeout(function(){ window.location.reload(0); }, 5000);
          });
      } else {
        alertify.error("Acción cancelada.");
        setTimeout(function(){ window.location.reload(0); }, 5000);
        return false;
      }
    })
    return false;
  }
}

function contactogestionencuesta (){

	idcliente=document.getElementById('id_cliente').value;
    rlegal=document.getElementById('rlegal').value;
    pcontacto=document.getElementById('pcontacto').value;
    usuario=document.getElementById('usuario').value;
    estatusgestion=document.getElementById('estatusgestion').value;
    
    contador=0;
    iteraciones=3;

    var variable=idcliente+'/'+rlegal+'/'+pcontacto+'/'+usuario+'/'+estatusgestion;

    $.post('view/encuesta/gestiontelefonicaencuesta.php',{postvariable:variable}, 
    	function(data){
    		alertify.success(data);
    	});
    setTimeout(function(){ window.location.reload(0); }, 3000);
    return false;
}

  function generarestatusencuesta(){
	var el_valor =document.getElementById('estatusactual').value;
	var idcliente=document.getElementById('id_cliente').value;
	var variable=el_valor+'/'+idcliente;
	//lista estatus
	$.post( 'view/Encuesta/estatusencuesta.php', { valor: variable} ).done( function(respuesta)
	{
		$( '#estatusgestion' ).html( respuesta );
	});
}


$(document).ready(function(){
    var encuestapos= 1;
    var encuItems = $('.encuesta li').length;
    var permiso=0;
 
    $('.encuesta li' ).hide();
    $('.encuesta li:first' ).show();
    if (encuestapos==1) {
      $('.botonprev').hide();
      $('.finish').hide();
    }


    $('#rightq').click(nextq);
    $('#leftq').click(prevq);
    $('#finishq').click(actualizarestatus);

    function nextq(){
  		idbanco=document.getElementById('banco').value
		idcliente=document.getElementById('id_cliente').value;
		usuario=document.getElementById('usuario').value;
		rlegal=document.getElementById('rlegal').value;

		if (encuestapos==1) {
			if ($('#encuesta-s input[name=pregunta1]:radio').is(':checked')) {
				respuesta1=$('input[name=pregunta1]:checked').val();
				var variable=respuesta1+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});  
				permiso=1;
			}else{
				alertify.error('Debe seleccionar una opción');
			}	
		}else if(encuestapos==2){
			if ($('#encuesta-s input[name=pregunta2]:radio').is(':checked')) {
				respuesta2=$('input[name=pregunta2]:checked').val();
	    		if (respuesta2==1) {
		    		var respuesta2_2=$('textarea#respuesta2-3').val();
		    		if (respuesta2_2!="") {
		    			var variable=respuesta2+'/'+respuesta2_2+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
			            $.post('view/encuesta/guardarencuesta2.php',{postvariable:variable});
			    	    permiso=1;
		    		}else{
						alertify.error('Debe ingresar información');
					}
	            }else{
		    		var variable=respuesta2+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		            $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	    permiso=1;
	            }
			}else{
				alertify.error('Debe seleccionar una opción');
			}	
		}else if (encuestapos==3){
			if ($('#encuesta-s input[name=pregunta3]:radio').is(':checked')) {
				respuesta3=$('input[name=pregunta3]:checked').val();
	    		var variable=respuesta3+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
			}else{
				alertify.error('Debe seleccionar una opción');
			}
		}else if(encuestapos==4){
			if ($('#encuesta-s input[name=pregunta4]:radio').is(':checked')) {
				respuesta4=$('input[name=pregunta4]:checked').val();
		    	var variable=respuesta4+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
			}else{
				alertify.error('Debe seleccionar una opción');
			}
	    }else if(encuestapos==5){
	    	if ($('#encuesta-s input[name=pregunta5]:radio').is(':checked')) {
	    		respuesta5=$('input[name=pregunta5]:checked').val();
		    	var variable=respuesta5+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
	    	}else{
				alertify.error('Debe seleccionar una opción');
			}
	    }else if(encuestapos==6){
	    	respuesta6=$('textarea#respuesta6').val();
	    	if (respuesta6!="") {
	    		var variable=respuesta6+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
	    	}else{
				alertify.error('Debe ingresar información');
			}
	    }else if (encuestapos==7){
	    	if ($('#encuesta-s input[name=pregunta7]:radio').is(':checked')) {
	    		respuesta7=$('input[name=pregunta7]:checked').val();
		    	if (respuesta7==2) {
		    		var respuesta7_2=$('textarea#respuesta7-3').val();
		    		if (respuesta7_2!="") {
		    			var variable=respuesta7+'/'+respuesta7_2+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
			            $.post('view/encuesta/guardarencuesta2.php',{postvariable:variable});
			    	    permiso=1;
		    		}else{
						alertify.error('Debe ingresar información');
					}
		    	}else{
		    		var variable=respuesta7+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		            $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	    permiso=1;
		    	}
	    	}else{
				alertify.error('Debe seleccionar una opción');
			}
	    }else if(encuestapos==8){
	    	if ($('#encuesta-s input[name=pregunta8]:radio').is(':checked')) {
	    		respuesta8=$('input[name=pregunta8]:checked').val();
		    	if (respuesta8==1) {
		    		nombreref=document.getElementById('respuesta8-4').value;
		    		apellidoref=document.getElementById('respuesta8-5').value;
		    		tlfref=document.getElementById('respuesta8-6').value;
		    		if (nombreref!="" && apellidoref!="" && tlfref!="") {
		    			var variable=respuesta8+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal+'/'+nombreref+'/'+apellidoref+'/'+tlfref;
				        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
				    	permiso=1;
		    		}else{
		    			alertify.error('Debe colocar todos los datos del referido');
		    		}
		    	}else{
			    	var variable=respuesta8+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
			        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
			    	permiso=1;
			    }	
	    	}else{
				alertify.error('Debe seleccionar una opción');
			}    
	    }else if(encuestapos==9){
	    	if ($('#encuesta-s input[name=pregunta9]:radio').is(':checked')) {
	    		respuesta9=$('input[name=pregunta9]:checked').val();
		    	var variable=respuesta9+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
	    	}else{
				alertify.error('Debe seleccionar una opción');
			}    
	    }else if(encuestapos==10){
	    	if ($('#encuesta-s input[name=pregunta10]:radio').is(':checked')) {
	    		respuesta10=$('input[name=pregunta10]:checked').val();
		    	var variable=respuesta10+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
	    	}else{
				alertify.error('Debe seleccionar una opción');
			}     
	    }else if(encuestapos==11){
	    	if ($('#encuesta-s input[name=pregunta11]:radio').is(':checked')) {
	    		respuesta11=$('input[name=pregunta11]:checked').val();
		    	var variable=respuesta11+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
	    	}else{
				alertify.error('Debe seleccionar una opción');
			}
	    }else if(encuestapos==12){
	    	respuesta12=$('textarea#respuesta12').val();
	    	if (respuesta12!="") {
	    		var variable=respuesta12+'/'+encuestapos+'/'+idcliente+'/'+idbanco+'/'+usuario+'/'+rlegal;
		        $.post('view/encuesta/guardarencuesta.php',{postvariable:variable});
		    	permiso=1;
	    	}else{
			  alertify.error('Debe ingresar información');
			}
		}

		if (permiso==1) {
			if (encuestapos >= encuItems) {
		      encuestapos=1;
		    }else{
		      encuestapos++;
		    }

		    $('.encuesta li' ).hide();
		    $('.encuesta li:nth-child('+encuestapos+')' ).fadeIn();
		    if (encuestapos>=1) {
		      $('.botonprev').show();
		      if(encuestapos>12){
		        $('.botonnext').hide();
		        $('.finish').fadeIn();
		      }
		    }
		    permiso=0;
		}
	    

	}

	function prevq(){

	    idcliente=document.getElementById('id_cliente').value;

	    if (encuestapos <= 1) {
	      encuestapos=encuItems;
	    }else{
	      encuestapos--;
	    }

	    $('.encuesta li' ).hide();
	    $('.encuesta li:nth-child('+encuestapos+')' ).fadeIn();
	    if (encuestapos<=1) {
	      $('.botonprev').hide();
	    }

	    var variable=encuestapos+'/'+idcliente;
	        $.post('view/encuesta/eliminarencuesta.php',{postvariable:variable});
	      return false;
    } 

    function actualizarestatus(){
    	idcliente=document.getElementById('id_cliente').value;
    	var variable=idcliente;
	    $.post('view/encuesta/finalizarencuesta.php',{postvariable:variable}, 
	    function(data){
	        alertify.success(data);
	    });
	    setTimeout(function(){ window.location.reload(0); }, 3000);
	    return false;
    } 

    
})

function validaciones(){

	$("input#fechapago").blur(function() {
        var fechapagoa= $(this).val();
        if (fechapagoa!=="") {
        	$('#errorfechapago').hide()        
        }
    })
    $("select#formapago").blur(function() {
        var formapagoa= $(this).val();
        if (formapagoa!=="" || formapagoa=="0") {
        	$('#errorformapago').hide()       
        }
    })
	$("select#tipomoneda").blur(function() {
        var tipomonedaa= $(this).val();
        if (tipomonedaa!=="") {
        	$('#errormoneda').hide()       
        }
        
    })
	$("input#nombredepositante").blur(function() {
        var nombredepositantea= $(this).val();
        if (nombredepositantea!=="") {
        	$('#errordepositante').hide()       
        }
        if (nombredepositantea.length<2) {
        	 document.getElementById("errordepositantemax").innerHTML="El nombre debe tener al menos 2 carácteres";      
        } else if (nombredepositantea.length>200) {
        	 document.getElementById("errordepositantemax").innerHTML="El largo máximo debe ser de 200 carácteres";      
        }
    })
    $("input#referencia").blur(function() {
        var referenciaa= $(this).val();
        if (referenciaa!=="") {
        	$('#errorreferencia').hide()       
        }
    })
    

}

 function alpha(e) { 
 	 key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ,.-1234567890";//Se define todo el abecedario que se quiere que se muestre.
    especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial){
        return false;
      }
 }

 function validacionesedicion(){

	$("input#fechapagoedit").blur(function() {
        var fechapagoa= $(this).val();
        if (fechapagoa!=="") {
        	$('#errorfechapagoe').hide()        
        }
    })
    $("select#formapagosedit").blur(function() {
        var formapagoa= $(this).val();
        if (formapagoa!=="" || formapagoa=="0") {
        	$('#errorformapagoe').hide()       
        }
    })
	$("select#tipomonedaedit").blur(function() {
        var tipomonedaa= $(this).val();
        if (tipomonedaa!=="") {
        	$('#errormonedae').hide()       
        }
        
    })
	$("input#nombredepositanteedit").blur(function() {
        var nombredepositantea= $(this).val();
        if (nombredepositantea!=="") {
        	$('#errordepositantee').hide()       
        }
        if (nombredepositantea.length<2) {
        	 document.getElementById("errordepositantemaxe").innerHTML="El nombre debe tener al menos 2 carácteres";      
        } else if (nombredepositantea.length>200) {
        	 document.getElementById("errordepositantemaxe").innerHTML="El largo máximo debe ser de 200 carácteres";      
        }
    })
    $("input#referenciaedit").blur(function() {
        var referenciaa= $(this).val();
        if (referenciaa!=="") {
        	$('#errorreferenciae').hide()       
        }
    })

  

}

function validacioneslaurbina(){

	$("input#fechagest").blur(function() {
        var fechagest= $(this).val();
        if (fechagest!=="") {
        	$('#errorfechagest').hide()        
        }
    })
    $("select#ntecnicoselectmifi").blur(function() {
        var tecnico= $(this).val();
        //console.log(tecnico);
        if (tecnico!=="") {
        	$('#errortecnico').hide()       
        }
    })
	$("select#selectestatus").blur(function() {
        var estatus= $(this).val();
        //console.log(estatus);
        if (estatus!=="") {
        	$('#errorestatus').hide()       
        }
        
    })
    $("input#fechainst").blur(function() {
        var fechainst= $(this).val();
        if (fechainst!=="") {
        	$('#errorfechainst').hide()       
        }
    })

    $("input#fecharecepalma").blur(function() {
        var fecharecepalma= $(this).val();
        if (fecharecepalma!=="") {
        	$('#errorfecharecep').hide()        
        }
    })

    $("input#fechaenv").blur(function() {
        var fechaenv= $(this).val();
        if (fechaenv!=="") {
        	$('#errorfechaenv').hide()        
        }
    })
    $("input#fechalog").blur(function() {
        var fechalog= $(this).val();
        if (fechalog!=="") {
        	$('#errorfechalog').hide()        
        }
    })
    

}


function validacionfechafact(){

	$("input#ffactura").blur(function() {
        var ffactura= $(this).val();
        if (ffactura!=="") {
        	$('#errorfechafact').hide()        
        }
    })
}


 function modificarbancoactual(){
	

	var usuario=document.getElementById('usuario').value;	
	var banco=document.getElementById('bancoasignado').value;
	var idregistro=document.getElementById('id_registro').value;
	if (banco=="") {
		alertify.error('Error el campo no puede estar vacio. Ingrese el banco a asignar');
		document.getElementById('bancoasignado').style.borderColor="red";
		document.getElementById('bancoasignado').focus('');
		return false;
	}

		var variable=idregistro+'/'+banco+'/'+usuario;
//	alert(variable);
	$.post('view/comercializacion/gestion/modificarbanco.php',{postvariable:variable},
		function(data)
		{
			alertify.success(data);
			setTimeout(function(){ window.location.reload(0); }, 4000); 


		});


	return false;

}


function guardarPercance(){
	
	var cantidadterminales= document.getElementById('cantidadterminales').value;
	var contador=0;
	var idregistro=document.getElementById('idregistro').value;
	var usuario =document.getElementById('usuario').value;
	var checklist;
	var cantidad=0;
	var cont=0;
	//alert(cantidadterminales);
	for (var j = 0; j< cantidadterminales; j++){
		cont++;
		checklist=document.getElementById('checklist'+[cont]);
		if(checklist.checked) 
		{
			var codmotivo =document.getElementById("codmotivo"+[cont]).value;
			//alert(codmotivo);
		}
		if (codmotivo=="" || codmotivo==0){
			cantidad++;
			
		}
	}
//alert(cantidad);
if (cantidad=='0' || cantidad==='0'){
	for (var i = 0; i < cantidadterminales; i++) {
	contador++;
	checklista=document.getElementById('checklist'+[contador]);
	//alert (cantidad);
		if(checklista.checked) 
		{

			var serialperc =document.getElementById("serialperc"+[contador]).value;
			var simcardperc =document.getElementById("simcardperc"+[contador]).value;
			var serialmifiperc =document.getElementById("serialmifiperc"+[contador]).value;
			var codmotivo =document.getElementById("codmotivo"+[contador]).value;
			var observacionpercanc =document.getElementById("observacionpercanc"+[contador]).value;
			var consecutivo =document.getElementById("consecutivo"+[contador]).value;
			var id_estatusperc =document.getElementById("idestatusperc"+[contador]).value;

			var variable=idregistro+'/'+serialperc+'/'+simcardperc+'/'+serialmifiperc+'/'+codmotivo+'/'+usuario+'/'+consecutivo+'/'+observacionpercanc+'/'+id_estatusperc;
			//alert (variable);
			$.post('view/Administracion/facturacionpos/guardarpercance.php',{postvariable:variable},

			function(data)
			{
				alertify.success("El estatus fue modificado");
				setTimeout(function(){ window.location.reload(0); }, 3000);	
			});

					

			}

		}

	}else{
			alertify.error('Error, el campo motivo es requerido');
		    document.getElementById('codmotivo').style.borderColor="red";
		    return false;
	}
		
}
 function validacioncaptacion(){

	$("input#nombre").blur(function() {
        var cnombre= $(this).val();
        if (cnombre!=="") {
        	$('#errornombre').hide()        
        }
    })


    $("input#apellido").blur(function() {
        var capellido= $(this).val();
        if (capellido!=="") {
        	$('#errorapellido').hide()        
        }
    })

    $("input#cantidadposs").blur(function() {
        var ccantidadposs= $(this).val();
        if (ccantidadposs!=="") {
        	$('#errorcantidad').hide()        
        }
    })

}

function guardarreferido(){
	idcliente=document.getElementById('idcliente').value;
	usuario=document.getElementById('usuario').value;
	nombreref=document.getElementById('nombreref').value;
	apellidoref=document.getElementById('apellidoref').value;
	telefonoref=document.getElementById('telefonoref').value;
	banco=document.getElementById('banco').value;
    if (nombreref=='') {
     alertify.error('Debe colocar el nombre del referido');
    }else if (apellidoref=='') {
     alertify.error('Debe colocar el apellido del referido');
    }else if (telefonoref=='') {
     alertify.error('Debe colocar el numero de telefono del referido');
    }else{
    	var variable=idcliente+'/'+usuario+'/'+nombreref+'/'+apellidoref+'/'+telefonoref+'/'+banco;
	$.post('view/encuesta/guardarpersonaref.php',{postvariable:variable}, 
	    function(data){
	    	if (data==1) {
	    		alertify.success('Datos de la gestion guardados con exito');
	    		setTimeout(function(){ window.location.reload(0); }, 300);
	    	}else{
	    		alertify.error('El referido no pudo ser registrado o ya esta registrado');	
	    	}
	    }
	);
    }
}

function buscarreportevendedoresmen(){
var variable=document.getElementById('fechainicial').value+'/'+document.getElementById('fechafinal').value;


		if((document.getElementById('fechainicial').value == "") && (document.getElementById('fechafinal').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	} else {
		window.location.href='?cargar=vendedoresmensual&var='+variable

	}
		
}

function buscaractivacionequiposs(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=buscarbancoactivacion&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}

     function validarpercances(contador){
   var cantidadterminales= document.getElementById('cantidadterminales').value;

	checklist=document.getElementById('checklist'+[contador]);

		 if( checklist.checked ) 
		 {
			
			document.getElementById('codmotivo'+[contador]).disabled=false;
			document.getElementById('observacionpercanc'+[contador]).disabled=false;
			document.getElementById('buttoneditar'+[contador]).disabled=false;
		 }
		 else
		 {
			
			document.getElementById('codmotivo'+[contador]).disabled=true;
			document.getElementById('observacionpercanc'+[contador]).disabled=true;
			document.getElementById('buttoneditar'+[contador]).disabled=true;
		 }

 		   var cont = 0; 
		   var cantidadterminales= document.getElementById('cantidadterminales').value;
		   //alert(cantidadterminales);
		  for (var x=1; x < cantidadterminales +1; x++)
		  {
		  	 checklist1=document.getElementById('checklist'+[x]);

		 if(checklist1.checked) {
		 cont = cont + 1;
		    if(cont>1){

		     	for(var i=1; i < cantidadterminales +1; i++){
			    	document.getElementById('buttoneditar'+[i]).disabled=true;
					document.getElementById('editartodos').style.display="block";
				}
	  		}
	  		else
	  		{
	  			document.getElementById('buttoneditar'+[x]).disabled=false;	
	  			document.getElementById('editartodos').style.display="none";
	  		}
		  }
		
		 }
}

function editar_percances(contador){

var idregistro=document.getElementById('idregistro').value;
var usuario =document.getElementById('usuario').value;
var serialperc =document.getElementById('serialperc'+contador).value;
var simcardperc =document.getElementById('simcardperc'+contador).value;
var serialmifiperc =document.getElementById('serialmifiperc'+contador).value;
var codmotivo =document.getElementById('codmotivo'+contador).value;
var observacionpercanc =document.getElementById('observacionpercanc'+contador).value;
var consecutivo =document.getElementById('consecutivo'+contador).value;
var id_estatusperc =document.getElementById('idestatusperc'+contador).value;
//var cantidadterminales=document.getElementById('canterminales');
//alert(cantidadterminales);
if (codmotivo=="" || codmotivo==0){
	alertify.error('Error, el campo motivo es requerido');
    document.getElementById('codmotivo').style.borderColor="red";
    return false;
	}

	var variable=idregistro+'/'+serialperc+'/'+simcardperc+'/'+serialmifiperc+'/'+codmotivo+'/'+usuario+'/'+consecutivo+'/'+observacionpercanc+'/'+id_estatusperc;
//alert(variable);
$.post('view/Administracion/facturacionpos/guardarpercance.php',{postvariable:variable},

function(data)
{
alertify.success("El estatus del serial "+serialperc+" fue modificado");
	setTimeout(function(){ window.location.reload(0); }, 300000);		
 });
}

function buscaractivacionequiposs(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=buscarbancoactivacion&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
}

}

 function generarclienteactivacionequipos(){

	var el_valor =document.getElementById('valor').value;
	
 	// Lista de bancos con registros en clientes potenciales comercializacion
	$.post( 'view/ClientePotencial/recepcion/clientesreporte.php', { valor: el_valor} ).done( function(respuesta)
	{
		$( '#clientes' ).html( respuesta );
	});

}

function activarequipo(contador)
{
var equis="";
var cantidadterminales= document.getElementById('cantterminales').value;
var banco =document.getElementById('banco').value;

var estatuss=document.getElementById("estatus"+[contador]).value;
//alert (estatuss);
  if (document.getElementById("estatus"+[contador]).checked == true) {
    
    document.getElementById("estatus"+[contador]).disabled=false;
  }
  if (document.getElementById("estatus"+[contador]).value == 28 || document.getElementById("estatus"+[contador]).value == 52) {
    
    var equis="Afiliar";

  }else if (document.getElementById("estatus"+[contador]).value == 30){
	var equis="Conectar";

  }else if (document.getElementById("estatus"+[contador]).value == 31){
	var equis="Activar";

  }

  if (  document.getElementById("estatus"+[contador]).checked = true) {

    alertify.set({ labels: {

      ok: equis,
      cancel: "Cancelar"
    }
  });

    alertify.set({ buttonReverse: true });
    	var id =document.getElementById("idinventariopos"+[contador]).value;
    	var estatus=document.getElementById('estatus'+[contador]).value;
    	var serialpos =document.getElementById("serialpos"+[contador]).value;
    	var serialsimcard =document.getElementById("serialsimcard"+[contador]).value;
    	var serialmifi =document.getElementById("serialmifi"+[contador]).value;
    	var usuario= document.getElementById('usuario').value;
		var consecutivo= document.getElementById('consecutivo').value;
		var idgestion= document.getElementById("idgestion"+[contador]).value;
		var banco =document.getElementById('banco').value;
		//alert (banco);
    	
    alertify.confirm("&#191;Desea "+ equis +" el siguiente equipo?", function (e) {
      if (e) {
      	var variable=id+'/'+estatus+'/'+serialpos+'/'+serialsimcard+'/'+serialmifi+'/'+usuario+'/'+consecutivo+'/'+idgestion;
        $.post('view/Activacion/guardaractivacion.php',{postvariable:variable},
          function(data)
          {
            alertify.success(data);

           setTimeout(function(){ window.location.reload(0); }, 3000);
          });
      } else {
      	if (document.getElementById("estatus"+[contador]).value == 28 || document.getElementById("estatus"+[contador]).value == 52) {
        	alertify.error("No se ha afiliado el equipo.");
    	}else if (document.getElementById("estatus"+[contador]).value == 30) {
      	
        	alertify.error("No se ha Conectado el equipo.");
    	}else if (document.getElementById("estatus"+[contador]).value == 31) {
      	
        	alertify.error("No se ha Activado el equipo.");
    	}
        
       setTimeout(function(){  window.location.href='?cargar=buscarbancoactivacion&var='+banco}, 600);

        return false;
      }
    })
    return false;
  }

}

function buscarclienteSerial(){
 var serial = $('#nroserial').val();
    
	if(document.getElementById('nroserial').value == "")
	{   
		alertify.alert("El campo no puede estar vacio.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('nroserial').focus();
		document.getElementById('nroserial').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('nroserial').value;
		window.location.href='?cargar=gestionaractivacionserial&var='+variable
	}
}


      function validararchivo1(){
	
	var	archivo1=document.getElementById('archivo').value;
	var	tamano1=document.getElementById('archivo');

	var	formapago=document.getElementById('codformap');
	var	nombres=document.getElementById('nombre');
	var	codestatus=document.getElementById('codigoestatus');

	if((document.getElementById('codformap').value == ""))
	{
		alertify.error("Debe seleccionar una forma de pago");
		document.getElementById('codformap').focus();
		return false;
	}


	if((document.getElementById('codigoestatus').value == ""))
	{
		alertify.error("El estatus del equipo es requerido");
		document.getElementById('codigoestatus').focus();
		return false;
	}
	
	var extensionesValidas = " .jpeg, .jpg, .pdf";
	var pesoPermitido = 500000;

	if (tamano1.files && tamano1.files[0]) {

	  var pesoFichero1 = tamano1.files[0].size/500000;

	        if(pesoFichero1 > pesoPermitido) {
		alertify.alert('El peso maximo permitido del fichero es: ' + pesoPermitido + ' KBs Su fichero 1 tiene: '+ pesoFichero1 +' KBs');
		document.getElementById('archivo').focus();
		return false;
	   }
	  }
	  //valida fotos
	var extension1 = archivo1.substring(archivo1.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida1 = extensionesValidas.indexOf(extension1);
	
	if(extensionValida1 < 0) {

		alertify.alert('La extensión no es válida, El documento tiene de extensión: .'+ extension1);
		document.getElementById('archivo').focus();
		return false;
	}

	else{
		document.subir.submit();
	}
}

function buscarcontrolaccesos(){
	var mes=document.getElementById('mes').value;
	var empleado=document.getElementById('empleado').value;
	var variable= mes+'/'+empleado;
	if((document.getElementById('mes').value == "") || (document.getElementById('empleado').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	
		window.location.href='?cargar=buscarcontrolacceso&var='+variable
}

function generarempleado(){
	
	$.post( 'view/ControlAcceso/selectempleados.php').done( function(respuesta)
	{  
		$( '#empleado' ).html( respuesta );
	});
}

function eliminardataaccesos(login){
var usuario=document.getElementById('usuario').value;
var dataeliminarS=document.getElementById('dataeliminar').value;
	alertify.set({ 
		labels: {
		ok: "Ok",
		cancel: "Cancelar"
		}
	});

	alertify.set({ buttonReverse: true });
	
	if(document.getElementById('dataeliminar').value == "")
	{
		alertify.alert("DEBE SELECCIONAR EL ARCHIVO!");
		document.getElementById('dataeliminar').focus();
		return false;
	}

	//alert(dataeliminarS);
	alertify.confirm("&#191;Desea Eliminar al archivo?", function (e) {
		if (e) {
			var variable=document.getElementById('dataeliminar').value+'/'+usuario;
			$.post('view/ControlAcceso/eliminararchivos.php',{postvariable:variable},
				function(data)
				{
					alertify.success("Archivo eliminado con &Eacute;xito");
					setTimeout(function(){ window.location.reload(1); }, 2000);
				});
		} else {
			alertify.error("No se ha eliminado el archivo");
			return false;
		}
	})
		return false;
}

function ocultareliminardata(){
          $('#eliminardata').css('display', 'block') ;
 } 


 function validacionfechafact(){

	$("input#ffactura").blur(function() {
        var ffactura= $(this).val();
        if (ffactura!=="") {
        	$('#errorfechafact').hide()        
        }
    })
}
  
  
function buscarvisitatecnica(){
var variable=document.getElementById('clientes').value+'/'+document.getElementById('fvisit').value+'/'+document.getElementById('nrorif').value;

if((document.getElementById('fvisit').value == ""))
{
		alertify.alert("DEBE SELECCIONAR EL AÑO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}

	if((document.getElementById('fvisit').value != "") && (document.getElementById('clientes').value == "") && (document.getElementById('nrorif').value == ""))
{
		alertify.alert("DEBE SELECCIONAR EL BANCO O AFILIACIÓN/SERIAL!.");
		document.getElementById('clientes').focus();
		return false;
	}


	if((document.getElementById('fvisit').value != "") && (document.getElementById('clientes').value != "") && (document.getElementById('nrorif').value != ""))
{
		alertify.alert("LA BÚSQUEDA DEBE SER AÑO Y BANCO Ó AÑO Y SERIAL");
		document.getElementById('clientes').focus();
		return false;
	}	

		if((document.getElementById('fvisit').value == "") && (document.getElementById('clientes').value == "") && (document.getElementById('nrorif').value == ""))
{
		alertify.alert("DEBE SELECCIONAR EL METODO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	
	if((document.getElementById('clientes').value != "") || (document.getElementById('fvisit').value != "") || (document.getElementById('nrorif').value != ""))
	{
		window.location.href='?cargar=buscarregistrovisita&var='+variable
	}
}

function buscarreportevisitatecnica(){
var variable=document.getElementById('clientes').value;

	if((document.getElementById('clientes').value == "") )
	{
		alertify.alert("DEBE SELECCIONAR EL CRITERIO DE BUSQUEDA!.");
		document.getElementById('clientes').focus();
		return false;
	}
	
	if((document.getElementById('clientes').value != ""))
	{
		window.location.href='?cargar=buscarreportevisita&var='+variable
	}
}


function validacionvisita(){

	$("input#fechagesti").blur(function() {
        var fechagesti= $(this).val();
        if (fechagesti!=="") {
        	$('#errorfechagesti').hide()        
        }
    })
    $("select#ntecnicoselect").blur(function() {
        var ntecnicoselect= $(this).val();
        if (ntecnicoselect!=="") {
        	$('#errortecnico').hide()       
        }
    })
	$("select#selectestatus").blur(function() {
        var selectestatus= $(this).val();
        if (selectestatus!=="") {
        	$('#errorestatus').hide()       
        }
        
    })
    $("input#fechavisita").blur(function() {
        var fechavisita= $(this).val();
        if (fechavisita!=="") {
        	$('#errorfechavisita').hide()       
        }
    })
}

	function validarfechasvisita(){

 var fechainicial=document.getElementById('fechainicialgest').value;
 var fechafinal=document.getElementById('fechafinalgest').value;
 
 var fechad = new Date(fechainicial);
 var aniodesde = fechad.getUTCFullYear();

 var fechah = new Date(fechafinal);
 var aniohasta = fechah.getUTCFullYear();

//alert (Date.parse(fechadesde));
//alert (Date.parse(fechahasta));

if ((isNaN(aniodesde)) || (isNaN(aniohasta)))
{
	document.getElementById('Buscar').disabled=true;
}
else if((Date.parse(fechainicial)) > (Date.parse(fechafinal))){
	//alertify.error("La fecha inicial no puede ser mayor que la fecha final");
	document.getElementById('Buscar').disabled=true;
	}
	else
	{
	document.getElementById('Buscar').disabled=false;	
	}
	
	
}

function buscarvisitasxfechas(){
var variable=document.getElementById('fechainicialgest').value+'/'+document.getElementById('fechafinalgest').value;


		if((document.getElementById('fechainicialgest').value == "") && (document.getElementById('fechafinalgest').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	} else {
		window.location.href='?cargar=buscarvisitaxfecha&var='+variable

	}
		
}

function buscargestionxfechas(){
var variable=document.getElementById('fechainicialgest').value+'/'+document.getElementById('fechafinalgest').value;


		if((document.getElementById('fechainicialgest').value == "") && (document.getElementById('fechafinalgest').value == ""))
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	} else {
		window.location.href='?cargar=buscargestionxfechas&var='+variable

	}
		
}


function buscargestionxfechas(){
var fechav=document.getElementById('fvisit').value;


		if(document.getElementById('fvisit').value == "")
	
	{
		alertify.alert("DEBE SELECCIONAR UNA OPCION A CONSULTAR!.");
		document.getElementById('fvisit').focus();
		return false;
		//window.location.href='?cargar=reportexejecutivo&var='+variable
	} else {
		window.location.href='?cargar=buscargestionxfechas&var='+fechav

	}
		
}



function buscarreportegestiones(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=generarreportegestiones&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}

function buscarclienteRifReporteGestiones(){
	if(document.getElementById('nrorif').value == "")
	{   
		 // .ajs-message.ajs-custom { color: #31708f;  background-color: #d9edf7;  border-color: #31708f; }
		alertify.set('notifier','position', 'bottom-right');
		alertify.alert("Debe ingresar la información a buscar.");
		//alertify.message("El campo no puede estar vacio.");
		txt = document.getElementById('nrorif').focus();
		document.getElementById('nrorif').style.borderColor="red";

		return false;
	}
	else{
		var variable=document.getElementById('nrorif').value;
		window.location.href='?cargar=generarreportegestionesrif&var='+variable
	}
}

function DeclinacionSinSeriales(){
	 
	if( checklist.checked ) {
		alertify.set({ labels: 
			{
				ok: "Declinar",
				cancel: "Cancelar"
			}
		});

		alertify.set({ buttonReverse: true });
		alertify.confirm("&#191;Desea Desclinar la compra?", function (e) {
			if (e) {
				var idregistro=document.getElementById('idregistro').value;
				var usuario=document.getElementById('usuario').value;
				var variable= idregistro+'/'+usuario;
				$.post('view/Administracion/facturacionpos/declinacion_sin_seriales.php',{postvariable:variable},
					function(data)
					{
						alertify.success(data);
						setTimeout(function(){ window.location.reload(0); }, 2000); 
					});
			} else {
				alertify.error("No se ha Declinado la Compra");
				setTimeout(function(){ window.location.reload(0); }, 2000); 
				return false;
			}
		})
		return false;
	}
}

function validarfechasprofit(){

 var fechainicial=document.getElementById('fechainicial').value;
 var fechafinal=document.getElementById('fechafinal').value;
 
 var fechad = new Date(fechainicial);
 var aniodesde = fechad.getUTCFullYear();

 var fechah = new Date(fechafinal);
 var aniohasta = fechah.getUTCFullYear();

//alert (Date.parse(fechadesde));
//alert (Date.parse(fechahasta));

if ((isNaN(aniodesde)) || (isNaN(aniohasta)))
{
	document.getElementById('Buscar').disabled=true;
}
else if((Date.parse(fechainicial)) > (Date.parse(fechafinal))){
	//alertify.error("La fecha inicial no puede ser mayor que la fecha final");
	document.getElementById('Buscar').disabled=true;
	}
	else
	{
	document.getElementById('Buscar').disabled=false;	
	}
	//Para habilitar el tipo de reporte
	if ((fechainicial!='') && (fechafinal!='')) {
	$('#tiporeporte').removeAttr('disabled');
	}
	else{
		 $('#tiporeporte').val("1");
       document.getElementById('tiporeporte').disabled=true;
       }
}





// function buscarregistrobanesco(){

// var variable=document.getElementById('estatus').value;
// 		//alert(reporteeje);
// 	if(document.getElementById('estatus').value != ""){

// 		window.location.href='?cargar=busquedaporestatus&var='+variable
// 	}
// 	else
// 	{
// 		alertify.alert("DEBE SELECCIONAR UN ESTATUS A CONSULTAR!.");
// 		document.getElementById('estatus').focus();
// 		return false;
// 	}	
// }

function confirmregistro(){
	$('table#asignacioncaptacion tbody').on( 'onclick', function () {
	var tabla=$('table#asignacioncaptacion').DataTable();
	var DT =  tabla.row($(this).parents("tr")).data();
	$("input#idregistros").val(DT.idregistro);

	var usuario=document.getElementById('usuario').value;
  var id_registro=document.getElementById('idregistro').value;
  var table = $('#asignacioncaptacion').DataTable();
  var estatus="1";
  var variable=estatus+'/'+id_registro+'/'+usuario;
  alert(variable);

  $.post('view/CaptacionCliente/aprobar.php',{postvariable:variable},
		function(data)
		{

if (data!=0) { alertify.error("Error, no se puede aprobar");
	
 }else{
 	  alertify.success('Registros Aprobados');
 	  table.rows().invalidate().draw();
 	}

});

	return false;
})
}



function modalidades(){
	var tipo=document.getElementById('modoventa').value;
	//alert(tipo);
	if (tipo==4){ 
		 if ($('#montodescuento').css('display') === 'none') {
          $('#montodescuento').css('display', 'block') ;
          $('#montoanticipo').css('display', 'none')  ;
          $('#montoinicial').css('display', 'none')  ;
          $('#equiprecompradiv').css('display','none') ;
          $('#serialesresguardos').css('display','none') ;
      }
      else {
          $('#montodescuento').css('display', 'none')  ;
      }
	}else if(tipo==6){
		if ($('#montoanticipo').css('display') === 'none') {
       // $('#equiprecompradiv').css('display','block') ;
        $('#montoanticipo').css('display','block') ;
        $('#montodescuento').css('display', 'none')  ;
        $('#montoinicial').css('display', 'none')  ;

    }
    else {
    		$('#equiprecompradiv').css('display','none') ;
        $('#montoanticipo').css('display', 'none')  ;
        $('#serialesresguardos').css('display','none') ;
      }
    }else if(tipo==2 || tipo==3){
		if ($('#montoinicial').css('display') === 'none') {
        $('#montoinicial').css('display','block') ;
        $('#montodescuento').css('display', 'none')  ;
        $('#montoanticipo').css('display', 'none')  ;
        $('#equiprecompradiv').css('display','none') ;
        $('#serialesresguardos').css('display','none') ;
    }else {
        $('#montoinicial').css('display', 'none')  ;
    }
	} else if(tipo!=4 || tipo!=6 || tipo!=2 || tipo!=3){
			$('#montodescuento').css('display', 'none')  ;
			$('#montoanticipo').css('display', 'none')  ;
			$('#montoinicial').css('display', 'none')  ;
			$('#equiprecompradiv').css('display','none') ;
			$('#serialesresguardos').css('display','none') ;
	}


}

function serialesrecompracomer(){
	if(document.getElementById('equiprecompra').checked){
					$('#serialesresguardos').css('display', 'block')  ;
				}else {
    		$('#serialesresguardos').css('display','none') ;
      }
}

// recuerdame coco
function buscarreporteriesgomedido(){

var regiones=document.getElementById('regiones').value;
var clientes=document.getElementById('clientes').value;

var variable=document.getElementById('regiones').value+'/'+document.getElementById('clientes').value;
		//alert(reporteeje);
	if((document.getElementById('clientes').value != "") && (document.getElementById('regiones').value != "")){

		window.location.href='?cargar=riesgo_medido&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UNA REGION Y UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}

function buscarreportecargaparametros(){

var variable=document.getElementById('clientes').value;
		//alert(reporteeje);
	if(document.getElementById('clientes').value != ""){

		window.location.href='?cargar=cargaparametros&var='+variable
	}
	else
	{
		alertify.alert("DEBE SELECCIONAR UN BANCO A CONSULTAR!.");
		document.getElementById('clientes').focus();
		return false;
	}	
}

function selectpreciopos(){
	$.post( 'view/ReportesRiesgoMedido/gestion/marcapos.php').done( function(respuesta)
    {
    	$( '#idmodeloposrm').html(respuesta);
    });

	$.post( 'view/ReportesRiesgoMedido/gestion/modeloposnew.php').done( function(respuesta)
    {
    	$( '#idtiposposnew').html(respuesta);
    });
}

function guardarpreciopos(){
	idmodelopos=document.getElementById('idmodeloposrm').value;
	usuario=document.getElementById('usuario').value;
	idtipopos=document.getElementById('idtipopos').value;
	montousd=document.getElementById('montousd').value;
	if (document.getElementById('checkbanescoprecio').checked){
			banescoprecio=document.getElementById('checkbanescoprecio').value;
			//alert (banescoprecio);
		}else
			banescoprecio='0';
    if (idmodelopos=='') {
     alertify.error('Debe colocar el modelo del POS');
    }else if (idtipopos=='') {
     alertify.error('Debe colocar el tipo del POS');
    }else if (montousd=='') {
     alertify.error('Debe colocar el precio');
    }else{
    	var variable=idmodelopos+'/'+usuario+'/'+idtipopos+'/'+montousd+'/'+banescoprecio;
    	//alert(variable);
	$.post('view/ReportesRiesgoMedido/gestion/guardarpreciospos.php',{postvariable:variable}, 
	    function(data){
	    	if (data==1) {
	    		alertify.success('Datos de la gestion guardados con exito');
	    		setTimeout(function(){ window.location.reload(0); }, 3000);
	    	}else{
	    		alertify.error('El precio no puedo ser registrado o ya se encuentra registrado');	
	    	}
	    }
	);
    }
}
// 
function regnuevopos(){
	idmodelopos=document.getElementById('idmodeloposnew').value;
	//usuario=document.getElementById('usuario').value;
	idtipopos=document.getElementById('idtiposposnew').value;

//esto deberia matar los espacios entre los strings
	idmodelopos=idmodelopos.trim();

    if (idmodelopos=='') {
     alertify.error('Debe colocar el modelo del POS');
    }else if (idtipopos=='') {
     alertify.error('Debe colocar el tipo del POS');
	//  agregar validacion adicional de ambos juntos = anteriores
    }else{
    	var variable=idmodelopos+'/'+idtipopos;
    	//alert(variable);
	$.post('view/ReportesRiesgoMedido/gestion/guardarnuevospos.php',{postvariable:variable}, 
	    function(data){
			// console.log(data);
			// console.log(typeof data);
	    	if (data==1) {
	    		alertify.success('Datos de la gestion guardados con exito');
	    		setTimeout(function(){ window.location.reload(0); }, 3000);
	    	}else{
	    		alertify.error('El pos no pudo ser registrado o ya se encuentra registrado');	
	    	}
	    }
	);
    }
}


function gestionarrecomp(contador){
   var cantidadterminales= document.getElementById('cantidadterminales').value;

	checklist=document.getElementById('checklistrecompra'+[contador]);

		 if( checklist.checked ) 
		 {
			
			document.getElementById('buttonrecompra'+[contador]).disabled=false;
		 }
		 else
		 {
			
			document.getElementById('buttonrecompra'+[contador]).disabled=true;
		 }

 		   var cont = 0; 
		   var cantidadterminales= document.getElementById('cantidadterminales').value;
		   //alert(cantidadterminales);
		  
}

function aprobarrecompra(contador){

var idregistro=document.getElementById('idregistro').value;
var usuario =document.getElementById('usuario').value;
var serialrecompra =document.getElementById('serialrecompra'+contador).value;
var montorecompra =document.getElementById('montorecompra'+contador).value;
var consecutivo =document.getElementById('consecutivo'+contador).value;
//var cantidadterminales=document.getElementById('canterminales');
//alert(cantidadterminales);
if (serialrecompra==""){
	alertify.error('Error, el campo serial es requerido');
    return false;
	}
if (montorecompra=="0" || montorecompra==""){
	alertify.error('Error, el campo monto es requerido');
    document.getElementById('monto').style.borderColor="red";
    return false;
	}

	var variable=idregistro+'/'+usuario+'/'+serialrecompra+'/'+montorecompra+'/'+consecutivo;
//alert(variable);
$.post('view/Administracion/facturacionpos/guardarrecompra.php',{postvariable:variable},

function(data)
{
alertify.success("El estatus del serial "+serialrecompra+" fue modificado");
	setTimeout(function(){ window.location.reload(0); }, 300000);		
 });
}

function montorecompra(contador){
	var tipo=document.getElementById('codigomotivo'+contador).value;
	//alert(tipo);
	if (tipo==46){
		 document.getElementById('monto'+contador).disabled=false;
	}
}

// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
function tasacambio(){
	var fechapago=document.getElementById('fechapago').value;
	var montousd=document.getElementById('montousd').value;
	let lista = [];

	$.ajax({
	    url: "view/comercializacion/gestion/tasacambioxfecha.php",
	    type: "POST",
	    datatype:"json",    
	    data:  {"postvariable":fechapago},
	    success: function(data) {
	    	var obj = JSON.parse(data);
				var vtasa_dolar = obj['tasa_dolarr'];
				$('#factorconversion').val(vtasa_dolar);
				totalmontobs = (vtasa_dolar*montousd);
				$('#monto').val(totalmontobs);
				//alert(vtasa_dolar);	
	    }
    });	
}
function datosadicional(){

	//coddocumento=document.getElementById('coddocumento').value;
	tlf1=document.getElementById('tlf1').value;
	tlf2=document.getElementById('tlf2').value;
	email=document.getElementById('email').value;
    nombre2=document.getElementById('nombre2').value;
    apellido2=document.getElementById('apellido2').value;
    usuario=document.getElementById('usuario').value;
    banco=document.getElementById('cbanco').value;
	cliente=document.getElementById('idcliente').value;
				 
	var variable=tlf1+'/'+tlf2+'/'+email+'/'+nombre2+'/'+apellido2+'/'+usuario+'/'+banco+'/'+cliente;

	alert(variable);
	$.post('view/VisitaTecnica/guardardatosadicional.php',{postvariable:variable}).done(function(data)
		{
     
			
		});
    //}
	
	return false;

}


function dir_adicional(){

	coddocumento=document.getElementById('coddocumento').value;
	codetipodir5=document.getElementById('codetipodir5').value;
	d_calle_av1=document.getElementById('edit_calleav').value;
	d_localidad1=document.getElementById('edit_localidad').value;
	d_sector1=document.getElementById('edit_sector').value;
	d_urbanizacion1=document.getElementById('edit_urbanizacion').value;
	d_nlocal1=document.getElementById('edit_nlocal').value;
	d_estado1=document.getElementById('edit_estado').value;
	d_codepostal1=document.getElementById('edit_codigopostal').value;
	d_ptoref1=document.getElementById('edit_ptoref').value;
	prefijo=document.getElementById('cbanco').value;
	usuario=document.getElementById('usuario').value;
	id_registro=document.getElementById('idcliente').value;

if ((d_calle_av1||d_localidad1||d_urbanizacion1||d_sector1||d_nlocal1||d_codepostal1||d_ptoref1)=="") {
	 alertify.error("Error, todos los campos estan vacios.");
      return false;
}
else if(d_estado1==0){
	alertify.error('Error, Debe seleccionar un Estado');
 	$('#edit_estado').focus().select();
	$('#edit_estado').select().css("border-color","red" );
	return false;
     }
else if(d_localidad1==""){
	alertify.error('Error, Debe seleccionar un Municipio');
 	$('#edit_localidad').focus().select();
	$('#edit_localidad').select().css("border-color","red");
	return false;
     }
else if(d_sector1==""){
	alertify.error('Error, Debe seleccionar una Parroquia');
 	$('#edit_sector').focus().select();
	$('#edit_sector').select().css("border-color","red");
	return false;
     }

else{
	var variable=coddocumento+'/-/-/-'+codetipodir5+'/-/-/-'+d_calle_av1+'/-/-/-'+d_localidad1+'/-/-/-'+d_sector1+'/-/-/-'+d_urbanizacion1+'/-/-/-'+d_nlocal1+'/-/-/-'+ d_estado1+'/-/-/-'+d_codepostal1+'/-/-/-'+d_ptoref1+'/-/-/-'+prefijo+'/-/-/-'+usuario+'/-/-/-'+id_registro;
	$.post('view/VisitaTecnica/guardardireccionadicional.php',{postvariable:variable},
		function(data)
		{
			 modified=false;
			 alertify.success(data);
		     setTimeout(function(){ window.location.reload(0); }, 4000); 

		});
	}
	return false;
}

function tipopos(){
	var banco=document.getElementById('bancos').value;

			$.post( 'view/CaptacionCliente/tipopos.php', { valor: banco} ).done( function(respuesta)
		{
			$( '#tipopos' ).html( respuesta );		
		})
}




function guardardatoservicio(){
	cliente=document.getElementById('cliente').value;
	documento=document.getElementById('documento').value;
	razonsocial=document.getElementById('razonsocial').value;
	usuario=document.getElementById('usuario').value;
	codformap=document.getElementById('codformap').value;
	ncuenta=document.getElementById('ncuenta').value;
	codigoestatus=document.getElementById('codigoestatus').value;
	actividad=document.getElementById('actividad').value;
	cbanco=document.getElementById('cbanco').value;

	if ((codformap=='') || (codigoestatus=='') || (actividad=='')){
		alertify.error('Error, los campos no pueden estar vacios');
		return false;
	}

    var variable=cliente+'/'+documento+'/'+razonsocial+'/'+codformap+'/'+ncuenta+'/'+codigoestatus+'/'+actividad+'/'+cbanco+'/'+usuario;
	//alert(variable);
	$.post('view/ServicioMantenimiento/guardar_domiciliacion.php',{postvariable:variable}, 
	    function(data){
	    	//console.log(data);
	    	if (data==1) {
	    		alertify.success('Datos de la gestion guardados con exito');
	    		setTimeout(function(){ window.location.reload(0); }, 3000);
	    	}else{
	    		alertify.error('El registro no pudo ser guardado');	
	    	}
	    }
	);
    
}

function editardirfiscal(){
  if ($('#confirfiscalinst').css('display') == 'block') {
      $('#guardarfiscalinst').css('display', 'block') ;
      $('#confirfiscalinst').css('display', 'none')  ;
  }
  else {
      $('#confirfiscalinst').css('display', 'block')  ;
  }
}

function dirfiscalinst(){

    coddocumento=document.getElementById('coddocumento').value;
 	codetipodir1=document.getElementById('codetipodir1').value;
    d_calle_av1=document.getElementById('d_calle_av1i').value;
	d_localidad1=document.getElementById('d_municipio1i').value;
	d_sector1=document.getElementById('d_parroquia1i').value;
	d_urbanizacion1=document.getElementById('d_urbanizacion1i').value;
	d_nlocal1=document.getElementById('d_nlocal1i').value;
	d_estado1=document.getElementById('d_estado1i').value;
  d_codepostal1=document.getElementById('d_codepostal1i').value;
	d_ptoref1=document.getElementById('d_ptoref1i').value;
	m=document.getElementById('fechacarga').value;
	n=document.getElementById('prefijo').value;
	namearch=document.getElementById('namearch').value;
	usuario=document.getElementById('usuario').value;
	id_registro=document.getElementById('id_registro').value;

if ((d_calle_av1||d_localidad1||d_estado1||d_urbanizacion1||d_nlocal1||d_estado1 ||d_codepostal1||d_ptoref1)=="") {
	 alertify.error("Error,los campos estan vacios.");
     return false;
}
else if(d_estado1==""){
	alertify.error('Error, Debe seleccionar un Estado');
 	$('#edit_estado').focus().select();
	$('#edit_estado').select().css("border-color","red" );
	return false;
     }
else if(d_localidad1==""){
	alertify.error('Error, debe seleccionar un Municipio');
 	$('#edit_localidad').focus().select();
	$('#edit_localidad').select().css("border-color","red");
	return false;
     }
else if(d_sector1==""){
	alertify.error('Error, debe seleccionar una Parroquia');
 	$('#edit_sector').focus();
	$('#edit_sector').css("border-color","red");
	return false;
     }

else{
	
	var variable=coddocumento+'|-|SEPARATOR|-|'+codetipodir1+'|-|SEPARATOR|-|'+d_calle_av1+'|-|SEPARATOR|-|'+d_localidad1+'|-|SEPARATOR|-|'+d_sector1+'|-|SEPARATOR|-|'+d_urbanizacion1+'|-|SEPARATOR|-|'+d_nlocal1+'|-|SEPARATOR|-|'+ d_estado1+'|-|SEPARATOR|-|'+d_codepostal1+'|-|SEPARATOR|-|'+d_ptoref1+'|-|SEPARATOR|-|'+m+'|-|SEPARATOR|-|'+n+'|-|SEPARATOR|-|'+namearch+'|-|SEPARATOR|-|'+usuario+'|-|SEPARATOR|-|'+id_registro;
			  $.post('view/comercializacion/gestion/direcciones.php',{postvariable:variable},
			   function(data)
			   {
			   	modified=false;
				alertify.success(data);
				//parent.document.location = parent.document.location;
			   setTimeout(function(){ window.location.reload(0); }, 4000); 

			   });

	}		  
        return false;
}

function buscarserialmasiva(){
	var value = $('#idserial').val();
	   var value_without_space2 = $.trim(value);
	   var value_without_space=value_without_space2.toUpperCase();
	 var rif =$('#idserial').val(value_without_space);
   
	   if(document.getElementById('idserial').value == "")
	   {   
			// PORFA NO EXPLOTESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
	
			window.location.href='?cargar=buscarregistromasivoglobal&var='
   
		   return false;
	   }
	   else{
		   var variable=document.getElementById('idserial').value;
		   window.location.href='?cargar=buscarregistromasivo&var='+variable
	   }
   }

   function mostrarBusquedaMasiva(){
      if ($('#panelbusquedamasiva').css('display') == 'none') {
          $('#panelbusquedamasiva').css('display', 'block') ;
          $('#panelbusquedafecha').css('display', 'none')  ;
      }
      else {
          $('#panelbusquedamasiva').css('display', 'none')  ;
      }
}