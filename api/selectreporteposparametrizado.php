<?php
require("../controller/ControladorReporteAngular.php");

$data = json_decode(file_get_contents('php://input'));


$controlador =new ControladorReporte();
$cantidad= $controlador->sp_reporteparametrizacion($data->banco,$data->estatus);
$result=pg_fetch_all($cantidad);

if (pg_num_rows($cantidad) > 0) {
  echo json_encode($result);
} else {
    echo "0 results";
}

?>

