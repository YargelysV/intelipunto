<?php
require("../controller/ControladorServicioTecnicoAngular.php");

$data = json_decode(file_get_contents('php://input'));

$controlador =new ControladorServicioTecnico();
$cantidad= $controlador->sp_buscargestionservicioderosal($data->banco,$data->fecha,$data->origen);
$result=pg_fetch_all($cantidad);

if (pg_num_rows($cantidad) > 0) {
  echo json_encode($result);
} else {
    echo "0 results";
}

?>