<?php
//session_start();
include_once("../../../model/Pagos.php");

class controladorPagosAjax{

	private $pagos;

	public function __construct(){
	$this->pagos= new Pagos();
	}

	public function sp_updateStatusPago($idafiliado,$referencia,$estatus,$usuario){
	    $this->pagos->set("idafiliado",$idafiliado);
	    $this->pagos->set("referencia",$referencia);
	    $this->pagos->set("estatus",$estatus);
	    $this->pagos->set("usuario",$usuario);
	    $datos=$this->pagos->sp_updateStatusPago();
	    return $datos;
	}

	public function sp_updateStatusPagoMifi($idafiliado,$referencia,$estatus,$usuario){
	    $this->pagos->set("idafiliado",$idafiliado);
	    $this->pagos->set("referencia",$referencia);
	    $this->pagos->set("estatus",$estatus);
	    $this->pagos->set("usuario",$usuario);
	    $datos=$this->pagos->sp_updateStatusPagoMifi();
	    return $datos;
	}


	public function sp_estatuspagos($idafiliado,$estatus){
	    $this->pagos->set("idafiliado",$idafiliado);
	    $this->pagos->set("estatus",$estatus);
	    $datos=$this->pagos->sp_estatuspagos();
	    return $datos;
	}
	public function sp_updateEstatusFact($idafiliado,$usuario){
	    $this->pagos->set("idafiliado",$idafiliado);
	    $this->pagos->set("usuario",$usuario);
	    $datos=$this->pagos->sp_updateEstatusFact();
	    return $datos;
	}

	public function sp_updateEstatusFactMifi($idafiliado,$usuario){
	    $this->pagos->set("idafiliado",$idafiliado);
	    $this->pagos->set("usuario",$usuario);
	    $datos=$this->pagos->sp_updateEstatusFactMifi();
	    return $datos;
	}

	public function sp_estatuspagosmifi($idafiliado,$estatus){
	    $this->pagos->set("idafiliado",$idafiliado);
	    $this->pagos->set("estatus",$estatus);
	    $datos=$this->pagos->sp_estatuspagosmifi();
	    return $datos;
	}
		public function sp_guardarfactura($idregistro,$numerofactura,$fechafactura,$montofactura,$serial,$simcard,$serialm,$codigodestino,$usuario,$consecutivo,$idestatus,$cuentabanco){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("numerofactura",$numerofactura);
	    $this->pagos->set("fechafactura",$fechafactura);
	    $this->pagos->set("montofactura",$montofactura);
	    $this->pagos->set("serial",$serial);
	    $this->pagos->set("simcard",$simcard);
	    $this->pagos->set("serialm",$serialm);
	    $this->pagos->set("codigodestino",$codigodestino);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $this->pagos->set("idestatus",$idestatus);
	    $this->pagos->set("cuentabanco",$cuentabanco);
	    //$this->pagos->set("fechainss",$fechainss);
	    //$this->pagos->set("idgestion",$idgestion);

	    $datos=$this->pagos->sp_guardarfactura();
	    return $datos;
	}


		public function sp_guardarfacturamifi($idregistro,$numerofactura,$fechafactura,$montofactura,$mifiserial,$codigodestino,$usuario,$consecutivo,$simserial){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("numerofactura",$numerofactura);
	    $this->pagos->set("fechafactura",$fechafactura);
	    $this->pagos->set("montofactura",$montofactura);
	    $this->pagos->set("mifiserial",$mifiserial);
	    $this->pagos->set("codigodestino",$codigodestino);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $this->pagos->set("simserial",$simserial);
	    $datos=$this->pagos->sp_guardarfacturamifi();
	    return $datos;
	}	


	public function sp_guardaranulacion($idregistro,$numerofactura,$fechafactura,$montofactura,$usuario){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("numerofactura",$numerofactura);
	    $this->pagos->set("fechafactura",$fechafactura);
	    $this->pagos->set("montofactura",$montofactura);
	    $this->pagos->set("usuario",$usuario);
	    $datos=$this->pagos->sp_guardaranulacion();
	    return $datos;
	}

		public function sp_guardaranulaciondemifi($idregistro,$numerofactura,$fechafactura,$montofactura,$usuario){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("numerofactura",$numerofactura);
	    $this->pagos->set("fechafactura",$fechafactura);
	    $this->pagos->set("montofactura",$montofactura);
	    $this->pagos->set("usuario",$usuario);
	    $datos=$this->pagos->sp_guardaranulaciondemifi();
	    return $datos;
	}

		public function sp_guardardevolucion($idregistro,$facturadev,$fechadev,$montodev,$serialdev,$simcarddev,$serialmdev,$codigomotivo,$usuario,$consecutivo,$observacion,$id_estatus,$idgestiones){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("facturadev",$facturadev);
	    $this->pagos->set("fechadev",$fechadev);
	    $this->pagos->set("montodev",$montodev);
	    $this->pagos->set("serialdev",$serialdev);
	    $this->pagos->set("simcarddev",$simcarddev);
	    $this->pagos->set("serialmdev",$serialmdev);
	    $this->pagos->set("codigomotivo",$codigomotivo);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $this->pagos->set("observacion",$observacion);
	  	$this->pagos->set("id_estatus",$id_estatus);
	  	$this->pagos->set("idgestiones",$idgestiones);
	    $datos=$this->pagos->sp_guardardevolucion();
	    return $datos;
	}

				public function sp_guardardevolucionesdemifi($idregistro,$facturadev,$fechadev,$montodev,$serialmifidev,$devsimcard,$codigomotivo,$usuario,$consecutivo,$observacion){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("facturadev",$facturadev);
	    $this->pagos->set("fechadev",$fechadev);
	    $this->pagos->set("montodev",$montodev);
	    $this->pagos->set("serialmifidev",$serialmifidev);
	    $this->pagos->set("devsimcard",$devsimcard);
	    $this->pagos->set("codigomotivo",$codigomotivo);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $this->pagos->set("observacion",$observacion);
	    
	  //  $this->pagos->set("id_estatus",$id_estatus);
	    $datos=$this->pagos->sp_guardardevolucionesdemifi();
	    return $datos;
	}

		public function sp_guardardeclinacionmifi($idregistro,$facturadevtotal,$fechadevtotal,$montodevtotal,$serialmifidec,$decsimcard,$codigomotivo,$usuario,$consecutivo){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("facturadevtotal",$facturadevtotal);
	    $this->pagos->set("fechadevtotal",$fechadevtotal);
	    $this->pagos->set("montodevtotal",$montodevtotal);
	    $this->pagos->set("serialmifidec",$serialmifidec);
	    $this->pagos->set("decsimcard",$decsimcard);
	    $this->pagos->set("codigomotivo",$codigomotivo);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $datos=$this->pagos->sp_guardardeclinacionmifi();
	    return $datos;
	}
	
	public function sp_guardardevoluciontotal($idregistro,$facturadevtotal,$fechadevtotal,$montodevtotal,$serialdevtotal,$simcarddevtotal,$serialmdevtotal,$codigomotivo,$usuario,$consecutivo){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("facturadevtotal",$facturadevtotal);
	    $this->pagos->set("fechadevtotal",$fechadevtotal);
	    $this->pagos->set("montodevtotal",$montodevtotal);
	    $this->pagos->set("serialdevtotal",$serialdevtotal);
	    $this->pagos->set("simcarddevtotal",$simcarddevtotal);
	    $this->pagos->set("serialmdevtotal",$serialmdevtotal);
	    $this->pagos->set("codigomotivo",$codigomotivo);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $datos=$this->pagos->sp_guardardevoluciontotal();
	    return $datos;
	}

	public function sp_crudpago($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$tipomoneda,$monto,$montousd,$factorconversion,$referencia,$nombredepositante,$imagenpago,$observacioncomer,$observacionadm,$usuario)
   {
			$this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$tipomoneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$nombredepositante);
			$this->pagos->set("capture",$imagenpago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuario);
			$resultado=$this->pagos->sp_crudpago();		
			return $resultado;		
    }

    	public function updatePago ($idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$moneda,$monto,$montousd,$factorconversion,$referencia,$depositante,$capture,$observacioncomer,$observacionadm,$usuariocarga)
    {
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$moneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$depositante);
			$this->pagos->set("capture",$capture);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuariocarga);
			$resultado=$this->pagos->updatePago();
			return $resultado;
	}	

		public function eliminarPago($referencia,$idafiliado){
		$this->pagos->set("referencia",$referencia);
		$this->pagos->set("idafiliado",$idafiliado);
		$datos=$this->pagos->eliminarPago();
		return $datos;
		}

		public function sp_verpagos($id_afiliado){
		$this->pagos->set("idafiliado",$id_afiliado);
		$datos=$this->pagos->sp_verpagos();
		return $datos;
		}

		public function editarPago($idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$tipomoneda,$monto,$montousd,$factorconversion,$referencia,$nombredepositante,$imagenpago,$observacioncomer,$observacionadm,$usuario,$id)
   {
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$tipomoneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$nombredepositante);
			$this->pagos->set("capture",$imagenpago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuario);
			$this->pagos->set("id",$id);
			$resultado=$this->pagos->editarPago();		
			return $resultado;		
    }
    public function sp_cargarpagosmifi($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$tipomoneda,$monto,$montousd,$factorconversion,$referencia,$nombredepositante,$imagenpago,$observacioncomer,$observacionadm,$usuario)
   {
			$this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$tipomoneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$nombredepositante);
			$this->pagos->set("capture",$imagenpago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuario);
			$resultado=$this->pagos->sp_cargarpagosmifi();		
			return $resultado;		
    }

    public function editarPagoMifi ($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$moneda,$monto,$montousd,$factorconversion,$referencia,$depositante,$capture,$observacioncomer,$observacionadm,$usuariocarga)
    {
		    $this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$moneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$depositante);
			$this->pagos->set("capture",$capture);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuariocarga);
			$resultado=$this->pagos->editarPagoMifi();
			return $resultado;
	}

	public function eliminarPagoMifi($referencia,$idafiliado){
		$this->pagos->set("referencia",$referencia);
		$this->pagos->set("idafiliado",$idafiliado);
		$datos=$this->pagos->eliminarPagoMifi();
		return $datos;
		}

	public function sp_guardarpercance($idregistro,$serialperc,$simcardperc,$serialmifiperc,$codmotivo,$usuario,$consecutivo,$observacion,$id_estatusperc){
	    $this->pagos->set("idregistro",$idregistro);
	    //$this->pagos->set("fechapercance",$fechapercance);
	    $this->pagos->set("serialperc",$serialperc);
	    $this->pagos->set("simcardperc",$simcardperc);
	    $this->pagos->set("serialmifiperc",$serialmifiperc);
	    $this->pagos->set("codmotivo",$codmotivo);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $this->pagos->set("observacion",$observacion);
	    $this->pagos->set("id_estatusperc",$id_estatusperc);
	    $datos=$this->pagos->sp_guardarpercance();
	    return $datos;
	}

	public function sp_DeclinacionSinSeriales($idregistro,$usuario){
		$this->pagos->set("iidregistro",$idregistro);
		$this->pagos->set("iusuario",$usuario);
		$datos=$this->pagos->sp_DeclinacionSinSeriales();
	    return $datos;
	}

	public function sp_guardarrifbanco($idregistro,$rifbanco){
	    $this->pagos->set("idregistro",$idregistro);
	    $this->pagos->set("rifbanco",$rifbanco);
	    $datos=$this->pagos->sp_guardarrifbanco();
	    return $datos;
	}

	public function sp_pagosnoconf(){
		$datos=$this->pagos->sp_pagosnoconf();
		return $datos;
	}

	public function sp_guardarrecompra($idregistro,$usuario,$serialrecompra,$montorecompra,$consecutivo){
	    $this->pagos->set("idregistro",$idregistro);
	    //$this->pagos->set("fechapercance",$fechapercance);
	    $this->pagos->set("usuario",$usuario);
	    $this->pagos->set("serialrecompra",$serialrecompra);
	    $this->pagos->set("montorecompra",$montorecompra);
	    $this->pagos->set("consecutivo",$consecutivo);
	    $datos=$this->pagos->sp_guardarrecompra();
	    return $datos;
	}

	public function sp_recompranoconf(){
		$datos=$this->pagos->sp_recompranoconf();
		return $datos;
}
	public function sp_registrosporaprobar(){
	    $datos=$this->pagos->sp_registrosporaprobar();
	    return $datos;
	}
	public function sp_expedientesporaprobar(){
	    $datos=$this->pagos->sp_expedientesporaprobar();
	    return $datos;
	}
	public function sp_estatusRechazo(){
	    $datos=$this->pagos->sp_estatusRechazo();
	    return $datos;
	}
	public function sp_subestatusRechazo($idprefijo){
		$this->pagos->set("idprefijo",$idprefijo);
	    $datos=$this->pagos->sp_subestatusRechazo();
	    return $datos;
	}

	public function sp_guardaraprobacion($id_registro,$estatus, $usuario){
	    $this->pagos->set("id_registro",$id_registro);
	    $this->pagos->set("estatus",$estatus);
	    $this->pagos->set("usuario",$usuario);
	    $datos=$this->pagos->sp_guardaraprobacion();
	    return $datos;
	}
	public function sp_expaprobadobackof($iconsecutivo,$usuario){
	    $this->pagos->set("iconsecutivo",$iconsecutivo);
	    $this->pagos->set("usuario",$usuario);

	    $datos=$this->pagos->sp_expaprobadobackof();
	    return $datos;
	}
	// aqui desaprobamos
	public function sp_expdesaprobadobackof($iconsecutivo, $estatusdes, $usuario){
	    $this->pagos->set("iconsecutivo",$iconsecutivo);
		$this->pagos->set("estatusdes",$estatusdes);
	    $this->pagos->set("usuario",$usuario);

	    $datos=$this->pagos->sp_expdesaprobadobackof();
	    return $datos;
	}
	// 
		// aqui desaprobamos
		public function sp_expdocdesaprobadobackof($iconsecutivo, $observacion, $usuario){
			$this->pagos->set("iconsecutivo",$iconsecutivo);
			$this->pagos->set("observacion",$observacion);
			$this->pagos->set("usuario",$usuario);
	
			$datos=$this->pagos->sp_expdocdesaprobadobackof();
			return $datos;
		}
		// 
		// aqui desaprobamos y lo notificAmos
		public function sp_expdocdesaprobadobackofmotivado($iconsecutivo, $estatusdes, $usuario){
			$this->pagos->set("iconsecutivo",$iconsecutivo);
			$this->pagos->set("estatusdes",$estatusdes);
			$this->pagos->set("usuario",$usuario);
	
			$datos=$this->pagos->sp_expdocdesaprobadobackofmotivado();
			return $datos;
		}
		// 
	public function sp_expdesaprobadobackofdocs($iconsecutivo, $estatusdes, $estatusdesc, $usuario){
	    $this->pagos->set("iconsecutivo",$iconsecutivo);
		$this->pagos->set("estatusdes",$estatusdes);
		$this->pagos->set("estatusdesc",$estatusdesc);
	    $this->pagos->set("usuario",$usuario);

	    $datos=$this->pagos->sp_expdesaprobadobackofdocs();
	    return $datos;
	}
	
	// separador pa no olvidar
public function spinsertarhistorial($idregistro,$afiliado,$usuario,$mensaje,$detalles){
	//gestioncomercial
	$this->pagos->set("idregistro",$idregistro);
	$this->pagos->set("afiliado",$afiliado);
	$this->pagos->set("usuario",$usuario);
	$this->pagos->set("mensaje",$mensaje);
	$this->pagos->set("detalles",$detalles);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->pagos->spinsertarhistorial();
	return $datos;
	}
//
}


?>
