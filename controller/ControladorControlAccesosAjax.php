<?php
	//session_start();
	include_once("../../model/ControlAcceso.php");

	class ControladorControlAcceso{

		private $controlaccesos;

		public function __construct(){
		$this->controlaccesos= new controlacceso();
		}

		public function sp_buscaridempleados(){
			$datos=$this->controlaccesos->sp_buscaridempleados();
			return $datos;
		}

		public function sp_asignarferiados($fechass){
			//echo $fechass;
			$this->controlaccesos->set("fechass",$fechass);
			$datos=$this->controlaccesos->sp_asignarferiados();
			return $datos;
		}

		public function sp_eliminarferiados($fechas){
			//echo $fechass;
			$this->controlaccesos->set("fechas",$fechas);
			$datos=$this->controlaccesos->sp_eliminarferiados();
			return $datos;
		}

		public function sp_mostrarferiados(){
			$datos=$this->controlaccesos->sp_mostrarferiados();
			return $datos;
		}

		public function spverarchivoscontrolacceso(){
			$datos=$this->controlaccesos->spverarchivoscontrolacceso();
			return $datos;
		}

		public function speliminararchivoaccesos($nombrearchivo,$login){
			$this->controlaccesos->set("nombrearchivos",$nombrearchivo);
			$this->controlaccesos->set("login",$login);
			$datos=$this->controlaccesos->speliminararchivoaccesos();
			return $datos;
		}

	}


?>

