<?php

include_once("../model/Pagos.php");

	class controladorPagos
	{
		private $pagos;
		
	public function __construct()
		{
		$this->pagos=new Pagos();
		}
		
	public function sp_verpagos($id_afiliado){
		$this->pagos->set("idafiliado",$id_afiliado);
		$datos=$this->pagos->sp_verpagos();
		return $datos;
	}

	public function sp_verpagosdemifi($idafiliado){
		$this->pagos->set("idafiliado",$idafiliado);
		$datos=$this->pagos->sp_verpagosdemifi();
		return $datos;
	}

	public function sp_crudpago($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$tipomoneda,$monto,$montousd,$factorconversion,$referencia,$nombredepositante,$imagenpago,$observacioncomer,$observacionadm,$usuario)
   {
			$this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$tipomoneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$nombredepositante);
			$this->pagos->set("capture",$imagenpago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuario);
			$resultado=$this->pagos->sp_crudpago();		
			return $resultado;		
    }

    public function sp_cargarpagosmifi($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$moneda,$monto,$montousd,$factorconversion,$referencia,$depositante,$capture,$observacioncomer,$observacionadm,$usuariocarga)
   {
			$this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$moneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$depositante);
			$this->pagos->set("capture",$capture);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuariocarga);
			$resultado=$this->pagos->sp_cargarpagosmifi();				
    }


	public function editarPago ($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$moneda,$monto,$montousd,$factorconversion,$referencia,$depositante,$capture,$observacioncomer,$observacionadm,$usuariocarga)
    {
		    $this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$moneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$depositante);
			$this->pagos->set("capture",$capture);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuariocarga);
			$resultado=$this->pagos->editarPago();
			return $resultado;
	}


	public function updatePago ($idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$moneda,$monto,$montousd,$factorconversion,$referencia,$depositante,$capture,$observacioncomer,$observacionadm,$usuariocarga)
    {
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$moneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$depositante);
			$this->pagos->set("capture",$capture);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuariocarga);
			$resultado=$this->pagos->updatePago();
			return $resultado;
	}	

	public function editarPagoMifi ($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$moneda,$monto,$montousd,$factorconversion,$referencia,$depositante,$capture,$observacioncomer,$observacionadm,$usuariocarga)
    {
		    $this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$moneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$depositante);
			$this->pagos->set("capture",$capture);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuariocarga);
			$resultado=$this->pagos->editarPagoMifi();
			return $resultado;
	}

    public function eliminarPago($referencia,$idafiliado)
	{
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("idafiliado",$idafiliado);	
			$resultado=$this->pagos->eliminarPago();
			return $resultado;
	}


    public function eliminarPagoMifi($referencia,$idafiliado)
	{
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("idafiliado",$idafiliado);	
			$resultado=$this->pagos->eliminarPagoMifi();
			return $resultado;
	}
}

?>
