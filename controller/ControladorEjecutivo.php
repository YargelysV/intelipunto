<?php

include_once("model/Ejecutivo.php");

	class controladorEjecutivo
	{
		private $ejecutivo;
		
		public function __construct(){
			$this->ejecutivo= new Ejecutivo();
		}
		
			public function mostrarEjecutivos(){
			$datos=$this->ejecutivo->mostrarEjecutivos();
			return $datos;
		} 

         
		public function ejecutivobanco ($vendedor)
		{
			$this->ejecutivo->set("vendedor",$vendedor);
			$datos=$this->ejecutivo->ejecutivobanco();
			return $datos;
		}

		public function verejecutivoxbanco ($vendedor)
		{
			$this->ejecutivo->set("vendedor",$vendedor);
			$datos=$this->ejecutivo->verejecutivoxbanco();
			return $datos;
		}
		
    	public function agregarEjecutivos ($operacion, $coddocumento,$nombres, $apellidos, $codejecutivo,$nejecutivo, $usuario, $aliasvendedor)
       	{
			$this->ejecutivo->set("operacion",$operacion);
			$this->ejecutivo->set("documento",$coddocumento);
			$this->ejecutivo->set("nombres",$nombres);
			$this->ejecutivo->set("apellidos",$apellidos);
			$this->ejecutivo->set("codejecutivo",$codejecutivo);
			$this->ejecutivo->set("nejecutivo",$nejecutivo);
			$this->ejecutivo->set("usuario",$usuario);
			$this->ejecutivo->set("aliasvendedor",$aliasvendedor);
			$resultado=$this->ejecutivo->crear();
			return $resultado;
		}

		public function mostrarEjecutivos1($coddocumento){
			$this->ejecutivo->set("documento",$coddocumento);
			$datos=$this->ejecutivo->mostrarEjecutivos1();
			return $datos;
		}    

		public function EditarEjecutivos ($operacion, $coddocumento,$nombres, $apellidos, $codejecutivo,$nejecutivo, $usuario, $aliasvendedor)
		{
			$this->ejecutivo->set("operacion",$operacion);
			$this->ejecutivo->set("documento",$coddocumento);
			$this->ejecutivo->set("nombres",$nombres);
			$this->ejecutivo->set("apellidos",$apellidos);
			$this->ejecutivo->set("codejecutivo",$codejecutivo);
			$this->ejecutivo->set("nejecutivo",$nejecutivo);
			$this->ejecutivo->set("usuario",$usuario);
			$this->ejecutivo->set("aliasvendedor",$aliasvendedor);
			$resultado=$this->ejecutivo->EditarEjecutivo();
			return $resultado;
		}

			public function actualizarbanco ($banco,$vendedor)
		{
			
			$this->ejecutivo->set("banco",$banco);
			$this->ejecutivo->set("vendedor",$vendedor);
			$resultado=$this->ejecutivo->actualizarbanco();
			return $resultado;
		}

			public function eliminarbanco ($banco)
		{
			$this->ejecutivo->set("banco",$banco);
	
			$resultado=$this->ejecutivo->eliminarbanco();
			return $resultado;
		}

	    public function EliminarEjecutivo($operacion,$documento)
	    {
			$this->ejecutivo->set("operacion",$operacion);	
			$this->ejecutivo->set("documento",$documento);
			$resultado=$this->ejecutivo->eliminar();
			return $resultado;
		}

		public function tipodocumento(){
			$datos=$this->ejecutivo->vertipodocumentos();
			return $datos;
		}
    }
?>