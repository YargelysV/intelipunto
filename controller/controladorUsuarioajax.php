<?php
	//session_start();
	include_once("../../model/Usuario.php");
	
		class controladorUsuario{
		
			private $usuario;
			
			public function __construct(){
				$this->usuario= new Usuario();
			}
		
			
		public function eliminar($login){
				$this->usuario->set("login",$login);	
				$resultado=$this->usuario->eliminar();
				return $resultado;
		}
		
		
		public function buscarusuario($buscar,$inicio,$nroreg){
		$this->usuario->set("login",$buscar);
		$this->usuario->set("inicio",$inicio);
		$this->usuario->set("nroreg",$nroreg);	
		$datos=$this->usuario->buscarusuario();
		return $datos;
		}
	
		public function reiniciar($login){
		$this->usuario->set("login",$login);
		$this->usuario->set("clave",1234567);	
		$resultado=$this->usuario->reiniciar();
		return $resultado;
		}

		public function desbloquear($login){
		$this->usuario->set("login",$login);
		$this->usuario->set("clave",1234567);	
		$resultado=$this->usuario->desbloquear();
		return $resultado;
		}
		
		public function editarestatusmodulo($login,$codtipoacceso, $modulo,$usuarioadm,$status){
		$this->usuario->set("login",$login);
		$this->usuario->set("codtipoacceso",$codtipoacceso);
		$this->usuario->set("modulo",$modulo);
		$this->usuario->set("usuarioadm",$usuarioadm);	
		$this->usuario->set("status",$status);
		$resultado=$this->usuario->editarestatusmodulo();
		return $resultado;
		}
		
		public function editartipoacceso($login, $codtipoacceso, $modulo,$usuario,$status,$codtipo){
			$this->usuario->set("login",$login);
			$this->usuario->set("codtipoacceso",$codtipoacceso);
			$this->usuario->set("modulo",$modulo);
			$this->usuario->set("usuario",$usuario);
			$this->usuario->set("status",$status);
			$this->usuario->set("codtipo",$codtipo);	
		$resultado=$this->usuario->editartipoacceso();
		return $resultado;
		}
		
		public function buscarmodulos($buscar,$inicio,$nroreg, $login,$acceso){
		$this->usuario->set("buscar",$buscar);
		$this->usuario->set("inicio",$inicio);
		$this->usuario->set("nroreg",$nroreg);	
		$this->usuario->set("login",$login);
		$this->usuario->set("codtipousuario",$acceso);
		$datos=$this->usuario->buscarmodulos();
		return $datos;
		}

		public function editaractividad($usuario){
		$this->usuario->set("login",$usuario);
		$this->usuario->set("estatus",0);
		$resultado=$this->usuario->editaractividad();
		return $resultado;
		}

		public function sp_buscarusuarios(){
		$resultado=$this->usuario->sp_buscarusuarios();
		return $resultado;
		}

		public function listarmodulos($login,$acceso){
		$this->usuario->set("login",$login);
		$this->usuario->set("codtipousuario",$acceso);
		$resultado=$this->usuario->listarmodulos();
		return $resultado;
		}

		public function splistaregion(){
		$resultado=$this->usuario->splistaregion();
		return $resultado;
		}

		public function sp_mostrarsucursal($idregion){
		$this->usuario->set("idregion",$idregion);
		$datos=$this->usuario->sp_mostrarsucursal();
		return $datos;
		}

		public function sp_selectReporteSuc($login){
		$this->usuario->set("login",$login);
		$datos=$this->usuario->sp_selectReporteSuc();
		return $datos;
		}

		public function sp_selectsucursalasig($idregion,$login){
		$this->usuario->set("idregion",$idregion);
		$this->usuario->set("login",$login);
		$datos=$this->usuario->sp_selectsucursalasig();
		return $datos;
		}

		public function spguardarsucursal($idregion,$valor,$login,$usuario_acceso){
		$this->usuario->set("idregion",$idregion);
		$this->usuario->set("valor",$valor);
		$this->usuario->set("login",$login);
		$this->usuario->set("usuario_acceso",$usuario_acceso);
		$datos=$this->usuario->spguardarsucursal();
		return $datos;
		}

		public function spEliminarsucursalUsuario($idregion,$login){
		$this->usuario->set("idregion",$idregion);
		$this->usuario->set("login",$login);
		$datos=$this->usuario->spEliminarsucursalUsuario();
		return $datos;
		}

	}
		
?>
	