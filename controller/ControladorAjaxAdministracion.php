<?php
	//session_start();
	include_once("../model/Adjuntar.php");

	class controladorAdjuntar{

		private $adjuntar;

		public function __construct(){
		$this->adjuntar= new Adjuntar();
		}

		public function spverclientefacturacion($cliente, $fechadetalle, $namearchivo,$masiva, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
			$this->adjuntar->set("cliente",$cliente);
			$this->adjuntar->set("fecha",$fechadetalle);
			$this->adjuntar->set("namearchivo",$namearchivo);
			$this->adjuntar->set("masiva",$masiva);
			$this->adjuntar->set("tipousuario",$tipousuario);
			$this->adjuntar->set("ejecutivo",$ejecutivo);
			$this->adjuntar->set("usuario",$usuario);
			$this->adjuntar->set("coordinador",$coordinador);
			$this->adjuntar->set("coordinadordos",$coordinadordos);
			$datos=$this->adjuntar->sp_verclientefacturacion();
			return $datos;
		}

		public function sp_buscarclientefacturacion($cliente,$fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
			$this->adjuntar->set("cliente",$cliente);
			$this->adjuntar->set("FechaRecepcionArchivo",$fecha);
			$this->adjuntar->set("origen",$origen);
			$this->adjuntar->set("tipousuario",$tipousuario);
			$this->adjuntar->set("ejecutivo",$ejecutivo);
			$this->adjuntar->set("usuario",$usuario);
			$this->adjuntar->set("coordinador",$coordinador);
			$this->adjuntar->set("coordinadordos",$coordinadordos);
			$datos=$this->adjuntar->sp_buscarclientefacturacion();
			return $datos;
		}

		public function sp_buscarclientefacturacionRif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
			$this->adjuntar->set("rif",$rif);
			$this->adjuntar->set("tipousuario",$tipousuario);
			$this->adjuntar->set("ejecutivo",$ejecutivo);
			$this->adjuntar->set("usuario",$usuario);
			$this->adjuntar->set("coordinador",$coordinador);
			$this->adjuntar->set("coordinadordos",$coordinadordos);
			$datos=$this->adjuntar->sp_buscarclientefacturacionRif();
			return $datos;
		}

		public function sp_verclientefacturacionmifi($cliente, $fechadetalle, $namearchivo,$masiva, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
			$this->adjuntar->set("cliente",$cliente);
			$this->adjuntar->set("fecha",$fechadetalle);
			$this->adjuntar->set("namearchivo",$namearchivo);
			$this->adjuntar->set("masiva",$masiva);
			$this->adjuntar->set("tipousuario",$tipousuario);
			$this->adjuntar->set("ejecutivo",$ejecutivo);
			$this->adjuntar->set("usuario",$usuario);
			$this->adjuntar->set("coordinador",$coordinador);
			$this->adjuntar->set("coordinadordos",$coordinadordos);
			$datos=$this->adjuntar->sp_verclientefacturacionmifi();
			return $datos;
		}
		
		public function spbuscarclientefacturacionmifixrif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->adjuntar->set("rif",$rif);
		$this->adjuntar->set("tipousuario",$tipousuario);
		$this->adjuntar->set("ejecutivo",$ejecutivo);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("coordinador",$coordinador);
		$this->adjuntar->set("coordinadordos",$coordinadordos);
		$datos=$this->adjuntar->spbuscarclientefacturacionmifixrif();
		return $datos;
	}

	public function sp_buscarclientefacturacionpagos($tipousuario, $ejecutivo, $usuario, $coordinador,$coordinadordos){
			$this->adjuntar->set("tipousuario",$tipousuario);
			$this->adjuntar->set("ejecutivo",$ejecutivo);
			$this->adjuntar->set("usuario",$usuario);
			$this->adjuntar->set("coordinador",$coordinador);
			$this->adjuntar->set("coordinadordos",$coordinadordos);
			$datos=$this->adjuntar->sp_buscarpagosnoconf();
			return $datos;
		}

		public function spverclientefacturacionpagos($usuario, $ejecutivoin, $tipousuario, $coordinador,$coordinadordos){
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("ejecutivoin",$ejecutivoin);
		$this->adjuntar->set("tipousuario",$tipousuario);
		$this->adjuntar->set("coordinador",$coordinador);
		$this->adjuntar->set("coordinadordos",$coordinadordos);
		$datos=$this->adjuntar->spverclientefacturacionpagos();
		return $datos;
	}

}
?>
