<?php
include_once("../../model/Captacion.php");

class controladorCaptacion{

	private $captacion;


	public function __construct(){
		$this->captacion= new Captacion();
	}

		public function sp_clientesnuevos($nombre,$apellido,$banco1,$rif,$telf,$observaciones,$usuario,$razon_social,$tipopos,$cantidadpos,$Correo,$Direccion,$medio_contacto,$tipo_cliente,$banco2,$n_afiliacion,$estatusaprob){
		$this->captacion->set("nombre",$nombre);
		$this->captacion->set("apellido",$apellido);
		$this->captacion->set("banco1",$banco1);
		$this->captacion->set("rif",$rif);
		$this->captacion->set("telf",$telf);
		$this->captacion->set("observaciones",$observaciones);
		$this->captacion->set("razon_social",$razon_social);
		$this->captacion->set("tipopos",$tipopos);
		$this->captacion->set("cantidadpos",$cantidadpos);
		$this->captacion->set("Correo",$Correo);
		$this->captacion->set("Direccion",$Direccion);
		$this->captacion->set("medio_contacto",$medio_contacto);
		$this->captacion->set("tipo_cliente",$tipo_cliente);
		$this->captacion->set("banco2",$banco2);
		$this->captacion->set("usuario",$usuario);
		$this->captacion->set("n_afiliacion",$n_afiliacion);
		$this->captacion->set("estatusaprob",$estatusaprob);
		$datos=$this->captacion->sp_clientesnuevos();
		return $datos;
	}

		public function sp_bancosinteres(){
		$datos=$this->captacion->sp_bancosinteres();
		return $datos;
	}
		public function sp_tipopos($banco){
			$this->captacion->set("banco",$banco);
			$datos=$this->captacion->sp_tipopos();
				return $datos;
	}

		public function sp_medio_contacto(){
			$datos=$this->captacion->sp_medio_contacto();
				return $datos;
	}

		public function sp_tipo_cliente(){
			$datos=$this->captacion->sp_tipo_cliente();
				return $datos;
	}

		public function sp_ejecutivo_asignado(){
		$datos=$this->captacion->sp_ejecutivo_asignado();
		return $datos;
		}
	

	public function sp_validarduplicadoscaptacion($rif,$razon_social,$n_afiliacion){
		$this->captacion->set("rif",$rif);
		$this->captacion->set("razon_social",$razon_social);
	 	$this->captacion->set("n_afiliacion",$n_afiliacion);
		$datos=$this->captacion->sp_validarduplicadoscaptacion();
		return $datos;
	}

	public function sp_estatusaprobacion($id_registro,$estatus, $usuario){
	    $this->captacion->set("id_registro",$id_registro);
	    $this->captacion->set("estatus",$estatus);
	    $this->captacion->set("usuario",$usuario);
	    $datos=$this->captacion->sp_estatusaprobacion();
	    return $datos;
	}

}
?>
	
