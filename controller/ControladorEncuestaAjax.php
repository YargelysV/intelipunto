<?php
//session_start();
include_once("../../model/Encuesta.php");

class controladorEncuestaN{

	private $encuesta;

	public function __construct(){
	$this->encuesta= new EncuestaNueva();
	}


	public function spagregarpersona($idCliente,$dato){
		$this->encuesta->set("idCliente",$idCliente);
		$this->encuesta->set("dato",$dato);
		$datos=$this->encuesta->spagregarpersona();
		return $datos;
	}

	public function sp_reloadregistro($idregistro){
    	$this->encuesta->set("idregistro",$idregistro);
    	$datos=$this->encuesta->sp_reloadregistro();
    	return $datos;
    }



    public function sp_guardarGestionEncuesta($idcliente,$rlegal,$pcontacto,$usuario,$estatusgestion,$observaciones){
		$this->encuesta->set("idcliente",$idcliente);
		$this->encuesta->set("rlegal",$rlegal);
		$this->encuesta->set("pcontacto",$pcontacto);
		$this->encuesta->set("usuario",$usuario);
		$this->encuesta->set("estatusgestion",$estatusgestion);
		$this->encuesta->set("observaciones",$observaciones);
		$datos=$this->encuesta->sp_guardarGestionEncuesta();
		return $datos;
	}
	
		public function spverReferidosxcliente($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->spverReferidosxcliente();
			return $datos;
			}

	public function sp_verpcontacto($idcliente){
		$this->encuesta->set("id_cliente",$idcliente);
		$datos=$this->encuesta->sp_verpcontacto();
		return $datos;
	}

	public function sp_guardarEncuesta1($pregunta,$respuesta,$idcliente,$idbanco,$usuario,$fechaencuesta,$upcontacto,$rlegal){

		$this->encuesta->set("pregunta",$pregunta);
		$this->encuesta->set("respuesta",$respuesta);
		$this->encuesta->set("idcliente",$idcliente);
		$this->encuesta->set("id_banco",$idbanco);
		$this->encuesta->set("usuario",$usuario);
		$this->encuesta->set("fechaencuesta",$fechaencuesta);
		$this->encuesta->set("upcontacto",$upcontacto);
		$this->encuesta->set("rlegal",$rlegal);
		$datos=$this->encuesta->sp_guardarEncuesta1();
		return $datos;
	}

	public function sp_guardarEncuesta2($pregunta,$respuesta,$idcliente,$idbanco,$usuario,$fechaencuesta,$upcontacto){

		$this->encuesta->set("pregunta",$pregunta);
		$this->encuesta->set("respuesta",$respuesta);
		$this->encuesta->set("idcliente",$idcliente);
		$this->encuesta->set("idbanco",$idbanco);
		$this->encuesta->set("usuario",$usuario);
		$this->encuesta->set("fechaencuesta",$fechaencuesta);
		$this->encuesta->set("upcontacto",$upcontacto);
		$datos=$this->encuesta->sp_guardarEncuesta2();
		return $datos;
	}

	public function sp_guardarEncuesta3($pregunta,$respuesta,$respuesta_2,$idcliente,$idbanco,$usuario,$fechaencuesta,$upcontacto){

		$this->encuesta->set("pregunta",$pregunta);
		$this->encuesta->set("respuesta",$respuesta);
		$this->encuesta->set("respuesta_2",$respuesta_2);
		$this->encuesta->set("idcliente",$idcliente);
		$this->encuesta->set("idbanco",$idbanco);
		$this->encuesta->set("usuario",$usuario);
		$this->encuesta->set("fechaencuesta",$fechaencuesta);
		$this->encuesta->set("upcontacto",$upcontacto);
		$datos=$this->encuesta->sp_guardarEncuesta3();
		return $datos;
	}

	public function sp_guardarEncuesta4($pregunta,$respuesta,$idcliente,$idbanco,$usuario,$fechaencuesta,$upcontacto,$nombreref,$apellidoref,$tlfref){

		$this->encuesta->set("pregunta",$pregunta);
		$this->encuesta->set("respuesta",$respuesta);
		$this->encuesta->set("idcliente",$idcliente);
		$this->encuesta->set("id_banco",$idbanco);
		$this->encuesta->set("usuario",$usuario);
		$this->encuesta->set("fechaencuesta",$fechaencuesta);
		$this->encuesta->set("upcontacto",$upcontacto);
		$this->encuesta->set("nombreref",$nombreref);
		$this->encuesta->set("apellidoref",$apellidoref);
		$this->encuesta->set("tlfref",$tlfref);
		$datos=$this->encuesta->sp_guardarEncuesta4();
		return $datos;
	}

	public function sp_eliminarregistro($pregunta, $idcliente){
		
		$this->encuesta->set("pregunta",$pregunta);
        $this->encuesta->set("idcliente",$idcliente);
        $datos=$this->encuesta->sp_eliminarregistro();
		return $datos;
	}

	public function sp_cambiaestatusencuesta($idcliente){
		$this->encuesta->set("idcliente",$idcliente);
		$datos=$this->encuesta->sp_cambiaestatusencuesta();
		return $datos;
	}


	public function spverbancoencuesta($cliente)
	{
		$this->encuesta->set("cliente",$cliente);
/*		$this->encuesta->set("fecha",$fechadetalle);
		$this->encuesta->set("namearchivo",$namearchivo);*/
		$datos=$this->encuesta->spverbancoencuesta();
		return $datos;
	}

		public function EliminarReferidos($opcion, $id_consecutivo)
	{
	$this->encuesta->set("opcion",$opcion);
	$this->encuesta->set("id_consecutivo",$id_consecutivo);
	$resultado=$this->encuesta->EliminarReferidos();
	return $resultado;
	}

	
	public function spbuscarpersonasreferidas(){
	$datos=$this->encuesta->spbuscarpersonasreferidas();
	return $datos;
	}

	public function spverestatusencuesta($idcliente){
		$this->encuesta->set("idcliente",$idcliente);
		$datos=$this->encuesta->spverestatusencuesta();
		return $datos;
	}
}


?>
