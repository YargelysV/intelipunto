<?php
	class EnrutadorHome
	{
		public function cargarVista($vista)
		{
			switch($vista):

				default:
					include_once('view/home1.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/home1.php');
			}else{
				return true;
			}
		}

	}

	class Enrutador
	{
		public function cargarVista($vista)
		{
			switch($vista):

				default:
					include_once('view/iniciologin.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/iniciologin.php');
			}else{
				return true;
			}
		}

	}

/*	class EnrutadorUsuario
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "crear":
					include_once('view/usuario/'.$vista.'.php');
					break;
				case "ver":
					include_once('view/usuario/'.$vista.'.php');
					break;
				case "editar":
					include_once('view/usuario/'.$vista.'.php');
					break;
				case "eliminar":
					include_once('view/usuario/'.$vista.'.php');
					break;
				case "cambiarpassword":
					include_once('view/usuario/'.$vista.'.php');
					break;
				case "modulos":
					include_once('view/usuario/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;
		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/usuario/inicioUsuario.php');
			}else{
				return true;
			}
		}

	}*/

		class EnrutadordeUsuarios
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "crear":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "ver":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "editar":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "eliminar":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "cambiarpassword":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "modulos":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "reiniciar":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "regiones":
					include_once('view/usuarios/'.$vista.'.php');
					break;
				case "reporte":
					include_once('view/usuarios/'.$vista.'.php');
					break;				
				default:
					include_once('view/error.php');
			endswitch;
		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/usuarios/inicioUsuario.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorPassword
	{
		public function cargarVista($vista)
		{
			switch($vista):

				default:
					include_once('view/password/cambiarpassword.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/password/cambiarpassword.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorFacturacion
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntar":
					include_once('view/Administracion/facturacion/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Administracion/facturacion/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Administracion/facturacion/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/facturacion/inicioFacturacion.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorFacturacionPos
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarcliente":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;
				case "facturarcliente":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;
				case "buscarrif":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;
					case "confirmarPago":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;	
				case "guardafacturacion":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;		
				case "transferenciadoc":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;
				case "transferenciadoc_ip":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/facturacionpos/inicioFacturacion.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorFacturacionMifi
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarcliente":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;
				case "facturarclientemifi":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;
				case "buscarmifixrif":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;
					case "confirmarpagosmifi":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;	
				case "guardafacturacion":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;		
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/facturacionmifi/iniciofacturacionmifi.php');
			}else{
				return true;
			}
		}

	}



class EnrutadorPreparacionCliente
	{
		public function cargarVista($vista)
		{
			switch($vista):


				case "buscarpreparacion":
					include_once('view/Administracion/preparacionCliente/'.$vista.'.php');
					break;

				case "buscarclientes":
					include_once('view/Administracion/preparacionCliente/'.$vista.'.php');
					break;

				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/preparacionCliente/inicioPreparacionCliente.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorSerie
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntar":
					include_once('view/Serie/Seriales/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Serie/Seriales/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Serie/Seriales/'.$vista.'.php');
					break;
				case "buscarpreparacion":
					include_once('view/Serie/Seriales/'.$vista.'.php');
					break;
				case "buscarseriales":
					include_once('view/Serie/Seriales/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Serie/Seriales/inicioSerie.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorSimcards
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntar":
					include_once('view/Serie/Simcards/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Serie/Simcards/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Serie/Simcards/'.$vista.'.php');
					break;
				case "buscarpreparacion":
					include_once('view/Serie/Simcards/'.$vista.'.php');
					break;
				case "buscarseriales":
					include_once('view/Serie/Simcards/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Serie/Simcards/inicioSim.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorMifi
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntar":
					include_once('view/Serie/Mifi/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Serie/Mifi/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Serie/Mifi/'.$vista.'.php');
					break;
				case "buscarpreparacion":
					include_once('view/Serie/Mifi/'.$vista.'.php');
					break;
				case "buscarseriales":
					include_once('view/Serie/Mifi/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Serie/Mifi/inicioMifi.php');
			}else{
				return true;
			}
		}

	}


		class EnrutadorInventario
	{
		public function cargarVista($vista)
		{
			switch($vista):

				
				case "buscarinventario":
					include_once('view/Serie/Inventario/'.$vista.'.php');
					break;
				case "reportedetallado":
					include_once('view/Serie/Inventario/'.$vista.'.php');
					break;
				case "reportegeneral":
					include_once('view/Serie/Inventario/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Serie/Inventario/buscarinventario.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorTxt
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntar":
					include_once('view/Administracion/Txt/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Administracion/Txt/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Administracion/Txt/'.$vista.'.php');
					break;
				case "buscar":
					include_once('view/Administracion/Txt/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/Txt/inicioTxt.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorGestionComercialiazacion
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarcliente":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "buscarrecepcion":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "buscarrif":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "gestioncomercializacion":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "editar":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "activaregistro":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "transferenciadoc":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "transferenciadoc_ip":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "transferenciacompagos":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
					case "transferenciacompagos_ip":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
                    case "gestiontelefonica":
					include_once('view/comercializacion/gestion/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/comercializacion/gestion/inicioComercializacion.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorReporteAfiliado
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscargestion":
					include_once('view/Reportes/afiliado/'.$vista.'.php');
					break;
				case "buscarreporte":
					include_once('view/Reportes/afiliado/'.$vista.'.php');
					break;
				case "reportedetallado":
					include_once('view/Reportes/afiliado/'.$vista.'.php');
					break;
				case "adjuntar":
					include_once('view/Reportes/afiliado/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/Reportes/afiliado/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/afiliado/inicioGestion.php');
			}else{
				return true;
			}
		}

	}

		
class EnrutadorReporteAdministracion
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarcliente":
					include_once('view/Reportes/administracion/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Reportes/administracion/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/administracion/inicioadministracion.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorReporteERP
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarcliente":
					include_once('view/Reportes/erp/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Reportes/erp/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/erp/inicioerp.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorReporteMantenimientoPos
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarcliente":
					include_once('view/Reportes/mantenimiento/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Reportes/mantenimiento/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/mantenimiento/iniciomantenimiento.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorReporteEquipos
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarreporteequipos":
					include_once('view/Reportes/equipos/'.$vista.'.php');
					break;
				case "buscarreportefecha":
					include_once('view/Reportes/equipos/'.$vista.'.php');
					break;
				case "reporteequipos":
					include_once('view/Reportes/equipos/'.$vista.'.php');
					break;
				case "reporteequipospos":
					include_once('view/Reportes/equipos/'.$vista.'.php');
					break;			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/equipos/inicioequipos.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorReporteEjecutivo
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarcliente":
					include_once('view/Reportes/ejecutivos/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/Reportes/ejecutivos/'.$vista.'.php');
					break;
				case "ejecutivo":
					include_once('view/Reportes/ejecutivos/'.$vista.'.php');
					break;	
				case "reporteeje":
					include_once('view/Reportes/ejecutivos/'.$vista.'.php');
					break;			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/ejecutivos/inicioejecutivos.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReporteDiario
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "reportexbanco":
					include_once('view/Reportes/reportediario/'.$vista.'.php');
					break;
				case "reportetrabajo":
					include_once('view//Reportes/reportediario/'.$vista.'.php');
					break;
				case "reportexdiasdetalles":
					include_once('view/Reportes/reportediario/'.$vista.'.php');
					break;	
				case "reportexdiascomisisones": 
					include_once('view/Reportes/reportediario/'.$vista.'.php');
					break;			
				case "reportexdias": 
					include_once('view/Reportes/reportediario/'.$vista.'.php');
					break;	
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportediario/inicioreportediario.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReportecobro
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "peticionReporte":
					include_once('view/Reportes/reportecobro/'.$vista.'.php');
					break;
				
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportecobro/inicioreportcobro.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorReporteFacturasPOS
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarreporteequipos":
					include_once('view/Reportes/reportefactura/'.$vista.'.php');
					break;
				case "buscarreportefecha":
					include_once('view/Reportes/reportefactura/'.$vista.'.php');
					break;
				case "reporteequipos":
					include_once('view/Reportes/reportefactura/'.$vista.'.php');
					break;
					
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportefactura/inicioreportefactura.php');
			}else{
				return true;
			}
		}

	}
	
		class EnrutadorReporteEstatus
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "reportexbanco":
					include_once('view/Reportes/reporteestatus/'.$vista.'.php');
					break;
				case "generarreporteestatus":
					include_once('view//Reportes/reporteestatus/'.$vista.'.php');
					break;
				// case "reportexdiasdetalles":
				// 	include_once('view/Reportes/reportediario/'.$vista.'.php');
				// 	break;	
				// case "reporteeje": 
				// 	include_once('view/Reportes/ejecutivos/'.$vista.'.php');
				// 	break;			
				// case "reportexdias": 
				// 	include_once('view/Reportes/reportediario/'.$vista.'.php');
				// 	break;	
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteestatus/inicioreporteestatus.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReportegrupal
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "reportegrupal":
					include_once('view/Reportes/reportegrupal/'.$vista.'.php');
					break;
				case "reportegrupaldetalles":
					include_once('view/Reportes/reportegrupal/'.$vista.'.php');
					break;
			
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportegrupal/Inicioreportegrupal.php');
			}else{
				return true;
			}
		}

	}

		class EnrutadorReportePos
	{
		public function cargarVista($vista) 
		{
			switch($vista):
				case "reportetipopos":
					include_once('view/Reportes/reportepos/'.$vista.'.php');
					break;
	
				case "reportexposdetalles":
					include_once('view/Reportes/reportepos/'.$vista.'.php');
					break;
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportepos/Inicioreportepos.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReporteConexiones
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "reportexbanco":
					include_once('view/Reportes/reportediario/'.$vista.'.php');
					break;
				case "reportetrabajo":
					include_once('view//Reportes/reportediario/'.$vista.'.php');
					break;
				// case "ejecutivo":
				// 	include_once('view/Reportes/ejecutivos/'.$vista.'.php');
				// 	break;	
				// case "reporteeje": 
				// 	include_once('view/Reportes/ejecutivos/'.$vista.'.php');
				// 	break;			
				case "reportexdias": 
					include_once('view/Reportes/reportediario/'.$vista.'.php');
					break;	
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteconexion/reporteconexionesejecutivo.php');
			}else{
				return true;
			}
		}

	}

			class EnrutadorFacturaPOS
	{
		public function cargarVista($vista)
		{
			switch($vista):

				
				case "busquedatotalfactura":
					include_once('view/Reportes/reportefactura/'.$vista.'.php');
					break;
				case "busquedaxnumfactura":
					include_once('view/Reportes/reportefactura/'.$vista.'.php');
					break;
				case "reportegeneral":
					include_once('view/Reportes/reportefactura/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportefactura/inicioreportefactura.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorServicioMantenimiento
{
	public function cargarVista($vista)
	{
		switch($vista):

			case "buscarmantenimiento":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;
			case "buscarrif":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;
			case "adjuntar":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;	
			case "transferencia":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;		
			case "recepcionmantenimiento":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;		
			case "cargaindividual":
				include_once('view/ServicioMantenimiento/CargarMantenimiento/'.$vista.'.php');
				break;		
			default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/ServicioMantenimiento/iniciomantenimiento.php');
		}else{
			return true;
		}
	}

}


class EnrutadorClientesPotenciales
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntar":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;
				case "buscarrecepcion":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;
				case "transferencia":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;
				case "transferencia2":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;
				case "transferencia3":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;
				case "transferencia4":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;	
						case "transferencia5":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;	
				case "buscarcliente":
					include_once('view/ClientePotencial/recepcion/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ClientePotencial/recepcion/inicioClientePotencial.php');
			}else{
				return true;
			}
		}

	}
class EjecutivoVenta
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "CargarEjecutivo":
					include_once('view/ejecutivo/'.$vista.'.php');
					break;

				case "datobancoasignado":
					include_once('view/ejecutivo/'.$vista.'.php');
					break;

				case "EditarEjecutivo":
					include_once('view/ejecutivo/'.$vista.'.php');
					break;

				default:
				include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Ejecutivo/inicioEjecutivo.php');
			}else{
				return true;
			}
		}

	}

	class EjecutivoBancoo
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "InicioEjecutivoxBanco":
					include_once('view/ejecutivo/'.$vista.'.php');
					break;

				case "agregarbancoejec":
					include_once('view/ejecutivo/'.$vista.'.php');
					break;

				default:
				include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Ejecutivo/inicioejecbanco.php');
			}else{
				return true;
			}
		}

	}

class GestionPos
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "gestionpos":
					include_once('view/Gestion/POS/'.$vista.'.php');
					break;
				case "reportedetallado":
					include_once('view/Gestion/POS/'.$vista.'.php');
					break;
				case "reportedetalladopos":
					include_once('view/Gestion/POS/'.$vista.'.php');
					break;	
				default:
				include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Gestion/POS/gestionpos.php');
			}else{
				return true;
			}
		}

	}

		class EnrutadorGestionMantenimiento
	{
		public function cargarVista($vista)
		{
			switch($vista):


				case "gestionmantenimiento":
					include_once('view/Gestion/Mantenimiento/'.$vista.'.php');
					break;

				default:
				include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Gestion/Mantenimiento/gestionmantenimiento.php');
			}else{
				return true;
			}
		}

	}
	class ServicioTecnico
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "VerDatosServTec":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "GestionarTecnico":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "CargarNuevoTecnico":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "Editartecnico":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "habilitartecnico":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				default:
				include_once('view/error.php');

			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ServicioTecnico/GestionarTecnico.php');
			}else{
				return true;
			}
		}

	}
class GestionarServicioTecnico
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarservitec":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "buscarrecepcion":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;	

				case "buscarrifservicio":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;	

				case "asignarserial":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "CargarNuevoTecnico":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "datoservitec":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				case "buscarInstalar":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				// case "datosserviciosafiliado":
				// 	include_once('view/ServicioTecnico/'.$vista.'.php');
				// 	break;
				case "buscarInstPOS":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				case "datoserialpos":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				case "buscarrifserviciogestion":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				default:
				include_once('view/error.php');

			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ServicioTecnico/InicioServicioTecnico.php');
			}else{
				return true;
			}
		}

	}

	class ConfiguraciondePOS
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarservitec":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "buscarconfigpos":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;	

				case "buscarrifservicio":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;	

				case "asignarserial":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "CargarNuevoTecnico":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;

				case "datoservitec":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				case "datosserviciosafiliado":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				case "datoserialpos":
					include_once('view/ServicioTecnico/'.$vista.'.php');
					break;
				default:
				include_once('view/error.php');

			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ServicioTecnico/inicioconfigpos.php');
			}else{
				return true;
			}
		}

	}


class DataServicioTecnico
{
	public function cargarVista($vista)
	{
		switch($vista):

			case "buscarcliente":
				include_once('view/ServicioTecnico/data/'.$vista.'.php');
				break;
			case "buscarrecepcion":
				include_once('view/ServicioTecnico/data/'.$vista.'.php');
				break;
			case "datacliente":
				include_once('view/ServicioTecnico/data/'.$vista.'.php');
				break;
			case "buscarrif":
				include_once('view/ServicioTecnico/data/'.$vista.'.php');
				break;
			default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/ServicioTecnico/data/iniciodata.php');
		}else{
			return true;
		}
	}

}

class EnrutadorAcuerdosServicios
{
	public function cargarVista($vista)
	{
		switch($vista):

			case "buscarcliente":
				include_once('view/AcuerdoServicio/Servicios/'.$vista.'.php');
				break;
			case "buscarrecepcion":
				include_once('view/AcuerdoServicio/Servicios/'.$vista.'.php');
				break;
			case "datacliente":
				include_once('view/AcuerdoServicio/Servicios/'.$vista.'.php');
				break;
			case "buscarrif":
				include_once('view/AcuerdoServicio/Servicios/'.$vista.'.php');
				break;
			default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/AcuerdoServicio/Servicios/inicioAcuerdo.php');
		}else{
			return true;
		}
	}

}

class EnrutadorDashboard
{
	public function cargarVista($vista)
	{
		switch($vista):

			case "dashboard":
				include_once('view/dashboard/user/'.$vista.'.php');
				break;
			
				default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/dashboard/user/dashboard.php');
		}else{
			return true;
		}
	}

}

class EnrutadorDocumentacion
{
	public function cargarVista($vista)
	{
		switch($vista):

			case "documentacion":
				include_once('view/manuales/'.$vista.'.php');
				break;
			
				default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/Documentacion/manuales.php');
		}else{
			return true;
		}
	}

}

class EnrutadorCaptacionCliente
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarcliente":
					include_once('view/CaptacionCliente/'.$vista.'.php');
					break;
					case "buscarrecepcion":
					include_once('view/CaptacionCliente/'.$vista.'.php');
					break;
					case "registrosxasignar":
					include_once('view/CaptacionCliente/'.$vista.'.php');
					break;
					case "iniciocaptacioncliente":
					include_once('view/CaptacionCliente/'.$vista.'.php');
					break;
					case "aceptacioncoord":
					include_once('view/CaptacionCliente/'.$vista.'.php');
					break;
					case "aceptacion":
					include_once('view/CaptacionCliente/'.$vista.'.php');
					break;

				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/CaptacionCliente/iniciocaptacioncliente.php');
			}else{
				return true;
			}
		}

	}

		class EnrutadorReporteEstatusGeneral
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreporteestatus":
					include_once('view/Reportes/reporteestatusgeneral/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteestatusgeneral/inicioreporteestatus.php');
			}else{
				return true;
			}
		}

	}

	// 

	class EnrutadorReporteEstatusUbii
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "generarreporteestatusfactura":
					include_once('view/Reportes/reporteestatusubii/'.$vista.'.php');
					break;	
				default:
					include_once('view/error.php');
			endswitch;

		}


		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteestatusubii/inicioreporteestatus.php');
			}else{
				return true;
			}
		}

	}

	
		class EnrutadorReporteResultadoGestion
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreporteestatus":
					include_once('view/Reportes/reporteresultadogestion/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteresultadogestion/inicioreporteestatus.php');
			}else{
				return true;
			}
		}

	}
	class ReporteParametrizacion
	{
	public function cargarVista($vista)
	{
		switch($vista):

			case "porparametrizar":
				include_once('view/Reportes/reporteparametrizacion/'.$vista.'.php');
				break;
			
				default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/Reportes/reporteparametrizacion/InicioReporteParametrizacion.php');
		}else{
			return true;
		}
	}

}
class ReporteCaptacionEjecutivo
	{
	public function cargarVista($vista)
	{
		switch($vista):

			case "reportexejecutivo":
				include_once('view/Reportes/reportecaptacion/'.$vista.'.php');
				break;
			
				default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/Reportes/reportecaptacion/reportexejecutivocap.php');
		}else{
			return true;
		}
	}

}



class EnrutadorEquiposDisponibles
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "busquedaequipos":
					include_once('view/Serie/Reporte/'.$vista.'.php');
					break;
				
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Serie/Reporte/inicioequiposdisponibles.php');
			}else{
				return true;
			}
		}

	}

			class EnrutadorVerFacturaMifi
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "ver_factura_mifi":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;	

				case "factura_anterior_mifi":
					include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
					break;		
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/facturacionmifi/'.$vista.'.php');
			}else{
				return true;
			}
		}

	}



	class EnrutadorVerFactura
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "ver_factura":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;	

				case "factura_anterior":
					include_once('view/Administracion/facturacionpos/'.$vista.'.php');
					break;		
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/facturacionpos/'.$vista.'.php');
			}else{
				return true;
			}
		}

	}

		class EnrutadorReporteGeneralDeMifi
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "reportegeneraldemifis":
					include_once('view/Reportes/reportegeneraldemifi/'.$vista.'.php');
					break;
					default:
					include_once('view/error.php');
			endswitch;
		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportegeneraldemifi/inicioreportegeneral.php');
			}else{
				return true;
			}
		}

	}

			class EnrutadorServiciotecnicoMifi
			{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarconfigmifi":
					include_once('view/ServicioTecnico/GestionMifi/'.$vista.'.php');
					break;	

				case "buscarserviciomifi":
					include_once('view/ServicioTecnico/GestionMifi/'.$vista.'.php');
					break;

				case "buscarrifmifiservicio":
					include_once('view/ServicioTecnico/GestionMifi/'.$vista.'.php');
					break;	

				case "procesarconfigmifi":
					include_once('view/ServicioTecnico/GestionMifi/'.$vista.'.php');
					break;
				default:
				include_once('view/error.php');
			endswitch;
		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ServicioTecnico/GestionMifi/inicioserviciotecnicomifi.php');
			}else{
				return true;
			}
		}
	}

	
		class EnrutadorReporteGestiondeMifi
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreporteestatusmifi":
					include_once('view/Reportes/reportedegestionmifi/'.$vista.'.php');
					break;
					default:
					include_once('view/error.php');
			endswitch;
		}
		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportedegestionmifi/inicioreportemifi.php');
			}else{
				return true;
			}
		}
	}

	class EnrutadorServiciotecnicoRosal
			{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscargestion":
					include_once('view/ServicioTecnico/GestionRosal/'.$vista.'.php');
					break;	

				case "buscargestiondetallada":
					include_once('view/ServicioTecnico/GestionRosal/'.$vista.'.php');
					break;

				case "buscargestionxrif":
					include_once('view/ServicioTecnico/GestionRosal/'.$vista.'.php');
					break;	

				case "procesargestion":
					include_once('view/ServicioTecnico/GestionRosal/'.$vista.'.php');
					break;
				default:
				include_once('view/error.php');
			endswitch;
		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ServicioTecnico/GestionRosal/inicioserviciotecnicorosal.php');
			}else{
				return true;
			}
		}
	}



			class EnrutadorReporteResultadoGestionRosal
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreportegestionrosal":
					include_once('view/Reportes/reporteresultadogestionrosal/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteresultadogestionrosal/inicioreportegestionrosal.php');
			}else{
				return true;
			}
		}

	}


		class EnrutadorReporteSustituciones
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreportesustituciones":
					include_once('view/Reportes/reportesustituciones/'.$vista.'.php');
					break;

				case "reportedetalladosustitucion":
					include_once('view/Reportes/reportesustituciones/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportesustituciones/inicioreporte.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorEncuesta
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "iniciobuscarencuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;

				case "buscarbancoencuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;

				case "buscarfechaencuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;

				case "buscarbancfechaencuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;

				case "DatosEncuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;

				case "buscarrifencuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;

				case "activarencuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;
				case "iniciopersonasreferidas":
					include_once('view/Encuesta/'.$vista.'.php');
					break;
				case "buscarclienteencuesta":
					include_once('view/Encuesta/'.$vista.'.php');
					break;
				case "personaref":
					include_once('view/Encuesta/'.$vista.'.php');
					break;
				default:
				include_once('view/error.php');

			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Encuesta/iniciobuscarencuesta.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorReporteEncuesta
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "buscarpreguntasa":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "buscarpreguntasb":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "buscarpreguntasc":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "iniciobuscarestatus":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "buscarestado":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteencuesta/inciobuscarpreguntas.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorReporteEncuestaEstatus
	{
		public function cargarVista($vista)
		{
			switch($vista):
			
				case "iniciobuscarestatus":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "buscarestado":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteencuesta/iniciobuscarestatus.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorReporteEncuestaRegistros
	{
		public function cargarVista($vista)
		{
			switch($vista):
			
				case "buscarregistros":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "iniciobuscarregistros":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "buscarregistrosrif": 
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "DatosEncuestaReporte": 
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "reportehistorialencuesta": 
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;		
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteencuesta/iniciobuscarregistros.php');
			}else{
				return true;
			}
		}

	}

class PersonasReferidasEncuesta
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "iniciopersonasreferidas":
					include_once('view/Encuesta/'.$vista.'.php');
					break;

				case "buscarpersonasref":
					include_once('view/Encuesta/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Encuesta/iniciopersonasreferidas.php');
			}else{
				return true;
			}
		}

	}

	class ReporteVendedoresMensual
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "iniciovendedoresmensual":
					include_once('view/Reportes/VendedoresMensual/'.$vista.'.php');
					break;

				case "vendedoresmensual":
					include_once('view/Reportes/VendedoresMensual/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/VendedoresMensual/iniciovendedoresmensual.php');
			}else{
				return true;
			}
		}

	}

	class ReportePersonasReferidas
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarclientepersonref":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "inicioreportepersonref":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "reportedetallepersonasref":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				case "buscarrifpersonarefreporte":
					include_once('view/Reportes/reporteencuesta/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteencuesta/inicioreportepersonref.php');
			}else{
				return true;
			}
		}

	}

	class ActivacionEquipos
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "inicioactivacionequipos":
					include_once('view/Activacion/'.$vista.'.php');
					break;
				case "buscarbancoactivacion":
					include_once('view/Activacion/'.$vista.'.php');
				break;
				case "gestionaractivacion":
					include_once('view/Activacion/'.$vista.'.php');
				case "gestionaractivacionserial":
					include_once('view/Activacion/'.$vista.'.php');
						
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Activacion/inicioactivacionequipos.php');
			}else{
				return true;
			}
		}

	}

			class EnrutadorReporteEquiposActivos
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreporteequipos":
					include_once('view/Reportes/reporteequiposactivos/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteequiposactivos/inicioreporteequipos.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorReporteEquiposInstalados
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreporteequiposinstalados":
					include_once('view/Reportes/reporteequiposinstalados/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteequiposinstalados/inicioreporteequipos.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReportePercance
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreportepercance":
					include_once('view/Reportes/reporteequipospercance/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteequipospercance/inicioreportepercance.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorServicioDomiciliacion
{
	public function cargarVista($vista)
	{
		switch($vista):

			case "buscarmantenimiento":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;
			case "buscarrif":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;
			case "adjuntar":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;	
			case "transferencia1":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;	
			case "recepcionmantenimiento":
				include_once('view/ServicioMantenimiento/'.$vista.'.php');
				break;				
			default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/ServicioMantenimiento/iniciomantenimiento.php');
		}else{
			return true;
		}
	}
}



	class EnrutadorReporteDomiciliacion
{
	public function cargarVista($vista)
	{
		switch($vista):

			case "generarreporte":
				include_once('view/ServicioMantenimiento/Reporte/'.$vista.'.php');
				break;
			
			case "recepcionmantenimiento":
				include_once('view/ServicioMantenimiento/Reporte/'.$vista.'.php');
				break;				
			default:
				include_once('view/error.php');
		endswitch;

	}

	public function validarGet($variable){
		if (empty($variable))
		{
			include_once('view/ServicioMantenimiento/Reporte/inicioreportedomic.php');
		}else{
			return true;
		}
	}
}


	class EnrutadorControlAccesos
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntarcontrolaccesos":
					include_once('view/ControlAcceso/'.$vista.'.php');
					break;
				case "buscaracceso":
					include_once('view/ControlAcceso/'.$vista.'.php');
				break;
				case "transferenciaacceso":
					include_once('view/ControlAcceso/'.$vista.'.php');
				break;
						
			endswitch;

		}
		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ControlAcceso/adjuntarcontrolaccesos.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorControlAccesosBuscar
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "iniciobuscaracceso":
					include_once('view/ControlAcceso/'.$vista.'.php');
					break;
				case "buscarcontrolacceso":
					include_once('view/ControlAcceso/'.$vista.'.php');
					break;
				case "asignarferiados":
					include_once('view/ControlAcceso/'.$vista.'.php');
					break;
						
			endswitch;

		}
		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ControlAcceso/iniciobuscaracceso.php');
			}else{
				return true;
			}
		}

	}

class EnrutadorAsignarFeriados
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "asignarferiados":
					include_once('view/ControlAcceso/'.$vista.'.php');
					break;
						
			endswitch;

		}
		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ControlAcceso/asignarferiados.php');
			}else{
				return true;
			}
		}

	}


class EnrutadorVisitaTecnica
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarregistrovisita":
					include_once('view/VisitaTecnica/'.$vista.'.php');
					break;

				case "detallesvisitatecnica":
					include_once('view/VisitaTecnica/'.$vista.'.php');
					break;	

				case "buscarregistrovisitaxrif":
					include_once('view/VisitaTecnica/'.$vista.'.php');
					break;	

				case "buscarvisitaxfecha":
					include_once('view/VisitaTecnica/'.$vista.'.php');
					break;	

				case "buscargestionxfechas":
					include_once('view/VisitaTecnica/'.$vista.'.php');
					break;	

			endswitch;

		}
		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/VisitaTecnica/iniciovisitatecnica.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReporteVisitaTecnica
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarreportevisita":
					include_once('view/VisitaTecnica/'.$vista.'.php');
					break;

				case "buscarvisitaxfecha":
					include_once('view/VisitaTecnica/'.$vista.'.php');
					break;		
				
			endswitch;

		}
		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/VisitaTecnica/inicioreportevisitatecnica.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReporteGestiones
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreportegestiones":
					include_once('view/Reportes/Reportegestiones/'.$vista.'.php');
					break;

				case "reportedetalladogestiones":
					include_once('view/Reportes/Reportegestiones/'.$vista.'.php');
					break;

				case "reportedetalladogestionesrif":
					include_once('view/Reportes/Reportegestiones/'.$vista.'.php');
					break;

				case "generarreportegestionesrif":
					include_once('view/Reportes/Reportegestiones/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/Reportegestiones/inicioreportegestiones.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReporteEstatusDomiciliacion
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "generarreporte":
					include_once('view/Reportes/reportedomiciliacion/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reportedomiciliacion/inicioreporte.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReporteClientesProfit
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "busquedaclientes":
					include_once('view/Reportes/reporteclienteprofit/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteclienteprofit/inicioreportecliente.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReportePlantillaProfit
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "busquedaplantilla":
					include_once('view/Reportes/reporteplantillaprofit/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteplantillaprofit/inicioreporteplantilla.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorInventarioBanesco
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "inicioinventariobanesco":
					include_once('view/modulobanesco/inventario/'.$vista.'.php');
					break;

				case "cargaappllaves":
					include_once('view/modulobanesco/inventario/'.$vista.'.php');
					break;

				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/modulobanesco/inventario/inicioinventariobanesco.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorReporteBanesco
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "busquedaporestatus":
					include_once('view/AfiliadoBanesco/'.$vista.'.php');
					break;
				case "inicioreportebanesco":
					include_once('view/AfiliadoBanesco/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/AfiliadoBanesco/inicioreporte.php');
			}else{
				return true;
			}
		}

	}

		class EnrutadorAfiliadoBanesco
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "transferencia":
					include_once('view/AfiliadoBanesco/'.$vista.'.php');
					break;
				case "inicioreportebanesco":
					include_once('view/AfiliadoBanesco/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/AfiliadoBanesco/inicioafiliado.php');
			}else{
				return true;
			}
		}

	}


		class EnrutadorEquiposBanesco
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "busqueda_equipos":
					include_once('view/EquiposBanesco/equipos/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/EquiposBanesco/equipos/inicioequipos.php');
			}else{
				return true;
			}
		}

	}


	class EnrutadorTerminalBanesco
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "registrosadesaf":
					include_once('view/desaf-desincorpBanesco/desaf/'.$vista.'.php');
					break;	
				case "registrosadesaftodos":
					include_once('view/desaf-desincorpBanesco/desaf/'.$vista.'.php');
					break;			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/desaf-desincorpBanesco/desaf/inicioterminal.php');
			}else{
				return true;
			}
		}

	}


		class EnrutadorReporteInventarioBanesco
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "iniciobanesco_reporte":
					include_once('view/modulobanesco/inventario/'.$vista.'.php');
					break;

				case "buscarreportebanesco":
					include_once('view/modulobanesco/inventario/'.$vista.'.php');
					break;
				case "buscarreportexLote":
					include_once('view/modulobanesco/inventario/'.$vista.'.php');
					break;	
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/modulobanesco/inventario/iniciobanesco_reporte.php');
			}else{
				return true;
			}
		}

	}


		class EnrutadorReporteBanescoGeneral
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "inicioreporte_general":
					include_once('view/modulobanesco/inventario/'.$vista.'.php');
					break;

				case "buscarreportegeneral":
					include_once('view/modulobanesco/inventario/'.$vista.'.php');
					break;
			
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/modulobanesco/inventario/inicioreporte_general.php');
			}else{
				return true;
			}
		}

	}	

	class EnrutadorPagosxAprobar
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "buscarpagosxaprobar":
					include_once('view/Administracion/pagosxaprobar/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Administracion/pagosxaprobar/buscarpagosxaprobar.php');
			}else{
				return true;
			}
		}

	}

		class EnrutadorRiesgoMedido
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "inicioriesgo_medido":
					include_once('view/ReportesRiesgoMedido/'.$vista.'.php');
					break;
				case "riesgo_medido":
					include_once('view/ReportesRiesgoMedido/'.$vista.'.php');
					break;
				case "mostrarinfopagos":
					include_once('view/ReportesRiesgoMedido/'.$vista.'.php');
					break;
				case "iniciopreciospos":
					include_once('view/ReportesRiesgoMedido/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ReportesRiesgoMedido/inicioriesgo_medido.php');
			}else{
				return true;
			}
		}

	}

		class EnrutadorRiesgoCargaParametros
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "iniciocargaparametros":
					include_once('view/ReportesRiesgoMedido/'.$vista.'.php');
					break;
				case "cargaparametros":
					include_once('view/ReportesRiesgoMedido/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ReportesRiesgoMedido/iniciocargaparametros.php');
			}else{
				return true;
			}
		}

	}

	class EnrutadorPreciospos
	{
		public function cargarVista($vista)
		{
			switch($vista):
				case "iniciopreciospos":
					include_once('view/ReportesRiesgoMedido/gestion/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/ReportesRiesgoMedido/gestion/iniciopreciospos.php');
			}else{
				return true;
			}
		}

	}
	class EnrutadorReingresoEquipos
		{			
			public function cargarVista($vista)
			{
				switch($vista):
					case "buscarequipo":
						include_once('view/modulobanesco/reingreso/'.$vista.'.php');
						break;
					default:
						include_once('view/error.php');
				endswitch;
			}

			public function validarGet($variable){
				if (empty($variable))
				{
					include_once('view/modulobanesco/reingreso/inicio_reingreso.php');
				}else{
					return true;
				}
			}

		}

	class EnrutadorExpediente
		{			
			public function cargarVista($vista)
			{
				switch($vista):
					case "buscarequipo":
						include_once('view/Administracion/aprobacion_backoffice/'.$vista.'.php');
						break;
					default:
						include_once('view/error.php');
				endswitch;
			}

			public function validarGet($variable){
				if (empty($variable))
				{
					include_once('view/Administracion/aprobacion_backoffice/inicio_aprob.php');
				}else{
					return true;
				}
			}

		}


	class EnrutadorDocumentos
		{			
			public function cargarVista($vista)
			{
				switch($vista):
					case "buscarequipo":
						include_once('view/Administracion/verificacion_documentos/'.$vista.'.php');
						break;
					default:
						include_once('view/error.php');
				endswitch;
			}
			public function validarGet($variable){
				if (empty($variable))
				{
					include_once('view/Administracion/verificacion_documentos/inicio_verificacion.php');
				}else{
					return true;
				}
			}

		}


class EnrutadorDesaf
	{
		public function cargarVista($vista)
		{
			switch($vista):

				case "adjuntar":
					include_once('view/Desafiliacion_masiva/'.$vista.'.php');
					break;
				case "buscarregistromasivo":
					include_once('view/Desafiliacion_masiva/'.$vista.'.php');
					break;
					case "buscarregistromasivoglobal":
						include_once('view/Desafiliacion_masiva/'.$vista.'.php');
						break;
				case "transferencia":
					include_once('view/Desafiliacion_masiva/'.$vista.'.php');
					break;
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Desafiliacion_masiva/inicioDesaf.php');
			}else{
				return true;
			}
		}

	}


	class EnrutadorReporteFacturacion
	{
		public function cargarVista($vista)
		{
			switch($vista):

				
				case "generarreporteestatusfactura":
					include_once('view/Reportes/reporteestatusfactura/'.$vista.'.php');
					break;
				
				default:
					include_once('view/error.php');
			endswitch;

		}

		public function validarGet($variable){
			if (empty($variable))
			{
				include_once('view/Reportes/reporteestatusfactura/inicioreportefactura.php');
			}else{
				return true;
			}
		}

	}	

?>




