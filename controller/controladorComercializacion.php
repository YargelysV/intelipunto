<?php

include_once("model/gestionComercializacion.php");

class ControladorComercializacion{

	private $gestioncomercial;

	public function __construct(){
		$this->gestioncomercial= new Comercializacion();
	}

    public function spvervendedor($banco){
    	$this->gestioncomercial->set("PrefijoBanco",$banco);
    	$datos=$this->gestioncomercial->spvervendedor();
    	return $datos;
    }
	public function estatusconfirpagoss($id_registro){
		$this->gestioncomercial->set("id_registro",$id_registro);
		$datos=$this->gestioncomercial->estatusconfirpagoss();
		return $datos;
		}
    public function sp_actividad($id_registro,$activo,$usuario){
		$this->gestioncomercial->set("id_registro",$id_registro);
		$this->gestioncomercial->set("activo",$activo);
		$this->gestioncomercial->set("usuario",$usuario);
		$datos=$this->gestioncomercial->sp_actividad();
		return $datos;
	}
	public function sp_verafiliadocomercializacion($id_registro){
		$this->gestioncomercial->set("id_registro",$id_registro);
		$datos=$this->gestioncomercial->sp_verafiliadocomercializacion();
		return $datos;
	}
	public function sp_vernotificacionespago($id_registro){
		$this->gestioncomercial->set("id_registro",$id_registro);
		$datos=$this->gestioncomercial->sp_vernotificacionespago();
		return $datos;
	}
	public function sp_verdescripcionmotivos($detalledesaprob){
		$this->gestioncomercial->set("detalledesaprob",$detalledesaprob);
		$datos=$this->gestioncomercial->sp_verdescripcionmotivos();
		return $datos;
	}
		//ver estatus gestion
	public function sp_verestatusgestion($operacion,$id_registro,$namearchivo){
		$this->gestioncomercial->set("Operacion",$operacion);
		$this->gestioncomercial->set("id_registro",$id_registro);
		$this->gestioncomercial->set("namearchivo",$namearchivo);
		$datos=$this->gestioncomercial->sp_verestatusgestion();
		return $datos;
	}

	public function sp_verestatusgestioncontacto($id_registro){
		$this->gestioncomercial->set("id_registro",$id_registro);
		$datos=$this->gestioncomercial->sp_verestatusgestioncontacto();
		return $datos;
	}	

	public function spinventarioxcliente($cliente,$anio){
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("anio",$anio);
		$datos=$this->gestioncomercial->spinventarioxcliente();
		return $datos;
	}

    public function sp_estatussolpos($operacion){
    	$this->gestioncomercial->set("Operacion",$operacion);
    	$datos=$this->gestioncomercial->sp_estatussolpos();
    	return $datos;
    }
    public function sp_estatusinteresado(){
    	$datos=$this->gestioncomercial->sp_estatusinteresado();
    	return $datos;
    }
    // public function sp_estatusinteresado(){
    // 	$sql="select * from sp_estatusinteresado()";
    // 	$resultado=$this->con->consultaRetorno($sql);
    // 	return $resultado;
    // }

//* funciones de abajo?*/

	public function spdetalleclientexinventario($cliente, $producto, $tarjeta, $bin){
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("producto",$producto);
		$this->gestioncomercial->set("tarjeta",$tarjeta);
		$this->gestioncomercial->set("bin",$bin);
		$datos=$this->gestioncomercial->spdetalleclientexinventario();
		return $datos;
	}

	public function spdetalleclientexinvprotar($cliente, $producto, $tarjeta, $anio, $bin){
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("producto",$producto);
		$this->gestioncomercial->set("tarjeta",$tarjeta);
		$this->gestioncomercial->set("anio",$anio);
		$this->gestioncomercial->set("bin",$bin);
		$datos=$this->gestioncomercial->spdetalleclientexinvprotar();
		return $datos;
	}

	public function spdetalleclientexinvporfecha($cliente, $producto, $tarjeta, $anio, $bin, $fechadesde, $fechahasta){
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("producto",$producto);
		$this->gestioncomercial->set("tarjeta",$tarjeta);
		$this->gestioncomercial->set("anio",$anio);
		$this->gestioncomercial->set("bin",$bin);
		$this->gestioncomercial->set("fechadesde",$fechadesde);
		$this->gestioncomercial->set("fechahasta",$fechahasta);
		$datos=$this->gestioncomercial->spdetalleclientexinvporfecha();
		return $datos;
	}

	public function spreprocesosinventario($idcliente, $fechadejob, $numerojob, $producto){
		$this->gestioncomercial->set("cliente",$idcliente);
		$this->gestioncomercial->set("fechadejob",$fechadejob);
		$this->gestioncomercial->set("numerojob",$numerojob);
		$this->gestioncomercial->set("producto",$producto);
		$datos=$this->gestioncomercial->spreprocesosinventario();
		return $datos;
	}

	public function sp_banco($cliente){
		$this->gestioncomercial->set("cliente",$cliente);
		$datos=$this->gestioncomercial->sp_banco();
		return $datos;
	}

	public function sp_perfilgeodelbanco($cliente){
		$this->gestioncomercial->set("cliente",$cliente);
		$datos=$this->gestioncomercial->sp_perfilgeodelbanco();
		return $datos;
	}


	public function sp_buscarclientecomercializacion($cliente, $fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador, $coordinadordos){
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("FechaRecepcionArchivo",$fecha);
		$this->gestioncomercial->set("origen",$origen);
		$this->gestioncomercial->set("tipousuario",$tipousuario);
		$this->gestioncomercial->set("ejecutivo",$ejecutivo);
		$this->gestioncomercial->set("usuario",$usuario);
		$this->gestioncomercial->set("coordinador",$coordinador);
		$this->gestioncomercial->set("coordinadordos",$coordinadordos);
		$datos=$this->gestioncomercial->sp_buscarclientecomercializacion();
		return $datos;
	}

	public function verclientecomercializacionbuscar ($banco, $fecha, $namearchivo, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos)
	{
		$this->gestioncomercial->set("PrefijoBanco",$banco);
		$this->gestioncomercial->set("FechaRecepcionArchivo",$fecha);
		$this->gestioncomercial->set("namearchivo",$namearchivo);
		$this->gestioncomercial->set("tipousuario",$tipousuario);
		$this->gestioncomercial->set("ejecutivo",$ejecutivo);
		$this->gestioncomercial->set("usuario",$usuario);
		$this->gestioncomercial->set("coordinador",$coordinador);
		$this->gestioncomercial->set("coordinadordos",$coordinadordos);
		$datos=$this->gestioncomercial->verclientecomercializacionbuscar();
		return $datos;
	}
	public function sp_buscarclientecomercializacionRif($rif,$tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
			$this->gestioncomercial->set("Rif",$rif);
			$this->gestioncomercial->set("tipousuario",$tipousuario);
			$this->gestioncomercial->set("ejecutivo",$ejecutivo);
			$this->gestioncomercial->set("usuario",$usuario);
			$this->gestioncomercial->set("coordinador",$coordinador);
			$this->gestioncomercial->set("coordinadordos",$coordinadordos);
			$datos=$this->gestioncomercial->sp_buscarclientecomercializacionRif();
			return $datos;
	}
	public function sp_registrosactivos($id_registro){
			$this->gestioncomercial->set("id_registro",$id_registro);
			$datos=$this->gestioncomercial->sp_registrosactivos();
			return $datos;
	}

	public function sp_dirinstalacioneditada($coddocumento,$e_namearchivo){
		$this->gestioncomercial->set("coddocumento",$coddocumento);
		$this->gestioncomercial->set("namearchivo",$e_namearchivo);
		$datos=$this->gestioncomercial->sp_dirinstalacioneditada();
		return $datos;
	}

    //ver
	public function sp_vistadireccioninstalacioneditada($coddocumento,$e_prefijo,$e_namearchivo){
		$this->gestioncomercial->set("coddocumento",$coddocumento);
		$this->gestioncomercial->set("e_prefijo",$e_prefijo);
		$this->gestioncomercial->set("namearchivo",$e_namearchivo);
		$datos=$this->gestioncomercial->sp_vistadireccioninstalacioneditada();
		return $datos;
	}

	public function spbanco($cliente){
		$this->gestioncomercial->set("cliente",$cliente);
		$datos=$this->gestioncomercial->spbanco();
		return $datos;
	}

	public function insertar_afiliado($consecutivo, $fechaenvio, $cliente, $PrefijoBanco, $tipopv, $cantpv, $tipolinea, $tipomodelonegocio, $tipcliente, $razonsocial, $rif, $formapago, $transferencia, $afiliado, $usuario){
		$this->gestioncomercial->set("Consecutivo",$consecutivo);
		$this->gestioncomercial->set("fechaenvio",$fechaenvio);
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("PrefijoBanco",$PrefijoBanco);
		$this->gestioncomercial->set("TipoPOS",$tipopv);
		$this->gestioncomercial->set("cantpv",$cantpv);
		$this->gestioncomercial->set("tipolinea",$tipolinea);
		$this->gestioncomercial->set("tipomodelonegocio",$tipomodelonegocio);
		$this->gestioncomercial->set("tipcliente",$tipcliente);
		$this->gestioncomercial->set("razonsocial",$razonsocial);
		$this->gestioncomercial->set("Rif",$rif);
		$this->gestioncomercial->set("gcFormapago",$formapago);
		$this->gestioncomercial->set("transferencia",$transferencia);
		$this->gestioncomercial->set("NombreAfiliado",$afiliado);
		$this->gestioncomercial->set("usuario",$usuario);
		$datos=$this->gestioncomercial->insertar_afiliado();
		return $datos;
	}

	public function verclientescomercializacion ()
	{
	$datos=$this->gestioncomercial->verclientescomercializacion();
	return $datos;
	}

	public function verafiliadosasignados ()
	{
	$datos=$this->gestioncomercial->verafiliadosasignados();
	return $datos;
	}

	public function verafiliadosasignadosdetalle ($afiliado)
	{
	$this->gestioncomercial->set("NombreAfiliado",$afiliado);
	$datos=$this->gestioncomercial->verafiliadosasignadosdetalle();
	return $datos;
	}

	
// seoarador pa no olvidar
public function spinsertardocumentos($idregistro,$directorio,$afiliado,$usuario,$eexp){
	//gestioncomercial
	$this->gestioncomercial->set("idregistro",$idregistro);
	$this->gestioncomercial->set("directorio",$directorio);
	$this->gestioncomercial->set("afiliado",$afiliado);
	$this->gestioncomercial->set("usuario",$usuario);
	$this->gestioncomercial->set("eexp",$eexp);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->gestioncomercial->spinsertardocumentos();
	return $datos;
	}
//
// separador pa no olvidar
public function spinsertarhistorial($idregistro,$afiliado,$usuario,$mensaje,$detalles){
	//gestioncomercial
	$this->gestioncomercial->set("idregistro",$idregistro);
	$this->gestioncomercial->set("afiliado",$afiliado);
	$this->gestioncomercial->set("usuario",$usuario);
	$this->gestioncomercial->set("mensaje",$mensaje);
	$this->gestioncomercial->set("detalles",$detalles);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->gestioncomercial->spinsertarhistorial();
	return $datos;
	}
//
public function sp_reporteriesgomedidoadpins2($id_registro){
	$this->gestioncomercial->set("id_registro",$id_registro);
	$datos=$this->gestioncomercial->sp_reporteriesgomedidoadpins2();
	return $datos;
	}
	// 
	
	public function sp_vercostomantenimiento($cliente,$coddocumento,$namearch,$id_registro){
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("coddocumento",$coddocumento);
		$this->gestioncomercial->set("namearchivo",$namearch);
		$this->gestioncomercial->set("id_registro",$id_registro);
		$datos=$this->gestioncomercial->sp_vercostomantenimiento();
		return $datos;
	}
	//grafica estatus contacto mensual
	public function sp_graficacontactomensual ($usuario,$fecha_desde,$fecha_hasta)
	{
	$this->gestioncomercial->set("usuario",$usuario);
	$this->gestioncomercial->set("fecha_desde",$fecha_desde);
	$this->gestioncomercial->set("fecha_hasta",$fecha_hasta);
	$datos=$this->gestioncomercial->sp_graficacontactomensual();
	return $datos;
	}
    //grafica estatus contacto general
	public function sp_graficacontacto ($usuario)
	{
	$this->gestioncomercial->set("usuario",$usuario);
	$datos=$this->gestioncomercial->sp_graficacontacto();
	return $datos;
	}
    //cantidad de registros por bancpo y por año
	public function sp_graficaregistrosxanho()
	{
	$datos=$this->gestioncomercial->sp_graficaregistrosxanho();
	return $datos;
	}

	//cantidad de registros no contactados por año
	public function sp_nocontactografica($usuario)
	{
	$this->gestioncomercial->set("usuario",$usuario);	
	$datos=$this->gestioncomercial->sp_nocontactografica();
	return $datos;
	}

		//cantidad de registros no contactados por año
	public function sp_graficarbancoanio()
	{
	
	$datos=$this->gestioncomercial->sp_graficarbancoanio();
	return $datos;
	}

	public function sp_verestatusequipo($id_registro)
	{
	$this->gestioncomercial->set("id_registro",$id_registro);	
	$datos=$this->gestioncomercial->sp_verestatusequipo();
	return $datos;
	}

	public function sp_verestatusbanesco($id_registro)
	{
	$this->gestioncomercial->set("id_registro",$id_registro);	
	$datos=$this->gestioncomercial->sp_verestatusbanesco();
	return $datos;
	}

	public function sp_verexpediente($coddocumento)
	{
	$this->gestioncomercial->set("coddocumento",$coddocumento);	
	$datos=$this->gestioncomercial->sp_verexpediente();
	return $datos;
	}

	public function sp_vermodalidadpagos($id_registro){
		$this->gestioncomercial->set("id_registro",$id_registro);
		$datos=$this->gestioncomercial->sp_vermodalidadpagos();
		return $datos;
	}
	public function sp_verestatuscomodato($id_registro){
	$this->gestioncomercial->set("id_registro",$id_registro);
	$datos=$this->gestioncomercial->sp_verestatuscomodato();
	return $datos;
	}


	public function mostrarseriales($id_registro){
	 $this->gestioncomercial->set("id_registro",$id_registro);
	 $datos=$this->gestioncomercial->mostrarseriales();
	 return $datos;
	 }

	 public function sp_verserialesrecompra($id_registro)
	{
	$this->gestioncomercial->set("id_registro",$id_registro);	
	$datos=$this->gestioncomercial->sp_verserialesrecompra();
	return $datos;
	}

	public function sp_buscarserialresg(){
		$datos=$this->gestioncomercial->sp_buscarserialresg();
		return $datos;
	}

	public function sp_tasacambioxfecha($fechapago){
		$this->gestioncomercial->set("fechapago",$fechapago);
		$datos=$this->gestioncomercial->sp_tasacambioxfecha();
		return $datos;
		}
	
	public function sp_verpagos($idregistro){
		$this->gestioncomercial->set("idregistro",$idregistro);
		$datos=$this->gestioncomercial->sp_verpagos();
		return $datos;
		}

	public function mostrarClienteFacturacion($id_registro){
		$this->gestioncomercial->set("id_registro",$id_registro);
		$datos=$this->gestioncomercial->mostrarClienteFacturacion();
		return $datos;
		}
}

?>

