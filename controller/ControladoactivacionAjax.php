<?php
//session_start();
include_once("../../model/Activacionequipos.php");

class Controladoractivacionn{

	private $activacion;

	public function __construct(){
	$this->activacion= new Activacion();
	}

	public function sp_guaradaractivacion ($consecutivo,$estatus,$serialpos,$serialsimcard,$serialmifi,$usuario,$id,$idgestion)
		{
			$this->activacion->set("iidregistro",$consecutivo);
			$this->activacion->set("codigomot",$estatus);
			$this->activacion->set("iserialpos",$serialpos);
			$this->activacion->set("iserialsimcard",$serialsimcard);
			$this->activacion->set("iserialmifi",$serialmifi);
			$this->activacion->set("iusuariopercance",$usuario);
			$this->activacion->set("consecutivo",$id);
			$this->activacion->set("idgestion",$idgestion);
			$datos=$this->activacion->sp_guaradaractivacion();
			return $datos;
		}
}
?>