<?php

	include_once("../model/Tecnicos.php");

		class controladorTecnicos
		{ 
			private $tecnicos;

			public function __construct(){
				$this->tecnicos= new Tecnicos();
			}
		
			public function sptecnicos(){
				$datos=$this->tecnicos->sptecnicos();
				return $datos;
			}
			
			public function mostrartecnicos(){
			$datos=$this->tecnicos->mostrartecnicos();
			return $datos;
		}   
		
			public function crear ($operacion, $zonatrabajo, $tipodoc, $coddocumento, 
        	$nombres, $apellidos, $usuario)
        	{
				$this->tecnicos->set("operacion",$operacion);
				$this->tecnicos->set("trabajo",$zonatrabajo);
				$this->tecnicos->set("tipodoc",$tipodoc);
				$this->tecnicos->set("coddocumento",$coddocumento);
				$this->tecnicos->set("nombres",$nombres);
				$this->tecnicos->set("apellidos",$apellidos);
				$this->tecnicos->set("usuario",$usuario);
				
				$resultado=$this->tecnicos->crear();
				return $resultado;
			}


			public function EditarTecnicos ($operacion, $codtecnico,$trabajo, $coddocumento, 
        	$nombres, $apellidos, $usuario)
        	{
				$this->tecnicos->set("operacion",$operacion);
				$this->tecnicos->set("codtecnico",$codtecnico);
				$this->tecnicos->set("trabajo",$trabajo);
				$this->tecnicos->set("coddocumento",$coddocumento);
				$this->tecnicos->set("nombres",$nombres);
				$this->tecnicos->set("apellidos",$apellidos);
				$this->tecnicos->set("usuario",$usuario);
				$resultado=$this->tecnicos->EditarTecnicos();
				return $resultado;
			}
		
  			public function EliminarTecnicos($operacion,$coddocumento)
		    {
				$this->tecnicos->set("operacion",$operacion);
				$this->tecnicos->set("coddocumento",$coddocumento);	
				$resultado=$this->tecnicos->EliminarTecnicos();
				return $resultado;
			}
		}

?>



