<?php

	include_once("../model/GestionMantenimiento.php");

		class controladorGestionMantenimiento
		{ 
			private $gestionmantenimiento;

			public function __construct(){
				$this->gestionmantenimiento= new GestionMantenimiento();
			}
				
			public function mostrargestion(){
			$datos=$this->gestionmantenimiento->mostrargestion();
			return $datos;
		}   

			public function mostrarzona($perfil){
			$this->gestionmantenimiento->set("perfil",$perfil);
			$datos=$this->gestionmantenimiento->mostrarzona();
			return $datos;
		}   
		
			public function crear ($operacion,$marcapos, $codtipo,$valordolar, $factorconversion, $usuario)
        	{
        		$this->gestionmantenimiento->set("operacion",$operacion);
        		$this->gestionmantenimiento->set("marcapos",$marcapos);
				$this->gestionmantenimiento->set("codtipo",$codtipo);
				$this->gestionmantenimiento->set("valordolar",$valordolar);
				$this->gestionmantenimiento->set("factorconversion",$factorconversion);
				$this->gestionmantenimiento->set("usuario",$usuario);
				$resultado=$this->gestionmantenimiento->crear();
				return $resultado;
			}


			public function EditarGestion ($operacion,$correlativo,$marcapos, $codtipo,$valordolar, $factorconversion, $usuario)
        	{
				$this->gestionmantenimiento->set("operacion",$operacion);
				$this->gestionmantenimiento->set("correlativo",$correlativo);
				$this->gestionmantenimiento->set("marcapos",$marcapos);
				$this->gestionmantenimiento->set("codtipo",$codtipo);
				$this->gestionmantenimiento->set("valordolar",$valordolar);
				$this->gestionmantenimiento->set("factorconversion",$factorconversion);
				$this->gestionmantenimiento->set("usuario",$usuario);
				$resultado=$this->gestionmantenimiento->EditarGestion();
				return $resultado;
			}

			public function crearzona ($operacion,$estados, $municipios,$parroquias, $tipozona, $usuario,$cod_perfil){
				
       			$this->gestionmantenimiento->set("operacion",$operacion);
				$this->gestionmantenimiento->set("estados",$estados);
				$this->gestionmantenimiento->set("municipios",$municipios);
				$this->gestionmantenimiento->set("parroquias",$parroquias);
				$this->gestionmantenimiento->set("tipozona",$tipozona);
				$this->gestionmantenimiento->set("usuario",$usuario);
				$this->gestionmantenimiento->set("cod_perfil",$cod_perfil);
				$resultado=$this->gestionmantenimiento->crearzona();
				return $resultado;
		}

				public function EditarZona ($operacion,$estados, $municipios,$parroquias, $tipozona,$usuario,$cod_perfil){
		
				$this->gestionmantenimiento->set("operacion",$operacion);
				$this->gestionmantenimiento->set("estados",$estados);
				$this->gestionmantenimiento->set("municipios",$municipios);
				$this->gestionmantenimiento->set("parroquias",$parroquias);
				$this->gestionmantenimiento->set("tipozona",$tipozona);
				$this->gestionmantenimiento->set("cod_perfil",$cod_perfil);
				$this->gestionmantenimiento->set("usuario",$usuario);
			   $resultado=$this->gestionmantenimiento->EditarZona();
			return $resultado;
		}
		

			public function spbuscarperfil($perfil){
		$this->gestionmantenimiento->set("perfil",$perfil);
		$datos=$this->gestionmantenimiento->spbuscarperfil();
		return $datos;
		}
		
			public function sp_buscarvalorpos($marcapos,$tipopos){
		$this->gestionmantenimiento->set("marcapos",$marcapos);
		$this->gestionmantenimiento->set("tipopos",$tipopos);
		$datos=$this->gestionmantenimiento->sp_buscarvalorpos();
		return $datos;
		}

			public function sp_detallevalorpos($marcapos,$tipopos,$fecha,$secuencial){
		$this->gestionmantenimiento->set("marcapos",$marcapos);
		$this->gestionmantenimiento->set("tipopos",$tipopos);
		$this->gestionmantenimiento->set("fecha",$fecha);
		$this->gestionmantenimiento->set("secuencial",$secuencial);
		$datos=$this->gestionmantenimiento->sp_detallevalorpos();
		return $datos;
		}

		}

?>



