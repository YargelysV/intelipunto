<?php
	//session_start();
	include_once("../model/ControlAcceso.php");

	class ControladorControlAcceso{

		private $controlacceso;

		public function __construct(){
		$this->controlacceso= new controlacceso();
		}

		public function sp_buscaridempleados(){
			$datos=$this->controlacceso->sp_buscaridempleados();
			return $datos;
		}

		public function sp_buscaraccesos($meses,$anio,$empleado){
			$this->controlacceso->set("meses",$meses);
			$this->controlacceso->set("anio",$anio);
			$this->controlacceso->set("empleado",$empleado);
			$datos=$this->controlacceso->sp_buscaraccesos();
			return $datos;
		}

		public function spasignarferiados($fechas){
			$this->controlacceso->set("fechass",$fechas);
			$datos=$this->controlacceso->spasignarferiados();
			return $datos;
		}
		
		public function sp_mostrarferiados(){
			$datos=$this->controlacceso->sp_mostrarferiados();
			return $datos;
		}

		public function sp_eliminarferiados($fechas){
			//echo $fechass;
			$this->controlacceso->set("fechas",$fechas);
			$datos=$this->controlacceso->sp_eliminarferiados();
			return $datos;
		}
	}


?>

