<?php

	include_once("../model/Ejecutivo.php");

		class controladorEjecutivo
		{ 
			private $ejecutivo;

			public function __construct(){
				$this->ejecutivo= new Ejecutivo();
			}
		
			public function spejecutivo(){
				$datos=$this->ejecutivo->spejecutivo();
				return $datos;
			}
			
			public function mostrarEjecutivos(){
			$datos=$this->ejecutivo->mostrarEjecutivos();
			return $datos;
			}  

			public function mostrartipodoc(){
			$datos=$this->ejecutivo->mostrartipodoc();
			return $datos;
			}    

					
			public function ejecutivobanco ($documento)
			{
			$this->ejecutivo->set("documento",$documento);
			$datos=$this->ejecutivo->ejecutivobanco();
			return $datos;
			}

			public function crear ($operacion, $codejecutivo,$nejecutivo, $tipodoc, $documento, 
        	$nombres, $apellidos, $ejecutivo,$usuario)
        	{
				$this->ejecutivo->set("operacion",$operacion);
				$this->ejecutivo->set("codejecutivo",$codejecutivo);
				$this->ejecutivo->set("nejecutivo",$nejecutivo);
				$this->ejecutivo->set("tipodoc",$tipodoc);
				$this->ejecutivo->set("documento",$documento);
				$this->ejecutivo->set("nombres",$nombres);
				$this->ejecutivo->set("apellidos",$apellidos);
				$this->ejecutivo->set("ejecutivo",$ejecutivo);
				$this->ejecutivo->set("usuario",$usuario);
				$resultado=$this->ejecutivo->crear();
				return $resultado;
			}

			public function actualizarbanco ($banco,$vendedor)
        	{
				
				$this->ejecutivo->set("banco",$banco);
				$this->ejecutivo->set("vendedor",$vendedor);
				$resultado=$this->ejecutivo->actualizarbanco();
				return $resultado;
			}

			

			public function eliminarbanco ($banco)
        	{
				$this->ejecutivo->set("banco",$banco);
	
				$resultado=$this->ejecutivo->eliminarbanco();
				return $resultado;
			}

				public function editarr ($operacion, $vendedor,$nombrebanco, $documento, $usuario)
        	{
				$this->ejecutivo->set("operacion",$operacion);
				$this->ejecutivo->set("vendedor",$vendedor);
				$this->ejecutivo->set("nombrebanco",$nombrebanco);
				$this->ejecutivo->set("documento",$documento);
				$this->ejecutivo->set("usuario",$usuario);
				$resultado=$this->ejecutivo->editarr();
				return $resultado;
			}

			public function EditarEjecutivo ($operacion, $codejecutivo,$nejecutivo, $tipodoc, $documento, $nombres, $apellidos, $ejecutivo,$usuario)
        	{
				$this->ejecutivo->set("operacion",$operacion);
				$this->ejecutivo->set("codejecutivo",$codejecutivo);
				$this->ejecutivo->set("nejecutivo",$nejecutivo);
				$this->ejecutivo->set("tipodoc",$tipodoc);
				$this->ejecutivo->set("documento",$documento);
				$this->ejecutivo->set("nombres",$nombres);
				$this->ejecutivo->set("apellidos",$apellidos);
				$this->ejecutivo->set("ejecutivo",$ejecutivo);
				$this->ejecutivo->set("usuario",$usuario);
				$resultado=$this->ejecutivo->EditarEjecutivo();
				return $resultado;
			}
		
		    public function EliminarEjecutivo($operacion,$documento)
		    {
				$this->ejecutivo->set("operacion",$operacion);
				$this->ejecutivo->set("documento",$documento);	
				$resultado=$this->ejecutivo->EliminarEjecutivo();
				return $resultado;
			}


		}

?>