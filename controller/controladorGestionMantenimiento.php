<?php

include_once("model/GestionMantenimiento.php");

	class controladorGestionMantenimiento
	{
		private $gestionmantenimiento;
		
		public function __construct(){
			$this->gestionmantenimiento= new GestionMantenimiento();
		}
		
		public function mostrargestion(){
			$datos=$this->gestionmantenimiento->mostrargestion();
			return $datos;
		} 

		public function spbuscartipozona(){
			$datos=$this->gestionmantenimiento->spbuscartipozona();
			return $datos;
		}   
         
    	public function crear ($operacion,$marcapos, $codtipo,$valordolar, $factorconversion, $valorbs, $fechaact, $usuario)
       	{
       			$this->gestionmantenimiento->set("operacion",$operacion);
				$this->gestionmantenimiento->set("marcapos",$marcapos);
				$this->gestionmantenimiento->set("codtipo",$codtipo);
				$this->gestionmantenimiento->set("valordolar",$valordolar);
				$this->gestionmantenimiento->set("factorconversion",$factorconversion);
				$this->gestionmantenimiento->set("valorbs",$valorbs);
				$this->gestionmantenimiento->set("fechaact",$fechaact);
				$this->gestionmantenimiento->set("usuario",$usuario);
				$resultado=$this->gestionmantenimiento->crear();
				return $resultado;
		}

	
		public function EditarGestion ($operacion,$correlativo, $marcapos,$codtipo, $valordolar, $factorconversion, $valorbs, $fechaact, $usuario)
		{
			$this->gestionmantenimiento->set("operacion",$operacion);
			$this->gestionmantenimiento->set("correlativo",$correlativo);
			$this->gestionmantenimiento->set("marcapos",$marcapos);
			$this->gestionmantenimiento->set("codtipo",$codtipo);
			$this->gestionmantenimiento->set("valordolar",$valordolar);
			$this->gestionmantenimiento->set("factorconversion",$factorconversion);
			$this->gestionmantenimiento->set("valorbs",$valorbs);			
			$this->gestionmantenimiento->set("fechaact",$fechaact);
			$this->gestionmantenimiento->set("usuario",$usuario);
			$resultado=$this->gestionmantenimiento->EditarGestion();
			return $resultado;
		}

		public function spbuscarperfil(){
		$datos=$this->gestionmantenimiento->spbuscarperfil();
		return $datos;
		}

		public function sp_buscarvalorpos($marcapos,$tipopos){
		$this->gestionmantenimiento->set("marcapos",$marcapos);
		$this->gestionmantenimiento->set("tipopos",$tipopos);
		$datos=$this->gestionmantenimiento->sp_buscarvalorpos();
		return $datos;
		}

		public function sp_detallevalorpos($marcapos,$tipopos,$fecha,$secuencial){
		$this->gestionmantenimiento->set("marcapos",$marcapos);
		$this->gestionmantenimiento->set("tipopos",$tipopos);
		$this->gestionmantenimiento->set("fecha",$fecha);
		$this->gestionmantenimiento->set("secuencial",$secuencial);
		$datos=$this->gestionmantenimiento->sp_detallevalorpos();
		return $datos;
		}

			public function sp_zona($perfil){
			$this->gestionmantenimiento->set("perfil",$perfil);	
			$datos=$this->gestionmantenimiento->sp_zona();
			return $datos;
		}

			public function sp_bancos($perfil){
			$this->gestionmantenimiento->set("perfil",$perfil);	
			$datos=$this->gestionmantenimiento->sp_bancos();
			return $datos;
		}


				public function crearr ($desc_perfil)
       	{
       			$this->gestionmantenimiento->set("desc_perfil",$desc_perfil);
				$resultado=$this->gestionmantenimiento->crearr();
				return $resultado;
		}

				public function spverificarmodulosactivos($login){
				$this->gestionmantenimiento->set("login",$login);
				$resultado=$this->gestionmantenimiento->spverificarmodulosactivos();
				return $resultado;
		}	

		public function GestionarZonas($idzona, $idparroq,$usuario){
			$this->gestionmantenimiento->set("idzona",$idzona);
			$this->gestionmantenimiento->set("idparroq",$idparroq);
			$this->gestionmantenimiento->set("usuario",$usuario);
			$datos=$this->gestionmantenimiento->GestionarZonas();
			return $datos;
			}

    }
?>

