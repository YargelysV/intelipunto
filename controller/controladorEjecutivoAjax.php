<?php

	include_once("../model/Ejecutivo.php");

		class controladorEjecutivo
		{ 
			private $ejecutivo;

			public function __construct(){
				$this->ejecutivo= new Ejecutivo();
			}
		
			public function spejecutivo(){
				$datos=$this->ejecutivo->spejecutivo();
				return $datos;
			}
		
			public function crear ($operacion, $codejecutivo,$nejecutivo, $documento, 
        	$nombres, $apellidos, $usuario)
        	{
				$this->ejecutivo->set("operacion",$operacion);
				$this->ejecutivo->set("codejecutivo",$codejecutivo);
				$this->ejecutivo->set("nejecutivo",$nejecutivo);
				$this->ejecutivo->set("documento",$documento);
				$this->ejecutivo->set("nombres",$nombres);
				$this->ejecutivo->set("apellidos",$apellidos);
				$this->ejecutivo->set("usuario",$usuario);
				$resultado=$this->ejecutivo->crear();
				return $resultado;
			}


			public function EditarEjecutivo ($operacion, $codejecutivo,$nejecutivo, $documento, 
        	$nombres, $apellidos, $usuario)
        	{
				$this->ejecutivo->set("operacion",$operacion);
				$this->ejecutivo->set("codejecutivo",$codejecutivo);
				$this->ejecutivo->set("nejecutivo",$nejecutivo);
				$this->ejecutivo->set("documento",$documento);
				$this->ejecutivo->set("nombres",$nombres);
				$this->ejecutivo->set("apellidos",$apellidos);
				$this->ejecutivo->set("usuario",$usuario);
				$resultado=$this->ejecutivo->EditarEjecutivo();
				return $resultado;
			}
		
		    public function EliminarEjecutivo($operacion,$documento)
		    {
				$this->ejecutivo->set("operacion",$operacion);
				$this->ejecutivo->set("documento",$documento);	
				$resultado=$this->ejecutivo->EliminarEjecutivo();
				return $resultado;
			}

			public function selectipobanco($codejecutivo){
			$this->ejecutivo->set("codejecutivo",$codejecutivo);
			$datos=$this->ejecutivo->selectipobanco();
			return $datos;
			}

			public function spbancoejecutivos($vendedor){
			$this->ejecutivo->set("vendedor",$vendedor);
			$datos=$this->ejecutivo->spbancoejecutivos();
			return $datos;
			}

		}

?>