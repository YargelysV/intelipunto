<?php

include_once("../model/ClientePotencial.php");

class controladorClientePotencial{

	private $clientepotencial;

	public function __construct(){
		$this->clientepotencial= new ClientePotencial();
	}

	public function sp_buscarclienterecibido($cliente, $fecha){
		$this->clientepotencial->set("cliente",$cliente);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->clientepotencial->sp_buscarclienterecibido();
		return $datos;
	}

	public function verclientepotencialbuscar ($banco, $fecha, $namearchivo)
		{
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$this->clientepotencial->set("name",$namearchivo);
		$datos=$this->clientepotencial->verclientepotencialbuscar();
		return $datos;
		}
}

?>

