<?php

include_once("model/Pagos.php");

	class controladorPagosBasico
	{
		private $pagos;
		
	public function __construct()
		{
		$this->pagos=new Pagos();
		}
		
	public function sp_verpagos($idregistro){
		$this->pagos->set("idafiliado",$idregistro);
		$datos=$this->pagos->sp_verpagos();
		return $datos;
	}

	public function sp_verpagosdemifi($idafiliado){
		$this->pagos->set("idafiliado",$idafiliado);
		$datos=$this->pagos->sp_verpagosdemifi();
		return $datos;
	}

	public function sp_crudpago($operacion,$idafiliado,$fechapago,$horapago,$bancoorigen,$bancodestino,$formapago,$tipomoneda,$monto,$montousd,$factorconversion,$referencia,$nombredepositante,$imagenpago,$observacioncomer,$observacionadm,$usuario)
   {
			$this->pagos->set("operacion",$operacion);
			$this->pagos->set("idafiliado",$idafiliado);
			$this->pagos->set("fechapago",$fechapago);
			$this->pagos->set("horapago",$horapago);
			$this->pagos->set("bancoorigen",$bancoorigen);
			$this->pagos->set("bancodestino",$bancodestino);
			$this->pagos->set("formapago",$formapago);
			$this->pagos->set("moneda",$tipomoneda);
			$this->pagos->set("monto",$monto);
			$this->pagos->set("montousd",$montousd);
			$this->pagos->set("factorconversion",$factorconversion);
			$this->pagos->set("referencia",$referencia);
			$this->pagos->set("depositante",$nombredepositante);
			$this->pagos->set("capture",$imagenpago);
			$this->pagos->set("observacioncomer",$observacioncomer);
			$this->pagos->set("observacionadm",$observacionadm);
			$this->pagos->set("usuariocarga",$usuario);
			$resultado=$this->pagos->sp_crudpago();	
			return $datos;			
    }

    public function sp_registrosporaprobar(){
	    $datos=$this->pagos->sp_registrosporaprobar();
	    return $datos;
	}
    public function sp_expedientesporaprobar(){
	    $datos=$this->pagos->sp_expedientesporaprobar();
	    return $datos;
	}


	

}

?>
