<?php

include_once("model/Encuesta.php");

	class controladorEncuesta
	{
		private $encuesta;
		
		public function __construct()
		{
			$this->encuesta=new EncuestaNueva();
		}
		
			
		public function spbuscarfechaencuesta($cliente,$fecha,$origen){
			$this->encuesta->set("cliente",$cliente);
			$this->encuesta->set("FechaRecepcionArchivo",$fecha);
			$this->encuesta->set("origen",$origen);
			$datos=$this->encuesta->spbuscarfechaencuesta();
			return $datos;
		}	

		public function spverbancoencuesta($cliente){
			$this->encuesta->set("cliente",$cliente);
/*			$this->encuesta->set("fecha",$fechadetalle);
			$this->encuesta->set("namearchivo",$namearchivo);*/
			$datos=$this->encuesta->spverbancoencuesta();
			return $datos;
			}

		public function sp_bancfechaencuesta($cliente, $fechadetalle, $namearchivo){
			$this->encuesta->set("cliente",$cliente);
			$this->encuesta->set("fecha",$fechadetalle);
			$this->encuesta->set("namearchivo",$namearchivo);
			$datos=$this->encuesta->sp_bancfechaencuesta();
			return $datos;
			}	

		public function mostrardetallencuesta($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->mostrardetallencuesta();
			return $datos;
			}
		 public function mostrarinventarioposafiliado($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->mostrarinventarioposafiliado();
		
			return $datos;
			}
		public function verdireccionInstalacion($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->verdireccionInstalacion();
			return $datos;
			}
		public function sp_buscartipousuario($usuario){
			$this->encuesta->set("usuario",$usuario);
			$datos=$this->encuesta->sp_buscartipousuario();
			return $datos;
			}
		public function spbuscarafiliadorifencuesta($rif){
			$this->encuesta->set("rif",$rif);
			$datos=$this->encuesta->spbuscarafiliadorifencuesta();
			return $datos;
		}

		public function spagregarpersona($idCliente,$dato){
			$this->encuesta->set("idCliente",$idCliente);
			$this->encuesta->set("dato",$dato);
			$datos=$this->encuesta->spagregarpersona();
			return $datos;
		}

		public function sp_registrosactivos($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->sp_registrosactivos();
			return $datos;
	
		}

		public function sp_verregistroencuesta($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->sp_verregistroencuesta();
			return $datos;
	
		}

		public function sp_actividadencuesta($idcliente,$activo,$usuario){
			$this->encuesta->set("idcliente",$idcliente);
			$this->encuesta->set("activo",$activo);
			$this->encuesta->set("usuario",$usuario);
			$datos=$this->encuesta->sp_actividadencuesta();
			return $datos;
		}
		public function sp_registrosactivosencuesta($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->sp_registrosactivosencuesta();
			return $datos;
		}
		public function spbuscarpersonasreferidas(){
			$datos=$this->encuesta->spbuscarpersonasreferidas();
			return $datos;
			}
		public function spbuscarpersonareferida($idcliente){
			$this->encuesta->set("idcliente",$idcliente);
			$datos=$this->encuesta->spbuscarpersonareferida();
			return $datos;
			}
		public function  sp_estatusactual($idcliente){
			$this->encuesta->set("id_cliente",$idcliente);
			$datos=$this->encuesta->sp_estatusactual();
			return $datos;
		}
		public function sp_registrohistorial($idcliente){
			$this->encuesta->set("id_cliente",$idcliente);
			$datos=$this->encuesta->sp_registrohistorial();
			return $datos;
		}	

		public function sp_cantidaderegistros($idcliente){
			$this->encuesta->set("id_cliente",$idcliente);
			$datos=$this->encuesta->sp_cantidaderegistros();
			return $datos;
		}

		public function sp_verpcontacto($idcliente){
		$this->encuesta->set("id_cliente",$idcliente);
		$datos=$this->encuesta->sp_verpcontacto();
		return $datos;
		}


    }
?>
