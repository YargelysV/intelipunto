<?php
//session_start();
include_once("../../../model/Reporte.php");

class ControladorReporte{
 
	private $reporte;

	public function __construct(){
		$this->reporte= new Reporte();
	}


	public function spverclientes(){

		$datos=$this->reporte->spverclientes();
		return $datos;
	}

	public function spvendedorinteligensa($codtipovendedor){
		$this->reporte->set("codtipovendedor",$codtipovendedor);
		$datos=$this->reporte->spvendedorinteligensa();
		return $datos;
	}
	public function sp_formapago(){
		$datos=$this->reporte->sp_formapago();
		return $datos;
	}

	public function sp_estatussolpos(){
		$datos=$this->reporte->sp_estatussolpos();
		return $datos;
	}
	public function sp_tipocuenta(){
		$datos=$this->reporte->sp_tipocuenta();
		return $datos;
	}
	public function sp_verestado(){
		$datos=$this->reporte->sp_verestado();
		return $datos;
	}
	public function sp_ciudad($id_estado){
	    $this->reporte->set("id_estado",$id_estado);
		$datos=$this->reporte->sp_ciudad();
		return $datos;
	}
	public function sp_municipio($id_estado){
	    $this->reporte->set("id_estado",$id_estado);
		$datos=$this->reporte->sp_municipio();
		return $datos;
	}
	public function sp_municipioselected($id_municipio){
	    $this->reporte->set("id_municipio",$id_municipio);
		$datos=$this->reporte->sp_municipioselected();
		return $datos;
	}
	public function sp_parroquia($id_municipio){
	    $this->reporte->set("id_municipio",$id_municipio);
		$datos=$this->reporte->sp_parroquia();
		return $datos;
	}
	public function sp_parroquiaselected($id_parroquia){
	    $this->reporte->set("id_parroquia",$id_parroquia);
		$datos=$this->reporte->sp_parroquiaselected();
		return $datos;
	}
	public function sp_estatusinteresado(){
		$datos=$this->reporte->sp_estatusinteresado();
		return $datos;
	}
	public function sp_tipolinea($tipopos){
		$this->reporte->set("TipoPOS",$tipopos);
		$datos=$this->reporte->sp_tipolinea();
		return $datos;
	}
	public function sp_verbancos($default){
        $this->reporte->set("default",$default);
		$datos=$this->reporte->sp_verbancos();
		return $datos;
	}


	public function sp_proveedorsimcard($tipolinea){
		$this->reporte->set("tipolinea",$tipolinea);
		$datos=$this->reporte->sp_proveedorsimcard();
		return $datos;
	}

	public function sp_tipotarjeta(){
		$datos=$this->reporte->sp_tipotarjeta();
		return $datos;
	}
	public function sp_tipotarjetaselected($tipotarjeta){
		$this->reporte->set("tipotarjeta",$tipotarjeta);
		$datos=$this->reporte->sp_tipotarjetaselected();
		return $datos;
	}
	public function sp_marcapos(){
		$datos=$this->reporte->sp_marcapos();
		return $datos;
	}
	public function sp_costopos($tipopos,$codmarcapos){
		$this->reporte->set("TipoPOS",$tipopos);
		$this->reporte->set("codmarcapos",$codmarcapos);
		$datos=$this->reporte->sp_costopos();
		return $datos;
	}
	public function sp_listbanco(){
		$datos=$this->reporte->sp_listbanco();
		return $datos;
	}
	public function sp_verafiliadocomercializacion($coddocumento,$cliente,$namearchivo,$direccioninstalacion){
		$this->reporte->set("coddocumento",$coddocumento);
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("namearchivo",$namearchivo);
		$this->reporte->set("direccioninstalacion",$direccioninstalacion);
		$datos=$this->reporte->sp_verafiliadocomercializacion();
		return $datos;
	}

	
	public function ProcesarInformacionComercializacion($operacion, $cliente, $usuario){
		$this->reporte->set("Operacion",$operacion);
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("usuario",$usuario);
		$datos=$this->reporte->ProcesarInformacionComercializacion();
		return $datos;
	}

	public function comercializacionreportedetallado ($banco, $fechaenvio, $fecharecepcion)
		{
		$this->reporte->set("PrefijoBanco",$banco);
		$this->reporte->set("fechaenvio",$fechaenvio);
		$this->reporte->set("fecharecepcion",$fecharecepcion);
		$datos=$this->reporte->comercializacionreportedetallado();
		return $datos;
		}

		public function spverclienteejecutivo($coddocumento){
		$this->reporte->set("coddocumento",$coddocumento);
		$datos=$this->reporte->spverclienteejecutivo();
		return $datos;
		}

		public function sp_ejecutivoxbanco($banco){
		
		$this->reporte->set("banco",$banco);
		$datos=$this->reporte->sp_ejecutivoxbanco();
		return $datos;
		}

		public function sp_buscargerente(){
		
		$datos=$this->reporte->sp_buscargerente();
		return $datos;
		}

		public function sp_busquedaxMes()
	{
	$datos=$this->reporte->sp_busquedaxMes();
	return $datos;
	}
      public function sp_reportecaptfiltroejec(){

		$datos=$this->reporte->sp_reportecaptfiltroejec();
		return $datos;
	}


	public function sp_detallepregunta(){

		$datos=$this->reporte->sp_detallepregunta();
		return $datos;
	}

	public function sp_reportediarioestatussustitucion ($banco)
		{
		$this->reporte->set("banco",$banco);
		//$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioestatussustitucion();
		return $datos;
		}
		
}


?>



