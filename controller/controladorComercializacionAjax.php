<?php
//session_start();
include_once("../../../model/gestionComercializacion.php");

class gestionComercializacion{

	private $comercializacion;

	public function __construct(){
		$this->comercializacion= new Comercializacion();
	}


	public function spverclientes(){

		$datos=$this->comercializacion->spverclientes();
		return $datos;
	}
    public function sp_reloadregistro($id_registro){
    	$this->comercializacion->set("id_registro",$id_registro);
    	$datos=$this->comercializacion->sp_reloadregistro();
    	return $datos;
    }

    public function sp_indicacontacto($id_registro,$usuario){
    	$this->comercializacion->set("id_registro",$id_registro);
    	$this->comercializacion->set("usuario",$usuario);
    	$datos=$this->comercializacion->sp_indicacontacto();
    	return $datos;
    }

	public function spvendedorinteligensa($codtipovendedor){
		$this->comercializacion->set("codtipovendedor",$codtipovendedor);
		$datos=$this->comercializacion->spvendedorinteligensa();
		return $datos;
	}
	public function formadepago(){
		$datos=$this->comercializacion->formadepago();
		return $datos;
	}

		public function sp_formadepago(){
		$datos=$this->comercializacion->sp_formapago();
		return $datos;
	}

		// 
		public function spverclientesbancos(){
			$datos=$this->comercializacion->spverclientesbancos();
			return $datos;
		}
	
		// 
		public function spverclientestbancos(){
			$datos=$this->comercializacion->spverclientestbancos();
			return $datos;
		}
	
		// 

		public function sp_tasacambioxfecha($fechapago){
			$this->comercializacion->set("fechapago",$fechapago);
			$datos=$this->comercializacion->sp_tasacambioxfecha();
			return $datos;
			}
		

		// 
			public function sp_verpagos($idregistro){
				$this->comercializacion->set("idregistro",$idregistro);
				$datos=$this->comercializacion->sp_verpagos();
				return $datos;
			}
		

	public function sp_estatussolpos($operacion){
		$this->comercializacion->set("Operacion",$operacion);
		$datos=$this->comercializacion->sp_estatussolpos();
		return $datos;
	}
	public function sp_tipocuenta(){
		$datos=$this->comercializacion->sp_tipocuenta();
		return $datos;
	}
	public function sp_actividadeconomica(){
		$datos=$this->comercializacion->sp_actividadeconomica();
		return $datos;
	}
	public function sp_verestado(){
		$datos=$this->comercializacion->sp_verestado();
		return $datos;
	}
	public function sp_ciudad($id_estado){
	    $this->comercializacion->set("id_estado",$id_estado);
		$datos=$this->comercializacion->sp_ciudad();
		return $datos;
	}
	public function sp_municipio($id_estado){
	    $this->comercializacion->set("id_estado",$id_estado);
		$datos=$this->comercializacion->sp_municipio();
		return $datos;
	}
	public function sp_municipioselected($id_municipio){
	    $this->comercializacion->set("id_municipio",$id_municipio);
		$datos=$this->comercializacion->sp_municipioselected();
		return $datos;
	}
	public function sp_parroquia($id_municipio){
	    $this->comercializacion->set("id_municipio",$id_municipio);
		$datos=$this->comercializacion->sp_parroquia();
		return $datos;
	}
	public function sp_parroquiaselected($id_parroquia){
	    $this->comercializacion->set("id_parroquia",$id_parroquia);
		$datos=$this->comercializacion->sp_parroquiaselected();
		return $datos;
	}
	public function sp_estatusinteresado(){
		$datos=$this->comercializacion->sp_estatusinteresado();
		return $datos;
	}
	public function sp_tipolinea(){
		//$this->comercializacion->set("TipoPOS",$tipopos);
		$datos=$this->comercializacion->sp_tipolinea();
		return $datos;
	}
	public function sp_proveedorsimcard($tipolinea){
		$this->comercializacion->set("tipolinea",$tipolinea);
		$datos=$this->comercializacion->sp_proveedorsimcard();
		return $datos;
	}

	public function sp_tipotarjeta(){
		$datos=$this->comercializacion->sp_tipotarjeta();
		return $datos;
	}
	public function sp_tipotarjetaselected($tipotarjeta){
		$this->comercializacion->set("tipotarjeta",$tipotarjeta);
		$datos=$this->comercializacion->sp_tipotarjetaselected();
		return $datos;
	}
	public function sp_marcapos($codmodelopos){
		  $this->comercializacion->set("codmodelopos",$codmodelopos);
		  $datos=$this->comercializacion->sp_marcapos();

		return $datos;
	}
	public function sp_tipopos(){
		  $datos=$this->comercializacion->sp_tipopos();
		return $datos;
	}
	public function sp_modelopos(){
		//$this->comercializacion->set("TipoPOS",$tipopos);
		  $datos=$this->comercializacion->sp_modelopos();
		return $datos;
	}

	public function sp_conexionpos($codmodelopos){
		  $this->comercializacion->set("codmodelopos",$codmodelopos);
		  $datos=$this->comercializacion->sp_conexionpos();
		return $datos;
	}

	public function sp_bancoasignar(){
		  $datos=$this->comercializacion->sp_bancoasignar();
		return $datos;
	}

	public function sp_costopos($tipopos,$codmarcapos,$id_registro){
		$this->comercializacion->set("TipoPOS",$tipopos);
		$this->comercializacion->set("codmarcapos",$codmarcapos);
		$this->comercializacion->set("id_registro",$id_registro);
		$datos=$this->comercializacion->sp_costopos();
		return $datos;
	}
	public function sp_listbanco(){
		$datos=$this->comercializacion->sp_listbanco();
		return $datos;
	}
	public function sp_verafiliadocomercializacion($id_registro){
		$this->comercializacion->set("id_registro",$id_registro);
		$datos=$this->comercializacion->sp_verafiliadocomercializacion();
		return $datos;
	}

	public function editar($NAfiliacion,$gcRcomercial_fantasia,$gcNombre,$gcApellido,$gcInstituto_organismo,$gcTlfmovil1,$gcTlfmovil2,$gcEmailcorpo,$gcEmailcomer,$gcEmailperso,$gcCactividad,$gcFlujocaja,$gcRotacioninv,$gcDiasLabora,$gcHorariocomer,$gcSenalinalamb,$gcModelo,$gcCantidadpos,$gcFormapago,$gcTipocta,$gccodetipodir3,$gcd_calle_av3,$gcd_localidad3,$gcd_sector3,$gcd_urbanizacion3,$gcd_nlocal3,$gcd_estado3,$gcd_codepostal3,$gcd_ptoref3,$d_instalacion3,  $gccodetipodir2,$gcd_calle_av2,$gcd_localidad2,$gcd_sector2,$gcd_urbanizacion2,$gcd_nlocal2,$gcd_estado2,$gcd_codepostal2,$gcd_ptoref2,$d_instalacion2,  $gccodetipodir1,$gcd_calle_av1,$gcd_localidad1,$gcd_sector1,$gcd_urbanizacion1,$gcd_nlocal1,$gcd_estado1,$gcd_codepostal1,$gcd_ptoref1,$d_instalacion1,$d_dirseleccionada){
		$this->comercializacion->set("NAfiliacion",$NAfiliacion);
		$this->comercializacion->set("gcRcomercial_fantasia",$gcRcomercial_fantasia);
		$this->comercializacion->set("gcNombre",$gcNombre);
		$this->comercializacion->set("gcApellido",$gcApellido);
		$this->comercializacion->set("gcInstituto_organismo",$gcInstituto_organismo);
		$this->comercializacion->set("gcTlfmovil1",$gcTlfmovil1);
		$this->comercializacion->set("gcTlfmovil2",$gcTlfmovil2);
		$this->comercializacion->set("gcEmailcorpo",$gcEmailcorpo);
		$this->comercializacion->set("gcEmailcomer",$gcEmailcomer);
		$this->comercializacion->set("gcEmailperso",$gcEmailperso);
		$this->comercializacion->set("gcCactividad",$gcCactividad);
		$this->comercializacion->set("gcFlujocaja",$gcFlujocaja);
		$this->comercializacion->set("gcRotacioninv",$gcRotacioninv);
		$this->comercializacion->set("gcDiasLabora",$gcDiasLabora);
		$this->comercializacion->set("gcHorariocomer",$gcHorariocomer);
		$this->comercializacion->set("gcSenalinalamb",$gcSenalinalamb);
		$this->comercializacion->set("gcModelo",$gcModelo);
		$this->comercializacion->set("gcCantidadpos",$gcCantidadpos);
		$this->comercializacion->set("gcFormapago",$gcFormapago);
		$this->comercializacion->set("gcTipocta",$gcTipocta);
		$this->comercializacion->set("gccodetipodir3",$gccodetipodir3);
		$this->comercializacion->set("gcd_calle_av3",$gcd_calle_av3);
		$this->comercializacion->set("gcd_localidad3",$gcd_localidad3);
		$this->comercializacion->set("gcd_sector3",$gcd_sector3);
		$this->comercializacion->set("gcd_urbanizacion3",$gcd_urbanizacion3);
		$this->comercializacion->set("gcd_nlocal3",$gcd_nlocal3);
		$this->comercializacion->set("gcd_estado3",$gcd_estado3);
		$this->comercializacion->set("gcd_codepostal3",$gcd_codepostal3);
		$this->comercializacion->set("gcd_ptoref3",$gcd_ptoref3);
		$this->comercializacion->set("d_instalacion3",$d_instalacion3);

		$this->comercializacion->set("gccodetipodir2",$gccodetipodir2);
		$this->comercializacion->set("gcd_calle_av2",$gcd_calle_av2);
		$this->comercializacion->set("gcd_localidad2",$gcd_localidad2);
		$this->comercializacion->set("gcd_sector2",$gcd_sector2);
		$this->comercializacion->set("gcd_urbanizacion2",$gcd_urbanizacion2);
		$this->comercializacion->set("gcd_nlocal2",$gcd_nlocal2);
		$this->comercializacion->set("gcd_estado2",$gcd_estado2);
		$this->comercializacion->set("gcd_codepostal2",$gcd_codepostal2);
		$this->comercializacion->set("gcd_ptoref2",$gcd_ptoref2);
		$this->comercializacion->set("d_instalacion2",$d_instalacion2);

		$this->comercializacion->set("gccodetipodir1",$gccodetipodir1);
		$this->comercializacion->set("gcd_calle_av1",$gcd_calle_av1);
		$this->comercializacion->set("gcd_localidad1",$gcd_localidad1);
		$this->comercializacion->set("gcd_sector1",$gcd_sector1);
		$this->comercializacion->set("gcd_urbanizacion1",$gcd_urbanizacion1);
		$this->comercializacion->set("gcd_nlocal1",$gcd_nlocal1);
		$this->comercializacion->set("gcd_estado1",$gcd_estado1);
		$this->comercializacion->set("gcd_codepostal1",$gcd_codepostal1);
		$this->comercializacion->set("gcd_ptoref1",$gcd_ptoref1);
		$this->comercializacion->set("d_instalacion1",$d_instalacion1);
		$this->comercializacion->set("d_dirseleccionada",$d_dirseleccionada);
		$resultado=$this->comercializacion->editar();
		return $resultado;

	}
    //function realiza update de direccion instalacion en la clie_tblclientepotencial
	public function editardirinstalacion($item,$afiliacion,$codetipodir,$e_calle_av,$e_localidad,$e_sector,$e_nlocal,$e_urbanizacion,$e_estado,$e_codepostal,$e_ptoref,$e_instalacion){
		$this->comercializacion->set("item",$item);
		$this->comercializacion->set("afiliacion",$afiliacion);
		$this->comercializacion->set("codetipodir",$codetipodir);
		$this->comercializacion->set("e_calle_av",$e_calle_av);
		$this->comercializacion->set("e_localidad",$e_localidad);
		$this->comercializacion->set("e_sector",$e_sector);
		$this->comercializacion->set("e_nlocal",$e_nlocal);
		$this->comercializacion->set("e_urbanizacion",$e_urbanizacion);
		$this->comercializacion->set("e_estado",$e_estado);
		$this->comercializacion->set("e_codepostal",$e_codepostal);
		$this->comercializacion->set("e_ptoref",$e_ptoref);
		$this->comercializacion->set("e_instalacion",$e_instalacion);
		$resultado=$this->comercializacion->editardirinstalacion();
		return $resultado;

	}
	
	public function sp_tienepos($id_registro){
		$this->comercializacion->set("id_registro",$id_registro);
		$datos=$this->comercializacion->sp_tienepos();
		return $datos;
	}

	public function sp_insertargestion($g_nafiliado,$g_codejecutivobanco,$g_codejecutivointeligensa,$g_estatuspos,$g_observacion,$coddocumento,$namearchivo,$usuario,$id_registro, $clientemigra, $clientecomodato, $clientvip){
		$this->comercializacion->set("g_nafiliado",$g_nafiliado);
		$this->comercializacion->set("g_codejecutivobanco",$g_codejecutivobanco);
		$this->comercializacion->set("g_codejecutivointeligensa",$g_codejecutivointeligensa);
		$this->comercializacion->set("g_estatuspos",$g_estatuspos);
		$this->comercializacion->set("g_observacion",$g_observacion);
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("clientemigra",$clientemigra);
		$this->comercializacion->set("clientecomodato",$clientecomodato);
		$this->comercializacion->set("clientvip",$clientvip);
		$datos=$this->comercializacion->sp_insertargestion();
		return $datos;
	}

	public function sp_equiposcostos($coddocumento,$namearchivo,$usuario,$direccion,$codmodelopos,$codmarcapos,$cantposaprobados,$tlinea,$tipopos){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("direccion",$direccion);
		$this->comercializacion->set("codmodelopos",$codmodelopos);
		$this->comercializacion->set("codmarcapos",$codmarcapos);
		$this->comercializacion->set("cantposaprobados",$cantposaprobados);
		$this->comercializacion->set("tipolinea",$tlinea);
		$this->comercializacion->set("TipoPOS",$tipopos);
		$datos=$this->comercializacion->sp_equiposcostos();
		return $datos;
	}
	public function sp_equiposcostos1($coddocumento,$namearchivo,$usuario,$direccion,$codmarcapos,$cantposaprobados,$tlinea,$tipopos){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("direccion",$direccion);
		$this->comercializacion->set("codmarcapos",$codmarcapos);
		$this->comercializacion->set("cantposaprobados",$cantposaprobados);
		$this->comercializacion->set("tipolinea",$tlinea);
		$this->comercializacion->set("TipoPOS",$tipopos);
		$datos=$this->comercializacion->sp_equiposcostos1();
		return $datos;
	}
	public function sp_indicainteres($coddocumento,$namearchivo,$usuario,$id_registro,$interes,$nafiliacion,$ejebanco, $ejeinteligensa){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("interes",$interes);
		$this->comercializacion->set("NAfiliacion",$nafiliacion);
		$this->comercializacion->set("ejebanco",$ejebanco);
		$this->comercializacion->set("ejeinteligensa",$ejeinteligensa);
		$datos=$this->comercializacion->sp_indicainteres();
		return $datos;
	}


	public function sp_comerdatosbasicos($coddocumento,$nombre,$apellido,$institutouorganismo,$rcomercialofantasia,$namearchivo){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("nombre",$nombre);
		$this->comercializacion->set("apellido",$apellido);
		$this->comercializacion->set("institutouorganismo",$institutouorganismo);
		$this->comercializacion->set("rcomercialofantasia",$rcomercialofantasia);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$datos=$this->comercializacion->sp_comerdatosbasicos();
		return $datos;
	}

	public function sp_comerdatoscontacto($coddocumento,$tlfmovil1,$tlfmovil2,$emailcorpo,$emailcomer,$emailperson,$namearchivo,$nombre,$apellido,$direccioninstalacion,$usuario,$tlfmovil3,$tlfmovil4){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("gcTlfmovil1",$tlfmovil1);
		$this->comercializacion->set("gcTlfmovil2",$tlfmovil2);
		$this->comercializacion->set("gcEmailcorpo",$emailcorpo);
		$this->comercializacion->set("gcEmailcomer",$emailcomer);
		$this->comercializacion->set("gcEmailperso",$emailperson);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("nombres",$nombre);
		$this->comercializacion->set("apellidos",$apellido);
		$this->comercializacion->set("direccion",$direccioninstalacion);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("gcTlfmovil3",$tlfmovil3);
		$this->comercializacion->set("gcTlfmovil4",$tlfmovil4);
		// $this->comercializacion->set("interes",$interes);
		// $this->comercializacion->set("g_nafiliado",$nafiliacion);
		// $this->comercializacion->set("codmarcapos",$codmarcapos);
		// $this->comercializacion->set("cantposaprobados",$cantposaprobados);
		// $this->comercializacion->set("tipolinea",$tipolinea);
		// $this->comercializacion->set("costopos",$costopos);
		// $this->comercializacion->set("ejebanco",$ejebanco);
		// $this->comercializacion->set("ejeinteligensa",$ejeinteligensa);

		$datos=$this->comercializacion->sp_comerdatoscontacto();
		return $datos;
	}

	public function sp_comerdatoscaracteristicaempresa($coddocumento,$namearch,$lun,$mar,$mie,$jue,$vie,$sab,$dom,$usuario,$txtlunvie,$txtsabdom,$acteconomica,$id_registro,$categoria,$subcategoria){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("namearchivo",$namearch);
		$this->comercializacion->set("lun",$lun);
		$this->comercializacion->set("mar",$mar);
		$this->comercializacion->set("mie",$mie);
		$this->comercializacion->set("jue",$jue);
		$this->comercializacion->set("vie",$vie);
		$this->comercializacion->set("sab",$sab);
		$this->comercializacion->set("dom",$dom);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("txtlunvie",$txtlunvie);
		$this->comercializacion->set("txtsabdom",$txtsabdom);
		$this->comercializacion->set("acteconomica",$acteconomica);
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("categoria",$categoria);
		$this->comercializacion->set("subcategoria",$subcategoria);
		$datos=$this->comercializacion->sp_comerdatoscaracteristicaempresa();
		return $datos;
	}

	public function sp_comerdatoseleccionequipos($coddocumento,$gcModelo,$gcCantidadpos,$gcSenalinalamb,$gcTipocta,$gcFormapago,$namearchivo){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("gcModelo",$gcModelo);
		$this->comercializacion->set("gcCantidadpos",$gcCantidadpos);
		$this->comercializacion->set("gcSenalinalamb",$gcSenalinalamb);
		$this->comercializacion->set("gcTipocta",$gcTipocta);
		$this->comercializacion->set("gcFormapago",$gcFormapago);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$datos=$this->comercializacion->sp_comerdatoseleccionequipos();
		return $datos;
	}

	public function sp_caracteristicasdepago($coddocumento,$cliente,$namearchivo,$usuario,$nafiliacion,$totalprecio,$ejebanco,$estatuspos1,$id_registro,$totalaprobados,$modventa,$fechaipago,$anticipomonto,$descuentomonto,$inicialmonto,$coordaprobacion,$observacionpago, $montocomision, $montorecompra, $serialresguardo1,$precioactual){
		$this->comercializacion->set("coddocumento",$coddocumento);
	    $this->comercializacion->set("cliente",$cliente);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("nafiliacion",$nafiliacion);
		$this->comercializacion->set("totalprecio",$totalprecio);
		$this->comercializacion->set("ejebanco",$ejebanco);
		$this->comercializacion->set("estatuspos1",$estatuspos1);
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("totalaprobados",$totalaprobados);
		$this->comercializacion->set("modventa",$modventa);
		$this->comercializacion->set("fechaipago",$fechaipago);
		$this->comercializacion->set("anticipomonto",$anticipomonto);
		$this->comercializacion->set("descuentomonto",$descuentomonto);
		$this->comercializacion->set("inicialmonto",$inicialmonto);
		$this->comercializacion->set("coordaprobacion",$coordaprobacion);
		$this->comercializacion->set("observacionpago",$observacionpago);
		$this->comercializacion->set("montocomision",$montocomision);
		$this->comercializacion->set("montorecompra",$montorecompra);
		$this->comercializacion->set("serialresguardo1",$serialresguardo1);
		$this->comercializacion->set("precioactual",$precioactual);
		$datos=$this->comercializacion->sp_caracteristicasdepago();
		return $datos;
	}


	public function direccionfiscal($coddocumento,$codetipodir,$e_calle_av,$e_localidad,$e_sector,$e_nlocal,$e_urbanizacion,$e_estado,$e_codepostal,$e_ptoref,$e_fechacarga,$e_prefijo,$e_namearchivo,$usuario,$id_registro){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("codetipodir",$codetipodir);
		$this->comercializacion->set("e_calle_av",$e_calle_av);
		$this->comercializacion->set("e_localidad",$e_localidad);
		$this->comercializacion->set("e_sector",$e_sector);
		$this->comercializacion->set("e_nlocal",$e_nlocal);
		$this->comercializacion->set("e_urbanizacion",$e_urbanizacion);
		$this->comercializacion->set("e_estado",$e_estado);
		$this->comercializacion->set("e_codepostal",$e_codepostal);
		$this->comercializacion->set("e_ptoref",$e_ptoref);
		$this->comercializacion->set("e_fechacarga",$e_fechacarga);
		$this->comercializacion->set("e_prefijo",$e_prefijo);
		$this->comercializacion->set("namearchivo",$e_namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("id_registro",$id_registro);
		$resultado=$this->comercializacion->direccionfiscal();
		return $resultado;
	}

	public function direccioncomercial($coddocumento,$codetipodir,$e_calle_av,$e_localidad,$e_sector,$e_nlocal,$e_urbanizacion,$e_estado,$e_codepostal,$e_ptoref,$e_fechacarga,$e_prefijo,$e_namearchivo){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("codetipodir",$codetipodir);
		$this->comercializacion->set("e_calle_av",$e_calle_av);
		$this->comercializacion->set("e_localidad",$e_localidad);
		$this->comercializacion->set("e_sector",$e_sector);
		$this->comercializacion->set("e_nlocal",$e_nlocal);
		$this->comercializacion->set("e_urbanizacion",$e_urbanizacion);
		$this->comercializacion->set("e_estado",$e_estado);
		$this->comercializacion->set("e_codepostal",$e_codepostal);
		$this->comercializacion->set("e_ptoref",$e_ptoref);
		$this->comercializacion->set("e_fechacarga",$e_fechacarga);
		$this->comercializacion->set("e_prefijo",$e_prefijo);
		$this->comercializacion->set("namearchivo",$e_namearchivo);
		$resultado=$this->comercializacion->direccioncomercial();
		return $resultado;
	}

	public function direccionevento($coddocumento,$codetipodir,$e_calle_av,$e_localidad,$e_sector,$e_nlocal,$e_urbanizacion,$e_estado,$e_codepostal,$e_ptoref,$e_fechacarga,$e_prefijo,$e_namearchivo){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("codetipodir",$codetipodir);
		$this->comercializacion->set("e_calle_av",$e_calle_av);
		$this->comercializacion->set("e_localidad",$e_localidad);
		$this->comercializacion->set("e_sector",$e_sector);
		$this->comercializacion->set("e_nlocal",$e_nlocal);
		$this->comercializacion->set("e_urbanizacion",$e_urbanizacion);
		$this->comercializacion->set("e_estado",$e_estado);
		$this->comercializacion->set("e_codepostal",$e_codepostal);
		$this->comercializacion->set("e_ptoref",$e_ptoref);
		$this->comercializacion->set("e_fechacarga",$e_fechacarga);
		$this->comercializacion->set("e_prefijo",$e_prefijo);
		$this->comercializacion->set("namearchivo",$e_namearchivo);
		$resultado=$this->comercializacion->direccionevento();
		return $resultado;
	}

 	public function direccioninstalacion($coddocumento,$codetipodir,$e_calle_av,$e_localidad,$e_sector,$e_nlocal,$e_urbanizacion,$e_estado,$e_codepostal,$e_ptoref,$e_fechacarga,$e_prefijo,$e_namearchivo,$usuario,$id_registro){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("codetipodir",$codetipodir);
		$this->comercializacion->set("e_calle_av",$e_calle_av);
		$this->comercializacion->set("e_localidad",$e_localidad);
		$this->comercializacion->set("e_sector",$e_sector);
		$this->comercializacion->set("e_nlocal",$e_nlocal);
		$this->comercializacion->set("e_urbanizacion",$e_urbanizacion);
		$this->comercializacion->set("e_estado",$e_estado);
		$this->comercializacion->set("e_codepostal",$e_codepostal);
		$this->comercializacion->set("e_ptoref",$e_ptoref);
		$this->comercializacion->set("e_fechacarga",$e_fechacarga);
		$this->comercializacion->set("e_prefijo",$e_prefijo);
		$this->comercializacion->set("namearchivo",$e_namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("id_registro",$id_registro);
		$resultado=$this->comercializacion->direccioninstalacion();
		return $resultado;
	}

	public function ProcesarInformacionComercializacion($operacion, $cliente, $usuario){
		$this->comercializacion->set("Operacion",$operacion);
		$this->comercializacion->set("cliente",$cliente);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->ProcesarInformacionComercializacion();
		return $datos;
	}

	public function sp_vercostomantenimiento($cliente,$coddocumento,$namearch,$id_registro){
		$this->comercializacion->set("cliente",$cliente);
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("namearchivo",$namearch);
		$this->comercializacion->set("id_registro",$id_registro);
		$datos=$this->comercializacion->sp_vercostomantenimiento();
		return $datos;
	}

	public function sp_interesmantenimiento($coddocumento,$namearchivo,$usuario,$direccion,$selectinteres){
		$this->comercializacion->set("coddocumento",$coddocumento);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("direccion",$direccion);
		$this->comercializacion->set("interes",$selectinteres);
		$datos=$this->comercializacion->sp_interesmantenimiento();
		return $datos;
	}

	public function sp_opcionmantenimiento(){
		$datos=$this->comercializacion->sp_opcionmantenimiento();
		return $datos;
	}

	public function sp_guardarnafiliado($id_registro,$nafiliacion){
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("nafiliacion",$nafiliacion);
		$datos=$this->comercializacion->sp_guardarnafiliado();
		return $datos;
	}

public function sp_guardarbanco($id_registro, $codigobanco, $usuario){
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("codigobanco",$codigobanco);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_guardarbanco();
		return $datos;
	}

   public function sp_graficacontactomensual ($usuario,$fecha_desde,$fecha_hasta)
   {
   $this->comercializacion->set("usuario",$usuario);
   $this->comercializacion->set("fecha_desde",$fecha_desde);
   $this->comercializacion->set("fecha_hasta",$fecha_hasta);
   $datos=$this->comercializacion->sp_graficacontactomensual();
   return $datos;
   } 
   
    // grafica de facturados-contactados-ventasconfondos-sincontactar
	public function sp_graficacontacto ($usuario)
	{
	$this->comercializacion->set("usuario",$usuario);
	$datos=$this->comercializacion->sp_graficacontacto();
	return $datos;
	}

	//cantidad de facturas por año filtrado por vendedor
	public function sp_nocontactografica($usuario)
	{
	$this->comercializacion->set("usuario",$usuario);		
	$datos=$this->comercializacion->sp_nocontactografica();
	return $datos;
	}
	
	public function spverbancoseje($valor, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->spverbancoseje();
		return $datos;
		}

		public function spverfechaxbancoeje($banco, $valor, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->comercializacion->set("PrefijoBanco",$banco);
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->spverfechaxbancoeje();
		return $datos;
		}

		public function spverfechaeje($valor, $tipousuario, $ejecutivo, $usuario, $coordinador,$coordinadordos){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->spverfechaeje();
		return $datos;
		}

		public function spverbancoxfechaeje($valor, $fecha, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("FechaRecepcionArchivo",$fecha);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->spverbancoxfechaeje();
		return $datos;
		}

		public function spverbancosdefeje($valor, $cliente, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("cliente",$cliente);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->spverbancosdefeje();
		return $datos;
		}

		public function spver_bancosxejecutivo($valor, $tipousuario, $ejecutivo, $usuario){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->spver_bancosxejecutivo();
		return $datos;
		}

		public function sp_verejecutivobancos($valor, $tipousuario, $ejecutivo, $usuario){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_verejecutivobancos();
		return $datos;
		}

		public function sp_verejecutivobco($banco, $valor, $tipousuario, $ejecutivo, $usuario){
		$this->comercializacion->set("PrefijoBanco",$banco);
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_verejecutivobco();
		return $datos;
		}

		public function sp_bancoejecutivo($valor, $ejecutivo, $tipousuario, $ejecutivobco, $usuario){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivobco",$ejecutivobco);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_bancoejecutivo();
		return $datos;
		}

		public function spbancoejec($valor, $cliente, $tipousuario, $ejecutivo, $usuario){
		$this->comercializacion->set("valor",$valor);
		$this->comercializacion->set("cliente",$cliente);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->spbancoejec();
		return $datos;
		}

		public function sp_verestatusequipo($id_registro)
		{
		$this->gestioncomercial->set("id_registro",$id_registro);	
		$datos=$this->gestioncomercial->sp_verestatusequipo();
		return $datos;
		}

		public function sp_verestatusbanesco($id_registro)
		{
		$this->gestioncomercial->set("id_registro",$id_registro);	
		$datos=$this->gestioncomercial->sp_verestatusbanesco();
		return $datos;
		}

		public function sp_modificardatos($id_registro,$serialp,$usuario){
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("serialp",$serialp);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_modificardatos();
		return $datos;
		}
		
	public function sp_modificarbancoactual($id_registro, $codigobanco, $usuario){
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("codigobanco",$codigobanco);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_modificarbancoactual();
		return $datos;
	}

	public function sp_modalidadventa(){
		$datos=$this->comercializacion->sp_modalidadventa();
		return $datos;
	}

	public function sp_coodregion(){
		$datos=$this->comercializacion->sp_coodregion();
		return $datos;
	}

	public function sp_vermodalidadpagos($id_registro){
		$this->comercializacion->set("id_registro",$id_registro);
		$datos=$this->comercializacion->sp_vermodalidadpagos();
		return $datos;
	}

	public function sp_verserialesrecompra($id_registro)
		{
		$this->gestioncomercial->set("id_registro",$id_registro);	
		$datos=$this->gestioncomercial->sp_verserialesrecompra();
		return $datos;
		}

	public function sp_buscarserialresg(){
		$datos=$this->comercializacion->sp_buscarserialresg();
		return $datos;
	}
	public function sp_guardarstatuscomodato($id_registro,$status,$usuario)
		{
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("status",$status);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_guardarstatuscomodato();
		return $datos;
		}

	public function sp_buscarserialresgu($busqueda){
		$this->comercializacion->set("busqueda",$busqueda);
		$datos=$this->comercializacion->sp_buscarserialresgu();
		return $datos;
	}

	public function sp_categoriacomercio(){
		$datos=$this->comercializacion->sp_categoriacomercio();
		return $datos;
	}
	public function sp_versubcategoriacomercio($id_categ){
	    $this->comercializacion->set("id_categ",$id_categ);
		$datos=$this->comercializacion->sp_versubcategoriacomercio();
		return $datos;
	}	

	public function sp_subcategselected($isubcategoriaselected){
	    $this->comercializacion->set("isubcategoriaselected",$isubcategoriaselected);
		$datos=$this->comercializacion->sp_subcategselected();
		return $datos;
	}
	// separador pa no olvidar
public function spinsertarhistorial($idregistro,$afiliado,$usuario,$mensaje,$detalles){
	//gestioncomercial
	$this->comercializacion->set("idregistro",$idregistro);
	$this->comercializacion->set("afiliado",$afiliado);
	$this->comercializacion->set("usuario",$usuario);
	$this->comercializacion->set("mensaje",$mensaje);
	$this->comercializacion->set("detalles",$detalles);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->comercializacion->spinsertarhistorial();
	return $datos;
	}
//

	public function sp_verestatusscontacto()
	{
	$datos=$this->comercializacion->sp_verestatusscontacto();
	return $datos;
	}	
}


?>



