<?php

	include_once("../model/ServicioMantenimiento.php");

		class controladorServicioMantenimiento
		{ 
			private $serviciomantenimiento;

			public function __construct(){
				$this->serviciomantenimiento= new ServicioMantenimiento();
			}
				
				public function sp_buscarmantenimiento($fecha){
				$this->serviciomantenimiento->set("fecha",$fecha);
				$datos=$this->serviciomantenimiento->sp_buscarmantenimiento();
				return $datos;

				}


			public function mostrarservicio($nafiliacion,$nterminal){
		$this->serviciomantenimiento->set("nafiliacion",$nafiliacion);
		$this->serviciomantenimiento->set("nterminal",$nterminal);
		$datos=$this->serviciomantenimiento->mostrarservicio();
		return $datos;
		}

	public function speditarserviciomantenimiento($nafiliacion,$nterminal,$documento, $formapago,$ntarjeta,$fechavenc,$ncuenta,$bancoorigen,$nombres,$coddocumento,$codtarjeta, $codseguridad,$usuario){
		$this->serviciomantenimiento->set("nafiliacion",$nafiliacion);
		$this->serviciomantenimiento->set("nterminal",$nterminal);
		$this->serviciomantenimiento->set("documento",$documento);
		$this->serviciomantenimiento->set("formapago",$formapago);
		$this->serviciomantenimiento->set("ntarjeta",$ntarjeta);
		$this->serviciomantenimiento->set("fechavenc",$fechavenc);
		$this->serviciomantenimiento->set("ncuenta",$ncuenta);
		$this->serviciomantenimiento->set("bancoorigen",$bancoorigen);
		$this->serviciomantenimiento->set("nombres",$nombres);
		$this->serviciomantenimiento->set("coddocumento",$coddocumento);
		$this->serviciomantenimiento->set("codtarjeta",$codtarjeta);
		$this->serviciomantenimiento->set("codseguridad",$codseguridad);
		$this->serviciomantenimiento->set("usuario",$usuario);
		$datos=$this->serviciomantenimiento->speditarserviciomantenimiento();
		return $datos;
	}


		public function crearmantenimiento ($operacion, $nroafiliacion,$razonsocial, $documento, $numterminal, $serialpos, $codigobanco,$codtipo,$marcapos, $usuario)
        	{
				$this->serviciomantenimiento->set("operacion",$operacion);
				$this->serviciomantenimiento->set("nroafiliacion",$nroafiliacion);
				$this->serviciomantenimiento->set("razonsocial",$razonsocial);
				$this->serviciomantenimiento->set("documento",$documento);
				$this->serviciomantenimiento->set("numterminal",$numterminal);
				$this->serviciomantenimiento->set("serialpos",$serialpos);
				$this->serviciomantenimiento->set("codigobanco",$codigobanco);
				$this->serviciomantenimiento->set("codtipo",$codtipo);
				$this->serviciomantenimiento->set("marcapos",$marcapos);
				$this->serviciomantenimiento->set("usuario",$usuario);
				
				$resultado=$this->serviciomantenimiento->crearmantenimiento();
				return $resultado;
			}

			public function editarcarga ($operacion, $nroafiliacion,$razonsocial, $documento, $numterminal, $serialpos,$codigobanco, $codtipo,$marcapos,$usuario)
        	{
				$this->serviciomantenimiento->set("operacion",$operacion);
				$this->serviciomantenimiento->set("nroafiliacion",$nroafiliacion);
				$this->serviciomantenimiento->set("razonsocial",$razonsocial);
				$this->serviciomantenimiento->set("documento",$documento);
				$this->serviciomantenimiento->set("numterminal",$numterminal);
				$this->serviciomantenimiento->set("serialpos",$serialpos);
				$this->serviciomantenimiento->set("codigobanco",$codigobanco);
				$this->serviciomantenimiento->set("codtipo",$codtipo);
				$this->serviciomantenimiento->set("marcapos",$marcapos);
				$this->serviciomantenimiento->set("usuario",$usuario);
				
				$resultado=$this->serviciomantenimiento->editarcarga();
				return $resultado;
			}

				public function mostrarcargaindividual(){
			$datos=$this->serviciomantenimiento->mostrarcargaindividual();
			return $datos;
		}  

		

		// public function spbuscarreportedomiciliacion($status, $modalidad){
		// $this->serviciomantenimiento->set("status",$status);
		// $this->serviciomantenimiento->set("modalidad",$modalidad);
		// $datos=$this->serviciomantenimiento->spbuscarreportedomiciliacion();
		// return $datos;

		// }	   


		}

?>



