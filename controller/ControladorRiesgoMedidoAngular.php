<?php

include_once("../model/RiesgoMedido.php");

	class controladorRiesgo{

		private $riesgo;

		public function __construct(){
			$this->riesgo= new RiesgoMedido();
		}

	public function sp_reporteriesgomedido($regiones,$banco,$usuario, $ejecutivoin, $tipousuario, $coordinador,$coordinadordos){
		$this->riesgo->set("regiones",$regiones);
		$this->riesgo->set("banco",$banco);
		$this->riesgo->set("usuario",$usuario);
		$this->riesgo->set("ejecutivoin",$ejecutivoin);
		$this->riesgo->set("tipousuario",$tipousuario);
		$this->riesgo->set("coordinador",$coordinador);
		$this->riesgo->set("coordinadordos",$coordinadordos);
		$datos=$this->riesgo->sp_reporteriesgomedido();
		return $datos;
	    }

	public function sp_reportecargaparametros($banco){
		$this->riesgo->set("banco",$banco);
		$datos=$this->riesgo->sp_reportecargaparametros();
		return $datos;
	    }
	    
	public function sp_verpagoscuotas($id_afiliado){
		$this->riesgo->set("id_afiliado",$id_afiliado);
		$datos=$this->riesgo->sp_verpagoscuotas();
		return $datos;
    }

    
		
}

?>

