<?php
//session_start();
include_once("../../../model/Adjuntar.php");

class controladorAdjuntar{

	private $adjuntar;

	public function __construct(){
	$this->adjuntar= new Adjuntar();
	}

	public function eliminar_tabltrans(){
	$this->adjuntar->speliminar_archivoseries();

	}

	public function spverclientes(){
	$datos=$this->adjuntar->spverclientes();
	return $datos;
	}

	public function spverarchivodiario(){
	$datos=$this->adjuntar->spverarchivodiario();
	return $datos;
	}

	public function spdatosusuario($usuario){
	$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->spdatosusuario();
	return $datos;
	}

	public function speliminar_archivoseries($fecha){
	$this->adjuntar->set("fecha",$fecha);
	$resultado=$this->adjuntar->speliminar_archivoseries();
	return $resultado;
	}

	public function speliminar_archivocliente(){
	$resultado=$this->adjuntar->speliminar_archivocliente();
	return $resultado;
	}

	public function mostrarmotivos($usuario){
	$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->mostrarmotivos();
	return $datos;
	}

	public function ver($login){
	$this->adjuntar->set("login",$login);
	$datos=$this->adjuntar->ver();	
	return $datos;
	}

	public function spverfechasseriales($valor){
	$this->adjuntar->set("valor",$valor);
	$datos=$this->adjuntar->spverfechasseriales();
	return $datos;
	} 	

	public function spverbancocliente(){
	$datos=$this->adjuntar->spverbancocliente();
	return $datos;
	}

	public function spverarchivodiariocliente($cliente, $fecha){
		$this->adjuntar->set("cliente",$cliente);
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->spverarchivodiariocliente();
		return $datos;
	}	

	    public function speliminar_archivo($nombre){
	    $this->adjuntar->set("nombre",$nombre);
		$resultado=$this->adjuntar->speliminar_archivo();
		return $resultado;
		}

	public function spbuscarserialesdevolucion($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialesdevolucion();
	return $datos;
	}
	public function spbuscarserialsimcarddevolucion($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialsimcarddevolucion();
	return $datos;
	}	
	public function spbuscarserialmifidev($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialmifidev();
	return $datos;
	}

		public function spvermotivodev(){
	//$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spvermotivodev();
	return $datos;
	}	

	 public function spverdestinoequipo(){
	//$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spverdestinoequipo();
	return $datos;
	}

	public function spverdestinoequipomifi(){
	//$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spverdestinoequipomifi();
	return $datos;
	}

		public function spbuscarseriales($busqueda,$banco){
	$this->adjuntar->set("busqueda",$busqueda);
	$this->adjuntar->set("banco",$banco);
	$datos=$this->adjuntar->spbuscarseriales();
	return $datos;
	}

	public function spbuscarserialsimcard($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialsimcard();
	return $datos;
	}

	public function spbuscarserialmifi($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialmifi();
	return $datos;
	}

		public function ver_factura($idregistro){
	$this->adjuntar->set("idregistro",$idregistro);
	//$this->adjuntar->set("directorio",$directorio);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->ver_factura();
	return $datos;
	}

		public function ver_facturademifi($idregistro){
	$this->adjuntar->set("idregistro",$idregistro);
	$datos=$this->adjuntar->ver_facturademifi();
	return $datos;
	}

	public function spfacturaanuladasdemifi($idregistro){
	$this->adjuntar->set("idregistro",$idregistro);
	//$this->adjuntar->set("directorio",$directorio);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->spfacturaanuladasdemifi();
	return $datos;
	}

	public function spEdicionRsCliente($idregistro,$dato,$user){
	$this->adjuntar->set("idregistro",$idregistro);
	$this->adjuntar->set("dato",$dato);
	$this->adjuntar->set("user",$user);
	//$this->adjuntar->set("directorio",$directorio);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->spEdicionRsCliente();
	return $datos;
	}

	public function spEdicionRsClCliente($idregistro,$dato,$user){
	$this->adjuntar->set("idregistro",$idregistro);
	$this->adjuntar->set("dato",$dato);
	$this->adjuntar->set("user",$user);
	//$this->adjuntar->set("directorio",$directorio);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->spEdicionRsClCliente();
	return $datos;
	}

	public function spEdicionRIFCliente($idregistro,$dato,$user,$ibanco){
	$this->adjuntar->set("idregistro",$idregistro);
	$this->adjuntar->set("dato",$dato);
	$this->adjuntar->set("user",$user);
	$this->adjuntar->set("ibanco",$ibanco);
	//$this->adjuntar->set("directorio",$directorio);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->spEdicionRIFCliente();
	return $datos;
	}

	public function spEdicionBancoCliente($idregistro,$dato,$user){
	$this->adjuntar->set("idregistro",$idregistro);
	$this->adjuntar->set("dato",$dato);
	$this->adjuntar->set("user",$user);
	$datos=$this->adjuntar->spEdicionBancoCliente();
	return $datos;
	}

	public function spEdicionModeloCliente($idregistro,$dato,$user){
	$this->adjuntar->set("idregistro",$idregistro);
	$this->adjuntar->set("dato",$dato);
	$this->adjuntar->set("user",$user);
	$datos=$this->adjuntar->spEdicionModeloCliente();
	return $datos;
	}
// zzz
	public function spverclientesbancos(){
		$datos=$this->adjuntar->spverclientesbancos();
		return $datos;
	}

	// 
	public function spverclientestbancos(){
		$datos=$this->adjuntar->spverclientestbancos();
		return $datos;
	}

	public function formadepago(){
		$datos=$this->adjuntar->formadepago();
		return $datos;
	}
	public function sp_verbcancoorigenasig($id_afiliado){
		$this->adjuntar->set("id_afiliado",$id_afiliado);
		$datos=$this->adjuntar->sp_verbcancoorigenasig();
		return $datos;
		}

	public function sp_verbcancodestinoasig($id_afiliado){
		$this->adjuntar->set("id_afiliado",$id_afiliado);
		$datos=$this->adjuntar->sp_verbcancodestinoasig();
		return $datos;
		}
		
	public function sp_verfromapagoasig($id_afiliado){
		$this->adjuntar->set("id_afiliado",$id_afiliado);
		$datos=$this->adjuntar->sp_verfromapagoasig();
		return $datos;
		}

	public function spbuscarpercance($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->spbuscarpercance();
	return $datos;
	}

	public function spvertipopercance(){
		$datos=$this->adjuntar->spvertipopercance();
		return $datos;
	}

	public function sp_costoposs($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->sp_costoposs();
	return $datos;
	}

	public function sp_regmodalidad($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->sp_regmodalidad();
	return $datos;
	}

	public function sp_tasacambioxfecha($fechapago){
	$this->adjuntar->set("fechapago",$fechapago);
	$datos=$this->adjuntar->sp_tasacambioxfecha();
	return $datos;
	}

	public function sp_bancoasignar(){
	  $datos=$this->adjuntar->sp_bancoasignar();
	return $datos;
	}

	public function sp_modelopos(){
	  $datos=$this->adjuntar->sp_modelopos();
	return $datos;
	}
// 
	// public function sp_tasacambioxfecha($fechapago){
	// 	$this->adjuntar->set("fechapago",$fechapago);
	// 	$datos=$this->adjuntar->sp_tasacambioxfecha();
	// 	return $datos;
	// 	}
	
}


?>
