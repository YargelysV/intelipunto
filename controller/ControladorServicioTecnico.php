<?php

include_once("model/ServicioTecnico.php");

	class controladorServicioTecnico
	{
		private $serviciotec;
		
		public function __construct()
		{
			$this->serviciotec=new Serviciotec();
		}
		
			public function verestatustecnico($nroafiliacion){
			$this->serviciotec->set("nroafiliacion",$nroafiliacion);
			$datos=$this->serviciotec->verestatustecnico();
			return $datos;

			}

			public function mostrarserviciotecnico(){
				$datos=$this->serviciotec->mostrarserviciotec();
				return $datos;
			}
		
				public function buscarserviciotecnico($busqueda){
			$this->serviciotec->set("busqueda",$busqueda);
			$datos=$this->serviciotec->buscarservicio();
			return $datos;
			}

			public function sp_buscarclienteserviciotecnico($cliente,$fecha){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->serviciotec->sp_buscarclienteserviciotecnico();
		return $datos;
		}

				public function sp_buscarafiliadoRif($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarafiliadoRif();
		return $datos;
	}

		public function sp_buscarafiliadoRifgestion($rif,$usuario){
		$this->serviciotec->set("rif",$rif);
		$this->serviciotec->set("usuario",$usuario);
		$datos=$this->serviciotec->sp_buscarafiliadoRifgestion();
		return $datos;
	}

		public function sp_buscarclientemifixrif($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarclientemifixrif();
		return $datos;
	}

			public function verclienteserviciotecnico($cliente, $fecha, $namearchivo){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fecha);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->verclienteserviciotecnico();
		return $datos;
			}

	public function sp_verclienteafiliado($cliente, $fechadetalle, $namearchivo){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fechadetalle);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->sp_verclienteafiliado();
		return $datos;
			}	


		public function sp_verclientedetalladomifi($cliente, $fechadetalle, $namearchivo){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fechadetalle);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->sp_verclientedetalladomifi();
		return $datos;
			}	

			public function sp_verclientegestion($cliente, $fechadetalle, $namearchivo, $region){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fechadetalle);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$this->serviciotec->set("region",$region);
		$datos=$this->serviciotec->sp_verclientegestion();
		return $datos;
			}

			public function sp_verclienteseriales($cliente, $fechadetalle, $namearchivo){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fechadetalle);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->sp_verclienteseriales();
		return $datos;
			}

			public function verdatosinstapos($nroafiliacion)
			{
			$this->serviciotec->set("nroafiliacion",$nroafiliacion);
			$resultado=$this->serviciotec->verdatosinstapos();
			return $resultado;
			}
		
			public function estatusinspos()
			{
			$datos=$this->serviciotec->verestatuspos();
			return $datos;
			}

			public function asignarserialpos ($serialposasignado, $nroafiliacion, $rifclie)
        	{
        		$this->serviciotec->set("serialposasignado",$serialposasignado);
        		$this->serviciotec->set("nroafiliacion",$nroafiliacion);
				$this->serviciotec->set("rifclie",$rifclie);
				
				$resultado=$this->serviciotec->asignarserialpos();
				return $resultado;
			}
			
			public function mostrardetalleafiliado($idcliente,$serial){
			$this->serviciotec->set("idcliente",$idcliente);
			$this->serviciotec->set("serial",$serial);

			$datos=$this->serviciotec->mostrardetalleafiliado();
			return $datos;
			}

				// 
	public function sp_reporteriesgomedidoadpins($idcliente){
		$this->serviciotec->set("idcliente",$idcliente);
		$datos=$this->serviciotec->sp_reporteriesgomedidoadpins();
		return $datos;
	    }

		
			public function mostrardetalleafiliadodemifi($idcliente,$serialmifi){
			$this->serviciotec->set("idcliente",$idcliente);
			$this->serviciotec->set("serialmifi",$serialmifi);

			$datos=$this->serviciotec->mostrardetalleafiliadodemifi();
			return $datos;
			}


			public function mostrardetalleafiliado1($idcliente){
			$this->serviciotec->set("idcliente",$idcliente);
			$datos=$this->serviciotec->mostrardetalleafiliado1();
			return $datos;
			}
            
            public function mostrarinventarioposafiliado($idcliente){
			$this->serviciotec->set("idcliente",$idcliente);
			
			$datos=$this->serviciotec->mostrarinventarioposafiliado();
		
			return $datos;
			}

			public function verdireccionInstalacion($idcliente){
			$this->serviciotec->set("idcliente",$idcliente);
			$datos=$this->serviciotec->verdireccionInstalacion();
			return $datos;
			}

			public function sp_buscartipousuario($usuario){
			$this->serviciotec->set("usuario",$usuario);
			$datos=$this->serviciotec->sp_buscartipousuario();
			return $datos;
			}
			

			public function mostrartecnico()
			{
				$datos=$this->serviciotec->mostrartecnico();
				return $datos;
			}

			public function tipodocumento()
			{
				$datos=$this->serviciotec->vertipodocumentos();
				return $datos;
			}

		public function sp_mostrartecnico($idcliente){
			$this->serviciotec->set("idcliente",$idcliente);
			$datos=$this->serviciotec->sp_mostrartecnico();
			return $datos;
		}

		public function sp_buscarclienteSTRif($rif){
			$this->serviciotec->set("rif",$rif);
			$datos=$this->serviciotec->sp_buscarclienteSTRif();
			return $datos;
		}

		public function mostrarserialafiliadoinstalado($idcliente,$idsecuencial){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$datos=$this->serviciotec->mostrarserialafiliadoinstalado();
		return $datos;
		}

		public function mostrarserialafiliadoinstalado2($idcliente,$idsecuencial,$serialpos){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$this->serviciotec->set("serialpos",$serialpos);
		$datos=$this->serviciotec->mostrarserialafiliadoinstalado2();
		return $datos;
		}

		public function mostrarserialafiliado1($serial,$nroafiliacion,$namearchivo){
		$this->serviciotec->set("serialpos",$serial);
		$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->mostrarserialafiliado1();
		return $datos;
		}

		public function sp_buscarservitec($cliente,$fecha,$origen){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
		$this->serviciotec->set("origen",$origen);
		$datos=$this->serviciotec->sp_buscarservitec();
		return $datos;
		}

		public function sp_buscarxfechamifi($cliente,$fecha,$origen){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
		$this->serviciotec->set("origen",$origen);
		$datos=$this->serviciotec->sp_buscarxfechamifi();
		return $datos;
		}		

		public function sp_buscargestionservicio($cliente,$fecha,$origen){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
		$this->serviciotec->set("origen",$origen);
		$datos=$this->serviciotec->sp_buscargestionservicio();
		return $datos;
		}	

		public function mostrarserialafiliadoinstaladomifi($idcliente,$idsecuencial){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$datos=$this->serviciotec->mostrarserialafiliadoinstaladomifi();
		return $datos;
		}

		public function mostrarserialafiliadogestionmifi($idcliente,$idsecuencial){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$datos=$this->serviciotec->mostrarserialafiliadogestionmifi();
		return $datos;
		}

		public function mostrarserialafiliadoinstaladomifi2($idcliente,$idsecuencial,$serialmifi){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$this->serviciotec->set("serialmifi",$serialmifi);
		$datos=$this->serviciotec->mostrarserialafiliadoinstaladomifi2();
		return $datos;
		}

		public function sp_verclientegestiondelrosal($cliente, $fechadetalle, $namearchivo){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fechadetalle);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->sp_verclientegestiondelrosal();
		return $datos;
			}

		public function sp_buscargestionservicioderosal($cliente,$fecha,$origen){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
		$this->serviciotec->set("origen",$origen);
		$datos=$this->serviciotec->sp_buscargestionservicioderosal();
		return $datos;
		}

		public function sp_buscarafiliadoRifgestionrosal($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarafiliadoRifgestionrosal();
		return $datos;
	}

		public function mostrarserialafiliadoinstalado03($idcliente,$idsecuencial){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$datos=$this->serviciotec->mostrarserialafiliadoinstalado03();
		return $datos;
		}

	public function mostrarserialafiliadoinstalado3($idcliente,$idsecuencial,$serialpos){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$this->serviciotec->set("serialpos",$serialpos);
		$datos=$this->serviciotec->mostrarserialafiliadoinstalado3();
		return $datos;
		}

		public function sp_buscarregion($usuario){
			$this->serviciotec->set("usuario",$usuario);
			$datos=$this->serviciotec->sp_buscarregion();
			return $datos;
		}

		public function spmostrarserialmifi2($idcliente,$idsecuencial){
			$this->serviciotec->set("idcliente",$idcliente);
			$this->serviciotec->set("idsecuencial",$idsecuencial);
			$datos=$this->serviciotec->spmostrarserialmifi2();
			return $datos;
			}

		public function mostrarserialafiliado($idcliente,$idsecuencial){
			//$this->serviciotec->set("serialpos",$serialpos);
			//$this->serviciotec->set("nroafiliacion",$nroafiliacion);
			$this->serviciotec->set("idcliente",$idcliente);
			$this->serviciotec->set("idsecuencial",$idsecuencial);
			$datos=$this->serviciotec->mostrarserialafiliado();
			return $datos;
			}

	public function sp_consultariesgomedido($banco,$cliente){
		$this->serviciotec->set("banco",$banco);
		$this->serviciotec->set("cliente",$cliente);
		$datos=$this->serviciotec->sp_consultariesgomedido();
		return $datos;
	    }
		
    }
?>