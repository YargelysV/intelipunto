<?php
	session_start();
	include_once("../model/gestionComercializacion.php");

	class controladorComercializacion{

		private $comercializacion;

		public function __construct(){
		$this->comercializacion= new Comercializacion();
		}

		public function verclientecomercializacionbuscar ($banco, $fecha, $namearchivo, $tipousuario, 
		$ejecutivo, $usuario,$coordinador,$coordinadordos)
	{
		$this->comercializacion->set("PrefijoBanco",$banco);
		$this->comercializacion->set("FechaRecepcionArchivo",$fecha);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->verclientecomercializacionbuscar();
		return $datos;
	}
		public function sp_verestatusgestion($operacion,$id_registro,$namearchivo)
		{
	    $this->comercializacion->set("Operacion",$operacion);
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("namearchivo",$namearchivo);
		$datos=$this->comercializacion->sp_verestatusgestion();
		return $datos;
		}

		public function sp_verestatusgestioncontacto($id_registro)
		{
		$this->comercializacion->set("id_registro",$id_registro);
		$datos=$this->comercializacion->sp_verestatusgestioncontacto();
		return $datos;
		}	

		public function spbuscarclientecomercializacion($cliente, $fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->comercializacion->set("cliente",$cliente);
		$this->comercializacion->set("FechaRecepcionArchivo",$fecha);
		$this->comercializacion->set("origen",$origen);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->sp_buscarclientecomercializacion();
		return $datos;
		}

		public function sp_buscarclientecomercializacionRif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->comercializacion->set("Rif",$rif);
		$this->comercializacion->set("tipousuario",$tipousuario);
		$this->comercializacion->set("ejecutivo",$ejecutivo);
		$this->comercializacion->set("usuario",$usuario);
		$this->comercializacion->set("coordinador",$coordinador);
		$this->comercializacion->set("coordinadordos",$coordinadordos);
		$datos=$this->comercializacion->sp_buscarclientecomercializacionRif();
		return $datos;
	    }

	    public function sp_verestatuscomodato($id_registro)
		{
		$this->comercializacion->set("id_registro",$id_registro);
		$datos=$this->comercializacion->sp_verestatuscomodato();
		return $datos;
		}
	// 
	public function spverclientesbancos(){
		$datos=$this->comercializacion->spverclientesbancos();
		return $datos;
	}

	// 
	public function spverclientestbancos(){
		$datos=$this->comercializacion->spverclientestbancos();
		return $datos;
	}

	// 
	public function formadepago(){
		$datos=$this->comercializacion->formadepago();
		return $datos;
	}

	public function sp_agregargestioncontacto($id_registro, $fechaEstatusGest, $selectEstatusGest,$observacionesEstatusGest, $usuario){
		$this->comercializacion->set("id_registro",$id_registro);
		$this->comercializacion->set("fechaEstatusGest",$fechaEstatusGest);
		$this->comercializacion->set("selectEstatusGest",$selectEstatusGest);
		$this->comercializacion->set("observacionesEstatusGest",$observacionesEstatusGest);
		$this->comercializacion->set("usuario",$usuario);
		$datos=$this->comercializacion->sp_agregargestioncontacto();
		return $datos;
	    }

	}
	


?>
