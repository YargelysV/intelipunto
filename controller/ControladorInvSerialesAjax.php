<?php
//session_start();
include_once("../model/Adjuntar.php");

class controladorAdjuntar{

	private $adjuntar;

	public function __construct(){
	$this->adjuntar= new Adjuntar();
	}

	public function sp_buscarserialespos($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_buscarserialespos();
		return $datos;
	}

	public function sp_buscarserialessimcards($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_buscarserialessimcards();
		return $datos;
	}

	public function sp_buscarserialesmifi($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_buscarserialesmifi();
		return $datos;
	}

	public function sp_buscarinventariopos(){
		$datos=$this->adjuntar->sp_buscarinventariopos();
		return $datos;
	}

	public function sp_reportedetalladopos($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_reportedetalladopos();
		return $datos;
	}

	public function sp_reportegeneralpos($fecha,$marcapos,$operacion){
		$this->adjuntar->set("fecha",$fecha);
		$this->adjuntar->set("marcapos",$marcapos);
		$this->adjuntar->set("operacion",$operacion);
		$datos=$this->adjuntar->sp_reportegeneralpos();
		return $datos;
	}
}
?>