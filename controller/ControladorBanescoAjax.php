<?php
//session_start();
include_once("../../../model/ModuloBanesco.php");
class moduloBanesco{

	private $banescoo;

	public function __construct(){
		$this->banesco= new Banesco();
	}


	public function buscarlote(){
		$datos=$this->banesco->buscarlote();
		return $datos;
	}

	public function spverbancos($valor){
		$this->banesco->set("valor",$valor);
		$datos=$this->banesco->spverbancos();
		return $datos;
		}

	public function buscarlotes($idlote){
		$this->banesco->set("idlote",$idlote);
		$datos=$this->banesco->buscarlotes();
		return $datos;

	}	

	public function actualizarserial($serial,$usuariocarga,$idestatus){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$this->banesco->set("idestatus",$idestatus);
		$datos=$this->banesco->actualizarserial();
		return $datos;

	}	

	public function sp_guardarstatusllaves($idlote,$usuariocarga,$statusllaves){
		$this->banesco->set("idlote",$idlote);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$this->banesco->set("statusllaves",$statusllaves);
		$datos=$this->banesco->sp_guardarstatusllaves();
		return $datos;
	}	

	public function reportebanesco($estatus){
		$this->banesco->set("estatus",$estatus);
		$datos=$this->banesco->reportebanesco();
		return $datos;
	}

	public function sp_guardarstatusapp($serial,$idestatusapp,$usuariocarga){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("idestatusapp",$idestatusapp);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarstatusapp();
		return $datos;
	}

	public function sp_guardarstatustem($serial,$idestatustem,$usuariocarga){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("idestatustem",$idestatustem);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarstatustem();
		return $datos;
	}

	public function sp_guardarstatusconfig($serial, $select,$iobserv,$usuariocarga){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("select",$select);
		$this->banesco->set("iobserv",$iobserv);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarstatusconfig();
		return $datos;
	}

	public function sp_updateconfig($serial, $usuariocarga){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_updateconfig();
		return $datos;
	}
	public function sp_guardarstatusequipobbu($serial,$idstatus,$usuariocarga){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("idstatus",$idstatus);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarstatusequipobbu();
		return $datos;
	}		

	public function sp_guardarstatusequipolotes($statusbbu,$usuariocarga){
		$this->banesco->set("statusbbu",$statusbbu);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarstatusequipolotes();
		return $datos;
	}		

	public function sp_guardarLoteApp($idlote,$usuariocarga,$statusapp){
		$this->banesco->set("idlote",$idlote);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$this->banesco->set("statusapp",$statusapp);
		$datos=$this->banesco->sp_guardarLoteApp();
		return $datos;
	}

	public function sp_guardarLoteTEM($idlote,$usuariocarga,$statustem){
		$this->banesco->set("idlote",$idlote);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$this->banesco->set("statustem",$statustem);
		$datos=$this->banesco->sp_guardarLoteTEM();
		return $datos;
	}

	public function buscarestatus(){
		$datos=$this->banesco->buscarestatus();
		return $datos;
	}

	public function buscarbanco($cliente){
		$this->banesco->set("cliente",$cliente);
		$datos=$this->banesco->buscarbanco();
		return $datos;
	}


	public function banescoafiliadosvar($variable){
		$this->banesco->set("variable",$variable);
		$datos=$this->banesco->banescoafiliadosvar();
		return $datos;
	}

	public function banescoafiliados(){
		$datos=$this->banesco->banescoafiliados();
		return $datos;
	}

	public function sp_verestatus(){
		//$this->banesco->set("seriales",$seriales);
		$datos=$this->banesco->sp_verestatus();
		return $datos;

	}

	public function sp_verestatusdesf(){
		//$this->banesco->set("seriales",$seriales);
		$datos=$this->banesco->sp_verestatusdesf();
		return $datos;

	}

	public function guardardesaf($serial,$usuariocarga,$idconsec){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$this->banesco->set("idconsec",$idconsec);
		$datos=$this->banesco->guardardesaf();
		return $datos;

	}

	public function guardardesincorporacion($serial,$usuariocarga,$idconsec, $select){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$this->banesco->set("idconsec",$idconsec);
		$this->banesco->set("select",$select);
		$datos=$this->banesco->guardardesincorporacion();
		return $datos;

	}

	public function busquedaequiposbbu($status){
		$this->banesco->set("status",$status);
		$datos=$this->banesco->busquedaequiposbbu();
		return $datos;
	}

	public function sp_guardarpercancebanesco($idconsec,$serial,$usuariocarga){
		$this->banesco->set("idconsec",$idconsec);
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarpercancebanesco();
		return $datos;
	} 


	public function sp_buscarequiposreingreso($serial){
		$this->banesco->set("serial",$serial);
		$datos=$this->banesco->sp_buscarequiposreingreso();
		return $datos;
	}

	public function actualizarestatusreingreso($serial,$usuario,$idestatus){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuario",$usuario);
		$this->banesco->set("idestatus",$idestatus);
		$datos=$this->banesco->actualizarestatusreingreso();
		return $datos;
		}

		public function sp_guardarreingresoapp($serial,$idestatusapp,$usuariocarga){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("idestatusapp",$idestatusapp);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarreingresoapp();
		return $datos;
	}

	public function sp_guardarreingresotem($serial,$idestatustem,$usuariocarga){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("idestatustem",$idestatustem);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$datos=$this->banesco->sp_guardarreingresotem();
		return $datos;
	}

	public function sp_buscarequipostaller($serial){
		$this->banesco->set("serial",$serial);
		$datos=$this->banesco->sp_buscarequipostaller();
		return $datos;
	}

}

?>



