<?php
include_once("model/RiesgoMedido.php");

class controladorRiesgo{

	private $riesgo;


	public function __construct(){
		$this->riesgo= new RiesgoMedido();
	}

	public function verprefijobanco(){
		$datos=$this->riesgo->verprefijobanco();
		return $datos;
	}


	public function vertipodepos(){
		$datos=$this->riesgo->vertipodepos();
		return $datos;
	}

	public function vermarcadepos(){
		$datos=$this->riesgo->vermarcadepos();
		return $datos;
	}

		public function vertipodelinea(){
		$datos=$this->riesgo->vertipodelinea();
		return $datos;
	}

		public function vertipodemifi(){
		$datos=$this->riesgo->vertipodemifi();
		return $datos;
	}
	
	public function insertar_tabltrans ($iproveedor,$ifechacompra,$imarcapos,$numserie, $tipopos,$iactivofijo, $ibancoasignado,$usuario,$idlotes){
		$this->riesgo->set("iproveedor",$iproveedor);
		$this->riesgo->set("ifechacompra",$ifechacompra);
		$this->riesgo->set("imarcapos",$imarcapos);
		$this->riesgo->set("numserie",$numserie);
		$this->riesgo->set("tipopos",$tipopos);
		$this->riesgo->set("iactivofijo",$iactivofijo);
		$this->riesgo->set("ibancoasignado",$ibancoasignado);
		$this->riesgo->set("usuario",$usuario);
		$this->riesgo->set("idlotes",$idlotes);
		$resultado=$this->riesgo->insertar_tabltrans();
		return $resultado;
	}

	public function sp_reporteriesgomedido($regiones,$banco,$usuario, $ejecutivoin, $tipousuario, $coordinador,$coordinadordos){
		$this->riesgo->set("regiones",$regiones);
		$this->riesgo->set("banco",$banco);
		$this->riesgo->set("usuario",$usuario);
		$this->riesgo->set("ejecutivoin",$ejecutivoin);
		$this->riesgo->set("tipousuario",$tipousuario);
		$this->riesgo->set("coordinador",$coordinador);
		$this->riesgo->set("coordinadordos",$coordinadordos);
		$datos=$this->riesgo->sp_reporteriesgomedido();
		return $datos;
	    }

	public function sp_reportecargaparametros($banco){
		$this->riesgo->set("banco",$banco);
		$datos=$this->riesgo->sp_reportecargaparametros();
		return $datos;
	    }

   	public function sp_verpagoscuotas($id_afiliado){
		$this->riesgo->set("id_afiliado",$id_afiliado);
		$datos=$this->riesgo->sp_verpagoscuotas();
		return $datos;
    }

    public function sp_verpreciopos(){
		$datos=$this->riesgo->sp_verpreciopos();
		return $datos;
	}

	public function sp_buscarregion($usuario,$area_usuario){
	  $this->riesgo->set("usuario",$usuario);
	  $this->riesgo->set("area_usuario",$area_usuario);
	  $datos=$this->riesgo->sp_buscarregion();
	  return $datos;
	}

	public function sp_buscarbancoregion($valor, $regiones){
	$this->riesgo->set("valor",$valor);
	$this->riesgo->set("regiones",$regiones);
	$datos=$this->riesgo->sp_buscarbancoregion();
	return $datos;
	}

	
}
?>
	