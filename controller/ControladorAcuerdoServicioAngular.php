<?php

include_once("../model/AcuerdoServicio.php");

	class controladorAcuerdoServicio
	{
		private $servicio;
		
		public function __construct()
		{
			$this->servicio=new Servicio();
		}
		

	public function spbuscaracuerdoservicioBF($cliente, $fecha)
	{
		$this->servicio->set("cliente",$cliente);
		$this->servicio->set("fecha",$fecha);
		$datos=$this->servicio->spbuscaracuerdoservicioBF();
		return $datos;
	}

	public function spbuscaracuerdoservicioBFgrafica($cliente, $fecha)
	{
		$this->servicio->set("cliente",$cliente);
		$this->servicio->set("fecha",$fecha);
		$datos=$this->servicio->spbuscaracuerdoservicioBFgrafica();
		return $datos;
	}

	public function spbuscaracuerdoservicioRIF($rif, $cliente, $fecha, $nombrearchivo, $afiliado)
	{
		$this->servicio->set("rif",$rif);
		$this->servicio->set("cliente",$cliente);
		$this->servicio->set("fecha",$fecha);
		$this->servicio->set("namearchivo",$nombrearchivo);
		$this->servicio->set("afiliado",$afiliado);
		$datos=$this->servicio->spbuscaracuerdoservicioRIF();
		return $datos;
	}
    	
    	public function spbuscarclienteacuerdorif($rif){
		$this->servicio->set("rif",$rif);
		$datos=$this->servicio->spbuscarclienteacuerdorif();
		return $datos;
	}	
    }
?>