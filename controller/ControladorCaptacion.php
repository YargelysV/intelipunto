<?php
include_once("model/Captacion.php");

class controladorCaptacion{

	private $captacion;


	public function __construct(){
		$this->captacion= new Captacion();
	}

	public function sp_bancosinteres(){
		$datos=$this->captacion->sp_bancosinteres();
		return $datos;
	}

	public function sp_tipo_cliente(){
		$datos=$this->captacion->sp_tipo_cliente();
		return $datos;
		}

		public function sp_ejecutivo_asignado(){
		$datos=$this->captacion->sp_ejecutivo_asignado();
		return $datos;
		}

	public function sp_registrosasignacionejecutivo($ocodigobanco){
		$this->captacion->set("ocodigobanco",$ocodigobanco);
		$datos=$this->captacion->sp_registrosasignacionejecutivo();
		return $datos;
	}

	public function spbuscarpersonasreferidas($idcliente){
			$this->captacion->set("idcliente",$idcliente);
			$datos=$this->captacion->spbuscarpersonasreferidas();
			return $datos;
			}

		public function sp_registroscaptacion($usuario){
		$this->captacion->set("usuario",$usuario);
		$datos=$this->captacion->sp_registroscaptacion();
		return $datos;
	}
		public function sp_buscarcaptacion($cliente){
		$this->captacion->set("cliente",$cliente);
		$datos=$this->captacion->sp_buscarcaptacion();
		return $datos;
	}

	public function sp_registroscaptacionxaprobar(){
		$datos=$this->captacion->sp_registroscaptacionxaprobar();
		return $datos;
	}

}
?>
	