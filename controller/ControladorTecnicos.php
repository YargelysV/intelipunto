<?php

include_once("model/Tecnicos.php");

	class controladorTecnicos
	{
		private $tecnicos;
		
		public function __construct(){
			$this->tecnicos= new Tecnicos();
		}
		
		public function mostrartecnicos(){
			$datos=$this->tecnicos->mostrartecnicos();
			return $datos;
		}    
         
    	public function agregartecnicos ($operacion, $coddocumento,$nombres, $apellidos, $codtecnicos, 
       	$ntecnicos, $usuario, $zonatrabajo)
       	{
			$this->tecnicos->set("operacion",$operacion);
			$this->tecnicos->set("coddocumento",$coddocumento);
			$this->tecnicos->set("nombres",$nombres);
			$this->tecnicos->set("apellidos",$apellidos);
			$this->tecnicos->set("codtecnico",$codtecnico);
			$this->tecnicos->set("ntecnico",$ntecnico);
			$this->tecnicos->set("zonatrabajo",$zonatrabajo);
			$resultado=$this->tecnicos->crear();
			return $resultado;
		}

		public function mostrarTecnicos1($coddocumento){
			$this->tecnicos->set("coddocumento",$coddocumento);

			$datos=$this->tecnicos->mostrarTecnicos1();
			return $datos;
		}    

		public function EditarTecnicos ($operacion, $coddocumento,$nombres, $apellidos, $codtecnico, 
       	$ntecnico, $usuario, $zonatrabajo)
		{
			$this->tecnicos->set("operacion",$operacion);
			$this->tecnicos->set("coddocumento",$coddocumento);
			$this->tecnicos->set("nombres",$nombres);
			$this->tecnicos->set("apellidos",$apellidos);
			$this->tecnicos->set("codtecnico",$codtecnico);
			$this->tecnicos->set("ntecnico",$ntecnico);			
			$this->tecnicos->set("zonatrabajo",$zonatrabajo);
			$resultado=$this->tecnicos->EditarTecnicos();
			return $resultado;
		}

	    public function EliminarTecnicos($operacion,$coddocumento)
	    {
			$this->tecnicos->set("operacion",$operacion);	
			$this->tecnicos->set("coddocumento",$coddocumento);
			$resultado=$this->tecnicos->eliminar();
			return $resultado;
		}

		public function tipodocumento(){
			$datos=$this->tecnicos->vertipodocumentos();
			return $datos;
		}

		public function sp_estatustecnico($documento){
		$this->tecnicos->set("documento",$documento);
		//$this->tecnicos->set("estatus",$estatus);
		$datos=$this->tecnicos->sp_estatustecnico();
		return $datos;

		}

		public function sp_sstatustecnico($documento,$estatus){
    		$this->tecnicos->set("documento",$documento);
    		$this->tecnicos->set("estatus",$estatus);
    		$datos=$this->tecnicos->sp_sstatustecnico();
    		return $datos;
    		}
    }
?>