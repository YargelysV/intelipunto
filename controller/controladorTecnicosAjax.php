<?php

	include_once("../../model/Tecnicos.php");

		class controladorTecnicos
		{ 
			private $tecnicos;

			public function __construct(){
				$this->tecnicos= new Tecnicos();
			}
		
			// public function sp_sstatustecnicos($documento,$estatus){
			// $this->tecnicos->set("documento",$documento);
			// $this->tecnicos->set("estatus",1);
			// $datos=$this->tecnicos->sp_sstatustecnico();
			// return $datos;
			// }

			public function sp_sstatustecnico($documento,$estatus){
    		
    		$this->tecnicos->set("documento",$documento);
    		$this->tecnicos->set("estatus",$estatus);
    		$datos=$this->tecnicos->sp_sstatustecnico();
    		return $datos;
    		}

			public function sp_buscarregion($usuario){
				$this->tecnicos->set("usuario",$usuario);
				$datos=$this->tecnicos->sp_buscarregion();
				return $datos;
			}

			public function sp_buscarbancoregion($valor, $region){
				$this->tecnicos->set("valor",$valor);
				$this->tecnicos->set("region",$region);
				$datos=$this->tecnicos->sp_buscarbancoregion();
				return $datos;
			}

			public function spverfecharegion($valor, $region){
				$this->tecnicos->set("valor",$valor);
				$this->tecnicos->set("region",$region);
				$datos=$this->tecnicos->spverfecharegion();
				return $datos;
			}

			public function spverbancoregionreporte($valor, $region){
				$this->tecnicos->set("valor",$valor);
				$this->tecnicos->set("region",$region);
				$datos=$this->tecnicos->spverbancoregionreporte();
				return $datos;
			}
		}


?>