<?php
	session_start();
	include_once("../../../model/ClientePotencial.php");

	class controladorClientePotencial{

		private $clientepotencial;

		public function __construct(){
		$this->clientepotencial= new ClientePotencial();
		}

		
		public function spverbancos($valor){
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverbancos();
		return $datos;
		}

		public function spveralmacenequipo($valor){
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spveralmacenequipo();
		return $datos;
		}

		public function spveralmacenequipo1($valor, $region){
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("region",$region);
		$datos=$this->clientepotencial->spveralmacenequipo1();
		return $datos;
		}

		public function spverestatusdemifi($valor){
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverestatusdemifi();
		return $datos;
		}

		public function spverbancosxejecutivo($usuario, $area, $tipousuario){
		$this->clientepotencial->set("usuario",$usuario);
		$this->clientepotencial->set("area",$area);
		$this->clientepotencial->set("codtipousuario",$tipousuario);
		$datos=$this->clientepotencial->spverbancosxejecutivo();
		return $datos;
		}
		public function spvernombrearchivobancos($banco, $fecha){
		$this->clientepotencial->set("banco",$banco);
		$this->clientepotencial->set("fecha",$fecha);
		$datos=$this->clientepotencial->spvernombrearchivobancos();
		return $datos;
		}

		public function spverperfiles($valor){
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverperfiles();
		return $datos;
		}

		public function spverbancosdef($valor, $cliente){
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("cliente",$cliente);
		$datos=$this->clientepotencial->spverbancosdef();
		return $datos;
		}

		public function spverfechaxbanco($banco, $valor){
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverfechaxbanco();
		return $datos;
		}

		public function spverfechaxbancoxregion($valor,$banco, $region){
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("region",$region);
		$datos=$this->clientepotencial->spverfechaxbancoxregion();
		return $datos;
		}

		public function spverbancoxfecha($valor, $fecha){
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->clientepotencial->spverbancoxfecha();
		return $datos;
		}

		public function spvernombreArchivorestaurar($banco, $fecha){
		$this->clientepotencial->set("banco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->clientepotencial->spvernombreArchivorestaurar();
		return $datos;
		}
		

		public function speliminararchivocliente($banco, $fecha, $nombrearchivo, $tipousuario, $login){
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$this->clientepotencial->set("nombrearchivo", $nombrearchivo);
		$this->clientepotencial->set("codtipousuario",$tipousuario);
		$this->clientepotencial->set("login",$login);
		$datos=$this->clientepotencial->speliminararchivocliente();
		return $datos;
		}

		public function spverbancosrestaurar($valor){
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverbancosrestaurar();
		return $datos;
		}

		public function spverfechaxbancorestaurar($banco, $valor){
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverfechaxbancorestaurar();
		return $datos;
		}

		public function sprestaurararchivocliente($banco, $fecha, $login, $nombrearchivo){
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$this->clientepotencial->set("login",$login);
		$this->clientepotencial->set("nombrearchivo",$nombrearchivo);
		$datos=$this->clientepotencial->sprestaurararchivocliente();
		return $datos;
		}

		public function sp_buscarclienterecibido($cliente){
		$this->clientepotencial->set("cliente",$cliente);
		$datos=$this->clientepotencial->sp_buscarclienterecibido();
		return $datos;
		}

		
		public function verclientecargados ($banco)
		{
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$datos=$this->clientepotencial->verclientecargados();
		return $datos;
		}

		public function sp_buscarclienter($cliente){
		$this->clientepotencial->set("cliente",$cliente);
		$datos=$this->clientepotencial->sp_buscarclienter();
		return $datos;
		}

		public function verclientepotencial ($banco, $fecha, $namearchivo)
		{
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$this->clientepotencial->set("name",$namearchivo);
		$datos=$this->clientepotencial->verclientepotencial();
		return $datos;
		}

		public function spverfecha($valor){
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverfecha();
		return $datos;
		}

		public function spverejecutivo($valor){
		$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverejecutivo();
		return $datos;
		}
		public function spverejecutivoxbanco($valor,$banco){
		
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$datos=$this->clientepotencial->spverejecutivoxbanco();
		return $datos;
		}	

		public function spverejecutivobco($banco,$valor){
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("cliente",$cliente);
		$datos=$this->clientepotencial->spverejecutivobco();
		return $datos;
		}

		public function spverbancoxejecutivo($valor, $ejecutivo){
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("ejecutivo",$ejecutivo);
		$datos=$this->clientepotencial->spverbancoxejecutivo();
		return $datos;
		}

		public function sp_buscarclienterecibidos($banco, $fecha){
		$this->clientepotencial->set("banco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->clientepotencial->sp_buscarclienterecibidos();
		return $datos;
	}

	public function spverbancoregionreporte($valor, $region){
		$this->clientepotencial->set("valor",$valor);
		$this->clientepotencial->set("region",$region);
		$datos=$this->clientepotencial->spverbancoregionreporte();
		return $datos;
	}

		public function spvermodalidad(){
		//$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spvermodalidad();
		return $datos;
		}

		public function spverestatusfacturacion(){
		//$this->clientepotencial->set("valor",$valor);
		$datos=$this->clientepotencial->spverestatusfacturacion();
		return $datos;
		}

	}


?>

