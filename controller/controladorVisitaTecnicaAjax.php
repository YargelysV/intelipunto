<?php

include_once("../../model/VisitaTecnica.php");

	class controladorVisita
	{
		private $visita;
		
		public function __construct(){
			$this->visita= new VisitaTecnica();
		}
		
		public function mostrartecnicos(){
			$datos=$this->visita->mostrartecnicos();
			return $datos;
		} 

		public function sp_mostrarvisitatecnica ($banco,$fechavisita, $nrorif)
		{
			$this->visita->set("banco",$banco);
			$this->visita->set("fechavisita",$fechavisita);
			$this->visita->set("nrorif",$nrorif);
			$datos=$this->visita->sp_mostrarvisitatecnica();
			return $datos;
		}

		public function sp_mostrarvisitatecnicaxrif ($busqueda)
		{
			$this->visita->set("busqueda",$busqueda);
			$datos=$this->visita->sp_mostrarvisitatecnicaxrif();
			return $datos;
		}

		public function sp_mostrarvisitatecnicaxfechas ($fechainicial,$fechafinal)
		{
			$this->visita->set("fechainicial",$fechainicial);
			$this->visita->set("fechafinal",$fechafinal);
			$datos=$this->visita->sp_mostrarvisitatecnicaxfechas();
			return $datos;
		} 

		public function sp_mostrargestionxfechas ($fechainicial,$fechafinal)
		{
			$this->visita->set("fechainicial",$fechainicial);
			$this->visita->set("fechafinal",$fechafinal);
			$datos=$this->visita->sp_mostrargestionxfechas();
			return $datos;
		} 

		public function sp_mostrarreportevisitatecnica ($banco)
		{
			$this->visita->set("banco",$banco);
			$datos=$this->visita->sp_mostrarreportevisitatecnica();
			return $datos;
		}

		public function spverdetallevisitatecnica ($idcliente,$serialpos)
		{
			$this->visita->set("idcliente",$idcliente);
			$this->visita->set("serialpos",$serialpos);
			$datos=$this->visita->spverdetallevisitatecnica();
			return $datos;
		}

		public function spverdetallevisitatecnica1 ($idcliente,$serialpos)
		{
			$this->visita->set("idcliente",$idcliente);
			$this->visita->set("serialpos",$serialpos);
			$datos=$this->visita->spverdetallevisitatecnica1();
			return $datos;
		}
   
   		public function sp_mostrargestionvisita ($idcliente,$idsecuencial)
		{
			$this->visita->set("idcliente",$idcliente);
			$this->visita->set("idsecuencial",$idsecuencial);
			$datos=$this->visita->sp_mostrargestionvisita();
			return $datos;
		}

		public function spestatusvisita ()
		{
			//$this->visita->set("codestatus",$codestatus);
			//$this->visita->set("serialpos",$serialpos);
			$datos=$this->visita->spestatusvisita();
			return $datos;
		}

		public function sp_mostrartecnicovisitatec($idcliente){
			$this->visita->set("idcliente",$idcliente);
			$datos=$this->visita->sp_mostrartecnicovisitatec();
			return $datos;
		}

		public function sprelacion_condstatus ($idvisita)
		{
			$this->visita->set("idvisita",$idvisita);
			$datos=$this->visita->sprelacion_condstatus();
			return $datos;
		}

		public function selecttecnicosvisita(){
		$datos=$this->visita->selecttecnicosvisita();
		return $datos;
		}

		public function sp_guardarvisita($idcliente,$serialpos,$fechagesti,$estatusvisita,$tecnico,$fechavisita,$usuario,$observacion,$idcondicion,$idsecuencial){
		$this->visita->set("idcliente",$idcliente);
		$this->visita->set("serialpos",$serialpos);
		$this->visita->set("fechagesti",$fechagesti);
		$this->visita->set("estatusvisita",$estatusvisita);
		$this->visita->set("tecnico",$tecnico);
		$this->visita->set("fechavisita",$fechavisita);
		$this->visita->set("usuario",$usuario);
		$this->visita->set("observacion",$observacion);
		$this->visita->set("idcondicion",$idcondicion);
		$this->visita->set("idsecuencial",$idsecuencial);	
		$datos=$this->visita->sp_guardarvisita();
		return $datos;
		}
         
    	public function sp_guardaraprobacionvisita($sec,$idaprob){
		$this->visita->set("sec",$sec);
		$this->visita->set("idaprob",$idaprob);	
		$datos=$this->visita->sp_guardaraprobacionvisita();
		return $datos;
		}   

		public function sp_guardarinfadicional($tlf1,$tlf2,$email,$nombre2,$apellido2,$usuario,$banco,$cliente){
		$this->visita->set("tlf1",$tlf1);
		$this->visita->set("tlf2",$tlf2);
		$this->visita->set("email",$email);
		$this->visita->set("nombre2",$nombre2);
		$this->visita->set("apellido2",$apellido2);
		$this->visita->set("usuario",$usuario);
		$this->visita->set("banco",$banco);
		$this->visita->set("cliente",$cliente);
		$datos=$this->visita->sp_guardarinfadicional();
		return $datos;
		}

		public function direccionadicional($coddocumento,$codetipodir,$e_calle_av,$e_localidad,$e_sector,$e_nlocal,$e_urbanizacion,$e_estado,$e_codepostal,$e_ptoref,$prefijo,$usuario,$id_registro){
		$this->visita->set("coddocumento",$coddocumento);
		$this->visita->set("codetipodir",$codetipodir);
		$this->visita->set("e_calle_av",$e_calle_av);
		$this->visita->set("e_localidad",$e_localidad);
		$this->visita->set("e_sector",$e_sector);
		$this->visita->set("e_nlocal",$e_nlocal);
		$this->visita->set("e_urbanizacion",$e_urbanizacion);
		$this->visita->set("e_estado",$e_estado);
		$this->visita->set("e_codepostal",$e_codepostal);
		$this->visita->set("e_ptoref",$e_ptoref);
		$this->visita->set("prefijo",$prefijo);
		$this->visita->set("usuario",$usuario);
		$this->visita->set("id_registro",$id_registro);
		$resultado=$this->visita->direccionadicional();
		return $resultado;
	}

    }
?>