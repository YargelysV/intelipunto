<?php
	//session_start();
	include_once("../model/Tecnicos.php");

	class controladorTecnicos{

		private $tecnicos;

		public function __construct(){
			$this->tecnicos= new Tecnicos();
		}

		public function mostrartecnicos(){
			$datos=$this->tecnicos->mostrartecnicos();
			return $datos;
		}

		public function crear ($operacion,$zonatrabajo, $tipodoc, $coddocumento, $nombres, $apellidos, $usuario)
        	{
				$this->tecnicos->set("operacion",$operacion);
				$this->tecnicos->set("trabajo",$zonatrabajo);
				$this->tecnicos->set("tipodoc",$tipodoc);
				$this->tecnicos->set("coddocumento",$coddocumento);
				$this->tecnicos->set("nombres",$nombres);
				$this->tecnicos->set("apellidos",$apellidos);
				$this->tecnicos->set("usuario",$usuario);
				
				$resultado=$this->tecnicos->crear();
				return $resultado;
			}

		public function EditarTecnicos ($operacion,$trabajo,  $tipodoc, $coddocumento, 
        	$nombres, $apellidos, $usuario)
        	{
				$this->tecnicos->set("operacion",$operacion);
				$this->tecnicos->set("trabajo",$trabajo);
				$this->tecnicos->set("tipodoc",$tipodoc);
				$this->tecnicos->set("coddocumento",$coddocumento);
				$this->tecnicos->set("nombres",$nombres);
				$this->tecnicos->set("apellidos",$apellidos);
				$this->tecnicos->set("usuario",$usuario);
				$resultado=$this->tecnicos->EditarTecnicos();
				return $resultado;
			}
			
		public function EliminarTecnicos($operacion,$coddocumento)
		    {
				$this->tecnicos->set("operacion",$operacion);
				$this->tecnicos->set("coddocumento",$coddocumento);	
				$resultado=$this->tecnicos->EliminarTecnicos();
				return $resultado;
			}	

		public function sp_verclienteafiliado($cliente, $fechadetalle, $namearchivo)
			{
				$this->tecnicos->set("cliente",$cliente);
				$this->tecnicos->set("fecha",$fechadetalle);
				$this->tecnicos->set("namearchivo",$namearchivo);
				$resultado=$this->tecnicos->sp_verclienteafiliado();
				return $resultado;
			}

			public function sp_buscarservitec($cliente,$fecha,$origen){
				$this->tecnicos->set("cliente",$cliente);
				$this->tecnicos->set("FechaRecepcionArchivo",$fecha);
				$this->tecnicos->set("origen",$origen);
				$datos=$this->tecnicos->sp_buscarservitec();
				return $datos;
			}

			public function sp_buscarafiliadoRif($rif){
				$this->tecnicos->set("rif",$rif);
				$datos=$this->tecnicos->sp_buscarafiliadoRif();
				return $datos;
			}

			public function mostrarinventarioposafiliado($idcliente, $busqueda){
				$this->tecnicos->set("idcliente",$idcliente);
				$this->tecnicos->set("busqueda",$busqueda);
				$datos=$this->tecnicos->mostrarinventarioposafiliado();
				return $datos;
			}

			public function spasignarserialpos ($correlativo,$fechaent, $numterminal, $serialposasignado,$numsim,$namearchivo,$usuario,$numamexx,$idregistro,$banco,$selectperfil,$selectversion,$orservicio,$nafiliado)
			{
				$this->tecnicos->set("correlativo",$correlativo);
				$this->tecnicos->set("fechaent",$fechaent);
				$this->tecnicos->set("numterminal", $numterminal);
				$this->tecnicos->set("serialpos",$serialposasignado);
				$this->tecnicos->set("numsim", $numsim);
				$this->tecnicos->set("namearchivo",$namearchivo);
				$this->tecnicos->set("usuariocarg",$usuario);
				$this->tecnicos->set("numeroamex",$numamexx);
				$this->tecnicos->set("idregistro",$idregistro);
				$this->tecnicos->set("banco",$banco);
				$this->tecnicos->set("selectperfil",$selectperfil);
				$this->tecnicos->set("selectversion",$selectversion);
				$this->tecnicos->set("orservicio",$orservicio);
				$this->tecnicos->set("nafiliado",$nafiliado);
				$resultado=$this->tecnicos->spasignarserialpos();
				return $resultado;
			}

			public function sp_verclientegestiondelrosal($banco, $fechadetalle, $namearchivo)
			{
				$this->tecnicos->set("cliente",$banco);
				$this->tecnicos->set("fecha",$fechadetalle);
				$this->tecnicos->set("namearchivo",$namearchivo);
				$datos=$this->tecnicos->sp_verclientegestiondelrosal();
				return $datos;
			}
			
			public function sp_verclientegestion($cliente, $fechadetalle, $namearchivo, $region)
			{
				$this->tecnicos->set("cliente",$cliente);
				$this->tecnicos->set("fecha",$fechadetalle);
				$this->tecnicos->set("namearchivo",$namearchivo);
				$this->tecnicos->set("region",$region);
				$datos=$this->tecnicos->sp_verclientegestion();
				return $datos;
			}

			public function sp_buscargestionservicio($cliente,$fecha, $origen){
				$this->tecnicos->set("cliente",$cliente);
				$this->tecnicos->set("FechaRecepcionArchivo",$fecha);
				$this->tecnicos->set("origen",$origen);
				$datos=$this->tecnicos->sp_buscargestionservicio();
				return $datos;
			}

			public function sp_buscarafiliadoRifgestion($rif,$usuario){
				$this->tecnicos->set("rif",$rif);
				$this->tecnicos->set("usuario",$usuario);
				$datos=$this->tecnicos->sp_buscarafiliadoRifgestion();
				return $datos;
			}	

			public function mostrarserialafiliados($idcliente,$idsecuencial,$usuario){
				//$this->serviciotec->set("serialpos",$serialpos);
				//$this->serviciotec->set("nroafiliacion",$nroafiliacion);
				$this->tecnicos->set("idcliente",$idcliente);
				$this->tecnicos->set("idsecuencial",$idsecuencial);
				$this->tecnicos->set("idusuario",$usuario);
				$datos=$this->tecnicos->mostrarserialafiliados();
				return $datos;
			}

			public function spactualizargestionpos($operacion,$serial,$nroafiliacion,$namearchivo, $usuario, $fecharecepalma, $tecnico, $fechagest, $estatus, $fechainst, $fechacarga, $observacion, $idcliente,$fechalog,$fechaenv,$banco, $representantelegal, $personacontacto,$mifiserial){
				$this->tecnicos->set("operacion",$operacion);
				$this->tecnicos->set("serialpos",$serial);
				$this->tecnicos->set("nroafiliacion",$nroafiliacion);
				$this->tecnicos->set("namearchivo",$namearchivo);
				$this->tecnicos->set("usuario",$usuario);
				$this->tecnicos->set("fecharecepalma",$fecharecepalma);
				$this->tecnicos->set("tecnico",$tecnico);
				$this->tecnicos->set("fechagest",$fechagest);
				$this->tecnicos->set("estatus",$estatus);
				$this->tecnicos->set("fechainst",$fechainst);
				$this->tecnicos->set("fechacarg",$fechacarga);
				$this->tecnicos->set("observacion",$observacion);
				$this->tecnicos->set("idcliente",$idcliente);
				$this->tecnicos->set("fechalog",$fechalog);
				$this->tecnicos->set("fechaenv",$fechaenv);
				$this->tecnicos->set("banco",$banco);
				$this->tecnicos->set("representantelegal",$representantelegal);
				$this->tecnicos->set("personacontacto",$personacontacto);
				$this->tecnicos->set("mifiserial",$mifiserial);
				$datos=$this->tecnicos->spactualizargestionpos();
				return $datos;
				}


			public function selecttecnicos(){
				$datos=$this->tecnicos->selecttecnicos();
				return $datos;
			}
			
			public function sp_verclientedetalladomifi($cliente, $fechadetalle, $namearchivo){
			$this->tecnicos->set("cliente",$cliente);
			$this->tecnicos->set("fecha",$fechadetalle);
			$this->tecnicos->set("namearchivo",$namearchivo);
			$datos=$this->tecnicos->sp_verclientedetalladomifi();
			return $datos;
			}

			public function spmostrarserialmifi2($idcliente,$idsecuencial){
			$this->tecnicos->set("idcliente",$idcliente);
			$this->tecnicos->set("idsecuencial",$idsecuencial);
			$datos=$this->tecnicos->spmostrarserialmifi2();
			return $datos;
			}

			public function spagregargestionmifi($operacion,$serialmifi,$namearchivo, $usuario, $fecharecepalma, $tecnico, $fechagest, $estatus, $fechainst, $fechacarga, $observacion, $idcliente){
			$this->tecnicos->set("operacion",$operacion);
			$this->tecnicos->set("serialmifi",$serialmifi);
			$this->tecnicos->set("namearchivo",$namearchivo);
			$this->tecnicos->set("usuario",$usuario);
			$this->tecnicos->set("fecharecepalma",$fecharecepalma);
			$this->tecnicos->set("tecnico",$tecnico);
			$this->tecnicos->set("fechagest",$fechagest);
			$this->tecnicos->set("estatus",$estatus);
			$this->tecnicos->set("fechainst",$fechainst);
			$this->tecnicos->set("fechacarg",$fechacarga);
			$this->tecnicos->set("observacion",$observacion);
			$this->tecnicos->set("idcliente",$idcliente);

			$datos=$this->tecnicos->spagregargestionmifi();
			return $datos;
			}

			public function selectestatusinstmifi($codestatus){
			$this->tecnicos->set("codestatus",$codestatus);
			$datos=$this->tecnicos->selectestatusinstmifi();
			return $datos;
			}

			public function sp_buscarxfechamifi($cliente,$fecha,$origen){
			$this->tecnicos->set("cliente",$cliente);
			$this->tecnicos->set("FechaRecepcionArchivo",$fecha);
			$this->tecnicos->set("origen",$origen);
			$datos=$this->tecnicos->sp_buscarxfechamifi();
			return $datos;
			}

			public function verclienteserviciotecnico($cliente, $fecha, $namearchivo){
			$this->tecnicos->set("cliente",$cliente);
			$this->tecnicos->set("fecha",$fecha);
			$this->tecnicos->set("namearchivo",$namearchivo);
			$datos=$this->tecnicos->verclienteserviciotecnico();
			return $datos;
			}

			public function sp_buscarclientemifixrif($rif){
			$this->tecnicos->set("rif",$rif);
			$datos=$this->tecnicos->sp_buscarclientemifixrif();
			return $datos;
			}

			public function eliminargestion($idafiliado,$idsecuencial){
				$this->tecnicos->set("idafiliado",$idafiliado);
				$this->tecnicos->set("idsecuencial",$idsecuencial);
				$datos=$this->tecnicos->eliminargestion();
				return $datos;
			}	
		}

?>
