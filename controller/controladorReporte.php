<?php

include_once("model/Reporte.php");

class ControladorReporte{

	private $reporte;

	public function __construct(){
		$this->reporte= new Reporte();
	}

	public function sp_comercializacionbuscar($operacion,$cliente, $fecha){
		$this->reporte->set("Operacion",$operacion);
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->reporte->sp_comercializacionbuscar();
		return $datos;
	}

  

	public function spbanco($cliente){
		$this->reporte->set("cliente",$cliente);
		$datos=$this->reporte->spbanco();
		return $datos;
	}


	public function sp_buscarclientecomercializacion($cliente, $fecha){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->reporte->sp_buscarclientecomercializacion();
		return $datos;
	}

	public function verclientecomercializacionbuscar ($banco, $fecha, $namearchivo)
	{
		$this->reporte->set("PrefijoBanco",$banco);
		$this->reporte->set("FechaRecepcionArchivo",$fecha);
		$this->reporte->set("namearchivo",$namearchivo);
		$datos=$this->reporte->verclientecomercializacionbuscar();
		return $datos;
	}
	public function sp_buscarclientecomercializacionRif($rif){
			$this->reporte->set("Rif",$rif);
			$datos=$this->reporte->sp_buscarclientecomercializacionRif();
			return $datos;
	}


		public function comercializacionreporte($cliente,$fecha){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->reporte->comercializacionreporte();
		return $datos;
		}

	public function comercializacionreportedetallado ($banco, $fechaenvio, $fecharecepcion)
	{
	$this->reporte->set("PrefijoBanco",$banco);
	$this->reporte->set("fechaenvio",$fechaenvio);
	$this->reporte->set("fecharecepcion",$fecharecepcion);
	$datos=$this->reporte->comercializacionreportedetallado();
	return $datos;
	}



	public function verclientescomercializacion ()
	{
	$datos=$this->reporte->verclientescomercializacion();
	return $datos;
	}

	public function verafiliadosasignados ()
	{
	$datos=$this->reporte->verafiliadosasignados();
	return $datos;
	}

	public function verafiliadosasignadosdetalle ($afiliado)
	{
	$this->reporte->set("NombreAfiliado",$afiliado);
	$datos=$this->reporte->verafiliadosasignadosdetalle();
	return $datos;
	}
	public function spmostrarclientefacturacion($coddocumento,$cliente,$fecha,$namearchivo){
		$this->reporte->set("coddocumento",$coddocumento);
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("fecharecepcion",$fecha);
		$this->reporte->set("namearchivo",$namearchivo);
		$datos=$this->reporte->spmostrarclientefacturacion();
		return $datos;
	}

	
	public function insertar_afiliado($consecutivo, $registro, $fechaenvio, $cliente, $PrefijoBanco, $tipopv, $cantpv, $tipolinea, $tipomodelonegocio, $tipcliente, $razonsocial, $rif, $afiliado, $usuario){
		$this->reporte->set("Consecutivo",$consecutivo);
		$this->reporte->set("registro",$registro);
		$this->reporte->set("fechaenvio",$fechaenvio);
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("PrefijoBanco",$PrefijoBanco);
		$this->reporte->set("cantpv",$cantpv);
		$this->reporte->set("tipolinea",$tipolinea);
		$this->reporte->set("tipomodelonegocio",$tipomodelonegocio);
		$this->reporte->set("tipcliente",$tipcliente);
		$this->reporte->set("razonsocial",$razonsocial);
		$this->reporte->set("Rif",$rif);
		$this->reporte->set("NombreAfiliado",$afiliado);
		$this->reporte->set("usuario",$usuario);
		$datos=$this->reporte->insertar_afiliado();
		return $datos;
	}

	public function sp_buscarreporte($cliente,$fecha){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->reporte->sp_buscarreporte();
		return $datos;
		}	

	public function sp_reporteadm($cliente,$fecha,$namearchivo){
		$this->reporte->set("cliente", $cliente);
		$this->reporte->set("fecha", $fecha);
		$this->reporte->set("namearchivo", $namearchivo);
		$datos=$this->reporte->sp_reporteadm();
		}

		
		public function sp_buscarreporteerp($cliente,$fecha){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->reporte->sp_buscarreporteerp();
		return $datos;
		}	

		public function sp_reporteerp($cliente, $fecha, $namearchivo){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("fecha",$fecha);
		$this->reporte->set("namearchivo",$namearchivo);
		$datos=$this->reporte->sp_reporteerp();
		return $datos;
		}

		public function sp_buscarreportepos($cliente,$fecha){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("FechaRecepcionArchivo",$fecha);
		$datos=$this->reporte->sp_buscarreportepos();
		return $datos;
		}	

		public function sp_buscarreporteequipos($clientes,$fechainicial,$fechafinal){
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_buscarreporteequipos();
		return $datos;
		}	
/*
		public function sp_buscarreportefecha($fechainicial,$fechafinal){
		//$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_buscarreportefecha();
		return $datos;
		}*/	

		public function sp_reporteequipos($clientes,$fecha){
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fecha",$fecha);
			
		$datos=$this->reporte->sp_reporteequipos();
		return $datos;
		}	

		public function sp_reporteequipospos($clientes,$fecha,$marca,$tipo){
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fecha",$fecha);
		$this->reporte->set("marca",$marca);
		$this->reporte->set("tipo",$tipo);
		$datos=$this->reporte->sp_reporteequipospos();
		return $datos;
		}

		public function sp_reportexdiasdetalles($banco, $usuarioeje, $tipopos, $fechainicial, $fechafinal, $tiporeporte){
		$this->reporte->set("banco",$banco);
		$this->reporte->set("usuarioeje",$usuarioeje);
		$this->reporte->set("tipoposdetalles",$tipopos);
		$this->reporte->set("fechainicio",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$this->reporte->set("tiporeporte",$tiporeporte);
		$datos=$this->reporte->sp_reportexdiasdetalles();
		return $datos;
		}
		public function sp_reportepos($cliente, $fecha, $namearchivo){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("fecha",$fecha);
		$this->reporte->set("namearchivo",$namearchivo);
		$datos=$this->reporte->sp_reportepos();
		return $datos;
		}

		public function sp_buscarreporteejecutivo($clientes,$ejecutivo,$fechainicial,$fechafinal){
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("ejecutivo",$ejecutivo);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_buscarreporteejecutivo();
		return $datos;
		}	

		public function sp_reporteejecutivo($cliente,$ejecutivo, $fechainicial,$fechafinal){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("ejecutivo",$ejecutivo);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reporteejecutivo();
		return $datos;
		}

			public function sp_reporteejecutivogestiones($fechainicial,$fechafinal)
		{
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reporteejecutivogestiones();
		return $datos;
	}

		public function sp_soloreporte($cliente,$ejecutivo,$fechainicial,$fechafinal){
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("ejecutivo",$ejecutivo);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_soloreporte();
		return $datos;
		}

		public function sp_ejecutivos($clientes,$estatus,$documento){
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("estatus",$estatus);
		$this->reporte->set("documento",$documento);
		$datos=$this->reporte->sp_ejecutivos();
		return $datos;
		}

		public function spverclienteejecutivo($coddocumento){
		$this->reporte->set("coddocumento",$coddocumento);
		$datos=$this->reporte->spverclienteejecutivo();
		return $datos;
		}
		public function sp_reportediarioxbanco($clientes){
		$this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_reportediarioxbanco();
		return $datos;
		}
		public function sp_reportexdias($clientes,$ejecutivo,$fechainicial,$fechafinal,$tiporeporte)
		{
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("ejecutivo",$ejecutivo);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$this->reporte->set("tiporeporte",$tiporeporte);
		$datos=$this->reporte->sp_reportexdias();
		return $datos;
		}

		public function sp_reportegrupal($gerente,$fechainicial,$fechafinal)
		{
		$this->reporte->set("gerente",$gerente);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reportegrupal();
		return $datos;
		}

		public function sp_reportegrupaldetalles($banco,$tipopos,$fechainicial,$fechafinal)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("tipopos",$tipopos);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reportegrupaldetalles();
		return $datos;
		}
		public function sp_detallesreportexpos($marcapos,$fechainicial,$fechafinal)
		{
		$this->reporte->set("detallemarcapos",$marcapos);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_detallesreportexpos();
		return $datos;
		}

		public function sp_reportexpos ($POS,$fechainicial,$fechafinal)
		{
		$this->reporte->set("POS",$POS);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reportexpos();
		return $datos;
		}

		public function	sp_reportefacturatotal()
		{
		$datos=$this->reporte->sp_reportefacturatotal();
		return $datos;
		}

		public function	sp_reportexnumfactura($factura)
		{
		$this->reporte->set("factura",$factura);
		$datos=$this->reporte->sp_reportexnumfactura();
		return $datos;
		}
			public function sp_reportediarioestatus ($banco)
		{
		$this->reporte->set("banco",$banco);

		$datos=$this->reporte->sp_reportediarioestatus();
		return $datos;
		}

			public function sp_reportegestionesdemifi ($banco,$sitio)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportegestionesdemifi();
		return $datos;
		}

		public function sp_reportediariogeneraldemifi ($banco)
		{
		$this->reporte->set("banco",$banco);
		$datos=$this->reporte->sp_reportediariogeneraldemifi();
		return $datos;
		}

		public function sp_reportediarioestatusgeneral ($banco, $region, $modalidad)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("region",$region);
		$this->reporte->set("modalidad",$modalidad);

		$datos=$this->reporte->sp_reportediarioestatusgeneral();
		return $datos;
		}

		public function sp_reportedomiciliaciongeneral ($banco)
		{
		$this->reporte->set("banco",$banco);
		$datos=$this->reporte->sp_reportedomiciliaciongeneral();
		return $datos;
		}

		public function sp_reportediarioequiposvendidos ($banco)
		{
		$this->reporte->set("banco",$banco);

		$datos=$this->reporte->sp_reportediarioequiposvendidos();
		return $datos;
		}

		public function sp_reportediarioequiposinstalados ($banco,$fechainicial,$fechafinal)
		{
			$this->reporte->set("banco",$banco);
			$this->reporte->set("fechainicial",$fechainicial);
			$this->reporte->set("fechafinal",$fechafinal);
			$datos=$this->reporte->sp_reportediarioequiposinstalados();
			return $datos;
		}

		public function sp_reportediarioresultadogestion ($region,$banco,$sitio)
		{
		$this->reporte->set("region",$region);
		$this->reporte->set("banco",$banco);
		$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioresultadogestion();
		return $datos;
		}

		public function sp_reporteparametrizacion ($banco, $estatus)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("estatus",$estatus);

		$datos=$this->reporte->sp_reporteparametrizacion();
		return $datos;
		}




 public function sp_reportecaptacionejec($clientes, $ejecutivo, $fechainicial, $fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("ejecutivo",$ejecutivo);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reportecaptacionejec();
		return $datos;
	}

	
		public function sp_reportediarioresultadogestionrosal ($banco,$sitio)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioresultadogestionrosal();
		return $datos;
		}
		
		public function sp_verbancos($default){
      		$this->reporte->set("default",$default);
		$datos=$this->reporte->sp_verbancos();
		return $datos;
	}

		public function sp_reportediarioestatussustitucion ($banco)
		{
		$this->reporte->set("banco",$banco);
		//$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioestatussustitucion();
		return $datos;
		}

		public function sp_reportedetalladosustitucion ($cliente,$secuencial)
		{
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("secuencial",$secuencial);
		$datos=$this->reporte->sp_reportedetalladosustitucion();
		return $datos;
		}

		public function sp_preguntasencuestaa($clientes, $fechainicial, $fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_preguntasencuestaa();
		return $datos;
	}

		public function sp_preguntasencuestab($clientes, $fechainicial, $fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_preguntasencuestab();
		return $datos;
	}

		public function sp_preguntasencuestac($clientes, $fechainicial,$fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_preguntasencuestac();
		return $datos;
	}

		public function sp_buscarestadoencue($clientes){
        $this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_buscarestadoencue();
		return $datos;
	}

	public function sp_buscarregistrosencuesta($clientes, $fechainicial,$fechafinal){
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_buscarregistrosencuesta();
		return $datos;
		}	

	public function spbuscarrifreporte($rif){
		$this->reporte->set("rif",$rif);
		$datos=$this->reporte->spbuscarrifreporte();
		return $datos;
		}

		public function sp_buscarestadoencueTD($cliente){
        $this->reporte->set("cliente",$cliente);
		$datos=$this->reporte->sp_buscarestadoencueTD();
		return $datos;
	}

	public function sp_buscarregistrosencuestacliente($clientes){
        $this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_buscarregistrosencuestacliente();
		return $datos;
	}	

	public function sp_verhistorialllamadas($idcliente){
        $this->reporte->set("idcliente",$idcliente);
		$datos=$this->reporte->sp_verhistorialllamadas();
		return $datos;
	}

	public function sp_reportedomiciliacion($cliente,$estatus)
		{
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("estatus",$estatus);
		$datos=$this->reporte->sp_reportedomiciliacion();
		return $datos;
		}

		public function sp_mostrarlogin ()
		{
		$datos=$this->reporte->sp_mostrarlogin();
		return $datos;
		}

		public function sp_mostrarestatuscomision ()
		{
		$datos=$this->reporte->sp_mostrarestatuscomision();
		return $datos;
		}

		public function spEditarComisiones ($id, $confcomisiones, $fechacomisiones,$usuario)
		{
			$this->reporte->set("idcliente",$idcliente);
			$this->reporte->set("confcomisiones",$confcomisiones);
			$this->reporte->set("fechacomisiones",$fechacomisiones);
			$this->reporte->set("usuario",$usuario);
			$resultado=$this->reporte->spEditarComisiones();
			return $resultado;
		}

		public function sp_buscarvendedoresmensual($fechainicial,$fechafinal){
		$this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_buscarvendedoresmensual();
		return $datos;
		}
	
		public function spEditarReferidos ($id_consecutivo, $nombre_ref, $apellido_ref,$telefono_ref,$usuario)
		{
		$this->reporte->set("id_consecutivo",$id_consecutivo);
		$this->reporte->set("nombre_ref",$nombre_ref);
		$this->reporte->set("apellido_ref",$apellido_ref);
		$this->reporte->set("telefono_ref",$telefono_ref);
		$this->reporte->set("usuario",$usuario);
		$resultado=$this->reporte->spEditarReferidos();
		return $resultado;
		}	

		public function sp_buscarclientespersonasreferidas($clientes){
        $this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_buscarclientespersonasreferidas();
		return $datos;
	}

		public function sp_buscardetallespersonasreferidas ($idclientes){
		$this->reporte->set("clientes",$idclientes);
		$datos=$this->reporte->sp_buscardetallespersonasreferidas();
		return $datos;
		}

		public function sp_buscarrifpersonasreferidas($rif){
		$this->reporte->set("rif",$rif);
		$datos=$this->reporte->sp_buscarrifpersonasreferidas();
		return $datos;
	}		

	public function spmostrarcomisiones($idcliente){
		$this->reporte->set("idcliente",$idcliente);
	$datos=$this->reporte->spmostrarcomisiones();
	return $datos;
	}

	public function spmostrarcomisionesestatus($idcliente){
		$this->reporte->set("idcliente",$idcliente);
	$datos=$this->reporte->spmostrarcomisionesestatus();
	return $datos;
	}

	public function sp_reportediarioestatusgestiones($banco)
		{
		$this->reporte->set("banco",$banco);
		//$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioestatusgestiones();
		return $datos;
		}

	public function sp_reportediarioestatusgestionesrif($nrif)
	{
		$this->reporte->set("nrif",$nrif);
		//$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioestatusgestionesrif();
		return $datos;
	}

	public function sp_reportedetalladoestatusgeneral ($cliente,$secuencial)
		{
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("secuencial",$secuencial);
		$datos=$this->reporte->sp_reportedetalladoestatusgeneral();
		return $datos;
		}

		public function sp_reporteclientesprofit($clientes,$fechainicial,$fechafinal)
		{
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reporteclientesprofit();
		return $datos;
		}

		public function sp_reporteplantillaprofit($clientes,$fechainicial,$fechafinal)
		{
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reporteplantillaprofit();
		return $datos;
		}

		public function sp_reportediarioequipospercances ($banco)
		{
		$this->reporte->set("banco",$banco);
		$datos=$this->reporte->sp_reportediarioequipospercances();
		return $datos;
		}

		public function sp_reporteestatusfacturacion ($banco,$estatus)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("estatus",$estatus);
		$datos=$this->reporte->sp_reporteestatusfacturacion();
		return $datos;
		}
}

?>

