<?php

include_once("model/VisitaTecnica.php");

	class controladorVisita
	{
		private $visita;
		
		public function __construct(){
			$this->visita= new VisitaTecnica();
		}
		
		public function mostrartecnicos(){
			$datos=$this->visita->mostrartecnicos();
			return $datos;
		} 

		public function sp_mostrarvisitatecnica ($banco, $fechavisita,$nrorif)
		{
			$this->visita->set("banco",$banco);
			$this->visita->set("fechavisita",$fechavisita);
			$this->visita->set("nrorif",$nrorif);
			$datos=$this->visita->sp_mostrarvisitatecnica();
			return $datos;
		}

		public function sp_mostrarvisitatecnicaxrif ($busqueda)
		{
			$this->visita->set("busqueda",$busqueda);
			$datos=$this->visita->sp_mostrarvisitatecnicaxrif();
			return $datos;
		}

		public function sp_mostrarvisitatecnicaxfechas ($fechainicial,$fechafinal)
		{
			$this->visita->set("fechainicial",$fechainicial);
			$this->visita->set("fechafinal",$fechafinal);
			$datos=$this->visita->sp_mostrarvisitatecnicaxfechas();
			return $datos;
		}

		public function sp_mostrargestionxfechas ($fechainicial,$fechafinal)
		{
			$this->visita->set("fechainicial",$fechainicial);
			$this->visita->set("fechafinal",$fechafinal);
			$datos=$this->visita->sp_mostrargestionxfechas();
			return $datos;
		}

		public function sp_mostrarreportevisitatecnica ($banco)
		{
			$this->visita->set("banco",$banco);
			$datos=$this->visita->sp_mostrarreportevisitatecnica();
			return $datos;
		}

		public function spverdetallevisitatecnica ($idcliente,$serialpos)
		{
			$this->visita->set("idcliente",$idcliente);
			$this->visita->set("serialpos",$serialpos);
			$datos=$this->visita->spverdetallevisitatecnica();
			return $datos;
		}

		public function spverdetallevisitatecnica1 ($idcliente,$serialpos)
		{
			$this->visita->set("idcliente",$idcliente);
			$this->visita->set("serialpos",$serialpos);
			$datos=$this->visita->spverdetallevisitatecnica1();
			return $datos;
		}

		public function sp_mostrargestionvisita ($idcliente,$idsecuencial)
		{
			$this->visita->set("idcliente",$idcliente);
			$this->visita->set("idsecuencial",$idsecuencial);
			$datos=$this->visita->sp_mostrargestionvisita();
			return $datos;
		}

		public function sp_mostrartecnicovisitatec($idcliente){
			$this->visita->set("idcliente",$idcliente);
			$datos=$this->visita->sp_mostrartecnicovisitatec();
			return $datos;
		}

		public function spestatusvisita ()
		{
			$datos=$this->visita->spestatusvisita();
			return $datos;
		}

		public function sprelacion_condstatus ($idvisita)
		{
			$this->visita->set("idvisita",$idvisita);
			$datos=$this->visita->sprelacion_condstatus();
			return $datos;
		}

		public function selecttecnicosvisita(){
		$datos=$this->visita->selecttecnicosvisita();
		return $datos;
		}

		public function sp_guardarvisita($idcliente,$serialpos,$fechagesti,$estatusvisita,$tecnico,$fechavisita,$usuario,$observacion,$idcondicion,$idsecuencial){
		$this->visita->set("idcliente",$idcliente);
		$this->visita->set("serialpos",$serialpos);
		$this->visita->set("fechagesti",$fechagesti);
		$this->visita->set("estatusvisita",$estatusvisita);
		$this->visita->set("tecnico",$tecnico);
		$this->visita->set("fechavisita",$fechavisita);
		$this->visita->set("usuario",$usuario);
		$this->visita->set("observacion",$observacion);
		$this->visita->set("idcondicion",$idcondicion);
		$this->visita->set("idsecuencial",$idsecuencial);	
		$datos=$this->visita->sp_guardarvisita();
		return $datos;
		}

		public function sp_guardaraprobacionvisita($sec,$idaprob){
		$this->visita->set("sec",$sec);
		$this->visita->set("idaprob",$idaprob);	
		$datos=$this->visita->sp_guardaraprobacionvisita();
		return $datos;
		}   
         
    	
    }
?>