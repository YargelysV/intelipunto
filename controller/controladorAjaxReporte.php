<?php
//session_start();
include_once("../model/Reporte.php");

class ControladorReporte{

	private $reporte;

	public function __construct(){
	$this->reporte= new Reporte();
	}

	public function sp_reportediarioestatusgeneral ($banco)
		{
			$this->reporte->set("banco",$banco);
			$datos=$this->reporte->sp_reportediarioestatusgeneral();
			return $datos;
		}

		public function sp_reportediarioequiposvendidos ($banco)
		{
			$this->reporte->set("banco",$banco);
			$datos=$this->reporte->sp_reportediarioequiposvendidos();
			return $datos;
		}

		// public function sp_reportediarioequiposinstalados ($banco,$fechainicial,$fechafinal)
		// {
		// 	$this->reporte->set("banco",$banco);
		// 	$this->reporte->set("fechainicial",$fechainicial);
		// 	$this->reporte->set("fechafinal",$fechafinal);
		// 	$datos=$this->reporte->sp_reportediarioequiposinstalados();
		// 	return $datos;
		// }

		public function sp_reportediarioresultadogestion ($region,$banco,$sitio)
		{
		$this->reporte->set("region",$region);
		$this->reporte->set("banco",$banco);
		$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioresultadogestion();
		return $datos;
		}

		public function sp_reportediarioresultadogestionrosal ($banco,$sitio)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("sitio",$sitio);
		$datos=$this->reporte->sp_reportediarioresultadogestionrosal();
		return $datos;
		}

		
 		public function sp_reportecaptacionejec($clientes, $ejecutivo, $fechainicial, $fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("ejecutivo",$ejecutivo);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reportecaptacionejec();
		return $datos;
		}	

		public function sp_reportediarioxbanco($clientes)
		{
		$this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_reportediarioxbanco();
		return $datos;
		}

		public function sp_reportexdias($clientes,$ejecutivo,$fechainicial,$fechafinal,$tiporeporte)
		{
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("ejecutivo",$ejecutivo);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$this->reporte->set("tiporeporte",$tiporeporte);
		$datos=$this->reporte->sp_reportexdias();
		return $datos;
		}

		
		public function sp_reportexdiasdetalles($banco, $usuarioeje, $tipopos, $fechainicial, $fechafinal, $tiporeporte,$usuario){
		$this->reporte->set("banco",$banco);
		$this->reporte->set("usuarioeje",$usuarioeje);
		$this->reporte->set("tipoposdetalles",$tipopos);
		$this->reporte->set("fechainicio",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$this->reporte->set("tiporeporte",$tiporeporte);
		$this->reporte->set("usuario",$usuario);
		$datos=$this->reporte->sp_reportexdiasdetalles();
		return $datos;
		}
		public function sp_reportedomiciliacion($cliente,$estatus)
		{
		$this->reporte->set("cliente",$cliente);
		$this->reporte->set("estatus",$estatus);
		$datos=$this->reporte->sp_reportedomiciliacion();
		return $datos;
		}

		public function sp_mostrarlogin ()
		{
		$datos=$this->reporte->sp_mostrarlogin();
		return $datos;
		}

		public function sp_reportediariogeneraldemifi ($banco)
		{
		$this->reporte->set("banco",$banco);
		$datos=$this->reporte->sp_reportediariogeneraldemifi();
		return $datos;
		}

		public function sp_mostrarestatuscomision ()
		{
		$datos=$this->reporte->sp_mostrarestatuscomision();
		return $datos;
		}

		public function spEditarComisiones ($id, $confcomisiones, $fechacomisiones,$usuario)
		{
			$this->reporte->set("id",$id);
			$this->reporte->set("confcomisiones",$confcomisiones);
			$this->reporte->set("fechacomisiones",$fechacomisiones);
			$this->reporte->set("usuario",$usuario);
			
			$resultado=$this->reporte->spEditarComisiones();
			return $resultado;
		}

		public function sp_preguntasencuestaa($clientes, $fechainicial, $fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_preguntasencuestaa();
		return $datos;
	}

	public function sp_preguntasencuestab($clientes, $fechainicial, $fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_preguntasencuestab();
		return $datos;
	}

	public function sp_preguntasencuestac($clientes, $fechainicial,$fechafinal){
        $this->reporte->set("clientes",$clientes);
        $this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_preguntasencuestac();
		return $datos;
	}

	public function sp_buscarestadoencue($clientes){
        $this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_buscarestadoencue();
		return $datos;
	}

	public function sp_buscarestadoencueTD($cliente){
        $this->reporte->set("cliente",$cliente);
		$datos=$this->reporte->sp_buscarestadoencueTD();
		return $datos;
	}

	public function sp_buscarregistrosencuesta($clientes, $fechainicial,$fechafinal){
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_buscarregistrosencuesta();
		return $datos;
		}	

	public function sp_buscarregistrosencuestacliente($clientes){
        $this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_buscarregistrosencuestacliente();
		return $datos;
	}		

	public function spbuscarrifreporte($rif){
		$this->reporte->set("rif",$rif);
		$datos=$this->reporte->spbuscarrifreporte();
		return $datos;
		}

	public function sp_buscarvendedoresmensual($fechainicial,$fechafinal){
		$this->reporte->set("fechainicial",$fechainicial);
        $this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_buscarvendedoresmensual();
		return $datos;
		}

	public function spEditarReferidos ($id_consecutivo, $nombre_ref, $apellido_ref,$telefono_ref,$usuario)
		{
		$this->reporte->set("id_consecutivo",$id_consecutivo);
		$this->reporte->set("nombre_ref",$nombre_ref);
		$this->reporte->set("apellido_ref",$apellido_ref);
		$this->reporte->set("telefono_ref",$telefono_ref);
		$this->reporte->set("usuario",$usuario);
		$resultado=$this->reporte->spEditarReferidos();
		return $resultado;
		}	

		public function sp_buscarclientespersonasreferidas($clientes){
        $this->reporte->set("clientes",$clientes);
		$datos=$this->reporte->sp_buscarclientespersonasreferidas();
		return $datos;
	}	

		public function sp_buscardetallespersonasreferidas ($idclientes){
		$this->reporte->set("clientes",$idclientes);
		$datos=$this->reporte->sp_buscardetallespersonasreferidas();
		return $datos;
		}

			public function sp_buscarrifpersonasreferidas($rif){
		$this->reporte->set("rif",$rif);
		$datos=$this->reporte->sp_buscarrifpersonasreferidas();
		return $datos;
	}		

			public function sp_reporteejecutivogestiones($fechainicial,$fechafinal)
		{
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reporteejecutivogestiones();
		return $datos;
		}

		public function spmostrarcomisiones($idcliente){
		$this->reporte->set("idcliente",$idcliente);
		$datos=$this->reporte->spmostrarcomisiones();
		return $datos;
		}

		public function sp_guardarcomisiones($idcliente,$usuario,$estatuscomision,$fechacomisiones,$observaciones, $vendedor){
		$this->reporte->set("idcliente",$idcliente);
		$this->reporte->set("usuario",$usuario);
		$this->reporte->set("estatuscomision",$estatuscomision);
		$this->reporte->set("fechacomisiones",$fechacomisiones);
		$this->reporte->set("observaciones",$observaciones);
		$this->reporte->set("vendedor",$vendedor);
		$datos=$this->reporte->sp_guardarcomisiones();
		return $datos;
		}

		public function sp_reportediarioestatussustitucion ($banco){
		$this->reporte->set("banco",$banco);
		$datos=$this->reporte->sp_reportediarioestatussustitucion();
		return $datos;
		}

		public function sp_reporteclientesprofit($clientes,$fechainicial,$fechafinal)
		{
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reporteclientesprofit();
		return $datos;
		}

		public function sp_reporteplantillaprofit($clientes,$fechainicial,$fechafinal)
		{
		$this->reporte->set("clientes",$clientes);
		$this->reporte->set("fechainicial",$fechainicial);
		$this->reporte->set("fechafinal",$fechafinal);
		$datos=$this->reporte->sp_reporteplantillaprofit();
		return $datos;
		}

		public function sp_reportediarioequipospercances ($banco)
		{
			$this->reporte->set("banco",$banco);
			$datos=$this->reporte->sp_reportediarioequipospercances();
			return $datos;
		}

		public function sp_reporteestatusfacturacion ($banco,$estatus)
		{
		$this->reporte->set("banco",$banco);
		$this->reporte->set("estatus",$estatus);
		$datos=$this->reporte->sp_reporteestatusfacturacion();
		return $datos;
		}	
}
?>