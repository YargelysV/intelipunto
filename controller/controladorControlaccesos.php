<?php

include_once("model/ControlAcceso.php");

class ControladorControlAcceso{

	private $controlacceso;

	public function __construct(){
		$this->controlacceso= new controlacceso();
	}		

		public function sp_buscaridempleados(){
			$datos=$this->controlacceso->sp_buscaridempleados();
			return $datos;
		}


		public function sp_buscaraccesos($meses,$anio,$empleado){
			$this->controlacceso->set("meses",$meses);
			$this->controlacceso->set("anio",$anio);
			$this->controlacceso->set("empleado",$empleado);
			$datos=$this->controlacceso->sp_buscaraccesos();
			return $datos;
		}

		public function sp_asignarferiados($fechass){
			$this->controlacceso->set("fechass",$fechass);
			$datos=$this->controlacceso->sp_asignarferiados();
			return $datos;
		}

		public function verarchivoacceso ($name_ar){
		$this->controlacceso->set("name_ar",$name_ar);
		$datos=$this->controlacceso->verarchivoacceso();
		return $datos;
		}

		public function insertar_tblacceso($tiempo_ar,$idusuario_ar,$nombre_ar,$apellido_ar,$numerotarjeta_ar,$dispositivo_ar,$puntoevento_ar,$verificacion_ar,$estado_ar,$evento_ar,$name_ar){
			$this->controlacceso->set("tiempo_ar",$tiempo_ar);
			$this->controlacceso->set("idusuario_ar",$idusuario_ar);
			$this->controlacceso->set("nombre_ar",$nombre_ar);
			$this->controlacceso->set("apellido_ar",$apellido_ar);
			$this->controlacceso->set("numerotarjeta_ar",$numerotarjeta_ar);
			$this->controlacceso->set("dispositivo_ar",$dispositivo_ar);
			$this->controlacceso->set("puntoevento_ar",$puntoevento_ar);
			$this->controlacceso->set("verificacion_ar",$verificacion_ar);
			$this->controlacceso->set("estado_ar",$estado_ar);
			$this->controlacceso->set("evento_ar",$evento_ar);
			$this->controlacceso->set("name_ar",$name_ar);
			$datos=$this->controlacceso->sp_insertar_tblacceso();
			return $datos;
		}

		public function verfechacargada(){
			$datos=$this->controlacceso->spverfechaaccesocargado();
			return $datos;
		}

}

?>

