<?php
//session_start();
include_once("../model/Adjuntar.php");

class controladorAdjuntar{

	private $adjuntar;

	public function __construct(){
	$this->adjuntar= new Adjuntar();
	}

	public function sp_buscarserialespos($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_buscarserialespos();
		return $datos;
	}

	public function sp_buscarserialessimcards($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_buscarserialessimcards();
		return $datos;
	}

	public function sp_buscarserialesmifi($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_buscarserialesmifi();
		return $datos;
	}

	public function sp_reportedetalladopos($fecha){
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_reportedetalladopos();
		return $datos;
	}

	public function sp_reportegeneralpos($fecha,$marcapos,$operacion){
		$this->adjuntar->set("fecha",$fecha);
		$this->adjuntar->set("marcapos",$marcapos);
		$this->adjuntar->set("operacion",$operacion);
		$datos=$this->adjuntar->sp_reportegeneralpos();
		return $datos;
	}

	public function spverarchivodiariocliente($cliente,$fecha){
		$this->adjuntar->set("cliente",$cliente);
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->spverarchivodiariocliente();
		return $datos;
	}	


	public function verclientefacturacion($cliente, $fechadetalle, $namearchivo){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("fecha",$fechadetalle);
	$this->adjuntar->set("namearchivo",$namearchivo);
	$datos=$this->adjuntar->verclientefacturacion();
	return $datos;
	}

	public function mostrarClienteFacturacion($rif, $cliente, $fechadetalle, $namearchivo,$id_registro){
	$this->adjuntar->set("coddocumento",$rif);
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("fecha",$fechadetalle);
	$this->adjuntar->set("namearchivo",$namearchivo);
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->mostrarClienteFacturacion();
	return $datos;
	}

	public function mostrarClienteFacturacionmifi($rif, $cliente, $fechadetalle, $namearchivo,$id_registro){
	$this->adjuntar->set("coddocumento",$rif);
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("fecha",$fechadetalle);
	$this->adjuntar->set("namearchivo",$namearchivo);
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->mostrarClienteFacturacionmifi();
	return $datos;
	}

	public function spinsertarexpfactura($idregistro,$directorio){
		//gestioncomercial
		$this->adjuntar->set("idregistro",$idregistro);
		$this->adjuntar->set("directorio",$directorio);
		//$this->adjuntar->set("usuario",$usuario);
		$datos=$this->adjuntar->spinsertarexpfactura();
		return $datos;
		}


public function selectformapago(){
	$datos=$this->adjuntar->selectformapago();
	return $datos;
	}

	public function selectmarcapos(){
	$datos=$this->adjuntar->selectmarcapos();
	return $datos;
	}

	public function selectestados(){
	$datos=$this->adjuntar->selectestados();
	return $datos;
	}

	public function selectmunicipio(){
	$datos=$this->adjuntar->selectmunicipio();
	return $datos;
	}

	public function selectparroquias(){
	$datos=$this->adjuntar->selectparroquias();
	return $datos;
	}

	public function sptipozonas($perfil){
	$this->adjuntar->set("perfil",$perfil);
	$datos=$this->adjuntar->sptipozonas();
	return $datos;
	}
	
	public function selecttipopos(){
	$datos=$this->adjuntar->selecttipopos();
	return $datos;
	}

		public function selecttipotarjeta(){
	$datos=$this->adjuntar->selecttipotarjeta();
	return $datos;
	}

		public function selecttipobanco(){
	$datos=$this->adjuntar->selecttipobanco();
	return $datos;
	}

	public function selectbanco(){
	$datos=$this->adjuntar->selectbanco();
	return $datos;
	}

	public function selectserialpos($nafiliado){
	$this->adjuntar->set("nafiliado",$nafiliado);
	$datos=$this->adjuntar->selectserialpos();
	return $datos;
		} 

	public function selectestatuspos($codestatus){
	$this->adjuntar->set("codestatus",$codestatus);
	$datos=$this->adjuntar->selectestatuspos();
	return $datos;
	}

	public function selectestatusinstmifi($codestatus){
	$this->adjuntar->set("codestatus",$codestatus);
	$datos=$this->adjuntar->selectestatusinstmifi();
	return $datos;
	}


	public function selecttecnicos(){
	$datos=$this->adjuntar->selecttecnicos();
	return $datos;
	}

	public function EditarClienteFacturacion($rif, $cliente, $namearchivo, $costopos, $fechafacturacion, $nfactura, $formapago, $codtarjeta, $ntransferencia, $bancoorigen, $fechapago, $horapago, $usuario,$idafiliado){
		$this->adjuntar->set("coddocumento",$rif);
		$this->adjuntar->set("cliente",$cliente);
		$this->adjuntar->set("namearchivo",$namearchivo);
		$this->adjuntar->set("costopos",$costopos);
		$this->adjuntar->set("fechafacturacion",$fechafacturacion);
		$this->adjuntar->set("nfactura",$nfactura);
		$this->adjuntar->set("nameformapago",$formapago);
		$this->adjuntar->set("codtarjeta",$codtarjeta);
		$this->adjuntar->set("ntransferencia",$ntransferencia);
		$this->adjuntar->set("bancoorigen",$bancoorigen);
		$this->adjuntar->set("fecha",$fechapago);
		$this->adjuntar->set("horapago",$horapago);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("id_registro",$idafiliado);
		$datos=$this->adjuntar->EditarClienteFacturacion();
		return $datos;
	}

	public function sp_buscarclientefacturacionRif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->adjuntar->set("rif",$rif);
		$this->adjuntar->set("tipousuario",$tipousuario);
		$this->adjuntar->set("ejecutivo",$ejecutivo);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("coordinador",$coordinador);
		$this->adjuntar->set("coordinadordos",$coordinadordos);
		$datos=$this->adjuntar->sp_buscarclientefacturacionRif();
		return $datos;
	}

		public function spbuscarclientefacturacionmifixrif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->adjuntar->set("rif",$rif);
		$this->adjuntar->set("tipousuario",$tipousuario);
		$this->adjuntar->set("ejecutivo",$ejecutivo);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("coordinador",$coordinador);
		$this->adjuntar->set("coordinadordos",$coordinadordos);
		$datos=$this->adjuntar->spbuscarclientefacturacionmifixrif();
		return $datos;
	}

	public function sp_verclientefacturacion($cliente, $fechadetalle, $namearchivo,$masiva, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("fecha",$fechadetalle);
	$this->adjuntar->set("namearchivo",$namearchivo);
	$this->adjuntar->set("masiva",$masiva);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_verclientefacturacion();
	return $datos;
	}

		public function sp_verclientefacturacionmifi($cliente, $fechadetalle, $namearchivo,$masiva, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("fecha",$fechadetalle);
	$this->adjuntar->set("namearchivo",$namearchivo);
	$this->adjuntar->set("masiva",$masiva);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_verclientefacturacionmifi();
	return $datos;
	}

	public function sp_buscarclientefacturacion($cliente,$fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("FechaRecepcionArchivo",$fecha);
	$this->adjuntar->set("origen",$origen);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_buscarclientefacturacion();
	return $datos;
	}

	public function sp_buscarclientefacturacionmifi($cliente,$fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("FechaRecepcionArchivo",$fecha);
	$this->adjuntar->set("origen",$origen);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_buscarclientefacturacionmifi();
	return $datos;
	}

	public function sp_buscarinventariopos(){
		$datos=$this->adjuntar->sp_buscarinventariopos();
		return $datos;
	}
}


?>
