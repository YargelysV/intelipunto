<?php
	
//	error_reporting(0);
	include_once("model/Modulos.php");

		class controladorModulo{
		
			private $modulo;
			
			public function __construct(){
				$this->modulo= new Modulo();
			}
			
		
		public function spverificarmodulosactivos($login){
		$this->modulo->set("login",$login);
		$resultado=$this->modulo->spverificarmodulosactivos();
		return $resultado;
		}
		public function sp_actividad($id_registro,$activo,$usuario){
		$this->modulo->set("id_registro",$id_registro);
		$this->modulo->set("activo",$activo);
		$this->modulo->set("usuario",$usuario);
		$datos=$this->modulo->sp_actividad();
		return $datos;
	    }
	    public function sp_vernotificaciones($area){
	    $this->modulo->set("area",$area);
	    $resultado=$this->modulo->sp_vernotificaciones();
	    return $resultado;
	    }

	    public function sp_countnotificaciones($area){
	    $this->modulo->set("area",$area);
	    $resultado=$this->modulo->sp_countnotificaciones();
	    return $resultado;
	    }		
		
		public function sp_actividadencuesta($idcliente,$activo,$usuario){
		$this->modulo->set("idcliente",$idcliente);
		$this->modulo->set("activo",$activo);
		$this->modulo->set("usuario",$usuario);
		$datos=$this->modulo->sp_actividadencuesta();
		return $datos;
	    }	

	    public function sp_pagosnoconf(){
		$datos=$this->modulo->sp_pagosnoconf();
		return $datos;
		}

		public function sp_recompranoconf(){
		$datos=$this->modulo->sp_recompranoconf();
		return $datos;
		}
		
		public function sp_tasacambio(){
		$datos=$this->modulo->sp_tasacambio();
		return $datos;
		}
	}
		
?>
	