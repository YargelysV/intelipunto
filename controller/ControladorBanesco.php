<?php

include_once("model/ModuloBanesco.php");

	class ControladorBanesco
	{
		private $banesco;
		
		public function __construct(){
			$this->banesco= new Banesco();
		}
		
		public function buscarlote(){
		$datos=$this->banesco->buscarlote();
		return $datos;
		}

		public function buscarlotes($idlote){
		$this->banesco->set("idlote",$idlote);
		$datos=$this->banesco->buscarlotes();
		return $datos;
		}	

		public function spverequiposinterminal(){
		$datos=$this->banesco->spverequiposinterminal();
		return $datos;

	}	

	public function actualizarserial($serial,$usuario,$idestatus){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuario",$usuario);
		$this->banesco->set("idestatus",$idestatus);
		$datos=$this->banesco->actualizarserial();
		return $datos;
	}	

	public function sp_guardarstatusllaves($idlote,$usuariocarga,$statusllaves){
		$this->banesco->set("idlote",$idlote);
		$this->banesco->set("usuariocarga",$usuariocarga);
		$this->banesco->set("statusllaves",$statusllaves);
		$datos=$this->banesco->sp_guardarstatusllaves();
		return $datos;
	}

	public function reportebanesco($estatus){
		$this->banesco->set("estatus",$estatus);
		$datos=$this->banesco->reportebanesco();
		return $datos;
	}

	public function busquedaequiposbbu($status){
		$this->banesco->set("status",$status);
		$datos=$this->banesco->busquedaequiposbbu();
		return $datos;
	}

	public function spverclientesbanesco ()
	{
		$datos=$this->banesco->spverclientesbanesco();
		return $datos;
	}		

	public function spverafiliadosbanesc (){
	$datos=$this->banesco->spverafiliadosbanesc();
	return $datos;
	}

	public function spvernumcuentabanesc (){
	$datos=$this->banesco->spvernumcuentabanesc();
	return $datos;
	}

	public function verafiliadosbanescodetalle ($afiliado)
	{
	$this->banesco->set("NombreAfiliado",$afiliado);
	$datos=$this->banesco->verafiliadosbanescodetalle();
	return $datos;
	}

	public function vercuentabanescodetalle ($numcuenta){
	$this->banesco->set("numcuenta",$numcuenta);
	$datos=$this->banesco->vercuentabanescodetalle();
	return $datos;
	}

	public function insertar_datos($Consecutivo, $id, $rif,$razonsocial, $banco, $afiliado, $motivo, $ncuenta, $usuario){
		$this->banesco->set("Consecutivo",$Consecutivo);
		$this->banesco->set("id",$id);
		$this->banesco->set("rif",$rif);
		$this->banesco->set("razonsocial",$razonsocial);
		$this->banesco->set("banco",$banco);
		$this->banesco->set("afiliado",$afiliado);
		$this->banesco->set("motivo",$motivo);
		$this->banesco->set("ncuenta",$ncuenta);
		$this->banesco->set("usuario",$usuario);
		$datos=$this->banesco->insertar_datos();
		return $datos;
	}

	public function verestatusafiliado ()
	{
		$datos=$this->banesco->verestatusafiliado();
		return $datos;
	}	

	public function vermotivorechazo ()
	{
		$datos=$this->banesco->vermotivorechazo();
		return $datos;
	}

	public function spverclientesbanescofact ()
	{
		$datos=$this->banesco->spverclientesbanescofact();
		return $datos;
	}

	public function insertar_terminal($Consecutivo, $id, $rif,$razonsocial, $banco, $afiliado,$serial,$usuario){
		jwtMiddleware();
		$this->banesco->set("Consecutivo",$Consecutivo);
		$this->banesco->set("id",$id); 
		$this->banesco->set("rif",$rif);
		$this->banesco->set("razonsocial",$razonsocial);
		$this->banesco->set("banco",$banco);
		$this->banesco->set("afiliado",$afiliado);
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuario",$usuario);
		$datos=$this->banesco->insertar_terminal();
		return $datos;
	}

		public function verprefijobanco(){
		$datos=$this->banesco->verprefijobanco();
		return $datos;
	}	

	public function buscarestatus(){
		$datos=$this->banesco->buscarestatus();
		return $datos;
		}

		public function buscarregistros ($idstatus)
		{
		$this->banesco->set("idstatus",$idstatus);
		$datos=$this->banesco->buscarregistros();
		return $datos;
		}

		public function buscarlotesreporte ($idlote)
		{
		$this->banesco->set("idlote",$idlote);
		$datos=$this->banesco->buscarlotesreporte();
		return $datos;
		}	

		public function buscarreportegeneral ($cliente)
		{
		$this->banesco->set("cliente",$cliente);
		$datos=$this->banesco->buscarreportegeneral();
		return $datos;
		}

		public function buscarbanco($cliente){
		$this->banesco->set("cliente",$cliente);
		$datos=$this->banesco->buscarbanco();
		return $datos;
		}
		
		public function banescoafiliadosvar($variable){
		$this->banesco->set("variable",$variable);
		$datos=$this->banesco->banescoafiliadosvar();
		return $datos;
		}

		public function banescoafiliados(){
		$datos=$this->banesco->banescoafiliados();
		return $datos;
		}

		public function spverbancos($valor){
		$this->banesco->set("valor",$valor);
		$datos=$this->banesco->spverbancos();
		return $datos;
		}

		public function sp_buscarequiposreingreso($serial){
		$this->banesco->set("serial",$serial);
		$datos=$this->banesco->sp_buscarequiposreingreso();
		return $datos;
		}

		public function actualizarestatusreingreso($serial,$usuario,$idestatus){
		$this->banesco->set("serial",$serial);
		$this->banesco->set("usuario",$usuario);
		$this->banesco->set("idestatus",$idestatus);
		$datos=$this->banesco->actualizarestatusreingreso();
		return $datos;
		}	

		public function sp_buscarequipostaller($serial){
		$this->banesco->set("serial",$serial);
		$datos=$this->banesco->sp_buscarequipostaller();
		return $datos;
		}
    }
?>