<?php

include_once("../model/ServicioTecnico.php");

	class controladorServicioTecnico
	{
		private $serviciotec;
		
		public function __construct()
		{
			$this->serviciotec=new Serviciotec();
		}
		

		public function buscarserviciotecnico($busqueda){
			$this->serviciotec->set("busqueda",$busqueda);
			$datos=$this->serviciotec->buscarservicio();
			return $datos;
			}

			public function verserial()
			
			{
				$datos=$this->serviciotec->serial();
				return $datos;
			}

			public function verclienteserviciotecnico($cliente, $fecha, $namearchivo)
			{
				$this->serviciotec->set("cliente",$cliente);
				$this->serviciotec->set("fecha",$fecha);
				$this->serviciotec->set("namearchivo",$namearchivo);
				$datos=$this->serviciotec->verclienteserviciotecnico();
				return $datos;
			}

			public function sp_buscarclienteSTRif($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarclienteSTRif();
		return $datos;
	}

	public function sp_buscarafiliadoRif($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarafiliadoRif();
		return $datos;
	}	

	public function sp_buscarafiliadoRifgestion($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarafiliadoRifgestion();
		return $datos;
	}	

	public function sp_buscarclientemifixrif($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarclientemifixrif();
		return $datos;
	}	

		public function sp_verclienteafiliado($cliente, $fechadetalle, $namearchivo)
			{
				$this->serviciotec->set("cliente",$cliente);
				$this->serviciotec->set("fecha",$fechadetalle);
				$this->serviciotec->set("namearchivo",$namearchivo);
				$datos=$this->serviciotec->sp_verclienteafiliado();
				return $datos;
			}


			public function sp_verclientedetalladomifi($cliente, $fechadetalle, $namearchivo)
			{
				$this->serviciotec->set("cliente",$cliente);
				$this->serviciotec->set("fecha",$fechadetalle);
				$this->serviciotec->set("namearchivo",$namearchivo);
				$datos=$this->serviciotec->sp_verclientedetalladomifi();
				return $datos;
			}

			public function sp_verclientegestion($cliente, $fechadetalle, $namearchivo, $region)
			{
				$this->serviciotec->set("cliente",$cliente);
				$this->serviciotec->set("fecha",$fechadetalle);
				$this->serviciotec->set("namearchivo",$namearchivo);
				$this->serviciotec->set("region",$region);
				$datos=$this->serviciotec->sp_verclientegestion();
				return $datos;
			}


	public function sp_verclienteseriales($cliente, $fechadetalle, $namearchivo)
		{
			$this->serviciotec->set("cliente",$cliente);
			$this->serviciotec->set("fecha",$fechadetalle);
			$this->serviciotec->set("namearchivo",$namearchivo);
			$datos=$this->serviciotec->sp_verclienteseriales();
			return $datos;
		}
	public function spbuscarclienteRifDetalleST($rif, $cliente, $fecha, $namearchivo)
	{
		$this->serviciotec->set("rif",$rif);
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fecha);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->spbuscarclienteRifDetalleST();
		return $datos;
	}


	public function sp_buscarClienteInstalar($cliente,$fecha, $origen){
	$this->serviciotec->set("cliente",$cliente);
	$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
	$this->serviciotec->set("origen",$origen);
	$datos=$this->serviciotec->sp_buscarClienteInstalar();
	return $datos;
	}

	public function sp_buscarservitec($cliente,$fecha, $origen){
	$this->serviciotec->set("cliente",$cliente);
	$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
	$this->serviciotec->set("origen",$origen);
	$datos=$this->serviciotec->sp_buscarservitec();
	return $datos;
	}

	public function sp_buscarxfechamifi($cliente,$fecha, $origen){
	$this->serviciotec->set("cliente",$cliente);
	$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
	$this->serviciotec->set("origen",$origen);
	$datos=$this->serviciotec->sp_buscarxfechamifi();
	return $datos;
	}

	public function sp_buscargestionservicio($cliente,$fecha, $origen){
	$this->serviciotec->set("cliente",$cliente);
	$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
	$this->serviciotec->set("origen",$origen);
	$datos=$this->serviciotec->sp_buscargestionservicio();
	return $datos;
	}

		public function sp_buscarclienteserviciotecnico($cliente,$fecha){
	$this->serviciotec->set("cliente",$cliente);
	$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
	$datos=$this->serviciotec->sp_buscarclienteserviciotecnico();
	return $datos;
	}


	public function mostrarinventarioposafiliado($idcliente, $busqueda){
	$this->serviciotec->set("idcliente",$idcliente);
	$this->serviciotec->set("busqueda",$busqueda);
	$datos=$this->serviciotec->mostrarinventarioposafiliado();
	return $datos;
	}

	public function mostrarserialafiliado($idcliente,$idsecuencial){
	//$this->serviciotec->set("serialpos",$serialpos);
	//$this->serviciotec->set("nroafiliacion",$nroafiliacion);
	$this->serviciotec->set("idcliente",$idcliente);
	$this->serviciotec->set("idsecuencial",$idsecuencial);
	$datos=$this->serviciotec->mostrarserialafiliado();
	return $datos;
	}

		public function mostrarserialafiliadomifi($idcliente){
	//$this->serviciotec->set("serialpos",$serialpos);
	//$this->serviciotec->set("nroafiliacion",$nroafiliacion);
	$this->serviciotec->set("idcliente",$idcliente);
	//$this->serviciotec->set("idsecuencial",$idsecuencial);
	$datos=$this->serviciotec->mostrarserialafiliadomifi();
	return $datos;
	}



public function spasignarserialpos ($correlativo,$fechaent, $numterminal, $serialposasignado,$numsim,$nroafiliacion,$namearchivo,$usuario,$numamexx,$idregistro,$banco)
	{
		$this->serviciotec->set("correlativo",$correlativo);
		$this->serviciotec->set("fechaent",$fechaent);
		$this->serviciotec->set("numterminal", $numterminal);
		$this->serviciotec->set("serialpos",$serialposasignado);
		$this->serviciotec->set("numsim", $numsim);
		$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$this->serviciotec->set("usuariocarg",$usuario);
		$this->serviciotec->set("numeroamex",$numamexx);
		$this->serviciotec->set("idregistro",$idregistro);
		$this->serviciotec->set("banco",$banco);
		$resultado=$this->serviciotec->spasignarserialpos();
		return $resultado;
	}


	public function spactualizargestionpos($operacion,$serial,$nroafiliacion,$namearchivo, $usuario, $fecharecepalma, $tecnico, $fechagest, $estatus, $fechainst, $fechacarga, $observacion, $idcliente,$fechalog,$fechaenv){
	$this->serviciotec->set("operacion",$operacion);
	$this->serviciotec->set("serialpos",$serial);
	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
	$this->serviciotec->set("namearchivo",$namearchivo);
	$this->serviciotec->set("usuario",$usuario);
	$this->serviciotec->set("fecharecepalma",$fecharecepalma);
	$this->serviciotec->set("tecnico",$tecnico);
	$this->serviciotec->set("fechagest",$fechagest);
	$this->serviciotec->set("estatus",$estatus);
	$this->serviciotec->set("fechainst",$fechainst);
	$this->serviciotec->set("fechacarg",$fechacarga);
	$this->serviciotec->set("observacion",$observacion);
	$this->serviciotec->set("idcliente",$idcliente);
	$this->serviciotec->set("fechalog",$fechalog);
	$this->serviciotec->set("fechaenv",$fechaenv);
	$datos=$this->serviciotec->spactualizargestionpos();
	return $datos;
	}

		public function spagregargestionmifi($operacion,$serialmifi,$namearchivo, $usuario, $fecharecepalma, $tecnico, $fechagest, $estatus, $fechainst, $fechacarga, $observacion, $idcliente){
	$this->serviciotec->set("operacion",$operacion);
	$this->serviciotec->set("serialmifi",$serialmifi);
	$this->serviciotec->set("namearchivo",$namearchivo);
	$this->serviciotec->set("usuario",$usuario);
	$this->serviciotec->set("fecharecepalma",$fecharecepalma);
	$this->serviciotec->set("tecnico",$tecnico);
	$this->serviciotec->set("fechagest",$fechagest);
	$this->serviciotec->set("estatus",$estatus);
	$this->serviciotec->set("fechainst",$fechainst);
	$this->serviciotec->set("fechacarg",$fechacarga);
	$this->serviciotec->set("observacion",$observacion);
	$this->serviciotec->set("idcliente",$idcliente);

	$datos=$this->serviciotec->spagregargestionmifi();
	return $datos;
	}

	public function mostrarserialafiliadoinstaladomifi($idcliente,$idsecuencial){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$datos=$this->serviciotec->mostrarserialafiliadoinstaladomifi();
		return $datos;
		}


			public function mostrarserialafiliadogestionmifi($idcliente,$idsecuencial){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$datos=$this->serviciotec->mostrarserialafiliadogestionmifi();
		return $datos;
		}

		public function mostrarserialafiliadoinstaladomifi2($idcliente,$idsecuencial,$serialmifi){
		//$this->serviciotec->set("serialpos",$serialpos);
	//	$this->serviciotec->set("nroafiliacion",$nroafiliacion);
		$this->serviciotec->set("idcliente",$idcliente);
		$this->serviciotec->set("idsecuencial",$idsecuencial);
		$this->serviciotec->set("serialmifi",$serialmifi);
		$datos=$this->serviciotec->mostrarserialafiliadoinstaladomifi2();
		return $datos;
		}

		public function sp_verclientegestiondelrosal($cliente, $fechadetalle, $namearchivo)
		{
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("fecha",$fechadetalle);
		$this->serviciotec->set("namearchivo",$namearchivo);
		$datos=$this->serviciotec->sp_verclientegestiondelrosal();
		return $datos;
		}

		public function sp_buscargestionservicioderosal($cliente,$fecha,$origen){
		$this->serviciotec->set("cliente",$cliente);
		$this->serviciotec->set("FechaRecepcionArchivo",$fecha);
		$this->serviciotec->set("origen",$origen);
		$datos=$this->serviciotec->sp_buscargestionservicioderosal();
		return $datos;
		}

		public function sp_buscarafiliadoRifgestionrosal($rif){
		$this->serviciotec->set("rif",$rif);
		$datos=$this->serviciotec->sp_buscarafiliadoRifgestionrosal();
		return $datos;
		}

		public function sp_consultariesgomedido($banco,$cliente){
		$this->serviciotec->set("banco",$banco);
		$this->serviciotec->set("cliente",$cliente);
		$datos=$this->serviciotec->sp_consultariesgomedido();
		return $datos;
	    }
	}
?>