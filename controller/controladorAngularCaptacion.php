<?php

	include_once("../model/Captacion.php");

	class controladorAngularCaptacion 
	{ 
		private $captacion;
		
	public function __construct()
		{
		$this->captacion=new Captacion();
		}
		


	public function sp_editarAsignacionEjecutivo ($operacion,$iejecutivo,$idregistro,$nombre,$apellido,$coddocumento,$tlf1,$razonsocial,$bancointeres,$ocodigobanco,$cantposasignados,
		$correorl,$medio,$direccion_instalacion,$tipocliente)
    {
		    $this->captacion->set("operacion",$operacion);
		    $this->captacion->set("iejecutivo",$iejecutivo);
			$this->captacion->set("idregistro",$idregistro);
			$this->captacion->set("nombre",$nombre);
			$this->captacion->set("apellido",$apellido);
			$this->captacion->set("coddocumento",$coddocumento);
			$this->captacion->set("tlf1",$tlf1);
			$this->captacion->set("razonsocial",$razonsocial);
			$this->captacion->set("bancointeres",$bancointeres);
			$this->captacion->set("ocodigobanco",$ocodigobanco);
			$this->captacion->set("cantposasignados",$cantposasignados);
			$this->captacion->set("correorl",$correorl);
			$this->captacion->set("medio",$medio);
			$this->captacion->set("direccion_instalacion",$direccion_instalacion);
			$this->captacion->set("tipocliente",$tipocliente);
			$resultado=$this->captacion->sp_editarAsignacionEjecutivo();
			return $resultado;
	}

   	public function sp_medio_contacto()
   	{
   		$datos=$this->captacion->sp_medio_contacto();
   		return $datos;
   
    }

}

?>



