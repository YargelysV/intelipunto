	<?php
include_once("model/ServicioMantenimiento.php");

class controladorServicioMantenimiento{

	private $serviciomantenimiento;


	public function __construct(){
		$this->serviciomantenimiento= new ServicioMantenimiento();
	}


	public function sp_buscarservicioRif($rif){
		$this->serviciomantenimiento->set("rif",$rif);
		$datos=$this->serviciomantenimiento->sp_buscarservicioRif();
		return $datos;
	}

	public function sp_buscarmantenimiento($fecha){
		$this->serviciomantenimiento->set("fecha",$fecha);
		$datos=$this->serviciomantenimiento->sp_buscarmantenimiento();
		return $datos;
	}

	public function mostrarservicio($cliente,$documento){
		$this->serviciomantenimiento->set("cliente",$cliente);
		$this->serviciomantenimiento->set("documento",$documento);
		$datos=$this->serviciomantenimiento->mostrarservicio();
		return $datos;
	}

		public function mostrarmantenimiento1($cliente){
		$this->serviciomantenimiento->set("cliente",$cliente);
		$datos=$this->serviciomantenimiento->mostrarmantenimiento1();
		return $datos;
	}

		public function vertipodepos(){
		$datos=$this->serviciomantenimiento->vertipodepos();
		return $datos;
	}	

		public function vermarcadepos(){
		$datos=$this->serviciomantenimiento->vermarcadepos();
		return $datos;
	}	

	    public function verprefijobanco(){
		$datos=$this->serviciomantenimiento->verprefijobanco();
		return $datos;
	}

	   public function insertar_carga ($iafiliacion,$irazonsocial,$idocumento,$iterminal, $iserial,$ibanco,$itipo, $imarca,$usuario){
		$this->serviciomantenimiento->set("iafiliacion",$iafiliacion);
		$this->serviciomantenimiento->set("irazonsocial",$irazonsocial);
		$this->serviciomantenimiento->set("idocumento",$idocumento);
		$this->serviciomantenimiento->set("iterminal",$iterminal);
		$this->serviciomantenimiento->set("iserial",$iserial);
		$this->serviciomantenimiento->set("ibanco",$ibanco);
		$this->serviciomantenimiento->set("itipo",$itipo);
		$this->serviciomantenimiento->set("imarca",$imarca);
		$this->serviciomantenimiento->set("usuario",$usuario);
		$resultado=$this->serviciomantenimiento->insertar_carga();
		return $resultado;
	}

	public function verclientesindividual(){
		$datos=$this->serviciomantenimiento->verclientesindividual();
		return $datos;
	}

		public function spbuscardomiciliacionxrif($rif){
		$this->serviciomantenimiento->set("rif",$rif);
		$datos=$this->serviciomantenimiento->spbuscardomiciliacionxrif();
		return $datos;

		}

		public function spbuscarregistrodomiciliacion($cliente){
		$this->serviciomantenimiento->set("cliente",$cliente);
		$datos=$this->serviciomantenimiento->spbuscarregistrodomiciliacion();
		return $datos;

		}

		public function spbuscarreportedomiciliacion($status, $modalidad){
		$this->serviciomantenimiento->set("status",$status);
		$this->serviciomantenimiento->set("modalidad",$modalidad);
		$datos=$this->serviciomantenimiento->spbuscarreportedomiciliacion();
		return $datos;

		}

		public function spverestatuscliente(){
		$datos=$this->serviciomantenimiento->spverestatuscliente();
		return $datos;
	}

		public function spagregarbancoorigen(){
		$datos=$this->serviciomantenimiento->spagregarbancoorigen();
		return $datos;
	}	

		public function sp_verformapago(){
		$datos=$this->serviciomantenimiento->sp_verformapago();
		return $datos;
	}

	public function sp_vermodalidadpago(){
		$datos=$this->serviciomantenimiento->sp_vermodalidadpago();
		return $datos;
	}

		public function sp_guardardomiciliacion($cliente,$documento,$razonsocial,$codformap, $ncuenta, $codigoestatus,$actividad,$cbanco,$usuario){
		$this->serviciomantenimiento->set("cliente",$cliente);
		$this->serviciomantenimiento->set("documento",$documento);
		$this->serviciomantenimiento->set("razonsocial",$razonsocial);
		$this->serviciomantenimiento->set("codformap",$codformap);
		$this->serviciomantenimiento->set("ncuenta",$ncuenta);
		$this->serviciomantenimiento->set("codigoestatus",$codigoestatus);
		$this->serviciomantenimiento->set("actividad",$actividad);
		$this->serviciomantenimiento->set("cbanco",$cbanco);
		$this->serviciomantenimiento->set("usuario",$usuario);
		$datos=$this->serviciomantenimiento->sp_guardardomiciliacion();
		return $datos;
	}

		public function ver_carta_domiciliacion($cliente){
		$this->serviciomantenimiento->set("cliente",$cliente);
		//$this->adjuntar->set("directorio",$directorio);
		//$this->adjuntar->set("usuario",$usuario);
		$datos=$this->serviciomantenimiento->ver_carta_domiciliacion();
		return $datos;
		}

}
?>
	
	