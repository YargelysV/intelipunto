<?php
include_once("model/Usuario.php");

		class controladorUsuario{
		
			private $usuario;
			
			public function __construct(){
				$this->usuario= new Usuario();
			}
			public function index(){
				$resultado=$this->usuario->listar();
				return $resultado;
			}
		
			public function crear ($tipodoc, $coddocumento,$login, $clave, $nombres, $apellidos, $fechanac, $correo,$codtipousuario, $activo, $cargo, $area,$region,$usuario){
				$this->usuario->set("tipodoc",$tipodoc);
				$this->usuario->set("coddocumento",$coddocumento);
				$this->usuario->set("login",$login);
				$this->usuario->set("clave",$clave);
				$this->usuario->set("nombres",$nombres);
				$this->usuario->set("apellidos",$apellidos);
				$this->usuario->set("fechanac",$fechanac);
				$this->usuario->set("correo",$correo);
				$this->usuario->set("codtipousuario",$codtipousuario);
				$this->usuario->set("activo",$activo);
				$this->usuario->set("cargo",$cargo);
				$this->usuario->set("area",$area);
				$this->usuario->set("region",$region);
				$this->usuario->set("usuario",$usuario);
				$resultado=$this->usuario->crear();
				return $resultado;
		}
		
		public function eliminar($login){
				$this->usuario->set("login",$login);	
				$this->usuario->eliminar();
		
		}
		
		public function ver($login){
		$this->usuario->set("login",$login);	
		$datos=$this->usuario->ver();
		return $datos;

		}
	
		public function editar($tipodoc, $coddocumento,$login, $clave, $nombres, $apellidos, $fechanac, $correo,$codtipousuario, $activo, $cargo, $area,$region,$usuario){
				$this->usuario->set("tipodoc",$tipodoc);
				$this->usuario->set("coddocumento",$coddocumento);
				$this->usuario->set("login",$login);
				$this->usuario->set("clave",$clave);
				$this->usuario->set("nombres",$nombres);
				$this->usuario->set("apellidos",$apellidos);
				$this->usuario->set("fechanac",$fechanac);
				$this->usuario->set("correo",$correo);
				$this->usuario->set("codtipousuario",$codtipousuario);
				$this->usuario->set("activo",$activo);
				$this->usuario->set("cargo",$cargo);
				$this->usuario->set("area",$area);
				$this->usuario->set("region",$region);
				$this->usuario->set("usuario",$usuario);
				$resultado=$this->usuario->editar();
				return $resultado;

		}
		
		public function verificar($login,$clave){
		$this->usuario->set("login",$login);
		$this->usuario->set("clave",$clave);	
		$_SESSION["session_user"]=$login;
		$_SESSION["ultimoAcceso"]= date("Y-n-j H:i:s"); 
		$_SESSION["IPuser"] = $_SERVER["REMOTE_ADDR"];
		$_SESSION["navUser"] = $_SERVER["HTTP_USER_AGENT"];
		$_SESSION["hostUser"] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$datos=$this->usuario->verificar();
		return $datos;
		}
		
		public function verificaronline($login){
		$this->usuario->set("login",$login);
		$this->usuario->set("enlinea",1);
		$online=$this->usuario->usuarioenlinea();
		return $online;
		}
		
		public function usuarioenlinea($login){
		$this->usuario->set("login",$login);
		$this->usuario->set("enlinea",0);
		$online=$this->usuario->usuarioenlinea();
		return $online;
		}
		
		public function tipodocumento(){
		$datos=$this->usuario->vertipodocumentos();
		return $datos;
		}
		
		public function tipousuario(){
		$datos=$this->usuario->vertipousuario();
		return $datos;
		}
		
		public function buscarusuario($buscar,$inicio,$nroreg){
			$this->usuario->set("login",$buscar);
			$this->usuario->set("inicio",$inicio);
			$this->usuario->set("nroreg",$nroreg);	
			$datos=$this->usuario->buscarusuario();
			return $datos;
		}
			
		public function spinsertarmotivo($login,$descmotivo,$usuarioina){
		$this->usuario->set("login",$login);
		$this->usuario->set("descmotivo",$descmotivo);
		$this->usuario->set("usuariosession",$usuarioina);	
		$datos=$this->usuario->spinsertarmotivo();
		return $datos;
		}
		
		public function spactualizarmotivo($login){
		$this->usuario->set("login",$login);
		$datos=$this->usuario->spactualizarmotivo();
		return $datos;
		}
		
		public function mostrarmotivos($login){
		$this->usuario->set("login",$login);
		$datos=$this->usuario->mostrarmotivos();
		return $datos;
		}	
			
		public function cambiarpassword($login, $clave){
		$this->usuario->set("login",$login);
		$this->usuario->set("clave",$clave);
		$resultado=$this->usuario->cambiarpassword();
		return $resultado;

		}
		
		public function buscarmodulos($buscar,$inicio,$nroreg, $login,$acceso){
		$this->usuario->set("buscar",$buscar);
		$this->usuario->set("inicio",$inicio);
		$this->usuario->set("nroreg",$nroreg);	
		$this->usuario->set("login",$login);
		$this->usuario->set("codtipousuario",$acceso);
		$datos=$this->usuario->buscarmodulos();
		return $datos;
		}
			
		public function listarmodulos($login,$acceso){
		$this->usuario->set("login",$login);
		$this->usuario->set("codtipousuario",$acceso);
		$resultado=$this->usuario->listarmodulos();
		return $resultado;
		}	

		public function splistaregion(){
		$resultado=$this->usuario->splistaregion();
		return $resultado;
		}
		
		public function spareainteligensa(){
		$datos=$this->usuario->spareainteligensa();
		return $datos;
		}
		
		public function buscarnombre(){
		$datos=$this->usuario->buscarnombre();
		return $datos;
		}

		public function spregionesinteligensa(){
		$datos=$this->usuario->spregionesinteligensa();
		return $datos;
		}

		public function verificarcaptcha($sid,$getip,$code){
		$this->usuario->set("sid",$sid);
		$this->usuario->set("getip",$getip);
		$this->usuario->set("code",$code);	
		$datos=$this->usuario->verificarcaptcha();
		return $datos;
		}

		public function cargarintentos($login){
		$this->usuario->set("login",$login);
		$datos=$this->usuario->cargarintentos();
		return $datos;
		}
	
		public function cargarintentosrenew($login){
		$this->usuario->set("login",$login);
		$datos=$this->usuario->cargarintentosrenew();
		return $datos;
		}

		public function sp_resetactividad($login){
		$this->usuario->set("login",$login);
		$datos=$this->usuario->sp_resetactividad();
		return $datos;
		}
	    public function sp_actividad($id_registro,$activo,$usuario){
		$this->usuario->set("id_registro",$id_registro);
		$this->usuario->set("activo",$activo);
		$this->usuario->set("usuario",$usuario);
		$datos=$this->usuario->sp_actividad();
		return $datos;
		}

		public function sp_usuariologin($usuario){
		$this->usuario->set("usuario",$usuario);
		$datos=$this->usuario->sp_usuariologin();
		return $datos;
		}

		public function sp_buscarusuarios(){
		$datos=$this->usuario->sp_buscarusuarios();
		return $datos;
		}

		public function sp_selectReporteSuc($login){
		$this->usuario->set("login",$login);
		$datos=$this->usuario->sp_selectReporteSuc();
		return $datos;
		}

		public function sp_mostrarsucursal($idregion){
		$this->usuario->set("idregion",$idregion);
		$datos=$this->usuario->sp_mostrarsucursal();
		return $datos;
		}

		public function sp_selectsucursalasig($idregion,$login){
		$this->usuario->set("idregion",$idregion);
		$this->usuario->set("login",$login);
		$datos=$this->usuario->sp_selectsucursalasig();
		return $datos;
		}

		public function spguardarsucursal($idregion,$valor,$login,$usuario_acceso){
		$this->usuario->set("idregion",$idregion);
		$this->usuario->set("valor",$valor);
		$this->usuario->set("login",$login);
		$this->usuario->set("usuario_acceso",$usuario_acceso);
		$datos=$this->usuario->spguardarsucursal();
		return $datos;
		}

		public function spEliminarsucursalUsuario($idregion,$login){
		$this->usuario->set("idregion",$idregion);
		$this->usuario->set("login",$login);
		$datos=$this->usuario->spEliminarsucursalUsuario();
		return $datos;
		}
	}
		
?>
	