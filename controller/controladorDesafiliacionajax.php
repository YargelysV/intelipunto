<?php
include_once("../../model/Desafiliacion.php");

class controladorDesafiliacion{

	private $desafiliacion;


	public function __construct(){
		$this->desafiliacion= new desafiliacion();
	}

	public function verprefijobanco(){
		$datos=$this->desafiliacion->verprefijobanco();
		return $datos;
	}


	public function vertipodepos(){
		$datos=$this->desafiliacion->vertipodepos();
		return $datos;
	}

	public function vermarcadepos(){
		$datos=$this->desafiliacion->vermarcadepos();
		return $datos;
	}

		public function vertipodelinea(){
		$datos=$this->desafiliacion->vertipodelinea();
		return $datos;
	}

		public function vertipodemifi(){
		$datos=$this->desafiliacion->vertipodemifi();
		return $datos;
	}
	
	public function insertar_datosmasivo ($id,$serial,$rif,$razon,$adm,$observacion,$usuario){
		$this->desafiliacion->set("id",$id);
		$this->desafiliacion->set("serial",$serial);
		$this->desafiliacion->set("rif",$rif);
		$this->desafiliacion->set("razon",$razon);
		$this->desafiliacion->set("adm",$adm);
		$this->desafiliacion->set("observacion",$observacion);
		$this->desafiliacion->set("usuario",$usuario);
		
		$resultado=$this->desafiliacion->insertar_datosmasivo();
		return $resultado;
	}

		public function verseriales(){
		$datos=$this->desafiliacion->verseriales();
		return $datos;
	}	
	public function busquedamasiva ($busqueda){
		$this->desafiliacion->set("busqueda",$busqueda);
		$datos=$this->desafiliacion->busquedamasiva();
		return $datos;
	}

		public function busquedamasivaglobal (){
		$datos=$this->desafiliacion->busquedamasivaglobal();
		return $datos;
	}
}
?>
	