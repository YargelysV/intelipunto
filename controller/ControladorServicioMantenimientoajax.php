<?php
include_once("../../model/ServicioMantenimiento.php");

class controladorServicioMantenimiento{

	private $serviciomantenimiento;


	public function __construct(){
		$this->serviciomantenimiento= new ServicioMantenimiento();
	}

	public function spverfechamantenimiento(){
	$datos=$this->serviciomantenimiento->spverfechamantenimiento();
	return $datos;
	} 	

		public function spverestatuscliente(){
		$datos=$this->serviciomantenimiento->spverestatuscliente();
		return $datos;
	}

		public function spagregarbancoorigen(){
		$datos=$this->serviciomantenimiento->spagregarbancoorigen();
		return $datos;
	}

		public function sp_verformapago(){
		$datos=$this->serviciomantenimiento->sp_verformapago();
		return $datos;
	}
	
	public function sp_vermodalidadpago(){
		$datos=$this->serviciomantenimiento->sp_vermodalidadpago();
		return $datos;
	}

	public function sp_guardardomiciliacion($cliente,$documento,$razonsocial,$codformap, $ncuenta, $codigoestatus,$actividad,$cbanco,$usuario){
		$this->serviciomantenimiento->set("cliente",$cliente);
		$this->serviciomantenimiento->set("documento",$documento);
		$this->serviciomantenimiento->set("razonsocial",$razonsocial);
		$this->serviciomantenimiento->set("codformap",$codformap);
		$this->serviciomantenimiento->set("ncuenta",$ncuenta);
		$this->serviciomantenimiento->set("codigoestatus",$codigoestatus);
		$this->serviciomantenimiento->set("actividad",$actividad);
		$this->serviciomantenimiento->set("cbanco",$cbanco);
		$this->serviciomantenimiento->set("usuario",$usuario);
		$datos=$this->serviciomantenimiento->sp_guardardomiciliacion();
		return $datos;
	}


		public function ver_carta_domiciliacion($cliente){
		$this->serviciomantenimiento->set("cliente",$cliente);
		//$this->adjuntar->set("directorio",$directorio);
		//$this->adjuntar->set("usuario",$usuario);
		$datos=$this->serviciomantenimiento->ver_carta_domiciliacion();
		return $datos;
		}

		public function mostrarservicio($cliente,$documento){
		$this->serviciomantenimiento->set("cliente",$cliente);
		$this->serviciomantenimiento->set("documento",$documento);
		$datos=$this->serviciomantenimiento->mostrarservicio();
		return $datos;
		}

		public function spbuscarreportedomiciliacion($status, $modalidad){
		$this->serviciomantenimiento->set("status",$status);
		$this->serviciomantenimiento->set("modalidad",$modalidad);
		$datos=$this->serviciomantenimiento->spbuscarreportedomiciliacion();
		return $datos;

		}

		public function spbuscarregistrodomiciliacion($cliente){
		$this->serviciomantenimiento->set("cliente",$cliente);
		$datos=$this->serviciomantenimiento->spbuscarregistrodomiciliacion();
		return $datos;

		}	

		public function spbuscardomiciliacionxrif($rif){
		$this->serviciomantenimiento->set("rif",$rif);
		$datos=$this->serviciomantenimiento->spbuscardomiciliacionxrif();
		return $datos;

		}

}
?>
	