<?php

	include_once("../model/GestionMantenimiento.php");

		class controladorGestionMantenimiento
		{ 
			private $gestionmantenimiento;

			public function __construct(){
				$this->gestionmantenimiento= new GestionMantenimiento();
			}

			public function sp_zona($perfil){
			$this->gestionmantenimiento->set("perfil",$perfil);
			$datos=$this->gestionmantenimiento->sp_zona();
			return $datos;
			}

			public function spbuscarperfil(){
			$datos=$this->gestionmantenimiento->spbuscarperfil();
			return $datos;
			}

			public function spbuscartipozona(){
			$datos=$this->gestionmantenimiento->spbuscartipozona();
			return $datos;
			}
		
			public function crearrperfil($desc_perfil){
			// $this->gestionmantenimiento->set("cod_perfil",$desc_perfil);
			$this->gestionmantenimiento->set("desc_perfil",$desc_perfil);
			$datos=$this->gestionmantenimiento->crearrperfil();
			return $datos;
			}

		
			public function GestionarZonas($idzona, $idparroq,$usuario){
			$this->gestionmantenimiento->set("idzona",$idzona);
			$this->gestionmantenimiento->set("idparroq",$idparroq);
			$this->gestionmantenimiento->set("usuario",$usuario);
			$datos=$this->gestionmantenimiento->GestionarZonas();
			return $datos;
			}

			
			public function eliminarbancos($codigobanco){
			// $this->gestionmantenimiento->set("cod_perfil",$desc_perfil);
			$this->gestionmantenimiento->set("codigobanco",$codigobanco);
			$datos=$this->gestionmantenimiento->eliminarbancos();
			return $datos;
			}

			public function sp_agregarbancos($cod_perfil,$codigobanco){
		    $this->gestionmantenimiento->set("cod_perfil",$cod_perfil);
			$this->gestionmantenimiento->set("codigobanco",$codigobanco);
			$datos=$this->gestionmantenimiento->sp_agregarbancos();
			return $datos;
			}



			public function spagregarbancos(){
			$datos=$this->gestionmantenimiento->spagregarbancos();
			return $datos;
			}


		}


?>