<?php

include_once("model/Activacionequipos.php");

class ControladorActivacion{

	private $activacion;

	public function __construct(){
		$this->activacion= new Activacion();
	}		

	public function sp_buscaractivacion ($banco)
		{
			$this->activacion->set("clientes",$banco);
			$datos=$this->activacion->sp_buscaractivacion();
			return $datos;
		}

	public function sp_gestionaractivacion ($consecutivo)
		{
			$this->activacion->set("idcliente",$consecutivo);
			$datos=$this->activacion->sp_gestionaractivacion();
			return $datos;
		}

	public function sp_gestionaractivacionserial ($nroserial)
		{
			$this->activacion->set("nroserial",$nroserial);
			$datos=$this->activacion->sp_gestionaractivacionserial();
			return $datos;
		}
}

?>

