<?php

include_once("../model/Encuesta.php");

	class controladorEncuesta
	{
		private $encuesta;
		
		public function __construct()
		{
			$this->encuesta=new EncuestaNueva();
		}
		

		public function spbuscarfechaencuesta($cliente,$fecha,$origen){
		$this->encuesta->set("cliente",$cliente);
		$this->encuesta->set("FechaRecepcionArchivo",$fecha);
		$this->encuesta->set("origen",$origen);
		$datos=$this->encuesta->spbuscarfechaencuesta();
		return $datos;
	}
		

		public function sp_bancfechaencuesta($cliente, $fechadetalle, $namearchivo)
			{
				$this->encuesta->set("cliente",$cliente);
				$this->encuesta->set("fecha",$fechadetalle);
				$this->encuesta->set("namearchivo",$namearchivo);
				$datos=$this->encuesta->sp_bancfechaencuesta();
				return $datos;
			}
			public function spbuscarafiliadorifencuesta($rif){
				$this->encuesta->set("rif",$rif);
				$datos=$this->encuesta->spbuscarafiliadorifencuesta();
				return $datos;
			}

			public function spbuscarpersonareferida($idcliente){
				$this->encuesta->set("idcliente",$idcliente);
				$datos=$this->encuesta->spbuscarpersonareferida();
				return $datos;
	}		

}
?>