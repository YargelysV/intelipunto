<?php
	//session_start();
	include_once("../model/Pagos.php");

	class controladorPagos{

		private $pagos;

		public function __construct(){
		$this->pagos= new Pagos();
		}
		
		public function sp_verpagos($idregistro){
		$this->pagos->set("idafiliado",$idregistro);
		$datos=$this->pagos->sp_verpagos();
		return $datos;
	}

		public function eliminarPago($referencia,$idafiliado){
		$this->pagos->set("referencia",$referencia);
		$this->pagos->set("idafiliado",$idafiliado);
		$datos=$this->pagos->eliminarPago();
		return $datos;
		}

		public function sp_verpagosdemifi($id_afiliado){
		$this->pagos->set("idafiliado",$id_afiliado);
		$datos=$this->pagos->sp_verpagosdemifi();
		return $datos;
		}

		
	}


?>
