<?php
include_once("model/Adjuntar.php");

class controladorAdjuntar{

	private $adjuntar;


	public function __construct(){
		$this->adjuntar= new Adjuntar();
	}

	public function verprefijobanco(){
		$datos=$this->adjuntar->verprefijobanco();
		return $datos;
	}


	public function vertipodepos(){
		$datos=$this->adjuntar->vertipodepos();
		return $datos;
	}

	public function vermarcadepos(){
		$datos=$this->adjuntar->vermarcadepos();
		return $datos;
	}

		public function vertipodelinea(){
		$datos=$this->adjuntar->vertipodelinea();
		return $datos;
	}

		public function vertipodemifi(){
		$datos=$this->adjuntar->vertipodemifi();
		return $datos;
	}
	
	public function insertar_tabltrans ($iproveedor,$ifechacompra,$imarcapos,$numserie, $tipopos,$iactivofijo, $ibancoasignado,$usuario,$idlotes){
		$this->adjuntar->set("iproveedor",$iproveedor);
		$this->adjuntar->set("ifechacompra",$ifechacompra);
		$this->adjuntar->set("imarcapos",$imarcapos);
		$this->adjuntar->set("numserie",$numserie);
		$this->adjuntar->set("tipopos",$tipopos);
		$this->adjuntar->set("iactivofijo",$iactivofijo);
		$this->adjuntar->set("ibancoasignado",$ibancoasignado);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("idlotes",$idlotes);
		$resultado=$this->adjuntar->insertar_tabltrans();
		return $resultado;
	}


	public function insertar_tablasimcards ($iproveedor,$ifechacompra,$numserie, $tiposim,$usuario){
		$this->adjuntar->set("iproveedor",$iproveedor);
		$this->adjuntar->set("ifechacompra",$ifechacompra);
		$this->adjuntar->set("numserie",$numserie);
		$this->adjuntar->set("tiposim",$tiposim);
		$this->adjuntar->set("usuario",$usuario);
		$resultado=$this->adjuntar->insertar_tablasimcards();
		return $resultado;
	}

	public function insertar_tablamifi ($iproveedor,$ifechacompra,$numserie, $tipomifi,$usuario){
		$this->adjuntar->set("iproveedor",$iproveedor);
		$this->adjuntar->set("ifechacompra",$ifechacompra);
		$this->adjuntar->set("numserie",$numserie);
		$this->adjuntar->set("tipomifi",$tipomifi);
		$this->adjuntar->set("usuario",$usuario);
		$resultado=$this->adjuntar->insertar_tablamifi();
		return $resultado;
	}

	public function insertar_tablclient ($TipoCliente,$CustomerName, $ShortName, $StatementName, $CustomerClass, $LookupButton2, $AddressCode, $ContactPerson, $Address1, $Address2, $Address3, $City, $State, $Zip, $CountryCode, $Country, $Phone1, $Phone2, $Phone3, $Fax, $UPSZone, $ShippingMethod, $TaxScheduleID, $ShipCompletDocument, $PrimaryShiptoAddressCode, $SalespersonID, $LookupButton6, $UserDefined1, $UserDefined2, $Comment1, $Comment2, 	$CustomerDiscount, $PaymentTermsID, $DiscountGracePeriod, $DueDateGracePeriod, $PriceLevel, $AccountsButton, $OptionsButton, $TaxRegistrationNumber, $CurrencyID, $EmailStatementsToAddress, $usuario){
		$this->adjuntar->set("TipoCliente",$TipoCliente);
		$this->adjuntar->set("CustomerName",$CustomerName);
		$this->adjuntar->set("ShortName",$ShortName);
		$this->adjuntar->set("StatementName",$StatementName);
		$this->adjuntar->set("CustomerClass",$CustomerClass);
		$this->adjuntar->set("LookupButton2",$LookupButton2);
		$this->adjuntar->set("AddressCode",$AddressCode);
		$this->adjuntar->set("ContactPerson",$ContactPerson);
		$this->adjuntar->set("Address1",$Address1);
		$this->adjuntar->set("Address2",$Address2);
		$this->adjuntar->set("Address3",$Address3);
		$this->adjuntar->set("City",$City);
		$this->adjuntar->set("State",$State);
		$this->adjuntar->set("Zip",$Zip);
		$this->adjuntar->set("CountryCode",$CountryCode);
		$this->adjuntar->set("Country",$Country);
		$this->adjuntar->set("Phone1",$Phone1);
		$this->adjuntar->set("Phone2",$Phone2);
		$this->adjuntar->set("Phone3",$Phone3);
		$this->adjuntar->set("Fax",$Fax);
		$this->adjuntar->set("UPSZone",$UPSZone);
		$this->adjuntar->set("ShippingMethod",$ShippingMethod);
		$this->adjuntar->set("TaxScheduleID",$TaxScheduleID);
		$this->adjuntar->set("ShipCompletDocument",$ShipCompletDocument);
		$this->adjuntar->set("PrimaryShiptoAddressCode",$PrimaryShiptoAddressCode);
		$this->adjuntar->set("SalespersonID",$SalespersonID);
		$this->adjuntar->set("LookupButton6",$LookupButton6);
		$this->adjuntar->set("UserDefined1",$UserDefined1);
		$this->adjuntar->set("UserDefined2",$UserDefined2);
		$this->adjuntar->set("Comment1",$Comment1);
		$this->adjuntar->set("Comment2",$Comment2);
		$this->adjuntar->set("CustomerDiscount",$CustomerDiscount);
		$this->adjuntar->set("PaymentTermsID",$PaymentTermsID);
		$this->adjuntar->set("DiscountGracePeriod",$DiscountGracePeriod);
		$this->adjuntar->set("DueDateGracePeriod",$DueDateGracePeriod);
		$this->adjuntar->set("PriceLevel",$PriceLevel);
		$this->adjuntar->set("AccountsButton",$AccountsButton);
		$this->adjuntar->set("OptionsButton",$OptionsButton);
		$this->adjuntar->set("TaxRegistrationNumber",$TaxRegistrationNumber);
		$this->adjuntar->set("CurrencyID",$CurrencyID);
		$this->adjuntar->set("EmailStatementsToAddress",$EmailStatementsToAddress);
		$this->adjuntar->set("usuario",$usuario);
		$resultado=$this->adjuntar->insertar_tablclient();
		return $resultado;
	}


	public function vertablatransferencia(){
		$datos=$this->adjuntar->vertablatransferencia();
		return $datos;
	}

	public function vertablatransferenciacliente(){
		$datos=$this->adjuntar->vertablatransferenciacliente();
		return $datos;
	}

	public function spverarchivodiario(){
		$datos=$this->adjuntar->spverarchivodiario();
		return $datos;
	}	

	public function spverarchivodiariocliente($cliente, $fecha){
		$this->adjuntar->set("cliente",$cliente);
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->spverarchivodiariocliente();
		return $datos;
	}	

	public function eliminar_tabltranscliente(){
		$this->adjuntar->eliminar_tabltranscliente();
	}	

	
	public function eliminar_tabltranstxt(){
		$this->adjuntar->eliminar_tabltranstxt();
	}

	public function insertar_tabltranstxt ($numserie){
		$this->adjuntar->set("numserie",$numserie);
		$resultado=$this->adjuntar->insertar_tabltranstxt();
		return $resultado;
	}

	public function spverarchivotxt(){
		$datos=$this->adjuntar->spverarchivotxt();
		return $datos;
	}	
		public function sp_verpagoaconfirmar($idafiliado,$referencia){
		$this->adjuntar->set("idafiliado",$idafiliado);
		$this->adjuntar->set("referencia",$referencia);
		$datos=$this->adjuntar->sp_verpagoaconfirmar();
		return $datos;
	}

/*			public function spfacturaanuladasdemifi($idregistro){
		$this->adjuntar->set("idregistro",$idregistro);
		$datos=$this->adjuntar->spfacturaanuladasdemifi();
		return $datos;
	}*/

		public function spinsertarfactura_mifi($afiliado,$directorio){
	$this->adjuntar->set("afiliado",$afiliado);
	$this->adjuntar->set("directorio",$directorio);
	$datos=$this->adjuntar->spinsertarfactura_mifi();
	return $datos;
	}

		public function ver_facturademifi($idregistro){
	$this->adjuntar->set("idregistro",$idregistro);
	$datos=$this->adjuntar->ver_facturademifi();
	return $datos;
	}

	public function spinsertarexpfactura($idregistro,$directorio){
		//gestioncomercial
		$this->adjuntar->set("idregistro",$idregistro);
		$this->adjuntar->set("directorio",$directorio);
		//$this->adjuntar->set("usuario",$usuario);
		$datos=$this->adjuntar->spinsertarexpfactura();
		return $datos;
		}


	public function sp_confirmarpagomifi($idafiliado,$referencia){
		$this->adjuntar->set("idafiliado",$idafiliado);
		$this->adjuntar->set("referencia",$referencia);
		$datos=$this->adjuntar->sp_confirmarpagomifi();
		return $datos;
	}

	public function verseriales(){
		$datos=$this->adjuntar->verseriales();
		return $datos;
	}	

	public function verserialessimcards(){
		$datos=$this->adjuntar->verserialessimcards();
		return $datos;
	}

	public function verserialesmifi(){
		$datos=$this->adjuntar->verserialesmifi();
		return $datos;
	}

	public function sp_buscarserialespos($fecha){
	$this->adjuntar->set("fecha",$fecha);
	$datos=$this->adjuntar->sp_buscarserialespos();
	return $datos;
	}

	public function sp_buscarserialessimcards($fecha){
	$this->adjuntar->set("fecha",$fecha);
	$datos=$this->adjuntar->sp_buscarserialessimcards();
	return $datos;
	}

	public function sp_buscarserialesmifi($fecha){
	$this->adjuntar->set("fecha",$fecha);
	$datos=$this->adjuntar->sp_buscarserialesmifi();
	return $datos;
	}

	public function usuario($usuario){
	$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->usuario();
	return $datos;
	}

	public function sp_reportedetalladopos($fecha){
	$this->adjuntar->set("fecha",$fecha);
	$datos=$this->adjuntar->sp_reportedetalladopos();
	return $datos;
	}


	public function sp_reportegeneralpos($fecha,$marcapos,$operacion){
	$this->adjuntar->set("fecha",$fecha);
	$this->adjuntar->set("marcapos",$marcapos);
	$this->adjuntar->set("operacion",$operacion);
	$datos=$this->adjuntar->sp_reportegeneralpos();
	return $datos;
	}

	public function sp_buscarclienterecibido($cliente, $fecha){
		$this->adjuntar->set("cliente",$cliente);
		$this->adjuntar->set("fecha",$fecha);
		$datos=$this->adjuntar->sp_buscarclienterecibido();
		return $datos;
	}

	public function spverbancocliente(){
	$datos=$this->adjuntar->spverbancocliente();
	return $datos;
	}


	public function spbanco($cliente){
		$this->adjuntar->set("cliente",$cliente);
		$datos=$this->adjuntar->spbanco();
		return $datos;
	}

	public function spcliente($rif){
		$this->adjuntar->set("rif",$rif);
		$datos=$this->adjuntar->spcliente();
		return $datos;
	}

	public function sp_buscarclientefacturacionRif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->adjuntar->set("rif",$rif);
		$this->adjuntar->set("tipousuario",$tipousuario);
		$this->adjuntar->set("ejecutivo",$ejecutivo);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("coordinador",$coordinador);
		$this->adjuntar->set("coordinadordos",$coordinadordos);
		$datos=$this->adjuntar->sp_buscarclientefacturacionRif();
		return $datos;
	}

	public function spbuscarclientefacturacionmifixrif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->adjuntar->set("rif",$rif);
		$this->adjuntar->set("tipousuario",$tipousuario);
		$this->adjuntar->set("ejecutivo",$ejecutivo);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("coordinador",$coordinador);
		$this->adjuntar->set("coordinadordos",$coordinadordos);
		$datos=$this->adjuntar->spbuscarclientefacturacionmifixrif();
		return $datos;
	}
	public function sp_buscarclientefacturacion($cliente,$fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("FechaRecepcionArchivo",$fecha);
	$this->adjuntar->set("origen",$origen);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_buscarclientefacturacion();
	return $datos;
	}

	public function sp_buscarclientefacturacionmifi($cliente,$fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("FechaRecepcionArchivo",$fecha);
	$this->adjuntar->set("origen",$origen);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_buscarclientefacturacionmifi();
	return $datos;
	}

		public function sp_verclientefacturacion($cliente, $fechadetalle, $namearchivo, $masiva, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("fecha",$fechadetalle);
	$this->adjuntar->set("namearchivo",$namearchivo);
	$this->adjuntar->set("masiva",$masiva);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_verclientefacturacion();
	return $datos;
	}


	public function sp_verclientefacturacionmifi($cliente, $fechadetalle, $namearchivo, $masiva, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
	$this->adjuntar->set("cliente",$cliente);
	$this->adjuntar->set("fecha",$fechadetalle);
	$this->adjuntar->set("namearchivo",$namearchivo);
	$this->adjuntar->set("masiva",$masiva);
	$this->adjuntar->set("tipousuario",$tipousuario);
	$this->adjuntar->set("ejecutivo",$ejecutivo);
	$this->adjuntar->set("usuario",$usuario);
	$this->adjuntar->set("coordinador",$coordinador);
	$this->adjuntar->set("coordinadordos",$coordinadordos);
	$datos=$this->adjuntar->sp_verclientefacturacionmifi();
	return $datos;
	}
	public function mostrarClienteFacturacion($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->mostrarClienteFacturacion();
	return $datos;
	}

	public function mostrarClienteFacturacionmifi($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->mostrarClienteFacturacionmifi();
	return $datos;
	}

	public function sp_buscarinventariopos(){
		$datos=$this->adjuntar->sp_buscarinventariopos();
		return $datos;
	}	

	public function spverificarmodulosactivos($login){
		$this->adjuntar->set("login",$login);
		$resultado=$this->adjuntar->spverificarmodulosactivos();
		return $resultado;
		}

	public function spregistrocliente($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->spregistrocliente();
	return $datos;
	}

	// datos nuevos para la asignacion de seriales. 

	public function mostrarClienteFacturasanuladas($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->mostrarClienteFacturasanuladas();
	return $datos;
	}

	public function mostrarClientedeFacturasanuladasmifi($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->mostrarClientedeFacturasanuladasmifi();
	return $datos;
	}

	 public function mostrarseriales($id_registro){
	 $this->adjuntar->set("id_registro",$id_registro);
	 $datos=$this->adjuntar->mostrarseriales();
	 return $datos;
	 }


	 public function mostrarserialesmifi($id_registro){
	 $this->adjuntar->set("id_registro",$id_registro);
	 $datos=$this->adjuntar->mostrarserialesmifi();
	 return $datos;
	 }

	 public function mostrardetalleseriales($id_registro){
	 $this->adjuntar->set("id_registro",$id_registro);
	 $datos=$this->adjuntar->mostrardetalleseriales();
	 return $datos;
	 }

	 	public function spbuscarserialesdevolucion($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialesdevolucion();
	return $datos;
	}
	public function spbuscarserialsimcarddevolucion($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialsimcarddevolucion();
	return $datos;
	}	
	public function spbuscarserialmifidev($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialmifidev();
	return $datos;
	}

	 public function mostrardetalleserialesdevtotal($id_registro){
	 $this->adjuntar->set("id_registro",$id_registro);
	 $datos=$this->adjuntar->mostrardetalleserialesdevtotal();
	 return $datos;
	 }

	 public function mostrardetalleserialesdemifi($id_registro){
	 $this->adjuntar->set("id_registro",$id_registro);
	 $datos=$this->adjuntar->mostrardetalleserialesdemifi();
	 return $datos;
	 }

	 public function spvermotivodev(){
	//$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spvermotivodev();
	return $datos;
	}

	 public function spverdestinoequipo(){
	//$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spverdestinoequipo();
	return $datos;
	}

	 public function spverdestinoequipomifi(){
	$datos=$this->adjuntar->spverdestinoequipomifi();
	return $datos;
	}


	public function spbuscarseriales($busqueda,$banco){
	$this->adjuntar->set("busqueda",$busqueda);
	$this->adjuntar->set("banco",$banco);
	$datos=$this->adjuntar->spbuscarseriales();
	return $datos;
	}

		public function spinsertarfactura($afiliado,$directorio){
	$this->adjuntar->set("afiliado",$afiliado);
	$this->adjuntar->set("directorio",$directorio);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->spinsertarfactura();
	return $datos;
	}

	public function spbuscarserialsimcard($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialsimcard();
	return $datos;
	}

	public function spbuscarserialmifi($busqueda){
	$this->adjuntar->set("busqueda",$busqueda);
	$datos=$this->adjuntar->spbuscarserialmifi();
	return $datos;
	}

		public function ver_factura($idregistro){
	$this->adjuntar->set("idregistro",$idregistro);
	//$this->adjuntar->set("directorio",$directorio);
	//$this->adjuntar->set("usuario",$usuario);
	$datos=$this->adjuntar->ver_factura();
	return $datos;
	}

	public function spfacturaanuladas($idregistro){
		$this->adjuntar->set("idregistro",$idregistro);
		$datos=$this->adjuntar->spfacturaanuladas();
		return $datos;
	}

		public function spfacturaanuladasdemifi($idregistro){
		$this->adjuntar->set("idregistro",$idregistro);
		$datos=$this->adjuntar->spfacturaanuladasdemifi();
		return $datos;
	}

	public function spinfocliente($idcliente){
		$this->adjuntar->set("idcliente",$idcliente);
		$datos=$this->adjuntar->spinfocliente();
		return $datos;
	}

	public function sp_verpagos($idregistro){
		$this->adjuntar->set("id_afiliado",$idregistro);
		$datos=$this->adjuntar->sp_verpagos();
		return $datos;
	}

	public function sp_verpagosdemifi($idregistro){
		$this->adjuntar->set("idafiliado",$idregistro);
		$datos=$this->adjuntar->sp_verpagosdemifi();
		return $datos;
	}

	public function spinsertardocumentosfactura($idregistro,$directorio,$afiliado,$usuario,$eexp){
		//gestioncomercial no es
		$this->adjuntar->set("idregistro",$idregistro);
		$this->adjuntar->set("directorio",$directorio);
		$this->adjuntar->set("afiliado",$afiliado);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("eexp",$eexp);
		//$this->adjuntar->set("usuario",$usuario);
		$datos=$this->adjuntar->spinsertardocumentosfactura();
		return $datos;
		}
		// separador pa no olvidar
	public function spinsertarhistorial($idregistro,$afiliado,$usuario,$mensaje,$detalles){
		//gestioncomercial
		$this->adjuntar->set("idregistro",$idregistro);
		$this->adjuntar->set("afiliado",$afiliado);
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("mensaje",$mensaje);
		$this->adjuntar->set("detalles",$detalles);
		//$this->adjuntar->set("usuario",$usuario);
		$datos=$this->adjuntar->spinsertarhistorial();
		return $datos;
		}
	//


	public function spbuscarpercance($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->spbuscarpercance();
	return $datos;
	}

	public function spvertipopercance(){
		$datos=$this->adjuntar->spvertipopercance();
		return $datos;
	}
	
	public function sp_verpago($idregistro,$idpagos){
		$this->adjuntar->set("id_afiliado",$idregistro);
		$this->adjuntar->set("idpagos",$idpagos);
		$datos=$this->adjuntar->sp_verpago();
		return $datos;
	}

	public function estatusconfirpagos($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->estatusconfirpagos();
	return $datos;
	}

	public function verconsecutivo(){
		$datos=$this->adjuntar->verconsecutivo();
		return $datos;
	}	

	public function sp_costoposs($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->sp_costoposs();
	return $datos;
	}

	public function sp_regmodalidad($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->sp_regmodalidad();
	return $datos;
	}
// 
public function sp_reporteriesgomedidoadpins($idregistro){
	$this->adjuntar->set("idregistro",$idregistro);
	$datos=$this->adjuntar->sp_reporteriesgomedidoadpins();
	return $datos;
	}
// 
	public function spverclientefacturacionpagos($usuario, $ejecutivoin, $tipousuario, $coordinador,$coordinadordos){
		$this->adjuntar->set("usuario",$usuario);
		$this->adjuntar->set("ejecutivoin",$ejecutivoin);
		$this->adjuntar->set("tipousuario",$tipousuario);
		$this->adjuntar->set("coordinador",$coordinador);
		$this->adjuntar->set("coordinadordos",$coordinadordos);
		$datos=$this->adjuntar->spverclientefacturacionpagos();
		return $datos;
	}

	public function sp_serialesrecompra($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->sp_serialesrecompra();
	return $datos;
	}

	public function spmostrarclientefacturacion_recompra($id_registro){
	$this->adjuntar->set("id_registro",$id_registro);
	$datos=$this->adjuntar->spmostrarclientefacturacion_recompra();
	return $datos;
	}

	public function mostrardetalleserialesrecompra($id_registro){
	 $this->adjuntar->set("id_registro",$id_registro);
	 $datos=$this->adjuntar->mostrardetalleserialesrecompra();
	 return $datos;
	 }
	//  
	public function sp_verdescripcionmotivos($detalledesaprob){
		$this->adjuntar->set("detalledesaprob",$detalledesaprob);
		$datos=$this->adjuntar->sp_verdescripcionmotivos();
		return $datos;
	}
}
?>
	