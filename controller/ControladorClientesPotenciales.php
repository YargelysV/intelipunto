<?php
include_once("model/ClientePotencial.php");

class controladorClientePotencial{

	private $clientepotencial;


	public function __construct(){
		$this->clientepotencial= new ClientePotencial();
	}

		public function eliminar_tablatransclientep(){
		$this->clientepotencial->speliminar_tablatransclientep();
	}

	public function verprefijobanco(){
		$datos=$this->clientepotencial->verprefijobanco();
		return $datos;
	}

	public function veractividadeconomica(){
		$datos=$this->clientepotencial->veractividadeconomica();
		return $datos;
	}	

	public function vertipodepos(){
		$datos=$this->clientepotencial->vertipodepos();
		return $datos;
	}

	public function vertipodelinea(){
		$datos=$this->clientepotencial->vertipodelinea();
		return $datos;
	}

	public function insertar_tabltranscliente ($Consecutivo,$fechahoy, $PrefijoBanco,$TipoCliente, $CantTerminales, $NombreAfiliado, $RazonComercial, $Rif, $ActividadEconomica, $NombreRepresentanteLegal, $CorreoElectronico, $Telefono1,  $Telefono2, $direccion, $Estado, $Municipio, $Parroquia, $agencia, $afiliado, $ejecutivo, $usuario, $name, $size,  $fechacarga, $bancoasignado,$Rifbanco){
		$this->clientepotencial->set("Consecutivo",$Consecutivo);
		$this->clientepotencial->set("fechahoy",$fechahoy);
		$this->clientepotencial->set("PrefijoBanco",$PrefijoBanco);
		$this->clientepotencial->set("TipoCliente",$TipoCliente);
		$this->clientepotencial->set("CantTerminales",$CantTerminales);
		$this->clientepotencial->set("NombreAfiliado",$NombreAfiliado);
		$this->clientepotencial->set("RazonComercial",$RazonComercial);
		$this->clientepotencial->set("Rif",$Rif);
		$this->clientepotencial->set("ActividadEconomica",$ActividadEconomica);
		$this->clientepotencial->set("NombreRepresentanteLegal",$NombreRepresentanteLegal);
		$this->clientepotencial->set("CorreoElectronico",$CorreoElectronico);
		$this->clientepotencial->set("Telefono1",$Telefono1);
		$this->clientepotencial->set("Telefono2",$Telefono2);
		$this->clientepotencial->set("Direccion",$direccion);
		$this->clientepotencial->set("Estado",$Estado);
		$this->clientepotencial->set("Municipio",$Municipio);
		$this->clientepotencial->set("Parroquia",$Parroquia);
		$this->clientepotencial->set("agencia",$agencia);
		$this->clientepotencial->set("NAfiliacion",$afiliado);
		$this->clientepotencial->set("ejecutivo",$ejecutivo);
		$this->clientepotencial->set("usuario",$usuario);
		$this->clientepotencial->set("name",$name);
		$this->clientepotencial->set("size",$size);
		$this->clientepotencial->set("fechacarga",$fechacarga);
		$this->clientepotencial->set("bancoasignado",$bancoasignado);
		$this->clientepotencial->set("Rifbanco",$Rif);
		$resultado=$this->clientepotencial->insertar_tabltranscliente();
		return $resultado;
	}

	
	public function vertablatransclientepotencial()
	{
		$datos=$this->clientepotencial->vertablatransclientepotencial();
		return $datos;
	}

	public function vertblverificacionarchivo()
	{
		$datos=$this->clientepotencial->vertblverificacionarchivo();
		return $datos;
	}

	public function verclientepotencialcargados()
	{
		$datos=$this->clientepotencial->verclientepotencialcargados();
		return $datos;
	}

	public function adjuntartablatransclientepotencial ()
	{
		$datos=$this->clientepotencial->adjuntartablatransclientepotencial();
		return $datos;
	}	

	public function eliminarduplicadoscargar ()
	{
		$datos=$this->clientepotencial->eliminarduplicadoscargar();
		return $datos;
	}	

	public function eliminarduplicadosbd ()
	{
		$datos=$this->clientepotencial->eliminarduplicadosbd();
		return $datos;
	}	

	public function verclientepotencialbuscar ($banco, $fecha, $namearchivo)
	{
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$this->clientepotencial->set("name",$namearchivo);
		$datos=$this->clientepotencial->verclientepotencialbuscar();
		return $datos;
	}

		public function verclientepotencial ($banco, $fecha, $namearchivo)
	{
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		$this->clientepotencial->set("name",$namearchivo);
		$datos=$this->clientepotencial->verclientepotencial();
		return $datos;
	}

	public function verbanco ($banco)
	{
		$this->clientepotencial->set("PrefijoBanco",$banco);
		$datos=$this->clientepotencial->verbanco();
		return $datos;
	}	

		public function verarchivocargado ($name)
	{
		$this->clientepotencial->set("name",$name);
		$datos=$this->clientepotencial->verarchivocargado();
		return $datos;
	}	

	public function verafiliados ()
	{
		$datos=$this->clientepotencial->verafiliados();
		return $datos;
	}	

	public function verejecutivos ()
	{
		$datos=$this->clientepotencial->verejecutivos();
		return $datos;
	}	

	public function vertipodemodelo(){
		$datos=$this->clientepotencial->vertipodemodelo();
		return $datos;
	}

	public function vertipocliente(){
		$datos=$this->clientepotencial->vertipocliente();
		return $datos;
	}

	public function verclienterepetidos(){
		$datos=$this->clientepotencial->verclienterepetidos();
		return $datos;
	}

	public function sp_buscarclienterecibidos($banco, $fecha){
		$this->clientepotencial->set("banco",$banco);
		$this->clientepotencial->set("FechaRecepcionArchivo",$fecha);
		
		$datos=$this->clientepotencial->sp_buscarclienterecibidos();
		return $datos;
	}

	public function sp_buscarclienter($cliente){
	$this->clientepotencial->set("cliente",$cliente);
	$datos=$this->clientepotencial->sp_buscarclienter();
	return $datos;
	}

	public function verestados(){
		$datos=$this->clientepotencial->verestados();
		return $datos;
	}

	public function vermunicipios(){
		$datos=$this->clientepotencial->vermunicipios();
		return $datos;
	}

	public function verparroquia(){
		$datos=$this->clientepotencial->verparroquia();
		return $datos;
	}

public function verificardireccion(){
		$datos=$this->clientepotencial->verificardireccion();
		return $datos;
	}

	public function validador_duplicidad(){
		//$this->clientepotencial->set("razonsocial",$razonsocial);
		//$this->clientepotencial->set("rif",$rif);
		$datos=$this->clientepotencial->validador_duplicidad();
		return $datos;
	}

		public function eliminarduplicados2cargar ()
	{
		$datos=$this->clientepotencial->eliminarduplicados2cargar();
		return $datos;
	}	

	public function vercategoriacomercio(){
		$datos=$this->clientepotencial->vercategoriacomercio();
		return $datos;
	}	

	public function versubcategoriacomercio(){
		$datos=$this->clientepotencial->versubcategoriacomercio();
		return $datos;
	}

	
}
?>
	