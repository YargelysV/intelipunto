<?php

include_once("../model/gestionComercializacion.php");

	class ControladorComercializacion{

		private $gestioncomercial;

		public function __construct(){
			$this->gestioncomercial= new Comercializacion();
		}

		public function sp_buscarclientecomercializacion($cliente, $fecha, $origen, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->gestioncomercial->set("cliente",$cliente);
		$this->gestioncomercial->set("FechaRecepcionArchivo",$fecha);
		$this->gestioncomercial->set("origen",$origen);
		$this->gestioncomercial->set("tipousuario",$tipousuario);
		$this->gestioncomercial->set("ejecutivo",$ejecutivo);
		$this->gestioncomercial->set("usuario",$usuario);
		$this->gestioncomercial->set("coordinador",$coordinador);
		$this->gestioncomercial->set("coordinadordos",$coordinadordos);
		$datos=$this->gestioncomercial->sp_buscarclientecomercializacion();
		return $datos;
		}

		public function verclientecomercializacionbuscar ($banco, $fecha, $namearchivo, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos)
		{
		$this->gestioncomercial->set("PrefijoBanco",$banco);
		$this->gestioncomercial->set("FechaRecepcionArchivo",$fecha);
		$this->gestioncomercial->set("namearchivo",$namearchivo);
		$this->gestioncomercial->set("tipousuario",$tipousuario);
		$this->gestioncomercial->set("ejecutivo",$ejecutivo);
		$this->gestioncomercial->set("usuario",$usuario);
		$this->gestioncomercial->set("coordinador",$coordinador);
		$this->gestioncomercial->set("coordinadordos",$coordinadordos);
		$datos=$this->gestioncomercial->verclientecomercializacionbuscar();
		return $datos;
		}

		public function sp_buscarclientecomercializacionRif($rif, $tipousuario, $ejecutivo, $usuario,$coordinador,$coordinadordos){
		$this->gestioncomercial->set("Rif",$rif);
		$this->gestioncomercial->set("tipousuario",$tipousuario);
		$this->gestioncomercial->set("ejecutivo",$ejecutivo);
		$this->gestioncomercial->set("usuario",$usuario);
		$this->gestioncomercial->set("coordinador",$coordinador);
		$this->gestioncomercial->set("coordinadordos",$coordinadordos);
		$datos=$this->gestioncomercial->sp_buscarclientecomercializacionRif();
		return $datos;
	    }
         

         
	    public function sp_verestatusgestion($operacion,$id_registro,$namearchivo){
	    $this->gestioncomercial->set("Operacion",$operacion);
		$this->gestioncomercial->set("id_registro",$id_registro);
		$this->gestioncomercial->set("namearchivo",$namearchivo);
		$datos=$this->gestioncomercial->sp_verestatusgestion();
		return $datos;
		}

		public function sp_verpagos($idregistro){
			$this->adjuntar->set("id_afiliado",$idregistro);
			$datos=$this->adjuntar->sp_verpagos();
			return $datos;
		}
}

?>

