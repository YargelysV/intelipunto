<?php

	include_once("../../model/ServicioTecnico.php");

		class controladorServicioTecnico
		{ 
			private $servitec;

			public function __construct(){
				$this->servitec= new Serviciotec();
			}
		
			public function mostrarserviciotecnico(){
				$datos=$this->servitec->mostrarserviciotec();
				return $datos;
			}
			

			public function buscarserviciotecnico($busqueda){
				$this->servitec->set("busqueda",$busqueda);
			$datos=$this->servitec->buscarservcio();
			return $datos;
			}


			public function spagregardatosins ($serial,$namearchivo,$fecharecpos,$coditecnico, $fechagespos, $estatuspos, $fechainspos, $usuariocarg,$nroafiliacion)
        	{
        		$this->servitec->set("serial",$serial);
        		$this->servitec->set("namearchivo",$namearchivo);
				$this->servitec->set("fecharecpos",$fecharecpos);
				$this->servitec->set("coditecnico",$coditecnico);
				$this->servitec->set("fechagespos",$fechagespos);
				$this->servitec->set("estatuspos",$estatuspos);
				$this->servitec->set("fechainspos",$fechainspos);
				$this->servitec->set("usuariocarg",$usuariocarg);
				$this->servitec->set("nroafiliacion",$nroafiliacion);
				$resultado=$this->servitec->spagregardatosins();
				return $resultado;
			}

			
				public function spvertecnicos(){
				$datos=$this->servitec->spvertecnicos();
				return $datos;
			}

				public function spestatusinspos(){
				$datos=$this->servitec->spestatusinspos();
				return $datos;
				}

				public function sp_guardarnafiliadoservicio($idcliente,$nafiliacion){
				$this->servitec->set("idcliente",$idcliente);
				$this->servitec->set("nafiliacion",$nafiliacion);
				$datos=$this->servitec->sp_guardarnafiliadoservicio();
				return $datos;
				}

				public function selecttecnicos(){
				$datos=$this->servitec->selecttecnicos();
				return $datos;
				}

				public function selectestatuspos($codestatus,$codregion, $idcliente){
				$this->servitec->set("codestatus",$codestatus);
				$this->servitec->set("idestgestion",$codregion);
				$this->servitec->set("idcliente",$idcliente);
				$datos=$this->servitec->selectestatusposi();
				return $datos;
				}

				public function selectversionaplicativo($selectperfil){
				$this->servitec->set("idperfil",$selectperfil);
				$datos=$this->servitec->selectversionaplicativo();
				return $datos;
				}


				public function selectperfilaplicativo($selectswitch){
				$this->servitec->set("idswitch",$selectswitch);
				$datos=$this->servitec->selectperfilaplicativo();
				return $datos;
				}

		}


?>