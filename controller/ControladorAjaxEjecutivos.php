<?php
	//session_start();
	include_once("../model/Ejecutivo.php");

	class controladorEjecutivo{

		private $ejecutivos;

		public function __construct(){
			$this->ejecutivos= new Ejecutivo();
		}

		public function mostrarEjecutivos(){
			$datos=$this->ejecutivos->mostrarEjecutivos();
			return $datos;
		}

		public function verEjecutivosBancos(){
			$datos=$this->ejecutivos->verEjecutivosBancos();
			return $datos;
		}

		public function EditarEjecutivo ($operacion, $codejecutivo,$aliasvend, $tipodoc, $documento, $nombres, $apellidos, $ejecutivo,$usuario)
        	{
				$this->ejecutivos->set("operacion",$operacion);
				$this->ejecutivos->set("codejecutivo",$codejecutivo);
				$this->ejecutivos->set("nejecutivo",$aliasvend);
				$this->ejecutivos->set("tipodoc",$tipodoc);
				$this->ejecutivos->set("documento",$documento);
				$this->ejecutivos->set("nombres",$nombres);
				$this->ejecutivos->set("apellidos",$apellidos);
				$this->ejecutivos->set("ejecutivo",$ejecutivo);
				$this->ejecutivos->set("usuario",$usuario);
				$resultado=$this->ejecutivos->EditarEjecutivo();
				return $resultado;
			}  

		public function ejecutivobanco ($vendedor)
		{
			$this->ejecutivos->set("vendedor",$vendedor);
			$datos=$this->ejecutivos->ejecutivobanco();
			return $datos;
		}	

		public function verejecutivoxbanco ($vendedor)
		{
			$this->ejecutivos->set("vendedor",$vendedor);
			$datos=$this->ejecutivos->verejecutivoxbanco();
			return $datos;
		}	


		public function actualizarbanco ($banco,$vendedor)
        {
				
			$this->ejecutivos->set("banco",$banco);
			$this->ejecutivos->set("vendedor",$vendedor);
			$resultado=$this->ejecutivos->actualizarbanco();
			return $resultado;
		}

		public function AsignarBancoEjec ($banco,$vendedor,$usuario)
        {
				
			$this->ejecutivos->set("banco",$banco);
			$this->ejecutivos->set("vendedor",$vendedor);
			$this->ejecutivos->set("usuario",$usuario);
			$resultado=$this->ejecutivos->AsignarBancoEjec();
			return $resultado;
		}

		public function eliminarbanco ($banco)
    	{
			$this->ejecutivos->set("banco",$banco);
			$resultado=$this->ejecutivos->eliminarbanco();
			return $resultado;
		}

		public function eliminarbancoejecutivo ($bancoorigen,$vendedor)
    	{
			$this->ejecutivos->set("bancoorigen",$bancoorigen);
			$this->ejecutivos->set("vendedor",$vendedor);
			$resultado=$this->ejecutivos->eliminarbancoejecutivo();
			return $resultado;
		}
	}


?>
