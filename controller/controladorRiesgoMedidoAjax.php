<?php
//session_start();
include_once("../../../model/RiesgoMedido.php");

class controladorRiesgo{

	private $riesgo;

	public function __construct(){
		$this->riesgo= new RiesgoMedido();
	}


	public function sp_verpreciopos(){
		$datos=$this->riesgo->sp_verpreciopos();
		return $datos;
	}

	public function spEditarPrecios ($correlativo, $marcapos, $tipopos,$valordolar,$usuario,$bancoreg)
		{
		$this->riesgo->set("correlativo",$correlativo);
		$this->riesgo->set("marcapos",$marcapos);
		$this->riesgo->set("tipopos",$tipopos);
		$this->riesgo->set("valordolar",$valordolar);
		$this->riesgo->set("usuario",$usuario);
		$this->riesgo->set("bancoreg",$bancoreg);
		$resultado=$this->riesgo->spEditarPrecios();
		return $resultado;
		}	
		
	public function sp_guardarreciopos($idmodelopos,$usuario,$idtipopos,$montousd,$preciobanesco){
		//$this->comercializacion->set("usuario",$usuario);
		$this->riesgo->set("idmodelopos",$idmodelopos);
		$this->riesgo->set("usuario",$usuario);
		$this->riesgo->set("idtipopos",$idtipopos);
		$this->riesgo->set("montousd",$montousd);
		$this->riesgo->set("preciobanesco",$preciobanesco);
		$datos=$this->riesgo->sp_guardarreciopos();
		return $datos;

	}
	//rezar para que no explote
	public function sp_guardarnuevopos($idmodelopos,$idtipopos){
		//$this->comercializacion->set("usuario",$usuario);
		$this->riesgo->set("idmodelopos",$idmodelopos);
		//$this->riesgo->set("usuario",$usuario);
		$this->riesgo->set("idtipopos",$idtipopos);
		$datos=$this->riesgo->sp_guardarnuevopos();
		return $datos;

	}

	public function sp_marcapos(){
		  $resultado=$this->riesgo->sp_marcapos();

		return $resultado;
	}

	public function sp_buscarregion($usuario,$area_usuario){
		  $this->riesgo->set("usuario",$usuario);
		  $this->riesgo->set("area_usuario",$area_usuario);
		  $datos=$this->riesgo->sp_buscarregion();
		  return $datos;
	}

	public function sp_buscarbancoregion($valor, $regiones){
	$this->riesgo->set("valor",$valor);
	$this->riesgo->set("regiones",$regiones);
	$datos=$this->riesgo->sp_buscarbancoregion();
	return $datos;
	}
}


?>



