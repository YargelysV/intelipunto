<?php
include_once("model/AcuerdoServicio.php");

class controladorAcuerdoServicio{

	private $servicio;


	public function __construct(){
		$this->servicio= new Servicio();
	}

	public function insertar_tabltrans ($numserie, $tipopos, $usuario){
		$this->servicio->set("numserie",$numserie);
		$this->servicio->set("tipopos",$tipopos);
		$this->servicio->set("usuario",$usuario);
		$resultado=$this->servicio->insertar_tabltrans();
		return $resultado;
	}

	public function spbanco($cliente){
		$this->servicio->set("cliente",$cliente);
		$datos=$this->servicio->spbanco();
		return $datos;
	}

	public function spbuscarclienteacuerdorif($rif){
		$this->servicio->set("rif",$rif);
		$datos=$this->servicio->spbuscarclienteacuerdorif();
		return $datos;
	}
}
?>
	