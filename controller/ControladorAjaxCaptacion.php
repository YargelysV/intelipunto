<?php
include_once("../model/Captacion.php");

class ControladorCaptacionn{

	private $captacionregistro;


	public function __construct(){
		$this->captacionregistro= new Captacion();
	}

		public function sp_registroscaptacion($usuario){
		$this->captacionregistro->set("usuario",$usuario);
		$datos=$this->captacionregistro->sp_registroscaptacion();
		return $datos;
		}

		public function editarCaptacion ($operacion,$nombres,$apellidos,$documento,$afiliacion,$telefono,$razonsocial,$bancointeres,$bancoasignado,$cantidadposs,$email,$medio_contacto,$direccion,$tipocliente,$usuario,$idregistro)
    {
		    $this->captacionregistro->set("operacion",$operacion);
			$this->captacionregistro->set("nombres",$nombres);
			$this->captacionregistro->set("apellidos",$apellidos);
			$this->captacionregistro->set("documento",$documento);
			$this->captacionregistro->set("afiliacion",$afiliacion);
			$this->captacionregistro->set("telefono",$telefono);
			$this->captacionregistro->set("razonsocial",$razonsocial);
			$this->captacionregistro->set("bancointeres",$bancointeres);
			$this->captacionregistro->set("bancoasignado",$bancoasignado);
			$this->captacionregistro->set("cantidadposs",$cantidadposs);
			$this->captacionregistro->set("email",$email);
			$this->captacionregistro->set("medio_contacto",$medio_contacto);
			$this->captacionregistro->set("direccion",$direccion);
			$this->captacionregistro->set("tipocliente",$tipocliente);
			$this->captacionregistro->set("usuario",$usuario);
			$this->captacionregistro->set("idregistro",$idregistro);
			$resultado=$this->captacionregistro->editarCaptacion();
			return $resultado;
	}

			public function sp_buscarcaptacion($cliente){
			$this->captacionregistro->set("cliente",$cliente);
			$datos=$this->captacionregistro->sp_buscarcaptacion();
			return $datos;
			}

			public function sp_registrosasignacionejecutivo($ocodigobanco){
		$this->captacionregistro->set("ocodigobanco",$ocodigobanco);
		$datos=$this->captacionregistro->sp_registrosasignacionejecutivo();
		return $datos;
	}


	public function editarejecutivoasignado ($operacion,$nombrecaptacion,$apellidocaptacion,$documentocaptacion,$afiliadocaptacion,$desc_ejecutivo,$telefonocaptacion,$razonsocialcaptacion,$mcontacto,$obancointeres,$asignarbanco,$usuario,$cantidadposscap,$emailcap,$direccioncap,$tcliente,$estatuscaptacion,$idregistro)
    {
		    $this->captacionregistro->set("operacion",$operacion);
			$this->captacionregistro->set("nombrecaptacion",$nombrecaptacion);
			$this->captacionregistro->set("apellidocaptacion",$apellidocaptacion);
			$this->captacionregistro->set("documentocaptacion",$documentocaptacion);
			$this->captacionregistro->set("afiliadocaptacion",$afiliadocaptacion);
			$this->captacionregistro->set("desc_ejecutivo",$desc_ejecutivo);
			$this->captacionregistro->set("telefonocaptacion",$telefonocaptacion);
			$this->captacionregistro->set("razonsocialcaptacion",$razonsocialcaptacion);
			$this->captacionregistro->set("mcontacto",$mcontacto);
			$this->captacionregistro->set("obancointeres",$obancointeres);
			$this->captacionregistro->set("asignarbanco",$asignarbanco);
			$this->captacionregistro->set("usuario",$usuario);
			$this->captacionregistro->set("cantidadposscap",$cantidadposscap);
			$this->captacionregistro->set("emailcap",$emailcap);			
			$this->captacionregistro->set("direccioncap",$direccioncap);
			$this->captacionregistro->set("tcliente",$tcliente);
			$this->captacionregistro->set("estatuscaptacion",$estatuscaptacion);
			$this->captacionregistro->set("idregistro",$idregistro);
			$resultado=$this->captacionregistro->editarejecutivoasignado();
			return $resultado;
	}	

	public function sp_registroscaptacionxaprobar(){
		$datos=$this->captacionregistro->sp_registroscaptacionxaprobar();
		return $datos;
	}

}
?>
	
