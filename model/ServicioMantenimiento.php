<?php
include_once('Conexion.php');

class ServicioMantenimiento
{
	
	//variables
	
	private $rif;
	private $fecha;
	private $nafiliacion;
	private $nterminal;
	private $documento;
	private $clientebco;
	private $formapago;
	private $ntarjeta;
	private $codseguridad;
	private $ncuenta;
	private $fechavenc;
	private $nombre;
	private $coddocumento;
	private $bancoorigen;
	private $usuario;
	private $codtarjeta;

	private $nroafiliacion;
	private $razonsocial;
	private $numterminal;
	private $serialpos;
	private $tipopos;
	private $marcapos;
	private $codigobanco;
	private $codtipo;
	private $cliente;

	private $iafiliacion;
	private $irazonsocial;
	private $idocumento;
	private $iterminal;
	private $iserial;
	private $itipo;
	private $imarca;
	private $ibanco;
	
	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido; 
	}

	public function get ($atributo){
		return $this->$atributo; 
	}
	

	public function sp_buscarservicioRif(){
		$sql="select * from spbuscarservicioRif('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverfechamantenimiento (){
		$sql="select * from spverfechamantenimiento()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarmantenimiento(){
		$sql="select * from spbuscarmantenimiento('{$this->fecha}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function mostrarservicio(){
	$sql="select * from spmostrarserviciomantenimiento('{$this->cliente}','{$this->documento}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

		public function mostrarmantenimiento1(){
	$sql="select * from spmostrarserviciomantenimiento1('{$this->cliente}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
	
	public function sp_guardardomiciliacion(){
	$sql="select * from sp_guardardomiciliacion('{$this->cliente}','{$this->documento}','{$this->razonsocial}','{$this->codformap}','{$this->ncuenta}','{$this->codigoestatus}','{$this->actividad}','{$this->cbanco}','{$this->usuario}')";
	
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

			public function ver_carta_domiciliacion (){
		$sql="select * from sp_vercartadomiciliacion('{$this->cliente}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}


		public function crearmantenimiento()
		{

        $sql2="select * from spverificaregistro ('{$this->numterminal}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);
         echo $num;
		if($num!=0){
			return false;
		}
		else{
		$sql2="select * from spcargaindividual ('{$this->operacion}','{$this->nroafiliacion}','{$this->razonsocial}','{$this->documento}','{$this->numterminal}','{$this->serialpos}','{$this->codigobanco}','{$this->codtipo}','{$this->marcapos}','{$this->usuario}')";

		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		}


		public function editarcarga (){
		$sql2="select * from spverificaregistro ('{$this->numterminal}')";

		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);

			if($num==0){
				return $num;
			}
			else{
		$sql2="select * from spcargaindividual ('{$this->operacion}','{$this->nroafiliacion}','{$this->razonsocial}','{$this->documento}','{$this->numterminal}','{$this->serialpos}','{$this->codigobanco}','{$this->codtipo}','{$this->marcapos}','{$this->usuario}')";

		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
			}
		}
	
	   public function mostrarcargaindividual (){

		$sql="select * from spmostrarcargaindividual()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function verprefijobanco()
		{
		$sql="select * from spverprefijobanco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function vertipodepos()
		{
		$sql="select * from spvertipopos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function vermarcadepos()
		{
		$sql="select * from spvermarcapos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function insertar_carga(){
		$sql="select spinsertarcarga('{$this->iafiliacion}','{$this->irazonsocial}','{$this->idocumento}','{$this->iterminal}','{$this->iserial}','{$this->ibanco}','{$this->itipo}','{$this->imarca}','{$this->usuario}')";
		$this->con->consultaSimple($sql);
		return true;
	}

		public function verclientesindividual()
	{
		$sql="select * from spverclientesindividual()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function spbuscardomiciliacionxrif(){
		$sql="select * from spbuscardomiciliacionxrif('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function spbuscarregistrodomiciliacion(){
		$sql="select * from spbuscarregistrodomiciliacion('{$this->cliente}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spbuscarreportedomiciliacion(){
		$sql="select * from sp_buscarreportedomiciliacion('{$this->status}','{$this->modalidad}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverestatuscliente()
	{
		$sql="select * from sp_verestatuscliente()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function spagregarbancoorigen()
	{
		$sql="select * from sp_agregarbancoorigen()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function sp_verformapago(){
		$sql="select * from sp_verformapago ()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_vermodalidadpago(){
		$sql="select * from sp_vermodalidadpago ()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

}
?>