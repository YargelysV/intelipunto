<?php
include_once('Conexion.php');

	class EncuestaNueva
	{
		//variables

		private $operacion;
		private $nroafiliacion;
		private $nafiliacion;
		private $razonsocial;
		private $banco;
		private $pos;
		private $linea;
		private $cliente;
		private $fecha;
		private $namearchivo;
		private $rif;
		private $origen;

		//config pos
		//config pos
		private $serialpos;
		private $numterminal;
		private $numsim;
		private $fechaent;
		private $busqueda;
		//gestionpos

		private $fecharecpos;
		private $coditecnico;
		private $fechagespos;
		private $estatuspos;
		private $fechainspos;
        private $correlativo;
        private $fechacarg;
        private $usuariocarg;
        private $idcliente;
        private $fechaenv;
        private $fechalog;
        private $dato;
        private $idCliente;

		
		//metodos
		public function __construct()
		{
		$this->con=new Conexion();
		}

		public function set($atributo,$contenido)
		{
		$this->$atributo=$contenido;
		}

		public function get ($atributo)
		{
		return $this->$atributo;
		}

		public function spbuscarfechaencuesta()
		{
		$sql="select * from spbuscarfechaencuesta('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spverbancoencuesta()
		{
		$sql="select * from spverbancoencuesta('{$this->cliente}')";
		$resultado= $this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
		}

		public function sp_bancfechaencuesta()
		{
		$sql="select * from sp_bancfechaencuesta('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function mostrardetallencuesta()
		{
		$sql="select * from mostrardetallencuesta('{$this->idcliente}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
		public function mostrarinventarioposafiliado()
		{
		$sql="select * from spasignarserialpos1('{$this->idcliente}','{$this->busqueda}')";
		$resultado= $this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
		}
		public function verdireccionInstalacion()
		{
		$sql="select * from spverdireccionInstalacion('{$this->idcliente}')";
		$resultado= $this->con->consultaRetorno($sql);
	//	echo $sql;
		return $resultado;
		}
			public function sp_buscartipousuario()
		{
		$sql="select * from sp_buscartipousuario('{$this->usuario}')";
		$resultado= $this->con->consultaRetorno($sql);
	//	echo $sql;
		return $resultado;
		}

		public function spbuscarafiliadorifencuesta(){
		$sql="select * from spbuscarafiliadorifencuesta('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spagregarpersona (){
		$sql="select * from spagregarpersona('{$this->dato}','{$this->idCliente}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

	public function sp_registrosactivos(){
	$sql="select * from sp_registrosactivos('{$this->idcliente}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_verregistroencuesta()
		{
		$sql="select * from sp_verregistroencuesta('{$this->idcliente}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_actividadencuesta()
		{
		$sql="select * from sp_actividadencuesta('{$this->idcliente}','{$this->activo}','{$this->usuario}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_registrosactivosencuesta()
		{
		$sql="select * from sp_registrosactivosencuesta('{$this->idcliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
	
	public function sp_reloadregistro (){
		$sql="select * from sp_reloadregistroencuesta('{$this->idregistro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

	public function spbuscarpersonasreferidas()
		{
		$sql="select * from sp_buscarpersonasreferidas()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
	public function spbuscarpersonareferida()
		{
		$sql="select * from sp_buscarpersonasreferidas('{$this->idcliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_indicacontactoref(){
		$sql="select * from sp_reloadregistropersonref('{$this->id_registro}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_guardarGestionEncuesta(){
	    	$sql="select * from sp_guardarGestionEncuesta('{$this->idcliente}','{$this->rlegal}','{$this->pcontacto}','{$this->usuario}','{$this->estatusgestion}', '{$this->observaciones}')";
	    	//echo $sql;
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function spverestatusencuesta(){
	    	$sql="select * from spverestatusencuesta('{$this->idcliente}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_verpcontacto(){
	    	$sql="select * from sp_verpcontacto('{$this->id_cliente}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_guardarEncuesta1(){
	    	$sql="select * from sp_guardarEncuesta1('{$this->pregunta}','{$this->respuesta}','{$this->id_cliente}','{$this->id_banco}','{$this->usuario}','{$this->fechaencuesta}','{$this->upcontacto}','{$this->rlegal}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_guardarEncuesta2(){
	    	$sql="select * from sp_guardarEncuesta2('{$this->pregunta}','{$this->respuesta}','{$this->idcliente}','{$this->idbanco}','{$this->usuario}','{$this->fechaencuesta}','{$this->upcontacto}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_guardarEncuesta3(){
	    	$sql="select * from sp_guardarEncuesta3('{$this->pregunta}','{$this->respuesta}','{$this->respuesta_2}','{$this->idcliente}','{$this->idbanco}','{$this->usuario}','{$this->fechaencuesta}','{$this->upcontacto}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_guardarEncuesta4(){
	    	$sql="select * from sp_guardarEncuesta4('{$this->pregunta}','{$this->respuesta}','{$this->idcliente}','{$this->id_banco}','{$this->usuario}','{$this->fechaencuesta}','{$this->upcontacto}','{$this->nombreref}','{$this->apellidoref}','{$this->tlfref}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_eliminarregistro(){
	    	$sql="select * from sp_eliminarregistro('{$this->pregunta}','{$this->idcliente}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_cambiaestatusencuesta(){
	    	$sql="select * from sp_cambiaestatusencuesta('{$this->idcliente}')";
	    	//echo $sql;
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_estatusactual(){
	    	$sql="select * from sp_estatusactual('{$this->id_cliente}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_registrohistorial(){
	    	$sql="select * from sp_registrohistorial('{$this->id_cliente}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_cantidaderegistros(){
	    	$sql="select * from sp_cantidaderegistros('{$this->id_cliente}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function sp_guardarpersonaref(){
	    	$sql="select sp_guardarpersonaref('{$this->idcliente}','{$this->usuario}','{$this->nombreref}','{$this->apellidoref}','{$this->telefonoref}','{$this->banco}')";
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	    public function spverReferidosxcliente()
		{
		$sql="select * from spverReferidosxcliente('{$this->idcliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function EditarReferidos (){
			$sql="select * from spReferidos ('{$this->operacion}','{$this->idregistro}','{$this->nombreref}','{$this->apellidoref}','{$this->telefonoref}')";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function EliminarReferidos(){
			$sql="select * from speliminar_referidos ('{$this->opcion}','{$this->id_consecutivo}',null,null,null)";
			echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}
	}
?>	
