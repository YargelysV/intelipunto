<?php
include_once('Conexion.php');

	class GestionMantenimiento
	{

		//variables
		private $operacion;
		private $correlativo;
		private $marcapos;
		private $codtipo;
		private $valordolar;
		private $factorconversion;
		private $valorbs;
		private $fechaact;
		private $usuario;
		private $estados;
		private $municipios;
		private $parroquias;
		private $tipozona;
		private $preciozona;
		private $id;
		private $tipopos;
		private $perfil;
		private $cod_perfil;
		private $desc_perfil;
		private $fecha;
		private $secuencial;

		//metodos
		public function __construct(){
			$this->con=new Conexion();
		}

		public function set($atributo,$contenido){
			$this->$atributo=$contenido;
		}

		public function get ($atributo){
			return $this->$atributo;
		}

		public function mostrargestion (){

		$sql="select * from spmostrar_gestion()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function mostrarzona (){

		$sql="select * from spmostrar_zona()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function mostrartipopos (){

		$sql="select * from sptipopos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function crear()
	{

		$sql2="select * from sp_gestion ('{$this->operacion}','0','{$this->marcapos}','{$this->codtipo}','{$this->valordolar}','{$this->factorconversion}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql2);
		//echo $sql2;
		return $resultado;
		}

		public function crearzona()
	 	{
		$sql2="select * from sp_gestionzona ('{$this->operacion}','{$this->estados}','{$this->municipios}','{$this->parroquias}','{$this->tipozona}','{$this->usuario}','{$this->cod_perfil}')";
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function EditarGestion (){


		$sql2="select * from sp_gestion ('{$this->operacion}','{$this->correlativo}','{$this->marcapos}','{$this->codtipo}','{$this->valordolar}','{$this->factorconversion}','{$this->usuario}')";
			$resultado=$this->con->consultaRetorno($sql2);
			//echo $sql2;
		return $resultado;
		}

			public function EditarZona (){

		$sql2="select * from sp_gestionzona ('{$this->operacion}','{$this->estados}','{$this->municipios}','{$this->parroquias}','{$this->tipozona}','{$this->cod_perfil}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}


		public function sp_zona(){
		$sql="select * from sp_zona('{$this->perfil}')";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_bancos(){
		$sql="select * from sp_bancos('{$this->perfil}')";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_preciozona(){
		$sql="select * from sp_preciozona()";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function crearrperfil(){
		$sql="select * from sp_crearrperfil ('{$this->desc_perfil}')";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_zonas(){
		$sql="select * from sp_zonas('{$this->tipozona}','{$this->preciozona}','{$this->cod_perfil}')";
	    $resultado=$this->con->consultaRetorno($sql);
	   return $resultado;
	}

		public function spbuscarperfil(){
		$sql="select * from spbuscarperfil()";
	    $resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spbuscartipozona(){
		$sql="select * from spvertipodezona()";
	    $resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarvalorpos(){
		$sql="select * from sp_buscarvalorpos('{$this->marcapos}','{$this->tipopos}')";
	    $resultado=$this->con->consultaRetorno($sql);
	    //echo $sql;
		return $resultado;
	}

		public function sp_detallevalorpos(){
		$sql="select * from sp_detallevalorpos('{$this->marcapos}','{$this->tipopos}','{$this->fecha}','{$this->secuencial}')";
	    $resultado=$this->con->consultaRetorno($sql);
	    //echo $sql;
		return $resultado;
	}


		public function eliminarbancos(){
		$sql="select * from sp_eliminarbancos ('{$this->codigobanco}')";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function sp_agregarbancos(){
		$sql="select * from sp_agregarbancos ('{$this->cod_perfil}','{$this->codigobanco}')";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spagregarbancos(){
		$sql="select * from spagregarbancos()";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function GestionarZonas()
	 	{
		$sql2="select * from sp_gestionzona ('{$this->idzona}','{$this->idparroq}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

	  }

?>
