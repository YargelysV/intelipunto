<?php
include_once('Conexion.php');

class Activacion
{

	//variables
	private $clientes;
	private $idcliente;
	private $idregistro;
	private $estatus;
	private $serialpos;
	private $serialmifi;
	private $usuario;
	private $id;
	private $serialsimcard;
	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function sp_buscaractivacion()
		{
		$sql="select * from sp_buscaractivacion('{$this->clientes}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_gestionaractivacion()
		{
		$sql="select * from sp_gestionaractivacion('{$this->idcliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_guaradaractivacion()
		{
		$sql="select * from sp_guardaractivacion('{$this->iidregistro}','{$this->codigomot}','{$this->iserialpos}','{$this->iserialsimcard}','{$this->iserialmifi}','{$this->iusuariopercance}','{$this->consecutivo}','{$this->idgestion}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_gestionaractivacionserial()
		{
		$sql="select * from sp_gestionaractivacionserial('{$this->nroserial}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
}

?>