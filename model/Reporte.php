<?php
include_once('Conexion.php');

class Reporte
{

	//variables
	private $usuario;
	private $tipodoc;
	private $coddocumento;
	private $namearchivo;
	private $documento;

	private $login;
	private $clave;
	private $nombres;
	private $apellidos;
	private $fechanac;
	private $correo;
	private $codtipousuario;
	private $activo;
	private $cargo;
	private $area;
	private $descmotivo;
	private $producto;

	private $numserie;
	private $nclase;
    private $codclase;
    private $cliente;

	
	private $Operacion;

	private $d_dirseleccionada;
	private $item;
	private	$e_prefijo;
	private	$e_fechacarga;

	private $id;
    private $id_prefijobanco;
	private $id_fechacarga;
	private $txthorario;
	private $campodelete;

	private $fechaenvio;
	private $fecharecepcion;
	private $interes;
	private $tipotarjeta;
	private $codmarcapos;
    private $id_estado;
    private $id_municipio;
    private $id_parroquia;

    private $lun;
    private $mar;
    private $mie;
    private $jue;
    private $vie;
    private $sab;
    private $dom;
    private $txtlunvie;
    private $txtsabdom;
    private $acteconomica;
    private $direccioninstalacion;
    private $cantposaprobados;
    private $nreferencia;
    private $bancoorigen;
    private $fechapago;
    private $horapago;
    private $formapago;
    private $fechafactura;
    private $nfactura;
    private $costopos;
    private $ejebanco;
    private $TipoPOS;
    private $ejeinteligensa;
    private	$codtipovendedor;

    private $origen;
    private $fechahasta;
    private $fechadesde;
    private $marca;
    private $tipo;
    private $fecha;
    private $fechainicial;
    private $fechafinal;
    private $estatus;
    private $ejecutivo;
    private $sitio;
    private $region;
    private $estatuscomision;
    private $fechacomisiones;
    private $observaciones;
    private $idcliente;
    private $nrif;
	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function speliminar_tablatransclientep(){
		$sql="delete from clie_tbltransferencia";
		$this->con->consultaSimple($sql);
		return true;
	}
	public function spverclientes(){
		$sql="select * from spverclientes()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
		public function sp_estatussolpos(){
		$sql="select * from sp_estatussolpos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_comercializacionbuscar(){
		$sql="select * from sp_comercializacionbuscar('{$this->Operacion}','{$this->cliente}','{$this->FechaRecepcionArchivo}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_tipocuenta(){
		$sql="select * from sp_tipocuenta()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_verestado(){
		$sql="select * from sp_verestado()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_ciudad(){
		$sql="select * from sp_ciudad('{$this->id_estado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_municipio(){
		$sql="select * from sp_municipio('{$this->id_estado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_municipioselected(){
		$sql="select * from sp_municipioselected('{$this->id_municipio}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_parroquia(){
		$sql="select * from sp_parroquias('{$this->id_municipio}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_parroquiaselected(){
		$sql="select * from sp_parroquiaselected('{$this->id_parroquia}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_estatusinteresado(){
		$sql="select * from sp_estatusinteresado()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tipolinea(){
		$sql="select * from sp_tipolinea('{$this->TipoPOS}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_proveedorsimcard(){
		$sql="select * from sp_proveedorsincard('{$this->tipolinea}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tipotarjeta(){
		$sql="select * from sp_tipotarjeta()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tipotarjetaselected(){
		$sql="select * from sp_tipotarjetaselected('{$this->tipotarjeta}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_marcapos(){
		$sql="select * from sp_marcapos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_costopos(){
		$sql="select * from sp_costopos('{$this->TipoPOS}','{$this->codmarcapos}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_listbanco(){
		$sql="select * from sp_listbanco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_dirinstalacioneditada(){
		$sql="select * from sp_dirinstalacioneditada('{$this->coddocumento}','{$this->PrefijoBanco}','{$this->namearchivo}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarclientecomercializacionRif(){
	$sql="select * from spbuscarclienterif('{$this->Rif}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verclientecomercializacionbuscar()
	{
	$sql="select * from spverclientepotencialbuscar('{$this->PrefijoBanco}','{$this->FechaRecepcionArchivo}','{$this->namearchivo}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_vistadireccioninstalacioneditada(){
		$sql="select * from sp_vistadireccioninstalacioneditada('{$this->coddocumento}','{$this->e_prefijo}','{$this->namearchivo}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function ProcesarInformacionComercializacion(){
		$sql="select * from spprocesarinformacioncomercializacion('{$this->Operacion}','{$this->cliente}','{$this->usuario}')";
		// echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spbanco(){
		$sql="select * from sp_banco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function comercializacionreporte(){
		$sql="select * from spcomercializacionreporte('{$this->cliente}','{$this->FechaRecepcionArchivo}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function comercializacionreportedetallado()
	{
		if (($this->fechaenvio=="") && ($this->fecharecepcion==""))
		{
		$sql="select * from spcomercializacionreportedetallado('{$this->PrefijoBanco}',NULL,NULL)";
		}
		if (($this->fechaenvio!="") && ($this->fecharecepcion==""))
		{
		$sql="select * from spcomercializacionreportedetallado('{$this->PrefijoBanco}','{$this->fechaenvio}',null)";
		}
		if (($this->fechaenvio!="") && ($this->fecharecepcion!=""))
		{
		$sql="select * from spcomercializacionreportedetallado('{$this->PrefijoBanco}','{$this->fechaenvio}','{$this->fecharecepcion}')";
		}
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function insertar_afiliado()
	{
		$sql="select * from spinsertarafiliado('{$this->Consecutivo}','{$this->registro}','{$this->fechaenvio}','{$this->cliente}','{$this->PrefijoBanco}','{$this->TipoPOS}','{$this->cantpv}','{$this->tipolinea}','{$this->tipomodelonegocio}','{$this->tipcliente}','{$this->razonsocial}','{$this->Rif}','{$this->NombreAfiliado}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function verclientescomercializacion()
	{
		$sql="select * from spverclientescomercializacion()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function verafiliadosasignados()
	{
		$sql="select * from spverafiliadosasignados()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function verafiliadosasignadosdetalle()
	{
		$sql="select * from spverafiliadosasignados() where afiliado='{$this->NombreAfiliado}'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

			public function sp_buscarreporte(){
		$sql="select * from sp_buscarreporte('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteadm(){
		$sql="select * from sp_reporteadm('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarreporteerp(){
		$sql="select * from sp_buscarreporteerp('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	

		public function sp_reporteerp()
		{
		$sql="select * from sp_reporteerp('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarreportepos(){
		$sql="select * from sp_buscarreportepos('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarreporteequipos(){
		$sql="select * from sp_buscarreporteequipos('{$this->clientes}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarreportefecha(){
		$sql="select * from sp_buscarreportefecha('{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteequipos()
		{
		$sql="select * from sp_reporteequipos('{$this->clientes}','{$this->fecha}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteequipospos()
		{
		$sql="select * from sp_reporteequipospos('{$this->clientes}','{$this->fecha}','{$this->marca}','{$this->tipo}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function sp_reportexdiasdetalles()
		{
		$sql="select * from sp_reportexdiasdetalles('{$this->banco}','{$this->usuarioeje}','{$this->tipoposdetalles}','{$this->fechainicio}','{$this->fechafinal}','{$this->tiporeporte}','{$this->usuario}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportepos()
		{
		$sql="select * from sp_reportepos('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarreporteejecutivo(){
		$sql="select * from sp_buscarreporteejecutivo('{$this->clientes}','{$this->ejecutivo}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteejecutivo()
		{
		$sql="select * from sp_reporteejecutivo('{$this->cliente}','{$this->ejecutivo}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteejecutivogestiones()
		{
		$sql="select * from sp_reporteejecutivogestiones('{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_soloreporte()
		{
		$sql="select * from sp_soloreporte('{$this->cliente}','{$this->ejecutivo}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_ejecutivos()
		{
		$sql="select * from sp_ejecutivos('{$this->clientes}','{$this->estatus}','{$this->documento}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spverclienteejecutivo()
		{
		$sql="select * from spverclienteejecutivo('{$this->coddocumento}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportediarioxbanco()
		{
		$sql="select * from sp_reportediarioxbanco('{$this->clientes}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function sp_mostrarlogin()
		{
		$sql="select * from sp_mostrarlogin()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportexdias()
		{
		$sql="select * from sp_reportexdias('{$this->clientes}','{$this->ejecutivo}','{$this->fechainicial}','{$this->fechafinal}','{$this->tiporeporte}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_ejecutivoxbanco()
		{
		$sql="select * from sp_ejecutivoxbanco('{$this->banco}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_reportediariogeneraldemifi()
		{
		$sql="select * from sp_reportediariogeneraldemifi('{$this->banco}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_reportegestionesdemifi()
		{
		$sql="select * from sp_reportegestionesdemifi('{$this->banco}','{$this->sitio}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscargerente()
		{
		$sql="select * from sp_buscargerente()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportegrupal()
		{
		$sql="select * from sp_reportegrupal('{$this->gerente}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
		
			public function sp_reportediarioestatus()
		{
		$sql="select * from sp_reportediarioestatus('{$this->banco}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportegrupaldetalles()
		{
		$sql="select * from sp_reportegrupaldetalles('{$this->banco}','{$this->tipopos}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportexpos()
		{
		$sql="select * from sp_reportexpos('{$this->POS}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_busquedaxMes()
		{
		$sql="select * from sp_busquedaxMes()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_detallesreportexpos()
		{
		$sql="select * from sp_detallesreportexpos('{$this->detallemarcapos}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportefacturatotal()
		{
		$sql="select * from sp_facturatotal()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		} 

		public function sp_reportexnumfactura()
		{
		$sql="select * from sp_busquedaxfactura('{$this->factura}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		} 

			public function sp_reportediarioestatusgeneral()
		{
		$sql="select * from sp_reportediarioestatusgeneral('{$this->banco}','{$this->region}','{$this->modalidad}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_reportedomiciliaciongeneral()
		{
		$sql="select * from sp_reportediariodomiciliacion('{$this->banco}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_reportediarioequiposvendidos()
		{
		$sql="select * from sp_reportediarioequiposvendidos('{$this->banco}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_reportediarioequiposinstalados()
		{
		$sql="select * from sp_reportediarioequiposinstalados('{$this->banco}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


			public function sp_reportediarioresultadogestion()
		{
		$sql="select * from sp_reportediarioresultadogestion('{$this->region}','{$this->banco}','{$this->sitio}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteparametrizacion()
		{
		$sql="select * from sp_reporteparametrizacion('{$this->banco}','{$this->estatus}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
        public function sp_reportecaptfiltroejec()
		{
		$sql="select * from sp_reportecaptfiltroejec()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
		public function sp_reportecaptacionejec()
		{
		$sql="select * from sp_reportecaptacionejec('{$this->clientes}','{$this->ejecutivo}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		
		public function sp_reportediarioresultadogestionrosal()
		{
		$sql="select * from sp_reportediarioresultadogestionrosal('{$this->banco}','{$this->sitio}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
		
			public function sp_verbancos()
		{
		$sql="select * from spverbancos('{$this->default}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportedomiciliacion()
		{
		$sql="select * from sp_reportedomiciliacion('{$this->cliente}','{$this->estatus}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_reportediarioestatussustitucion()
		{
		$sql="select * from sp_reportediarioestatussustitucion('{$this->banco}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportedetalladosustitucion()
		{
		$sql="select * from sp_reportedetalladosustitucion('{$this->cliente}','{$this->secuencial}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_preguntasencuestaa()
		{
		$sql="select * from sp_preguntasencuestaa('{$this->clientes}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_preguntasencuestab()
		{
		$sql="select * from sp_preguntasencuestab('{$this->clientes}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_preguntasencuestac()
		{
		$sql="select * from sp_preguntasencuestac('{$this->clientes}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_detallepregunta()
		{
		$sql="select * from sp_detallepregunta()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarestadoencue()
		{
		$sql="select * from sp_buscarestadoencue('{$this->clientes}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarregistrosencuesta()
		{
		$sql="select * from sp_buscarregistrosencuesta('{$this->clientes}','{$this->fechainicial}','{$this->fechafinal}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spbuscarrifreporte()
		{
		$sql="select * from spbuscarrifreporte('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
		public function sp_buscarestadoencueTD()
		{
		$sql="select * from sp_buscarestadoencuetd('{$this->cliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarregistrosencuestacliente()
		{
		$sql="select * from sp_buscarregistrosencuestacliente('{$this->clientes}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_verhistorialllamadas()
		{
		$sql="select * from sp_verhistorialllamadas('{$this->idcliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spEditarComisiones()
		{
		$sql="select * from EditarComisiones('{$this->id}','{$this->confcomisiones}','{$this->fechacomisiones}','{$this->usuario}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_mostrarestatuscomision()
		{
		$sql="select * from sp_estatuscomision()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarvendedoresmensual()
		{
		$sql="select * from spreportevendedormensual('{$this->fechainicial}','{$this->fechafinal}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
		
		public function spEditarReferidos()
		{
		$sql="select * from spEditarReferidos('{$this->id_consecutivo}','{$this->nombre_ref}','{$this->apellido_ref}','{$this->telefono_ref}','{$this->usuario}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarclientespersonasreferidas()
		{
		$sql="select * from sp_buscarclientespersonasreferidas('{$this->clientes}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscardetallespersonasreferidas()
		{
		$sql="select * from sp_buscardetallespersonasreferidas('{$this->clientes}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarrifpersonasreferidas()
		{
		$sql="select * from sp_buscarrifpersonasreferidas('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}			

		public function spmostrarcomisiones()
		{
		$sql="select * from spmostrarcomisiones('{$this->idcliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarcomisiones()
		{
		$sql="select * from sp_guardarcomisiones('{$this->idcliente}','{$this->usuario}',
		'{$this->estatuscomision}','{$this->fechacomisiones}','{$this->observaciones}','{$this->vendedor}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spmostrarcomisionesestatus()
		{
		$sql="select * from spmostrarcomisionesestatus('{$this->idcliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportediarioestatusgestiones()
		{
		$sql="select * from sp_reportediarioestatusgestiones('{$this->banco}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportediarioestatusgestionesrif()
		{
		$sql="select * from sp_reportediarioestatusgestionesrif('{$this->nrif}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportedetalladoestatusgeneral()
		{
		$sql="select * from sp_reportedetalladoestatusgeneral('{$this->cliente}','{$this->secuencial}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteclientesprofit()
		{
		$sql="select * from sp_reporteclientesprofit('{$this->clientes}','{$this->fechainicial}','{$this->fechafinal}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteplantillaprofit()
		{
		$sql="select * from sp_reporteplantillaprofit('{$this->clientes}','{$this->fechainicial}','{$this->fechafinal}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reportediarioequipospercances()
		{
		$sql="select * from sp_reportediarioequipospercances('{$this->banco}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_reporteestatusfacturacion()
		{
		$sql="select * from spverclientefacturacionreporte('{$this->banco}','{$this->estatus}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

}

?>
