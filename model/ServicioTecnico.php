<?php
include_once('Conexion.php');

	class Serviciotec
	{
		//variables

		private $operacion;
		private $nroafiliacion;
		private $nafiliacion;
		private $razonsocial;
		private $banco;
		private $pos;
		private $linea;
		private $cliente;
		private $fecha;
		private $namearchivo;
		private $rif;
		private $origen;

		//config pos
		private $serialpos;
		private $numterminal;
		private $numsim;
		private $fechaent;
		private $busqueda;
		//gestionpos

		private $fecharecpos;
		private $coditecnico;
		private $fechagespos;
		private $estatuspos;
		private $fechainspos;
        private $correlativo;
        private $fechacarg;
        private $usuariocarg;
        private $idcliente;
        private $fechaenv;
        private $fechalog;
        private $serialmifi;
        private $selectperfil;
		private $selectversion;

		
		//metodos
		public function __construct()
		{
		$this->con=new Conexion();
		}

		public function set($atributo,$contenido)
		{
		$this->$atributo=$contenido;
		}

		public function get ($atributo)
		{
		return $this->$atributo;
		}

		public function mostrarserviciotecnico ()
		{
		$sql="select * from spmostrarserviciotec()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function verestatustecnico(){
		$sql="select * from sp_verestatusgestion('{$this->nroafiliacion}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


		public function spdatosserviciotec()
		{
		$sql="select * from spdatoservitec('{$this->nroafiliacion}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

    	public function buscarservicio()
    	{
		$sql="select * from spbuscarservitec('{$this->busqueda}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function verestatuspos()
		{
		$sql="select * from spestatusinspos()";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spagregardatosins ()
		{
		$sql="select * from spagregardatosins ('{$this->serial}','{$this->namearchivo}','{$this->fecharecpos}','{$this->coditecnico}','{$this->fechagespos}','{$this->estatuspos}','{$this->fechainspos}','{$this->usuariocarg}','{$this->nroafiliacion}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;

		}

	public function verdatosinstapos()
		{
		$sql="select * from spverdatosinstapos('{$this->nroafiliacion}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function serial()
		{
		$sql="select * from spserial()";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function mostrardetalleafiliado()
		{
		$sql="select * from spmostrardetalleafiliado('{$this->idcliente}','{$this->serial}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function mostrardetalleafiliadodemifi()
		{
		$sql="select * from spmostrardetalleafiliadodemifi('{$this->idcliente}','{$this->serialmifi}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function mostrardetalleafiliado1()
		{
		$sql="select * from spmostrardetalleafiliado('{$this->idcliente}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
/*spmostrarinventarioposafiliado anterior*/
		public function mostrarinventarioposafiliado()
		{
		$sql="select * from spasignarserialpos1('{$this->idcliente}','{$this->busqueda}')";
		$resultado= $this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
		}

		public function verdireccionInstalacion()
		{
		$sql="select * from spverdireccionInstalacion('{$this->idcliente}')";
		$resultado= $this->con->consultaRetorno($sql);
	//	echo $sql;
		return $resultado;
		}

		public function sp_buscartipousuario()
		{
		$sql="select * from sp_buscartipousuario('{$this->usuario}')";
		$resultado= $this->con->consultaRetorno($sql);
	//	echo $sql;
		return $resultado;
		}


		public function mostrartecnico()
		{
		$sql="select * from spmostrar_tecnicos()";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function spvertecnicos(){
		$sql="select * from spvertecnicos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function spestatusinspos(){
		$sql="select * from spestatusinspos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
		

		public function verclienteserviciotecnico()
		{
		$sql="select * from spverclienteserviciotecnico('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarafiliadoRif(){
		$sql="select * from spbuscarafiliadoRif('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarafiliadoRifgestion(){
		$sql="select * from sp_buscarafiliadoRifgestion('{$this->rif}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarclientemifixrif(){
		$sql="select * from sp_buscarclientemifixrif('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
		public function sp_verclientedetalladomifi()
		{
		$sql="select * from sp_verclientedetalladomifi('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_verclienteafiliado()
		{
		$sql="select * from sp_verclienteafiliado('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_verclientegestion()
		{
		$sql="select * from sp_verclientegestion('{$this->cliente}','{$this->fecha}','{$this->namearchivo}','{$this->region}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_verclienteseriales()
		{
		$sql="select * from sp_verclienteseriales('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function sp_buscarclienteSTRif(){
		$sql="select * from spbuscarclienteSTRif('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spbuscarclienteRifDetalleST()
	{
	$sql="select * from spbuscarclienteRifDetalleST('{$this->rif}','{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarservitec(){
	$sql="select * from sp_buscarservitec('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarxfechamifi(){
	$sql="select * from sp_buscarfechamifi('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscargestionservicio(){
	$sql="select * from sp_buscargestionservicio('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarClienteInstalar(){
	$sql="select * from sp_buscarClienteInstalar('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarclienteserviciotecnico(){
	$sql="select * from spbuscarclienteserviciotecnico('{$this->cliente}','{$this->FechaRecepcionArchivo}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrarserialafiliado()
	{
	$sql="select * from spmostrarserialafiliado('{$this->idcliente}','{$this->idsecuencial}')";
	//echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrarserialafiliadomifi()
	{
	$sql="select * from spmostrarserialafiliadomifi2('{$this->idcliente}')";
	//echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}	

	public function spactualizargestionpos()
	{
	$sql="select * from spactualizargestionpos('{$this->operacion}','{$this->serialpos}','{$this->nroafiliacion}','{$this->namearchivo}','{$this->usuario}','{$this->fecharecepalma}','{$this->tecnico}','{$this->fechagest}','{$this->estatus}','{$this->fechainst}','{$this->fechacarg}','{$this->observacion}','{$this->idcliente}','{$this->fechalog}','{$this->fechaenv}')";
	//echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

		public function spagregargestionmifi()
	{
	$sql="select * from spagregargestionmifi('{$this->operacion}','{$this->serialmifi}','{$this->namearchivo}','{$this->usuario}','{$this->fecharecepalma}','{$this->tecnico}','{$this->fechagest}','{$this->estatus}','{$this->fechainst}','{$this->fechacarg}','{$this->observacion}','{$this->idcliente}')";
	echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

		public function mostrarserialafiliadoinstalado()
	{
	$sql="select * from spmostrarserialafiliado('{$this->idcliente}','{$this->idsecuencial}')  where estatus='4' or estatus='5' or estatus='24'  or estatus='25' or estatus='28' or estatus='29' or estatus='30' or estatus='31' or estatus='32'  or  estatus='36' or  estatus='37' or  estatus='38' or  estatus='58' or  estatus='61'";
	//echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

// 
public function sp_reporteriesgomedidoadpins (){
	$sql="select * from sp_reporteriesgomedidoadpins ('{$this->idcliente}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
 }

	public function mostrarserialafiliadoinstalado2()
	{
	$sql="select * from spmostrarserialafiliado2('{$this->idcliente}','{$this->idsecuencial}','{$this->serialpos}')  where estatus='4' or estatus='5' or estatus='24'  or estatus='25' or estatus='28' or estatus='29' or estatus='30' or estatus='31' or estatus='32'  or  estatus='36' or  estatus='37' or  estatus='38' or  estatus='58' or estatus='61'";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrarserialafiliado1()
	{
	$sql="select * from spmostrarserialafiliado1('{$this->serialpos}','{$this->nroafiliacion}','{$this->namearchivo}')";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function sp_guardarnafiliadoservicio(){
	$sql="select * from sp_guardarnafiliadoservicio('{$this->idcliente}','{$this->nafiliacion}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

		public function mostrarserialafiliadogestionmifi()
	{
	$sql="select * from spmostrarserialafiliadomifi2('{$this->idcliente}','{$this->idsecuencial}')";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}


		public function mostrarserialafiliadoinstaladomifi()
	{
	$sql="select * from spmostrarserialafiliadomifi2('{$this->idcliente}','{$this->idsecuencial}') where estatus='4' or estatus='5' or estatus='6' or estatus='3'";
	//echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function mostrarserialafiliadoinstaladomifi2()
	{
	$sql="select * from spmostrarserialmifi_instalado('{$this->idcliente}','{$this->idsecuencial}','{$this->serialmifi}')  where estatus='4' or estatus='5' or  estatus='6' or estatus='3'";
	//echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_verclientegestiondelrosal()
		{
	$sql="select * from sp_verclientegestionderosal('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_buscargestionservicioderosal(){
	$sql="select * from sp_buscargestionserviciorosal('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function sp_buscarafiliadoRifgestionrosal(){
		$sql="select * from sp_buscarafiliadoRifgestionrosal('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function mostrarserialafiliadoinstalado3()
	{
	$sql="select * from spmostrarserialafiliado2('{$this->idcliente}','{$this->idsecuencial}','{$this->serialpos}')  where estatus='4' or estatus='5' or estatus='29' or estatus='30' or estatus='31' or estatus='32'  or  estatus='36' or  estatus='37' or  estatus='38' or estatus='43' or estatus='45' or estatus='46'";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrarserialafiliadoinstalado03()
	{
	$sql="select * from spmostrarserialafiliado('{$this->idcliente}','{$this->idsecuencial}')  where estatus='4' or estatus='5'  or estatus='29' or estatus='30' or estatus='31' or estatus='32'  or  estatus='36' or  estatus='37' or  estatus='38' or estatus='43' or estatus='45' or estatus='46'";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}
	public function selecttecnicos(){
		$sql="select codtecnicos as tecnico, nombres||' '||apellidos as ntecnico  from  tbltecnicos where estatus='1'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	
	public function selectestatuspos(){
		$sql="select * from spestatusservices ('{$this->codestatus}','{$this->idestgestion}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_mostrartecnico(){
		$sql="select * from sp_mostrartecnico('{$this->idcliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spmostrarserialmifi2()
		{
		$sql="select * from spmostrarserialmifi2('{$this->idcliente}','{$this->idsecuencial}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spmostrarserialmifi()
		{
		$sql="select * from spmostrarserialmifi2('{$this->idcliente}','{$this->idsecuencial}','{$this->serialmifi}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function selectversionaplicativo(){
		$sql="select * from selectversionaplicativo ({$this->idperfil})";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selectperfilaplicativo(){
		$sql="select * from selectperfilaplicativo ('{$this->idswitch}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selectestatusposi(){
		$sql="select * from spestatusservices ('{$this->codestatus}','{$this->idestgestion}','{$this->idcliente}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_consultariesgomedido()
	{
		$sql="select * from sp_riesgomedidost('{$this->banco}','{$this->cliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

}
?>	
