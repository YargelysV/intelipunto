<?php
include_once('Conexion.php');

class ControlAcceso
{

	//variables
	private $mes;
	private $empleado;
	private $fechass;
	private $fechas;
	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function sp_buscaridempleados()
		{
		$sql="select * from sp_buscaridempleados()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_buscaraccesos()
		{
		$sql="select * from sp_buscaraccesos('{$this->meses}','{$this->anio}','{$this->empleado}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_asignarferiados()
		{
		$sql="select * from spasignarferiados('{$this->fechass}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_eliminarferiados()
		{
		$sql="select * from sp_eliminarferiados('{$this->fechas}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_mostrarferiados()
		{
		$sql="select * from sp_mostrarferiados()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function verarchivoacceso()
		{
		$sql="select verarchivoacceso('{$this->name_ar}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
		
	public function sp_insertar_tblacceso(){
		$sql="select sp_insertar_tblacceso('{$this->tiempo_ar}','{$this->idusuario_ar}','{$this->nombre_ar}','{$this->apellido_ar}','{$this->numerotarjeta_ar}','{$this->dispositivo_ar}','{$this->puntoevento_ar}','{$this->verificacion_ar}','{$this->estado_ar}','{$this->evento_ar}','{$this->name_ar}')";
		//echo $sql;

		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverarchivoscontrolacceso()
		{
		$sql="select * from spverarchivoscontrolacceso()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function speliminararchivoaccesos()
		{
		$sql="select * from speliminararchivoaccesos('{$this->nombrearchivos}','{$this->login}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spverfechaaccesocargado()
		{
		$sql="select * from spverfechaaccesocargado()";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

}

?>