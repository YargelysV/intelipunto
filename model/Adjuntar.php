<?php
include_once('Conexion.php');

class Adjuntar
{
	
	//variables
	private $usuario;
	private $tipodoc;
	private $coddocumento;
	private $login;
	private $clave;
	private $nombres;
	private $apellidos;
	private $fechanac;
	private $correo;
	private $codtipousuario;
	private $activo;
	private $cargo;
	private $area;
	private $descmotivo;
	private $producto;
	private $origen;
	private $marcapos;

	private $iproveedor;
	private $ifechacompra;
	private $imarcapos;
	private $numserie;
	private $tipopos;
	private $iactivofijo;
	private $ibancoasignado;
	private $fecha;
	private $namearchivo;
	private $costopos;
	private $fechafacturacion;
	private $nfactura;
	private $codtarjeta;
	private $nameformapago;
	private $ntransferencia;
	private $bancoorigen;
	private $horapago;
	private $perfil;

	private $cliente;
	private	$TipoCliente;
	private $CustomerName; 
	private $ShortName; 
	private $StatementName; 
	private $CustomerClass; 
	private $LookupButton2; 
	private $AddressCode; 
	private $ContactPerson; 
	private $Address1; 
	private $Address2; 
	private $Address3; 
	private $City; 
	private $State; 
	private $Zip; 
	private $CountryCode; 
	private $Country; 
	private $Phone1; 
	private $Phone2; 
	private $Phone3; 
	private $Fax; 
	private $UPSZone; 
	private $ShippingMethod; 
	private $TaxScheduleID; 
	private	$ShipCompletDocument; 
	private $PrimaryShiptoAddressCode; 
	private $SalespersonID; 
	private $LookupButton6; 
	private $UserDefined1; 
	private $UserDefined2; 
	private $Comment1; 
	private $Comment2; 
	private $CustomerDiscount; 
	private $PaymentTermsID; 
	private $DiscountGracePeriod; 
	private $DueDateGracePeriod; 
	private $PriceLevel; 
	private $AccountsButton; 
	private $OptionsButton; 
	private $TaxRegistrationNumber; 
	private $CurrencyID; 
	private $EmailStatementsToAddress; 
	private $nombre;
	private $masiva;
	private $operacion;
	private $id_registro;
	private $id_afiliado;
	private $referencia;
	private $tipousuario;
	private $ejecutivo;
	private $codestatus;
	private $coordinador;
	private $serial;
	private $afiliado;
	private $archivo;
	private $valor;
	


	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido; 
	}

	public function get ($atributo){
		return $this->$atributo; 
	}
	
	public function spdatosusuario(){
		$sql="select * from spdatosusuario('{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverificar_responsable(){
		$sql="select * from spverificar_responsable('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function mostrarmotivos(){
		$sql="select * from spmostrarmotivos('{$this->usuario}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function ver(){
		$sql="select * from spUsuarioS('1','{$this->login}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function insertar_tabltrans(){
		$sql="select spadjuntarserie('{$this->iproveedor}','{$this->ifechacompra}','{$this->imarcapos}','{$this->numserie}','{$this->tipopos}','{$this->iactivofijo}','{$this->ibancoasignado}','{$this->usuario}','{$this->idlotes}')";
		//echo $sql;
		$this->con->consultaSimple($sql);
		return true;
	}

	public function insertar_tablasimcards(){
		$sql="select spadjuntarsimcard('{$this->iproveedor}','{$this->ifechacompra}','{$this->numserie}','{$this->tiposim}','{$this->usuario}')";
		//echo $sql;
		$this->con->consultaSimple($sql);
		return true;
	}

	public function insertar_tablamifi(){
		$sql="select spadjuntarmifi('{$this->iproveedor}','{$this->ifechacompra}','{$this->numserie}','{$this->tipomifi}','{$this->usuario}')";
		//echo $sql;
		$this->con->consultaSimple($sql);
		return true;
	}

	public function vertablatransferencia (){
		$sql="select * from tblserialpos";
		$resultado=$this->con->consultaRetorno($sql);
		$num=pg_num_rows($resultado);

		if($num>0){
			return true;
		}
			else{
			return false;
		}
	}

	public function spverarchivodiario (){
		$sql="select * from tblserialpos";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vertablatransferenciacliente (){
		$sql="select * from adm_archivoclientes";
		$resultado=$this->con->consultaRetorno($sql);
		$num=pg_num_rows($resultado);

		if($num>0){
			return true;
		}
		else{
			return false;
		}
	}

	public function spverarchivodiariocliente (){
		$sql="select * from spverarchivodiariocliente('{$this->cliente}','{$this->fecha}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function insertar_tablclient(){

		$sql="select spadjuntarcliente('{$this->TipoCliente}','{$this->CustomerName}','{$this->ShortName}','{$this->StatementName}','{$this->CustomerClass}','{$this->LookupButton2}','{$this->AddressCode}','{$this->ContactPerson}','{$this->Address1}','{$this->Address2}','{$this->Address3}','{$this->City}','{$this->State}','{$this->Zip}','{$this->CountryCode}','{$this->Country}','{$this->Phone1}','{$this->Phone2}','{$this->Phone3}','{$this->Fax}','{$this->UPSZone}','{$this->ShippingMethod}','{$this->TaxScheduleID}','{$this->ShipCompletDocument}','{$this->PrimaryShiptoAddressCode}','{$this->SalespersonID}','{$this->LookupButton6}','{$this->UserDefined1}','{$this->UserDefined2}','{$this->Comment1}','{$this->Comment2}','{$this->CustomerDiscount}','{$this->PaymentTermsID}','{$this->DiscountGracePeriod}','{$this->DueDateGracePeriod}','{$this->PriceLevel}','{$this->AccountsButton}','{$this->OptionsButton}','{$this->TaxRegistrationNumber}','{$this->CurrencyID}','{$this->EmailStatementsToAddress}','{$this->usuario}')";
		//echo $sql;
		$this->con->consultaSimple($sql);
		return true;
	}

	public function eliminar_tabltranscliente(){
		$sql="delete from adm_archivoclientes";
		$this->con->consultaSimple($sql);
		return true;
	}

	public function speliminar_archivocliente(){
		$sql="delete from adm_archivoclientes";
		$this->con->consultaSimple($sql);
		return true;
	}	

	public function eliminar_tabltranstxt(){
		
			$sql="delete from txt_archivo ";
			$this->con->consultaSimple($sql);
			return true;
	}
	
	public function insertar_tabltranstxt(){
		$sql="select spadjuntartxt('{$this->numserie}')";
		$this->con->consultaSimple($sql);
		return true;
	}

	public function spverarchivotxt (){
		$sql="select * from txt_archivo";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_verpagoaconfirmar(){
	$sql="select * from sp_verpagoaconfirmar('{$this->idafiliado}','{$this->referencia}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_confirmarpagomifi(){
	$sql="select * from sp_confirmarpagomifi('{$this->idafiliado}','{$this->referencia}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verseriales (){
		$sql="select * from tblserialpos";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function verserialessimcards (){
		$sql="select * from tblserialsimcard";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function verserialesmifi (){
		$sql="select * from tblinventariomifi";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function selectestados(){
		$sql="select id_estado as iestados, estado from tblestados";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selectmunicipio(){
		$sql="select id_municipio as imunicipios, municipio, id_estado as iestados from tblmunicipios";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sptipozonas(){
		$sql="select codzona as itipozona, desczona from sptipozonas('{$this->perfil}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selectparroquias(){
		$sql="select id_parroquia as iparroquias, parroquia, id_municipio as imunicipios from tblparroquias";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function verprefijobanco()
	{
		$sql="select * from spverprefijobanco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vertipodepos()
	{
		$sql="select * from spvertipopos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vermarcadepos()
	{
		$sql="select * from spvermarcapos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vertipodelinea()
	{
		$sql="select * from spvertipolinea()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vertipodemifi()
	{
		$sql="select * from spvertipodemifi()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	
	public function spverfechasseriales (){
		$sql="select * from spverfechasseriales('{$this->valor}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function speliminar_archivoseries(){
		$sql="select * from speliminar_archivoseries('{$this->fecha}')";
		//echo $sql;
		$this->con->consultaSimple($sql);
		return true;
	}	

	public function sp_buscarserialespos (){
		$sql="select * from sp_buscarserialespos('{$this->fecha}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarserialessimcards (){
		$sql="select * from sp_buscarserialessimcards('{$this->fecha}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarserialesmifi (){
		$sql="select * from sp_buscarserialesmifi('{$this->fecha}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function sp_reportedetalladopos (){
		$sql="select * from sp_reportedetalladopos('{$this->fecha}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_reportegeneralpos (){
		$sql="select * from sp_reportegeneralpos('{$this->fecha}','{$this->marcapos}','{$this->operacion}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function usuario (){
	$sql="select * from spusuario('{$this->usuario}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarclienterecibido(){
	$sql="select * from spbuscarclienterecibido('{$this->cliente}','{$this->fecha}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverbancocliente (){
	$sql="select * from spverbancocliente()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
	
	
	public function spbanco(){
		$sql="select * from sp_banco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function verclientefacturacion(){
	$sql="select * from spverclientefacturacion('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spcliente(){
		$sql="select * from sp_cliente('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function mostrarClienteFacturacion(){
	$sql="select * from spmostrarClienteFacturacion('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrarClienteFacturacionmifi(){
	$sql="select * from spmostrarClienteFacturacionMifi('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	//recordar
	public function spinsertarexpfactura (){
		$sql="select * from spinsertarexpfacturas('{$this->idregistro}','{$this->directorio}')";
		//echo $sql;
		$resultado=$this->con->spinsertarexpfactura($sql);
	    return $resultado;
	}
//

	public function EditarClienteFacturacion(){
	$sql="select * from spEditarClienteFacturacion('{$this->coddocumento}','{$this->cliente}','{$this->namearchivo}','{$this->costopos}','{$this->fechafacturacion}','{$this->nfactura}','{$this->nameformapago}','{$this->codtarjeta}','{$this->ntransferencia}','{$this->bancoorigen}','{$this->fecha}','{$this->horapago}','{$this->usuario}','{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
	
	public function sp_buscarclientefacturacionRif(){
		$sql="select * from spbuscarclientefacturacionRif('{$this->rif}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	} 

	public function spbuscarclientefacturacionmifixrif(){
		$sql="select * from spbuscarclientefacturacionmifixrif('{$this->rif}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarclientefacturacion(){
	$sql="select * from spbuscarclientefacturacion('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarclientefacturacionmifi(){
	$sql="select * from spbuscarclientefacturacionmifi('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_verclientefacturacion(){
	$sql="select * from spverclientefacturacion('{$this->cliente}','{$this->fecha}','{$this->namearchivo}','{$this->masiva}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
	// 


	public function sp_verclientefacturacionmifi(){
	$sql="select * from spverclientefacturacionmifi('{$this->cliente}','{$this->fecha}','{$this->namearchivo}','{$this->masiva}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function selectformapago(){
		$sql="select * from tblformapago";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selectmarcapos(){
		$sql="select codtipo as imarcapos,  marcapos from tblmarcapos";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function selecttipopos(){
		$sql="select * from tbltipopos";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selecttipotarjeta(){
		$sql="select * from tbltipotarjeta";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selecttipobanco(){
		$sql="select codigobanco::integer as bancoorigen, ibp from tblbanco";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selectbanco(){
		$sql="select codigobanco::integer as banco, ibp from tblbanco";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selectserialpos(){
		$sql="select spserialpos as serialpo from spserialpos('{$this->nafiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function selectestatuspos(){
		$sql="select * from spestatusservices ('{$this->codestatus}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function selectestatusinstmifi(){
		$sql="select * from spestatus_instmifi ('{$this->codestatus}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function selecttecnicos(){
		$sql="select codtecnicos as tecnico, nombres||' '||apellidos as ntecnico  from  tbltecnicos where estatus='1'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function speliminar_archivo (){
		$sql="select * from speliminararchivotemp('{$this->nombre}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarinventariopos(){
		$sql="select * from sp_buscarinventariopos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverificarmodulosactivos (){
		$sql="select * from spverificarmodulosactivos('{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function spregistrocliente (){
		$sql="select * from spregistrocliente('{$this->id_registro}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		//  DATOS NUEVOS PARA LA ASIGNACION DE SERIALES

		public function spvermotivodev (){
		$sql="select * from sp_vermotivodev()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function spverdestinoequipo (){
		$sql="select * from sp_verdestinoequipo()";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
	}

	public function spverdestinoequipomifi (){
		$sql="select * from sp_verdestinodemifi()";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
	}

		public function spbuscarseriales (){
		$sql="select * from spserialesdisponibles('{$this->busqueda}','{$this->banco}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
//echo $resultado['serialpos'];
		return $resultado;
	}

	public function spinsertarfactura (){
		$sql="select * from spinsertarfactura('{$this->afiliado}','{$this->directorio}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}



	public function spbuscarserialsimcard (){
		$sql="select * from spserialsimcardisponibles('{$this->busqueda}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
//echo $resultado['serialpos'];
		return $resultado;
	}

		public function spbuscarserialmifi (){
		$sql="select * from spserialmifi('{$this->busqueda}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function mostrarseriales(){
	$sql="select * from sp_serialesdisponibles1('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}


		public function mostrarserialesmifi(){
	$sql="select * from sp_mifidisponible('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrardetalleseriales(){
	$sql="select * from spmostrardetalleseriales('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spbuscarserialesdevolucion (){
		$sql="select * from spserialesdev('{$this->busqueda}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}	

	public function spbuscarserialsimcarddevolucion (){
		$sql="select * from spserialsimcardev('{$this->busqueda}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
//echo $resultado['serialpos'];
		return $resultado;
	}	
		public function spbuscarserialmifidev (){
		$sql="select * from spserialmifidev('{$this->busqueda}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	
		public function mostrardetalleserialesdevtotal(){
	$sql="select * from spmostrardetalleserialesdevoluciontotal('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

		public function mostrardetalleserialesdemifi(){
	$sql="select * from spmostrardetalleserialesdemifi('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function mostrarClienteFacturasanuladas(){
	$sql="select * from spmostrarClienteFacturasanuladas('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function mostrarClientedeFacturasanuladasmifi(){
	$sql="select * from spmostrarclientefacturasanuladas('{$this->id_registro}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function ver_factura (){
		$sql="select * from sp_verfactura('{$this->idregistro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

	public function spfacturaanuladas (){
		$sql="select * from sp_verfacturadevolucion('{$this->idregistro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

		public function spfacturaanuladasdemifi (){
		$sql="select * from sp_verfacturadevoluciondemifi('{$this->idregistro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

		public function spinsertarfactura_mifi (){
		$sql="select * from spinsertarfactura_mifi('{$this->afiliado}','{$this->directorio}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

		public function ver_facturademifi (){
		$sql="select * from sp_verfactura_demifi('{$this->idregistro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}


	public function spEdicionRsCliente (){
		$sql="select * from spEdicionRsCliente('{$this->dato}','{$this->idregistro}','{$this->user}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

	public function spEdicionRsClCliente (){
		$sql="select * from spEdicionRsClCliente('{$this->dato}','{$this->idregistro}','{$this->user}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

	public function spEdicionRIFCliente (){
		$sql="select * from spEdicionRIFCliente('{$this->dato}','{$this->idregistro}','{$this->user}','{$this->ibanco}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

	public function spEdicionBancoCliente (){
		$sql="select * from sp_modificarbancoactual('{$this->idregistro}','{$this->dato}','{$this->user}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}

	public function spEdicionModeloCliente (){
	$sql="select * from sp_modificarmodeloactual('{$this->idregistro}','{$this->dato}','{$this->user}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
    return $resultado;
	}

	public function spinfocliente(){
		$sql="select * from spinfocliente('{$this->idcliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverclientesbancos()
	{
	$sql="select * from spvercliente()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverclientestbancos()
	{
	$sql="select * from spvertodoclientes()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function formadepago()
	{
	$sql="select * from sp_formapago()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
// 
	public function sp_verdescripcionmotivos(){
		// $sql="select * from sp_verafiliadocomercializacion('{$this->id_registro}') limit 1";
			$sql="select * from sp_verdescripcionmotivos('{$this->detalledesaprob}') limit 1";

		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_verpagos(){
		$sql="select * from sp_verpagos('{$this->id_afiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_verbcancoorigenasig(){
		$sql="select * from sp_verbcancoorigenasig('{$this->id_afiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_verbcancodestinoasig(){
		$sql="select * from sp_verbcancodestinoasig('{$this->id_afiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_verfromapagoasig(){
		$sql="select * from sp_verfromapagoasig('{$this->id_afiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_verpagosdemifi(){
		$sql="select * from sp_verpagosdemifi('{$this->idafiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spvertipopercance()
	{
	$sql="select * from spvertipopercance()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spbuscarpercance(){
	$sql="select * from spbuscarpercance('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrarserialesp(){
	$sql="select * from sp_serialesdisponibles1('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	
	}

	public function sp_verpago(){
		$sql="select * from sp_verpagos('{$this->id_afiliado}','{$this->idpagos}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function estatusconfirpagos(){
	$sql="select * from estatusconfirpagos('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verconsecutivo(){
	$sql="select * from sp_verconsecutivoserial()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_costoposs(){
	$sql="select * from sp_costopos('{$this->id_registro}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_regmodalidad(){
	$sql="select * from tbl_registromodalidadpago where ('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverclientefacturacionpagos(){
	$sql="select * from spverclientefacturacionpagos('{$this->tipousuario}','{$this->ejecutivoin}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarpagosnoconf(){
	$sql="select * from sp_buscarpagosnoconf('{$this->usuario}','{$this->ejecutivoin}','{$this->tipousuario}','{$this->coordinador}','{$this->coordinadordos}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_serialesrecompra(){
	$sql="select * from tbl_seriales_recompra where id_cliente=('{$this->id_registro}') and estatus_conf_recompra='0'" ;
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spmostrarclientefacturacion_recompra(){
	$sql="select * from spmostrarclientefacturacion_recompra('{$this->id_registro}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_tasacambioxfecha (){
		$sql="select * from sp_tasacambioxfecha('{$this->fechapago}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
// 

public function sp_reporteriesgomedidoadpins (){
	$sql="select * from sp_reporteriesgomedidoadpins ('{$this->idregistro}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
 }
//  
	public function mostrardetalleserialesrecompra(){
	$sql="select * from tbl_seriales_recompra where id_cliente='{$this->id_registro}'";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spinsertardocumentosfactura (){
		$sql="select * from spinsertardocumentosfactura('{$this->idregistro}','{$this->directorio}','{$this->afiliado}','{$this->usuario}','{$this->eexp}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}
	//recordar
	public function spinsertarhistorial (){
		$sql="select * from spinsertarhistorial('{$this->idregistro}','{$this->afiliado}','{$this->usuario}','{$this->mensaje}','{$this->detalles}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	//

	public function sp_bancoasignar(){
	$sql="select * from sp_bancoasignar()";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
	
	public function sp_modelopos(){
	$sql="select * from sp_modelopos()";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
}
?>