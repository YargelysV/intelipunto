<?php
include_once('Conexion.php');

class Servicio
{
	
	//variables
	private $usuario;
	private $cliente;
	private $fecha;
	private $namearchivo;
	private $fechafacturacion;
	private $rif;
	private $afiliado;
	
	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido; 
	}

	public function get ($atributo){
		return $this->$atributo; 
	}
	
	
	public function spbanco(){
		$sql="select * from sp_banco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spbuscaracuerdoservicioBF(){
		$sql="select * from spbuscaracuerdoservicioBF('{$this->cliente}','{$this->fecha}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function spbuscaracuerdoservicioBFgrafica(){
		$sql="select * from spbuscaracuerdoservicioBFgrafica('{$this->cliente}','{$this->fecha}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function spbuscaracuerdoservicioRIF(){
		$sql="select * from spbuscaracuerdoserviciorif('{$this->rif}','{$this->cliente}','{$this->fecha}','{$this->namearchivo}','{$this->afiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spbuscarclienteacuerdorif(){
		$sql="select * from spbuscarclienteacuerdorif('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


}
?>