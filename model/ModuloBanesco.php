<?php
include_once('Conexion.php');	

	class Banesco
	{
		
		private $idlote;
		private $consecutivo;
		private $id;
		private $rif;
		private $razonsocial;
		private $banco;
		private $representantelegal;
		private $actividadecon;
		private $estatusafiliado;
		private $afiliado;
		private $ejecutivo;
		private $correo1;
		private $correo2;
		private $correo3;
		private $tlf1;
		private $usuario;
		private $motivo;

		public function __construct(){
			$this->con=new Conexion();
		}

		public function set($atributo,$contenido){
			$this->$atributo=$contenido;
		}

		public function get ($atributo){
			return $this->$atributo;
		}

		public function buscarlote(){
		$sql="select * from sp_buscarlote()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function buscarlotes(){
		$sql="select * from sp_buscarlotes('{$this->idlote}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function actualizarserial(){
		$sql="select * from sp_actualizarserialstatus('{$this->serial}','{$this->idestatus}','{$this->usuariocarga}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarstatusllaves(){
		$sql="select * from sp_guardarstatusxlotesllaves('{$this->idlote}','{$this->usuariocarga}','{$this->statusllaves}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarstatusapp(){
		$sql="select * from sp_guardarstatusapp('{$this->serial}','{$this->idestatusapp}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarstatustem(){
		$sql="select * from sp_guardarstatustem('{$this->serial}','{$this->idestatustem}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarstatusconfig(){
		$sql="select * from sp_guardarstatusconfig('{$this->serial}','{$this->select}','{$this->iobserv}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_updateconfig(){
		$sql="select * from sp_updateconfig('{$this->serial}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarstatusequipobbu(){
		$sql="select * from sp_guardarstatusequipobbu('{$this->serial}','{$this->idstatus}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarstatusequipolotes(){
		$sql="select * from sp_guardarstatusequipolotes('{$this->statusbbu}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarLoteApp(){
		$sql="select * from sp_guardarLoteApp('{$this->idlote}','{$this->usuariocarga}','{$this->statusapp}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarLoteTEM(){
		$sql="select * from sp_guardarLoteTEM('{$this->idlote}','{$this->usuariocarga}','{$this->statustem}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function reportebanesco(){
		$sql="select * from sp_reportediariobanescogeneral('{$this->estatus}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function busquedaequiposbbu(){
		$sql="select * from sp_busquedaequiposbbu('{$this->status}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spverclientesbanesco()
		{
		$sql="select * from spverclientesbanesco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spverafiliadosbanesc()
		{
			$sql="select * from spverafiliadosasignados()";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function spvernumcuentabanesc()
		{
			$sql="select * from spvernumcuentabanesc()";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function verafiliadosbanescodetalle()
		{
			$sql="select * from spverafiliadosasignados() where afiliado='{$this->NombreAfiliado}'";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function vercuentabanescodetalle()
		{
			$sql="select * from spvernumcuentabanesc() where ncuenta='{$this->numcuenta}'";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

     	public function insertar_datos()
		{
		$sql="select * from spinsertardatosbanesco('{$this->Consecutivo}','{$this->id}','{$this->rif}','{$this->razonsocial}','{$this->banco}','{$this->afiliado}','{$this->ejecutivo}','{$this->motivo}','{$this->ncuenta}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function verestatusafiliado()
		{
			$sql="select * from sp_verestatusafiliado()";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function vermotivorechazo()
		{
			$sql="select * from sp_vermotivorechazo()";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function spverclientesbanescofact()
		{
		$sql="select * from spverclientesbanescofact()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function insertar_terminal()
		{
		$sql="select * from sp_insertarterminalbanesco('{$this->Consecutivo}','{$this->id}','{$this->rif}','{$this->razonsocial}','{$this->banco}','{$this->afiliado}','{$this->serial}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function verprefijobanco()
	{
		$sql="select * from spverprefijobanco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function buscarestatus()
	{
		$sql="select * from sp_buscarestatusbanesco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function buscarregistros(){
		$sql="select * from sp_reporteserialstatus('{$this->idstatus}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function buscarreportegeneral(){
		$sql="select * from sp_reportegeneralbanesco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function buscarbanco()
		{
		$sql="select * from sp_buscarbancobanesco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function buscarlotesreporte()
		{
		$sql="select * from sp_buscarreportexlotes('{$this->idlote}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function banescoafiliadosvar(){
		$sql="select * from sp_buscarafiliadosbanesco('{$this->variable}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		// public function banescoafiliados(){
		// $sql="select * from sp_buscarafiliadosbanesco()";
		// $resultado=$this->con->consultaRetorno($sql);
		// return $resultado;
		// }

		public function desafiliarregistro(){
		$sql="select * from sp_insertarterminalbanesco('{$this->iserial}','{$this->idestatus}','{$this->usuariocarga}')";
		$resultado=$this->con->consultaRetorno($sql);
	}

		public function sp_verestatus(){
		$sql="select * from sp_verestatusconfig()";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $resultado;
		return $resultado;
		}

		public function sp_verestatusdesf(){
		$sql="select * from sp_verestatusdesinc()";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $resultado;
		return $resultado;
		}

		public function guardardesaf(){
		$sql="select * from sp_insertardesafiliacion('{$this->serial}','{$this->idconsec}','{$this->usuariocarga}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function guardardesincorporacion(){
		$sql="select * from sp_insertardesincorporacion('{$this->serial}','{$this->idconsec}','{$this->usuariocarga}','{$this->select}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spverbancos()
		{
		$sql="select * from spverbancos('{$this->valor}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarpercancebanesco(){
		$sql="select * from sp_guardarpercancebanesco('{$this->idconsec}','{$this->serial}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarequiposreingreso(){
		$sql="select * from sp_buscarequiposreingreso('{$this->serial}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function actualizarestatusreingreso(){
		$sql="select * from sp_actualizarestatusreingreso('{$this->serial}','{$this->idestatus}','{$this->usuariocarga}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarreingresoapp(){
		$sql="select * from sp_guardarreingresoapp('{$this->serial}','{$this->idestatusapp}','{$this->usuariocarga}')";
		echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarreingresotem(){
		$sql="select * from sp_guardarreingresotem('{$this->serial}','{$this->idestatustem}','{$this->usuariocarga}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function sp_buscarequipostaller(){
		$sql="select * from sp_buscarequipostaller('{$this->serial}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
	}
?>	
