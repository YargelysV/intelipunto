<?php
include_once('Conexion.php');

class Modulo
{

	//variables

	private $login;
	private $id_registro;
	private $activo;
	private $usuario;

	//metodos
	//metodos
	public function __construct(){
		$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function spverificarmodulosactivos (){
		$sql="select * from spverificarmodulosactivos('{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_actividad(){
		$sql="select * from sp_actividad('{$this->id_registro}','{$this->activo}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_vernotificaciones (){
		$sql="select * from sp_vernotificaciones('{$this->area}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_countnotificaciones (){
		$sql="select * from sp_countnotificaciones('{$this->area}')";
		// $sql="select * from sp_vernotificaciones('{$this->area}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_actividadencuesta()
		{
		$sql="select * from sp_actividadencuesta('{$this->idcliente}','{$this->activo}','{$this->usuario}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
	
	public function sp_pagosnoconf(){
		$sql="select * from sp_pagosnoconf()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_recompranoconf(){
		$sql="select id_cliente from tbl_seriales_recompra where estatus_conf_recompra='0'";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_tasacambio(){
		$sql="select id_consecutivo, ROUND(tasa_dolar::numeric, 4) as tasa_dolar from tbl_tasadecambio group by tasa_dolar, id_consecutivo order by id_consecutivo desc limit 1";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	
}
?>
