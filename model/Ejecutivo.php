<?php
include_once('Conexion.php');

	class Ejecutivo
	{

		//variables
		private $operacion;
		private $codejecutivo;
		private $nejecutivo;
		private $documento;
		private $tipodoc;
		private $nombres;
		private $apellidos;
		private $usuario;
		private $vendedor;
		private $banco;
		private $ejecutivo;
	
		
		
		//metodos
		public function __construct(){
			$this->con=new Conexion();
		}

		public function set($atributo,$contenido){
			$this->$atributo=$contenido;
		}

		public function get ($atributo){
			return $this->$atributo;
		}

		public function mostrarEjecutivos (){

		$sql="select * from spmostrarejecutivo()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function verEjecutivosBancos (){

		$sql="select * from spver_ejecutivoactivo()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		} 

		public function spbancoejecutivos(){
		$sql="select * from spverbancoejecutivos('{$this->vendedor}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function selectipobanco(){
		$sql="select * from sp_tipobancoejecutivo('{$this->codejecutivo}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function mostrartipodoc (){

		$sql="select * from sptipodocumento()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function crear()
		{

        $sql2="select * from spverificaejecutivorep ('{$this->codejecutivo}','{$this->tipodoc}','{$this->documento}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);

		if($num!=0){
			return false;
		}
		else{
		$sql2="select * from spejecutivos ('{$this->operacion}','{$this->codejecutivo}','{$this->nejecutivo}','{$this->tipodoc}','{$this->documento}','{$this->nombres}','{$this->apellidos}','{$this->ejecutivo}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}
}

		public function mostrarEjecutivos1 (){
		$sql="select * from spmostrar_ejecutivo() where coddocumento='{$this->documento}'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function EditarEjecutivo (){
			$sql="select * from spejecutivos ('{$this->operacion}','{$this->codejecutivo}','{$this->nejecutivo}','{$this->tipodoc}','{$this->documento}','{$this->nombres}','{$this->apellidos}','{$this->ejecutivo}','{$this->usuario}')";
			//echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}
		
		public function editarr (){
				

		$sql2="select * from sp_ejecutivo ('{$this->operacion}','{$this->vendedor}','{$this->nombrebanco}','{$this->documento}','{$this->usuario}')";
			$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function ejecutivobanco()
		{

		$sql="select * from spejecutivo_banco('{$this->vendedor}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;

	  }	


	  public function verejecutivoxbanco()
		{

		$sql="select * from spverejecutivo_banco('{$this->vendedor}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;

	  }	
	
	  		public function EliminarEjecutivo (){
		$sql="select * from spejecutivos ('{$this->operacion}',null,null,null,'{$this->documento}',null,null,null,null)";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function actualizarbanco()	
		{

		$sql2="select * from sp_ejecutivo ('{$this->banco}','{$this->vendedor}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

			public function AsignarBancoEjec()	
		{

		$sql2="select * from sp_asignarbcoejec ('{$this->banco}','{$this->vendedor}','{$this->usuario}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}
		

		public function eliminarbanco()	
		{

		$sql2="select * from sp_eliminarrbanco ('{$this->banco}')";
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function vertipodocumentos(){
		$sql="select * from sptipodocumento()";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function eliminarbancoejecutivo()	
		{

		$sql2="select * from sp_eliminarbancoejecutivo ('{$this->bancoorigen}','{$this->vendedor}')";
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}
	}
?>	
