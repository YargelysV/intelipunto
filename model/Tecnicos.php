<?php
include_once('Conexion.php');

	class Tecnicos
	{
		private $operacion;
		private $codtecnico;
		private $coddocumento;
		private $tipodoc;
		private $nombres;
		private $apellidos;
		private $trabajo;
		private $usuario;
		private $documento;
		private $estatus;
		private $selectperfil;
		private $selectversion;
		private $idusuario;
		private $idafiliado;
		private $idsecuencial;

		public function __construct(){
			$this->con=new Conexion();
		}

		public function set($atributo,$contenido){
			$this->$atributo=$contenido;
		}

		public function get ($atributo){
			return $this->$atributo;
		}

		public function mostrartecnicos (){
		$sql="select * from spmostrar_tecnicos()";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $resultado;
		return $resultado;
		}

		public function mostrartipodoc (){

		$sql="select * from sptipodocumento()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function crear()
		{

        $sql2="select * from spverificatecnicorep ('{$this->tipodoc}','{$this->coddocumento}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);
         echo $num;
		if($num!=0){
			return false;
		}
		else{
		$sql2="select * from sptecnicos ('{$this->operacion}','{$this->trabajo}','{$this->tipodoc}','{$this->coddocumento}','{$this->nombres}','{$this->apellidos}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}
}

		public function mostrartecnicos1 (){
		$sql="select * from spmostrar_tecnicos() where coddocumento='{$this->coddocumento}'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function EditarTecnicos (){
			
			$sql="select * from sptecnicos ('{$this->operacion}','{$this->trabajo}','{$this->tipodoc}','{$this->coddocumento}','{$this->nombres}','{$this->apellidos}','{$this->usuario}')";
			echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
			
		}
	
		public function EliminarTecnicos (){
		$sql="select * from sptecnicos ('{$this->operacion}',null,null,null,'{$this->coddocumento}',null,null,null)";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
		
		public function vertipodocumentos(){
		$sql="select * from sptipodocumento()";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_estatustecnico()
		{
		$sql="select * from sp_estatustecnico('{$this->documento}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_sstatustecnico(){
		$sql="select * from sp_sstatustecnico('{$this->documento}','{$this->estatus}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function sp_verclienteafiliado()
			{
			$sql="select * from sp_verclienteafiliado('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
			$resultado= $this->con->consultaRetorno($sql);
			return $resultado;
			}

		public function sp_buscarservitec(){
			$sql="select * from sp_buscarservitec('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function sp_buscarafiliadoRif(){
			$sql="select * from spbuscarafiliadoRif('{$this->rif}')";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function mostrarinventarioposafiliado()
		{
			$sql="select * from spasignarserialpos1('{$this->idcliente}','{$this->busqueda}')";
			$resultado= $this->con->consultaRetorno($sql);
			//echo $sql;
			return $resultado;
		}

		public function spasignarserialpos()
		{
		$sql="select * from spasignarserialpos('{$this->correlativo}','{$this->fechaent}','{$this->numterminal}','{$this->serialpos}','{$this->numsim}','{$this->namearchivo}','{$this->usuariocarg}','{$this->numeroamex}','{$this->idregistro}','{$this->banco}','{$this->selectperfil}','{$this->selectversion}','{$this->orservicio}','{$this->nafiliado}')";
		echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		

		public function sp_verclientegestion()
		{
		$sql="select * from sp_verclientegestion('{$this->cliente}','{$this->fecha}','{$this->namearchivo}','{$this->region}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscargestionservicio(){
		$sql="select * from sp_buscargestionservicio('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

			public function sp_buscarafiliadoRifgestion(){
		$sql="select * from sp_buscarafiliadoRifgestion('{$this->rif}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function mostrarserialafiliado()
		{
		$sql="select * from spmostrarserialafiliado('{$this->idcliente}','{$this->idsecuencial}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function selecttecnicos(){
		$sql="select codtecnicos as tecnico, nombres||' '||apellidos as ntecnico  from  tbltecnicos where estatus='1'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarregion()
		{
		$sql="select * from psbuscarregiones('{$this->usuario}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_buscarregionusuarios()
		{
		$sql="select * from sp_buscarregionusuarios('{$this->usuario}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}		

	public function sp_buscarbancoregion()
		{
		$sql="select * from spverbancoregion('{$this->valor}','{$this->region}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spverfecharegion()
		{
		$sql="select * from spverfecharegion('{$this->valor}','{$this->region}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_verclientedetalladomifi()
		{
		$sql="select * from sp_verclientedetalladomifi('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarxfechamifi(){
		$sql="select * from sp_buscarfechamifi('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_buscarclientemifixrif(){
		$sql="select * from sp_buscarclientemifixrif('{$this->rif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function mostrardetalleafiliado()
		{
		$sql="select * from spmostrardetalleafiliado('{$this->idcliente}','{$this->serial}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spactualizargestionpos()
		{
		$sql="select * from spactualizargestionpos('{$this->operacion}','{$this->serialpos}','{$this->nroafiliacion}','{$this->namearchivo}','{$this->usuario}','{$this->fecharecepalma}','{$this->tecnico}','{$this->fechagest}','{$this->estatus}','{$this->fechainst}','{$this->fechacarg}','{$this->observacion}','{$this->idcliente}','{$this->fechalog}','{$this->fechaenv}','{$this->banco}','{$this->personacontacto}','{$this->representantelegal}','{$this->mifiserial}')";
		echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spmostrarserialmifi2()
		{
		$sql="select * from spmostrarserialmifi2('{$this->idcliente}','{$this->idsecuencial}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spagregargestionmifi()
		{
		$sql="select * from spagregargestionmifi('{$this->operacion}','{$this->serialmifi}','{$this->namearchivo}','{$this->usuario}','{$this->fecharecepalma}','{$this->tecnico}','{$this->fechagest}','{$this->estatus}','{$this->fechainst}','{$this->fechacarg}','{$this->observacion}','{$this->idcliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function selectestatusinstmifi(){
		$sql="select * from spestatus_instmifi ('{$this->codestatus}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function verclienteserviciotecnico()
		{
		$sql="select * from spverclienteserviciotecnico('{$this->cliente}','{$this->fecha}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spverbancoregionreporte()
		{
		$sql="select * from spverbancoregionreporte('{$this->valor}','{$this->region}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function mostrarserialafiliados()
		{
		$sql="select * from mostrarserialafiliados('{$this->idcliente}','{$this->idsecuencial}','{$this->idusuario}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function eliminargestion()
		{
		$sql="select * from eliminargestion('{$this->idafiliado}','{$this->idsecuencial}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}
}
?>	

