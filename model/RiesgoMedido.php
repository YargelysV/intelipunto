<?php
include_once('Conexion.php');

class RiesgoMedido
{

	//variables

	private $banco;
	private $usuario;
	private $ejecutivoin;
	private $tipousuario;
	private $coordinador;
	private $coordinadordos;
	private $id_afiliado;
	private $correlativo;
	private $marcapos;
	private $tipopos;
	private $valordolar;
	private $idmodelopos;
	private $idtipopos;
	private $montousd;
	private $preciobanesco;
	private $bancoreg;
	private $area_usuario;

	//metodos
	//metodos
	public function __construct(){
		$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function sp_reporteriesgomedido (){
		$sql="select * from sp_reporriesgomedido('{$this->regiones}','{$this->banco}','{$this->usuario}','{$this->ejecutivoin}','{$this->tipousuario}','{$this->coordinador}','{$this->coordinadordos}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
 	}

 	public function sp_reportecargaparametros (){
		$sql="select * from sp_reportecargaparametros('{$this->banco}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
 	}

 	public function sp_verpagoscuotas (){
		$sql="select * from sp_verpagoscuotas('{$this->id_afiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
 	}

 	public function sp_verpreciopos (){
		$sql="select * from sp_verpreciopos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
 	}

 	public function spEditarPrecios()
		{
		$sql="select * from spEditarPrecios('{$this->correlativo}','{$this->marcapos}','{$this->tipopos}','{$this->valordolar}','{$this->usuario}','{$this->bancoreg}')";
		echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function sp_guardarreciopos(){
	    	$sql="select * from sp_guardarpreciopos('{$this->idmodelopos}','{$this->usuario}','{$this->idtipopos}','{$this->montousd}','{$this->preciobanesco}')";
	    	//echo $sql;
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }

	public function sp_guardarnuevopos(){
	    	$sql="select * from sp_guardarnuevospos('{$this->idmodelopos}','{$this->idtipopos}')";
	    	//echo $sql;
	    	$resultado=$this->con->consultaRetorno($sql);
	    	return $resultado;
	    }
 
	public function sp_marcapos(){
		$sql="select * from sp_marcapos()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
 
	public function sp_modeloposnew(){
		$sql="select * from sp_tipoposnew()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	 public function sp_buscarregion (){
		$sql="select * from sp_buscarregionusuarios('{$this->usuario}','{$this->area_usuario}')";
		echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
 	}

	public function sp_buscarbancoregion()
		{
		$sql="select * from spverbancoregionreporte('{$this->valor}','{$this->regiones}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		} 	
}
?>
