<?php
include_once('Conexion.php');

	class VisitaTecnica
	{
		private $operacion;
		private $codtecnico;
		private $coddocumento;
		private $tipodoc;
		private $nombres;
		private $apellidos;
		private $trabajo;
		private $usuario;
		private $documento;
		private $estatus;
		private $selectperfil;
		private $selectversion;
		private $idusuario;
		private $idafiliado;
		private $idsecuencial;

		public function __construct(){
			$this->con=new Conexion();
		}

		public function set($atributo,$contenido){
			$this->$atributo=$contenido;
		}

		public function get ($atributo){
			return $this->$atributo;
		}

		public function sp_mostrarvisitatecnica(){
		$sql="select * from sp_mostrarvisitatecnica('{$this->banco}','{$this->fechavisita}','{$this->nrorif}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_mostrarvisitatecnicaxrif(){
		$sql="select * from sp_mostrarvisitatecnicarifyserial('{$this->busqueda}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_mostrarvisitatecnicaxfechas(){
		$sql="select * from sp_mostrarvisitatecnicaxfechas('{$this->fechainicial}','{$this->fechafinal}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_mostrargestionxfechas(){
		$sql="select * from sp_busquedaxfechagestion('{$this->fechainicial}','{$this->fechafinal}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_mostrarreportevisitatecnica(){
		$sql="select * from sp_mostrarreportevisitatecnica('{$this->banco}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


		public function spverdetallevisitatecnica(){		
		$sql="select * from spverdetallevisitatecnica('{$this->idcliente}','{$this->serialpos}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spverdetallevisitatecnica1(){		
		$sql="select * from spverdetallevisitatecnica('{$this->idcliente}','{$this->serialpos}') where idresultado='1' or idresultado='2'";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_mostrargestionvisita(){		
		$sql="select * from sp_mostrargestionvisita('{$this->idcliente}','{$this->idsecuencial}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_mostrartecnicovisitatec(){
		$sql="select * from sp_mostrartecnicovisitatec('{$this->idcliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function spestatusvisita(){
		$sql="select * from spestatusvisita()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sprelacion_condstatus(){
		$sql="select * from sprelacion_condstatus('{$this->idvisita}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function selecttecnicosvisita(){
		$sql="select idtecnico as tecnico, nombres||' '||apellidos as ntecnico  from  tbltecnicos where estatus='1'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarvisita(){
		$sql="select * from sp_guardargestionvisita('{$this->idcliente}','{$this->serialpos}','{$this->fechagesti}','{$this->estatusvisita}','{$this->tecnico}','{$this->fechavisita}','{$this->usuario}','{$this->observacion}','{$this->idcondicion}','{$this->idsecuencial}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardaraprobacionvisita(){
		$sql="select * from sp_guardaraprobacionvisita('{$this->sec}','{$this->idaprob}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_guardarinfadicional(){
		$sql="select * from sp_guardarinfadicional('{$this->tlf1}','{$this->tlf2}','{$this->email}','{$this->nombre2}','{$this->apellido2}','{$this->usuario}','{$this->banco}','{$this->cliente}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	    public function direccionadicional(){
       	$sql="select * from sp_agregardirecciontecnico('{$this->coddocumento}','{$this->codetipodir}','{$this->e_calle_av}','{$this->e_localidad}','{$this->e_sector}','{$this->e_nlocal}','{$this->e_urbanizacion}','{$this->e_estado}','{$this->e_codepostal}','{$this->e_ptoref}','{$this->prefijo}','{$this->usuario}','{$this->id_registro}')";
        	//echo $sql;
        $resultado= $this->con->consultaRetorno($sql);
        return $resultado;
     }
	
}
?>	

