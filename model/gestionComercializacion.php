<?php
include_once('Conexion.php');

class Comercializacion
{

	//variables
	private $usuario;
	private $tipodoc;
	private $coddocumento;
	private $namearchivo;
	private $codigobanco;

	private $login;
	private $clave;
	private $nombres;
	private $apellidos;
	private $fechanac;
	private $correo;
	private $codtipousuario;
	private $activo;
	private $cargo;
	private $area;
	private $descmotivo;
	private $producto;

	private $numserie;
	private $nclase;
    private $codclase;
    private $cliente;

	private	$TipoCliente;
	private $CustomerName;
	private $ShortName;
	private $StatementName;
	private $CustomerClass;
	private $LookupButton2;
	private $AddressCode;
	private $ContactPerson;
	private $Address1;
	private $Address2;
	private $Address3;
	private $City;
	private $State;
	private $Zip;
	private $CountryCode;
	private $Country;
	private $Phone1;
	private $Phone2;
	private $Phone3;
	private $Fax;
	private $UPSZone;
	private $ShippingMethod;
	private $TaxScheduleID;
	private	$ShipCompletDocument;
	private $PrimaryShiptoAddressCode;
	private $SalespersonID;
	private $LookupButton6;
	private $UserDefined1;
	private $UserDefined2;
	private $Comment1;
	private $Comment2;
	private $CustomerDiscount;
	private $PaymentTermsID;
	private $DiscountGracePeriod;
	private $DueDateGracePeriod;
	private $PriceLevel;
	private $AccountsButton;
	private $OptionsButton;
	private $TaxRegistrationNumber;
	private $CurrencyID;
	private $EmailStatementsToAddress;

//variables clientes enviado por banco
	private $Consecutivo;
	private $FechaRecepcionArchivo;
	private $PrefijoBanco;
	private $TipoPOS;
	private $cantpv;
	private $NAfiliacion;
	private $NombreAfiliado;
	private $Rif;
	private $ActividadEconomica;
	private $NombreRepresentanteLegal;
	private $CIRepresentanteLegal;
	private $RifRepresentanteLegal;
	private $CorreoElectronico;
	private $TelefonoMovil;
	private $Telefono1;
	private $Telefono2;
	private $CuentaRecaudadoraPOS;
	private $CalleAvenida;
	private $Localidad;
	private $Sector;
	private $Urbanizacion;
	private $Local;
	private $Estado;
	private $CodigoPostal;
	private $PuntodeReferencia;
	private $tipolinea;
	private $tipomodelonegocio;
	private $tipcliente;
	private $razonsocial;
	private $transferencia;

	//variables edicion gestionComercializacion
    private $gcOperacion;
	private $gcRcomercial_fantasia;
	private $gcNombre;
	private $gcApellido;
	private $gcInstituto_organismo;
	private $gcTlfmovil1;
	private $gcTlfmovil2;
	private $gcEmailcorpo;
	private $gcEmailcomer;
	private $gcEmailperso;
	private $gcSector;
	private $gcCactividad;
	private $gcFlujocaja;
	private $gcRotacioninv;
	private $gcDiasLabora;
	private $gcHorariocomer;
	private $gcSenalinalamb;
	private $gcModelo;
	private $gcCantidadpos;
	private $gcFormapago;
	private $gcTipocta;

	private $gccodetipodir3;
    private $gcd_calle_av3;
	private $gcd_localidad3;
	private $gcd_sector3;
	private $gcd_urbanizacion3;
	private $gcd_nlocal3;
	private $gcd_estado3;
	private $gcd_codepostal3;
	private $gcd_ptoref3;
    private $d_instalacion3;

	private $gccodetipodir2;
    private $gcd_calle_av2;
	private $gcd_localidad2;
	private $gcd_sector2;
	private $gcd_urbanizacion2;
	private $gcd_nlocal2;
	private $gcd_estado2;
	private $gcd_codepostal2;
	private $gcd_ptoref2;
	private $d_instalacion2;

	private $gccodetipodir1;
    private $gcd_calle_av1;
	private $gcd_localidad1;
	private $gcd_sector1;
	private $gcd_urbanizacion1;
	private $gcd_nlocal1;
	private $gcd_estado1;
	private $gcd_codepostal1;
	private $gcd_ptoref1;
	private $d_instalacion1;

	private $gcCodevendedor;
	private $gcNamevendedor;

	//gestion
	private $g_codegestion;
	private $g_nafiliado;
	private $g_codejecutivobanco;
	private $g_codejecutivointeligensa;
	private $g_estatuspos;
	private $g_observacion;
    //editdir
	private $afiliacion;
	private $codetipodir;
	private $e_calle_av;
	private $e_localidad;
	private $e_sector;
	private $e_nlocal;
	private $e_urbanizacion;
	private $e_estado;
	private $e_codepostal;
	private $e_ptoref;
	private $e_instalacion;

	private $Operacion;

	private $d_dirseleccionada;
	private $item;
	private	$e_prefijo;
	private	$e_fechacarga;

	private $id;
    private $id_prefijobanco;
	private $id_fechacarga;
	private $txthorario;
	private $campodelete;

	private $fechaenvio;
	private $fecharecepcion;
	private $interes;
	private $tipotarjeta;
	private $codmarcapos;
    private $id_estado;
    private $id_municipio;
    private $id_parroquia;

    private $lun;
    private $mar;
    private $mie;
    private $jue;
    private $vie;
    private $sab;
    private $dom;
    private $txtlunvie;
    private $txtsabdom;
    private $acteconomica;
    private $direccioninstalacion;
    private $cantposaprobados;
    private $nreferencia;
    private $bancoorigen;
    private $fechapago;
    private $horapago;
    private $formapago;
    private $fechafactura;
    private $nfactura;
    private $costopos;
    private $ejebanco;
    private $ejeinteligensa;
    private	$codtipovendedor;
    private $codestatus;
    private $observaciones;
    private $id_registro;
    private $fecha_desde;
    private $fecha_hasta;
    private $fechallamada;
    private $origen;

    private $tipousuario;
    private $ejecutivo;
    private $coordinador;
    private $coordinadordos;
    private $montocomision;
    private $montorecompra;
    private $serialresguardo1;
    private $busqueda;
	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function speliminar_tablatransclientep(){
		$sql="delete from clie_tbltransferencia";
		$this->con->consultaSimple($sql);
		return true;
	}
	public function sp_reloadregistro(){
		$sql="select * from sp_reloadregistro('{$this->id_registro}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_indicacontacto(){
		$sql="select * from sp_indicacontacto('{$this->id_registro}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function spverclientes(){
		$sql="select * from spverclientes()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
		public function sp_estatussolpos(){
		$sql="select * from sp_estatussolpos('{$this->Operacion}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

    //busqueda por rif o por razon social ANGULAR
    public function sp_comersharecustom(){
		$sql="select * from sp_comersharecustom('{$this->coddocumento}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_actividad(){
		$sql="select * from sp_actividad('{$this->id_registro}','{$this->activo}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_verafiliadocomercializacion(){
		// $sql="select * from sp_verafiliadocomercializacion('{$this->id_registro}') limit 1";
			$sql="select * from sp_verafiliadocomercializacion('{$this->id_registro}') limit 1";

		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_vernotificacionespago(){
		// $sql="select * from sp_verafiliadocomercializacion('{$this->id_registro}') limit 1";
			$sql="select * from sp_vernotificaciones({$this->id_registro})";

		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_verdescripcionmotivos(){
		// $sql="select * from sp_verafiliadocomercializacion('{$this->id_registro}') limit 1";
			$sql="select * from sp_verdescripcionmotivos('{$this->detalledesaprob}') limit 1";

		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	// 
	public function estatusconfirpagoss(){
		$sql="select * from estatusconfirpagos('{$this->id_registro}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
      //ver estatus gestion model
	public function sp_verestatusgestion(){
		$sql="select * from sp_verestatusgestion('{$this->Operacion}','{$this->id_registro}','{$this->namearchivo}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_verestatusgestioncontacto(){
		$sql="select * from sp_verestatusgestioncontacto('{$this->id_registro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function sp_verestatusscontacto(){
		$sql="select * from tblestatusllamada";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function editar(){
		$sql="select * from speditardatacomercializacion('{$this->NAfiliacion}','{$this->gcRcomercial_fantasia}','{$this->gcNombre}','{$this->gcApellido}','{$this->gcInstituto_organismo}','{$this->gcTlfmovil1}','{$this->gcTlfmovil2}','{$this->gcEmailcorpo}','{$this->gcEmailcomer}','{$this->gcEmailperso}','{$this->gcCactividad}','{$this->gcFlujocaja}','{$this->gcRotacioninv}','{$this->gcDiasLabora}','{$this->gcHorariocomer}','{$this->gcSenalinalamb}','{$this->gcModelo}','{$this->gcCantidadpos}','{$this->gcFormapago}','{$this->gcTipocta}','{$this->gccodetipodir3}','{$this->gcd_calle_av3}','{$this->gcd_localidad3}','{$this->gcd_sector3}','{$this->gcd_urbanizacion3}','{$this->gcd_nlocal3}','{$this->gcd_estado3}','{$this->gcd_codepostal3}','{$this->gcd_ptoref3}','{$this->d_instalacion3}', '{$this->gccodetipodir2}','{$this->gcd_calle_av2}','{$this->gcd_localidad2}','{$this->gcd_sector2}','{$this->gcd_urbanizacion2}','{$this->gcd_nlocal2}','{$this->gcd_estado2}','{$this->gcd_codepostal2}','{$this->gcd_ptoref2}','{$this->d_instalacion2}','{$this->gccodetipodir1}','{$this->gcd_calle_av1}','{$this->gcd_localidad1}','{$this->gcd_sector1}','{$this->gcd_urbanizacion1}','{$this->gcd_nlocal1}','{$this->gcd_estado1}','{$this->gcd_codepostal1}','{$this->gcd_ptoref1}','{$this->d_instalacion1}','{$this->d_dirseleccionada}')";

		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

      public function editardirinstalacion(){
        //hubo cambio de este "sp_editardirinstalacion" por este "sp_dirinstall" ?
		$sql="select * from sp_dirinstall('{$this->item}','{$this->afiliacion}','{$this->codetipodir}','{$this->e_calle_av}','{$this->e_localidad}','{$this->e_sector}','{$this->e_nlocal}','{$this->e_urbanizacion}','{$this->e_estado}','{$this->e_codepostal}','{$this->e_ptoref}','{$this->e_instalacion}')";

		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spvervendedor(){
		$sql="select * from spvervendedor('{$this->PrefijoBanco}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function spvendedorinteligensa(){
		$sql="select * from spvervendedorinteligensa('{$this->codtipovendedor}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

    public function sp_formapago(){
		$sql="select * from sp_formapago()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tienepos(){
			$sql="select * from sp_tienepos('{$this->id_registro}')";
			$resultado= $this->con->consultaRetorno($sql);
			return $resultado;
	}

	public function sp_insertargestion(){
			$sql="select * from sp_insertargestion('{$this->g_nafiliado}','{$this->g_codejecutivobanco}','{$this->g_codejecutivointeligensa}','{$this->g_estatuspos}','{$this->g_observacion}','{$this->coddocumento}','{$this->namearchivo}','{$this->usuario}','{$this->id_registro}','{$this->clientemigra}','{$this->clientecomodato}','{$this->clientvip}')";
			//echo $sql;
			$resultado= $this->con->consultaRetorno($sql);
			return $resultado;
	}

	public function sp_comerdatosbasicos(){
	$sql="select * from sp_comerdatosbasicos('{$this->coddocumento}','{$this->nombres}','{$this->apellidos}','{$this->institutouorganismo}','{$this->rcomercialofantasia}','{$this->namearchivo}')";
    $resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_comerdatoscontacto(){
	$sql="select * from sp_comerdatoscontacto('{$this->coddocumento}','{$this->gcTlfmovil1}','{$this->gcTlfmovil2}','{$this->gcEmailcorpo}','{$this->gcEmailcomer}','{$this->gcEmailperso}','{$this->namearchivo}','{$this->nombres}','{$this->apellidos}','{$this->direccion}','{$this->usuario}','{$this->gcTlfmovil3}','{$this->gcTlfmovil4}')";
    $resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function sp_comerdatoscaracteristicaempresa(){
	$sql="select * from sp_comerdatoscaracteristicaempresa('{$this->coddocumento}','{$this->namearchivo}','{$this->lun}','{$this->mar}','{$this->mie}','{$this->jue}','{$this->vie}','{$this->sab}','{$this->dom}','{$this->usuario}','{$this->txtlunvie}','{$this->txtsabdom}','{$this->acteconomica}','{$this->id_registro}','{$this->categoria}','{$this->subcategoria}')";
    $resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_comerdatoseleccionequipos(){
	$sql="select * from sp_comerdatoseleccionequipos('{$this->coddocumento}','{$this->gcModelo}','{$this->gcCantidadpos}','{$this->gcSenalinalamb}','{$this->gcTipocta}','{$this->gcFormapago}','{$this->namearchivo}')";
    $resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_equiposcostos(){
	$sql="select * from sp_equiposcostos('{$this->coddocumento}','{$this->namearchivo}','{$this->usuario}','{$this->direccion}','{$this->codmodelopos}','{$this->codmarcapos}','{$this->cantposaprobados}','{$this->tipolinea}','{$this->TipoPOS}')";
    //echo $sql;
    $resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}
	
	public function sp_equiposcostos1(){
	$sql="select * from sp_equiposcostos1('{$this->coddocumento}','{$this->namearchivo}','{$this->usuario}','{$this->direccion}','{$this->codmarcapos}','{$this->cantposaprobados}','{$this->tipolinea}','{$this->TipoPOS}')";
    $resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_indicainteres(){
	$sql="select * from sp_indicainteres('{$this->coddocumento}','{$this->namearchivo}','{$this->usuario}','{$this->id_registro}','{$this->interes}','{$this->NAfiliacion}',
	'{$this->ejebanco}','{$this->ejeinteligensa}')";
    $resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_caracteristicasdepago(){
	$sql="select * from sp_caracteristicasdepagos('{$this->coddocumento}','{$this->cliente}','{$this->namearchivo}','{$this->usuario}','{$this->nafiliacion}','{$this->totalprecio}','{$this->ejebanco}','{$this->estatuspos1}','{$this->id_registro}','{$this->totalaprobados}','{$this->modventa}','{$this->fechaipago}','{$this->anticipomonto}','{$this->descuentomonto}','{$this->inicialmonto}','{$this->coordaprobacion}','{$this->observacionpago}', '{$this->montocomision}','{$this->montorecompra}','{$this->serialresguardo1}','{$this->precioactual}')";
		//echo $sql;
    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_banco(){
		$sql="select * from sp_banco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_perfilgeodelbanco(){
		$sql="select * from sp_perfilgeodelbanco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function sp_tipocuenta(){
		$sql="select * from sp_tipocuenta()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_actividadeconomica(){
		$sql="select * from spveractividadeconomica()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_verestado(){
		$sql="select * from sp_verestado()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_ciudad(){
		$sql="select * from sp_ciudad('{$this->id_estado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_municipio(){
		$sql="select * from sp_municipio('{$this->id_estado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_municipioselected(){
		$sql="select * from sp_municipioselected('{$this->id_municipio}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_parroquia(){
		$sql="select * from sp_parroquias('{$this->id_municipio}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_parroquiaselected(){
		$sql="select * from sp_parroquiaselected('{$this->id_parroquia}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_estatusinteresado(){
		$sql="select * from sp_estatusinteresado()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tipolinea(){
		$sql="select * from sp_tipolinea()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_proveedorsimcard(){
		$sql="select * from sp_proveedorsincard('{$this->tipolinea}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tipotarjeta(){
		$sql="select * from sp_tipotarjeta()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tipotarjetaselected(){
		$sql="select * from sp_tipotarjetaselected('{$this->tipotarjeta}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_marcapos(){
		$sql="select * from sp_marcapos('{$this->codmodelopos}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_tipopos(){
		$sql="select * from sp_tipopos()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_modelopos(){
		$sql="select * from sp_modelopos()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_conexionpos(){
		$sql="select * from sp_conexionpos('{$this->codmodelopos}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_bancoasignar(){
		$sql="select * from sp_bancoasignar()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_costopos(){
		$sql="select * from sp_costopos('{$this->TipoPOS}','{$this->codmarcapos}','{$this->id_registro}')";
		 //echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_listbanco(){
		$sql="select * from sp_listbanco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_dirinstalacioneditada(){
		$sql="select * from sp_dirinstalacioneditada('{$this->coddocumento}','{$this->namearchivo}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function direccionfiscal(){
		$sql="select * from sp_direccionfiscal('{$this->coddocumento}','{$this->codetipodir}','{$this->e_calle_av}','{$this->e_localidad}','{$this->e_sector}','{$this->e_nlocal}','{$this->e_urbanizacion}','{$this->e_estado}','{$this->e_codepostal}','{$this->e_ptoref}','{$this->e_fechacarga}','{$this->e_prefijo}','{$this->namearchivo}','{$this->usuario}','{$this->id_registro}')";
		$resultado= $this->con->consultaRetorno($sql);
	    return $resultado;
     }
      public function direccioncomercial(){
		$sql="select * from sp_direccioncomercial('{$this->coddocumento}','{$this->codetipodir}','{$this->e_calle_av}','{$this->e_localidad}','{$this->e_sector}','{$this->e_nlocal}','{$this->e_urbanizacion}','{$this->e_estado}','{$this->e_codepostal}','{$this->e_ptoref}','{$this->e_fechacarga}','{$this->e_prefijo}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
	    return $resultado;
     }

       public function direccionevento(){
		$sql="select * from sp_direccionevento('{$this->coddocumento}','{$this->codetipodir}','{$this->e_calle_av}','{$this->e_localidad}','{$this->e_sector}','{$this->e_nlocal}','{$this->e_urbanizacion}','{$this->e_estado}','{$this->e_codepostal}','{$this->e_ptoref}','{$this->e_fechacarga}','{$this->e_prefijo}','{$this->namearchivo}')";
		$resultado= $this->con->consultaRetorno($sql);
	    return $resultado;
     }
     //
     public function direccioninstalacion(){
       $sql="select * from sp_editadireccioninstalacion('{$this->coddocumento}','{$this->codetipodir}','{$this->e_calle_av}','{$this->e_localidad}','{$this->e_sector}','{$this->e_nlocal}','{$this->e_urbanizacion}','{$this->e_estado}','{$this->e_codepostal}','{$this->e_ptoref}','{$this->e_fechacarga}','{$this->e_prefijo}','{$this->namearchivo}','{$this->usuario}','{$this->id_registro}')";
        $resultado= $this->con->consultaRetorno($sql);
        return $resultado;
     }

	public function sp_buscarclientecomercializacion(){
	$sql="select * from spbuscarclienterecibido('{$this->cliente}','{$this->FechaRecepcionArchivo}','{$this->origen}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarclientecomercializacionRif(){
	$sql="select * from spbuscarclienterif('{$this->Rif}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_registrosactivos(){
	$sql="select * from sp_registrosactivos('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verclientecomercializacionbuscar()
	{
	$sql="select * from spverclientepotencialbuscar('{$this->PrefijoBanco}','{$this->FechaRecepcionArchivo}','{$this->namearchivo}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);

	return $resultado;
	}

	public function sp_vistadireccioninstalacioneditada(){
		$sql="select * from sp_vistadireccioninstalacioneditada('{$this->coddocumento}','{$this->e_prefijo}','{$this->namearchivo}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function ProcesarInformacionComercializacion(){
		$sql="select * from spprocesarinformacioncomercializacion('{$this->Operacion}','{$this->cliente}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_vercostomantenimiento(){
		$sql="select * from sp_vercostomantenimiento('{$this->cliente}','{$this->coddocumento}','{$this->namearchivo}','{$this->id_registro}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spbanco(){
		$sql="select * from sp_banco('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function comercializacionreportedetallado()
	{
		if (($this->fechaenvio=="") && ($this->fecharecepcion==""))
		{
		$sql="select * from spcomercializacionreportedetallado('{$this->PrefijoBanco}',NULL,NULL)";
		}
		if (($this->fechaenvio!="") && ($this->fecharecepcion==""))
		{
		$sql="select * from spcomercializacionreportedetallado('{$this->PrefijoBanco}','{$this->fechaenvio}',null)";
		}
		if (($this->fechaenvio!="") && ($this->fecharecepcion!=""))
		{
		$sql="select * from spcomercializacionreportedetallado('{$this->PrefijoBanco}','{$this->fechaenvio}','{$this->fecharecepcion}')";
		}
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function insertar_afiliado()
	{
		$sql="select * from spinsertarafiliado('{$this->Consecutivo}','{$this->fechaenvio}','{$this->cliente}','{$this->PrefijoBanco}','{$this->TipoPOS}','{$this->cantpv}','{$this->tipolinea}','{$this->tipomodelonegocio}','{$this->tipcliente}','{$this->razonsocial}','{$this->Rif}','{$this->gcFormapago}','{$this->transferencia}','{$this->NombreAfiliado}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function verclientescomercializacion()
	{
		$sql="select * from spverclientescomercializacion()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

//recordar
	public function spinsertardocumentos (){
		$sql="select * from spinsertardocumentos('{$this->idregistro}','{$this->directorio}','{$this->afiliado}','{$this->usuario}','{$this->eexp}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
	    return $resultado;
	}
//
//recordar
public function spinsertarhistorial (){
	$sql="select * from spinsertarhistorial('{$this->idregistro}','{$this->afiliado}','{$this->usuario}','{$this->mensaje}','{$this->detalles}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
}
//
public function sp_reporteriesgomedidoadpins2 (){
	$sql="select * from sp_reporteriesgomedidoadpinscomercial ('{$this->id_registro}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
 }
//  
	public function verafiliadosasignados()
	{
		$sql="select * from spverafiliadosasignados()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function verafiliadosasignadosdetalle()
	{
		$sql="select * from spverafiliadosasignados() where afiliado='{$this->NombreAfiliado}'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function sp_interesmantenimiento()
	{
		$sql="select * from sp_interesmantenimiento('{$this->coddocumento}','{$this->namearchivo}','{$this->usuario}','{$this->direccion}','{$this->interes}')";
	    $resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_opcionmantenimiento(){
		$sql="select * from sp_opcionmantenimiento()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_guardarnafiliado(){
		$sql="select * from sp_guardarnafiliado('{$this->id_registro}','{$this->nafiliacion}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_guardarbanco(){
		$sql="select * from sp_guardarbanco('{$this->id_registro}','{$this->codigobanco}','{$this->usuario}')"; 
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_agendallamada(){
		$sql="select * from sp_agendallamada('{$this->id_registro}','{$this->fechallamada}','{$this->g_nafiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_graficacontactomensual(){
		$sql="select * from sp_graficacontactomensual('{$this->usuario}','{$this->fecha_desde}','{$this->fecha_hasta}')";
		// echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_graficacontacto(){
		$sql="select * from sp_graficacontacto('{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	//cantidad de registros por bancpo y por año
	public function sp_graficaregistrosxanho(){
		$sql="select * from sp_graficaregistrosxanho()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
// 	//cantidad de registros no contacto por anhio
// 	public function sp_nocontactografica(){
// 		$sql="select  fecharecepcion::date as year,count(estatus_contacto) as value from clie_tblclientepotencial  where estatus_contacto=4 and usuarioedita='JUNIORS' group by fecharecepcion order by fecharecepcion asc
// ";
// 		$resultado=$this->con->consultaRetorno($sql);
// 		return $resultado;
// 	}
		//cantidad de registros no contacto por anhio
		public function sp_nocontactografica(){
			$sql="select  fecharecepcion::date as year,count(estatus_contacto) as value from clie_tblclientepotencial  where estatus_contacto=4
 and usuarioedita='{$this->usuario}' group by fecharecepcion order by fecharecepcion asc";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		//cantidad de registros por bancpo y por año
		public function sp_graficarbancoanio(){
			$sql="select  * from sp_graficageneralbanco()";
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

	public function spverbancoseje()
	{
	$sql="select * from spverbancoseje('{$this->valor}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverfechaxbancoeje()
	{
	$sql="select * from spverfechaxbancoeje('{$this->valor}','{$this->PrefijoBanco}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverfechaeje()
	{
	$sql="select * from spverfechaseje('{$this->valor}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function spverbancoxfechaeje()
	{
	$sql="select * from spverbancoxfechaeje('{$this->valor}','{$this->FechaRecepcionArchivo}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverbancosdefeje()
	{
	$sql="select * from spverbancoseje('{$this->valor}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}','{$this->coordinador}','{$this->coordinadordos}') where codigobanco::integer='{$this->cliente}' ";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spver_bancosxejecutivo()
	{
	$sql="select * from spver_bancosxejecutivo('{$this->valor}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}')";
	$resultado=$this->con->consultaRetorno($sql);
	//echo $sql;
	return $resultado;
	}

	public function spbancoejec()
	{
	$sql="select * from spver_bancosxejecutivo('{$this->valor}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}') where codigobanco::integer='{$this->cliente}' ";
	$resultado=$this->con->consultaRetorno($sql);
	//echo $sql;
	return $resultado;
	}

	public function sp_verejecutivobancos()
	{
	$sql="select * from sp_verejecutivobancos('{$this->valor}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_verejecutivobco()
	{
	$sql="select * from sp_verejecutivobco('{$this->valor}','{$this->PrefijoBanco}','{$this->tipousuario}','{$this->ejecutivo}','{$this->usuario}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

		public function sp_bancoejecutivo()
	{
	$sql="select * from ejecutivobanco('{$this->valor}','{$this->ejecutivo}','{$this->tipousuario}','{$this->ejecutivobco}','{$this->usuario}')";
	$resultado=$this->con->consultaRetorno($sql);
	//echo $sql;
	return $resultado;
	}

		public function sp_verestatusequipo(){
		$sql="select * from sp_verestatusequipocomercializacion('{$this->id_registro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


			public function sp_verestatusbanesco(){
		$sql="select * from sp_verestatusbanesco('{$this->id_registro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_verexpediente(){
		$sql="select * from sp_verexpediente('{$this->coddocumento}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function sp_modificardatos(){
		$sql="select * from sp_modificardatos('{$this->id_registro}','{$this->serialp}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_modificarbancoactual(){
		$sql="select * from sp_modificarbancoactual('{$this->id_registro}','{$this->codigobanco}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
	}

	public function sp_modalidadventa(){
		$sql="select * from sp_modalidadventa()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_verestatuscomodato(){
		$sql="select * from sp_verestatuscomodato('{$this->id_registro}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_coodregion(){
		$sql="select * from tbl_coordregion where estatus='1'";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
	}

	public function sp_vermodalidadpagos(){
	$sql="select * from tbl_registromodalidadpago where id_cliente='{$this->id_registro}'";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function mostrarseriales(){
	$sql="select * from sp_serialesdisponibles1('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

		public function sp_verserialesrecompra(){
		$sql="select * from sp_verserialesrecompra('{$this->id_registro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarserialresg(){
		$sql="select * from tblserialpos sp
		left join tbl_seriales_recompra sr
		on sr.serialrecompra=sp.serial
		where sp.estatus='3' and sr.serialrecompra is null ";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
	}
	public function sp_guardarstatuscomodato(){
		$sql="select * from sp_guardarstatuscomodato('{$this->id_registro}','{$this->status}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarserialresgu(){
		$sql="select * from tblserialpos sp
		where sp.serial='{$this->busqueda}'";
		$resultado=$this->con->consultaRetorno($sql);
		//echo $sql;
		return $resultado;
	}

		// 
		public function spverclientesbancos()
		{
		$sql="select * from spvercliente()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
	
		// 
		public function spverclientestbancos()
		{
		$sql="select * from spvertodoclientes()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
	
		// 
		public function formadepago()
		{
		$sql="select * from sp_formapago()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		
	public function sp_tasacambioxfecha (){
		$sql="select * from sp_tasacambioxfecha('{$this->fechapago}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	// 
	
	public function sp_verpagos(){
		$sql="select * from sp_verpagos('{$this->idregistro}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function mostrarClienteFacturacion(){
	$sql="select * from spmostrarClienteFacturacion('{$this->id_registro}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}	

		public function sp_categoriacomercio(){
		$sql="select * from spvercategoriacomercio()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	public function sp_versubcategoriacomercio(){
		$sql="select * from sp_versubcategoriacomercio('{$this->id_categ}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function sp_subcategselected(){
		$sql="select * from sp_subcategselected('{$this->isubcategoriaselected}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function sp_agregargestioncontacto(){
		$sql="select * from sp_agregargestioncontacto('{$this->id_registro}','{$this->fechaEstatusGest}','{$this->selectEstatusGest}','{$this->observacionesEstatusGest}','{$this->usuario}')";
		echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
}

?>
