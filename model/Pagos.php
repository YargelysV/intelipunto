<?php
include_once('Conexion.php');

	class Pagos
	{
		private $operacion;
		private $idafiliado;
		private $bancoorigen;
		private $bancodestino;
		private $formapago;
		private $moneda; 
        private $monto;
        private $depositante;
        private $fechapago;
        private $horapago;
        private $capture;
        private $observacion;
        private $usuariocarga;
        private $estatus;
        private $usuario;
        private $montofactura;
        private $numerofactura;
        private $fechafactura;
        private $observacioncomer;
        private $observacionadm;
        private $montousd;
        private $factorconversion;
        private $serialmf;
        private $serialp;
        private $serialdev;
        private $consecutivo;
        private $idestatus;
        private $serialrecompra;
        private $montorecompra;
        private $confirmacionrecom;


		public function __construct()
		{
		$this->con=new Conexion();
		}

		public function set($atributo,$contenido)
		{
		$this->$atributo=$contenido;
		}

		public function get ($atributo)
		{
		return $this->$atributo;
		}

		public function sp_verpagosdemifi(){
		$sql="select * from sp_verpagosdemifi('{$this->idafiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
		
		public function sp_crudpago()
		{
		$sql2="select * from sp_crudpago('{$this->operacion}','{$this->idafiliado}','{$this->fechapago}','{$this->horapago}','{$this->bancoorigen}','{$this->bancodestino}','{$this->formapago}','{$this->moneda}','{$this->monto}','{$this->montousd}','{$this->factorconversion}','{$this->referencia}','{$this->depositante}','{$this->capture}','{$this->observacioncomer}','{$this->observacionadm}','{$this->usuariocarga}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}
		

		public function editarPago()
		{
		$sql2="select * from sp_edittarpagos('{$this->idafiliado}','{$this->fechapago}','{$this->horapago}','{$this->bancoorigen}','{$this->bancodestino}','{$this->formapago}','{$this->moneda}','{$this->monto}','{$this->montousd}','{$this->factorconversion}','{$this->referencia}','{$this->depositante}','{$this->capture}','{$this->observacioncomer}','{$this->observacionadm}','{$this->usuariocarga}','{$this->id}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}


		public function updatePago()
		{
		$sql2="select * from sp_edittarpagos('{$this->idafiliado}','{$this->fechapago}','{$this->horapago}','{$this->bancoorigen}','{$this->bancodestino}','{$this->formapago}','{$this->moneda}','{$this->monto}','{$this->montousd}','{$this->factorconversion}','{$this->referencia}','{$this->depositante}','{$this->capture}','{$this->observacioncomer}','{$this->observacionadm}','{$this->usuariocarga}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}


		public function eliminarPago(){
		$sql="select * from sp_eliminarpago('{$this->referencia}','{$this->idafiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function eliminarPagoMifi(){
		$sql="select * from sp_eliminarpagomifi('{$this->referencia}','{$this->idafiliado}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_updateStatusPago(){
		$sql="select * from sp_UpdateStatusPago('{$this->idafiliado}','{$this->referencia}','{$this->estatus}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
		public function sp_updateStatusPagoMifi(){
		$sql="select * from sp_UpdateStatusPagoMifi('{$this->idafiliado}','{$this->referencia}','{$this->estatus}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
		public function sp_estatuspagos(){
		$sql="select * from sp_estatuspagos('{$this->idafiliado}','{$this->estatus}')";
		$resultado=$this->con->consultaRetorno($sql);
		// return $resultado;
		$num=pg_num_rows($resultado);
		echo $num;
		if($num!=0){
			return 1;
		}else
		return 0;
		
		}

		public function sp_estatuspagosmifi(){
		$sql="select * from sp_estatuspagosmifi ('{$this->idafiliado}','{$this->estatus}')";
		$resultado=$this->con->consultaRetorno($sql);
		// return $resultado;
		$num=pg_num_rows($resultado);
		echo $num;
		if($num!=0){
			return 1;
		}else
		return 0;
		
		}

		public function sp_updateEstatusFact(){
		$sql="select * from sp_updateEstatusFact('{$this->idafiliado}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_updateEstatusFactMifi(){
		$sql="select * from sp_updateEstatusFactMifi('{$this->idafiliado}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_cargarpagosmifi()
		{
        $sql2="select * from sp_pagorepetidomifi('{$this->idafiliado}','{$this->referencia}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);
         //echo $num;
		if($num!=0){
			return false;
		}
		else{
		$sql2="select * from sp_cargarpagosmifi('{$this->operacion}','{$this->idafiliado}','{$this->fechapago}','{$this->horapago}','{$this->bancoorigen}','{$this->bancodestino}','{$this->formapago}','{$this->moneda}','{$this->monto}','{$this->montousd}','{$this->factorconversion}','{$this->referencia}','{$this->depositante}','{$this->capture}','{$this->observacioncomer}','{$this->observacionadm}','{$this->usuariocarga}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}
		}

		public function editarPagoMifi()
		{
		$sql2="select * from sp_cargarpagosmifi('{$this->operacion}','{$this->idafiliado}','{$this->fechapago}','{$this->horapago}','{$this->bancoorigen}','{$this->bancodestino}','{$this->formapago}','{$this->moneda}','{$this->monto}','{$this->montousd}','{$this->factorconversion}','{$this->referencia}','{$this->depositante}','{$this->capture}','{$this->observacioncomer}','{$this->observacionadm}','{$this->usuariocarga}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}


		public function sp_guardarfactura()
		{
		$sql2="select * from sp_facturar('{$this->idregistro}','{$this->numerofactura}','{$this->fechafactura}','{$this->montofactura}','{$this->serial}','{$this->simcard}','{$this->serialm}','{$this->codigodestino}','{$this->usuario}','{$this->consecutivo}','{$this->idestatus}','{$this->cuentabanco}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function sp_guardarfacturamifi()
		{
		$sql2="select * from sp_facturarmifi('{$this->idregistro}','{$this->numerofactura}','{$this->fechafactura}','{$this->montofactura}','{$this->mifiserial}','{$this->codigodestino}','{$this->usuario}','{$this->consecutivo}','{$this->simserial}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function sp_guardaranulacion()
		{
		$sql2="select * from sp_guardaranulacion('{$this->idregistro}','{$this->numerofactura}','{$this->fechafactura}','{$this->montofactura}','{$this->usuario}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

			public function sp_guardaranulaciondemifi()
			{
		$sql2="select * from sp_guardaranulaciondemifi('{$this->idregistro}','{$this->numerofactura}','{$this->fechafactura}','{$this->montofactura}','{$this->usuario}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
			}
		public function sp_guardardevolucion()
		{
		$sql2="select * from sp_guardardevolucion('{$this->idregistro}','{$this->facturadev}','{$this->fechadev}','{$this->montodev}','{$this->serialdev}','{$this->simcarddev}','{$this->serialmdev}','{$this->codigomotivo}','{$this->usuario}','{$this->consecutivo}','{$this->observacion}','{$this->id_estatus}','{$this->idgestiones}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function sp_guardardevolucionesdemifi()
		{
		$sql2="select * from sp_guardardevolucionesdemifi('{$this->idregistro}','{$this->facturadev}','{$this->fechadev}','{$this->montodev}','{$this->serialmifidev}','{$this->devsimcard}','{$this->codigomotivo}','{$this->usuario}','{$this->consecutivo}','{$this->observacion}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

			public function sp_guardardeclinacionmifi()
		{
		$sql2="select * from sp_guardardeclinacionmifi('{$this->idregistro}','{$this->facturadevtotal}','{$this->fechadevtotal}','{$this->montodevtotal}','{$this->serialmifidec}','{$this->decsimcard}','{$this->codigomotivo}','{$this->usuario}','{$this->consecutivo}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function sp_guardardevoluciontotal()
		{
		$sql2="select * from sp_guardardevoluciontotal('{$this->idregistro}','{$this->facturadevtotal}','{$this->fechadevtotal}','{$this->montodevtotal}','{$this->serialdevtotal}','{$this->simcarddevtotal}','{$this->serialmdevtotal}','{$this->codigomotivo}','{$this->usuario}','{$this->consecutivo}')";
		echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function sp_guardarpercance()
		{
		$sql2="select * from sp_guardarpercance('{$this->idregistro}','{$this->serialperc}','{$this->simcardperc}','{$this->serialmifiperc}','{$this->codmotivo}','{$this->usuario}','{$this->consecutivo}','{$this->observacion}','{$this->id_estatusperc}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function sp_verpagos(){
		$sql="select * from sp_verpagos('{$this->idafiliado}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		public function sp_DeclinacionSinSeriales(){
		$sql="select * from sp_declinacionsinseriales('{$this->iidregistro}', '{$this->iusuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	

		public function sp_registrosporaprobar(){
		$sql="select * from sp_registrosporaprobar()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	
		public function sp_expedientesporaprobar(){
		$sql="select * from sp_expedientesporaprobar()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	
		// 
		public function sp_estatusRechazo(){
			$sql="select * from tbl_estatusrechazo";
			//echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}

		public function sp_subestatusRechazo(){
			$sql="select * from tbl_substatusrechazo where prefijorif='{$this->idprefijo}'";
			//echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}
		public function sp_guardaraprobacion(){
		$sql="select * from sp_guardaraprobacionbanesco('{$this->id_registro}','{$this->estatus}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	
		// tratar los dos
		public function sp_expaprobadobackof(){
		$sql="select * from sp_expaprobadobackof('{$this->iconsecutivo}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	
		public function sp_expdesaprobadobackof(){
		$sql="select * from sp_expdesaprobadobackof('{$this->iconsecutivo}','{$this->estatusdes}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	
		// 
		public function sp_expdocdesaprobadobackof(){
			$sql="select * from sp_expdocdesaprobadobackof('{$this->iconsecutivo}','{$this->observacion}','{$this->usuario}')";
			//echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
			}	
		// 
		public function sp_expdocdesaprobadobackofmotivado(){
			$sql="select * from sp_expdocdesaprobadobackof('{$this->iconsecutivo}','{$this->estatusdes}','{$this->usuario}')";
			echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
			}	
		public function sp_expdesaprobadobackofdocs(){
		$sql="select * from sp_expdesaprobadobackofdocs('{$this->iconsecutivo}','{$this->estatusdes}','{$this->estatusdesc}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	
		// 
		public function sp_pagosnoconf(){
		$sql="select * from sp_pagosnoconf()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	

		public function sp_guardarrecompra()
		{
		$sql2="select * from sp_guardarrerecompra('{$this->idregistro}','{$this->usuario}','{$this->serialrecompra}','{$this->montorecompra}','{$this->consecutivo}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		return $resultado;
		}

		public function sp_recompranoconf(){
		$sql="select id_cliente from tbl_seriales_recompra where estatus_conf_recompra='0'";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

		//recordar
		public function spinsertarhistorial (){
			$sql="select * from spinsertarhistorial('{$this->idregistro}','{$this->afiliado}','{$this->usuario}','{$this->mensaje}','{$this->detalles}')";
			//echo $sql;
			$resultado=$this->con->consultaRetorno($sql);
			return $resultado;
		}
		//
	}
?>	
