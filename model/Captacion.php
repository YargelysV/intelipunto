<?php
include_once('Conexion.php');

class Captacion
{

	//variables
	private $usuario;
	private $nombre;
	private $apellido;
	private $tlf;
	private $bancos;
    private $tipousuario;
    private $ejecutivo;
    private $operacion;
    private $idregistro;
    private $coddocumento;
    private $tlf1;
    private $razonsocial;
    private $bancointeres;
    private $ocodigobanco;
    private $cantposasignados;
    private $correorl;
    private $medio;
    private $direccion_instalacion;
    private $tipocliente;
    private $iejecutivo;
    private $banco2;
    private $banco1;


	//metodos
	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function sp_registrosasignacionejecutivo(){
	$sql="select * from sp_registrosasignacionejecutivo('{$this->ocodigobanco}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}
	public function sp_registroscaptacion(){
	$sql="select * from sp_registroscaptacion('{$this->usuario}')";
	//echo $sql; 
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function editarCaptacion(){ 
	$sql="select * from sp_editarcaptacion('{$this->operacion}','{$this->nombres}','{$this->apellidos}','{$this->documento}','{$this->afiliacion}','{$this->telefono}','{$this->razonsocial}','{$this->bancointeres}','{$this->bancoasignado}','{$this->cantidadposs}','{$this->email}','{$this->medio_contacto}','{$this->direccion}','{$this->tipocliente}','{$this->idregistro}')";
    $resultado= $this->con->consultaRetorno($sql);
     //echo $sql;
	return $resultado;
	}
	public function sp_editarAsignacionEjecutivo(){ 
	$sql="select * from sp_editarAsignacionEjecutivo('{$this->operacion}','{$this->iejecutivo}','{$this->idregistro}','{$this->nombre}','{$this->apellido}','{$this->coddocumento}','{$this->tlf1}','{$this->razonsocial}','{$this->bancointeres}','{$this->ocodigobanco}','{$this->cantposasignados}','{$this->correorl}','{$this->medio}','{$this->direccion_instalacion}','{$this->tipocliente}')";
    $resultado= $this->con->consultaRetorno($sql);
    // echo $sql;
	return $resultado;
	}

	public function  sp_clientesnuevos(){
	$sql="select * from sp_clientesnuevos('{$this->nombre}','{$this->apellido}','{$this->banco1}','{$this->rif}','{$this->telf}','{$this->observaciones}','{$this->usuario}','{$this->razon_social}','{$this->tipopos}','{$this->cantidadpos}','{$this->Correo}','{$this->Direccion}','{$this->medio_contacto}','{$this->tipo_cliente}','{$this->banco2}','{$this->n_afiliacion}','{$this->estatusaprob}')";
    $resultado= $this->con->consultaRetorno($sql);
    //echo $sql;
	return $resultado;
	}

	public function sp_bancosinteres(){
	$sql="select * from sp_listbanco()";
    $resultado= $this->con->consultaRetorno($sql);
  //  echo $sql;
	return $resultado;
	}

	public function sp_tipopos(){
	$sql="select * from sp_tipoposcaptacion('{$this->banco}')";
    $resultado= $this->con->consultaRetorno($sql);
  //  echo $sql;
	return $resultado;
	}

	public function sp_medio_contacto(){
	$sql="select * from sp_medio_contacto()";
    $resultado= $this->con->consultaRetorno($sql);
  //  echo $sql;
	return $resultado;
	}

	public function sp_tipo_cliente(){
	$sql="select * from sp_tipo_cliente()";
    $resultado= $this->con->consultaRetorno($sql);
  //  echo $sql;
	return $resultado;
	}

	public function  sp_validarduplicadoscaptacion(){
	$sql="select * from sp_validarduplicadoscaptacion('{$this->rif}','{$this->razon_social}','{$this->n_afiliacion}')";
    $resultado= $this->con->consultaRetorno($sql);
    //echo $sql;
	return $resultado;
	}
	
		public function spbuscarpersonasreferidas()
		{
		$sql="select * from sp_buscarpersonasreferidas('{$this->idcliente}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_ejecutivo_asignado(){
	$sql="select * from spmostrarejecutivoasignado()";
    $resultado= $this->con->consultaRetorno($sql);
  //  echo $sql;
	return $resultado;
	}

	public function editarejecutivoasignado(){ 
	$sql="select * from sp_editarasignacionejecutivo('{$this->operacion}','{$this->nombrecaptacion}','{$this->apellidocaptacion}','{$this->documentocaptacion}','{$this->afiliadocaptacion}','{$this->desc_ejecutivo}','{$this->telefonocaptacion}','{$this->razonsocialcaptacion}','{$this->mcontacto}','{$this->obancointeres}','{$this->asignarbanco}','{$this->usuario}','{$this->cantidadposscap}','{$this->emailcap}','{$this->direccioncap}','{$this->tcliente}','{$this->estatuscaptacion}','{$this->idregistro}')";
    $resultado= $this->con->consultaRetorno($sql);
     //echo $sql;
	return $resultado;
	}

	public function sp_registroscaptacionxaprobar(){
	$sql="select * from sp_registroscaptacion_xaprobar()";
    $resultado= $this->con->consultaRetorno($sql);
  //  echo $sql;
	return $resultado;
	}

	public function sp_estatusaprobacion(){
		$sql="select * from sp_updateaprobacioncaptacion('{$this->id_registro}','{$this->estatus}','{$this->usuario}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		
	}

}

?>



