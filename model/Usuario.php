

<?php
include_once('Conexion.php');
class Usuario
{

	//variables
	private $tipodoc;
	private $coddocumento;
	private $login;
	private $clave;
	private $nombres;
	private $apellidos;
	private $fechanac;
	private $correo;
	private $codtipousuario;
	private $activo;
	private $cargo;
	private $area;

	private $inicio;
	private $nroreg;

	private $con;
	private $cont;

	private $pass_cifrada;
	private $enlinea;
	private $usuariosession;
	private $descmotivo;

	private $buscar;
	private $modulo;
	private $codtipoacceso;
	private $estatus;
	private $usuarioadm;

	private $sid;
	private $getip;
	private $code;
	private $id_registro;
	private $usuario;
	private $region;

	//metodos
	public function __construct(){
		$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido;
	}

	public function get ($atributo){
		return $this->$atributo;
	}

	public function listar (){
		$sql="select * from tblUsuario order by usuario";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function crear(){

		$sql2="select spVerificarUsuarioRep ('{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);

		if($num!=0){
			return false;
		}
		else{
			$pass_cifrada=password_hash($this->clave, PASSWORD_DEFAULT, array("cost"=>12));

			$sql="select spUsuarioIA('3','{$this->tipodoc}','{$this->coddocumento}','{$this->login}','{$pass_cifrada}',
			'{$this->nombres}','{$this->apellidos}','{$this->fechanac}','{$this->correo}','{$this->codtipousuario}',
			'{$this->activo}','{$this->cargo}','{$this->area}','{$this->region}','{$this->usuario}')";
			//echo $sql;
			$this->con->consultaSimple($sql);

			return true;
		}
	}

	public function eliminar(){

		$sql2="select spverificarusuarioactivo ('{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);

		if($num!=0){
		return false;
		}
		else{

			$sql="select * from spUsuarioE('2','{$this->login}')";
			$this->con->consultaSimple($sql);
			return true;
		}
	}

	public function ver(){

			$sql="select * from spUsuarioS('1','{$this->login}')";
			//echo $sql;
			$resultado= $this->con->consultaRetorno($sql);
			$row=pg_fetch_assoc($resultado);

		//set a los atributos
		$this->tipodoc=$row['tipodoc'];
		$this->coddocumento=$row['coddocumento'];
		$this->login=$row['usuario'];
		$this->clave=$row['clave'];
		$this->nombres=$row['nombres'];
		$this->apellidos=$row['apellidos'];
		$this->fechanac=$row['fechanac'];
		$this->correo=$row['correo'];
		$this->codtipousuario=$row['codtipousuario'];
		$this->activo=$row['activo'];
		$this->cargo=$row['cargo'];
		$this->area=$row['area'];
		$this->descmotivo=$row['motivo'];
		$this->coordbanco=$row['coordbanco'];
		$this->region=$row['region'];
		return $row;
	}

	public function editar(){
		$sql2="select spVerificarEditarUsuario ('{$this->login}','{$this->nombres}','{$this->apellidos}','{$this->fechanac}','{$this->correo}','{$this->codtipousuario}','{$this->activo}','{$this->cargo}','{$this->area}','{$this->region}')";
		//echo $sql2;
		$resultado=$this->con->consultaRetorno($sql2);
		$num=0;
		//echo $num;

		if($num!=0){

			return false;
		}
		else{

			$sql="select spUsuarioIA('4','{$this->tipodoc}','{$this->coddocumento}','{$this->login}','{$this->clave}',
			'{$this->nombres}','{$this->apellidos}','{$this->fechanac}','{$this->correo}','{$this->codtipousuario}',
			'{$this->activo}','{$this->cargo}','{$this->area}','{$this->region}','{$this->usuario}')";

			$this->con->consultaSimple($sql);
			//echo $sql;
			return true;
			}
}

	public function verificar(){

		$sql="select * from verificausuario('{$this->login}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		$row=pg_fetch_array($resultado);

		if ($row['activo']=='2')
		{ //bloqueado
			$cont=4;
			return $cont;
		}
		elseif  ($row['activo']=='0')
		{ //inactivo
			$cont=5;
			return $cont;
		}
		elseif  ($row['codtipousuario']=='U')
		{ //inactivo
			$cont=6;
			return $cont;
		}
		else
		{

			if(password_verify($this->clave, $row['clave'])){

				if(password_verify('1234567', $row['clave']) || ($row['dif_dias']>='30') ){
         		$cont=1;
				return $cont;
         	  	}

           		$cont=2;
				return $cont;
				}

			elseif(isset($row['usuario']))
				{
				$cont=3;
				return $cont;
				}
			else
				{
				$cont=0;
				return $cont;
				}
		}
	}

/*	public function verificar(){
    $cont=2;
    return $cont;
	}*/

	public function usuarioenlinea(){

		$sql="select * from spusuarioenlinea('{$this->enlinea}','{$this->login}')";
		$this->con->consultaSimple($sql);
		return true;
	}

	public function vertipodocumentos(){

			$sql="select * from sptipodocumento()";
			$resultado= $this->con->consultaRetorno($sql);

			return $resultado;
	}

	public function vertipousuario(){

			$sql="select * from sptipousuario()";
			$resultado= $this->con->consultaRetorno($sql);

			return $resultado;
	}

	public function buscarusuario(){

			$sql="select * from spbuscarusuario('{$this->login}','{$this->inicio}','{$this->nroreg}')";
			$resultado= $this->con->consultaRetorno($sql);
			return $resultado;
	}

	public function reiniciar(){

		$sql2="select spverificarusuarioactivo ('{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_num_rows($resultado);

		if($num!=0){

			return false;
		}
		else{
			$pass_cifrada=password_hash($this->clave, PASSWORD_DEFAULT, array("cost"=>12));
			$sql="select * from spreiniciarclave('{$this->login}','{$pass_cifrada}')";
			$this->con->consultaSimple($sql);
			return true;
			}
	}


	public function spinsertarmotivo(){
		$sql="select * from spinsertarmotivo('{$this->login}','{$this->descmotivo}','{$this->usuariosession}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function spactualizarmotivo(){
		$sql="select * from spactualizarmotivo('{$this->login}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function mostrarmotivos(){
		$sql="select * from spmostrarmotivos('{$this->login}')";
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function cambiarpassword(){
		$sql2="select * from spvalidapassword ('{$this->login}','{$this->clave}')";
		$resultado=$this->con->consultaRetorno($sql2);
		$num=pg_fetch_row($resultado);
		//print_r($num[0]);
		$sql3="select * from splistaclaves('{$this->login}')";
		$resultado1=$this->con->consultaRetorno($sql3);
		$clavevieja=0;

		while ($num1=pg_fetch_array($resultado1)):
			if(password_verify($this->clave, $num1['clave']))
			{
			$clavevieja=1;
			}
		endwhile;

		if(($num[0]=='t') || ($clavevieja==1)){
	    return false;
		}
		else{

			$pass_cifrada=password_hash($this->clave, PASSWORD_DEFAULT, array("cost"=>12));
			$sql="select * from spcambiopassword('{$this->login}','{$pass_cifrada}')";
			$this->con->consultaSimple($sql);
			//echo "guardo clave";
			return true;
	    }
    }


	public function editarestatusmodulo(){

	$sql="select sp_editarstatusmodulo('{$this->login}','{$this->codtipoacceso}',
	'{$this->modulo}','{$this->usuarioadm}','{$this->status}')";
	$this->con->consultaSimple($sql);
	//echo $sql;
	return true;
	}


	public function editartipoacceso(){
	$sql="select * from speditartipoaccesomodulo('{$this->login}','{$this->codtipoacceso}','{$this->modulo}','{$this->usuario}','{$this->status}','{$this->codtipo}')";
	$this->con->consultaSimple($sql);
	//echo $sql;
	return true;
	}

	public function buscarmodulos(){
	$sql="select * from spbuscarmodulos('{$this->buscar}','{$this->inicio}','{$this->nroreg}','{$this->login}','{$this->codtipousuario}')";
	// echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function listarmodulos (){
	$sql="select * from splistarmodulos('{$this->login}','{$this->codtipousuario}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function splistaregion (){
	$sql="select * from sp_listaregiones()";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spareainteligensa(){
	$sql="select * from spareainteligensa()";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spregionesinteligensa(){
	$sql="select * from spregionesinteligensa()";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function buscarnombre(){
	$sql="select usuario from tblusuario";
	$resultado= $this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verificarcaptcha(){
	$sql="select * from tblcaptcha where sid='{$this->sid}' and ip='{$this->getip}' and code='{$this->code}'";
	//echo $sql;
	$resultado= $this->con->consultaRetorno($sql);
	$row=pg_fetch_array($resultado);
	//echo $row;
	if($row!=0){
			return true;
			}
	else
			return false;
	}

	public function cargarintentos(){
	$sql="select spcargarintentos(1,'{$this->login}')";
	$this->con->consultaSimple($sql);
	return true;
	}

	public function cargarintentosrenew(){
	$sql="select spcargarintentos(2,'{$this->login}')";
	$this->con->consultaSimple($sql);
	return true;
	}

	public function sp_resetactividad(){
	$sql="select sp_resetactividad('{$this->login}')";
	$this->con->consultaSimple($sql);
	return true;
	}

	public function desbloquear(){
	$pass_cifrada=password_hash($this->clave, PASSWORD_DEFAULT, array("cost"=>12));
	$sql="select * from spdesbloquearusuario('{$this->login}','{$pass_cifrada}')";
	$this->con->consultaSimple($sql);
	return true;
	}

	public function editaractividad(){
	$sql="select speditaractividad ('{$this->login}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_actividad(){
		$sql="select * from sp_actividad('{$this->id_registro}','{$this->activo}','{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_usuariologin(){
		$sql="select * from sp_usuariologin('{$this->usuario}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_buscarusuarios(){
		$sql="select * from sp_buscarusuarios()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_selectReporteSuc(){
		$sql="select * from sp_reportesucursal('{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_mostrarsucursal(){
		$sql="select * from sp_mostrarsucursal('{$this->idregion}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function sp_selectsucursalasig(){
		$sql="select * from sp_sucursal_asignada('{$this->idregion}','{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spguardarsucursal(){
		$sql="select * from spguardarsucursal('{$this->idregion}','{$this->valor}','{$this->login}','{$this->usuario_acceso}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spEliminarsucursalUsuario(){
		$sql="select * from sp_EliminarsucursalUsuario('{$this->idregion}','{$this->login}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function spguardar_regionadic(){
		$sql="select * from spguardar_regionadic('{$this->idregion}','{$this->valor}','{$this->login}','{$this->usuario_acceso}')";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

}
?>
