<?php
include_once('Conexion.php');

class ClientePotencial
{

	//variables
	private $buscar;
	private $inicio;
	private $nroreg;
	private $valorr;
	private $valor;
	private $usuario;
	private $tipodoc;
	private $coddocumento;
	private $login;
	private $clave;
	private $nombres;
	private $apellidos;
	private $fechanac;
	private $correo;
	private $codtipousuario;
	private $activo;
	private $cargo;
	private $area;
	private $descmotivo;
	private $producto;
	private $agencia;
	private	$nombrearchivo;
	private $numserie;

	private	$TipoCliente;
	private $CustomerName;
	private $ShortName;
	private $StatementName;
	private $CustomerClass;
	private $LookupButton2;
	private $AddressCode;
	private $ContactPerson;
	private $Address1;
	private $Address2;
	private $Address3;
	private $City;
	private $State;
	private $Zip;
	private $CountryCode;
	private $Country;
	private $Phone1;
	private $Phone2;
	private $Phone3;
	private $Fax;
	private $UPSZone;
	private $ShippingMethod;
	private $TaxScheduleID;
	private	$ShipCompletDocument;
	private $PrimaryShiptoAddressCode;
	private $SalespersonID;
	private $LookupButton6;
	private $UserDefined1;
	private $UserDefined2;
	private $Comment1;
	private $Comment2;
	private $CustomerDiscount;
	private $PaymentTermsID;
	private $DiscountGracePeriod;
	private $DueDateGracePeriod;
	private $PriceLevel;
	private $AccountsButton;
	private $OptionsButton;
	private $TaxRegistrationNumber;
	private $CurrencyID;
	private $EmailStatementsToAddress;

//variables clientes enviado por banco
	private $Consecutivo;
	private $FechaRecepcionArchivo;
	private $PrefijoBanco;
	private $TipoPOS;
	private $CantTerminales;
	private $TipoLinea;
	private $TipoModeloNegocio;
	private $NAfiliacion;
	private $NombreAfiliado;
	private $RazonComercial;
	private $Rif;
	private $ActividadEconomica;
	private $NombreRepresentanteLegal;
	private $CIRepresentanteLegal;
	private $RifRepresentanteLegal;
	private $CorreoElectronico;
	private $TelefonoMovil;
	private $Telefono1;
	private $Telefono2;
	private $CuentaRecaudadoraPOS;
	private $CalleAvenida;
	private $Localidad;
	private $Sector;
	private $Urbanizacion;
	private $Local;
	private $Estado;
	private $CodigoPostal;
	private $PuntodeReferencia;
	private $Direccion;
	private $Municipio;
	private $Parroquia;

	private $name;
	private $size;
	private $ejecutivo;
	private $ejecutivocall;
	private $fechacarga;

	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido)
	{
		$this->$atributo=$contenido;
	}

	public function get ($atributo)
	{
		return $this->$atributo;
	}

	public function speliminar_tablatransclientep()
	{
		$sql="delete from clie_tbltransferencia";
		$this->con->consultaSimple($sql);
		return true;
	}

	public function verprefijobanco()
	{
		$sql="select * from spverprefijobanco()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function veractividadeconomica()
	{
		$sql="select * from spveractividadeconomica()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function vertipodepos()
	{
		$sql="select * from spvertipopos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vertipodelinea()
	{
		$sql="select * from spvertipolinea()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function insertar_tabltranscliente()
	{
		$sql="select spinsertar_tabltranscliente('{$this->Consecutivo}','{$this->fechahoy}','{$this->PrefijoBanco}','{$this->TipoCliente}','{$this->CantTerminales}', '{$this->NombreAfiliado}', '{$this->RazonComercial}','{$this->Rif}','{$this->ActividadEconomica}','{$this->NombreRepresentanteLegal}','{$this->CorreoElectronico}','{$this->Telefono1}','{$this->Telefono2}', '{$this->Direccion}','{$this->Estado}','{$this->Municipio}','{$this->Parroquia}','{$this->agencia}','{$this->NAfiliacion}','{$this->ejecutivo}','{$this->usuario}','{$this->name}','{$this->size}','{$this->fechacarga}','{$this->bancoasignado}','{$this->Rifbanco}')";
		//echo $sql;

		
		// $this->con->consultaSimple($sql);
		// return true;
		$resultado= $this->con->consultaRetorno($sql);
	    $row=pg_fetch_array($resultado);
	    if ($row['spinsertar_tabltranscliente']=='f')
		{ //duplicado
			$r=0;
			return $r;
		}
		else{
			$r=1;
			return $r;
			// return true;
		}
	}

	public function vertablatransclientepotencial ()
	{
		$sql="select * from spvertablatransferenciacliepoten()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vertblverificacionarchivo ()
	{
		$sql="select * from vertblverificacionarchivo()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function verclientepotencialcargados ()
	{
		$sql="select * from spverclientepotencialcargados()";
	//	echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverificar_responsable()
	{
		$sql="select * from spverificar_responsable('{$this->cliente}')";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function adjuntartablatransclientepotencial()
	{
		$sql="select * from spadjuntartablaclientepotencial()";
		echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function eliminarduplicadoscargar()
	{
		$sql="select * from eliminarduplicadoscargar()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function eliminarduplicadosbd()
	{
		$sql="select * from eliminarduplicadosbd()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverbancos()
	{
	$sql="select * from spverbancos('{$this->valor}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spveralmacenequipo1()
	{
	$sql="select * from spversitioequipo('{$this->valor}', '{$this->region}') where codestatus not in (0,11)";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spveralmacenequipo()
	{
	$sql="select * from spversitioequipo('{$this->valor}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverestatusdemifi()
	{
	$sql="select * from spverestatusmifi('{$this->valor}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverbancosxejecutivo()
	{
	$sql="select * from spverbancosxejecutivo('{$this->usuario}','{$this->area}','{$this->codtipousuario}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spvernombrearchivobancos()
	{
	$sql="select * from spvernombrearchivobancos('{$this->banco}', '{$this->fecha}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverperfiles()
	{
	$sql="select * from spverperfiles('{$this->valor}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverbancosdef()
	{
	$sql="select * from spverbancos('{$this->valor}') where codigobanco::integer='{$this->cliente}' ";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverfechaxbanco()
	{
	$sql="select * from spverfechaxbanco('{$this->valor}','{$this->PrefijoBanco}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverfechaxbancoxregion()
	{
	$sql="select * from spverfechaxbancoxregion('{$this->valor}','{$this->PrefijoBanco}','{$this->region}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverbancoxfecha()
	{
	$sql="select * from spverbancoxfecha('{$this->valor}','{$this->FechaRecepcionArchivo}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}


	public function speliminararchivocliente()
	{
	$sql="select * from speliminararchivocliente('{$this->PrefijoBanco}','{$this->FechaRecepcionArchivo}','{$this->nombrearchivo}','{$this->codtipousuario}','{$this->login}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	$num=pg_fetch_row($resultado);
	//echo $num[0];
	return $num;
	}

	public function verclientepotencialbuscar()
	{
	$sql="select * from spverclientepotencialbuscardetalle('{$this->PrefijoBanco}','{$this->FechaRecepcionArchivo}','{$this->name}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verclientepotencial()
	{
	$sql="select * from spverclientepotencial('{$this->PrefijoBanco}','{$this->FechaRecepcionArchivo}', '{$this->name}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verbanco ()
	{
		$sql="select * from tblbanco where codigobanco::integer='{$this->PrefijoBanco}'";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function verarchivocargado ()
	{
		$sql="select * from spverarchivocargado('{$this->name}')";
		$resultado=$this->con->consultaRetorno($sql);
		$num=pg_num_rows($resultado);
		if($num==0)
			return false;
		else
			return true;
	}

	public function verafiliados()
	{
		$sql="select * from spverafiliados()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function verejecutivos()
	{
		$sql="select * from spverejecutivos()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}


	public function vertipodemodelo()
	{
		$sql="select * from spvertipodemodelo()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function vertipocliente()
	{
		$sql="select * from spvertipocliente()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

		public function verclienterepetidos()
	{
		$sql="select * from spverclientesactivos()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverbancosrestaurar()
	{
	$sql="select * from spverbancosrestaurar('{$this->valor}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
//	echo $sql;
	}

	public function spverfechaxbancorestaurar()
	{
	$sql="select * from spverfechaxbancorestaurar('{$this->valor}','{$this->PrefijoBanco}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spvernombreArchivorestaurar()
	{
	$sql="select * from spvernombreArchivorestaurar('{$this->banco}','{$this->FechaRecepcionArchivo}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sprestaurararchivocliente()
	{
	$sql="select * from sprestaurararchivocliente('{$this->PrefijoBanco}','{$this->FechaRecepcionArchivo}','{$this->login}','{$this->nombrearchivo}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	$num=pg_fetch_row($resultado);
	//echo $num[0];
	return $num;
	}

	public function sp_buscarclienterecibidos(){
	$sql="select * from spbuscarclienterecibido('{$this->banco}','{$this->FechaRecepcionArchivo}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function sp_buscarclienter(){
	$sql="select * from spbuscarclienter('{$this->cliente}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verclientecargados(){
	$sql="select * from spverclientecargados('{$this->PrefijoBanco}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverfecha()
	{
	$sql="select * from spverfechas('{$this->valor}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverejecutivo()
	{
	$sql="select * from spverejecutivo('{$this->valor}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverejecutivoxbanco()
	{
	$sql="select * from spverejecutivoxbanco('{$this->valor}','{$this->PrefijoBanco}')";
	//echo $sql;
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverejecutivobco()
	{
	$sql="select * from spverejecutivo('{$this->valor}') where codigobanco::integer='{$this->cliente}' ";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}	

	public function verestados()
	{
	$sql="select * from tblestados";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function vermunicipios()
	{
	$sql="select * from tblmunicipios";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verparroquia()
	{
	$sql="select * from tblparroquias";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function verificardireccion()
	{
	$sql="select * from sp_verificardireccion()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function spverbancoxejecutivo()
	{
	$sql="select * from spverbancoxejecutivo('{$this->valor}','{$this->ejecutivo}')";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function validador_duplicidad()
	{
	$sql="select * from validador_duplicidad()";
	$resultado=$this->con->consultaRetorno($sql);
	return $resultado;
	}

	public function eliminarduplicados2cargar()
	{
		$sql="select * from eliminarduplicados2cargar()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverbancoregionreporte()
		{
		$sql="select * from spverbancoregionreporte('{$this->valor}','{$this->region}')";
		//echo $sql;
		$resultado= $this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function spvermodalidad()
		{
		$sql="select * from tblmodalidadventa";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}
	public function spverestatusfacturacion()
		{
		$sql="select * from spverestatusfacturacion()";
		//echo $sql;
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function vercategoriacomercio()
		{
		$sql="select * from spvercategoriacomercio()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}

	public function versubcategoriacomercio()
		{
		$sql="select * from spversubcategoria()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}	

	public function verestatusdocumentos()
		{
		$sql="select * from verestatusdocumentosubi()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
		}


}
?>