<?php
include_once('Conexion.php');

class Desafiliacion
{
	
	//variables
	private $usuario;
	private $id;
	private $serial;
	private $rif;
	private $razon;
	private $adm;
	private $observacion;
	


	//metodos
	public function __construct(){
	$this->con=new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo=$contenido; 
	}

	public function get ($atributo){
		return $this->$atributo;     
	}


	public function insertar_datosmasivo(){
		$sql="select * from sp_insertardesafiliacionmasiva('{$this->id}','{$this->serial}','{$this->rif}','{$this->razon}','{$this->adm}','{$this->observacion}','{$this->usuario}' )";
		echo $sql;
		$this->con->consultaSimple($sql);
		return true;
	}

	public function verseriales (){
		$sql="select * from tblserialpos";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}

	public function spverclientesdesaf()
		{
		$sql="select * from spverclientesdesaf()";
		$resultado=$this->con->consultaRetorno($sql);
		return $resultado;
	}
	    
public function busquedamasiva(){
        $sql="select * from spverequiposdesafiliados('{$this->busqueda}')";
        $resultado=$this->con->consultaRetorno($sql);
        return $resultado;
    }
    
    public function busquedamasivaglobal(){
        $sql="select * from spverequiposdesafiliadosglobal()";
        $resultado=$this->con->consultaRetorno($sql);
        return $resultado;
    }

}

?>