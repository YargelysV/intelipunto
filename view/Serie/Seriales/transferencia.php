<?php
error_reporting(0);
set_time_limit(0);
if (isset($_SESSION["session_user"]))
{

	/*echo "total var:".$total."<br>";
	echo "name var:".$name."<br>";
	echo "size var:".$size."<br>";*/
	$type=$_FILES['archivo']['type'];				
	$tipo="application/vnd.ms-excel";

	//Buscar tipos de Pos
	$controladortipopos =new controladorAdjuntar();
	$tipopos=$controladortipopos->vertipodepos();
	$tip = 0;
	$opcionestipopos=array();
	while($pos = pg_fetch_array($tipopos)){
	$opcionestipopos[$tip] = $pos['tipopos'];
	$tip++;
	}

	$controladormarcapos =new controladorAdjuntar();
	$marcapos=$controladormarcapos->vermarcadepos();
	$marc = 0;
	$opcionesmarcapos=array();
	while($mpos = pg_fetch_array($marcapos)){
	$opcionesmarcapos[$marc] = $mpos['desc_modelo']; // revisar y modificar la tabla
	$marc++;
	}

	//BUSCAR TODOS LOS PREFIJOS DE LOS BANCOS
	$controladorbanco =new controladorAdjuntar();
	$prefijobanco=$controladorbanco->verprefijobanco();
	$pre = 0;
	$opcionesprefijo=array();
	while($prefijo = pg_fetch_array($prefijobanco)){
	$opcionesprefijo[$pre] = $prefijo['codigobanco'];
	//echo $opcionesprefijo[$pre]."<br>";
	$pre++;
	}

	//BUSCAR SI NUMERO DE SERIAL EXISTE
	$controladorafiliado =new controladorAdjuntar();
	$seriales=$controladorafiliado->verseriales();
	$serialesact = 0;
	$serialesactivos=array();
	while($serie = pg_fetch_array($seriales)){
	$serialesactivos[$serialesact] =$serie['serialcompleto'];
//	echo $serialesactivos[$serialesact]."<br>";
	$serialesact++;
	}

	$controladorconsec =new controladorAdjuntar();
	$consecutivo=$controladorconsec->verconsecutivo();
	$data=pg_fetch_assoc($consecutivo);
	
	$i=$data['ifechacarga'];
	$long=$data['longitud'];
	//echo $i;
	$hoy = date("dmY");
	//echo $hoy;
	$lote= date("Ymd");
	//echo $lote;
	$cont=0;

	
	if($lote===$i){

		$idlotes=$lote.'00'.$long+1;
		echo $idlotes;
	} 

	else{ 

		$idlotes=$lote.$cont.'01';

	} 


	if ($type==$tipo)
	{
		echo "<script>alertify.alert('Error Cargando el Archivo, Verifique e intente nuevamente.', function () {
			window.location.assign('Serie.php');});</script>";
	}
	else{
					$date = date("Y-n-j H:i:s");
					$archivo = $_FILES['archivo']['tmp_name'];
					$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
?>
	<div class="sub-content" align="center" >
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" id="errores">
<?php
					$cont=0;
					$control=0;
					while($data = fgetcsv($fichero,10000,";"))
					{
						$cont++;
						if($cont==1)
					
						{
							
								if ((utf8_encode($data[0])!="Proveedor") ||(utf8_encode($data[1])!="Fecha Carga") ||(utf8_encode($data[2])!="Marca POS") || (utf8_encode($data[3])!="Tipo de POS") ||(utf8_encode($data[4])!="Serial POS") ||(utf8_encode($data[5])!="Banco Asignado"))
									{
									echo "<script>alertify.alert('El orden del formato es incorrecto', function () {
									window.location.assign('Serie.php?cargar=adjuntar');});</script>";	
									$control=2;
									}
					
							
										
						}
							
						else if (($cont>1) && ($control!=2))
						
						{
							if((strlen($data[0]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo PROVEEDOR en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							if((strlen($data[1]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo FECHA DE COMPRA en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							if((strlen($data[2]) == '') || (!(in_array(($data[2]), $opcionesmarcapos))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Marca POS" en la linea '.$cont.' verifique la información e intente nuevamente</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

							if((strlen($data[3]) == '') || (!(in_array(($data[3]), $opcionestipopos))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Tipo de POS" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

							if((in_array(($data[4]), $serialesactivos))||(strlen($data[4]) == ''))
							{
								echo '<h4 style="color:red;">'.'ERROR  "Número de Serial de POS ya esta en nuestra base de datos ó el número de serial esta en blanco".<br> En la linea '.$cont.' verifique la información e intente nuevamente. </h4> 
									- Número de serial: '.$data[4].'<br><br>
									';
								$control = 1;
							}

							/*if((strlen($data[4]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo NÚMERO DE SERIE en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							*/
							if ($data[5]!= '') {
								// code...
							
								if(!(in_array(($data[5]), $opcionesprefijo))) // || (!(in_array(($data[5]), $opcionesprefijo)))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo "Banco Asignado" en la linea '.$cont.'</h4><br>';
								$control = 1;	
								$_SESSION["comprobar"]=$control;
								}
							}
						}		
					}	
	if($control==1)
	{
?>
	<table border="0" width="300" align="center">
				<tr>
					<td height="100" colspan="2" align="center">
					<font face="impact" size="5" color="red">
					Posibles Causas de error:<br>
					</font>
					</td>
				</tr>
					<td height="100" colspan="2" align="center">
					<font face="arial" size="4" color="green">
					- Verifique archivo .CSV<br>
					- Debe contener informaci&oacute;n en la primera columna.<br>
					- Ningun campo debe venir en blanco, y debe estar delimitado las columnas por punto y coma (;) .<br>
					- Consulte con el administrador del Sistema.<br>
					 </font>
					</td>
				</tr>
			<tr>
					<td height="100" colspan="1" align="center">
		<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='Serie?cargar=adjuntar'"/>
					</td>
				</tr>
		</table>
<?php
}
?>				</div>
			</div>
		</div>
	</div>				
<?php
					if($control == 0){
					$date = date("Y-n-j H:i:s");
					$archivo = $_FILES['archivo']['tmp_name'];
					$fichero1 = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
					$count=0;
					$contador=0;
					$controlador1 =new controladorAdjuntar();
					
					while($data1 = fgetcsv($fichero1,10000,";"))
					{
						$count++;
						if($count>1)
						{ 

						$insertar=$controlador1->insertar_tabltrans(trim($data1[0]),trim($data1[1]),trim($data1[2]),trim($data1[4]),trim($data1[3]),trim($data1[6]),trim($data1[5]),$_SESSION["session_user"],$idlotes);
						
					 } 
					}
					echo "<script>alertify.alert('Archivo Cargado con &Eacute;xito', function () {
					window.location.assign('Serie.php');});</script>";	
					}	


	}
}
?>