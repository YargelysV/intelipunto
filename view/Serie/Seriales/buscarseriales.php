<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $fecha= $var;
    $controladortrans =new controladorAdjuntar();
    $trans=$controladortrans->usuario($usuario);
    $row=pg_fetch_assoc($trans);
?>


 <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard x3" style="color:#F8F9F9;"> </i> SERIALES DE POS FECHA DE COMPRA: <b style="color:#F8F9F9;"><?php echo $fecha;?></b></a>
        </div>
       </nav>
    </div>
    <?php if (($row['area']=='2')|| ($_SESSION["superusuario"]=='A')) {?>
    <div align="left">
      <a  class="btn btn-success"   href="Descargas/SERIES<?php echo $var ?>.Mac" download="SERIES<?PHP echo $var ; ?>.Mac"> Generar Archivo Seriales POS(.Mac) </a>
    </div>
    <br>
    <?php } ?>
    <div class="row" align="center">
      <div class="col-12">
        <table class="table table-bordered"  id="serialpos" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>Proveedor</th>
                <th>Fecha Compra</th>
                <th>Marca POS</th>
                <th>Tipo de POS</th>
                <th>Serial de POS</th>
                <th>Estatus del equipo</th>
                <th>fecha carga</th>
                <th>usuario</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table>
      </div>
      <div class="row">
        <div>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='Serie.php'"/>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
  var fechadetalle= document.getElementById('fecha').value;
  
    $('table#serialpos').DataTable({
      "ajax": {
      "url": "api/SerialesPos.php",
            "type": "POST",
            "data": {"fechadetalle":fechadetalle}
      },
      "columns": [
        {"data": "correlativo", className: "text-center"},
        {"data": "iproveedor", className: "text-center"},
        {"data": "ifechacompra", className: "text-center"},
        {"data": "imarcapos", className: "text-center" },
        {"data": "tipopv", className: "text-center" },
        {"data": "serialpos", className: "text-center" },
        {"data": "estatusequipos", className: "text-center" },
        {"data": "fechac", className: "text-center" },
        {"data": "usuario", className: "text-center" }],
      "order" : [[0, "asc"]],

            "columnDefs": [
                   {
                         "targets": [ 7, 8 ],
                         "visible": false,
                     }
                 ],
            
            "scrollX": true,
            "scrollY": false,
            "info":     false,
            "scrollCollapse": false,
            
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
    }],



            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            age:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });

}
  </script>

