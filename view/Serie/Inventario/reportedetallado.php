<?php
  $usuario=$_SESSION["session_user"];

  $var=($_REQUEST['var']);
  $array =explode("/",$var);
  $fecha= $array[0];
  $proveedor = isset($array[1]) ? $array[1] : "";

  $controlador =new controladorAdjuntar();
  $reporte=$controlador->sp_reportedetalladopos($fecha);
  $row = pg_fetch_assoc($reporte);

?>

  <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
   <input type="hidden" id="proveedor"  name="proveedor" value="<?php echo $row['proveedor']; ?>"  />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#"  style="color:#F8F9F9;"><i class="fa fa-cubes"> </i> REPORTE DETALLADO DE COMPRA INVENTARIO: </a> 
           </div>
       </nav>
           <b style="color:#5DADE2;">Fecha:  <?php echo $fecha;?> 
           <br>Proveedor:  <?php echo $row['proveedor'];?>  </b>
           <br>
       
    </div>
    
    <div class="row" align="center">
      <div class="col-12">
        <table class="table table-bordered"  id="reporteserialdetalladofecha" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>Acción</th>
                <th>Marca de POS</th>
                <th>Tipo de POS</th>
                <th>Cantidad</th>
                <th>enlace</th>
                <th>Disponibles</th>
                <th>enlace</th>
                <th>Asignados</th>
                <th>Proveedor</th>
              </tr>
            </thead>
        </table>
      </div>
      <div class="row">
       
      </div>
    </div>
 <br>
    <div class="row">
        <div class="col-md-5 col-md-offset-5">
        <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='reporteinventario.php'"/>
        </div>
      </div>
    
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
    fecha=document.getElementById('fecha').value
    $('table#reporteserialdetalladofecha').DataTable({
      "ajax": {
      "url": "api/selectInventarioDetallado.php",
            "type": "POST",
            "data": {"fecha":fecha}
      },
      "columns": [
        {"data": "correlativo", className: "text-center"},
        {"data": "enlacedetalle", className: "text-center"},
        {"defaultContent":"<a class='detalle' type='href' title='Ver'>Ver Detalle</a>", className: "text-center"},
        {"data": "marca", className: "text-center"},
        {"data": "tipo", className: "text-center" },
        {"data": "total", className: "text-center" },
        {"data": "enlacedisponibles", className: "text-center"},
        {"data": "porasignar", className: "text-center", 
          "render" : function (data, type, full, meta, row) {
            if (data !=null) {
              return "<a class='porasignar' type='href'>"+data+"</a>";
            }else if (data==null || data===null){
              return "0";
            } 
          }
        },
        {"data": "enlaceasignados", className: "text-center" },
        {"data": "asignados", className: "text-center", 
          "render" : function (data, type, full, meta, row) {
            if (data !==null) {
              return "<a class='asignados' type='href'>"+data+"</a>";
            }else if (data==null || data===null){
              return "0";
            } 
          } 
        },
        {"data": "proveedor", className: "text-center" }],
      "order" : [[0, "asc"]],

            "columnDefs": [
                   {
                         "targets": [ 1,6,8 ],
                         "visible": false,
                     }
                 ],


            buttons: [
              'excel'
              
            ],
            
            dom: 'Bfrtip',
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });
  $('table#reporteserialdetalladofecha tbody').on( 'click', 'a.asignados', function () {
  //alert('algo');
    var table=$('table#reporteserialdetalladofecha').DataTable();
    var D =  table.row($(this).parents("tr")).data();
    var enlaceasignados=D.enlaceasignados;
    var url = "reporteinventario?cargar=reportegeneral&var="+enlaceasignados; 
    $(location).attr('href',url);

  });

  $('table#reporteserialdetalladofecha tbody').on( 'click', 'a.porasignar', function () {
  //alert('algo');
    var table=$('table#reporteserialdetalladofecha').DataTable();
    var D =  table.row($(this).parents("tr")).data();
    var enlacedisponibles=D.enlacedisponibles;
    var url = "reporteinventario?cargar=reportegeneral&var="+enlacedisponibles; 
    $(location).attr('href',url);

  });

  $('table#reporteserialdetalladofecha tbody').on( 'click', 'a.detalle', function () {
  //alert('algo');
    var table=$('table#reporteserialdetalladofecha').DataTable();
    var D =  table.row($(this).parents("tr")).data();
    var enlacedetalle=D.enlacedetalle;
    var url = "reporteinventario.php?cargar=reportegeneral&var="+enlacedetalle; 
    $(location).attr('href',url);

  });
}
</script>