<?php
  $usuario=$_SESSION["session_user"];

  $var=($_REQUEST['var']);
  $array =explode("/",$var);
  $fecha= $array[0];
  $marcapos= $array[1];
  $operacion= $array[2];
  $controlador =new controladorAdjuntar();
  $reporte=$controlador->sp_reportegeneralpos($fecha,$marcapos,$operacion);
  // $row = pg_fetch_assoc($reporte);

?>

  <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
  <input type="hidden" id="marcapos"  name="marcapos"  value="<?php echo $marcapos; ?>" />
    <input type="" id="operacion"  name="operacion"  value="<?php echo $operacion; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-cubes"> </i> REPORTE DETALLADO DE COMPRA INVENTARIO: </a>
        </div>
       </nav>
            <b style="color:#5DADE2;">Fecha:  <?php echo $fecha;?> </b> 
          <br>
       
    </div>
    
    <div class="row" align="center">
      <div class="col-12">
        <table class="table table-bordered"  id="reporteserialgeneralfecha" style="width: 100%"> 
           <thead>
              <tr>
                <th>N°</th>
                <th>Proveedor</th>
                <th>Serial POS</th>
                <th>Modelo&nbsp;de&nbsp;POS</th>
                <th>&nbsp;Serial&nbsp;&nbsp;Simcard&nbsp;</th>
                <th>&nbsp;Serial&nbsp;&nbsp;Mifi&nbsp;</th>
                <th>Nº Afiliado</th>
                <th>&nbsp;Rif&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Terminal</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table>
      </div>
      <div class="row">
       
      </div>
    </div>
 <br>
    <div class="row">
        <div class="col-md-5 col-md-offset-5">
        <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=reportedetallado&var=<?php echo $fecha;?>'"/>
        </div>
      </div>
    
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
    fecha=document.getElementById('fecha').value;
    marcapos=document.getElementById('marcapos').value;
    operacion=document.getElementById('operacion').value;
    $('table#reporteserialgeneralfecha').DataTable({
      "ajax": {
      "url": "api/selectInventarioGeneral.php",
            "type": "POST",
            "data": {"fecha":fecha, "marcapos":marcapos, "operacion":operacion}
      },
      "columns": [
        {"data": "correlativo", className: "text-center"},
        {"data": "proveedor", className: "text-center"},
        {"data": "seriall", className: "text-center"},
        {"data": "marcaposs", className: "text-center"},
        {"data": "simserial", className: "text-center" },
        {"data": "mifiserial", className: "text-center" },
        {"data": "afiliacion", className: "text-center"},
        {"data": "documento", className: "text-center" },
        {"data": "razon", className: "text-center" },
        {"data": "terminal", className: "text-center"},
        {"data": "bancoasig", className: "text-center" }],

      "order" : [[0, "asc"]],

        "scrollX": 2900,
        "scrollY": false,
        "info": false,
        "scrollCollapse": false,

        "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });

}
</script>


