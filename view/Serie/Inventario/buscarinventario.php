<?php
  $usuario=$_SESSION["session_user"];

    $controladortrans =new controladorAdjuntar();
    $trans=$controladortrans->usuario($usuario);
    $row=pg_fetch_assoc($trans);
    $controlador =new controladorAdjuntar();
    $cantidad= $controlador->sp_buscarinventariopos();
    $result=pg_num_rows($cantidad);


?>


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-cubes"> </i> REPORTE DE INVENTARIO POR FECHA DE COMPRA: <b style="color:#F8F9F9;"></b></a>
        </div>
       </nav>
    </div>
    
    <?php  if ($result > 0) {
     ?>
    
      <div class="row" align="center">
      <div class="col-12">
        <table class="table table-bordered"  id="reporteserialfecha" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>Acción</th>
                <th>Fecha Compra</th>
                <th>Cantidad de POS</th>
                <th>Disponibles</th>
                <th>Asignados</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table>
      </div>
      <div class="row">
      </div>
    </div>

    <?php  
      } else {
     ?>
    

    <div class="row" align="center">
      <div>
          <nav class="navbar navbar-default" style="background-color:#A3F62B;">
        <div> No existe inventario registrado en el Sistema</div>
          </nav>
      </div>

    </div>
      <div>
              <p style="text-align: center;">
              <input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='home'"/>  
              </p>
            </div>
      <?php  
      } 
     ?>
    
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
  
    $('table#reporteserialfecha').DataTable({
      "ajax": {
      "url": "api/selectInventarioPos.php",
            "type": "POST",
            "data": {}
      },
      "columns": [
        {"data": "correlativo", className: "text-center"},
        {"data": "enlace", className: "text-center"},
        {"defaultContent":"<a class='editar' type='href' title='Ver'>Ver Detalle</a>", className: "text-center"},
        {"data": "ifechacompra", className: "text-center"},
        {"data": "cantidad", className: "text-center" },
        {"data": "porasignar", className: "text-center" },
        {"data": "asignados", className: "text-center" }],
      "order" : [[0, "asc"]],

            "columnDefs": [
                   {
                         "targets": [ 1 ],
                         "visible": false,
                     }
                 ],

    "scrollX": 2900,
    "scrollY": false,
    "info": false,
    "scrollCollapse": false,               

           dom: 'Bfrtip', 
           buttons: [
              'excel'              
            ],
            
             "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });
    $('table#reporteserialfecha tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#reporteserialfecha').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "?cargar=reportedetallado&var="+enlace; 
        $(location).attr('href',url);

  });
}
</script>