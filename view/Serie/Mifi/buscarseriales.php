<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $fecha= $var;
    $controladortrans =new controladorAdjuntar();
    $trans=$controladortrans->usuario($usuario);
    $row=pg_fetch_assoc($trans);
?>


 <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard x3" style="color:#F8F9F9;"> </i> SERIALES DE MIFI FECHA DE COMPRA: <b style="color:#F8F9F9;"><?php echo $fecha;?></b></a>
        </div>
       </nav>
    </div>
    <?php if (($row['area']=='2')|| ($_SESSION["superusuario"]=='A')) {?>
     <div align="left">
      <a  class="btn btn-success"   href="Descargas/SERIES<?php echo $var ?>.Mac" download="SERIES<?PHP echo $var ; ?>.Mac"> Generar Archivo Seriales MIFI (.Mac) </a>
    </div>
    <br>
    <?php } ?>

    <div class="row" align="center">
      <div class="col-12">
        <table class="table table-bordered"  id="serialmifi" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>Proveedor</th>
                <th>Fecha Compra</th>
                <th>Tipo de MIFI</th>
                <th>Serial de MIFI</th>
                <th>Estatus del equipo</th>
                <th>fecha carga</th>
                <th>usuario</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table>
      </div>
      <div class="row">
        <div>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='Mifi.php'"/>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
  var fechadetalle= document.getElementById('fecha').value;
  
    $('table#serialmifi').DataTable({
      "ajax": {
      "url": "api/Mifi.php",
            "type": "POST",
            "data": {"fechadetalle":fechadetalle}
      },
      "columns": [
        {"data": "correlativo", className: "text-center"},
        {"data": "iproveedor", className: "text-center"},
        {"data": "ifechacompra", className: "text-center"},
        {"data": "tipomifi", className: "text-center" },
        {"data": "serialmifii", className: "text-center" },
        {"data": "estatusequipos", className: "text-center" },
        {"data": "fechac", className: "text-center" },
        {"data": "usuario", className: "text-center" }],
      "order" : [[0, "asc"]],

            "columnDefs": [
                   {
                         "targets": [ 6, 7 ],
                         "visible": false,
                     }
                 ],
    "scrollX": 2900,
    "scrollY": false,
    "info": false,
    "scrollCollapse": false,
    
 dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',  
            exportOptions : {
            columns: [ 0, 1, 2, 3, 4, 5]
      }          
    }],
            
            

            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });

}
</script>