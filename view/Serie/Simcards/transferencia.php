<?php
error_reporting(0);
set_time_limit(0);
if (isset($_SESSION["session_user"]))
{

	/*echo "total var:".$total."<br>";
	echo "name var:".$name."<br>";
	echo "size var:".$size."<br>";*/
	$type=$_FILES['archivo']['type'];				
	$tipo="application/vnd.ms-excel";

	//Buscar tipos de Pos
	$controladortipopos =new controladorAdjuntar();
	$tipolinea=$controladortipopos->vertipodelinea();
	$tip = 0;
	$opcionestipopos=array();
	while($pos = pg_fetch_array($tipolinea)){
	$opcionestipopos[$tip] = $pos['nameproveedor'];
	$tip++;
	}

	//BUSCAR TODOS LOS PREFIJOS DE LOS BANCOS
	$controladorbanco =new controladorAdjuntar();
	$prefijobanco=$controladorbanco->verprefijobanco();
	$pre = 0;
	$opcionesprefijo=array();
	while($prefijo = pg_fetch_array($prefijobanco)){
	$opcionesprefijo[$pre] = $prefijo['codigobanco'];
	//echo $opcionesprefijo[$pre]."<br>";
	$pre++;
	}

	//BUSCAR SI NUMERO DE SERIAL EXISTE
	$controladorafiliado =new controladorAdjuntar();
	$seriales=$controladorafiliado->verserialessimcards();
	$serialesact = 0;
	$serialesactivos=array();
	while($serie = pg_fetch_array($seriales)){
	$serialesactivos[$serialesact] =$serie['serialsim'];
//	echo $serialesactivos[$serialesact]."<br>";
	$serialesact++;
	}	

	if ($type==$tipo)
	{
		echo "<script>alertify.alert('Error Cargando el Archivo, Verifique e intente nuevamente.', function () {
			window.location.assign('Simcards.php');});</script>";
	}
	else{
					$date = date("Y-n-j H:i:s");
					$archivo = $_FILES['archivo']['tmp_name'];
					$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
?>
	<div class="sub-content" align="center" >
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" id="errores">
<?php
					$cont=0;
					$control=0;
					while($data = fgetcsv($fichero,10000,";"))
					{
						$cont++;
						if($cont==1)
					
						{
							/*echo utf8_encode($data[0])."<br>";
							echo utf8_encode($data[1])."<br>";
							echo utf8_encode($data[2])."<br>";
							echo utf8_encode($data[3])."<br>";
							echo utf8_encode($data[4])."<br>";
							echo utf8_encode($data[5])."<br>";
							echo utf8_encode($data[6])."<br>";*/
										if ((utf8_encode($data[0])!="Proveedor") ||(utf8_encode($data[1])!="Fecha Compra") ||(utf8_encode($data[2])!="Tipo de linea") ||(utf8_encode($data[3])!="Serial Sim"))
										{
										echo "<script>alertify.alert('El orden del formato es incorrecto', function () {
										window.location.assign('Simcards.php?cargar=adjuntar');});</script>";	
										$control=2;
										}
						}
							
						else if (($cont>1) && ($control!=2))
						
						{
							if((strlen($data[0]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo PROVEEDOR en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							if((strlen($data[1]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo FECHA DE COMPRA en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							if((strlen($data[2]) == '') || (!(in_array(($data[2]), $opcionestipopos))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Tipo de Línea" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

							if((in_array(($data[3]), $serialesactivos))||(strlen($data[3]) == ''))
							{
								echo '<h4 style="color:red;">'.'ERROR  "Número de Serial de Simcards ya esta en nuestra base de datos ó el número de serial esta en blanco".<br> En la linea '.$cont.' verifique la información e intente nuevamente. </h4> 
									- Número de serial: '.$data[3].'<br><br>
									';
								$control = 1;
							}

							/*if((strlen($data[4]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo NÚMERO DE SERIE en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							if((strlen($data[7]) == '') || (!(in_array(($data[7]), $opcionesprefijo))))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo "Prefijo del Banco" en la linea '.$cont.'</h4><br>';
							$control = 1;	
							$_SESSION["comprobar"]=$control;
							}
							*/
						}		
					}	
	if($control==1)
	{
?>
	<table border="0" width="300" align="center">
				<tr>
					<td height="100" colspan="2" align="center">
					<font face="impact" size="5" color="red">
					Posibles Causas de error:<br>
					</font>
					</td>
				</tr>
					<td height="100" colspan="2" align="center">
					<font face="arial" size="4" color="green">
					- Verifique archivo .CSV<br>
					- Debe contener informaci&oacute;n en la primera columna.<br>
					- Ningun campo debe venir en blanco, y debe estar delimitado las columnas por punto y coma (;) .<br>
					- Consulte con el administrador del Sistema.<br>
					 </font>
					</td>
				</tr>
			<tr>
					<td height="100" colspan="1" align="center">
		<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='Simcards?cargar=adjuntar'"/>
					</td>
				</tr>
		</table>
<?php
}
?>				</div>
			</div>
		</div>
	</div>				
<?php
					if($control == 0){
					$date = date("Y-n-j H:i:s");
					$archivo = $_FILES['archivo']['tmp_name'];
					$fichero1 = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
					$count=0;
					$controlador1 =new controladorAdjuntar();
					while($data1 = fgetcsv($fichero1,10000,";"))
					{
						$count++;
						if($count>1)
						{
							if ($data1[6]=='')
								 $data1[6]='0';

						$insertar=$controlador1->insertar_tablasimcards(trim($data1[0]),trim($data1[1]),trim($data1[3]),trim($data1[2]),$_SESSION["session_user"]);
						}
					}
					echo "<script>alertify.alert('Archivo Cargado con &Eacute;xito', function () {
					window.location.assign('Simcards.php');});</script>";	
					}	


	}
}
?>