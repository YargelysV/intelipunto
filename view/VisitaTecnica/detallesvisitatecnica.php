<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array =explode("/",$var);
  $idcliente= $array[0];
  $banco = $array[1];
  $serialpos = $array[2];
  $cliente = isset($array[3]) ? $array[3] : "";
  $idsecuencial = isset($array[4]) ? $array[4] : "";
  $busqueda = isset($array[5]) ? $array[5] : "";
  $fechav = isset($array[6]) ? $array[6] : "";

   $buscrecepcion= isset($_SESSION['bsqdarecepcion']  ) ? $_SESSION['bsqdarecepcion']  : "" ;
   $busscarrif= isset($_SESSION['buscariff']  ) ? $_SESSION['buscariff']  : "" ;

  //$buscarind= isset($_SESSION['buscarind']  ) ? $_SESSION['buscarind']  : "" ;

  $controlador =new controladorVisita();
  $servicio=$controlador->spverdetallevisitatecnica($idcliente, $serialpos);
  $row = pg_fetch_assoc($servicio);

  $controlador =new controladorVisita();
  $validplus=$controlador->spverdetallevisitatecnica1($idcliente, $serialpos);
  $validplus=pg_num_rows($validplus);

  $controlador =new controladorVisita();
  $codtecnico= $controlador->sp_mostrartecnicovisitatec($idcliente);
  $tec = pg_fetch_assoc($codtecnico);

  ?>

  
<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="fechav"  name="fechav"  value="<?php echo $fechav; ?>" />
<input type="hidden" id="razonsocial"  name="razonsocial"  value="<?php echo $row['razonsocial']; ?>">
<input class="hidden" id="coddocumento" name="coddocumento" value="<?php echo $row['coddocumento'];?>" readonly>
<input type="hidden" id="cbanco"  name="cbanco"  value="<?php echo $row['codigobanco']; ?>">
<input type="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">
<input type="hidden" id="idsecuencial" name="idsecuencial" value="<?php echo $idsecuencial; ?>">
<input type="hidden" id="idcliente"  name="idcliente"  value="<?php echo $idcliente; ?>" />
<input type="hidden" id="idvisita" name="idvisita" value="<?php echo $row['idvisita'];?>">
<input type="hidden" id="fechavisit" name="fechavisit" value="<?php echo $row['fechavisit'];?>">
<input type="hidden" id="busqueda" name="busqueda" value="<?php echo $busqueda;?>">
<input type="hidden" id="idtecnico"  name="tecnico"  value="<?php echo $tec['sp_mostrartecnicovisitatec'];?>" />
<input type="hidden" id="idresultado" name="idresultado" value="<?php echo $row['idresultado'];?>">
<input type="hidden" id="serialpos" name="serialpos" value="<?php echo $serialpos;?>">
<input type="hidden" id="validar" name="validar" value="<?php echo $validplus;?>">
<input type="text" class="hidden" id="prefijo" value="<?php echo $cliente; ?>">


<style type="text/css">
    
.error {
  width : 100%;
  padding: 0;

  font-size: 80%;
  color: white;
  background-color: #EC1717;
  border-radius: 0 0 5px 5px;

  box-sizing: border-box;
}

.error.active {
  padding: 0.3em;
}


</style>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> Visita Técnica
          </b></a>
         </div>
      </nav>
    </div>

 <div class="row" align="center"> 
     <div class="col-lg-10" align="left">
      <div class="panel panel-info">
                    <div class="panel-heading" style="text-align: center;">
                    Datos del Afiliado
                    </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
              <div class="list-group">
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> ID:
              <span class="pull-center text-muted small"><b><?php echo $row['idcliente'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Razón Social:
              <span class="pull-center text-muted small"><b><?php echo $row['razonsocial'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Documento:
              <span class="pull-center text-muted small"><b><?php echo $row['coddocumento'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Nº de Afiliacion:
              <span class="pull-center text-muted small"><b><?php echo $row['nafiliacion'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Banco:
              <span class="pull-center text-muted small"><b><?php echo $row['ibp'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Serial del Equipo:
              <span class="pull-center text-muted small"><b><?php echo $row['serialp'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Estatus del Equipo:
              <span class="pull-center text-muted small"><b><?php echo $row['descestatus'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Resultado de la Visita:
              <span class="pull-center text-muted small"><b><?php echo $row['descresult'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Estatus de la Visita:
              <span class="pull-center text-muted small"><b><?php echo $row['descvisita'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Información Adicional:
               <button type="button" class="btn btn-link" data-toggle="modal" id="btninf_adicional"
                data-target="#inf_adicional" value="Agregar">Agregar</button>
              </div>

              </div>

           </div>

       </div> 
               <div class="row" align="center"> 
                <div class="panel-heading" style="color:blue;">
                   Registro de Visitas Técnicas
                </div>
        <div class="col-10">
          <table class="table table-bordered"  id="tbl_visitatecnica" > 
             <thead>
                <tr>
                  <th>NRO</th>
                  <th>Fecha&nbsp;&nbsp;&nbsp;Carga</th>
                  <th>Serial&nbsp;&nbsp;de&nbsp;&nbsp;POS</th>
                  <th>Fecha&nbsp;&nbsp;Gestión</th>
                  <th>Estatus&nbsp;de&nbsp;la&nbsp;Visita</th>
                  <th>Condicion&nbsp;&nbsp;Visita</th>
                  <th>Nombre&nbsp;del&nbsp;Técnico</th>
                  <th>Fecha&nbsp;de&nbsp;Visita</th>
                  <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Observaciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                </tr>
              </thead>
          </table>
        </div>
        </div>

     </div>


  </div>
 <br>
           <div class="row" >
                   <div class="col-md-4 col-md-offset-4">
                  <?php if ($buscrecepcion=="" && $busscarrif!="" ){   ?> 
                   <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='?cargar=buscarregistrovisitaxrif&var=<?php echo $busscarrif; ?>'">Volver</button>
             <?php }  else {  ?>
                <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='?cargar=buscarregistrovisita&var=<?php echo $buscrecepcion; ?>'">  Volver</button> 
                  <?php  } ?> 
                 
                   <br>
                  </div>
               
              </div>


<div class="modal fade" id="inf_adicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header"  style="background-color:#63B6ED;">
              <h4 class="modal-title" align="center" id="exampleModalLabel" style="color:white;">Datos Adicionales</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
          </div>  
            <div class="modal-body" style="text-align: center;">

            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <span class="alert-danger" id="resultcorreo"></span>
                          <h4 class="panel-title">
                            <button class="btn btn-warning" data-toggle="collapse" href="#datoscliente"><span class="glyphicon glyphicon-plus"></span> Información del Cliente</button>
                          </h4>
                    </div>
                      <div id="datoscliente" class="panel-collapse collapse">
                        <div class="panel-body"> 
                           <div class="form-group col-xs-4">
                            <label>Teléfono #1</label>
                            <input class="form-control phone" type="tel"  name="tlf1" onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" minlength="11" maxlength="11"  id="tlf1" value="<?php echo isset($row['tlf1']) ? $row['tlf1'] : "" ;?>">
                          </div>
                          <div class="form-group col-xs-4">
                            <label>Teléfono #2</label>
                            <input class="form-control phone" type="tel"  name="tlf2" onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" minlength="11" maxlength="11"  id="tlf2" value="<?php echo isset($row['tlf2']) ? $row['tlf2'] : "" ;?>">
                          </div>
                          <div class="form-group col-xs-4">
                            <label>Email Personal</label>
                            <input class="form-control" onkeyup="modificaciones();validarEmail(this)" name="email" id="email" value="<?php echo isset($row['email']) ? $row['email'] : "" ;?>" required>
                          </div>
                          <div class="form-group col-xs-4" align="center">
                            <label>Nombre</label>
                            <input class="form-control form-horizontal" type="text"
                            onkeypress="return soloLetras(event)" onkeyup="modificaciones()" name="nombre2" id="nombre2" maxlength="50" value="<?php echo isset($row['nombre2']) ? $row['nombre2'] : "" ;?>">
                          </div>
                          <div class="form-group col-xs-4">
                            <label>Apellido </label>
                            <input class="form-control form-horizontal" type="text" onkeypress="return soloLetras(event)" onkeyup="modificaciones()" name="apellido2" id="apellido2" maxlength="50" value="<?php echo isset($row['apellido2']) ? $row['apellido2'] : "" ;?>">
                          </div>
                        </div>    <!--end div panel body -->
                            <div class="panel-footer">
                               <button class="btn btn-primary" type="submit" id="updatoscontacto" name="Actualizar" value="Actualizar Campos" onClick="datosadicional();">Guardar</button>
                            </div>
                      </div>
                </div>               
            </div>

<div class="panel panel-default" style="text-align:center;" id="accordiondirinstall">
          <div class="panel-heading">
              <h4 class="panel-title">
                    <button class="btn btn-warning" data-toggle="collapse" href="#collapseDirinstall"><span class="glyphicon glyphicon-plus"></span>Dirección de Instalación - Técnico</button>
              </h4>
          </div>

<div id="collapseDirinstall" class="panel-collapse collapse"><!-- collapse all panel dirinstall -->
          
    <div class="panel-body">
      <div class="form-group">
            <br>
              <label>Dirección de instalación Original</label>
              <textarea style="border-color:green;background-color: #FFFFFF" class="form-control" rows="4" readonly>+ Direccion Completa: <?php echo  "\n". $row['direccion']. 
                 "\n".
                 "- Estado: " .$row['estado'].  
                 "\n".
                 "- Municipio: ".$row['municipio'].
                 "\n".
                 "- Parroquia: ".$row['parroquia']; ?>
              </textarea>
          </div>

            <div class="panel-group">
              <label>Agregar Dirección Adicional </label>
                <div class="panel-heading">
                  <div class="panel-body">
                      <input class="form-control hidden" type="numeric" id="codetipodir5"  value="5">
                      <!--<?php if ($tipousuario!='A' && $validaestatus==1){
                      echo 'disabled'; } ?>-->
                      <div class="form-group col-xs-4">
                        <label>Calle ó Av:</label>
                        <input onchange="modificaciones()" class="form-control col-xs-4" name="" id="edit_calleav" maxlength="200" value="<?php echo isset($row['dcalle_av']) ? $row['dcalle_av'] : "" ;?>">
                      </div>
                      <!-- hidden input's direccion de instalación -->
                      <div class="form-group col-xs-4">
                        <label>Estado:</label>
                        <input class="form-control hidden" id="txtd_estado4"  value="<?php echo isset($row['codestado']) ? $row['codestado'] : "" ;?>">
                        <select name="edit_estado" class="form-control" onchange="modificaciones()" id="edit_estado" style="color:#000000" >
                      </select>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Municipio:</label>
                        <input class="form-control hidden" id="txtd_localidad4"  value="<?php echo isset($row['codmunicipio']) ? $row['codmunicipio'] : "" ;?>">
                        <select name="edit_localidad" class="form-control" onchange="modificaciones()" id="edit_localidad" style="color:#000000">
                        </select>
                       </div>
                       <div class="form-group col-xs-4">
                        <label>Parroquia</label>
                        <input class="form-control hidden" id="txtd_sector4"  value="<?php echo isset($row['codparroquia']) ? $row['codparroquia'] : "" ;?>">
                        <select name="edit_sector" id="edit_sector" class="form-control" style="color:#000000" >
                        </select>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Urbanizacion:</label>
                        <input class="form-control col-xs-4" onkeyup="modificaciones()" name="" id="edit_urbanizacion" maxlength="200" value="<?php echo isset($row['durbanizacion']) ? $row['durbanizacion'] : "" ;?>">
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Local N°:</label>
                        <input class="form-control col-xs-4" name="" onkeyup="modificaciones()" id="edit_nlocal"  maxlength="19" value="<?php echo isset($row['ddesclocal']) ? $row['ddesclocal'] : "" ;?>">
                      </div>

                      <div class="form-group col-xs-4">
                        <label>Código Postal:</label>
                        <input class="form-control col-xs-4" name="" id="edit_codigopostal" onkeyup="modificaciones()" onkeypress=" return soloNumeros(event)" maxlength="4" value="<?php echo isset($row['dcodpostal']) ? $row['dcodpostal'] : "" ;?>">
                      </div>
                      <div class="form-group col-xs-6">
                        <label>Punto de Referencia:</label>
                        <input class="form-control col-xs-8" onkeyup="modificaciones()" name="" id="edit_ptoref" maxlength="200" value="<?php echo isset($row['dptoref']) ? $row['dptoref'] : "" ;?>">
                      </div>
                  </div>

                </div>
            </div>
                        </div>
                        <div class="panel-footer">
                          <button class="btn btn-primary" type="submit" name="Actualizar" value="Actualizar Campos" onClick="dir_adicional()">Guardar</button>
                        </div>
 </div>      <!-- <?php if ($tipousuario!='A' && $validaestatus==1){ echo 'disabled'; } ?> -->

</div>
        </div> 
        </div>
    </div>
</div>

<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:1150px;right: 40%;height: 100%;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrar Visita</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>  
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" class="form-control" id="secuencial">
                    <input type="hidden" class="form-control" id="serialpo">
                    <input type="hidden" class="form-control" id="fechacarga">
                    <input type="hidden" class="form-control" id="tecnico">
                    <div class="col-lg-6">
                        <div class="form-group">
                        <label for="" class="col-form-label">Fecha de Gestion</label>
                        <input type="date" class="form-control" id="fechagesti">
                        <span class="error" id="errorfechagesti"></span>
                        </div>               
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                          <label for="" class="col-form-label">Nombre del técnico</label>
                          <select id="ntecnicoselect" class="form-control">
                          </select>
                          <input class="form-control hidden" id="ntecnicos">
                          <span class="error" id="errortecnico"></span>
                        </div> 
                    </div>
                    <br><br><br><br><br>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="" class="col-form-label">Estatus Visita Tecnica</label>
                            <select id="selectestatus" class="form-control">
                            </select>
                            <input class="form-control hidden" id="estatusvisita">
                            <span class="error" id="errorestatus"></span>
                        </div> 
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="" class="col-form-label">Condicion</label>
                            <select id="selectcondicion" class="form-control">
                            </select>
                            <input class="form-control hidden" id="idcondicion">
                            <span class="error" id="errorcondicion"></span>
                        </div> 
                    </div>
                    <br><br><br><br><br>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="" class="col-form-label">Fecha de Visita</label>
                            <input type="date" class="form-control" id="fechavisita">
                            <span class="error" id="errorfechavisita"></span>
                        </div> 
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="" class="col-form-label">Observaciones</label>
                            <input type="text" class="form-control" id="observacion">
                        </div> 
                    </div>
                  </div>   
                </div>
               
            <div class="panel-footer" align="center">
               <input type="button" class="btn btn-primary" id="btnguardar" name="guardar" value="Guardar" onclick="guardarregistrovisita();">            
             </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  
  selectdatos(); validacionvisita(); mascaras(); direccion();
</script>

<script type="text/javascript">
  function dtCore(){
    var  idcliente= document.getElementById('idcliente').value;
    var  idvisita= document.getElementById('idvisita').value;
    var  idsecuencial= document.getElementById('idsecuencial').value;
    var  fechavisit=document.getElementById('fechavisit').value;
    var  busqueda=document.getElementById('busqueda').value;
    var  validar=document.getElementById('validar').value;
    var  idresultado=document.getElementById('idresultado').value;

   // var arrayfechavisit=fechavisit.split("/");
   // var añovisita=parseInt(arrayfechavisit[0]);

    //alert(vari);
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var añoactual = today.getFullYear();


   $('#tbl_visitatecnica').DataTable({  
        "ajax":{            
            "url": "view/VisitaTecnica/selectMostrargestionesvisita.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{ "idcliente": idcliente, "idsecuencial": idsecuencial}
        },
        "columns":[
            {"data": "secuencial", className: "text-center"},
            {"data": "fechacarga"},
            {"data": "serialpo", className: "text-center" },
            {"data": "fechagesti"},
            {"data": "desvisita", className: "text-center" },
            {"data": "descondicion", className: "text-center" },
            {"data": "ntecnico", className: "text-center" },
            {"data": "fechavisita", className: "text-center"},
            {"data": "observacion", className: "text-center" }
          ],
          "order" : [[0, "asc"]],

          /*"columnDefs": [
                 {
                       "targets": [ 1,9,10, 11, 12, 13 ],
                       "visible": false,
                   }
               ],*/
          "scrollX": true,
          "scrollY": false,
          "info":     false,
          "scrollCollapse": false,
          
          dom: 'Bfrtip',
          buttons: [
            {name: "btnuevo",
              action: function ( e, dt, node, config ) {
               $('#modalCRUD').modal('show');
              },text: '<button class="btn btn-warning" id="btnNuevo" type="button">+</button>'
            }
          ],
        language:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });

   $("#btnNuevo").click(function(){
      opcion = 2; 
      var table=$('table#tbl_visitatecnica').DataTable();
      var T =  table.row($(this).parents("tr")).data();
      $("#modalCRUD").trigger("reset");
      $(".modal-header").css( "background-color", "#63B6ED");
      $(".modal-header").css( "color", "white" );
      $(".modal-title").text("Registrar Visita");
      $('#modalCRUD').modal('show');
      $('#ntecnicoselect').attr('required');    
      $('#selectestatus').attr('required');
      $('#fechagesti').attr('required');    

    });

if ((busqueda == fechavisit) && (idresultado==2)) {
        // posee visita en el año en curso
        console.log('busqueda y año visita, connn registros.')
        $('.dt-buttons').addClass('hidden');

    } else if ((busqueda == fechavisit) && (idresultado==1)) {
        // posee visita en el año en curso
        console.log('busqueda y año visita, connn registros.')
        $('.dt-buttons').removeClass('hidden');

    }else if ((busqueda != añoactual) && (validar==0 || validar!=0)) {
        console.log('busqueda y año diferentes, con o sin registros.')
        $('.dt-buttons').addClass('hidden');

    }   else if ((busqueda == fechavisit) && (validar!=0)) {
        console.log('busqueda y año visita, con registros.')
        $('.dt-buttons').removeClass('hidden');
    } 

  else {
       // no posee visitas en el año en curso
        console.log('busqueda y año actual igual, con registros.')
        $('.dt-buttons').removeClass('hidden');
 }
}
// $("#btninf_adicional").click(function(){
//       //opcion = 2; 
    
//       $('#inf_adicional').modal('show');
//       $("#modalCRUD").trigger("reset");
//       $(".modal-header").css( "background-color", "#63B6ED");
//       $(".modal-header").css( "color", "white" );
//       $(".modal-title").text("Registrar Visita");
//       $('#ntecnicoselect').attr('required');    
//       $('#selectestatus').attr('required');
//       $('#fechagesti').attr('required');    

//     });
</script>