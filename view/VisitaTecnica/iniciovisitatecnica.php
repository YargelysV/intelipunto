
<?php
  $tituloPagina=$_SESSION["page"];
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
    $_SESSION["visitatecnica"]="1";
   
  }
?>


<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-calendar-plus-o" style="color:#045FB4"> </i> <?php echo $tituloPagina;?></h1>
      </div>


      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              Busqueda 
            </div>

          <div class="panel-body">
            <div>
              <div class="col-md-5" >Año:</div>               
              <div class="col-md-5" id="axos"></div>
            </div>
            <div>
              <br>
              <div class="col-lg-5">Banco:</div>
              <div class="col-lg-5">
                  <select name="clientes" id="clientes" class="form-control" required>
                  <option value="">Seleccione Banco</option>
                  </select>
              </div>
            </div>
            <br>
            <div>
              <div class="col-lg-5">Serial ó Afiliado:</div>
            <br>
              <div class="col-lg-5">
                <input type="text" id="nrorif" class="form-control" placeholder="Eje:J-057105401...">
              </div>
            </div>
            <br>
            
            <br>
            
          </div>
           <input type="hidden" id="valor" value="13" name="valor"/>
         
            <script languaje="javascript">
            generarclientereportesequipos();
            </script>


<div class="panel-footer">
          <p style="text-align: center;">
             <input class="btn btn-success" type="button" name="Buscar" value="Buscar" onClick="buscarvisitatecnica()"/>
            <!--  <input class="btn btn-primary" type="button" name="Buscar" value="Buscar por Serial" onclick="mostrarBusquedaRif();"/> -->
          </p>
</div>
          </div>
        </div>
      </div>



<!-- busqueda por RIF o Razón Social -->
      <div class="row" id="panelbusquedarif" style="display:none;">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Buscar Afiliado por RIF &oacute; Raz&oacute;n Social, Serial del Equipo
            </div>
          <div class="panel-body">
            <div>
                <div class="input-group custom-search-form center">
                       <input type="text" id="nrorif" class="form-control" placeholder="Eje:J-057105401...">
                          <span class="input-group-btn">
                           <button class="btn btn-default" type="button" onclick="buscarclienteRifvisita()">
                                    <i class="fa fa-search"></i>
                          </button>
                        </span>
                 </div>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end panel busqueda por rif -->


<!--       <div class="row" id="panelbusquedafecha" > 
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              Busqueda por Fecha
            </div>

            <div class="panel-body"> 
            <div>
              <div class="col-lg-3" style="text-align:center">Año:</div>               
              <div class="col-lg-3" id="axos"></div>
            </div>
            
          </div>
          <div class="panel-footer">
            <p style="text-align: center;">
               <input class="btn btn-primary" type="button" name="Buscar" value="Buscar" onClick="buscargestionxfechas()"/>
               
            </p>
          </div>
          </div>
        </div>
   
      </div>  -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>



   