<?php
  $tituloPagina=$_SESSION["page"];
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
    $_SESSION["visitatecnica"]="1";
   
  }
?>


<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-calendar-plus-o" style="color:#045FB4"> </i> <?php echo $tituloPagina;?></h1>
      </div>


      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              Busqueda por Banco
            </div>

          <div class="panel-body">
            <div>
              <div class="col-lg-5">Banco:</div>
              <div class="col-lg-5">
                  <select name="clientes" id="clientes" required>
                  <option value="">Seleccione Banco</option>
                  </select>
              </div>
              <br>
            </div>
          </div>
           <input type="hidden" id="valor" value="13" name="valor"/>
           <input type="hidden" id="tipousuario" value='<?php echo $_SESSION["codtipousuario"];?>' name="tipousuario"/>
            <script languaje="javascript">
            generarclientereportesequipos();
            </script>


<div class="panel-footer">
          <p style="text-align: center;">
             <input class="btn btn-success" type="button" name="Buscar" value="Buscar" onClick="buscarreportevisitatecnica()"/>
             <input class="btn btn-danger" type="button" name="Buscar" value="Buscar por Fecha" onclick="mostrarBusquedaFecha();"/>
          </p>
</div>
          </div>
        </div>
      </div>


       <div class="row" id="panelbusquedafecha" style="display:none;"> 
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              Busqueda por Fecha
            </div>

          <table border="0" align="center">
              <br>
              <tr align="center">
                <td> </td>
                  <td class="center">Desde </td>
                  <td class="center">Hasta</td>
              </tr>
              <tr align="center">
                <td><div class="col-lg-5">Fecha:</div>  </td> 
                <td align="center"><input type="date" name="fechainicialgest" id="fechainicialgest" class="form-control "  style="color: #000000" required="required" max="<?php  echo date("Y-m-d");?>" onchange="validarfechasvisita()" value="">
                </td>
                <td align="center"><input type="date" name="fechafinalgest" id="fechafinalgest" class="form-control inputcentrado"  style="color: #000000" required="required" max="<?php  echo date("Y-m-d");?>" onchange="validarfechasvisita()" value="" > </td>
              </tr>

            </table>
            <br>


          <div class="panel-footer">
            <p style="text-align: center;">
               <input class="btn btn-primary" type="button" name="Buscar" value="Buscar" onClick="buscarvisitasxfechas()"/>
               
            </p>
          </div>
          </div>
        </div>
   
      </div>   <!-- /busqueda x fecha -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>




