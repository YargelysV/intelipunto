<?php
  $usuario=$_SESSION["session_user"];
  $codtipousuario=$_SESSION["superusuario"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $fechainicial= isset($array[0]) ? $array[0] : "";
  $fechafinal= isset($array[1]) ? $array[1] : "";
  $_SESSION['bsqdafecha']=$fechainicial.'/'.$fechafinal;
  $_SESSION['bsqdarecepcion']='';
  $_SESSION['buscariff']='';

 // $_SESSION['volver']=$POS.'/'.$fechainicial.'/'.$fechafinal;
  $controladorc =new controladorVisita();
  $resultados= $controladorc->sp_mostrarvisitatecnicaxfechas($fechainicial,$fechafinal);
  $databanco = pg_fetch_assoc($resultados);

  $count = pg_num_rows($resultados);

?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
<input type="hidden" id="codtipousuario"  name="codtipousuario"  value="<?php echo $codtipousuario; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Registros de Visita Técnica:</a>
             </div>
       </nav>
      <b style="color:#5DADE2;"></b>    <b style="color:#5DADE2;">Búsqueda:<?php echo $fechainicial.' / '.$fechafinal;  ?></b> 
    
    </div>
    <div class="row"  align="center">

          <div class="col-12">
        <table class="table table-bordered"  id="visitatecnicafechas"> 
           <thead>
              <tr>
                
                <th>N°</th>
                <th>enlace</th>
                <th>Acción</th>
                <th>ID</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Resultado&nbsp;de&nbsp;la&nbsp;Visita&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Visita&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Afiliado&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Representante&nbsp;Legal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th> 
                <th>Correo&nbsp;Electrónico</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Ejecutivo</th>   
                <th>Tipo&nbsp;de&nbsp;POS</th>
                <th>Modelo&nbsp;de&nbsp;POS</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nº&nbsp;Factura</th>
                <th>Serial&nbsp;POS</th>
                <th>Serial&nbsp;Simcard</th>
                <th>Serial&nbsp;Mifi</th>                 
                <th>Nº&nbsp;Terminal</th>                           
                <th>Estatus&nbsp;del&nbsp;Equipo</th>
                <th>Fecha&nbsp;Instalación</th>                      
                <th>Fecha&nbsp;Gestión</th>                      
                <th>Fecha&nbsp;Visita</th>                      
                <th>Tipo&nbsp;Zona</th>                      
                <th>Nombre del Técnico</th>                      
                <th>Estado</th>                      
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirreción&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>                      
                <th>idsecuencial</th>                      
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Verificado&nbsp;&nbsp;&nbsp;&nbsp;</th>  
               
              </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="4" style="text-align:left">Total:</th>
                
            </tr>
        </tfoot>
        </table>
 
      </div>

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportevisitatecnica'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var fechainicial= document.getElementById('fechainicial').value;
  var fechafinal= document.getElementById('fechafinal').value;
  var codtipousuario= document.getElementById('codtipousuario').value;


  $('table#visitatecnicafechas').DataTable({
    "ajax": {
    "url": "view/VisitaTecnica/selectVisitaTecnicaxFecha.php",  
          "type": "POST",
          "data": {"fechainicial":fechainicial, "fechafinal":fechafinal}
    },
    "columns": [
      
      {"data": "sec", className: "text-center"},
      {"data": "enlace", className: "text-center" },
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Gestionar</a>", className: "text-center"},
      {"data": "cliente", className: "text-center" },
      {"data": "descresult", className: "text-center" },
      {"data": "desc_visita", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "documento", className: "text-center" },
      {"data": "afiliado", className: "text-center" },
      {"data": "representante", className: "text-center" },
      {"data": "correo", className: "text-center" },
      {"data": "tlf1", className: "text-center" },
      {"data": "ejecut", className: "text-center" },
      {"data": "tipopos", className: "text-center" },
      {"data": "marcapos", className: "text-center" },
      {"data": "ibpbanco", className: "text-center" },
      {"data": "factura", className: "text-center" },
      {"data": "serial", className: "text-center" },
      {"data": "sim", className: "text-center" },
      {"data": "serialmifi", className: "text-center" },                      
      {"data": "terminal", className: "text-center" },
      {"data": "estatus", className: "text-center" },     
      {"data": "fechainst", className: "text-center" },      
      {"data": "fechagest", className: "text-center" },      
      {"data": "fechavisit", className: "text-center" },      
      {"data": "desc_zona", className: "text-center" },      
      {"data": "tecniconame", className: "text-center" },      
      {"data": "estado", className: "text-center" },      
      {"data": "direccion_zona", className: "text-center" },      
      {"data": "idsecuencial", className: "text-center" },      
      {"data": "idaprob",
      render : function ( data, type, full, meta ) {
        var codusuario = $("#codtipousuario").val();
          if (codusuario != "A" || codusuario !== "A") {
                return "-";
          }else{

                if (data == 0 || data === "0") {
              return '<form><div class="form-group"><label>NO <input class="radio-buttons" type="radio" name="permiso"  checked value="1"></label><label> &nbsp;SI <input class="radio-buttons" type="radio" name="permiso" value="2" ></label></div></form>';
            }else if (data == 1 || data === "1"){
              return '<form><div class="form-group"><label>NO <input type="radio" class="radio-buttons" name="permiso"  value="1" disabled></label><label> &nbsp;SI <input type="radio" class="radio-buttons" name="permiso" checked value="2" disabled></label></div></form>';
            }else
              return "";                   
            }                              
        } 

    }
    ],
    "order" : [[0, "asc"]],
  "columnDefs": [
                 {
                       "targets": [1,2,29],
                       "visible": false,
                   }
               ],

    "scrollX": 2900,
    "scrollY": false,
    "info": false,
    "scrollCollapse": false,

    dom: 'Bfrtip',
     buttons: [
     {
            extend: 'excelHtml5',
            text: 'Exportar Excel', 
            title: 'Reporte Visita Técnica', 
               
        }
     ],
    
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

  $('table#visitatecnicafechas tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#visitatecnicafechas').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "visitatecnica?cargar=detallesvisitatecnica&var="+enlace; 
        $(location).attr('href',url);

  });

  $('table#visitatecnicafechas tbody').on( 'click', 'input.radio-buttons', function () {
      //alert('algo');
       var table=$('table#visitatecnicafechas').DataTable();
       var T =  table.row($(this).parents("tr")).data();

       var sec = T.idsecuencial;
       var idaprob = T.idaprob;

       //alert(idaprob);
       var ruta="view/VisitaTecnica/guardar_registrovisita.php?r=1&sec="+sec+"&idaprob="+idaprob;
        $.ajax({
            url : ruta,
            type : "POST",
            //data : {idLote:idLote,IdEquipo:IdEquipo},
            success: function(data){

                if(data != 1  || data != '1'){
                    

                    if (idaprob==0) {
                    var table=$('#visitatecnicafechas').DataTable();
                    table.ajax.reload();    
                     
                    } else {
                    var table=$('#visitatecnicafechas').DataTable();
                    table.ajax.reload(); 
                     }

                }else{
                    alertify.error();
                }
            }
        })         
      });

}
  </script>