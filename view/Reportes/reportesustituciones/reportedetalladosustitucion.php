<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] : "";
  $secuencial= isset($array[1]) ? $array[1] : "";
  $banco= isset($array[2]) ? $array[2] : "";
/*  $fechainicial = isset($array[2]) ? $array[2] : "";
  $fechafinal= isset($array[3]) ? $array[3] : "";*/
//  $volver= $_SESSION['volver'];

  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_reportedetalladosustitucion($cliente,$secuencial);
  $databd =pg_fetch_assoc($resultados);
 
?>

<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="secuencial"  name="secuencial"  value="<?php echo $secuencial; ?>" />
<input type="" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<!-- <input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" /> -->




 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header ">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Detallado de Sustituciones: </a>
          <br>
        </div>
       </nav>
       
    </div>
<br>

 <div class="row" align="center"> 
     <div class="col-lg-12" align="left">
      <div class="panel panel-info">
        <div class="panel-heading" style="text-align: center;">
          <i class="fa fa-users"></i> Datos del Comercio
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="list-group">
            <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Razon Social:
              <span class="pull-center text-muted small"><b><?php echo $databd['razonsoc'];?> </b>
              </span>
             </div>
           <div class="list-group-item">
            <i class="fa fa-check fa-fw"></i> Nº documento:
            <span class="pull-center text-muted small"><b><?php echo $databd['documento'];?> </b>
            </span>
          </div>
        
    </div>
  </div>

   <div class="row">
            <div class="col-lg-12" align="center">
                <table class="table table-bordered"  id="tblreportesustitucion"> 
                   <thead>
                      <tr>
                        <th>Nro.</th>
                        <th>Fecha Carga</th>
                        <th>Serial POS</th>
                        <th>Fecha Almacen</th>
                        <th>Nombre Tecnico</th>
                        <th>idtecnico</th>
                        <th>Fecha Gestion</th>
                        <th>idestatus</th>
                        <th>Estatus</th>
                        <th>Fecha Instalacion</th>
                        <th>observacion</th>
                        <th></th>                        
                      </tr>
                    </thead>
                </table>
             </div>
        </div>

    </div>
  </div>

</div>
   
      <div class="col-md-4 col-md-offset-4" align="center" >
      <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportesustituciones?cargar=generarreportesustituciones&var=<?php echo $banco ?>'">Volver</button>
      </div>
 </div>
 </div>


<script type="text/javascript">
    function dtCore(){

      var cliente=document.getElementById('cliente').value; 
      var secuencial=document.getElementById('secuencial').value; 

    tablaSustitucion = $('#tblreportesustitucion').DataTable({  
        "ajax":{            
            "url": "api/selectReporteDetalladoSustitucion.php", 
            "method": 'POST',
            "data":{"cliente": cliente, "secuencial": secuencial} //usamos el metodo POST
        },
        "columns":[
            {"data": "sec", className: "text-center"},
            {"data": "fechacarga", className: "text-center" },
            {"data": "serialpo", className: "text-center" },
            {"data": "fecha_almacen", className: "text-center" },
            {"data": "ntecnico", className: "text-center" },
            {"data": "tecnico", className: "text-center" },
            {"data": "fechagest", className: "text-center" },
            {"data": "estatus", className: "text-center" },
            {"data": "estatusinst", className: "text-center" },
            {"data": "fechainst", className: "text-center"},
            {"data": "observacion", className: "text-center" },
            {"data": "id", className: "text-center" },
            ],
          "order" : [[11, "asc"]],

          "columnDefs": [
                 {
                       "targets": [ 5, 7,11],
                       "visible": false,
                   }
               ],
          "scrollX": true,
          "scrollY": false,
          "info":     false,
          "scrollCollapse": false,
          
          dom: 'Bfrtip',
          buttons: [
          'print'
          ],
        language:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });

}
</script>
