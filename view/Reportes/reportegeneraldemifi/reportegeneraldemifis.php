<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] : "";

 // $_SESSION['volver']=$POS.'/'.$fechainicial.'/'.$fechafinal;
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_reportediariogeneraldemifi($banco);
  $databanco = pg_fetch_assoc($resultados);

  $count = pg_num_rows($resultados);

?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>



<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="tipousuario" value='<?php echo $_SESSION["codtipousuario"];?>' name="tipousuario"/>
<input type="hidden" id="ejecutivo" value='<?php echo $_SESSION["ejebanco"]?>' name="ejecutivo"/>
<input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"]?>' name="usuario"/>



 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte General de Equipos Mifi:</a>
             </div>
       </nav>
      <b style="color:#5DADE2;"></b>   <b style="color:#5DADE2;">Búsqueda:<?php if ($banco=='0') {
       echo 'Todos los bancos';
     } else { echo $databanco['nbanco']; } ?></b> 
    
    </div>
    <div class="row"  align="center">
            <div class="col-12">
        <table class="table table-bordered"  id="reportegeneraldemifi"> 
           <thead>
              <tr>               

                <th>N°</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;ID&nbsp;&nbsp;&nbsp;</th>          
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>        
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Estatus&nbsp;Contacto&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cliente&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;del&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Fecha&nbsp;Recepción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Ejecutivo&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Fecha&nbsp;Instalación&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Correo&nbsp;Electrónico&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Telefono&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Telefono&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Telefono&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Telefono&nbsp;4&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Cantidad&nbsp;Terminales</th>
                <th>Tipo&nbsp;de&nbsp;POS</th>
                <th>Modelo&nbsp;de&nbsp;POS</th>
                <th>Nº Factura</th>
                <th>Serial&nbsp;Mifi</th>
                <th>Serial&nbsp;Simcard</th>
                <th>Fecha&nbsp;Carga</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;Fiscal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>codbanco</th>
       </tr>
            </thead>
            <tfoot>
              <th colspan="7" style="text-align:left">Total:</th>
            </tfoot>
        </table>
 
      </div> 

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportegeneraldemifi'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

    <script type="text/javascript">
  function dtCore(){

    var banco= document.getElementById('banco').value;

  $('table#reportegeneraldemifi').DataTable({
    "ajax": {
    "url": "api/selectReporteGeneraldeMifi.php",  
          "type": "POST",
          "data": {"banco":banco}
    },
    "columns": [
      
      {"data": "sec", className: "text-center"},
      {"data": "coddocumento", className: "text-center" },
      {"data": "consecutivo", className: "text-center" },
      {"data": "nbanco", className: "text-center" },
      {"data": "razonsocial",  className: "text-center"},
      {"data": "estatuscontac", className: "text-center" },
      {"data": "estatuscliente", className: "text-center" },
      {"data": "descequipo", className: "text-center" },
      {"data": "fecharecep", className: "text-center" },
      {"data": "ejec", className: "text-center" },
      {"data": "fechainstalacion", className: "text-center" },
      {"data": "correo", className: "text-center" },
      {"data": "tlf1", className: "text-center" },
      {"data": "tlf2", className: "text-center" },
      {"data": "tlf3", className: "text-center" },
      {"data": "tlf4", className: "text-center" },
      {"data": "cantidadpos", className: "text-center" },
      {"data": "tipoposs", className: "text-center" },
      {"data": "marcapos", className: "text-center" },
      {"data": "factura", className: "text-center" },
      {"data": "serialmifi", className: "text-center" },
      {"data": "serialsim", className: "text-center" },
      {"data": "fechacarga", className: "text-center" },
      {"data": "direccions" },
      {"data": "codbanco", className: "text-center" }
      
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
                 {
                       "targets": [24],
                       "visible": false,
                   }
               ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel'
    }],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

}
  </script>