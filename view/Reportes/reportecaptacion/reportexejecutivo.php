<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $clientes= isset($array[0]) ? $array[0] : "";
  $ejecutivo= isset($array[1]) ? $array[1] : "";
  $fechainicial = isset($array[2]) ? $array[2] : "";
  $fechafinal= isset($array[3]) ? $array[3] : "";
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_reportecaptacionejec($clientes,$ejecutivo,$fechainicial,$fechafinal);
  $databanco = pg_fetch_assoc($resultados);
  $_SESSION['Volver']=$clientes.'/'.$ejecutivo.'/'.$fechainicial.'/'.$fechafinal;
  $count = pg_num_rows($resultados);
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $clientes; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Captación: </a>
          <br>
     <!--    <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $databanco['ibp'];?></b> -->
        </div>
       </nav>
    </div>
    <div class="row">

    <div class="col-12">
        <table class="table table-bordered"  id="reportecaptacion"> 
           <thead>
              <tr>               

                <th>N°</th>
                <th>&nbsp;&nbsp;&nbsp;Fecha&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usuario&nbsp;Carga&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Tipo&nbsp;de&nbsp;POS&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
       </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
          </tfoot>
        </table>
 
      </div> 
      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportecaptacionejecutivo'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var clientes= document.getElementById('clientes').value;
  var ejecutivo= document.getElementById('ejecutivo').value;
  var fechainicial= document.getElementById('fechainicial').value;
  var fechafinal= document.getElementById('fechafinal').value;

  $('table#reportecaptacion').DataTable({
    "ajax": {
    "url": "api/selectReporteCaptacionEjecutivos.php",  
          "type": "POST",
          "data": {"clientes":clientes,"ejecutivo":ejecutivo,"fechainicial":fechainicial,"fechafinal":fechafinal }
    },
    "columns": [
      
      {"data": "id_consecutivo", className: "text-center"},
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "usuario", className: "text-center" },
      {"data": "nombre", className: "text-center" },
      {"data": "razonsocial",  className: "text-center"},
      {"data": "coddocumento", className: "text-center" },
      {"data": "tipopos", className: "text-center" },
      {"data": "ibp", className: "text-center" }
      
    ],
    "order" : [[0, "asc"]],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend:'print',          
        }
    ],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

}
  </script>