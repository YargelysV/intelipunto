


<?php 
if (isset($_SESSION["session_user"]))
{


?>

<script type="text/javascript">
        $(function () {
            $('#importDialog').modal({ 'show': false });
        });
</script>

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-gears" style="color:#F8F9F9;"> </i> Reporte Conexiones al Sistema desde el 15/05/2019</a>
        </div>
       </nav>
        <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="reporteconexion"> 
           <thead>
              <tr>               

                <th>N°</th>
                <th>&nbsp;&nbsp;&nbsp;Usuario&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Ejecutivo&nbsp;&nbsp;&nbsp;</th>          
                <th>&nbsp;&nbsp;&nbsp;Conexión&nbsp;1&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Conexión&nbsp;2&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Conexión&nbsp;3&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Historial de&nbsp;Conexiones&nbsp;&nbsp;&nbsp;&nbsp;</th>
       </tr>
            </thead>
            <tfoot>
              <th colspan="7" style="text-align:left">Total:</th>
            </tfoot>
        </table>
 
      </div> 
      <br>
    </div>
          
    </div>
  </div>
</div>

<?php

  }



  else{
    header("Location:../salir");
  }


  ?>

    <script type="text/javascript">
  function dtCore(){

  $('table#reporteconexion').DataTable({
    "ajax": {
    "url": "api/selectReporteConex.php",  
          "type": "POST"
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "usuario", className: "text-center" },
      {"data": "nombreejecutivo", className: "text-center" },
      {"data": "fecha1", className: "text-center" },
      {"data": "fecha2",  className: "text-center"},
      {"data": "fecha3", className: "text-center" },
      {"data": "conexiones", className: "text-center" }
      
    ],
    "order" : [[0, "asc"]],
    "scrollX": false,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
     buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel'
    }],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

}
  </script>