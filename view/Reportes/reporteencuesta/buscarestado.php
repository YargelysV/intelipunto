<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] : "";
  //echo $cliente;
 
  $_SESSION['Volver']=$cliente;
  //$count = pg_num_rows($resultados);
 
?>

<?php if ($cliente!=0){
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_buscarestadoencue($cliente);
  $databanco = pg_fetch_assoc($resultados); ?>

<input type="hidden" id="cliente"  name="clientes"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="detalle"  name="detalle"  value="<?php echo $detalle; ?>" />
<input type="hidden" id="total"  name="total"  value="<?php echo $databanco['totalb']; ?>" />
<input type="hidden" id="nbanco"  name="nbanco"  value="<?php echo $databanco['nbanco']; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Por Estados: <?php if ($cliente=='0') {
           echo 'Todos los bancos';
          } else { echo $databanco['nbanco']; } ?></a>
          <br>
          <br><b style="color:#000000;" align="left"> TOTAL REGISTROS:</b>
          <b style="color:#000000;"> <?php echo $databanco['totalb']; ?></b>
        </div>
      </nav>
    </div>
  <div class="col-lg-12" align="center" >
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="buscarestatus" style="width: 100%"> 
           <thead>
              <tr>
                <th>Correlativo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Encuestado</th>
                <th>Porcentaje</th>
                <th>NO Encuestado</th>
                <th>Porcentaje</th>
                <th>Llamar Luego</th>
                <th>Porcentaje</th>
                <th>Encuesta Rechazada</th>
                <th>Porcentaje</th>
                <th>Contacto ilocalizable</th>
                <th>Porcentaje</th>
                <th>totalb</th>
              </tr>
            </thead>
          </table>
      </div>
    </div>
  </div> 
        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteestatusencuesta.php'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php } else if ($cliente=='0') {
  $controladortb =new ControladorReporte();
  $resultadostb= $controladortb->sp_buscarestadoencueTD($cliente);
  $databancotb = pg_fetch_assoc($resultadostb);?>

<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="detalle"  name="detalle"  value="<?php echo $detalle; ?>" />
<input type="hidden" id="totaltb"  name="totaltb"  value="<?php echo $databancotb['totalb']; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Por Estados: <?php if ($cliente=='0') {
           echo 'Todos los bancos';
          } else { echo $databancotb['nbanco']; } ?></a>
           <br>
          <br><b style="color:#000000;" align="left"> TOTAL REGISTROS:</b>
          <b style="color:#000000;"> <?php echo $databancotb['totalb']; ?></b>
      </nav>
    </div>
  <div class="col-lg-12" align="center" >
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="buscarestatusTB" style="width: 100%"> 
           <thead>
              <tr>
                <th>Correlativo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Encuestado</th>
                <th>Porcentaje</th>
                <th>NO Encuestado</th>
                <th>Porcentaje</th>
                <th>Llamar Luego</th>
                <th>Porcentaje</th>
                <th>Encuesta Rechazada</th>
                <th>Porcentaje</th>
                <th>Contacto ilocalizable</th>
                <th>Porcentaje</th>
                <th>totalb</th>
              </tr>
            </thead>
          </table>
      </div>
    </div>

      <br>
    </div>
  </div> 
        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteestatusencuesta.php'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php } ?>
<script type="text/javascript">
  function dtCore(){
  var clientes=document.getElementById('cliente').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#buscarestatus').DataTable({
    "ajax": {
    "url": "api/SelectReporteEncuestaEstados.php",
          "type": "POST",
          "data": {"clientes":clientes}
    },
    "columns": [
      {"data": "codbanco", className: "text-center"},
      {"data": "nbanco", className: "text-center"},
      {"data": "contencuestado", className: "text-center"  },
      {"data": "encuestado", className: "text-center"},
      {"data": "noencuestado", className: "text-center" },
      {"data": "noencuestado", className: "text-center" },
      {"data": "contllamarluego", className: "text-center" },
      {"data": "llamarluego", className: "text-center" },
      {"data": "contencuestarechazada", className: "text-center" },
      {"data": "encuestarechazada", className: "text-center" },
      {"data": "contilocazable", className: "text-center" },
      {"data": "ilocazable", className: "text-center" },
      {"data": "totalb"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 0, 12],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });


  $('table#buscarestatusTB').DataTable({
    "ajax": {
    "url": "api/SelectReporteEncuestaEstadosTB.php",
          "type": "POST",
          "data": {"clientes":clientes}
    },
    "columns": [
      {"data": "codbanco", className: "text-center"},
      {"data": "nbanco", className: "text-center"},
      {"data": "contencuestado", className: "text-center"  },
      {"data": "encuestado", className: "text-center"},
      {"data": "noencuestado", className: "text-center" },
      {"data": "noencuestado", className: "text-center" },
      {"data": "contllamarluego", className: "text-center" },
      {"data": "llamarluego", className: "text-center" },
      {"data": "contencuestarechazada", className: "text-center" },
      {"data": "encuestarechazada", className: "text-center" },
      {"data": "contilocazable", className: "text-center" },
      {"data": "ilocazable", className: "text-center" },
      {"data": "totalb"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 0, 12],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });
}
  </script>