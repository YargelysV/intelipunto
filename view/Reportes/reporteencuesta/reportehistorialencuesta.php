<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $nroafiliacion= $array[0];
  $namearchivo= $array[1];
  $fechacarga= $array[2];
  $idcliente= $array[3];
  $rif= isset($array[4]) ? $array[4]  : "" ;
  $banco= isset($array[5] ) ? $array[5] : "" ;
  $busqueda=isset($array[6] ) ? $array[6] : "" ;
  $bancoparametrizacion=isset($array[7] ) ? $array[7] : "" ;
  $estatusparametrizacion=isset($array[8] ) ? $array[8] : "" ;
  $adminvolver=isset($array[9])? $array[9]:"";

  $enlace= $nroafiliacion.'/'.$namearchivo.'/'.$fechacarga.'/'.$idcliente.'/'.$rif.'/'.$banco.'/'.$busqueda.'/'.$bancoparametrizacion.'/'.$estatusparametrizacion;
  //echo $cliente;
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_verhistorialllamadas($idcliente);
  $datacliente = pg_fetch_assoc($resultados);
  $_SESSION['Volver']=$idcliente;
  $count = pg_num_rows($resultados);
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="idcliente"  name="clientes"  value="<?php echo $idcliente; ?>" />

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> Historial de llamadas al cliente: <?php echo $datacliente['nombre_cliente'] ;
          ?> </a>
        </div>
      </nav>
    </div>
    <div class="row">
      <div class="demo-container" ng-app="AppReporteHistorialLlamadas" ng-controller="ReporteHistorialLlamadas">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div>

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
          <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa?cargar=DatosEncuesta&var=<?php echo $enlace; ?>'">Volver</button>
        </div>
    </div>
  </div>
</div>

<?php  } ?>