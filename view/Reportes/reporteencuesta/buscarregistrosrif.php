<?php
/*buscarrif.php view*/
 $encuesta=$_SESSION["encuesta"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $_SESSION['buscarrif']=$var;
  $_SESSION['bsqrecepcion']='';
  $array = explode("/",$var);
  $rif= $array[0];
  //echo $rif;
  
  
  $controladorc =new ControladorReporte();
  $comercia= $controladorc->spbuscarrifreporte($rif);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />
<input type="hidden" id="rlegal" name="rlegal"  value="<?php echo $databanco['rlegal'];?>" />
<input type="hidden" id="rsocial" name="rsocial"  value="<?php echo $databanco['rsocial'];?>" />
<input type="hidden" id="nbanco" name="nbanco"  value="<?php echo $databanco['banco'];?>" />
<input type="hidden" id="fechaenc" name="nbanco"  value="<?php echo $databanco['fecha'];?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="" style="color:#F8F9F9;"> </i> Reporte Por Registros: <?php echo $databanco['banco']; ?> </a>
         <br>
         <br><b style="color:#5DADE2;" align="left"> Búsqueda:<?php echo $rif; ?>/<?php echo $databanco['rsocial'];?>/<?php echo $databanco['fecha'];?> </b>
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="busquedaregistrosclientesrif" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Preguntas&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Respuestas&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Respuestas&nbsp;&nbsp;&nbsp;</th>
                <th>idcliente</th>
                <th>rlegal</th>
                <th>banco</th>
                <th>fecha</th>
                <th>rifcliente</th>
                <th>rsocial</th>
              </tr>
            </thead>
        </table>
      </div>
    </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta'"/>
        </div>
      </div>

    </div>
  </div>
</div>
<?php  } ?>

<script type="text/javascript">
  function dtCore(){
  var rif=document.getElementById('nrorif').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedaregistrosclientesrif').DataTable({
    "ajax": {
    "url": "api/SelectReporteEncuestaRegistrosRif.php",
          "type": "POST",
          "data": {"rif":rif}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "pregunta1"},
      {"data": "respuesta1", className: "text-center" },
      {"data": "respuesta2", className: "text-center" },
      {"data": "idcliente", className: "text-center" },
      {"data": "rlegal", className: "text-center" },
      {"data": "banco", className: "text-center" },
      {"data": "fecha", className: "text-center" },
      {"data": "rifcliente", className: "text-center" },
      {"data": "rsocial"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 4, 5, 6, 7, 8,9],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }  
  });
}
  </script>