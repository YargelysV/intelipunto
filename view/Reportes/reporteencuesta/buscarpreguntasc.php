<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $clientes= isset($array[0]) ? $array[0] : "";
  $detalle= isset($array[1]) ? $array[1] : "";
  $fechainicial = isset($array[2]) ? $array[2] : "";
  $fechafinal= isset($array[3]) ? $array[3] : "";
  //echo $cliente;
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_preguntasencuestac($clientes,$fechainicial,$fechafinal);
  $databanco = pg_fetch_assoc($resultados);
  $_SESSION['Volver']=$clientes.'/'.$fechainicial.'/'.$fechafinal;
  $count = pg_num_rows($resultados);
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $clientes; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="detalle"  name="detalle"  value="<?php echo $detalle; ?>" />
<input type="hidden" id="total"  name="total"  value="<?php echo $databanco['totalc']; ?>" />
<?php if ($clientes=='0') {?>
  <input type="hidden" id="nclientes"  name="nclientes"  value="TODOS LOS BANCOS" />
<?php } else { ?>
  <input type="hidden" id="nclientes"  name="nclientes"  value="<?php echo $databanco['nbanco']; ?>" />
<?php } ?>

<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Por Preguntas: <?php if ($clientes=='0') {
           echo 'Todos los bancos';
          } else { echo $databanco['nbanco']; } ?> </a>
          <br>
          <br><b style="color:#000000;" align="left"> TOTAL COMERCIOS CONSULTADOS:</b>
          <b style="color:#000000;"> <?php echo $databanco['totalc']; ?></b>
        </div>
      </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="buscarpreguntasc" style="width: 100%"> 
           <thead>
              <tr>
                <th>correlativo</th>
                <th>Nro.&nbsp;de&nbsp;preguntas</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Preguntas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Recomendación</th>
                <th>Porcentaje</th>
                <th>Redes</th>
                <th>Porcentaje</th>
                <th>Vallas</th>
                <th>Porcentaje</th>
                <th>Internet</th>
                <th>Porcentaje</th>
                <th>No&nbsp;sabe/No&nbsp;contesta</th>
                <th>Porcentaje</th>
                <th>totalb</th>
              </tr>
            </thead>
          </table>
      </div>
    </div>

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteencuesta'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

<script type="text/javascript">
  function dtCore(){
  var clientes=document.getElementById('clientes').value;
  var fechainicial= document.getElementById('fechainicial').value;
  var fechafinal= document.getElementById('fechafinal').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#buscarpreguntasc').DataTable({
    "ajax": {
    "url": "api/selectReporteEncuestaPreguntasc.php",
          "type": "POST",
          "data": {"clientes":clientes,"fechainicial":fechainicial,"fechafinal":fechafinal}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "numpreguntas", className: "text-center"},
      {"data": "nbanco", className: "text-center"  },
      {"data": "preguntas", className: "text-center"},
      {"data": "recomendacion", className: "text-center" },
      {"data": "porcentajerec", className: "text-center" },
      {"data": "redes", className: "text-center" },
      {"data": "porcentajerd", className: "text-center" },
      {"data": "vallas", className: "text-center" },
      {"data": "porcentajevll", className: "text-center" },
      {"data": "internet", className: "text-center" },
      {"data": "porcentajeinter", className: "text-center" },
      {"data": "nc", className: "text-center" },
      {"data": "porcentajenc", className: "text-center" },
      {"data": "totalc"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 0, 14],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });
}
  </script>