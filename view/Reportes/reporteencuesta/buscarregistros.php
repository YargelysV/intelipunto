<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] : "";
  $fechainicial = isset($array[1]) ? $array[1] : "";
  $fechafinal= isset($array[2]) ? $array[2] : "";
  //echo $cliente;
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_buscarregistrosencuesta($cliente,$fechainicial,$fechafinal);
  $databanco = pg_fetch_assoc($resultados);
  $_SESSION['Volver']=$cliente.'/'.$fechainicial.'/'.$fechafinal;
  $count = pg_num_rows($resultados);
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $cliente; ?>" />

<input type="hidden" id="fechainicial" name="fechainicial"  value="<?php echo $fechainicial;?>" />
<?php if ($cliente=='0') {?>
  <input type="hidden" id="nclientes"  name="nclientes"  value="TODOS LOS BANCOS" />
<?php } else { ?>
  <input type="hidden" id="nclientes"  name="nclientes"  value="<?php echo $databanco['banco']; ?>" />
<?php } ?>

<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> Reporte Por Encuestados: <?php if ($cliente=='0') {
          echo 'Todos los bancos';
          } else { echo $databanco['banco']; } ?> </a>
          <br>
     <!--    <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $databanco['ibp'];?></b> -->
        </div>
      </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="busquedaregistros" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>Acción</th>
                <th>ID</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Representante&nbsp;Legal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Persona&nbsp;Contacto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Estatus</th>
                <th>Ejecutivo Asignado</th>
                <th>&nbsp;&nbsp;Fecha&nbsp;&nbsp;</th>
                <th>pregunta</th>
                <th>respuesta</th>
                <th>banco</th>
                <th>nbanco</th>
                <th>codbanco</th>
              </tr>
            </thead>
            <tfoot>
            
                <th colspan="4" style="text-align:left">Total:</th>
            
          </tfoot>
        </table>
      </div>
    </div>
      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>


<script type="text/javascript">
  function dtCore(){
  var clientes=document.getElementById('clientes').value;
  var fechainicial= document.getElementById('fechainicial').value;
  var fechafinal= document.getElementById('fechafinal').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedaregistros').DataTable({
    "ajax": {
    "url": "api/SelectReporteEncuestaRegistros.php",
          "type": "POST",
          "data": {"clientes":clientes,"fechainicial":fechainicial,"fechafinal":fechafinal}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Detalles</a>", className: "text-center"},
      {"data": "idcliente", className: "text-center" },
      {"data": "rifcliente", className: "text-center" },
      {"data": "rsocial", className: "text-center" },
      {"data": "rlegal", className: "text-center" },
      {"data": "pcontac", className: "text-center" },
      {"data": "estatus", className: "text-center" },
      {"data": "ejecutivo", className: "text-center" },
      {"data": "fecha", className: "text-center" },
      {"data": "pregunta", className: "text-center" },
      {"data": "respuesta"},
      {"data": "banco"},
      {"data": "nbanco"},
      {"data": "codbanco"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 10, 11, 12, 13, 14 ],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });

  $('table#busquedaregistros tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#busquedaregistros').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var idcliente=D.idcliente;
        var clientes=document.getElementById('clientes').value;
        var url = "reporteregistrosencuesta?cargar=DatosEncuestaReporte&var="+idcliente+'/'+clientes; 
        $(location).attr('href',url);

  });
}
  </script>