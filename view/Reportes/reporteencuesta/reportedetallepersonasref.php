<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $idclientes= isset($array[0]) ? $array[0] : "";
  $_SESSION['Volver'];

 // $_SESSION['volver']=$POS.'/'.$fechainicial.'/'.$fechafinal;
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_buscardetallespersonasreferidas($idclientes);
  $databanco = pg_fetch_assoc($resultados);

  $count = pg_num_rows($resultados);

?>

<style type="text/css">
.No-Contactado{
    color:#f1f1f1; 
    background:orange;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

    .Contactado{
    color:white; 
    background:#01DF3A;
    border-radius:0.2em;
    padding:.3em;
    container;
  }


  </style>


<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>



<input type="hidden" id="idclientes"  name="idclientes"  value="<?php echo $idclientes; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Detallado Personas Referidas: <?php echo $databanco['razonsocialenc']; ?></a>
             </div>
       </nav>
      
    
    </div>
    <div class="row"  align="center">
<!--     <div class="demo-container" ng-app="Appreportedetallepersonref" ng-controller="Controllerreportedetallepersonref">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
     </div> -->
      <div class="col-12">
        <table class="table table-bordered"  id="reportedetallepersonasreferidas"> 
           <thead>
              <tr>               
                <th>N°</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;Encuestado&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Personas&nbsp;&nbsp;&nbsp;&nbsp;Referidas&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Nº&nbsp;Tlf&nbsp;&nbsp;Personas&nbsp;&nbsp;Referidas&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Servicio&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Representante&nbsp;Legal&nbsp;&nbsp;&nbsp;&nbsp;</th>            
                <th>&nbsp;&nbsp;Teléfono&nbsp;&nbsp;(Cliente)&nbsp;</th>
              </tr>
           </thead>
        </table>
 
      </div> 
      <br>

  <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportepersonasreferidas?cargar=buscarclientepersonref&var=<?php echo $_SESSION['Volver']; ?>'">Volver</button>
        </div>
        
    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){


 var idclientes= document.getElementById('idclientes').value;
/*  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;*/

  $('table#reportedetallepersonasreferidas').DataTable({
    "ajax": {
    "url": "api/SelectReporteDetalladoPersonRef.php",  
          "type": "POST",
          "data": {"idclientes":idclientes}
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "bancoencuestado", className: "text-center"},
      {"data": "razonsocialenc", className: "text-center"},
      {"data": "nombreref", className: "text-center"},
      {"data": "telfenc", className: "text-center" },   
      {"data": "estatus", className: "text-center",
      "render" : function (data, type, full, meta, row) {
         if (data == "1" || data === "1"){
          return "<p class='Contactado'>Contactado</p>";
        }else if (data == "2" || data === "2"){
          return "<p class='No-Contactado'>No Contactado</p>";
        }else { 
          return "";}
        }
      }, 
      {"data": "rifencuestado", className: "text-center" }, 
      {"data": "nombreenc"},
      {"data": "tlfenc2"}
           
      
    ],
    "order" : [[0, "asc"]],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });
}
  </script>
