<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $clientes= isset($array[0])? $array[0] : "";
  $codbanco= isset($array[1])? $array[1] : "";
  $busqueda=isset($array[2] ) ? $array[2] : "" ;
  //echo $cliente;
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_buscarregistrosencuestacliente($clientes);
  $databanco = pg_fetch_assoc($resultados);
  $_SESSION['Volver']=$clientes;
  $count = pg_num_rows($resultados);
  $banco=$codbanco;


?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $clientes; ?>" />
<input type="hidden" id="fechainicial" name="fechainicial"  value="<?php echo $fechainicial;?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
<input type="hidden" id="rlegal" name="rlegal"  value="<?php echo $databanco['rlegal'];?>" />
<input type="hidden" id="rsocial" name="rlegal"  value="<?php echo $databanco['rsocial'];?>" />
<input type="hidden" id="rif" name="rlegal"  value="<?php echo $databanco['rifcliente'];?>" />
<input type="hidden" id="nbanco" name="nbanco"  value="<?php echo $databanco['banco'];?>" />
<input type="hidden" id="fechaenc" name="nbanco"  value="<?php echo $databanco['fecha'];?>" />
<input type="hidden" id="codbanco"  name="codbanco"  value="<?php echo $codbanco; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> Reporte Por Registros: <?php if ($clientes=='0') {
          echo 'Todos los bancos';
          } else { echo $databanco['banco']; } ?> </a>
          <br>
          <br><b style="color:#5DADE2;" align="left">Búsqueda:<?php echo $databanco['rsocial'];?>/<?php echo $databanco['fecha'];?></b>
        </div>
      </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="busquedaregistrosclientes" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Preguntas&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Respuestas&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Respuestas&nbsp;&nbsp;&nbsp;</th>
                <th>idcliente</th>
                <th>rlegal</th>
                <th>banco</th>
                <th>fecha</th>
                <th>rifcliente</th>
                <th>rsocial</th>
                <th>idbanco</th>
              </tr>
            </thead>
        </table>
      </div>
    </div>

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
          <?php if ($busqueda==1 || $busqueda==2 || $busqueda==3 || $busqueda==4) { ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa?cargar=DatosEncuesta&var=<?php echo $enlace; ?>'">Volver</button>
          <?php } else if ($codbanco!=0){ ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta?cargar=buscarregistros&var=<?php echo $codbanco; ?>'">Volver</button>
          <?php } else { ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta?cargar=buscarregistros&var=<?php echo 0; ?>'">Volver</button>
          <?php } ?>  
        </div>

    </div>
  </div>
</div>

<?php  } ?>


<script type="text/javascript">
  function dtCore(){
  var clientes=document.getElementById('clientes').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedaregistrosclientes').DataTable({
    "ajax": {
    "url": "api/SelectReporteEncuestaRegistrosdetalle.php",
          "type": "POST",
          "data": {"clientes":clientes}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "pregunta1"},
      {"data": "respuesta1", className: "text-center" },
      {"data": "respuesta2", className: "text-center" },
      {"data": "idcliente", className: "text-center" },
      {"data": "rlegal", className: "text-center" },
      {"data": "banco", className: "text-center" },
      {"data": "fecha", className: "text-center" },
      {"data": "rifcliente", className: "text-center" },
      {"data": "rsocial"},
      {"data": "idbanco"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 4, 5, 6, 7, 8,9,10],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }  
  });
}
  </script>