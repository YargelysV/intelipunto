<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] : "";
  //echo $cliente;
 
  $_SESSION['Volver']=$cliente;
  //$count = pg_num_rows($resultados);
 
?>

<?php 
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_buscarclientespersonasreferidas($cliente);
  $databanco = pg_fetch_assoc($resultados);
  $count = pg_num_rows($resultados); ?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>
  
<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="nbanco"  name="nbanco"  value="<?php echo $databanco['bancoencuestado']; ?>" /><?php echo $cliente; ?>
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <b style="color:#F8F9F9;">   <b style="color:#F8F9F9;">Búsqueda:<?php if ($cliente=='0') {
       echo 'Todos los bancos';
     } else { echo $databanco['bancoencuestado']; } ?></b> </b>
        </div>
      </nav>
    </div>
  <div class="col-lg-12" align="center" >
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="reportepersonasreferidas"> 
           <thead>
              <tr>               
                <th>N°</th>
                 <th>enlace</th>
                <th>Acción</th>
                <th>&nbsp;&nbsp;Banco&nbsp;&nbsp;Asignado&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Nº&nbsp;Personas&nbsp;&nbsp;Referidas&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                 <th>&nbsp;&nbsp;&nbsp;&nbsp;Representante&nbsp;Legal&nbsp;&nbsp;&nbsp;&nbsp;</th>      
                <th>&nbsp;&nbsp;Teléfono&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
              </tr>
           </thead>
        </table>
 
      </div> 
      <br>
    </div>
  </div> 
        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportepersonasreferidas'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>


  <script type="text/javascript">
  function dtCore(){


 var clientes= document.getElementById('clientes').value;
/*  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;*/

  $('table#reportepersonasreferidas').DataTable({
    "ajax": {
    "url": "api/selectReportePersoRef.php",  
          "type": "POST",
          "data": {"clientes":clientes}
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "idclientes", className: "text-center" },
      {"defaultContent": "<p class='abrir'><a class='abrir' type='href' title='Ver'>Ver Detalles</a></p>", className: "text-center" },
      {"data": "bancoencuestado", className: "text-center" },
      {"data": "razonsocialenc", className: "text-center"},
      {"data": "cont", className: "text-center" },
      {"data": "rifencuestado", className: "text-center" },
      {"data": "nombreenc"},
      {"data": "tlfenc2"}
           
      
    ],
    "order" : [[0, "asc"]],
 "columnDefs": [
                 {
                       "targets": [1],
                       "visible": false,
                   }
               ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [ 'print'

    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

    $('table#reportepersonasreferidas tbody').on( 'click', 'a.abrir', function () {
    //alert('algo');
          var table=$('table#reportepersonasreferidas').DataTable();
          var D =  table.row($(this).parents("tr")).data();
          var idclientes=D.idclientes;
         
          var url = "reportepersonasreferidas?cargar=reportedetallepersonasref&var="+idclientes; 
          $(location).attr('href',url);

    });

}

  </script>
