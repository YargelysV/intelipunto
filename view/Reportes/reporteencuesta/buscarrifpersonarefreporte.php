<?php
/*buscarrif.php view*/
 $encuesta=$_SESSION["encuesta"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $_SESSION['buscarrif']=$var;
  $_SESSION['bsqrecepcion']='';
  $array = explode("/",$var);
  $rif= $array[0];
  //echo $rif;
  
  
  $controladorc =new ControladorReporte();
  $comercia= $controladorc->sp_buscarrifpersonasreferidas($rif);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="" style="color:#F8F9F9;"> </i> Reporte Personas Referidas: <?php echo $databanco['razonsocialenc']; ?> </a>
         <br>
         <br><b style="color:#5DADE2;" align="left"> Búsqueda:<?php echo $rif; ?> </b>
        </div>
       </nav>
    </div>
    <div class="row" align="center">
    <!-- <div class="demo-container" ng-app="AppRifPersonasRef" ng-controller="RifPersonasRef">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div> -->
            <div class="col-12">
        <table class="table table-bordered"  id="reportepersonasreferidasxrif"> 
           <thead>
              <tr>               
                <th>N°</th>
                 <th>enlace</th>
                <th>Acción</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Nº&nbsp;Personas&nbsp;&nbsp;Referidas&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                 <th>&nbsp;&nbsp;&nbsp;&nbsp;Representante&nbsp;Legal&nbsp;&nbsp;&nbsp;&nbsp;</th>      
                <th>&nbsp;&nbsp;Teléfono&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Banco&nbsp;&nbsp;Asignado&nbsp;&nbsp;</th>
              </tr>
           </thead>
        </table>
 
      </div> 

      <div class="row">
        

    </div>
    <br>
    <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportepersonasreferidas'">Volver</button>
        </div>
  </div>
</div>
<?php  } ?>


  <script type="text/javascript">
  function dtCore(){


 var nrorif= document.getElementById('nrorif').value;
/*  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;*/

  $('table#reportepersonasreferidasxrif').DataTable({
    "ajax": {
    "url": "api/SelectReportePersonRefRif.php",  
          "type": "POST",
          "data": {"nrorif":nrorif}
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "idclientes", className: "text-center" },
      {"defaultContent": "<p class='abrir'><a class='abrir' type='href' title='Ver'>Ver Detalles</a></p>", className: "text-center" },
      {"data": "razonsocialenc", className: "text-center"},
      {"data": "cont", className: "text-center" },
      {"data": "rifencuestado", className: "text-center" },
      {"data": "nombreenc"},
      {"data": "tlfenc2"},
      {"data": "bancoencuestado"}          
      
    ],
    "order" : [[0, "asc"]],
    "columnDefs": [
                 {
                       "targets": [1],
                       "visible": false,
                   }
               ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

    $('table#reportepersonasreferidasxrif tbody').on( 'click', 'a.abrir', function () {
    //alert('algo');
          var table=$('table#reportepersonasreferidasxrif').DataTable();
          var D =  table.row($(this).parents("tr")).data();
          var idclientes=D.idclientes;
         
          var url = "reportepersonasreferidas?cargar=reportedetallepersonasref&var="+idclientes; 
          $(location).attr('href',url);

    });

}

  </script>