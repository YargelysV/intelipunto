 <?php

   $usuario=$_SESSION["session_user"];
   $var=($_REQUEST['var']);
   $_SESSION['busrecepcion']=$var;
   $array =explode("/",$var);   
   $clientes= isset($array[0]) ? $array[0] : "";
   $fechainicial = isset($array[1]) ? $array[1] : "";
   $fechafinal= isset($array[2]) ? $array[2] : "";

  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_buscarreporteequipos($clientes,$fechainicial,$fechafinal);
  $row = pg_fetch_assoc($resultados);

  $count = pg_num_rows($resultados);

  $nfechadesde = new DateTime($fechainicial);
  $nfechahasta = new DateTime($fechafinal);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="" id="clientes"  name="clientes"  value="<?php echo $clientes; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />

<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
<nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte de Equipos POS:</a>
          
        </div>
       </nav>
 <b style="color:#5DADE2;">Búsqueda: <?php if ($clientes!='') echo $row['ibp'];  else echo $nfechadesde->format('d-m-Y');?> / <?php echo $nfechahasta->format('d-m-Y');?> </b>
        </div>

    <div class="row" align="center">
    <div class="demo-container" ng-app="AppReporteEquipos" ng-controller="ReporteEquiposController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div>

       </div>
       </div>
         <br>
 <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='reporteequipos.php'"/>
        </div>
      </div> 
  </div>

</div>


<?php  } ?>
