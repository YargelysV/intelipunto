<?php
$tituloPagina=$_SESSION["page"];
if (isset($_SESSION["session_user"]))
{
  $usuario=$_SESSION["session_user"];
}

?>
     <input type="hidden" id="tipousuario" value='<?php echo $_SESSION["codtipousuario"];?>' name="tipousuario"/>
           <input type="hidden" id="ejecutivo" value='<?php echo $_SESSION["ejebanco"]?>' name="ejecutivo"/>
           <input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"]?>' name="usuario"/>

<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i style="color:#01DFD7;"> </i> <?php echo $tituloPagina;?> </h1>
      </div>


      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              Buscar Registro de Factura
            </div>
          <div class="panel-body">
            <div>
                <div class="input-group custom-search-form center">
                       <input type="text" id="factura" class="form-control" placeholder="Ej. 123456789...">
                          <span class="input-group-btn">
                           <button class="btn btn-default" type="button" onclick="buscarfacturapos()">
                                    <i class="fa fa-search"></i>
                          </button>
                        </span>
                 </div>
                <br>
            </div>
          </div>
             <div class="panel-footer">
  <br>
            </div>
          </div>
        </div>
      </div> <!-- end panel busqueda por rif -->


    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->



