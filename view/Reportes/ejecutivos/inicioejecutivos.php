<?php
  $tituloPagina=$_SESSION["page"];
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
    $_SESSION["comercializacionproceso"]="1";
   
  }
?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i style="color:#A01010;" class="fa fa-group"></i> Reporte de Ejecutivos POS</h1>
      </div>
      
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-yellow">
            <div class="panel-heading">
              BUSCAR REPORTE DE EJECUTIVOS
            </div>
            
           <table border="0" align="center">
             <br>
             <div>
              <div class="col-lg-5">Banco:</div>
              <div class="col-lg-5">
                <select name="clientes" id="clientes" required>
                <option value="">Seleccione Banco</option>
                </select>
              </div>
              <br>
            </div>

            <?php if ($_SESSION["superusuario"]=='E') {?>
            <div>

            <div class="col-lg-5">Ejecutivo:</div>
              <div class="col-lg-5">
                <input type="text" value="" name="ejecutivo" id="ejecutivo"  readonly />
              </div>
               <br>
            </div>
            <?php } 

              else {?>

                <div class="form-group">
                  <div class="col-lg-5">Ejecutivo</div>
                  <div class="col-lg-5">
                 <select name="ejecutivo" id="ejecutivo" required>
                      <option value="">Seleccione Ejecutivo</option>
                    </select>
                </div>
                </div>

                 <?php } ?>
             <style type="text/css">
              .dias_RE{
                position: relative;
                left: 30%;
                bottom: 10px;
              }

              .fecha_RE{
                position: relative;
                top: 40px;
                right: 80%;
              }

              @media only screen and (max-width: 1200px){
                .fecha_RE{
                  position: relative;
                  top: 0px;
                  margin: 0 0 5px 5px;
                  left: 10px;



                }
              }
              @media only screen and (max-width: 1200px) {
              .dias_RE {
                       
                       position: relative;
                       top: 0px;
                       left: 15px;
                       margin-left: 5px;
                  }
            }
            </style>

                  <div class="fecha_RE">Fecha:</div>  
                  <tr align="center">
                    <td class="dias_RE" align="center">DESDE </td>
                    <td class="dias_RE" align="center">HASTA</td>
                  </tr>
                  <tr align="center">
                    <td class="dias_RE" align="center"><input type="date" name="fechainicial" id="fechainicial" class="inputcentrado"  style="color: #000000" required="required" max="<?php  echo date("Y-m-d");?>" onchange="validarfechasreporte()" value="">
                    </td>
                    <td class="dias_RE" align="center"><input type="date" name="fechafinal" id="fechafinal" class="inputcentrado"  style="color: #000000" required="required" max="<?php  echo date("Y-m-d");?>" onchange="validarfechasreporte()" value="" >
                    </tr>

                  </table>
          <br>
            <input type="hidden" id="valor" value="10" name="valor"/>
            <script languaje="javascript">
              generarcliente();
            </script>
            <div class="form-group">
                    <div style="text-align: center;">
                      <input class="btn btn-success" type="button" id="Buscar" name="Buscar" value="Buscar" onClick="buscarejecutivos()" disabled="disabled"/>
                    </div>
                  </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>