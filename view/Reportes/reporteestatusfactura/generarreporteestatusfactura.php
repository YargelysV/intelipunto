<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= $array[0];
  $estatus= $array[1];

 // $_SESSION['volver']=$POS.'/'.$fechainicial.'/'.$fechafinal;
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_reporteestatusfacturacion($banco,$estatus);
  $databanco = pg_fetch_assoc($resultados);

  $count = pg_num_rows($resultados);

?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<style type="text/css">
  .Pago-Confirmado{
    
    color:white;
    background:#01DF3A;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

  .Pendiente-Por-Pagos{
     
    color:#f1f1f1;
    background:orange;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

  .Pendiente-Por-Confirmar{
    
    color:#f1f1f1;
    background:red;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 

  }

  .Inteliservices{
    
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

    .Cliente-Inteligensa{
    
    color:#f1f1f1;
    background:#62ABCD;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }
    .Migracion{
    
    color:#f1f1f1;
    background:#C94E71;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

    .Comodato{
    
    color:#f1f1f1;
    background:#BA55D3;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

</style>

<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />

<input type="hidden" id="estatus"  name="estatus"  value="<?php echo $estatus; ?>" />


<input type="hidden" id="tipousuario" value='<?php echo $_SESSION["codtipousuario"];?>' name="tipousuario"/>
<input type="hidden" id="ejecutivo" value='<?php echo $_SESSION["ejebanco"]?>' name="ejecutivo"/>
<input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"]?>' name="usuario"/>



 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Estatus Pagos:</a>
             </div>
       </nav>
      <b style="color:#5DADE2;"></b>    <b style="color:#5DADE2;">Búsqueda:<?php if ($banco=='0') {
       echo 'Todos los bancos';
     } else { echo $databanco['ibp']; } ?></b> 
    
    </div>
    <div class="row"  align="center">

          <div class="col-12">
        <table class="table table-bordered"  id="reporte_equiposfactura"> 
           <thead>
              <tr>
                
                <th>correlativo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;Pago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>enlace</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;del&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Ejecutivo Asignado</th>
                <th>Región Asignada</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;Estatus&nbsp;Cliente&nbsp;</th>
                <th>N°&nbsp;Afiliado</th>
                <th>Tipo de Pos</th>
                <th>Cantidad de Pos</th>
                <th>Fecha de Recepción</th>
                <th>Fecha de Carga</th>
                <th>Nombre Archivo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Banco</th>            
              </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="4" style="text-align:left">Total:</th>

            </tr>
        </tfoot>
        </table>
 
      </div>

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportefacturaestatus'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var banco= document.getElementById('banco').value;
  var estatus= document.getElementById('estatus').value;


  $('table#reporte_equiposfactura').DataTable({
    "ajax": {
    "url": "api/selectFacturacionReporte.php",  
          "type": "POST",
          "data": {"banco":banco, "estatus":estatus}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "estatuspago", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data == "1" || data === "1") {
        return "<p class='Pago-Confirmado'>Pago Confirmado</p>";
      }else if (data == "2" || data === "2"){
        return "<p class='Pendiente-Por-Pagos'>Pendiente Por Pagos</p>";
      }else if (data == "0" || data === "0"){
        return "<p class='Pendiente-Por-Confirmar'>Pendiente Por Confirmar</p>";
      }else{
        return "";
      }
      }
  },
      {"data": "enlaceafiliado" },
      {"defaultContent":<?php if ($estatus!=2) {?>"<a class='editar' style='cursor:pointer;' type='href' title='Ver'>Datos Facturación</a>" <?php }else{ ?> "<a>-</a>" <?php } ?>, className: "text-center"},
      {"data": "descequipo", className: "text-center" },
      {"data": "ejecutivo", className: "text-center" },
      {"data": "region_ejec", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "estatusmigra", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if       (data == "0" || data === "0" || data === "" ||  data == "" || data==null) {
        return "<p class='Cliente-Inteligensa'>Cliente Inteligensa</p>";

      }else if (data == "1" || data === "1"){
        return "<p class='Migracion'>Migración</p>";

      }else if (data == "2" || data === "2"){
        return "<p class='Comodato'>Comodato</p>";

      }else{
        return "";
      }} },
      {"data": "nafiliacion", className: "text-center" },
      {"data": "descmodelo"},
      {"data": "cantidadterminales"},
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "idate", className: "text-center" },
      {"data": "namearchivo", className: "text-center" },
      {"data": "direccion"},
      {"data": "ibp"}
    ],
    "order" : [[0, "asc"]],
    
    "columnDefs": [
           {
                 "targets": [ 2,15 ],
                 "visible": false,
             }
         ],

    "scrollX": 2900,
    "scrollY": false,
    "info": false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
     buttons: [
     {
            extend: 'excelHtml5',
            text: 'Exportar Excel', 
            title: 'Reporte Equipos Vendidos', 
               
        }
     ],
    
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }



  });

        $('table#reporte_equiposfactura tbody').on( 'click', 'a.editar', function () {
      //alert('algo');
        var table=$('table#reporte_equiposfactura').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlaceafiliado=D.enlaceafiliado;
        var estatusmigra=D.estatusmigra;
        var url = "facturacionpos?cargar=facturarcliente&var="+enlaceafiliado+'/'+estatusmigra; 
        $(location).attr('href',url);
      
  });

}
  </script>