<?php
set_time_limit(0);
//error_reporting(0);
if (isset($_SESSION["session_user"]))
{

	$name=$_FILES['archivo']['name'];
	$type=$_FILES['archivo']['type'];				
	$tipo="application/vnd.ms-excel";
	
	//BUSCAR SI EL REGISTRO EXISTE
	$controladorclientes =new ControladorReporte();
	$cliente=$controladorclientes->verclientescomercializacion();
	$clientact = 0;
	$clienteactivos=array();
	while($cliea = pg_fetch_array($cliente)){
	$clienteactivos[$clientact] =$cliea['cbanco'].$cliea['razon'].$cliea['documento'].$cliea['registro'];
	//$clienteactivos[$clientact] =$cliea['ibp'].$cliea['cbanco'].$cliea['tipopv'].$cliea['cantidadpos'].$cliea['tipoline'].$cliea['tipomodelo'].$cliea['tipoclient'].$cliea['razon'].$cliea['documento'].$cliea['formapago'].$cliea['transferencia'];
	//echo $clienteactivos[$clientact]."<br>";
	$clientact++;
	}	

	//BUSCAR AFILIADO EXISTE
	$controladorafiliado =new ControladorReporte();
	$afiliadoc=$controladorafiliado->verafiliadosasignados();
	$afiliadoact = 0;
	$afiliadosactivos=array();
	while($afilia = pg_fetch_array($afiliadoc)){
	$afiliadosactivos[$afiliadoact] =$afilia['afiliado'];
	//echo $afiliadosactivos[$afiliadoact]."<br>";
	$afiliadoact++;
	}	

	// if ($type!=$tipo){
	// 	echo "<script>alertify.alert('Error Cargando el Archivo, Verifique e intente nuevamente.', function () {
	// 		window.location.assign('PrintFile.php');});</script>";
	// }
	// else{

		$date = date("Y-n-j H:i:s");
		$archivo = $_FILES['archivo']['tmp_name'];
		$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");

		?>
		<div class="sub-content" align="center" >
			<div id="page-wrapper">
	            <div class="container-fluid">
	                <div class="row" id="errores">
	                	<div class="col-lg-12">
                        <h1 class="page-header">Recepción Archivo Gestión Comercial</h1>
                    	</div>
	                	<br>	
						<?php


						$cont=0;
						$control=0;
						while($data = fgetcsv($fichero,10000,";"))
						{
							$cont++;
							if ($cont==1)
							{
								if ((utf8_encode(trim($data[0]))!="Consecutivo") ||(utf8_encode(trim($data[1]))!="Nº Registro")||(utf8_encode(trim($data[2]))!="Fecha de envío del archivo") ||(utf8_encode(trim($data[3]))!="Nombre del Banco Adquiriente") ||(utf8_encode(trim($data[4]))!="Prefijo del Banco") || (utf8_encode(trim($data[5]))!="Tipo de Punto de Venta") ||(utf8_encode(trim($data[6]))!="Cantidad de Punto de Venta") ||(utf8_encode(trim($data[7]))!="Tipo de Línea") ||(utf8_encode(trim($data[8]))!="Tipo módelo de negocio")||(utf8_encode(trim($data[9]))!="Tipo de Cliente") ||(utf8_encode(trim($data[10]))!="Razón social / Nombre del afiliado")||(utf8_encode(trim($data[11]))!="Rif / CI") ||(utf8_encode(trim($data[12]))!="N° de Afiliación"))
								{
								
								//echo utf8_encode(trim($data[0])).'-'.utf8_encode(trim($data[1])).'-'.utf8_encode(trim($data[2])).'-'.utf8_encode(trim($data[3])).'-'.utf8_encode(trim($data[4])).'-'.utf8_encode(trim($data[5])).'-'.utf8_encode(trim($data[6])).'-'.utf8_encode(trim($data[7])).'-'.utf8_encode(trim($data[8])).'-'.utf8_encode(trim($data[9])).'-'.utf8_encode(trim($data[10])).'-'.utf8_encode(trim($data[11])).'-'.utf8_encode(trim($data[12]));

								echo "<script>alertify.alert('El orden del formato es incorrecto', function () {
								 window.location.assign('reportegestion.php?cargar=adjuntar');});</script>";	
								$control=2;
								}
							}
							else if (($cont>1) && ($control!=2))
							{ 
								
								if(!(in_array(($data[4]).($data[10]).($data[11]).($data[1]), $clienteactivos)))
								{
									 echo '<h4 style="color:red;">'.'ERROR  "Registro no existe en nuestro sistema, con estas características". En la linea '.$cont.' verifique la información e intente nuevamente. </h4>
									 	- Nº Registro: '.$data[1].'<br>
									 	- Fecha de Envío del Archivo: '.$data[2].'<br>
									 	- Nombre del Banco Adquiriente: '.$data[3].'<br>
									 	- Prefijo del Banco: '.$data[4].'<br>
									 	- Tipo de Punto de Venta: '.$data[5].'<br>
									 	- Cantidad de Punto de Venta: '.$data[6].'<br>
									 	- Razón social / Nombre del afiliado: '.$data[10].'<br>
									 	- Rif / CI: '.$data[11].'<br>
									 	';

									 	echo $control;
									 $control = 1;
								}

								if((in_array(($data[12]), $afiliadosactivos)))
								{
									echo '<h4 style="color:red;">'.'ERROR  "Número de Afiliado ya esta asignado a otro registro o el registro ya se encuentra en el sistema actualizado".<br> En la linea '.$cont.' verifique la información e intente nuevamente. </h4> <br>';

									$cafiliadodetalle =new ControladorReporte();
									$afdetalle=$cafiliadodetalle->verafiliadosasignadosdetalle($data[12]);
									$afilia = pg_fetch_assoc($afdetalle);

									echo '<h4 style="color:red;">'.'Asignado al siguiente registro:</h4> 
										- Nombre del Banco Adquiriente: '.$afilia['ibp'].'<br>
										- Razón social / Nombre del afiliado: '.$afilia['razon'].'<br>
										- Rif / CI: '.$afilia['documento'].'<br><br>
										';
									$control = 1;
								}

								if((strlen($data[0]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo CONSECUTIVO en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if(strlen($data[1]) == '')
								{
								echo '<h4 style="color:red;">'.'ERROR en campo Nº Registro en la linea'.$cont.'</h4><br>';
								$control = 1;	
								}

								if(strlen($data[2]) == '')
								{
								echo '<h4 style="color:red;">'.'ERROR en campo FECHA DE ENVÍO DEL ARCHIVO en la linea'.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[3]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo NOMBRE DEL BANCO ADQUIRIENTE en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[4]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo PREFIJO DEL BANCO en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[5]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo TIPO DE PUNTO DE VENTA en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[6]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo CANTIDAD DE PUNTO DE VENTA en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[7]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo TIPO DE LÍNEA en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								/*if((strlen($data[8]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo TIPO MÓDELO DE NEGOCIO en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}*/

								if((strlen($data[9]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo TIPO DE CLIENTE en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[10]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo RAZÓN SOCIAL en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[11]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo RIF/CI en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								/*if((strlen($data[12]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo FORMA DE PAGO en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}

								if((strlen($data[12]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo Nº AUTORIZACIÓN DE LA TRANSFERENCIA en la linea '.$cont.'</h4><br>';
								$control = 1;	
								}*/

								if((strlen($data[12]) == ''))
								{
								echo '<h4 style="color:red;">'.'ERROR en campo N° DE AFILIACIÓN en la linea '.$cont.', No puede estar en blanco.</h4><br>';
								$control = 1;	
								}
							}	
						}//fin while
						if($control==1)
						{
						?>
							<table border="0" width="300" align="center">
								<tr>
									<td height="100" colspan="2" align="center">
									<font face="impact" size="5" color="red">
									Posibles Causas de error:<br>
									</font>
									</td>
								</tr>
									<td height="100" colspan="2" align="center">
									<font face="arial" size="4" color="green">
									- Verifique archivo .CSV<br>
									- El archivo debe contener cabecera, en el orden indicado.<br>
									- Ningun campo debe venir en blanco, y debe estar delimitado las columnas por punto y coma (;) .<br>
									- Consulte con el administrador del Sistema.<br>
									 </font>
									</td>
								</tr>
								<tr>
									<td height="100" colspan="1" align="center">
									<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='reportegestion?cargar=adjuntar'"/>
									</td>
								</tr>
							</table>
						<?php
						}
						?>				
					</div>
				</div>
			</div>
		</div>				
		<?php
		if($control == 0){
			$date = date("Y-n-j H:i:s");
			$name=$_FILES['archivo']['name'];
			$archivo = $_FILES['archivo']['tmp_name'];
			$fichero1 = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
			$count=0;
			$controlador1 =new ControladorReporte();
			while($data1 = fgetcsv($fichero1,10000,";"))
			{
				if($count>0)
				$insertar=$controlador1->insertar_afiliado(trim($data1[0]),trim($data1[1]),trim($data1[2]),trim($data1[3]),trim($data1[4]),trim($data1[5]),trim($data1[6]),trim($data1[7]),trim($data1[8]),trim($data1[9]),trim($data1[10]),trim($data1[11]),trim($data1[12]),$_SESSION["session_user"]);
				$count++;
			}
			    echo "<script>alertify.alert('Archivo Cargado con &Eacute;xito', function () {
			   window.location.assign('reportegestion.php');});</script>";	
		}	
	//}
}
?>

			