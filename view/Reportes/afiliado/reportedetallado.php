<?php
  error_reporting(0);
  $tituloPagina=$_SESSION["page"];
  $usuario=$_SESSION["session_user"];
  $variable=($_REQUEST['var']);
  $array = explode(",", $variable);
  $cliente= $array[0];
  $fechaenvio = $array[1]; 
  $fecharecepcion = $array[2]; 
  //$fechaenvio= $array[1];
  //$fecharecepcion= $array[2];
  
  $controladorcomer =new ControladorReporte();
  $comercia=$controladorcomer->comercializacionreportedetallado($cliente,$fechaenvio, $fecharecepcion);
  $datadetallada = pg_fetch_assoc($comercia);
  
  $controladorc =new ControladorReporte();
  $comerciali=$controladorc->spbanco($cliente);
  $databanco = pg_fetch_assoc($comerciali);
?>

<input type="hidden" id="cliente" name="cliente" value="<?php echo $cliente; ?>" />
<input type="hidden" id="fechaenvio" name="fechaenvio" value="<?php echo $fechaenvio; ?>" />
<input type="hidden" id="fecharecepcion" name="fecharecepcion" value="<?php echo $fecharecepcion; ?>" />
 
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> Reporte detallado Cliente: <b style="color:#F8F9F9;"><?php echo $databanco['ibp'];?></a>
          <br>          
          <a>Fecha envío del archivo:  <b style="color:#FF5733;"><?php echo $fechaenvio;?></a>
          <br>
          <a>Fecha recepción del archivo:  <b style="color:#FF5733;"><?php echo $fecharecepcion;?></a>
          <br>
          <a>Estatus:  <b style="color:#FF5733;"><?php echo $datadetallada['estatusafiliado'];?></a>
        </div>
       </nav>
    </div>
      <br><br>

    <div class="row">
        <?php
        if ($datadetallada['estatusafiliado']!='Por Enviar') {?>
          <div class="demo-container" ng-app="AppGestionComercialReporteDetallado" ng-controller="GestionComercialReporteDetalladoController">
              <div id="gridContainer" dx-data-grid="gridOptions"></div>
          </div>
          <?php
        } else {
          ?>
           <br>
         <div> Debe procesar la información para poder acceder al detalle de la misma, Verifique la información en el Módulo 
        <a href="?cargar=buscargestion&operacion=3&var=<?php  echo $cliente;  ?>" > Generar Excel</a></div>
        </div>
        <br> <br>
           <?php
          } 
          ?>
        

    <br>
      <div class="row">
        <div class="col-md-5 col-md-offset-5">
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarreporte&var=<?php echo $cliente; ?>'"/>
        </div>
      </div>
     
    </div>
  </div>
</div>
  
