﻿<?php
  $tituloPagina=$_SESSION["page"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $operacion='3';
  $cliente= isset($array[0]) ? $array[0] :"";
  $fecha= isset($array[1]) ? $array[1] :"";
  
  $controladorcomer =new ControladorReporte();
  $databanco=$controladorcomer->sp_comercializacionbuscar($operacion,$cliente,$fecha);
  
  $controladorbanco =new ControladorReporte();
  $banconame=$controladorbanco->spbanco($cliente);
  $dbanco = pg_fetch_assoc($banconame);
 
  $total = pg_num_rows($databanco);

 ?>

 <input type="hidden" id="operacion" name="operacion" value="<?php echo $operacion; ?>" />
 <input type="hidden" id="cliente" name="cliente" value="<?php echo $cliente; ?>" />
  <input type="hidden" id="namecliente" name="namecliente" value="<?php echo $dbanco['ibp']; ?>" />
 <input type="hidden" id="fecha" name="fecha" value="<?php echo  $fecha;?>" />
  <input type="hidden" id="usuario" name="usuario" value="<?php echo  $usuario;?>" />
  <input type="hidden" id="procesarinformacion" name="procesarinformacion" value="<?php echo  $_SESSION["comercializacionproceso"];;?>" />

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#35474F;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#00FFFF;"> <?php echo $tituloPagina;?> Clientes: <b style="color:#337AB7;"> <?php echo $dbanco['ibp'];?></a>
        </div>
       </nav>
    </div>
     <div>
      <?php
      if ($total>0)
      {
        if ($_SESSION["comercializacionproceso"]=='1') {?>
          <font color="red">Se debe procesar el archivo a enviar al banco, para poder descargar: </font>
          <input type="button" class="btn btn-success"  name="clientesbanco" id="clientesbanco" value="Procesar" onclick="procesarclientesbanco()"  />
          <?php 
        } else if ($_SESSION["comercializacionproceso"]=='2') { ?>
            <font color="red">Archivo procesado con éxito, puede proceder a descargar </font>
            <input type="button" class="btn btn-success"  name="clientesbanco" id="clientesbanco" value="Procesar"  disabled="disbled" />
        <?php } 
        ?>
      </div>
      <br>
    <div class="row">
    <div class="demo-container" ng-app="AppGestionComercial" ng-controller="GestionComercialController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
    </div> 
     <?php } else{ ?>
        <div class="row">
        <div> No existe información para procesar de este cliente, Verifique la información en el Módulo 
        <a href="?cargar=buscarreporte&var=<?php  echo $cliente;  ?>" > Buscar Reporte</a></div>
        </div>
        <br> <br>
       <?php } ?>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='reportegestion.php'"/>
        </div>
      </div>
     
    </div>
  </div>
</div>
  
