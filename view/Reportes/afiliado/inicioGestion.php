<?php
  $tituloPagina=$_SESSION["page"];
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
    $_SESSION["comercializacionproceso"]="1";
  }
?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i style="color:#A01010;" class="fa fa-list-alt"> </i> Reporte de Afiliado 
        </h1>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-yellow">
            <div class="panel-heading">
              BUSCAR CLIENTES PARA ENVIAR AL BANCO
            </div>
            <div class="panel-body">
              <div>
                <div class="col-lg-5">Banco:</div>
                <div class="col-lg-5">
                    <select name="clientes" id="clientes" required>
                    <option value="">Seleccione Banco</option>
                    </select>
                </div>
                 <div class="col-lg-5">Fecha de Envío del Reporte:</div>
                <div class="col-lg-5">
                    <select name="fecha" id="fecha" required>
                    <option value="">Seleccione Fecha</option>
                    </select>
                </div>
                <br>
              </div>
            </div>
            <input type="hidden" id="valor" value="5" name="valor"/>
            <script languaje="javascript">
              generar();
            </script>

            <?php
         if ($_SESSION["reportegestion"]=='W') {?>
            <div class="panel-footer">
              <p style="text-align: center;">
                <input class="btn btn-success" type="button" name="Generar" value="Generar Excel" onClick="buscarclientecomercial()"/>
                <input class="btn btn-primary" type="button" name="buscar" value="Buscar Reporte" onClick="buscarreporte()"/>
                <input class="btn btn-warning" type="button" name="crear" value="Adjuntar Archivo" onClick="window.location.href='?cargar=adjuntar'"/>
              </p>
            </div>
              <?php }
             else {
            ?>
               <div class="panel-footer" align="center">
             <input class="btn btn-success" type="button" name="Generar" value="Generar Excel" onClick="buscarclientecomercial()"/>
                <input class="btn btn-primary" type="button" name="buscar" value="Buscar Reporte" onClick="buscarreporte()"/>
             <?php } ?>
           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




