<?php
  $tituloPagina=$_SESSION["page"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] :"";
  $fecha= isset($array[1]) ? $array[1] :"";

 $controladorc =new ControladorReporte();
  $resultados= $controladorc->comercializacionreporte($cliente,$fecha);
  $databanco = pg_fetch_assoc($resultados);
?>

<input type="hidden" id="cliente" name="cliente" value="<?php echo $cliente; ?>" />
 <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#35474F;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#00FFFF;"> Reporte de Afiliado Cliente: <b style="color:#FF5733;"><?php echo $databanco['ibp'];?></a>
        </div>
       </nav>
    </div>
      <br>
    <div class="row">
    <div class="demo-container" ng-app="AppGestionComercialReporte" ng-controller="GestionComercialReporteController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
    </div> 
    <br>
      <div class="row">
          <div class="col-md-5 col-md-offset-5">
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='reportegestion.php'"/>
        </div>
      </div>
     
    </div>
  </div>
</div>
  
