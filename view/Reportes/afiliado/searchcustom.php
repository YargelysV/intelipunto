<?php
  $tituloPagina=$_SESSION["page"];
  $usuario=$_SESSION["session_user"];
  $operacion=($_REQUEST['operacion']);
  $cliente=($_REQUEST['cliente']);
  $fecha = isset($_REQUEST['fecha']) ? $fecha : "";

  //busqueda generalizada por  banco y fecha
  $controladorcomer =new ControladorReporte();
  $comercia=$controladorcomer->sp_comercializacionbuscar($operacion,$cliente,$fecha);

  $controladorcomer1 =new ControladorReporte();
  $comercia1=$controladorcomer1->sp_comercializacionbuscar($operacion,$cliente,$fecha);
  $databanco = pg_fetch_assoc($comercia1);
  $total = pg_num_rows($comercia1);

?>



<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#35474F;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#00FFFF;"> <?php echo $tituloPagina;?> Clientes: <b style="color:#FF5733;"><?php echo $databanco['ibp'];?></a>
        </div>
       </nav>
    </div>
    <div class="row">
    <div class="demo-container" ng-app="AppGestionComercial" ng-controller="GestionComercialController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='gestioncomercial.php'"/>
        </div>
      </div>
    </div>
  </div>
</div>
