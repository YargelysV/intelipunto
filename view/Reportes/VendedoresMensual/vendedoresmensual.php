<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $fechainicial= isset($array[0]) ? $array[0] : "";
  $fechafinal= isset($array[1]) ? $array[1] : "";
   $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_buscarvendedoresmensual($fechainicial,$fechafinal);
  $databanco = pg_fetch_assoc($resultados);
  $_SESSION['Volver']=$fechainicial.'/'.$fechafinal;
  $count = pg_num_rows($resultados);
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Mensual de Vendedores: </a>
          <br>
     <!--    <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $databanco['ibp'];?></b> -->
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="vendedormensual" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivos</th>
                <th>&nbsp;&nbsp;Región&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Ejecutivos&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Cantidad</th>
                <th>Ventas Con Fondos</th>
                <th>POS Facturados</th>
                <th>POS Instalados</th>
                <th>POS Declinados</th>
              </tr>
            </thead>
            <tfoot align="right">
              <th></th><th></th><th></th><th></th><th></th><th></th><th></th>
            </tfoot>
        </table>
      </div>
    </div>
      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportevendedoresmensual'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

<script type="text/javascript">
  function dtCore(){
  var fechainicial=document.getElementById('fechainicial').value;
  var fechafinal=document.getElementById('fechafinal').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#vendedormensual').DataTable({
    "ajax": {
    "url": "api/SelectReporteVendedoresMensual.php",
          "type": "POST",
          "data": {"fechainicial":fechainicial, "fechafinal":fechafinal}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "region", className: "text-center"},
      {"data": "ejecutivos"},
      {"data": "cantidad", className: "text-center" },
      {"data": "ventasconfondos", className: "text-center" },
      {"data": "facturado", className: "text-center" },
      {"data": "pos_instalados", className: "text-center" },
      {"data": "declinacion", className: "text-center" }
    ],
    "order" : [[0, "asc"]],
    
    dom: 'Bfrtip',
     buttons: [
     {
             extend: 'excelHtml5', footer: true,
            text: 'Exportar Excel',
            title: 'Reporte Vendedores Mensual', 
               
        }
     ],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html(
                'Total:'+lastRow 
            );

            // Total over all pages
            totalcartera = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 3 );
 
            // Total over this page
            totalventasfondo = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 4 );

            totalfacturados = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 5 );

            totalintalados = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 6 );

            totaldeclinados = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 7 );
      
            // Update footer
            $( api.column( 1 ).footer() ).html('');
            $( api.column( 2 ).footer() ).html('Total: '+totalcartera);
            $( api.column( 3 ).footer() ).html('Total: '+totalventasfondo);
            $( api.column( 4 ).footer() ).html('Total: '+totalfacturados);
            $( api.column( 5 ).footer() ).html('Total: '+totalintalados);
            $( api.column( 6 ).footer() ).html('Total: '+totaldeclinados);
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }  
  });
}
  </script>