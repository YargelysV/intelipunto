<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] : "";
  $ejecutivo= isset($array[1]) ? $array[1] : "";
  $fechainicial = isset($array[2]) ? $array[2] : "";
  $fechafinal= isset($array[3]) ? $array[3] : "";
  $tiporeporte=isset($array[4]) ? $array[4] : "";
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_reporteejecutivogestiones($fechainicial,$fechafinal);
  $databanco = pg_fetch_assoc($resultados);

  $count = pg_num_rows($resultados);
  // $busrecepcion= isset($_SESSION['busrecepcion']  ) ? $_SESSION['busrecepcion']  : "" ;
  // $buscarejec= isset($_SESSION['buscarejec']  ) ? $_SESSION['buscarejec']  : "" ;
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
<input type="hidden" id="tiporeporte"  name="tiporeporte"  value="<?php echo $tiporeporte; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Diario: Cantidad de Gestiones por Ejecutivo </a>
          <br>
       <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $fechainicial;?> / <?php echo $fechafinal;?>  </b> 
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="reportetrabajo"> 
           <thead>
              <tr>               
                <th>Nº</th>
                <th>Usuario</th>
                <th>Nombre y apellido</th>
                <th>Cantidad</th>
              </tr>
            </thead>
        </table>
      </div>
      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportediario'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var fechainicial= document.getElementById('fechainicial').value;
  var fechafinal= document.getElementById('fechafinal').value;

  $('table#reportetrabajo').DataTable({
    "ajax": {
    "url": "api/SelectreportexGestiones.php",  
          "type": "POST",
          "data": {"fechainicial":fechainicial,"fechafinal":fechafinal}
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "nomejecutivo", className: "text-center" },
      {"data": "nombresapellido", className: "text-center" },
      {"data": "cantidad", className: "text-center" },
      
    ],
    "order" : [[0, "asc"]],

    "scrollX": false,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend:'excelHtml5',
            text: 'Excel',
            title: 'Gestiones por ejecutivos',     
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

}
  </script>

