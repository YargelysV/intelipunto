<?php
  $tituloPagina=$_SESSION["page"];
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
    $_SESSION["comercializacionproceso"]="1";
   
  }
?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i style="color:#A01010;" class="fa fa-group"></i> Reporte diario</h1>
      </div>
      
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              GENERAR REPORTE DIARIO 
            </div>
            
           <table border="0" align="center">
             <br>
             <div>
              <div class="col-lg-5">Banco:</div>
              <div class="col-lg-5">
                <select class="form-control" name="clientes" id="clientes" required>
                <option value="">Seleccione un Banco</option>
                </select>
              </div>
              <br>
            </div>
             <br>  
              <div class="form-group">
                  <div class="col-lg-5">Ejecutivo:</p></div>
                  <div class="col-lg-5">
                 <select class="form-control" name="ejecutivo" id="ejecutivo" required>
                      <option value="">Seleccione Ejecutivo</option>
                  </select>
                </div>
                </div>

              

                
                  <tr align="center">
                    <td> </td>

                    <td class="center">DESDE </td>
                    <td class="center">HASTA</td>
                  </tr>
                  <tr align="center">
                     <td><div class="col-lg-5">Fecha:</div>  </td> 
                     
                    <td align="center"><input type="date" name="fechainicial" id="fechainicial" class="form-control "  style="color: #000000" required="required" max="<?php  echo date("Y-m-d");?>" onchange="validarfechasreporte()" value="">
                    </td>
                    <td align="center"><input type="date" name="fechafinal" id="fechafinal" class="form-control inputcentrado"  style="color: #000000" required="required" max="<?php  echo date("Y-m-d");?>" onchange="validarfechasreporte()" value="" >
                    </tr>

                  </table>
          <br>
         
            <script type="text/javascript">


              function opcionesreporte(){
                var clientes=document.getElementById('clientes').value;
                var ejecutivo=document.getElementById('ejecutivo').value;
                if (clientes=='' && ejecutivo=='') {
                  $("#option_ejecutivo").removeAttr("style");
                }
                else{

                  $("#option_ejecutivo").css("display", "none");
                }
              }
                   window.setInterval("opcionesreporte()",1);
            </script>

                <div class="form-group">
                  <div class="col-lg-5"><p class="nombrefiltro">Tipo de reporte:</p></div>
                  <div class="col-lg-5">
                 <select class='form-control filtro'name="tiporeporte" id="tiporeporte" required disabled >
                    
                      <option value="1">Ventas</option>
                      <option value="2">Administraci&oacute;n</option>
                      <option value="4">Instalado</option>
                      <option value="3" style="display:none;" id="option_ejecutivo">Ejecutivo</option>
                     
                  </select>
                </div>
                </div>
          <br>  
           <br>  
            <!-- <input type="hidden" id="valor" value="13" name="valor"/> -->
            <input type="hidden" id="valor" value="13" name="valor"/>
           <input type="hidden" id="tipousuario" value='<?php echo $_SESSION["codtipousuario"];?>' name="tipousuario"/>
           <input type="hidden" id="ejecutivobco" value='<?php echo $_SESSION["ejebanco"];?>' name="ejecutivobco"/>
            <input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"];?>' name="usuario"/>
             <input type="hidden" id="coordinador" value='<?php echo $_SESSION["coordbanco"];?>' name="coordinador"/>
       
             <?php if (($_SESSION["codtipousuario"]=='E') || ($_SESSION["codtipousuario"]=='O') || ($_SESSION["codtipousuario"]=='B')|| ($_SESSION["coordbanco"]!=0) || ($_SESSION["ejebanco"]!=0)) { ?>
        
          <script languaje="javascript">
            generarejecutivoreporte();
            </script>

          <?php } else {  ?>           
            <script languaje="javascript">
            generarclientereportes();
            </script>
          <?php } ?>

            <div class="form-group">
                    <div style="text-align: center;">
                      <input class="btn btn-lg btn-success" type="button" id="Buscar" name="Buscar" value="Buscar" onClick="buscarreportediario();"/>
                    </div>
                  </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




