<?php 
  $tipomodulo= $_SESSION["session_user"];
   $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $idcliente= isset($array[0]) ? $array[0] : "";
  $razonsocial= isset($array[1]) ? $array[1] : "";
  $coddocumento= isset($array[2]) ? $array[2] : "";
  $arrays= isset($array[3]) ? $array[3] : "";
  $vendedor= isset($array[4]) ? $array[4] : "";
  $volvervar = isset($array[5]) ? $array[5] : "";
  $volverv = explode("-",$volvervar);
  $banco= isset($volverv[0]) ? $volverv[0] : "";
  $usuarioeje= isset($volverv[1]) ? $volverv[1] : "";
  $tipopos = isset($volverv[2]) ? $volverv[2] : "";
  $fechainicial= isset($volverv[3]) ? $volverv[3] : "";
  $fechafinal=isset($volverv[4]) ? $volverv[4] : "";
  $tiporeporte=isset($volverv[5]) ? $volverv[5] : "";
  $usuario=$_SESSION["session_user"];
  $controlador =new ControladorReporte();
  $datoscomisiones=$controlador->spmostrarcomisionesestatus($idcliente);
  $row = pg_fetch_assoc($datoscomisiones);
?>
<script languaje="javascript">
     mascaras();
  </script>
<input type="text" id="idcliente" class="hidden" value="<?php echo $idcliente; ?>">
<input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">
<input type="text" id="banco" class="hidden" value="<?php echo $banco; ?>">
<input type="text" id="banco" class="text" value="<?php echo $row['spmostrarcomisionesestatus']; ?>">
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="usuarioeje"  name="usuarioeje"  value="<?php echo $usuarioeje; ?>" />
<input type="hidden" id="tipopos"  name="tipopos"  value="<?php echo $tipopos; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
<input type="hidden" id="tiporeporte"  name="tiporeporte"  value="<?php echo $tiporeporte; ?>" />
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-phone-square" style="color:#F8F9F9;"> </i> Registrar Comisiones - Cliente: <?php echo $razonsocial." - RIF: ".$coddocumento;?>  </a>
        </div>
       </nav>
      </div>
      <!--nuevo bloque-->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading" style="text-align: center;">
            <i class="fa  fa-edit fa-fw"></i> Registrar Comisiones
            </div>
            <div class="panel-body">
            <div class="row">
              <div class="col-lg-3">
                <div class="form-group">
                    <label for="" class="col-form-label">Confirmacion.</label>
                      <select type="select" id="estatus_comision" class="form-control">
                      </select>
                    <input type="hidden" name="confcomisiones" id="confcomisiones" class="form-control" readonly>
                    </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                    <label for="" class="col-form-label">Fecha de Comision</label>
                    <input type="date" class="form-control" id="fechacomisiones">
                </div>
              </div>
              <input class="form-control" type="hidden" value="<?php echo $vendedor ?>" name="vendedor" id="vendedor" autocomplete='off' required>
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Observaciones</label>
                  <input class="form-control" type="text" name="observaciones" id="observaciones" autocomplete='off' required>
                </div>
              </div>
              </div>
              <div class="col-lg-3">
                <?php 
                
                if ($row['spmostrarcomisionesestatus']==1 || $row['spmostrarcomisionesestatus']===1){?>
                  <br>
                  <input class="btn btn-primary btn-block" align="center" type="button" name="editar" id="" value="Guardar" onclick="guardarcomisiones();" disabled/>
                <?php }else if($row['spmostrarcomisionesestatus']!=1){?>
                <br>
                  <input class="btn btn-primary btn-block" align="center" type="button" name="editar" id="" value="Guardar" onclick="guardarcomisiones();" />
                <?php } ?>
              </div>
            </div>
          </div>
        <div class="panel-group">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h4 class="panel-title" align="center">
                Comisiones de <?php echo $vendedor; ?>                     
              </h4>
            </div>
            <div class="panel-body">
              <div class="row" align="center">
                <div class="col-12">
                  <table class="table table-bordered"  id="registrocomisiones"> 
                     <thead>
                        <tr>        
                          <th>ID</th>     
                          <th>&nbsp;&nbsp;Fecha&nbsp;&nbsp;</th>
                          <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                          <th>Observaciones</th>            
                        </tr>
                     </thead>
                  </table>
                </div> 
              </div>
            </div>
            <!--div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportediario?cargar=reportexdiasdetalles&var=<?php echo $arrays ?>'">Volver</button>
        </div-->
          </div>
        </div>
        <!--div class="col-md-4 col-md-offset-4" align="center" >
          <?php if ($busqueda==1 || $busqueda==2 || $busqueda==3 || $busqueda==4) { ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa?cargar=DatosEncuesta&var=<?php echo $enlace; ?>'">Volver</button>
          <?php } else if ($codbanco!=0){ ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta?cargar=buscarregistros&var=<?php echo $codbanco; ?>'">Volver</button>
          <?php } else { ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta?cargar=buscarregistros&var=<?php echo 0; ?>'">Volver</button>
          <?php } ?>  
        </div-->
      </div>
      </div>  
    </div>
  </div>
</div> 
</div>     
  <script languaje="javascript">
    seleccionarestatuscomision(); 
  </script>
  <script type="text/javascript">
  function dtCore(){ 
  var idcliente= document.getElementById('idcliente').value;
  //alert(idcliente);
/*  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;*/

  $('table#registrocomisiones').DataTable({
    "ajax": {
    "url": "api/Selectmostrarcomisiones.php",  
          "type": "POST",
          "data": {"idcliente":idcliente}
    },
    "columns": [

      {"data": "idregistro", className: "text-center" }, 
      {"data": "fechacomision", className: "text-center" }, 
      {"data": "estatuscomision", className: "text-center",
        "render" : function (data, type, full, meta, row) {
          if (data == "0" || data === "0") {
            return "POR DEFINIR";
          }else if (data == "1" || data === "1"){
            return "SIN PAGO";
          }else if (data == "2" || data === "2"){
            return "PAGO";
          }else if (data == "3" || data === "3"){
            return "PAGO PARCIAL";
          }else{
            return "";
          }
        }
      },
      {"data": "observaciones", className: "text-center"}

    ],
    "order" : [[0, "asc"]],

   "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend:'excelHtml5',
            text: 'Excel',          
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }     
  });


}
  </script>