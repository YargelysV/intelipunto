<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] : "";
  $usuarioeje= isset($array[1]) ? $array[1] : "";
  $tipopos = isset($array[2]) ? $array[2] : "";
  $fechainicial= isset($array[3]) ? $array[3] : "";
  $fechafinal=isset($array[4]) ? $array[4] : "";
  $tiporeporte=isset($array[5]) ? $array[5] : "";
  //echo $tiporeporte;
   $controladorc =new ControladorReporte();
   $resultados= $controladorc->sp_reportexdiasdetalles($banco,$usuarioeje,$tipopos,$fechainicial,$fechafinal,$tiporeporte,$usuario);
   $databanco = pg_fetch_assoc($resultados);
  $volver=$_SESSION['Volver'];
  $count = 1;//pg_num_rows($resultados);
  // $busrecepcion= isset($_SESSION['busrecepcion']  ) ? $_SESSION['busrecepcion']  : "" ;
  // $buscarejec= isset($_SESSION['buscarejec']  ) ? $_SESSION['buscarejec']  : "" ;
 
?>



<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="usuarioeje"  name="usuarioeje"  value="<?php echo $usuarioeje; ?>" />
<input type="hidden" id="tipopos"  name="tipopos"  value="<?php echo $tipopos; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
<input type="hidden" id="idcliente"  name="idcliente"  value="<?php echo $databanco['id']; ?>" />
<input type="hidden" id="tiporeporte"  name="tiporeporte"  value="<?php echo $tiporeporte; ?>" />
<input type="hidden" id="array"  name="array"  value="<?php echo $var; ?>" />
 <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">  

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Diario: </a>
          <br>
     <!--    <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $databanco['ibp'];?></b> -->
        </div>
       </nav>
    </div>
    <div class="row">
<div class="col-12">
        <table class="table table-bordered"  id="reporteejecutivoxdiasdetalle"> 
           <thead>
              <tr>               

                <th>Acción</th>
                <th>N°</th>
                <th>ID</th>
                <th>&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Rif&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Número&nbsp;&nbsp;de&nbsp;&nbsp;Afiliación&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ejecutivo Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ejecutivo Gestión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>              
                <th>&nbsp;&nbsp;&nbsp;Fecha&nbsp;Última&nbsp;Gestión&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Tipo&nbsp;de&nbsp;POS&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Modelo&nbsp;de&nbsp;POS&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cartera&nbsp;&nbsp;de&nbsp;&nbsp;POS&nbsp;&nbsp;&nbsp;&nbsp;</th>                
                <th>&nbsp;&nbsp;&nbsp;Cantidad&nbsp;&nbsp;Ventas&nbsp;&nbsp;con Fondos&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Fecha&nbsp;Cierre&nbsp;de Venta&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cantidad&nbsp;Facturados&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Fecha&nbsp;Conf.&nbsp;de Pagos&nbsp;&nbsp;&nbsp;</th>
                <th>Nº Factura</th>
                <th>Monto USD Transferencia</th>
                <th>Monto USD Efectivo</th>
                <th>Monto Bs.S</th>
                <th>Vendedor Final</th>
                <th>Estatus Comisiones</th>
                <th>Fecha Comisiones</th>
                <th>Observaciones Comisiones</th>
<!--                 <th>Serial Mifi</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th> -->
       </tr>
            </thead>
        </table>
 
      </div> 

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportediario?cargar=reportexdias&var=<?php echo $volver; ?>'">Volver</button>
        </div>

    </div>
  </div>
</div>


  <div class="container-fluid">
  <div class="modal fade" id="Modal_comisiones" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <center> Asignación de Comisiones</center>
            </div>
   <input type="hidden" name="id" value="" id="id" class="form-control" placeholder="" tabindex="3" readonly>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Confirmacion.</label>
                      <select type="select" id="estatus_comision" class="form-control">
                      </select>
                    <!-- <select class="form-control" id="confcomisiones">
                                       <option value="">Seleccionar</option>
                                       <option value="1">PAGADO</option>
                                       <option value="2">SIN PAGO</option>
                    </select>  -->
                    <input type="hidden" name="confcomisiones" id="confcomisiones" class="form-control" readonly>
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Fecha de Comision</label>
                    <input type="date" class="form-control" id="fechacomisiones">
                    </div> 
                    </div> 
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Observaciones</label>
                    <input type="text" class="form-control" id="observacionescomisiones">
                    </div> 
                    </div>    
                </div>
           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <input type="button" class="btn btn-dark" id="btneditar" name="editar" value="Guardar" onclick="editarRegistroComision();">  
            </div>
       
        </div>
    </div>
  </div>
</div>

  <script type="text/javascript">
  function dtCore(){

  var banco= document.getElementById('banco').value;
  var usuarioeje= document.getElementById('usuarioeje').value;
  var tipopos= document.getElementById('tipopos').value;
  var fechainicial= document.getElementById('fechainicial').value;
  var fechafinal= document.getElementById('fechafinal').value;
  var tiporeporte= document.getElementById('tiporeporte').value;
  var usuario= document.getElementById('usuario').value;
  
  $('table#reporteejecutivoxdiasdetalle').DataTable({
    "ajax": {
    "url": "api/SelectReportexDiasDetallado.php",  
          "type": "POST",
          "data": {"banco":banco,"usuarioeje":usuarioeje,"tipopos":tipopos,"fechainicial":fechainicial,"fechafinal":fechafinal,"tiporeporte":tiporeporte, "usuario":usuario}
    },
    "columns": [
      
      {"defaultContent": "<button class='Editar btn btn-primary' type='button' id='editar' title='Editar'><i class='fa fa-edit'></i></button>", className: "text-center" },
      {"data": "correlativo", className: "text-center"},
      {"data": "id", className: "text-center" },
      {"data": "nombrebanco", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "afiliado", className: "text-center" },
      {"data": "ejecutivogestion",  className: "text-center"},
      {"data": "gestionador", className: "text-center" },
      {"data": "fechaedicion", className: "text-center" },
      {"data": "tipo", className: "text-center" },
      {"data": "modelo", className: "text-center" },
      {"data": "cantidad", className: "text-center" },
      {"data": "ventas_con_fondos", className: "text-center" },
      {"data": "fecha_venta", className: "text-center" },
      {"data": "facturado", className: "text-center" },
      {"data": "fechafactura", className: "text-center" },
      {"data": "numerofactura", className: "text-center" },
      {"data": "montousdtransf", className: "text-center" },
      {"data": "montousdefect", className: "text-center" },
      {"data": "montobs", className: "text-center" },
      {"data": "vendedorfinal", className: "text-center" },
      {"data": "fechacomisiones", className: "text-center" },
      {"data": "confcomisiones", className: "text-center",
        "render" : function (data, type, full, meta, row) {
          if (data == "0" || data === "0") {
            return "POR DEFINIR";
          }else if (data == "1" || data === "1"){
            return "SIN PAGO";
          }else if (data == "2" || data === "2"){
            return "PAGO";
          }else{
            return "";
          }
        }
      },
      {"data": "observacioncom", className: "text-center" }
    ],
    "order" : [[0, "asc"]],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend:'excelHtml5',
            text: 'Excel',          
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

 /* $('table#reporteejecutivoxdiasdetalle tbody').on( 'click', 'button.Editar', function () {

        var table=$('table#reporteejecutivoxdiasdetalle').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        $("input#id").val(D.id)
        $("input#fechacomisiones").val(D.fechacomisiones)
        $("input#confcomisiones").val(D.confcomisiones)
        seleccionarestatuscomision(); 
        $(".modal-header").css( "background-color", "#17a2b8");
        $(".modal-header").css( "color", "white" );

        $("input#id").val(D.id) 
        var cod_estatus=D.idcomision;
        var confcomisioness=D.confcomisioness;

        $("input#estatus_comision").val("");

        $('#Modal_comisiones').modal('show');


  });*/
  $('table#reporteejecutivoxdiasdetalle tbody').on( 'click', 'button.Editar', function () {
    //alert('algo');
          var table=$('table#reporteejecutivoxdiasdetalle').DataTable();
          var D =  table.row($(this).parents("tr")).data();
          var id=D.id;
          var razonsocial=D.razonsocial;
          var coddocumento=D.coddocumento;
          var vendedorfinal=D.vendedorfinal;
          var variable= banco+'-'+usuarioeje+'-'+tipopos+'-'+fechainicial+'-'+fechafinal+'-'+tiporeporte;
          var url = "reportediario?cargar=reportexdiascomisisones&var="+id+'/'+razonsocial+'/'+coddocumento+'/'+variable+'/'+vendedorfinal; 
          $(location).attr('href',url);

    });
}
  </script>



