<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] : "";
  $ejecutivo= isset($array[1]) ? $array[1] : "";
  $fechainicial = isset($array[2]) ? $array[2] : "";
  $fechafinal= isset($array[3]) ? $array[3] : "";
  $tiporeporte=isset($array[4]) ? $array[4] : "";
  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_reportexdias($cliente,$ejecutivo,$fechainicial,$fechafinal,$tiporeporte);
  $databanco = pg_fetch_assoc($resultados);
  $_SESSION['Volver']=$cliente.'/'.$ejecutivo.'/'.$fechainicial.'/'.$fechafinal.'/'.$tiporeporte;
  $count = pg_num_rows($resultados);
  // $busrecepcion= isset($_SESSION['busrecepcion']  ) ? $_SESSION['busrecepcion']  : "" ;
  // $buscarejec= isset($_SESSION['buscarejec']  ) ? $_SESSION['buscarejec']  : "" ;
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="fechainicial"  name="fechainicial"  value="<?php echo $fechainicial; ?>" />
<input type="hidden" id="fechafinal"  name="fechafinal"  value="<?php echo $fechafinal; ?>" />
<input type="hidden" id="tiporeporte"  name="tiporeporte"  value="<?php echo $tiporeporte; ?>" />
<input type="hidden" id="nombrebanco"  name="nombrebanco"  value="<?php echo $databanco['nombrebanco']; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Diario: </a>
          </nav>
     <b style="color:#5DADE2;">   <b style="color:#5DADE2;">Búsqueda:<?php if ($cliente=='0' || $cliente=='') {
       echo 'Todos los bancos';
     } else { echo $databanco['nombrebanco']; } ?></b> </b> 
        </div>
       
    </div>
    <div class="row">
    
    <div class="col-12">
        <table class="table table-bordered"  id="reporteejecutivoxdias"> 
           <thead>
              <tr>               

                <th>N°</th>
                <th>enlace</th>
                <th>&nbsp;&nbsp;&nbsp;Accion&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ejecutivo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cartera&nbsp;&nbsp;de&nbsp;&nbsp;POS&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Tipo&nbsp;de&nbsp;POS&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cantidad&nbsp;&nbsp;Ventas&nbsp;&nbsp;con Fondos&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cantidad&nbsp;&nbsp;POS&nbsp;&nbsp;Facturados&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cantidad&nbsp;&nbsp;POS&nbsp;&nbsp;Instalados&nbsp;&nbsp;&nbsp;&nbsp;</th>
              </tr>
            </thead>
            <tfoot align="right">
              <th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>
            </tfoot>
        </table>
 
      </div> 

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportediario'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var clientes= document.getElementById('clientes').value;
  var ejecutivo= document.getElementById('ejecutivo').value;
  var fechainicial= document.getElementById('fechainicial').value;
  var fechafinal= document.getElementById('fechafinal').value;
  var tiporeporte= document.getElementById('tiporeporte').value;

  if (tiporeporte==1) {

    var nombre_reporte="Reporte de Ventas ";

  }  else if (tiporeporte==2) {

    var nombre_reporte="Reporte de Administración ";
  } 
   else if (tiporeporte==4) {

    var nombre_reporte="Reporte de Instalación ";
   }

   if (clientes==0) {

    var nombre_banco="Todos los bancos "
   }

   else {

    var nombre_banco=nombrebanco;
   }

  $('table#reporteejecutivoxdias').DataTable({
    "ajax": {
    "url": "api/SelectReportexdias.php",  
          "type": "POST",
          "data": {"clientes":clientes,"ejecutivo":ejecutivo,"fechainicial":fechainicial,"fechafinal":fechafinal,"tiporeporte":tiporeporte}
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace", className: "text-center" },
      {"defaultContent": "<p class='abrir'><a class='abrir' type='href' title='Ver'>Detalle</a></p>", className: "text-center" },
      {"data": "nombrebanco", className: "text-center" },
      {"data": "vendedor", className: "text-center" },
      {"data": "cantidad", className: "text-center" },
      {"data": "tipopos",  className: "text-center"},
      {"data": "ventas_con_fondos", className: "text-center" },
      {"data": "facturado", className: "text-center" },
      {"data": "pos_instalados", className: "text-center" }
      
    ],
    "order" : [[0, "asc"]],
    "columnDefs": [
                 {
                       "targets": [1],
                       "visible": false,
                   }
               ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel', 
            title: nombre_reporte + nombre_banco + fechainicial +'/'+ fechafinal,        
         
        exportOptions : {
        columns: [ 0, 3, 4, 5, 6, 7, 8, 9]
      }
    }],
    "footerCallback": function ( tfoot, data, start, end, display) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html(
                'Total:'+lastRow 
            );

            // Total over all pages
            totalcartera = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 5 );
 
            // Total over this page
            totalventasfondo = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 7 );

            totalfacturados = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 8 );

            totalintalados = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 9 );
 
            // Update footer
            $( api.column( 2 ).footer() ).html('');
            $( api.column( 3 ).footer() ).html('');
            $( api.column( 4 ).footer() ).html('');
            $( api.column( 5 ).footer() ).html('Total: '+totalcartera);
            $( api.column( 6 ).footer() ).html('');
            $( api.column( 7 ).footer() ).html('Total: '+totalventasfondo);
            $( api.column( 8 ).footer() ).html('Total: '+totalfacturados);
            $( api.column( 9 ).footer() ).html('Total: '+totalintalados);
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

$('table#reporteejecutivoxdias tbody').on( 'click', 'a.abrir', function () {
    //alert('algo');
          var table=$('table#reporteejecutivoxdias').DataTable();
          var D =  table.row($(this).parents("tr")).data();
          var enlace=D.enlace;
          var url = "reportediario?cargar=reportexdiasdetalles&var="+enlace+'/'+fechainicial+'/'+fechafinal+'/'+tiporeporte; 
          $(location).attr('href',url);

    });


}
  </script>