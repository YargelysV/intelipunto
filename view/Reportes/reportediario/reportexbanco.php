<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] : "";
  // $estatus= isset($array[1]) ? $array[1] : "";
  // $documento= isset($array[2]) ? $array[2] : "";
  // $fechainicial = isset($array[3]) ? $array[3] : "";
  // $fechafinal= isset($array[4]) ? $array[4] : "";

  $controladorc =new ControladorReporte();
  $resultados= $controladorc->sp_reportediarioxbanco($cliente);
  $databanco = pg_fetch_assoc($resultados);
  $count = pg_num_rows($resultados);
  // $busrecepcion= isset($_SESSION['busrecepcion']  ) ? $_SESSION['busrecepcion']  : "" ;
  // $buscarejec= isset($_SESSION['buscarejec']  ) ? $_SESSION['buscarejec']  : "" ;
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>

<input type="hidden" id="clientes"  name="clientes"  value="<?php echo $cliente; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Reporte Diario por Banco :</a>
               <!--    <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $databanco['ibp'];?></b> -->
               </nav>
          <b style="color:#5DADE2;">   <b style="color:#5DADE2;">Búsqueda:<?php if ($cliente=='0' || $cliente=='') {
       echo 'Todos los bancos';
     } else { echo $databanco['ibp']; } ?></b> </b>      
        </div>
       
    </div>
<div class="col-lg-12" align="center" >
    <div class="row">
        <table class="table table-bordered"  id="reportexejecutivos"> 
           <thead>
              <tr>               

                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N°&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cartera&nbsp;de&nbsp;POS&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Modelo&nbsp;de&nbsp;POS&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Cantidad&nbsp;Ventas&nbsp;con&nbsp;Fondos&nbsp;&nbsp;&nbsp;</th>                
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cantidad&nbsp;Facturados&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
             </tr>
            </thead>
            <tfoot align="right">
              <tr><th></th><th></th><th></th><th></th><th></th><th>
            </tfoot>
        </table>

        <br>
  </div>
</div> 

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportediario'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var clientes= document.getElementById('clientes').value;
  var count= 0;

  $('table#reportexejecutivos').DataTable({
    "ajax": {
    "url": "api/selectReporteDiarioBanco.php",  
          "type": "POST",
          "data": {"clientes":clientes}
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "ibp", className: "text-center" },
      {"data": "carteratotal", className: "text-center" },
      {"data": "tipopos", className: "text-center" },
      {"data": "estatusventa",  className: "text-center"},
      {"data": "cantfacturados", className: "text-center" }
      
    ],
    "order" : [[0, "asc"]],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5',
            title: 'Reporte por Banco',
            text: 'Excel',         
        }
    ],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
  
            // Total over all pages
            totalcartera = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 2 );
 
            // Total over this page
            totalventasfondo = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 4 );

            totalfacturados = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 5 );

            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html('Registros: '+lastRow);
            $( api.column( 1 ).footer() ).html('Total');
            $( api.column( 2 ).footer() ).html(totalcartera);
            $( api.column( 4 ).footer() ).html(totalventasfondo);
            $( api.column( 5 ).footer() ).html(totalfacturados);
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

}
  </script>