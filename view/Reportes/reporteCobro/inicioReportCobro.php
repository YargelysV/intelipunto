<?php
  $tituloPagina=$_SESSION["page"];
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
    $_SESSION["comercializacionproceso"]="1";
   
  }
?>
<div id="app">

   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"><i style="color:#A01010;" class="fa fa-group"></i> Reporte Instalados </h1>
        </div>
        
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-info">
              <div class="panel-heading">
                GENERAR REPORTE INSTALALADO
              </div>
              
             <table border="0" align="center">
               <br>
               <div>
                <div class="col-lg-5">Banco:</div>
                <div class="col-lg-5">
                  <select  v-model="parametros.valueBanco" class="form-control" name="cliente" id="cliente">
                    <option value="0" selected>Seleccione banco</option>
                    
                    <template v-for="Banco in Bancos">
                      <option :value="Banco.codigobanco">{{ Banco.ibp }}</option>
                    </template>
                    <option value="999">Todos los bancos</option>
                  </select>
                </div>
                <br>
              </div>
               <!-- <br>  
                <div class="form-group">
                    <div class="col-lg-5">Estatus:</p></div>
                      <div class="col-lg-5">
                       <select  v-model="parametros.valueEstatus" class="form-control" name="estatus" id="estatus">
                            <option value="0" >Seleccione estatus</option>
                            <template v-for="tipo in tipoBusqueda">
                              <option  v-bind:value="tipo.id">{{ tipo.estatus }}</option>
                            </template>
                      </select>
                    </div>
                  </div> -->

                    </table>
            <br>
            
              <!-- <input type="hidden" id="valor" value="13" name="valor"/> -->
              <input type="hidden" id="valor" value="13" name="valor"/>
              <input type="hidden" id="tipousuario" value=' <?php echo $_SESSION["codtipousuario"];?>' name="tipousuario"/>
              <input type="hidden" id="ejecutivobco" value='<?php echo $_SESSION["ejebanco"];?>' name="ejecutivobco"/>
              <input type="hidden" id="usuario" value='     <?php echo $_SESSION["session_user"];?>' name="usuario"/>
              <input type="hidden" id="coordinador" value='<?php  echo $_SESSION["coordbanco"];?>' name="coordinador"/>
         
               <!-- <?php if (($_SESSION["codtipousuario"]=='E') || ($_SESSION["codtipousuario"]=='O') || ($_SESSION["codtipousuario"]=='B')|| ($_SESSION["coordbanco"]!=0) || ($_SESSION["ejebanco"]!=0)) { ?>
          
            <script languaje="javascript">
              generarejecutivoreporte();
              </script>

            <?php } else {  ?>           
              <script languaje="javascript">
              generarclientereportes();
              </script>
            <?php } ?> -->

              <div class="form-group">
                      <div style="text-align: center;">
                        <input class="btn btn-lg btn-success" type="button" id="Buscar" name="Buscar" value="Buscar"  :disabled='buttonDisable()' onclick='peticionReporte()' />
                      </div>
                    </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
</div>


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script type="text/javascript">
  var url='https://api.coindesk.com/v1/bpi/currentprice.json';
  var vm = new Vue ({
    el:"#app",
    created: function (){
      this.selectBanco();
    },
    data: {
      Bancos:null,
      tipoBusqueda: [{
          id:2,
          estatus:'En Garantia'},

          {id:1,
          estatus:'Fuera de garantia'}],
      parametros:{
        valueBanco: 0,
        valueEstatus: 0
      },
      // url:  currentRoute: window.location.pathname
    },
      methods: {
        selectBanco: function(){
          axios.get('view/reportes/reporteCobro/bancos.php').then((response) => {
               this.Bancos=response.data;
             });
        },
        buttonDisable: function() {
          if ((this.parametros.valueBanco!=0)) {
            return false;
          } else {
            return true;
          }
        }
     }
  })
</script>