<?php
//error_reporting(0);
//session_start();

if (isset($_SESSION["session_user"])) {
  $controlador = new controladorUsuario();
  $o = $controlador->verificaronline($_SESSION["session_user"]);

  $usuario = $_SESSION["session_user"];
  $pass = $_SESSION["session_var"];

  $csrf_token = bin2hex(random_bytes(32));
  $_SESSION['csrf_token'] = $csrf_token;

  $controlador = new controladorUsuario();
  $row = $controlador->ver($usuario);
  //$_SESSION["contrasena"]="0";

?>
  <!-- Page Content -->
  <div id="page-wrapper" class="background-home">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"></h1>
        </div>
        <!-- gallery -->

        <?php if ($_SESSION["codtipousuario"] == "U") { ?>
          <div id="myCarousel" class="carousel slide">
            <input type="hidden" name="csrf_token" value="<?php echo $csrf_token; ?>">
            <ol class="carousel-indicators">

            </ol>

            <div class="carousel-inner">
          
            </div>

          </div>

        <?php } else { ?>


          <div id="myCarousel" class="carousel slide">

            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>


            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
              <div class="active item"><img src="img/apos.jpg" alt="banner3" /></div>
              <div class="item"><img src="img/move2500.jpg" alt="banner1" /></div>
              <div class="item"><img src="img/link2500.png" alt="banner2" /></div>

            </div>

          </div>

        <?php  } ?>

        


      </div>
      
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php
} else {
  header("Location:../salir");
}


?>