<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= $array[0];
  $fechadetalle=$array[1];
  
  $controladorc =new controladorAcuerdoServicio();
  $comerciali=$controladorc->spbanco($banco);
  $databanco = pg_fetch_assoc($comerciali);
?>
<input type="hidden" id="fechadetalle" name="fechadetalle" value="<?php echo $fechadetalle; ?>">
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>">
<input type="hidden" id="databanco" name="databanco" value="<?php echo  $databanco['ibp']; ?>">

 <div id="page-wrapper" ng-app="AppAcuerdosServiciosBF">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-suitcase 3x"> </i>    BÚSQUEDA: Acuerdos de Servicios Por cliente y Fecha de Recepción </a>
         <br>
         <b style="color:blue;"><?php echo $databanco['ibp']." <br>Fecha Recepción: ".$fechadetalle;?></b> 
          <br>
        </div>
        <br>
       </nav>
    </div>
    <br>
    <div class="row">
      <div class="demo-container"  ng-controller="AcuerdosServiciosBFController">
          <div id="gridContainer" dx-data-grid="gridOptions"></div>
           <br><br> 
      </div>
      <div class="row" align="left">
          <button type="button" href="#graficas" class="btn btn-info"><a href="#graficas" style="color:white;">Ver Grafico</a></button>
      </div>
      
    </div>
   <div class="scroll" id="graficas">
    <div  class="demo-container"   ng-controller="AcuerdosServiciosBFgraficoController">
        <div id="chart-demo">
            <div id="chart" dx-chart="chartOptions"></div>           
        </div>
    </div>
    </div>

    <div class="row" align="center">
          <div class="col-md-4 col-md-offset-4">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='acuerdosservicios'"/>
          </div>
    </div>
  </div>
</div>


