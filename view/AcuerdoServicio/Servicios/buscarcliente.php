<?php
	$usuario=$_SESSION["session_user"];
	$var=($_REQUEST['var']);
  $array = explode("/",$var);
  $rif= $array[0];
  $banco= $array[1];
  $fechadetalle=isset($array[2]) ? $array[2] : "";
  $nombrearchivo= isset($array[3]) ? $array[3] : "";
  $irif = isset($array[4]) ? $array[4] : "";
  $afiliado = isset($array[5]) ? $array[5] : "";
  //$afiliado= $array[4];

  $controladorc =new controladorAcuerdoServicio();
  $comerciali=$controladorc->spbanco($banco);
  $databanco = pg_fetch_assoc($comerciali);

  $var1=$banco.'/'. $fechadetalle;
?>
<input type="hidden" id="rif" name="rif" value="<?php echo $rif; ?>">
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>">
<input type="hidden" id="fechadetalle" name="fechadetalle" value="<?php echo $fechadetalle; ?>">
<input type="hidden" id="nombrearchivo" name="nombrearchivo" value="<?php echo $nombrearchivo; ?>">
<input type="hidden" id="afiliado" name="afiliado" value="<?php echo $afiliado; ?>">
<input type="hidden" id="databanco" name="databanco" value="<?php echo  $databanco['ibp']; ?>">


 <div id="page-wrapper" ng-app="AppAcuerdosServiciosRIF">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#35474F;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#00FFFF;">Acuerdos de Servicios Por Rif/Ci ó Afiliado </a>
         <br>
         <b style="color:blue;"><?php echo $databanco['ibp']." <br>Fecha Recepción: ".$fechadetalle."<br> Rif/Ci:".$rif."<br> Nº de Afiliado:".$afiliado;?></b> 
          <br>
        </div>
        <br>
       </nav>
    </div>
    <br><br><br><br><br>
    <div class="row">
      <div class="demo-container"  ng-controller="AcuerdosServiciosRIFController">
          <div id="gridContainer" dx-data-grid="gridOptions"></div>
           <br><br> 
      </div>
      <div class="row" align="left">
          <button type="button" href="#graficas" class="btn btn-info"><a href="#graficas" style="color:white;">Ver Grafico</a></button>
      </div>
      
    </div>
   <div class="scroll" id="graficas">
    <div  class="demo-container"   ng-controller="AcuerdosServiciosRIFgraficoController">
        <div id="chart-demo">
            <div id="chart" dx-chart="chartOptions"></div>           
        </div>
    </div>
    </div>

    <div class="row" align="center">
          <div class="col-md-4 col-md-offset-4">
           <?php if($irif=='') {?>
           <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarrecepcion&var=<?php echo $var1;?>'"/>
          <?php } else {?>
          <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarrif&var=<?php echo $irif;?>'"/>
         <?php } ?>
          </div>
    </div>
  </div>
</div>


