<?php
/*buscarrif.php view*/
 $tecnicos=$_SESSION["serviciotecnico"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
   $_SESSION['buscarrif']=$var;
  $_SESSION['bsqrecepcion']='';
  $array = explode("/",$var);
  $rif= $array[0];
  
  $controladorclient =new ControladorServicioTecnico();
  $comercia= $controladorclient->sp_buscarclientemifixrif($rif);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>
<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>
<style type="text/css">
  .dataTables_wrapper .dt-buttons {
  /*width: 300px;*/
  float: right;
}
  .Pendiente-por-Instalar{
    
    color:white;
    background:#808080;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

  .Revisión-de-Equipo{
     
    color:#f1f1f1;
    background:#000080;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

  .Mifi-Instalado{
    
    color:#f1f1f1;
    background:#33A2FF;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 

  }

  .Declinación-de-Compra{
    
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 
  }

  .Gestionado-por-Inteligensa{
    
    color:#f1f1f1;
    background:#008080;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 
  }

  .En-proceso-de-configuracion{
    
    color:#f1f1f1;
    background:#2BE787;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 
  }

  
</style>
<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $tecnicos; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="" style="color:#F8F9F9;"> </i> Busqueda por Rif: Resultados de la Gestion </a>
         <br>
         <b style="color:#5DADE2;"> Búsqueda: <?php echo $rif; ?> </b>
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="confimifibancosrif" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>&nbsp;Acción&nbsp;</th>
                <th>&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Servicio&nbsp;&nbsp;</th>
                <th>N°&nbsp;de&nbsp;Factura</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nombre&nbsp;del&nbsp;Archivo</th>
                <th>Tipo de Equipo</th>
                <th>Tipo de Línea</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarrifmifiservicio&var='"/>
        </div>
      </div>

    </div>
  </div>
</div>
<?php  } ?>
<script type="text/javascript">
  function dtCore(){
    
  var nrorif=document.getElementById('nrorif').value;
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV

  $('table#confimifibancosrif').DataTable({
    "ajax": {
    "url": "api/selectServicioTecnicoRifMifi.php",
          "type": "POST",
          "data": {"nrorif":nrorif}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace", className: "text-center"},
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Gestionar</a>", className: "text-center"},
      {"data": "estatuservices", className: "text-center", 
        "render" : function (data, type, full, meta, row) {
          if (data == "1" || data === "1") {
            return "<p class='Pendiente-por-Instalar'>Pendiente por Instalar</p>";
          }else if (data == "2" || data === "2"){
            return "<p class='Revision-de-Equipo'>Revisión de Equipo</p>";
          }else if (data == "3" || data === "3"){
            return "<p class='Mifi-Instalado'>Mifi Instalado</p>";
          }else if (data == "4" || data === "4"){
            return "<p class='Declinación-de-Compra'>Declinación de Compra</p>";
          }else if (data == "5" || data === "5"){
            return "<p class='Gestionado-por-Inteligensa'>Gestionado por Inteligensa</p>";
          }else if (data == "0" || data === "0"){
            return "<p class='En-proceso-de-configuracion'>En proceso de configuración</p>";
          }else{
            return "";
          }
        }
      },
      {"data": "factura", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "documento", className: "text-center" },
      {"data": "ibp", className: "text-center" },
      {"data": "direccion"},
      {"data": "namearchivo"},
      {"data": "tipopos"},
      {"data": "tipolinea", className: "text-center" }
    ],
    "order" : [[0, "asc"]],

          "columnDefs": [
                 {
                       "targets": [ 1 ],
                       "visible": false,
                   }
               ],
          buttons: [
            'print'
            
          ],
          "scrollX": true,
          "scrollY": false,
          "info":     false,
          "scrollCollapse": false,
          
          dom: 'Bfrtip',
          age:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });

  $('table#confimifibancosrif tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#confimifibancosrif').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "serviciotecnicomifi?cargar=procesarconfigmifi&var="+enlace; 
        $(location).attr('href',url);

  });
}
  </script>