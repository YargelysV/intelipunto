<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] :"";
  $fecha= isset($array[1]) ? $array[1] :"";
  
   if ($banco=="")
  {
    $origen='1';
  }
  if ($fecha=="")
  {
    $origen='2';
  }

   $controlador =new ControladorServicioTecnico();
   $resultados=$controlador->sp_buscarxfechamifi($banco,$fecha,$origen);
    $databanco = pg_fetch_assoc($resultados);
 
  ?>
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $databanco['ibp']; ?>" />
<input type="hidden" id="origen"  name="origen"  value="<?php echo $origen; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-gears"> </i> Servicio Técnico /  Gestión de Mifi: Busqueda Masiva<b style="color:#FF5733;">
          </b></a>
          <br>
          <b style="color:blue;">Búsqueda:<?php if( $banco!='') echo $databanco['ibp']; else  echo $fecha;?></b>
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="confimififecha" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>&nbsp;Acción&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Fecha&nbsp;de&nbsp;Recepción</th>
                <th>Fecha&nbsp;de&nbsp;Carga</th>
                <th>Nombre&nbsp;del&nbsp;Archivo</th>
                <th>Cantidad&nbsp;del&nbsp;Registros</th>
                <th>Codigo banco</th>
                <th>Uusuario</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='serviciotecnicomifi.php'"/>
        </div>
      </div>

<script type="text/javascript">
  function dtCore(){
    
  var banco=document.getElementById('banco').value;
  var fechadetalle= document.getElementById('fecha').value;
  var origen= document.getElementById('origen').value;
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#confimififecha').DataTable({
    "ajax": {
    "url": "api/selectServicioTecnicoMifi.php",
          "type": "POST",
          "data": {"banco":banco,"fechadetalle":fechadetalle,"origen":origen}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace", className: "text-center"},
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Ver Detalles</a>", className: "text-center"},
      {"data": "ibp", className: "text-center" },
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "fechacarga", className: "text-center" },
      {"data": "namearchivo", className: "text-center" },
      {"data": "cantidad"},
      {"data": "codigobanco"},
      {"data": "usuario"}
    ],
    "order" : [[0, "asc"]],

          "columnDefs": [
                 {
                       "targets": [ 1, 8, 9 ],
                       "visible": false,
                   }
               ],
          buttons: [
          {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
            exportOptions : {
              columns: [ 0, 3, 5, 4, 6, 7]
            }
          }],
          "scrollX": true,
          "scrollY": false,
          "info":     false,
          "scrollCollapse": false,
          
          dom: 'Bfrtip',
          age:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });

  $('table#confimififecha tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#confimififecha').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "serviciotecnicomifi?cargar=buscarconfigmifi&var="+enlace; 
        $(location).attr('href',url);

  });
}
  </script>





