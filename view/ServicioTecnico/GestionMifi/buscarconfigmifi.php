<?php
  $usuario=$_SESSION["session_user"];
  $tecnico=$_SESSION["serviciotecnico"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= $array[0];
  $fechadetalle=$array[1];
  $namearchivo = isset($array[2]) ? $array[2] : "";
  $masiva = isset($array[3]) ? $array[3] : "";
  $cliente = isset($array[4]) ? $array[4] : "";
  $fecha = isset($array[5]) ? $array[5] : "";

  $retroceder2=$banco.'/'."";
  $retroceder1="".'/'.$fechadetalle;


  $_SESSION["fecha"]=$fechadetalle;
  $_SESSION['bsqrecepcion']=$banco.'/'.$fechadetalle;
  $_SESSION['configpos']=$banco.'/'.$fechadetalle.'/'.$namearchivo.'/'.$masiva;
  $controladorc =new ControladorServicioTecnico();
  $comerciali=$controladorc->sp_verclientedetalladomifi($banco,$fechadetalle,$namearchivo);
  $databanco = pg_fetch_assoc($comerciali);
?>
<input type="hidden" id="fechadetalle" name="fechadetalle" value="<?php echo $fechadetalle; ?>">
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>">
<input type="hidden" id="namearchivo" name="namearchivo" value="<?php echo $namearchivo; ?>">
<input type="hidden" id="databanco" name="databanco" value="<?php echo  $databanco['ibp']; ?>">
<input type="hidden" id="permisos" name="permisos" value="<?php echo  $tecnico; ?>">

<style type="text/css">
  .dataTables_wrapper .dt-buttons {
  /*width: 300px;*/
  float: left;
}
  .Pendiente-por-Instalar{
    
    color:white;
    background:#808080;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

  .Revisión-de-Equipo{
     
    color:#f1f1f1;
    background:#000080;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

  .Mifi-Instalado{
    
    color:#f1f1f1;
    background:#33A2FF;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 

  }

  .Gestionado-por-Inteligensa{
    
    color:#f1f1f1;
    background:#008080;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 
  }

  .En-proceso-de-configuracion{
    
    color:#f1f1f1;
    background:#2BE787;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 
  }

  
</style>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-gears" style="color:#F8F9F9;"> </i> Servicio Técnico - Gestión Mifi / Busqueda Detallada:  </a>
          <br>
          <b style="color:#5DADE2;">Búsqueda: <?php echo $databanco['ibp'];?> </b>
        </div>
      </nav>
      <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="confimifibancos" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>&nbsp;Acción&nbsp;</th>
                <th>&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Servicio&nbsp;&nbsp;</th>
                <th>N°&nbsp;de&nbsp;Factura</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nombre&nbsp;del&nbsp;Archivo</th>
                <th>Tipo de POS</th>
                <th>Tipo de Línea</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
          </tfoot>
          </table>
        </div>
      </div>
 
      <div class="row" align="center">
                <div class="col-md-4 col-md-offset-4">
                   <?php if ($masiva=='1') {?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarserviciomifi&var=<?php echo $retroceder1; ?>'"/>
                   <?php } else if ($masiva=='2') {?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarserviciomifi&var=<?php echo $retroceder2; ?>'"/> 
                   <?php } else { ?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='serviciotecnicomifi.php'"/>
                   <?php } ?>
                </div>
            </div>

     

    </div>
  </div>
</div>


<script type="text/javascript">
  function dtCore(){
    
  var banco=document.getElementById('banco').value;
  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#confimifibancos').DataTable({
    "ajax": {
    "url": "api/selectServicioDetalladoMifi.php",
          "type": "POST",
          "data": {"banco":banco,"fechadetalle":fechadetalle,"namearchivo":namearchivo}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace", className: "text-center"},
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Gestionar</a>", className: "text-center"},
      {"data": "estatuservices", className: "text-center", 
        "render" : function (data, type, full, meta, row) {
          if (data == "1" || data === "1") {
            return "<p class='Pendiente-por-Instalar'>Pendiente por Instalar</p>";
          }else if (data == "2" || data === "2"){
            return "<p class='Revision-de-Equipo'>Revisión de Equipo</p>";
          }else if (data == "3" || data === "3"){
            return "<p class='Mifi-Instalado'>Mifi Instalado</p>";
          }else if (data == "5" || data === "5"){
           return "<p class='Gestionado-por-Inteligensa'>Gestionado por Inteligensa</p>";
          }else if (data == "0" || data === "0"){
            return "<p class='En-proceso-de-configuracion'>En proceso de configuración</p>";
          }else{
            return "";
          }
        }
      },
      {"data": "factura", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "documento", className: "text-center" },
      {"data": "ibp", className: "text-center" },
      {"data": "direccion"},
      {"data": "namearchivo"},
      {"data": "tipopos"},
      {"data": "tipolinea", className: "text-center" }
    ],
    "order" : [[0, "asc"]],

          "columnDefs": [
                 {
                       "targets": [ 1 ],
                       "visible": false,
                   }
               ],
          buttons: [
          {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
            exportOptions : {
              columns: [ 0, 3, 5, 4, 6, 7, 8, 9, 10, 11]
            }
          }],
            
          "scrollX": true,
          "scrollY": false,
          "info":     false,
          "scrollCollapse": false,
          
          dom: 'Bfrtip',
          "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
          age:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });

  $('table#confimifibancos tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#confimifibancos').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "serviciotecnicomifi?cargar=procesarconfigmifi&var="+enlace; 
        $(location).attr('href',url);

  });
}
  </script>

