<?php
/*buscarrif.php view*/
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $rif= $array[0];

  $controladorclient =new controladorServicioTecnico();
  $comercia= $controladorclient->sp_buscarclienteSTRif($rif);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-gears" style="color:#F8F9F9;"> </i> Servicio Técnico/Gestión Técnica-Búsqueda Masiva Por RIF: </a>
          <br>
          <b style="color:#5DADE2;">Búsqueda: <?php echo $rif;?> </b>
        </div>
       </nav>
    </div>
    <div class="row">
    <div class="demo-container" ng-app="AppServicioTecnicoRif" ng-controller="ServicioTecnicoRifController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='dataserviciotecnico.php'"/>
        </div>
      </div>

    </div>
  </div>
</div>
<?php  } ?>
