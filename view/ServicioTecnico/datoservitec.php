<?php

     $var=($_REQUEST['var']);
     $array = explode("/",$var);
     $nroafiliacion= $array[0];
     $namearchivo= $array[1];
     $busqueda= $array[2];
     $tituloPagina=$_SESSION["page"];
     $usuario=$_SESSION["session_user"];
     $controlador =new ControladorServicioTecnico();
     $datosinstapos=$controlador->mostrardetalleafiliado($nroafiliacion,$namearchivo);
     $total = pg_num_rows($datosinstapos);

      $datainventario=$controlador->mostrarinventarioposafiliado($nroafiliacion,$namearchivo);
      $totalposengestion = pg_num_rows($datainventario);
  //  echo $totalposengestion;


    ?>


  <style type="text/css">
  .bs-example{
    margin: 20px;
  }
  .panel-title .glyphicon{
    font-size: 14px;
  }
  .modal-content{
    height:600px;
    overflow:auto;
  }
  </style>

  <!-- Page Content -->
  <div id="page-wrapper" >
    <div class="container-fluid">
      <div class="row" align="center">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $tituloPagina;?> / N&deg; de Afiliación: <b  color="blue"><?php echo $nroafiliacion;?> </b></h1>

    <!--         id del registro -->
 <input class="hidden" id="usuario" name="usuario"  value="<?php echo $usuario; ?>"  readonly="">
 <input class="hidden" id="nroafiliacion" name="nroafiliacion"  value="<?php echo $nroafiliacion; ?>"  readonly="">
 <input class="hidden" id="namearchivo" name="namearchivo"  value="<?php echo $namearchivo; ?>"  readonly="">
 <input class="hidden" id="totalserial" name="totalserial"  value="<?php echo $totalposengestion; ?>"  readonly="">


<?php $row = pg_fetch_assoc($datosinstapos) ?>
        <!-- Panel datos básicos -->
        <div class="col-lg-10" align="left">
          <div class="panel panel-info">
            <div class="panel-heading" style="text-align: center;">
              <i class="fa fa-user fa-fw"></i>Registro del Afiliado
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="list-group">
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Rif:
                  <span class="pull-center text-muted small"><b><?php echo $row['coddocumento'];?></b>
                  </span>

              </div>
              <div class="list-group-item">
                <i class="fa fa-check fa-fw"></i> Razón Social:
                <span class="pull-center text-muted small"><b><?php echo $row['razonsocial'];?></b>
                </span>
              </div>
                <div class="list-group-item">
                <i class="fa fa-check fa-fw"></i> Nombre:
                <span class="pull-center text-muted small"><b><?php echo isset($row['nombre']) ? $row['nombre'] : "" ;?></b>
                </span>
              </div>
              <div class="list-group-item">
                <i class="fa fa-check fa-fw"></i> Apellido:
                <span class="pull-center text-muted small"><b><?php echo isset($row['apellido']) ? $row['apellido'] : "" ;?></b>
                </span>
              </div>
              <div class="list-group-item">
                <i class="fa fa-check fa-fw"></i> Instituto u Organismo:
                <span class="pull-center text-muted small"><b><?php echo isset($row['institutoorganismo']) ? $row['institutoorganismo'] : "" ;?></b>
                </span>
              </div>
              <div class="list-group-item">
                <i class="fa fa-check fa-fw"></i> Tipo de Pos:
                <span class="pull-center text-muted small"><b><?php echo isset($row['tipopos']) ? $row['tipopos'] : "" ;?> </b>
                </span>
              </div>
              <div class="list-group-item">
                <i class="fa fa-check fa-fw"></i> Cantidad de Pos:
                <span class="pull-center text-muted small"><b><?php echo isset($row['cant_pos_confirm']) ? $row['cant_pos_confirm'] : "" ;?> </b>
                </span>
              </div>
              <div class="list-group-item">
                <i class="fa fa-check fa-fw"></i> Dirección:
               <span class="pull-center text-muted small" rows="4" ><b><?php echo $row['direccion'];?></b>
                </span>
              </div>
              <br>
              <div class="col-md-4 col-md-offset-4">
       <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
                Gestionar Servicio Técnico</button>
           </div>
            </div>
            <!-- /.list-group -->
          </div>
          <!-- /.panel-body -->
        </div>

        <!-- /.panel -->
         <div class="col-md-5 col-md-offset-5">

          <input type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='?cargar=buscarservitec&var=<?php echo $busqueda; ?>'">
        </div>
        </div>

        <br>
        <br>
        <!-- Modal servicio tecnico-->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header yellow">
          <h5 class="modal-title" id="exampleModalLabel" color="blue"><b style="color:blue;"><?php echo $tituloPagina;?>
            </b> /  Nro Afiliación:</b>  <b style="color:blue;"> <?php echo isset($row['nafiliacion']) ? $row['nafiliacion'] : "" ;?></b> / Razón Social: <b style="color:blue;"> <?php echo isset($row['razonsocial']) ? $row['razonsocial'] : "" ;?> </b>/ Rif: <b style="color:blue;"> <?php echo isset($row['coddocumento']) ? $row['coddocumento'] : "" ;?></b>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            Configuración del POS
          </div>
          <div ng-app="appServitec" ng-controller="controllerServitec" class="contenedor-tabla">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>NRO</th>
                  <th>Fecha de entrega CCR</th>
                  <th>N° de Terminal</th>
                  <th>N° Serial de POS</th>
                  <th>Tipo de POS</th>
                  <th>N° Sim Card</th>
                  <th>Acci&oacute;n</th>
                  <th>Gestionar</th>
                </tr>
              </thead>
              <?php
              $count=0;
              while ($row2=pg_fetch_array($datainventario)):
              $count++;
              ?>
              <tbody>
                <td align="center">
                  <input class="form-control" type="text" id="correlativo" value="<?php echo $count ?>" readonly>
                </td>
                <td>
                  <input class="form-control" type="date" id='<?php echo "fechaent".$count ?>' value="<?php echo isset($row2['fechaent']) ? $row2['fechaent']: date("Y-m-d")?>">
                </td>
                <td align="center">
                  <input class="form-control" id='<?php echo "numterminal".$count ?>' name="numterminal" onkeypress="return soloNumeros(event);"  value="<?php echo isset($row2['numtermx']) ? $row2['numtermx']: "" ?>" maxlength="15">
                </td>
                <td align="center">
                  <?php if ($row2['serialpo']!='') {?>
                  <input class="form-control" id='<?php echo "serialpos".$count ?>' name="serialpos" onkeypress="return soloNumeros(event);" value="<?php echo ($row2['serialpo']); ?>" readonly>
                  <?php } else { ?>
                  <input class="form-control" id='<?php echo "serialpos".$count ?>' name="serialpos" onkeypress="return soloNumeros(event);" value="<?php echo isset($row2['serialpo']) ? $row2['serialpo']: "" ?>" maxlength="15">
                  <?php } ?>
                </td>
                <td align="center">
                  <input class="form-control" type="text" id='<?php echo "tipopos".$count ?>' value="<?php echo isset($row['tipopos']) ? $row['tipopos']: "";?>" readonly>
                </td>
                <td align="center">
                 <input class="form-control" id='<?php echo "numsim".$count ?>'  name="numsim" required="" onkeypress="return soloNumeros(event);" value="<?php echo isset($row2['numsimx']) ?$row2['numsimx']: "";?>" maxlength="15">
                </td>
                <?php if ($row2['serialpo']=='') {?>
                <td align="center">
                  <button type="button" class="btn btn-primary" onClick="guardarserial('<?php echo $count; ?>')">Guardar</button>
                </td>
                <td align="center">
                  <button id='<?php echo "gestionar".$count ?>'  type="button" class="btn btn-info" btn btn-info btn-lg" data-toggle="modal" data-target="#mimodal" disabled="disabled">Gestionar</button>
                </td>
                <?php } else { ?>
                <td align="center">
                  <button type="button" class="btn btn-primary" onClick="guardarserial('<?php echo $count; ?>')">Actualizar</button>
                </td>
                 <td align="center">
                 <button id='<?php echo "gestionar".$count ?>'  type="button" class="btn btn-info" btn btn-info btn-lg" data-toggle="modal" data-target="#mimodal"  onClick="function(){alert('hola');}">Gestionar </button>
                </td>
                <?php } ?>
                <?php  endwhile; ?>
              </tbody>
            </table>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!--.modal fade-->




<!-- Modal -->
<div id="mimodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
       <div class="modal-header yellow">
                <h5 class="modal-title" id="exampleModalLabel" color="blue"><b style="color:blue;"><?php echo $tituloPagina;?></b> /  Nro Afiliación:</b>  <b style="color:blue;"> <?php echo isset($row['nafiliacion']) ? $row['nafiliacion'] : "" ;?></b> / Razón Social: <b style="color:blue;"> <?php echo isset($row['razonsocial']) ? $row['razonsocial'] : "" ;?> </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
        </div>
      <div class="modal-body">



 <?php   $TOTAL=0;
         $TOTAL++;  ?>

              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Gestion Atencion POS
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                              <th>NRO</th>
                              <th>Fecha Recepción (Almacen)</th>
                              <th>Nombre del Tecnico</th>
                              <th>Fecha de Gestion</th>
                              <th>Status Instalacion de POS</th>
                              <th>Fecha Instalacion de POS</th>

                            <th>ACCI&Oacute;N</th>
                          </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                            <td align="center"><?php echo $TOTAL;?> </td>
                            <td align="center">
                              <input class="form-control" type="date" id="fecharecpos" value="<?php echo isset($row['fecharecepalmacen']) ? $row['fecharecepalmacen'] : date("Y-m-d")?>">
                            </td>
                            <td align="center">
                              <select style="height:25px;
                              width: 150px;" name="tecnicos" id="tecnicos" required>
                              <option value="">Seleccione Tecnico</option>
                            </select>
                           </td>

                            <td align="center">
                             <input class="form-control" type="date" id="fechagestion" value="<?php echo isset($row['fechainstalacion']) ? $row['fechainstalacion'] : date("Y-m-d")?>">
                           </td>

                           <td align="center">
                          <select style="height:25px;
                          width: 150px;" name="statuspos" id="statuspos" required>
                          <option value="">Seleccione Estatus de Instalación</option>
                          </select>
                        </td>
                           <td align="center">
                             <input class="form-control" type="date" id="fechainspos" value="<?php echo isset($row['fechagestion']) ? $row['fechagestion'] : date("Y-m-d")?>">
                           </td>

                       <td align="center" width="70">
                       <button type="button" class="btn btn-primary" onClick="guardardatosins()">Guardar</button>
                       </td>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


      </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->



 <script languaje="javascript">
    seleccionarservicio();
  </script>
