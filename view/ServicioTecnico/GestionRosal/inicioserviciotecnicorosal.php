<?php
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
  }
?>

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i title="" style="color:#01DFD7" class="fa fa-list-alt"> </i> Resultados de la Gestión (El Rosal/Maracaibo)</h1>
      </div>


      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-yellow">
            <div class="panel-heading">
              BUSCAR DATA PARA SERVICIO TÉCNICO
            </div>

          <div class="panel-body">
            <div>
                <div class="col-lg-5">Banco:</div>
              <div class="col-lg-5">
                  <select name="clientes" id="clientes" required>
                  <option value="">Seleccione Banco</option>
                  </select>
              </div>
                <br>
            </div>
            <div class="col-lg-5">Fecha de Recepción:</div>
                <div class="col-lg-5">
                  <select name="fecha" id="fecha" required>
                  <option value="">Seleccione Fecha</option>
                  </select>
                </div>
          </div>
          <div>
              
              </div>
           <input type="hidden" id="valor" value="19" name="valor"/>
            <script languaje="javascript">
            generar();
            </script>

<div class="panel-footer">
          <p style="text-align: center;">
             <input class="btn btn-success" type="button" name="Buscar" value="Buscar" onClick="buscarservitecrosal()"/>
             <input class="btn btn-primary" type="button" name="Buscar" value="Buscar por Rif" onclick="mostrarBusquedaRif();"/>
          </p>
</div>
          </div>
        </div>
      </div>

         <div class="row" id="panelbusquedarif" style="display:none;">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Buscar Afiliado por RIF ó Razón Social
            </div>
          <div class="panel-body">
            <div>
                <div class="input-group custom-search-form center">
                       <input type="text" id="busqueda" class="form-control" placeholder="Eje:J-057105401...">
                          <span class="input-group-btn">
                           <button class="btn btn-default" type="button" onclick="gestionrosalrif()">
                                    <i class="fa fa-search"></i>
                          </button>
                        </span>
                 </div>
                <br>
            </div>
          </div>
             <div class="panel-footer">
  <br>
            </div>
          </div>
        </div>
      </div> 


      </div>
    </div>
  </div>

