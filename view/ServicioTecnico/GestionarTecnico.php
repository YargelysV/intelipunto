<?php 
if (isset($_SESSION["session_user"]))
{
	$_SESSION["session_tecnico"]="1";

	$tecnicos=$_SESSION["serviciotecnico"];

?>
<script type="text/javascript">
  	function dtCore(){
  		$.fn.dataTable.ext.buttons.alert = {
	    	className: 'buttons-alert',
	    	action: function ( e, dt, node, config ) {
	       		 $('#modalCRUD').modal('show');
   			}
  		};
    	
		tablaUsuarios = $('#tblplantillatecnicos').DataTable({  
		    "ajax":{            
		        "url": "api/CrudTecnicos.php", 
		        "method": 'POST', //usamos el metodo POST
		        "data":{opcion:1}
	    	},
		    "columns":[
		        {"data": "enlace", className: "text-center"},
		      	{"data": "correlativo"},
		      	{"data": "tecnico", className: "text-center" },
		      	{"data": "tipodoc"},
		      	{"data": "documento", className: "text-center" },
		      	{"data": "nombre", className: "text-center" },
		      	{"data": "apellido", className: "text-center" },
		      	{"data": "trabajo", className: "text-center" },
		      	{"data": "fecha", className: "text-center" },
		      	{"data": "estatuss", className: "text-center",
				    "render" : function (data, type, full, meta, row) {
				    	if(data=="Activo" || data==="Activo"){
				      		return "<a class='activarenlace' type='href' title='Ver'>Activo</a>";
				    	}else 
				    		return "<a class='activarenlace' type='href' title='Ver'>Inactivo</a>";
				    }	
				},
			    {"defaultContent":"<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='fa fa-edit'></i></button></div></div>"}
			    ],
			    "order" : [[1, "asc"]],

			    "columnDefs": [
			           {
			                 "targets": [ 0,2,8 ],
			                 "visible": false,
			             }
			         ],
			    "scrollX": true,
			    "scrollY": false,
			    "info":     false,
			    "scrollCollapse": false,
			    
			    dom: 'Bfrtip',
			    buttons: [
					{
					    extend: 'alert', 
					    text: '<button class="btn btn-warning" id="btnNuevo" type="button">+</button>'
					  }, 
					],
                "footerCallback": function ( tfoot, data, start, end, display ) {
                    var api = this.api();
                    var lastRow = api.rows().count();
                    for (i = 0; i < api.columns().count(); i++) {
                      $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
                    }
                    // Update footer
                    $( api.column( 1 ).footer() ).html(
                        'Total:'+lastRow 
                    );
                },
				language:{
			      "sProcessing":     "Procesando...",
			       "sLengthMenu":     "Mostrar _MENU_ registros",
			       "sZeroRecords":    "No se encontraron resultados",
			       "sEmptyTable":     "Ningún dato disponible en esta tabla",
			       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			       "sInfoPostFix":    "",
			       "sSearch":         "Buscar:",
			       "Print":           "Imprimir",
			       "sUrl":            "",
			       "sInfoThousands":  ",",
			        "sLoadingRecords": "Cargando...",
			      "oPaginate": false,
			       "fixedHeader": {"header": false, "footer": false},
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			      }
			    }
	  	});
		    
var fila; 
$('#formUsuarios').submit(function(e){                         
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
   	var tipodoc=document.getElementById('tipodocagregar').value;
    var documento=document.getElementById('documentoagregar').value;
    var nombre=document.getElementById('nombreagregar').value;
    var apellido=document.getElementById('apellidoagregar').value;
    var trabajo=document.getElementById('trabajoagregar').value;
    var tecnico=document.getElementById('codtecnico').value;
    //alert(tipodoc);                  
        $.ajax({
          url: "api/CrudTecnicos.php",
          type: "POST",
          datatype:"json",    
          data:  {"tipodoc":tipodoc, "documento":documento, "nombre":nombre, "apellido":apellido, "trabajo":trabajo, "opcion":opcion, "tecnico":tecnico},
          success: function(data) {
            tablaUsuarios.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});
        
$('table#tblplantillatecnicos tbody').on( 'click', 'a.activarenlace', function () {
//alert('algo');
    var table=$('table#tblplantillatecnicos').DataTable();
    var D =  table.row($(this).parents("tr")).data();
    var enlace=D.enlace;
    var url = "tecnicos?cargar=habilitartecnico&var="+enlace; 
    $(location).attr('href',url);

});
$("#btnNuevo").click(function(){
    opcion = 2; //alta           
    user_id=null;
    $("#formUsuarios").trigger("reset");
    $(".modal-header").css( "background-color", "#007bff");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("Registrar Técnico");
    $('#modalCRUD').modal('show');	
    $('#codtecnicodiv').removeAttr('disabled');    
});
//Editar        
$(document).on("click", ".btnEditar", function(){		        
    opcion = 3;//editar
    fila = $(this).closest("tr");	        
    tecnico = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    tipodoc = fila.find('td:eq(1)').text();
    documento = fila.find('td:eq(2)').text();
    nombre = fila.find('td:eq(3)').text();
    apellido = fila.find('td:eq(4)').text();
    trabajo = fila.find('td:eq(5)').text();
    //alert(tecnico);
    $("#tipodocagregar").val(tipodoc);
    $("#documentoagregar").val(documento);
    $("#nombreagregar").val(nombre);
    $("#apellidoagregar").val(apellido);
    $("#trabajoagregar").val(trabajo);
    $("#codtecnico").val(tecnico);
    $(".modal-header").css("background-color", "#17a2b8");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Editar Técnico");		
    $('#modalCRUD').modal('show');	
    $('#codtecnicodiv').Attr('disabled');	 
          
});

}
</script>
<script type="text/javascript">
        $(function () {
            $('#importDialog').modal({ 'show': false });
        });
</script>
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $tecnicos; ?>" />
<input type="hidden" id="opcion"  name="opcion"  value="1" />
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-gears" style="color:#F8F9F9;"> </i> Servicio Técnico / Plantilla Técnica</a>
        </div>
       </nav>
	   <div class="container"> 
	    </div>
			<div class="row">
	          <div class="col-lg-12" align="center">
	            <table id="tblplantillatecnicos" class="table table-bordered"   > 
	               <thead>
	                  <tr>
	                    <th>enlace</th>
	                    <th>Correlativo</th>
	                    <th>codtecnico</th>
	                    <th>Tipo&nbsp;Doc.</th>
	                    <th>Doc.&nbsp;Indentidad</th>
	                    <th>Nombres</th>
	                    <th>Apellidos</th>
	                    <th>Zona&nbsp;de&nbsp;Trabajo</th>
	                    <th>fecha</th>
	                    <th>estatus</th>
	                    <th>&nbsp;Acción&nbsp;</th>
	                  </tr>
	                </thead>
                    <tfoot>
                        <th colspan="4" style="text-align:left">Total:</th> 
                    </tfoot>
	            </table>
	          </div>
	        </div>		
		</div>
	</div>
</div>
<!--Modal para CRUD-->
<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        <form id="formUsuarios">    
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Tipo&nbsp;Doc.</label>
                    <select id="tipodocagregar" required>
                                       <option value="">Seleccionar</option>
                                       <option value="V">V</option>
                                       <option value="E">E</option>
                                       <option value="P">P</option>
                    </select>
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Doc. Indentidad</label>
                    <input type="text" class="form-control" id="documentoagregar" maxlength="10">
                    </div> 
                    </div>    
                </div>
                <div class="row"> 
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Nombres</label>
                    <input type="text" class="form-control" id="nombreagregar">
                    </div>               
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Apellidos</label>
                    <input type="text" class="form-control" id="apellidoagregar">
                    </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group" id="codtecnicodiv">
                        <label for="" class="col-form-label">Técnico</label>
                        <input type="text" class="form-control" id="codtecnico" disabled>
                        </div>
                    </div>    
                </div> 
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                        <label for="" class="col-form-label">Zona de Trabajo</label>
                        <input type="text" class="form-control" id="trabajoagregar">
                        </div>
                    </div>    
                </div>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnGuardar" class="btn btn-dark">Guardar</button>
            </div>
        </form>    
        </div>
    </div>
</div>		
<?php

	}else{
		header("Location:../salir");
	}
	?>
