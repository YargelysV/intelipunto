<?php
  $usuario=$_SESSION["session_user"];
  $tecnico=$_SESSION["serviciotecnico"];
  $var=($_REQUEST['var']);
  $_SESSION['bsqrecepcion']=$var;
  $array = explode("/",$var);
  $banco= $array[0];
  $fechadetalle=$array[1];
  $region=$array[2];
  $namearchivo = isset($array[6]) ? $array[6] : "";
  $masiva = isset($array[3]) ? $array[3] : "";
  $cliente = isset($array[4]) ? $array[4] : "";
  $fecha = isset($array[5]) ? $array[5] : "";
  

   $retroceder2=$banco.'/'."";
  $retroceder1="".'/'.$fechadetalle;


  $_SESSION["fecha"]=$fechadetalle;
  $_SESSION["returnpos"]=$banco.'/'.$fechadetalle;
  $controladorc =new ControladorServicioTecnico();
  $comerciali=$controladorc->sp_verclientegestion($banco,$fechadetalle,$namearchivo,$region);
  $databanco = pg_fetch_assoc($comerciali);
  $coun = pg_num_rows($comerciali);

  if ($coun==0) {  ?>
?>

<script type="text/javascript">
redirectResult();
</script>

<?php } else { ?>


<style type="text/css">
  .dataTables_wrapper .dt-buttons {
  /*width: 300px;*/
  float: left;
}
</style>



<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>">
<input type="hidden" id="fechadetalle" name="fechadetalle" value="<?php echo $fechadetalle; ?>">
<input type="hidden" id="namearchivo" name="namearchivo" value="<?php echo $namearchivo; ?>">
<input type="hidden" id="databanco" name="databanco" value="<?php echo  $databanco['ibp']; ?>">
<input type="hidden" id="permisos" name="permisos" value="<?php echo  $tecnico; ?>">
<input type="hidden" id="region" name="region" value="<?php echo  $region; ?>">

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-gears" style="color:#F8F9F9;"> </i> Servicio Técnico/Gestión Técnica-Busqueda Detallada: </a>
          <br>
          <b style="color:#5DADE2;">Búsqueda: <?php echo $databanco['ibp'];?></b>
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="gestionarurbina" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>&nbsp;Acción&nbsp;</th>
                <th>&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Servicio&nbsp;&nbsp;</th>
                <th>Fecha de Recepción</th>
                <th>N°&nbsp;de&nbsp;Afiliación</th>
                <th>N°&nbsp;de&nbsp;Factura</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nombre&nbsp;del&nbsp;Archivo</th>
                <th>Tipo de POS</th>
                <th>Cantidad de POS</th>
                <th>Tipo de Línea</th>
                <th>registro</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
          </tfoot>
        </table>
      </div>
    </div>

      <div class="row" align="center">
                <div class="col-md-4 col-md-offset-4">
                   <?php if ($masiva=='1') {?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarInstalar&var=<?php echo $retroceder1; ?>'"/>
                   <?php } else if ($masiva=='2') {?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarInstalar&var=<?php echo $retroceder2; ?>'"/> 
                   <?php } else { ?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='gestionarServicoTecnico.php'"/>
                   <?php } ?>
                </div>
            </div>

     

    </div>
  </div>
</div>
<?php  } ?>

<script type="text/javascript">
  function dtCore(){
    
  var banco=document.getElementById('banco').value;
  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;
  var region= document.getElementById('region').value;
  
    $('table#gestionarurbina').DataTable({
      "ajax": {
      "url": "api/selectServicioConfig.php",
            "type": "POST",
            "data": {"banco":banco,"fechadetalle":fechadetalle,"namearchivo":namearchivo, "region":region}
      },
      "columns": [
        {"data": "correlativo", className: "text-center"},
        {"data": "enlace", className: "text-center"},
        {"defaultContent":"<a class='editar' type='href' title='Ver'>Gestionar</a>", className: "text-center"},
        {"data": "estatuservices", className: "text-center"},
        {"data": "fecharecepcion", className: "text-center" },
        {"data": "nafiliacion", className: "text-center" },
        {"data": "factura", className: "text-center" },
        {"data": "razonsocial", className: "text-center" },
        {"data": "documento", className: "text-center" },
        {"data": "ibp", className: "text-center" },
        {"data": "namearchivo"},
        {"data": "tipopos"},
        {"data": "cantterminalesasig", className: "text-center" },
        {"data": "tipolinea", className: "text-center" },
        {"data": "registro", className: "text-center" }
      ],
      "order" : [[0, "asc"]],

            "columnDefs": [
                   {
                         "targets": [ 1 ],
                         "visible": false,
                     }
                 ],

            "scrollX": true,
            "scrollY": false,
            "info":     false,
            "scrollCollapse": false,
            
            dom: 'Bfrtip',
            buttons: [
          {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
            exportOptions : {
              columns: [ 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
            }
          }],
            
            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });

    $('table#gestionarurbina tbody').on( 'click', 'a.editar', function () {
    //alert('algo');
          var table=$('table#gestionarurbina').DataTable();
          var D =  table.row($(this).parents("tr")).data();
          var enlace=D.enlace;
          var url = "gestionarServicoTecnico?cargar=datoserialpos&var="+enlace; 
          $(location).attr('href',url);

    });

}
  </script>


