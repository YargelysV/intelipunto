<?php
/*buscarrif.php view*/
  $tecnicos=$_SESSION["serviciotecnico"];
  $usuario=$_SESSION["session_user"];
  //$region=$_SESSION["regiones"];
  $var=($_REQUEST['var']);
   $_SESSION['buscarrif']=$var;
  $_SESSION['bsqrecepcion']='';
  $array = explode("/",$var);
  $rif= $array[0];
  $usuario= $array[1];
  
  $controladorclient =new ControladorServicioTecnico();
  $comercia= $controladorclient->sp_buscarafiliadoRifgestion($rif,$usuario);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>
<style type="text/css">
  .dataTables_wrapper .dt-buttons {
  /*width: 300px;*/
  float: right;
}

</style>
<input type="" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $tecnicos; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="" style="color:#F8F9F9;"> </i> Busqueda por Rif: Resultados de la Gestion.. </a>
         <br>
         <b style="color:#5DADE2;"> Búsqueda: <?php echo $rif; ?> </b>
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="confiposbancos" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>&nbsp;Acción&nbsp;</th>
                <th>&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Servicio&nbsp;&nbsp;</th>
                <th>N°&nbsp;de&nbsp;Afiliación</th>
                <th>N°&nbsp;de&nbsp;Factura</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nombre&nbsp;del&nbsp;Archivo</th>
                <th>Tipo de POS</th>
                <th>Cantidad de POS</th>
                <th>Tipo de Línea</th>
                <th>registro</th>
              </tr>
            </thead>
        </table>
      </div>
    </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='gestionarServicoTecnico.php'"/>
        </div>
      </div>

    </div>
  </div>
</div>
<?php  } ?>

<script type="text/javascript">
  function dtCore(){
  
  var nrorif=document.getElementById('nrorif').value;
  var usuario=document.getElementById('usuario').value;  
  
  $('table#confiposbancos').DataTable({
    "ajax": {
    "url": "api/selectServicioAfiliadoRifgestion.php",
          "type": "POST",
          "data": {"nrorif":nrorif, "usuario":usuario}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace", className: "text-center"},
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Gestionar</a>", className: "text-center"},
      {"data": "descestatuss", className: "text-center"},
      {"data": "nafiliacion", className: "text-center" },
      {"data": "factura", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "documento", className: "text-center" },
      {"data": "ibp", className: "text-center" },
      {"data": "direccion"},
      {"data": "namearchivo"},
      {"data": "tipopos"},
      {"data": "cantterminalesasig", className: "text-center" },
      {"data": "tipolinea", className: "text-center" },
      {"data": "registro", className: "text-center" }
    ],
    "order" : [[0, "asc"]],

          "columnDefs": [
                 {
                       "targets": [ 1 ],
                       "visible": false,
                   }
               ],
          buttons: [
            'print'
            
          ],
          "scrollX": true,
          "scrollY": false,
          "info":     false,
          "scrollCollapse": false,
          
          dom: 'Bfrtip',
          age:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });

  $('table#confiposbancos tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#confiposbancos').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "gestionarServicoTecnico?cargar=datoserialpos&var="+enlace; 
        $(location).attr('href',url);

  });
}
  </script>