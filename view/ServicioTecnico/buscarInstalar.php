<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] :"";
  $fecha= isset($array[1]) ? $array[1] :"";
  
   if ($banco=="")
  {
    $origen='1';
  }
  if ($fecha=="")
  {
    $origen='2';
  }

   $controlador =new ControladorServicioTecnico();
   $resultados=$controlador->sp_buscargestionservicio($banco,$fecha,$origen);
    $databanco = pg_fetch_assoc($resultados);
 
  ?>
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $databanco['ibp']; ?>" />
<input type="hidden" id="origen"  name="origen"  value="<?php echo $origen; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-gears"> </i> Servicio Técnico / Resultados de la Gestión: Busqueda Masiva<b style="color:#FF5733;">
          </b></a>
          <br>
          <b style="color:blue;">Búsqueda:<?php if( $banco!='') echo $databanco['ibp']; else  echo $fecha;?></b>
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="gestionarurbinafechas" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>enlace</th>
                <th>&nbsp;Acción&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Fecha&nbsp;de&nbsp;Recepción&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>codbanco</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Fecha&nbsp;de&nbsp;Carga&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nombre&nbsp;del&nbsp;Archivo</th>
                <th>Cantidad de Registros</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="9" style="text-align:left">Total:</th>
          </tfoot>
        </table>
      </div>
    </div>
      <br>
      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='gestionarServicoTecnico.php'"/>
        </div>
      </div>

<script type="text/javascript">
  function dtCore(){
    
  var banco=document.getElementById('banco').value;
  var fecha= document.getElementById('fecha').value;
  var origen= document.getElementById('origen').value;
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#gestionarurbinafechas').DataTable({
    "ajax": {
    "url": "api/selectServicioInstalar.php",
          "type": "POST",
          "data": {"banco":banco,"fecha":fecha,"origen":origen}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace", className: "text-center"},
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Gestionar</a>", className: "text-center"},
      {"data": "ibp", className: "text-center"},
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "codigobanco", className: "text-center" },
      {"data": "fechacarga", className: "text-center" },
      {"data": "namearchivo", className: "text-center" },
      {"data": "cantidad", className: "text-center" }
    ],
    "order" : [[0, "asc"]],

          "columnDefs": [
                 {
                       "targets": [ 1,5 ],
                       "visible": false,
                   }
               ],
          buttons: [
          {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
            exportOptions : {
              columns: [ 0, 3, 4, 6, 7, 8]
            }
          }],
            
            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
          
          dom: 'Bfrtip',
          age:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });

  $('table#gestionarurbinafechas tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#gestionarurbinafechas').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "gestionarServicoTecnico?cargar=buscarInstPOS&var="+enlace; 
        $(location).attr('href',url);

  });
}
  </script>
