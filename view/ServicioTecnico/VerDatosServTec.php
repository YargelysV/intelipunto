<?php

    $afiliacion=($_REQUEST['Afiliacion']);

    $controlador =new controladorServicioTecnico();
    $datosinstapos=$controlador->verdatosinstapos($afiliacion);

    $controladorservi =new ControladorServicioTecnico();
    $serviciotecnico=$controladorservi->spdatoservitec($afiliacion);
    $total=pg_num_rows($serviciotecnico);
?>
 
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> N&deg; de Afiliación: <font color="blue"><?php echo $afiliacion;?> </font></h1>
            </div>
            <?php if ($total>0) 
            {
            ?>
                <div class="panel-body">
                    <div class="row">
                       <div class="col-lg-100">
                            <div class="col-lg-100">
                                <form role="form">
                                    <?php $row = pg_fetch_assoc($serviciotecnico) ?>
                                    
                                    <div class="panel-heading" style="text-align: center;">
                                         <b style="color:blue;"> Registro del Afiliado Comprador </b>
                                    </div>

                                    <div class="form-group">
                                        <label>Rif/CI </label>
                                        <input class="form-control" value="<?php echo $row['coddocumento'];?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Razón Social</label>
                                        <input class="form-control" value="<?php echo $row['razonsocial'];?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Razón Comercial o Fantasía</label>
                                        <input class="form-control" value="<?php echo $row['razoncomercial_fantasia'];?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Nombre y Apellido</label>
                                        <input class="form-control" value="<?php echo $row['nombre'];?><?php echo $row['apellido'];?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Instituto u Organismo</label>
                                        <input class="form-control" value="<?php echo $row['instituto_organismo'];?>" readonly>
                                    </div>

                                    <div class="panel-heading" style="text-align: center;">
                                        <b style="color:blue;"> Direccion </b>
                                    </div>

                                    <div class="form-group">
                                        <label>Dirección de Instalación</label>
                                        <textarea class="form-control" rows="4" readonly><?php echo " Calle o Av: ".$row['calleav']." Localidad: ".$row['localidad']." Sector: ".$row['sector']." Urbanización: ".$row['urbanizacion']." Local: ".$row['local']." Estado: ".$row['estado']." Código Postal: ".$row['codigopostal']." Punto de Ref: ".$row['puntoref'];?> 
                                        </textarea>
                                    </div>

                                    <div class="panel-heading" style="text-align: center;">
                                        <b style="color:blue;"> Teléfonos </b>
                                    </div>

                                    <div class="form-group">
                                        <label>Telefono Fijo 1</label>
                                        <input class="form-control" value="<?php echo $row['telf1'];?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Telefono Fijo 2</label>
                                        <input class="form-control" value="<?php echo $row['telf2'];?>" readonly>
                                    </div>

                                     <div class="form-group">
                                        <label>Telefono Móviles 1</label>
                                        <input class="form-control" value="<?php echo $row['tlf_movil1'];?>" readonly>
                                    </div>

                                     <div class="form-group">
                                        <label>Telefono Móviles 2</label>
                                        <input class="form-control" value="<?php echo $row['tlf_movil2'];?>" readonly>
                                    </div>

                                    <div class="panel-heading" style="text-align: center;">
                                        <b style="color:blue;"> Correos </b>
                                    </div>

                                    <div class="form-group">
                                        <label>Correo Electrónico Corporativo</label>
                                        <input class="form-control" value="<?php echo $row['email_corporativo'];?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Correo Electrónico Comercial</label>
                                        <input class="form-control" value="<?php echo $row['email_comercial'];?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Correo Electrónico Personal</label>
                                        <input class="form-control" value="<?php echo $row['email_personal'];?>" readonly>
                                    </div>
                                    </br>  
                                    </br>   
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<style type="text/css">  
  
.contenedor-tabla{

display: table;
border: medium #C0C0C0 solid;
width: 800px;

}

.contenedor-fila{

display: table-row;
width: 25%;
vertical-align:center;
text-align: center; 
}

.contenedor-columna{

display: table-cell;
width: 25%;
vertical-align:center;  
text-align: center;
}
</style>          
                    <?php
                    $cantidad_pos = $row['cantidad_pos'];
                    ?>
                    <div  style="text-align: center;">
                        <p>   DATOS DEL SERVICIO TECNICO </p>
                    </div>    
                    <div class="contenedor-tabla">  
                         <div class="contenedor-fila">  
                                <div class="contenedor-columna">  
                                <p>NRO</p>
                            </div>
                            <div class="contenedor-columna">  
                                <p>Serial de POS</p>
                            </div>
                            <div class="contenedor-columna">  
                                <p>Tipo de POS</p>
                            </div>
                            <div class="contenedor-columna">  
                                <p>Acción</p>
                            </div>
                        </div>
                    </div>     
                    <div ng-app="appServitec" ng-controller="controllerServitec" class="contenedor-tabla">  
                        <div ng-repeat="x in [].constructor(<?php echo $cantidad_pos ?>) track by $index">
                            <div class="contenedor-fila"> 
                                <div class="contenedor-columna">  
                                    <p>{{$index+1}}</p> 
                                </div>  
                                <div class="contenedor-columna">  
                                   <p>  <input type="text"   id="{{$index+1}}"  name="{{$index+1}}" ng-model="country[($index+1)]" ng-keyup="complete(country[($index+1)], $index+1)" class="form-control" />  
                                    <ul class="list-group" ng-model="hidethis[($index+1)]" ng-hide="hidethis[($index+1)]">    <li class="list-group-item" ng-repeat="countrydata in filterCountry[($index+1)]" ng-click="fillTextbox(countrydata[($index+1)], $index+1)">{{countrydata}}</li>  
                                    </ul>  </p> 
                                </div>  
                                <div class="contenedor-columna">  
                                    <p> <input type="hidden" id="nroafiliacion" name="nroafiliacion" value="<?php echo $afiliacion;?>" />
                                    <input type="hidden" id="rifclie" name="rifclie" value="<?php echo $row['coddocumento'];?>" />  
                                    <input class="form-control" type="text" id="tipopos" value="<?php echo $row['tipopos'] ;?>" readonly></p> 
                                </div>  
                                <div class="contenedor-columna">  
                                    <p>   <input type="button" class="btn btn-success" name="enviar" value="Guardar" onclick="asignarserial()" />
                                     <button type="button" class="btn btn-success" btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Gestionar</button> </p> 
                                </div> 
                            </div>   
                        </div>
                    </div>
                <?php } ?>  

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                DATOS DEL SERVICIO TECNICO
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>NRO</th>
                                        <th>Fecha entrega de CCR</th>
                                        <th>Fecha Recepción de POS</th>
                                        <th>N° Serial de POS</th>
                                        <th>Fecha Instalacion de POS</th>
                                        <th>Status Instalacion de POS</th>
                                        <th>ACCI&Oacute;N</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $cantidad_pos = $row['cantidad_pos'];
                                    $TOTAL=0;
                                    $TOTAL++;
                                    ?>

                                    <tr>
                                        <td align="center"><?php echo $TOTAL;?> </td>
                                        <td>
                                            <input class="form-control" type="date" id="fechaent" value="<?php echo isset($row['fechaentccr']) ? $row['fechaentccr'] : date("Y-m-d")?>">
                                        </td>
                                        <td>
                                            <input class="form-control" type="date" id="fecharec" value="<?php echo isset($row['fecharecpos']) ? $row['fecharecpos'] : date("Y-m-d");?>">
                                        </td>
                                        <td>  
                                            <input class="form-control" type="text" name="nombre" id="nombre" placeholder="Usuario" onkeyup="autocompletar()" autocomplete="off"><!--Campo de busqueda-->
                                        </td>
                                        <td>
                                            <input class="form-control" type="date" id="fechains" value="<?php echo isset($row['fechainspos']) ? $row['fechainspos'] :  "";?>">
                                        </td>
                                        <td>
                                            <select name="estatus" id="estatus" style="width:280px;padding-left:5px;color:#000000">
                                        
                                            <?php

                                            $controlador =new controladorServicioTecnico();
                                            $estatuspos=$controlador->estatusinspos();
                                            while ($row2=pg_fetch_array($estatuspos)):

                                            ?>
                                            <option value="<?php echo $row2['codestatus']?>" 
                                            
                                            <?php
                                            if ($row2['codestatus']=="1")
                                            {
                                            echo "selected";
                                            }?>><?php
                                            echo $row2['descestatus']
                                            ?></option>
                                            
                                            <?php
                                            endwhile;
                                            ?>
                                            </select>
                                        </td>        
                                        <td align="center" width="70">
                                            <input type="hidden" id="nroafiliacion" name="nroafiliacion" value="<?php echo $afiliacion;?>" />
                                            <input type="button" class="btn btn-success" name="enviar" value="Guardar" onclick="ModificarServiTec()"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="modal-footer">
                                <input type="button" align="center" class="btn btn-success" name="enviar" value="Agregar Nueva Gestion" onclick="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>

