<?php
//  session_start();
$tituloPagina=$_SESSION["page"];
if (isset($_SESSION["session_user"]))
{
  $usuario=$_SESSION["session_user"];

}

?>
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-phone-square" style="color:#045FB4"> </i> <?php echo $tituloPagina;?>/Gesti&oacute;n Comercial </h1>
      </div>


      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              BUSCAR AFILIADO
            </div>

          <div class="panel-body">
            <div>
              <div class="col-lg-5">Banco:</div>
              <div class="col-lg-5">
                  <select name="clientes" id="clientes" required>
                  <option value="">Seleccione Banco</option>
                  </select>
              </div>
              <br>
            </div>

            <div>
                <div class="col-lg-5">Fecha de Recepci&oacute;n:</div>
                <div class="col-lg-5">
                  <select name="fecha" id="fecha" required>
                  <option value="">Seleccione Fecha</option>
                  </select>
                </div>
            </div>
          </div>
           <input type="hidden" id="valor" value="1" name="valor"/>
           <input type="hidden" id="tipousuario" value='<?php echo $_SESSION["codtipousuario"];?>' name="tipousuario"/>
           <input type="hidden" id="ejecutivo" value='<?php echo $_SESSION["ejebanco"]?>' name="ejecutivo"/>
           <input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"]?>' name="usuario"/>
           <input type="hidden" id="coordinador" value='<?php echo $_SESSION["coordbanco"]?>' name="coordinador"/>
           <input type="hidden" id="coordinadordos" value='<?php echo $_SESSION["coordinadordos"]?>' name="coordinadordos"/>

           <?php if (($_SESSION["codtipousuario"]=='W')|| ($_SESSION["codtipousuario"]=='E') || ($_SESSION["codtipousuario"]=='O') || ($_SESSION["codtipousuario"]=='B') || ($_SESSION["ejebanco"]!=0) || ($_SESSION["coordbanco"]!=0) || ($_SESSION["coordinadordos"]!=0)) { ?>
        
          <script languaje="javascript">
            generareje();
            </script>

          <?php } else {  ?>           
            <script languaje="javascript">
            generar();
            </script>
          <?php } ?>


<div class="panel-footer">
          <p style="text-align: center;">
             <input class="btn btn-success" type="button" name="Buscar" value="Buscar" onClick="buscarcomercializacioncliente()"/>
             <input class="btn btn-primary" type="button" name="Buscar" value="Buscar por Rif" onclick="mostrarBusquedaRif();"/>
          </p>
</div>
          </div>
        </div>
      </div>



<!-- busqueda por RIF o Razón Social -->
      <div class="row" id="panelbusquedarif" style="display:none;">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading" id="headingEspecial">
              Buscar Afiliado por RIF &oacute; Raz&oacute;n Social
            </div>
          <div class="panel-body">
            <div>
                <div class="input-group custom-search-form center">
                       <input type="text" id="nrorif" class="form-control" placeholder="Eje:J-057105401...">
                          <span class="input-group-btn">
                           <button class="btn btn-default" type="button" onclick="buscarclienteRif()">
                                    <i class="fa fa-search"></i>
                          </button>
                        </span>
                 </div>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end panel busqueda por rif -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->



