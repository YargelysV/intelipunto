<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] :"";
  $fecha= isset($array[1]) ? $array[1] :"";
  $tipousuario= isset($array[2]) ? $array[2] :"";
  $ejecutivo= isset($array[3]) ? $array[3] :"";
  $usuario= isset($array[4]) ? $array[4] :"";
  $coordinador= isset($array[5]) ? $array[5] :"";
  $coordinadordos= isset($array[6]) ? $array[6] :"";

 if ($banco=="")
  {
    $origen='1';
  }
  if ($fecha=="")
  {
    $origen='2';
  }

  $controladorclient =new ControladorComercializacion();
  $comercia= $controladorclient->sp_buscarclientecomercializacion($banco,$fecha,$origen,$tipousuario,$ejecutivo,$usuario,$coordinador,$coordinadordos);
  $databanco = pg_fetch_assoc($comercia);

  

?>

<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
 <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $databanco['ibp']; ?>" />
<input type="hidden" id="origen"  name="origen"  value="<?php echo $origen; ?>" />
<input type="hidden" id="tipousuario"  name="tipousuario"  value="<?php echo $tipousuario; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="coordinador"  name="coordinador"  value="<?php echo $coordinador;?>" />
<input type="hidden" id="coordinadordos"  name="coordinadordos"  value="<?php echo $coordinadordos;?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-phone-square" style="color:#F8F9F9;"> </i> Comercialización/Gestión Telefónica - Busqueda General: </a>
          <br>
           
          <br> <b style="color:#5DADE2;"> Búsqueda: <?php if( $banco!='') echo $databanco['ibp']; else  echo $fecha; ?></b>
        
        </div>
       </nav>
    </div>
     <div class="row">
          <div class="col-12">
            <table class="table table-bordered"  id="busquedafechacomerc" style="width: 100%"> 
                <thead>
                  <tr>
                    <th>correlativo</th>
                    <th>enlace</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Fecha de Recepción</th>
                    <th>Fecha de Carga</th>
                    <th>Nombre del Archivo</th>
                    <th>Cantidad de Registros</th>
                  </tr>
                </thead>
                <tfoot>
            <tr>
                <th colspan="8" style="text-align:left">Total:</th>
            </tr>
          </tfoot>
            </table>
          </div>
        </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='gestioncomercializacion.php'"/>
        </div>
      </div>

         <!--div align="right">
                <a href="view/ClientePotencial/recepcion/reporte_clientes_pdf?cliente=<?php echo  $banco;?>" target="_blank"><img src="img/pdf.png" height="50" width="60">  <b style="color:red;">Descargar</b> </a>
         </div-->
    </div>
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
  var banco=document.getElementById('banco').value;
  var fecha= document.getElementById('fecha').value;
  var origen= document.getElementById('origen').value;
  var ejecutivo= document.getElementById('ejecutivo').value;
  var tipousuario= document.getElementById('tipousuario').value;
  var usuario= document.getElementById('usuario').value;
  var coordinador= document.getElementById('coordinador').value;
  var coordinadordos= document.getElementById('coordinadordos').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('#busquedafechacomerc').DataTable({
    "ajax": {
    "url": "api/selectComercializacionMasiva.php",
          "type": "POST",
          "data": {"banco":banco,"fecha":fecha,"origen":origen, 
          "ejecutivo":ejecutivo,"tipousuario":tipousuario,"usuario":usuario,"coordinador":coordinador,"coordinadordos":coordinadordos}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace"},
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Ver Detalle</a>", className: "text-center"},
      {"data": "ibp" },
      {"data": "fecharecepcion" },
      {"data": "fechacarga", className: "text-center" },
      {"data": "namearchivo", className: "text-center" },
      {"data": "cantidad", className: "text-center" }
    ],
    "info":     false,
    "columnDefs": [
           {
               "targets": [ 1 ],
               "visible": false,
           }
    ],
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
         
        exportOptions : {
        columns: [ 0, 3, 4, 5, 6, 7]
      }
    }],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }

  });
  $('table#busquedafechacomerc tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#busquedafechacomerc').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "gestioncomercializacion?cargar=buscarrecepcion&var="+enlace; 
        $(location).attr('href',url);

  });
}
</script>
