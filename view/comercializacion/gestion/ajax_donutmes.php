<?php
	require("../../../controller/controladorComercializacionAjax.php");
	
if(isset($_GET['Donut_chart_mes'])){
 $arraycompleto=$_GET['Donut_chart_mes'];
 $array = explode("/",$arraycompleto);
 $usuario= isset($array[0]) ? $array[0] :"";
 $fecha_desde= isset($array[1]) ? $array[1] :"";	
 $fecha_hasta= isset($array[2]) ? $array[2] :""; 

//construct data grafica tipo Dona mensual
$ctrlcomercial =new gestionComercializacion();
$comercial=$ctrlcomercial->sp_graficacontactomensual($usuario,$fecha_desde,$fecha_hasta);
$datosmes = array();
$sincontacto1="Sin Contactar";
$contactado1="Contactado";
$venta1="Venta con Fondos";
$facturado1="Facturado";
$red1='#FF0000';//sin contactar
$orange1='#FFA500';//contactado
$purple1='#800080';//venta con fondos
$blue1='#04B4AE';//facturado
while($rows1 = pg_fetch_array($comercial))
{
    
   if ($rows1["descripcion"]==$contactado1) {
     
      $color=$orange1;
   } 
   if ($rows1["descripcion"]==$venta1) {
      
      $color=$purple1;
   } 
    if ($rows1["descripcion"]==$sincontacto1) {
       
      $color=$red1;
    }
   if ($rows1["descripcion"]==$facturado1) {
   
      $color=$blue1;  
   } 

 $datosmes[] = array(
 
  'label'  => $rows1["descripcion"],
  'value'  => $rows1["cantidad"],
  'color'  => $color,   

 );
}
$datosmes = json_encode($datosmes);
echo $datosmes;


}

?>