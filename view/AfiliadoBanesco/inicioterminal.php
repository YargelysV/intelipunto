   <!-- Page Content -->
   <div id="page-wrapper">
     <div class="container-fluid">
       <div class="row">
         <div class="col-lg-12">
           <h1 class="page-header"><i style="color:#FF335E;" class="fa fa-mobile"></i> Desafiliación de Equipos</h1>
         </div>


         <div class="col-md-8 col-md-offset-2">
           <form action="?cargar=terminaltransferencia" method="post" enctype="multipart/form-data" name="formarchivo">
             <div class="panel panel-primary">
               <div class="panel-heading">
                 Seleccione desde cualquier ubicaci&oacute;n de su computador, el archivo que desea cargar al sistema.
               </div>



               <div class="panel-body">
                 <div class="col-lg-5">Seleccionar Archivo CSV</div>
                 <div class="col-lg-5">
                   <input name="archivo" type="file" id="archivo" required />
                 </div>
               </div>


               <div class="panel-footer">
                 <p style="text-align: center;">
                   <input name="enviar" id="enviar" class="btn btn-primary" type="button" value="Siguiente" onclick="validararchivo()" />
                 </p>
               </div>

             </div>
         </div>
         </form>


       </div>
       <!-- /.row -->
     </div>
     <!-- /.container-fluid -->
   </div>
   <!-- /#page-wrapper -->