<?php
set_time_limit(0);
//error_reporting(0);
if (isset($_SESSION["session_user"]))
{

	$name=$_FILES['archivo']['name'];
	$type=$_FILES['archivo']['type'];				
	$tipo="application/vnd.ms-excel";
	
	
	$controladorclientes =new controladorBanesco();
	$cliente=$controladorclientes->spverclientesbanescofact();
	$clientact = 0;
	$clienteactivos=array();
	while($cliea = pg_fetch_array($cliente)){
	$clienteactivos[$clientact] =$cliea['registro'].$cliea['razon'].$cliea['documento'].$cliea['afiliado'].$cliea['serial'];

	$clientact++;
	}	

	//BUSCAR AFILIADO EXISTE
/*	$controladorafiliado =new controladorBanesco();
	$afiliadoc=$controladorafiliado->spverafiliadosbanesc();
	$afiliadoact = 0;
	$afiliadosactivos=array();
	while($afilia = pg_fetch_array($afiliadoc)){
	$afiliadosactivos[$afiliadoact] =$afilia['afiliado'];
	//echo $afiliadosactivos[$afiliadoact]."<br>";
	$afiliadoact++;
	}	*/

/*	$controladorstatus =new controladorBanesco();
	$status=$controladorstatus->verestatusafiliado();
	$status_afiliado = 0;
	$opcionesejecutivo=array();
	while($descestatus = pg_fetch_array($status)){
	$estatusafiliado[$status_afiliado] = $descestatus['desc_estatus'];
	//echo $opcionesejecutivo[$tipejecutivo]."<br>";
	$status_afiliado++;
	}*/


	$controladorrechazo =new controladorBanesco();
	$rechaz=$controladorrechazo->vermotivorechazo();
	$status_rechazo = 0;
	$opcionrechazo=array();
	while($id_motivo = pg_fetch_array($rechaz)){
	$estatusderechazo[$status_rechazo] = $id_motivo['idmotivo'];
	//echo $id_motivo['idmotivo']."<br>";
	$status_rechazo++;
	}

	$controladorbanco =new controladorBanesco();
	$prefijobanco=$controladorbanco->verprefijobanco();
	$pre = 0;
	$opcionesprefijo=array();
	while($prefijo = pg_fetch_array($prefijobanco)){
	$opcionesprefijo[$pre] = $prefijo['codigobanco'];
	//echo $opcionesprefijo[$pre]."<br>";
	$pre++;
	}

	// if ($type!=$tipo){
	// 	echo "<script>alertify.alert('Error Cargando el Archivo, Verifique e intente nuevamente.', function () {
	// 		window.location.assign('PrintFile.php');});</script>";
	// }
	// else{

		$date = date("Y-n-j H:i:s");
		$archivo = $_FILES['archivo']['tmp_name'];
		$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");

		?>

<style type="text/css">

.name{
 text-align: center;
width: 900px; 
height: 200px; color:red;
}
 
</style>	


		<div class="sub-content" align="center" >
			<div id="page-wrapper">
	            <div class="container-fluid">
	                <div class="row" id="errores">
	                	<div class="col-lg-12">
                        <h1 class="page-header">Desafiliacion de equipos - Banesco</h1>
                    	</div>
	                	<br>	
						<?php


						$cont=0;
						$control=0;
						while($data = fgetcsv($fichero,10000,";"))
						{
							$cont++;
							if ($cont==1)
							{
								if ((utf8_encode(trim($data[0]))!="Consecutivo") || (utf8_encode(trim($data[1]))!="ID")||(utf8_encode(trim($data[2]))!="Rif / CI") ||(utf8_encode(trim($data[3]))!="Razon Social") ||(utf8_encode(trim($data[4]))!="Banco") || (utf8_encode(trim($data[5]))!="Actividad Económica") ||(utf8_encode(trim($data[6]))!="Afiliado") ||(utf8_encode(trim($data[7]))!="Correo Electronico") ||(utf8_encode(trim($data[8]))!="Teléfono") ||(utf8_encode(trim($data[9]))!="Tipo de Comunicación") ||(utf8_encode(trim($data[10]))!="Modelo del Equipo")||(utf8_encode(trim($data[11]))!="Serial")||(utf8_encode(trim($data[12]))!="N° Terminal"))
								{
								
								echo "<script>alertify.alert('El orden del formato es incorrecto', function () {
								 window.location.assign('afiliadoterminal.php');});</script>";	
								$control=2;
								}
							}
							else if ($cont>1) 
							{ 
								
					if(!(in_array(($data[1]).($data[3]).($data[2]).($data[6]).($data[9]), $clienteactivos)))
					{	

						 echo '<div class="name"><div class="alert alert-warning form-group col-xs-12">'.'ERROR  "Registro no existe en nuestro sistema, con estas características. Debe validar si el registro posee N° Factura y Serial asignado". En la linea '.$cont.' verifique la información e intente nuevamente.<br> </b>
						 	<p>- Nº Registro: '.$data[1].'<br>
						 	- Razón Social: '.$data[3].'<br>
						 	- Rif: '.$data[2].'<br>
						 	- Afiliado: '.$data[6].'<br>
						 	- Serial del Equipo: '.$data[11].'<br></p></div></div>';
						 	
						 $control = 1;
					}


								if((strlen($data[6]) != null) && (!(preg_match("/^([0-9]{7})$/",$data[6]))))
								{
								echo '<div class="name"><div class="alert alert-warning form-group col-xs-12" >'.'<b>ERROR en campo N° de Afiliación en la linea'.$cont.', Debe poseer 7 Caracteres Numéricos..</b> 
        								</div></div>';
								$control = 1;	
								}

								if((strlen(trim($data[4])) == '')  && (($data[0])!="")
								|| (!(in_array(($data[4]), $opcionesprefijo))))
								{ 
								echo '<div class="name"><div class="alert alert-warning form-group col-xs-12" >'.'<b>ERROR en campo "Nombre del Banco" en la linea '.$cont.'</b> 
        								</div></div>';
								$control = 1;	
								$_SESSION["comprobar"]=$control;
								}

								/*if((in_array(($data[7]), $afiliadosactivos)))
								{
									echo '<h4 style="color:red;">'.'ERROR  "Número de Afiliado ya esta asignado a otro registro o el registro ya se encuentra en el sistema actualizado".<br> En la linea '.$cont.' verifique la información e intente nuevamente. </h4> <br>';

									$cafiliadodetalle =new controladorBanesco();
									$afdetalle=$cafiliadodetalle->verafiliadosbanescodetalle($data[7]);
									$afilia = pg_fetch_assoc($afdetalle);

									echo '<h4 style="color:red;">'.'Asignado al siguiente registro:</h4> 
										- Nombre del Banco Adquiriente: '.$afilia['ibp'].'<br>
										- Razón social / Nombre del afiliado: '.$afilia['razon'].'<br>
										- Rif / CI: '.$afilia['documento'].'<br><br>
										';
									$control = 1;
								}*/

								/*if(((strlen($data[7]) == '') || (strlen($data[7]) == null) && (strlen($data[22]) == null)) )
								{
								echo '<h4 style="color:red;">'.'ERROR. Verificar registro en la línea '.$cont.'</h4><br>';
								$_SESSION["comprobar"]=$control;
								$control = 1;	
								}

								if((strlen(trim($data[7])) == '') && (!(in_array(($data[22]), $estatusderechazo))))
								{
									//echo (!(in_array(($data[22]), $estatusderechazo)));
								echo '<h4 style="color:red;">'.'ERROR en campo Motivo de Rechazo, el motivo no coincide en la linea '.$cont.'</h4><br>';
								$control = 1;	
								$_SESSION["comprobar"]=$control;
								}*/

								// if(((strlen($data[7]) !== '') && (strlen($data[22]) !== null)) )
								// {
								// echo '<h4 style="color:red;">'.'ERROR. Si el cliente fue rechazado por el banco no debe poseer afiliado, verifique el registro en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// $_SESSION["comprobar"]=$control;
								// }

								if((strlen($data[11]) == ''))
								{
								echo '<div class="name"><div class="alert alert-warning form-group col-xs-12" '.'ERROR en campo Serial en la linea '.$cont.'</div></div>';
								$control = 1;	
								}

								// if(strlen($data[1]) == '')
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo Nº Registro en la linea'.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// if(strlen($data[2]) == '')
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo FECHA DE ENVÍO DEL ARCHIVO en la linea'.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// if((strlen($data[3]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo NOMBRE DEL BANCO ADQUIRIENTE en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// if((strlen($data[4]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo PREFIJO DEL BANCO en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// if((strlen($data[5]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo TIPO DE PUNTO DE VENTA en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// if((strlen($data[6]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo CANTIDAD DE PUNTO DE VENTA en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// if((strlen($data[7]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo N° DE AFILIACIÓN en la linea '.$cont.', No puede estar en blanco.</h4><br>';
								// $control = 1;	
								// }

								
								// if((strlen($data[10]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo RAZÓN SOCIAL en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// if((strlen($data[11]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo RIF/CI en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// }

								

								// if((strlen($data[11]) == ''))
								// {
								// echo '<h4 style="color:red;">'.'ERROR en campo RIF/CI en la linea '.$cont.'</h4><br>';
								// $control = 1;	
								// }

								// /*if(((strlen($data[7]) !== '') && (strlen($data[22]) !== null)) )
								// {
								// echo '<h4 style="color:red;">'.'ERROR. Si el cliente fue rechazado por el banco no debe poseer afiliado'.$cont.'</h4><br>';
								// $control = 1;	
								// }*/

								// if(((strlen($data[7]) == '') || (strlen($data[7]) == null) && (strlen($data[22]) == null)) )
								// {
								// echo '<h4 style="color:red;">'.'ERROR. Verificar registro en la línea '.$cont.'</h4><br>';
								// $control = 1;	
								// }
								
							}	
						}//fin while
						if($control==1)
						{
						?>
							<table border="0" width="300" align="center">
								<tr>
									<td height="100" colspan="2" align="center">
									<font face="impact" size="5" color="red">
									Posibles Causas de error:<br>
									</font>
									</td>
								</tr>
									<td height="100" colspan="2" align="center">
									<font face="arial" size="4" color="green">
									- Verifique archivo .CSV<br>
									- El archivo debe contener cabecera, en el orden indicado.<br>
									- Ningun campo debe venir en blanco, y debe estar delimitado las columnas por punto y coma (;) .<br>
									- Consulte con el administrador del Sistema.<br>
									 </font>
									</td>
								</tr>
								<tr>
									<td height="100" colspan="1" align="center">
									<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='afiliadoterminal.php'"/>
									</td>
								</tr>
							</table>
						<?php
						}
						?>				
					</div>
				</div>
			</div>
		</div>				
		<?php
		if($control == 0){
			$date = date("Y-n-j H:i:s");
			$name=$_FILES['archivo']['name'];
			$archivo = $_FILES['archivo']['tmp_name'];
			$fichero1 = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
			$count=0;
			$controlador1 =new controladorBanesco();
			while($data1 = fgetcsv($fichero1,10000,";"))
			{
				if($count>0)
				$insertar=$controlador1->insertar_terminal(trim($data1[0]),trim($data1[1]),trim($data1[2]),trim($data1[3]),trim($data1[4]),trim($data1[6]),trim($data1[11]),$_SESSION["session_user"]);
				$count++;
			}
			   //  echo "<script>alertify.alert('Archivo Cargado con &Eacute;xito', function () {
			   // window.location.assign('afiliadoterminal.php');});</script>";	
		}
	//}
}
?>

			