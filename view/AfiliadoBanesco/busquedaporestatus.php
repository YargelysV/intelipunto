<?php
  $usuario=$_SESSION["session_user"];
  $estatus=$_POST['estatus'];
    // $controladortrans =new controladorBanesco();
    // $trans=$controladortrans->usuario($usuario);
    // $row=pg_fetch_assoc($trans);
    $controlador =new controladorBanesco();
    $cantidad= $controlador->reportebanesco($estatus);
    $result=pg_fetch_assoc($cantidad);

    $count = pg_num_rows($cantidad);
?>


<?php 
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="hidden" id="estatus"  name="estatus"  value="<?php echo $estatus; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default"  style="background-color:#337AB7;">
        <div class="navbar-header">
          <a class="navbar-brand" href="#" style="color:#F8F9F9;">Data Clientes. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Estatus:<?php echo $result['desc_afiliado'] ?></a>
        </div>
      </nav>
      
    
    </div>
      <?php if ($estatus==1) { ?>

        <div class="col-12" id="tableapp">
        <table class="table table-bordered"  id="registrosdesaf" style="width: 100%;background-color:#F8F9F9;"> 
           <thead>
              <tr>
                <th>Percances</th>
                <th>Consecutivo</th>
                <th>ID</th>
                <th>Rif&nbsp;&nbsp;/&nbsp;&nbsp;C.I</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Serial&nbsp;&nbsp;del&nbsp;&nbsp;Equipo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>N°&nbsp;&nbsp;Cuenta&nbsp;&nbsp;Bancaria</th>
                <th>Afiliado</th> 
                <th>Correo&nbsp;&nbsp;Electrónico</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Tipo&nbsp;de&nbsp;Comunicación&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Modelo&nbsp;de&nbsp;Equipo&nbsp;&nbsp;</th>
                <th>N°&nbsp;&nbsp;de&nbsp;&nbsp;Terminal</th>                  
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table><?php } else { ?> 


    <div class="demo-container" ng-app="Appreportebanesco" ng-controller="Controllerreportebanesco">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
     </div>

      <br>
<?php  } ?>
      <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reportebanesco'">Volver</button>
        </div> 


    </div>
  </div>
</div>

<?php  } ?>


<script type="text/javascript">
  function dtCore(){

  var estatus= document.getElementById('estatus').value;
  var usuario= document.getElementById('usuario').value;
  //var today = new Date();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

        if(dd < 10)
          dd = '0' + dd;

        if(mm < 10)
          mm = '0' + mm;

        today = yyyy + '-' + mm + '-' + dd;


    $('table#registrosdesaf').DataTable({
      "ajax": {
      "url": "view/AfiliadoBanesco/banesco/selectEquiposDesafBanesco.php",
            "type": "POST",
            "data": {"estatus":estatus}
      },
      "columns": [
        {"data": "status", 
         "render" : function (data, type, full, meta, row) {
            var idestatus =full['inteliservices'];
            var serial =full['serialp'];
            var inicializado =full['statusinic'];

              if ((data==null) && (serial== null )) {

                 return '';
              } else if (serial!= null && inicializado==0) {

                 return '';
              }
              else if (data=='1' || data==='1' || data==='2'){

                 return '<input type="checkbox" class="mycheck" id="mycheck" checked disabled>';
              } else if (idestatus==9 || idestatus==22 || idestatus==23 || idestatus==28 || idestatus==30 || idestatus==45 || idestatus==46 || idestatus==52) {
                  
                  return 'Gestionado por Inteligensa';
             } else 
                  return '<input type="checkbox" class="mycheck" id="mycheck" >';
          }                               
        },
        {"data": "sec"},
        {"data": "consecutivo"},
        {"data": "coddocumento"},
        {"data": "razonsocial"},        
        {"data": "serialp"},
        {"data": "descbanco"},
        {"data": "ncuentabco"},
        {"data": "numafiliado"},
        {"data": "correo"},
        {"data": "tlf1"},
        {"data": "desctipo"},
        {"data": "descmodelo"},
        {"data": "terminal", className: "text-center" }
      
      ],
      "order" : [[1, "asc"]],

            
            
            "scrollX": true,
            "scrollY": false,
            "info":     false,
            "scrollCollapse": false,
            



            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });

    $('#registrosdesaf tbody').on( 'click', 'input.mycheck', function () {
      //alert('algo');
       var table=$('#registrosdesaf').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.consecutivo;
       var serial = T.serialp;
       var usuariocarga = $("#usuario").val();
       //var statusapp= document.getElementById('app').value;
       var checks=$(this).val();

       console.log(idconsec);
       console.log(usuariocarga);

      if (checks) {

        Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/AfiliadoBanesco/banesco/actualizarpercanc.php",
                        type : "POST",
                        data : {"idconsec":idconsec,"serial":serial, "usuariocarga":usuariocarga},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#registrosdesaf').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#registrosdesaf').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });

        
        } else {
      
        return false;
        }
      });

}
  </script>