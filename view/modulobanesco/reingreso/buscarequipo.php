<?php
  $usuario=$_SESSION["session_user"];
  $serial=$_POST['serial'];

  $controladorc =new ControladorBanesco();
  $resultados= $controladorc->sp_buscarequiposreingreso($serial);
  $datalote = pg_fetch_assoc($resultados);

  $ahora=date('Y-m-d');

  $count = pg_num_rows($resultados);

?>
<?php 
if ($count==0) {  ?>

<script type="text/javascript">
function dtCore(){}
redirectResultB();
</script>
<?php } else { ?>

<input type="hidden" id="serial"  name="serial"  value="<?php echo $serial; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard x3" style="color:#F8F9F9;"> </i> Reingreso Equipos: </a>
        </div>
       </nav>
    </div>
    <div class="row" align="center">
      <div class="col-12" id="tableapp">
        <table class="table table-bordered"  id="almacenbanesco" style="width: 100%;background-color:#F8F9F9;"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>Serial</th>
                <th>Estatus</th>
                <th>Carga de llaves</th>
                <th>Carga de app</th>
                <th>Carga del tem</th>
                
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table>
      </div>
      <div class="row">
        <div>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='reingreso_equipos.php'"/>
        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

   function dtCore(){

  var serial= document.getElementById('serial').value;
  var usuariocarga= document.getElementById('usuario').value;
  //var today = new Date();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

        if(dd < 10)
          dd = '0' + dd;

        if(mm < 10)
          mm = '0' + mm;

        today = yyyy + '-' + mm + '-' + dd;


    $('table#almacenbanesco').DataTable({

      "ajax": {
      "url": "view/modulobanesco/reingreso/selectEquiposReingreso.php",
            "type": "POST",
            "data": {"serial":serial}
      },
      "columns": [
        {"data": "id", className: "text-center"},
        {"data": "seriales", className: "text-center" },
        {"data": "status", className: "text-center"},
        {"data": "idestatus", 
          render : function ( data, type, full, meta ) {
            //console.log(full['fechallaves']);
            if (data ==null || data=="null"){

                return '<input type="checkbox" class="check-buttons" value="1">';
            } else if (full['fechagestion'] != today) {

                 return '<input type="checkbox" class="check-buttons" checked value="0" disabled>';
            } 
            else if (full['disponible'] == "1") {

                 return '<input type="checkbox" class="check-buttons" checked value="0" disabled>';
            }
            else if (data ==='4') {

                 return '<input type="checkbox" class="check-buttons" value="0" disabled>';
            }
            else if (data ==='2' || data ==='3') {

                 return '<input type="checkbox" class="check-buttons" checked value="0" disabled>';
            }
            else 
            return '<input type="checkbox" class="check-buttons" checked value="0" disabled>';
           // if ((data == 1 || data == "1" || data === "1" || data == 2 || data == "2" || data === "2")) {
           //    return '<input type="checkbox" class="check-buttons" id="llaves" checked value="1" >';
           //  }else 
           //    return '<input type="checkbox" class="check-buttons" id="llaves" value="0">';
                
        } 
      },
        {"data": "idestatus", 
          render : function ( data, type, full, meta ) {
           if (data == 1 || data == "1" || data === "1") {
              return '<input type="checkbox" class="check-buttonss" value="1">';

            }else if ((data == 2 || data == "2" || data === "2") && (full['fechagestion'] != today)) {

              return '<input type="checkbox" class="check-buttonss" checked value="0" disabled>';
            } else if ((data == 2 || data == "2" || data === "2") && (full['fechagestion'] == today)) {

                 return '<input type="checkbox" class="check-buttonss" checked value="2" > ';
             } else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] == today)) {

                    if (full['disponible'] == "1") { 

                        return '<input type="checkbox" class="check-buttonss" checked value="2" disabled> ';

                   } else 

                        return '<input type="checkbox" class="check-buttonss" checked disabled value="2"> ';
             }
              else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] != today)) {

                 return '<input type="checkbox" class="check-buttonss" checked value="2" disabled> ';
             }

             else 

                return '<input type="checkbox" class="check-buttonss" value="2" disabled> ';
        },
         
      },
      {"data": "idestatus", 
          render : function ( data, type, full, meta ) {
           if (data == 2 || data == "2" || data === "2") {
              return '<input type="checkbox" class="check-buttonsss" value="2">';

            }else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] != today)) {

              return '<input type="checkbox" class="check-buttonsss" checked value="0" disabled>';
            } else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] == today)) {

                    if (full['disponible'] == "1") { 

                        return '<input type="checkbox" class="check-buttonss" checked value="2" disabled> ';

                   } else 

                        return '<input type="checkbox" class="check-buttonsss" checked value="3"> ';
             } else 

                return '<input type="checkbox" class="check-buttonsss" value="3" disabled> ';
        },
         
      }
        
      ],
      "order" : [[0, "asc"]],

            
            
            "scrollX": true,
            "scrollY": false,
            "info":     false,
            "scrollCollapse": false,
            

            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },

       

            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }

        });


    $('#almacenbanesco tbody').on( 'click', 'input.check-buttons', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatus = T.idestatus;
       var usuariocarga = $("#usuario").val();
       var checks=$(this).val();
      if (checks) {
        
         Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarestatusserial.php",
                        type : "POST",
                        data : {"serial":serial,"usuariocarga":usuariocarga,"idestatus":idestatus},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });

        
        } else {
      
        return false;
        }
      });


            $('#almacenbanesco tbody').on( 'click', 'input.check-buttonss', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatusapp = T.idestatus;
       var usuariocarga = $("#usuario").val();
       //var statusapp= document.getElementById('app').value;
       var checks=$(this).val();

       //alert(statusapp);
      if (checks) {

        Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/reingreso/estatusreingresoapp.php",
                        type : "POST",
                        data : {"serial":serial,"usuariocarga":usuariocarga,"idestatusapp":idestatusapp},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                       table.ajax.reload(); 
                                    });              
                }
            });

        
        } else {
      
        return false;
        }
      });


    $('#almacenbanesco tbody').on( 'click', 'input.check-buttonsss', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatustem = T.idestatus;
       var usuariocarga = $("#usuario").val();
       //var statusapp= document.getElementById('app').value;
       var checks=$(this).val();

      if (checks) {

        Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/reingreso/estatusreingresotem.php",
                        type : "POST",
                        data : {"serial":serial,"usuariocarga":usuariocarga,"idestatustem":idestatustem},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });

        
        } else {
      
        return false;
        }
      });




}

  </script>


<?php } ?>