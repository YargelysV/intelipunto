<?php
$tituloPagina = $_SESSION["page"];
if (isset($_SESSION["session_user"])) {
  $usuario = $_SESSION["session_user"];
}
?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"><i style="color:#045FB4;" class="fa fa-group"></i> Reporte Inventario Banesco</h1>
      </div>

      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Buscar Reporte
            </div>

            <div class="panel-body">
              <div>
                <div class="col-lg-4" style="text-align:center">Estatus:</div>
                <div class="col-lg-6">
                  <select class="form-control" name="idestatus" id="idestatus" required>
                    <option value="">Seleccione Estatus</option>
                  </select>
                </div>
                <br>
              </div>
            </div>
            <input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"]; ?>' name="usuario" />
            <input type="hidden" id="valor" value="13" name="valor">


            <div class="panel-footer">
              <p style="text-align: center;">
                <input class="btn btn-success" type="button" id="Buscar" name="Buscar" value="Buscar" onClick="busquedareportebanesco();" />
                <input class="btn btn-primary" type="button" id="BuscarxLotesss" name="Buscar" value="Busqueda por Lote" onClick="busquedaporlotes();" />
              </p>
            </div>
          </div>
        </div>
      </div>


      <div class="row" id="panel_lote" style="display:none;">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Busqueda por Lote
            </div>
           <div class="panel-body">
            <div>
              <div class="col-lg-4" style="text-align:center">N° Lote:</div>
                <div class="col-lg-6">
                  <select class="form-control" name="idlote" id="idlote" required>
                  <option value="">Seleccione Lote</option>
                  </select>
              </div>
              <br>
            </div>
          </div>
          <div class="panel-footer">
                    <p style="text-align: center;">
                      <input class="btn btn-primary" type="button" id="BuscarxLotesss" name="Buscar" value="Buscar" onClick="busquedalotes();"/>
                 </p>
          </div>
          </div>
        </div>
      </div>
      </div> 
              </p>
            </div>
          </div>
        </div>
      </div>


      <script languaje="javascript">
        buscarreporteinventario();
      </script>

    </div>
  </div>
</div>
</div>
</div>
</div>