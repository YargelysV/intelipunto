<?php
  $usuario=$_SESSION["session_user"];
  $idlote=($_REQUEST['var']);

  $controladorc =new ControladorBanesco();
  $resultados= $controladorc->buscarlotes($idlote);
  $datalote = pg_fetch_assoc($resultados);

  $ahora=date('Y-m-d');

  $fecha_gestion=$datalote['fechagestion'];
  $count = pg_num_rows($resultados);

?>

<input type="hidden" id="idlote"  name="idlote"  value="<?php echo $idlote; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="estatusllaves" name="estatusllaves" value="<?php echo  $datalote['status']; ?>">
<input type="hidden" id="idstatus" name="idstatus" value="<?php echo  $datalote['idestatus']; ?>">

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard x3" style="color:#F8F9F9;"> </i> Seriales Inventario Banesco: </a>
        </div>
       </nav>
    </div>
    <div class="row" align="center">
      <div class="col-12" id="tableapp">
        <table class="table table-bordered"  id="almacenbanesco" style="width: 100%;background-color:#F8F9F9;"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>Fecha Carga</th>
                <th>Marca POS</th>
                <th>Serial</th>
                <th>Estatus</th>
                <th>&nbsp;Carga&nbsp;de&nbsp;Llaves
                <?php if ($datalote['idestatus']== "0" || $datalote['idestatus']== "null" || $datalote['idestatus']== null) { ?>
                  <input type="checkbox" name="statusllaves" id="statusllaves" onClick="actualizarxLoteLlaves(this)" value="1" > 
                   <?php } else if ($fecha_gestion!=$ahora) { ?>
                    <input type="checkbox" name="statusllaves" id="statusllaves" onClick="actualizarxLoteLlaves(this)" checked value="0" disabled>
                   <?php }

                   else { ?>
                    <input type="checkbox" name="statusllaves" id="statusllaves" checked value="0" onClick="actualizarxLoteLlaves(this)">
                  <?php }?>
                </th>
                <th>&nbsp;Carga&nbsp;de&nbsp;APP
                 <?php if ($datalote['idestatus']=== "1") { ?>
                  <input type="checkbox" name="statusapp" id="statusapp" onClick="actualizarxLoteApp(this)" value="2" >  <?php } elseif (($datalote['idestatus']=== "2") && ($fecha_gestion!=$ahora)) {?>
                    <input type="checkbox" name="statusapp" id="statusapp" checked value="2" disabled>
                  <?php } else if (($datalote['idestatus']=== "2") && ($fecha_gestion==$ahora)) { ?> 
                    <input type="checkbox" name="statusapp" id="statusapp" checked value="1" onClick="actualizarxLoteApp(this)">
                  <?php }?>
                  </th> 
                  <th>&nbsp;Carga&nbsp;del&nbsp;TEM 
                <?php if ($datalote['idestatus']=== "2") { ?>
                  <input type="checkbox" name="statustem" id="statustem" onClick="actualizarxLoteTEM(this)" value="3" >  <?php } elseif (($datalote['idestatus']=== "3") && ($fecha_gestion!=$ahora)) {?>
                    <input type="checkbox" name="statustem" id="statustem" checked value="2" disabled>
                  <?php } else if (($datalote['idestatus']=== "3") && ($fecha_gestion==$ahora)) { ?> 
                    <input type="checkbox" name="statustem" id="statustem" checked value="2" onClick="actualizarxLoteTEM(this)">
                  <?php }?>
                </th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;del&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table>
      </div>
      <div class="row">
        <div>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='inventario_banesco.php'"/>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  async function dtCore(){



  var idlote= document.getElementById('idlote').value;
  var usuariocarga= document.getElementById('usuario').value;
  //var today = new Date();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

        if(dd < 10)
          dd = '0' + dd;

        if(mm < 10)
          mm = '0' + mm;

        today = yyyy + '-' + mm + '-' + dd;


    $('table#almacenbanesco').DataTable({

      "ajax": {
      "url": "view/modulobanesco/inventario/selectAppllaves.php",
            "type": "POST",
            "data": {"idlote":idlote}
      },
     "columns": [
        {"data": "id", className: "text-center"},
        {"data": "fechacarg", className: "text-center"},
        {"data": "marcapo", className: "text-center"},
        {"data": "seriales", className: "text-center" },
        {"data": "status", className: "text-center" },
        {"data": "idestatus", 
          render : function ( data, type, full, meta ) {
            //console.log(full['fechallaves']);
            if (data ==null || data=="null"){

                return '<input type="checkbox" class="check-buttons" value="1">';
            } else if (full['fechagestion'] != today) {

                 return '<input type="checkbox" class="check-buttons" checked value="0" disabled>';
            }
            else 
            return '<input type="checkbox" class="check-buttons" checked value="0">';
           // if ((data == 1 || data == "1" || data === "1" || data == 2 || data == "2" || data === "2")) {
           //    return '<input type="checkbox" class="check-buttons" id="llaves" checked value="1" >';
           //  }else 
           //    return '<input type="checkbox" class="check-buttons" id="llaves" value="0">';
                
        } 
      },
        {"data": "idestatus", 
          render : function ( data, type, full, meta ) {
           if (data == 1 || data == "1" || data === "1") {
              return '<input type="checkbox" class="check-buttonss" value="1">';

            }else if ((data == 2 || data == "2" || data === "2") && (full['fechagestion'] != today)) {

              return '<input type="checkbox" class="check-buttonss" checked value="0" disabled>';
            } else if ((data == 2 || data == "2" || data === "2") && (full['fechagestion'] == today)) {

                 return '<input type="checkbox" class="check-buttonss" checked value="2" > ';
             } else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] == today)) {

                 return '<input type="checkbox" class="check-buttonss" checked value="2" > ';
             }
              else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] != today)) {

                 return '<input type="checkbox" class="check-buttonss" checked value="2" disabled> ';
             }

             else 

                return '<input type="checkbox" class="check-buttonss" value="2" disabled> ';
        },
         
      },
      {"data": "idestatus", 
          render : function ( data, type, full, meta ) {
           if (data == 2 || data == "2" || data === "2") {
              return '<input type="checkbox" class="check-buttonsss" value="2">';

            }else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] != today)) {

              return '<input type="checkbox" class="check-buttonsss" checked value="0" disabled>';
            } else if ((data == 3 || data == "3" || data === "3") && (full['fechagestion'] == today)) {

                 return '<input type="checkbox" class="check-buttonsss" checked value="3" > ';
             } else 

                return '<input type="checkbox" class="check-buttonsss" value="3" disabled> ';
        },
         
      },
      {"data": "idestatus", 
          render : function ( data, type, full, meta ) {

              var seriales =full['seriales'];
              var id =full['iconfig'];
              var fecha =full['fechagestion'];
              var currentCell = $("table#almacenbanesco").DataTable().cells({"row":meta.row, "column":meta.col}).nodes(0);
              var contador=1;

                
            //console.log(full.idestatusconfig);
           if (data == 3 || data == "3" || data === "3") {
              return '<select id="aggregate" class="form-control" style="width:195px;" disabled><option value="" selected="select">Seleccione</option></select>'

            }else if ((data == 4 || data == "4" || data === "4") && (full['fechagestion'] != today)) {
                    
                $.ajax({
                url : "view/modulobanesco/inventario/selectEstatus.php",
                type:'POST',
                dataType: 'html',
                data: {"id":id, "fecha":fecha }
                }).done(function (data) {                                                                              
                     $(currentCell).html(data);
                     
                }); 
                     return ''

            } else if ((data == 4 || data == "4" || data === "4") && (full['fechagestion'] == today)) {

                $.ajax({
                url : "view/modulobanesco/inventario/selectEstatus.php",
                type:'POST',
                dataType: 'html',
                data: {"id":id, "fecha":fecha }
                }).done(function (data) {                                                                              
                     $(currentCell).html(data);
                     
                }); 
                     return ''
                        
             } else 

                return '<select id="agg'+full['seriales']+'" name="agg" class="form-control" style="width:195px;"><option value="0" selected="select">Seleccione</option><option value="1">Error en Carga</option><option value="2">Equipo no operativo</option></select>'

        },
         
      },
      {"data": "iconfig", className: "text-center",
         "render" : function (data, type, full, meta) {

            if (full['idestatus']==='4' && full['fechagestion'] == today) {

                return  '<input type="text" id="iobservacion'+full['seriales']+'" value="'+full['observacion']+'"" class="form-control status" disabled>  <input type="checkbox" class="check-buttonsd">Habilitar' 

            } else if (full['idestatus']==='4' && full['fechagestion'] != today) {

                return  '<input type="text" id="iobservacion'+full['seriales']+'" value="'+full['observacion']+'"" class="col-lg-2 form-control status" disabled>' 

            } 

            else 

                return  '<input type="text" id="iobservacion'+full['seriales']+'" class="col-lg-2 form-control status" style="display:none"><button class="Guardar btn btn-primary" id="iguardar'+full['seriales']+'" style="display:none" align="center"><i class="fa fa-save"></i></button>' 
        }

       }
      ],
      "order" : [[0, "asc"]],

            
            
            "scrollX": true,
            "scrollY": false,
            "info":     false,
            "scrollCollapse": false,
            

            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },

        "columnDefs": [
            
            { width: '20%', targets: 9 }
        ],

            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }

        });


        $('#almacenbanesco tbody').on( 'click', 'input.check-buttons', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatus = T.idestatus;
       var usuariocarga = $("#usuario").val();
       var checks=$(this).val();
      if (checks) {
        
         Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarestatusserial.php",
                        type : "POST",
                        data : {"serial":serial,"usuariocarga":usuariocarga,"idestatus":idestatus},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });

        // var ruta="view/modulobanesco/inventario/actualizarestatusserial.php?r=1&serial="+serial+"&usuariocarga="+usuariocarga+"&statusllaves="+statusllaves;
        // $.ajax({
        //     url : ruta,
        //     type : "POST",
           
        //     success: function(data){

        //         if(data == 1  || data == '1'){
        //             var table=$('#almacenbanesco').DataTable();
        //             table.ajax.reload();    

        //         }else{                   
        //             alertify.error('Error el estatus no fue actualizado');
        //         }
        //     }
        // })
        
        } else {
      
        return false;
        }
      });

        $('#almacenbanesco tbody').on( 'click', 'input.check-buttonss', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatusapp = T.idestatus;
       var usuariocarga = $("#usuario").val();
       //var statusapp= document.getElementById('app').value;
       var checks=$(this).val();

       //alert(statusapp);
      if (checks) {

        Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarestatusapp.php",
                        type : "POST",
                        data : {"serial":serial,"usuariocarga":usuariocarga,"idestatusapp":idestatusapp},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });
        
        // var ruta="view/modulobanesco/inventario/actualizarestatusapp.php?r=1&serial="+serial+"&usuariocarga="+usuariocarga+"&statusapp="+statusapp;
        // $.ajax({
        //     url : ruta,
        //     type : "POST",
           
        //     success: function(data){

        //         if(data == 1  || data == '1'){
        //             var table=$('#almacenbanesco').DataTable();
        //             table.ajax.reload();    

        //         }else{                   
        //             alertify.error('Error el módulo no fue asignado');
        //         }
        //     }
        // })
        
        } else {
      
        return false;
        }
      });


    $('#almacenbanesco tbody').on( 'click', 'input.check-buttonsss', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatustem = T.idestatus;
       var usuariocarga = $("#usuario").val();
       //var statusapp= document.getElementById('app').value;
       var checks=$(this).val();

      if (checks) {

        Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarestatustem.php",
                        type : "POST",
                        data : {"serial":serial,"usuariocarga":usuariocarga,"idestatustem":idestatustem},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });

        
        } else {
      
        return false;
        }
      });

        $('#almacenbanesco tbody').on( 'change', 'select.form-control', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatus = T.idestatus;
       var usuariocarga = $("#usuario").val();
       var select = $("#agg"+serial).val();
       var checks=$(this).val();
       var observacion= $("#iobservacion"+serial).css("display", "block");
       var button= $("#iguardar"+serial).css("display", "block");

       if (select==0) {
        var observacion= $("#iobservacion"+serial).css("display", "none");
        var button= $("#iguardar"+serial).css("display", "none");
       }
       
      });


       $('#almacenbanesco tbody').on( 'click', 'button.Guardar', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var usuariocarga = $("#usuario").val();
       var select = $("#agg"+serial).val();
       var iobserv = $("#iobservacion"+serial).val();
       var observacion= $("#iobservacion"+serial).css("display", "block");
       var button= $("#iguardar"+serial).css("display", "block");

       

       Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    console.log(result.value);
                    $.ajax({
                        url : "view/modulobanesco/inventario/actualizarestatusconfig.php",
                        type : "POST",
                        data : {"serial":serial, "select":select, "iobserv":iobserv, "usuariocarga":usuariocarga},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido guardado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelada";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });



      });   

    $('#almacenbanesco tbody').on( 'click', 'input.check-buttonsd', function () {
      //alert('algo');
       var table=$('#almacenbanesco').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.id;
       var serial = T.seriales;
       var status = T.status;
       var idestatus = T.idestatus;
       var usuariocarga = $("#usuario").val();
       var select = $("#agg"+serial).val();
       var checks=$(this).val();

        if(checks){

            Swal.fire({
            title: 'Desea realizar el cambio de estatus?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/modulobanesco/inventario/updateconfigestatus.php",
                        type : "POST",
                        data : {"serial":serial, "usuariocarga":usuariocarga},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido guardado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#almacenbanesco').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelada";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#almacenbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });


        } else {
      
        return false;
        }
       
      });

    

}

//  function asyncCall(result) {
  
//   var i = $(this).val();
//   //var i = $("input#idstatusaaa").val();
//     //var confcomisiones=$('input#confcomisiones').val();
//     //alert();
//     console.log(i);
//     $.post('view/modulobanesco/inventario/selectEstatus.php', {postvariable:i}).done( function(respuesta)
//     {
//         $('#aggregate').html( respuesta );
//     });
//   // Expected output: "resolved"
// }
  </script>


