<?php
  $usuario=$_SESSION["session_user"];
  $facturacion=$_SESSION["administracion"];
  $var=($_REQUEST['var']);
  $_SESSION['bsqrecepcion']=$var;
  $array = explode("/",$var);
  $banco= $array[0];
  $fechadetalle=$array[1];
  $tipousuario= isset($array[2]) ? $array[2] :"";
  $ejecutivo= isset($array[3]) ? $array[3] :"";
  $usuario= isset($array[4]) ? $array[4] :"";
  $coordinador= isset($array[5]) ? $array[5] :"";
  $coordinadordos= isset($array[6]) ? $array[6] :"";
  $masiva = isset($array[7]) ? $array[7] : "";
  $namearchivo = isset($array[8]) ? $array[8] : "";

  // $cliente = isset($array[4]) ? $array[4] : "";
  //$fecha = isset($array[5]) ? $array[5] : "";

  $retroceder1="".'/'.$fechadetalle.'/'.$tipousuario.'/'.$ejecutivo.'/'.$usuario.'/'.$coordinador.'/'.$coordinadordos;
  $retroceder2=$banco.'/'."".'/'.$tipousuario.'/'.$ejecutivo.'/'.$usuario.'/'.$coordinador.'/'.$coordinadordos;

  $_SESSION["fecha"]=$fechadetalle;

  $controladorc =new controladorAdjuntar();
  $comerciali=$controladorc->sp_verclientefacturacionmifi($banco,$fechadetalle,$namearchivo,$masiva,$tipousuario,$ejecutivo,$usuario,$coordinador,$coordinadordos);
  $databanco = pg_fetch_assoc($comerciali);
?>
<style type="text/css">
  .Pago-Confirmado{
    
    color:white;
    background:#01DF3A;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

  .Pendiente-Por-Pagos{
     
    color:#f1f1f1;
    background:orange;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

  .Pendiente-Por-Confirmar{
    
    color:#f1f1f1;
    background:red;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 

  }

  .Inteliservices{
    
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

</style>
<input type="hidden" id="fechadetalle" name="fechadetalle" value="<?php echo $fechadetalle; ?>">
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>">
<input type="hidden" id="namearchivo" name="namearchivo" value="<?php echo $namearchivo; ?>">
<input type="hidden" id="databanco" name="databanco" value="<?php echo  $databanco['ibp']; ?>">
<input type="hidden" id="masiva" name="masiva" value="<?php echo  $masiva; ?>">
<input type="hidden" id="permisos" name="permisos" value="<?php echo  $facturacion; ?>">
<input type="hidden" id="tipousuario"  name="tipousuario"  value="<?php echo $tipousuario; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="coordinador"  name="coordinador"  value="<?php echo $coordinador; ?>" />
<input type="hidden" id="coordinadordos"  name="coordinadordos"  value="<?php echo $coordinadordos; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#2E3F50;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-clipboard" style="color:#F8F9F9;"> </i> Facturación Mifi: Búsqueda Detallada  </a>
        </div>
       </nav>
        <b style="color:#5DADE2;">Búsqueda: <?php echo $databanco['ibp'];?></b>
       <br>
    </div>
    <div class="row">
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="busquedafactmifi" style="width: 100%"> 
           <thead>
              <tr>
                <th>correlativo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;Pago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>enlace</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;del&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Ejecutivo Asignado</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>N° Afiliado</th>
                <th>Tipo&nbsp;de&nbsp;Mifi</th>
                <th>Cantidad de Mifi</th>
                <th>Fecha de Recepción</th>
                <th>Fecha de Carga</th>
                <th>Nombre Archivo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>ibp</th>
                <th>codbanco</th>
              </tr>
            </thead>
          <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>            
          </tfoot>
        </table>
      </div>
    </div>


         <div class="row" align="center">
                <div class="col-md-4 col-md-offset-4">
                     <?php if ($masiva=="2") {?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarcliente&var=<?php echo $retroceder2;?>'"/>
                    <?php }else if ($masiva=="1") {?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarcliente&var=<?php echo $retroceder1;?>'"/>
                     <?php } else { ?>
                      <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='facturacionmifi.php'"/>
                   <?php } ?>
                  </div>
            </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
  banco= document.getElementById('banco').value;
  fechadetalle= document.getElementById('fechadetalle').value;
  namearchivo= document.getElementById('namearchivo').value;
  masiva= document.getElementById('masiva').value;
  tipousuario= document.getElementById('tipousuario').value;
  ejecutivo= document.getElementById('ejecutivo').value;
  usuario= document.getElementById('usuario').value;
  coordinador= document.getElementById('coordinador').value;
  coordinadordos= document.getElementById('coordinadordos').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedafactmifi').DataTable({
    "ajax": {
    "url": "api/SelectFacturacionDetalladaMifi.php",
          "type": "POST",
          "data": {"banco":banco,"fechadetalle":fechadetalle,"namearchivo":namearchivo, 
          "masiva":masiva,"tipousuario":tipousuario,"ejecutivo":ejecutivo, "usuario":usuario,"coordinador":coordinador,"coordinadordos":coordinadordos}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "estatuspago", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data == "1" || data === "1") {
        return "<p class='Pago-Confirmado'>Pago Confirmado</p>";
      }else if (data == "2" || data === "2"){
        return "<p class='Pendiente-Por-Pagos'>Pendiente Por Pagos</p>";
      }else if (data == "0" || data === "0"){
        return "<p class='Pendiente-Por-Confirmar'>Pendiente Por Confirmar</p>";
      }else{
        return "";
      }
      }
  },
      {"data": "enlaceafiliado" },
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Datos Facturación</a>", className: "text-center"},
      {"data": "idestatusequipo", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data == "1" || data === "1") {
        return "<p class='Inteliservices'>Pendiente por Instalar</p>";
      }else if (data == "2" || data === "2"){
        return "<p class='Verificar-Serial'>Revisión de Equipo</p>";
      }else if (data == "3" || data === "3"){
        return "<p class='Inteliservices'>Mifi Instalado</p>";
      }else if (data == "4" || data === "4"){
        return "<p class='Inteliservices'>Declinación de Compra</p>";
      }else if (data == "5" || data === "5"){
        return "<p class='Inteliservices'>Gestionado por Inteligensa</p>";
      }else if (data == "0" || data === "0"){
        return "<p class='Inteliservices'>En proceso de Configuración</p>";
      }else if (data == null || data === null){
        return "<p class='Inteliservices'>Por asignar serial mifi</p>";
      }else{
        return "";
      }
      }
  },
      {"data": "ejecutivo", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "nafiliacion", className: "text-center" },
      {"data": "tipopos"},
      {"data": "cantidadterminales"},
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "idate", className: "text-center" },
      {"data": "namearchivo", className: "text-center" },
      {"data": "direccion"},
      {"data": "ibp"},
      {"data": "codigobanco"},
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 2,15,16 ],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        'excel'
    ],

    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },

    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });

  $('table#busquedafactmifi tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#busquedafactmifi').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlaceafiliado=D.enlaceafiliado;
        var url = "facturacionmifi?cargar=facturarclientemifi&var="+enlaceafiliado; 
        $(location).attr('href',url);

  });
}
  </script>
