<?php
/*buscarrif.php view*/
  $facturacion=$_SESSION["administracion"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $rif= $array[0];
  $tipousuario= $array[1];
  $ejecutivo= $array[2];
  $login= $array[3];
  $coordinador= isset($array[4]) ? $array[4] :"";
  $coordinadordos= isset($array[5]) ? $array[5] :"";

  $controladorclient =new ControladorAdjuntar();
  $comercia= $controladorclient->spbuscarclientefacturacionmifixrif($rif, $tipousuario, $ejecutivo, $login,$coordinador, $coordinadordos);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>
<style type="text/css">
  .Pago-Confirmado{
    
    color:white;
    background:#01DF3A;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

  .Pendiente-Por-Pagos{
     
    color:#f1f1f1;
    background:orange;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

  .Pendiente-Por-Confirmar{
    
    color:#f1f1f1;
    background:red;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 

  }

  .Inteliservices{
    
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

</style>
<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $facturacion; ?>" />
<input type="hidden" id="tipousuario"  name="tipousuario"  value="<?php echo $tipousuario; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $login; ?>" />
<input type="hidden" id="coordinador"  name="coordinador"  value="<?php echo $coordinador; ?>" />
<input type="hidden" id="coordinadordos"  name="coordinadordos"  value="<?php echo $coordinadordos; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard" style="color:#F8F9F9;"> </i> Busqueda por Rif: Facturación POS </a>
        </div>
       </nav>
    </div>
    <div class="row">
          <div class="col-12">
            <table class="table table-bordered"  id="busquedariffactposmifi" style="width: 100%"> 
               <thead>
                  <tr>
                    <th>correlativo</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;Pago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>enlace</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;del&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Ejecutivo&nbsp;Asignado</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>N°&nbsp;Afiliado</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>codbanco</th>
                    <th>Tipo&nbsp;de&nbsp;Pos</th>
                    <th>Cantidad de Pos</th>
                    <th>Fecha de Recepción</th>
                    <th>Fecha de Carga</th>
                    <th>Nombre Archivo</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;</th>
                  </tr>
                </thead>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5 col-md-offset-5">
               <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='facturacionmifi.php'"/>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php  } ?>


<script type="text/javascript">
  function dtCore(){
    nrorif= document.getElementById('nrorif').value;
    tipousuario= document.getElementById('tipousuario').value;
    ejecutivo= document.getElementById('ejecutivo').value;
    usuario= document.getElementById('usuario').value;
    coordinador= document.getElementById('coordinador').value;
    coordinadordos= document.getElementById('coordinadordos').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedariffactposmifi').DataTable({
    "ajax": {
    "url": "api/selectFacturacionMifixRif.php",
          "type": "POST",
          "data": {"nrorif":nrorif,"tipousuario":tipousuario,"ejecutivo":ejecutivo, "usuario":usuario,"coordinador":coordinador, "coordinadordos":coordinadordos}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "estatuspago", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data == "1" || data === "1") {
        return "<p class='Pago-Confirmado'>Pago Confirmado</p>";
      }else if (data == "2" || data === "2"){
        return "<p class='Pendiente-Por-Pagos'>Pendiente Por Pagos</p>";
      }else if (data == "0" || data === "0"){
        return "<p class='Pendiente-Por-Confirmar'>Pendiente Por Confirmar</p>";
      }else{
        return "";
      }
      }
  },
      {"data": "enlaceafiliado" },
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Datos Facturación</a>", className: "text-center"},
      {"data": "idestatusequipo", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data == "1" || data === "1") {
        return "<p class='Inteliservices'>Pendiente por Instalar</p>";
      }else if (data == "2" || data === "2"){
        return "<p class='Verificar-Serial'>Revisión de Equipo</p>";
      }else if (data == "3" || data === "3"){
        return "<p class='Inteliservices'>Mifi Instalado</p>";
      }else if (data == "4" || data === "4"){
        return "<p class='Inteliservices'>Declinación de Compra</p>";
      }else if (data == "5" || data === "5"){
        return "<p class='Inteliservices'>Gestionado por Inteligensa</p>";
      }else if (data == "0" || data === "0"){
        return "<p class='Inteliservices'>En proceso de Configuración</p>";
      }else if (data == null || data === null){
        return "<p class='Inteliservices'>Por asignar serial mifi</p>";
      }else{
        return "";
      }
      }
  },
      {"data": "ejecutivo", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "nafiliacion", className: "text-center" },
      {"data": "ibp", className: "text-center" },
      {"data": "codigobanco", className: "text-center" },
      {"data": "tipopos"},
      {"data": "cantidadterminales"},
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "idate", className: "text-center" },
      {"data": "namearchivo", className: "text-center" },
      {"data": "direccion"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 2,10 ],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });

  $('table#busquedariffactposmifi tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#busquedariffactposmifi').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlaceafiliado=D.enlaceafiliado;
        var url = "facturacionmifi?cargar=facturarclientemifi&var="+enlaceafiliado; 
        $(location).attr('href',url);

  });
}
  </script>