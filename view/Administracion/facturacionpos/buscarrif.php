<?php
/*buscarrif.php view*/
  $facturacion=$_SESSION["administracion"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $rif= $array[0];
  $tipousuario= $array[1];
  $ejecutivo= $array[2];
  $login= $array[3];
  $coordinador= isset($array[4]) ? $array[4] :"";
  $coordinadordos= isset($array[5]) ? $array[5] :"";

  $controladorclient =new ControladorAdjuntar();
  $comercia= $controladorclient->sp_buscarclientefacturacionRif($rif, $tipousuario, $ejecutivo, $login,$coordinador, $coordinadordos);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>
<style type="text/css">
  .Pago-Confirmado{
    
    color:white;
    background:#01DF3A;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

  .Pendiente-Por-Pagos{
     
    color:#f1f1f1;
    background:orange;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

  .Pendiente-Por-Confirmar{
    
    color:#f1f1f1;
    background:red;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 

  }

  .Inteliservices{
    
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

    .Cliente-Inteligensa{
    
    color:#f1f1f1;
    background:#62ABCD;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }
    .Migracion{
    
    color:#f1f1f1;
    background:#C94E71;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

    .Comodato{
    
    color:#f1f1f1;
    background:#BA55D3;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

</style>
<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $facturacion; ?>" />
<input type="hidden" id="tipousuario"  name="tipousuario"  value="<?php echo $tipousuario; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $login; ?>" />
<input type="hidden" id="coordinador"  name="coordinador"  value="<?php echo $coordinador; ?>" />
<input type="hidden" id="coordinadordos"  name="coordinadordos"  value="<?php echo $coordinadordos; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard" style="color:#F8F9F9;"> </i> Busqueda por Rif: Facturación POS </a>
        </div>
       </nav>
    </div>
    <div class="row">
          <div class="col-12">
            <table class="table table-bordered"  id="busquedariffactpos" style="width: 100%"> 
               <thead>
                  <tr>
                    <th>correlativo</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;Pago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>enlace</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;del&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Ejecutivo&nbsp;Asignado</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;Estatus&nbsp;Cliente&nbsp;</th>
                    <th>N°&nbsp;Afiliado</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>codbanco</th>
                    <th>Tipo&nbsp;de&nbsp;Pos</th>
                    <th>Cantidad de Pos</th>
                    <th>Fecha de Recepción</th>
                    <th>Fecha de Carga</th>
                    <th>Nombre Archivo</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;</th>
                  </tr>
                </thead>
            </table>
          </div>
        </div>
      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='facturacionpos.php'"/>
        </div>
      </div>

    </div>
  </div>
</div>
<?php  } ?>


<script type="text/javascript">
  function dtCore(){
    nrorif= document.getElementById('nrorif').value;
    tipousuario= document.getElementById('tipousuario').value;
    ejecutivo= document.getElementById('ejecutivo').value;
    usuario= document.getElementById('usuario').value;
    coordinador= document.getElementById('coordinador').value;
    coordinadordos= document.getElementById('coordinadordos').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedariffactpos').DataTable({
    "ajax": {
    "url": "api/SelectFacturacionRif.php",
          "type": "POST",
          "data": {"nrorif":nrorif,"tipousuario":tipousuario,"ejecutivo":ejecutivo, "usuario":usuario,"coordinador":coordinador, "coordinadordos":coordinadordos}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "estatuspago", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data == "1" || data === "1") {
        return "<p class='Pago-Confirmado'>Pago Confirmado</p>";
      }else if (data == "2" || data === "2"){
        return "<p class='Pendiente-Por-Pagos'>Pendiente Por Pagos</p>";
      }else if (data == "0" || data === "0"){
        return "<p class='Pendiente-Por-Confirmar'>Pendiente Por Confirmar</p>";
      }else{
        return "";
      }
      }
  },
      {"data": "enlaceafiliado" },
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Datos Facturación</a>", className: "text-center"},
      {"data": "idestatusequipo", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if       (data == "1" || data === "1") {
        return "<p class='Inteliservices'>Inteliservices</p>";

      }else if (data == "2" || data === "2"){
        return "<p class='Verificar-Serial'>Verificar Serial</p>";

      }else if (data == "3" || data === "0"){
        return "<p class='Inteliservices'>Inteliservices</p>";

      }else if (data == "4" || data === "4"){
        return "<p class='Inteliservices'>Inteliservices</p>";

      }else if (data == "5" || data === "5"){
        return "<p class='Inteliservices'>Inteliservices</p>";

      }else if (data == "6" || data === "6"){
        return "<p class='Inteliservices'>Inteliservices</p>";

      }else if (data == "8" || data === "8"){
        return "<p class='Inteliservices'>Entregado al Rosal</p>";

      }else if (data == "9" || data === "9"){
        return "<p class='Inteliservices'>Declinación de Compra</p>";

      }else if (data == "10" || data === "10"){
        return "<p class='Inteliservices'>POS Instalado (Inteligensa)</p>";

      }else if (data == "12" || data === "12"){
        return "<p class='Inteliservices'>POS Instalado Maracaibo</p>";

      }else if (data == "13" || data === "13"){
        return "<p class='Inteliservices'>Configuración Mifi</p>";

      }else if (data == "14" || data === "14"){
        return "<p class='Inteliservices'>Mifi Instalado</p>";

      }else if (data == "0" || data === "0"){
        return "<p class='Inteliservices'>Sin Parámetros</p>";

      }else if (data == null || data === null){
        return "<p class='Inteliservices'>Por Asignar Serial</p>";

      }else if (data == "16" || data === "16"){
        return "<p class='Inteliservices'>Revisar Mifi</p>";

      }else if (data == "20" || data === "20"){
        return "<p class='Inteliservices'>Gestión del Rosal</p>";

      }else if (data == "21" || data === "21"){
        return "<p class='Inteliservices'>Gestión Maracaibo</p>";

      }else if (data == "22" || data === "22"){
        return "<p class='Inteliservices'>Equipo Robado</p>";

      }else if (data == "23" || data === "23"){
        return "<p class='Inteliservices'>Equipo Vulnerado</p>";

      }
      else if (data == "24" || data === "24"){
        return "<p class='Inteliservices'>Gestión Anaco</p>";
      }
      else if (data == "25" || data === "25"){
        return "<p class='Inteliservices'>Gestión Táchira</p>";
      }
      else if (data == "26" || data === "26"){
        return "<p class='Inteliservices'>POS Instalado Anaco</p>";
      }
      else if (data == "23" || data === "23"){
        return "<p class='Inteliservices'>POS Instalado Táchira</p>";
      }
      else{
        return "";
      }
      }
  },
      {"data": "ejecutivo", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "estatusmigra", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if       (data == "0" || data === "0" || data==null || data===null) {
        return "<p class='Cliente-Inteligensa'>Cliente Inteligensa</p>";

      }else if (data == "1" || data === "1"){
        return "<p class='Migracion'>Migración</p>";

      }else if (data == "2" || data === "2"){
        return "<p class='Comodato'>Comodato</p>";

      }else{
        return "";
      }} },
      {"data": "nafiliacion", className: "text-center" },
      {"data": "ibp", className: "text-center" },
      {"data": "codigobanco", className: "text-center" },
      {"data": "tipopos"},
      {"data": "cantidadterminales"},
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "idate", className: "text-center" },
      {"data": "namearchivo", className: "text-center"},
      {"data": "direccion"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 2,10 ],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',
            
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });

  $('table#busquedariffactpos tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#busquedariffactpos').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlaceafiliado=D.enlaceafiliado;
        var estatusmigra=D.estatusmigra;
        var url = "facturacionpos?cargar=facturarcliente&var="+enlaceafiliado+'/'+estatusmigra; 
        $(location).attr('href',url);

  });
}
  </script>