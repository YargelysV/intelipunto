<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] :"";
  $fecha= isset($array[1]) ? $array[1] :"";
  $tipousuario= isset($array[2]) ? $array[2] :"";
  $ejecutivo= isset($array[3]) ? $array[3] :"";
  $usuario= isset($array[4]) ? $array[4] :"";
  $coordinador= isset($array[5]) ? $array[5] :"";
  $coordinadordos= isset($array[6]) ? $array[6] :"";


   if ($banco=="")
  {
    $origen='1';
  }
  if ($fecha=="")
  {
    $origen='2';
  }

  $controladorc =new controladorAdjuntar();
  $comerciali= $controladorc->sp_buscarclientefacturacion($banco,$fecha,$origen,$tipousuario,$ejecutivo,$usuario,$coordinador,$coordinadordos);
  $databanco = pg_fetch_assoc($comerciali);

?>

<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $databanco['ibp']; ?>" />
<input type="hidden" id="origen"  name="origen"  value="<?php echo $origen; ?>" />
<input type="hidden" id="tipousuario"  name="tipousuario"  value="<?php echo $tipousuario; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="coordinador"  name="coordinador"  value="<?php echo $coordinador; ?>" />
<input type="hidden" id="coordinadordos"  name="coordinadordos"  value="<?php echo $coordinadordos; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
          <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard" style="color:#F8F9F9;"> </i> Facturación POS: Búsqueda Masiva </a>
          </div>
       </nav>
         <b style="color:#5DADE2;">Búsqueda: <?php if( $banco!='') echo $databanco['ibp']; else  echo $fecha; ?></b>
       <br>
    </div>
    <div class="row">
          <div class="col-10">
            <table class="table table-bordered"  id="busquedafechafactpos" style="width: 100%"> 
               <thead>
                  <tr>
                    <th>Correlativo</th>
                    <th>enlace</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>codbanco</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>usuario</th>
                    <th>Fecha&nbsp;de&nbsp;Recepción</th>
                    <th>Fecha&nbsp;Carga</th>
                    <th>&nbsp;Nombre&nbsp;del&nbsp;Archivo&nbsp;</th>
                    <th>Cantidad de Registros</th>
                  </tr>
                </thead>
                <tfoot>
            
                <th colspan="4" style="text-align:left">Total:</th>
            
          </tfoot>
            </table>
          </div>
        </div>
      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='facturacionpos.php'"/>
        </div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function dtCore(){
  banco= document.getElementById('banco').value; 
  fecha= document.getElementById('fecha').value;
  origen= document.getElementById('origen').value; 
  tipousuario= document.getElementById('tipousuario').value;
  ejecutivo= document.getElementById('ejecutivo').value;
  usuario= document.getElementById('usuario').value;
  coordinador= document.getElementById('coordinador').value;
  coordinadordos= document.getElementById('coordinadordos').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedafechafactpos').DataTable({
    "ajax": {
    "url": "api/selectGestionFacturacion.php",
          "type": "POST",
          "data": {"banco":banco,"fecha":fecha,"origen":origen,"tipousuario":tipousuario,"ejecutivo":ejecutivo,
          "usuario":usuario,"coordinador":coordinador, "coordinadordos":coordinadordos}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "enlace" },
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Ver Detalle</a>", className: "text-center"},
      {"data": "codigobanco" },
      {"data": "ibp", className: "text-center" },
      {"data": "usuario", className: "text-center" },
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "fechacarga", className: "text-center" },
      {"data": "namearchivo"},
      {"data": "cantidad", className: "text-center"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 1, 3, 5 ],
                 "visible": false,
             }
         ],
    "scrollX": true,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
         
        exportOptions : {
        columns: [ 0, 4, 5, 6, 7, 8, 9]
      }
    }],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });

  $('table#busquedafechafactpos tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#busquedafechafactpos').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "facturacionpos?cargar=buscarrecepcion&var="+enlace; 
        $(location).attr('href',url);

  });
}
  </script>
