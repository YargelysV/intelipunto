<?php
  $usuario=$_SESSION["session_user"];
  $area_usuario= $_SESSION["area_usuario"] ;
  $codtipousuario=  $_SESSION["superusuario"];
  $var=($_REQUEST['var']);
  $_SESSION["session_facturacion"]="1";
  $array = explode("/",$var);
  $rif= $array[0]; 
  $banco= $array[1];
  $fechacarga=isset($array[2]) ? $array[2] : "";
  $namearchivo = isset($array[3]) ? $array[3] : "";
  $masiva = isset($array[4]) ? $array[4] : "";
  $idregistro=isset($array[5]) ? $array[5] : "";
  $tipousuario=isset($array[6]) ? $array[6] : "";
  $ejecutivo=isset($array[7]) ? $array[7] : "";
  $user_login=isset($array[8]) ? $array[8] : "";
  $coordinador = isset($array[9]) ? $array[9] : "";
  $coordinadordos = isset($array[10]) ? $array[10] : "";
  $busqueda = isset($array[11]) ? $array[11] : "";
  $var1=$banco.'/'.$fechacarga.'/'.$tipousuario.'/'.$ejecutivo.'/'.$user_login.'/'.$coordinador.'/'.$coordinadordos.'/'.$masiva;
  $varbusqueda=$busqueda.'/'.$tipousuario.'/'.$ejecutivo.'/'.$user_login.'/'.$coordinador.'/'.$coordinadordos;

  $fecha = '2019-01-01';

  $_SESSION['session_retornovar']=$var;
  $varvolver=$busqueda.'/'.$banco.'/'.$tipousuario.'/'.$ejecutivo.'/'.$user_login;
  $varvolveradm=$idregistro.'/'.$rif.'/'.$banco.'/'.$tipousuario.'/'.$ejecutivo.'/'.$user_login.'/'.'1';
  $varvolveradm1=$idregistro.'/'.$rif.'/'.$banco.'/'.$fechacarga.'/'.$namearchivo.'/'.$tipousuario.'/'.$ejecutivo.'/'.$user_login.'/'.'3'.'/'."".'/'."".'/'.$coordinador.'/'.$coordinadordos;


   $controladoregistro =new ControladorAdjuntar();
   $comercialregistro=$controladoregistro->spregistrocliente($idregistro);
   $dataregistro = pg_fetch_assoc($comercialregistro);

if(!(isset($dataregistro['d_codetipo'])) && $fechacarga!=$fecha)
{
  if ($_SESSION["comercializacion"]!='0')
  {   
    ?>  
    
    <script type="text/javascript">
    function redireccionar(){
      alertify.alert("Error!, registro no posee dirección fiscal, debe agregar la información correspondiente o comunicarse con el ejecutivo comercial!.", function (e) {
        if (e) {
          window.location="gestioncomercializacion?cargar=editar&var=<?php echo trim($varvolveradm);?>"
        } else
        {
          window.location="gestioncomercializacion?cargar=editar&var=<?php echo trim($varvolveradm);?>"  
        } 
    })
    return false;
    }
    setTimeout ("redireccionar()", 1000); //tiempo expresado en milisegundos
    </script>
    <?php
  }
  else{
    ?>    
    <script type="text/javascript">
    function redireccionar(){
      alertify.alert("Error!, registro no posee dirección fiscal, Comuniquese con el ejecutivo comercial para agregar la información correspondiente!.", function (e) {
        if (e) {
           window.location="?cargar=buscarrecepcion&var=<?php echo trim($varvolver);?>"
        } else
        {
           window.location="?cargar=buscarrecepcion&var=<?php echo trim($varvolver);?>"
        } 
    })
    return false;
    }
    setTimeout ("redireccionar()", 1000); //tiempo expresado en milisegundos
    </script>
    <?php
  }
}
else
{
  $controladorc =new controladorAdjuntar();
  $comerciali=$controladorc->spcliente($idregistro);
  $databanco = pg_fetch_assoc($comerciali);

  $controladorc =new controladorAdjuntar();
  $infoclientes=$controladorc->spinfocliente($idregistro);
  $datacliente = pg_fetch_assoc($infoclientes);

    $controladorc =new controladorAdjuntar();
  $facturasanuladas=$controladorc->spfacturaanuladas($idregistro);
  $datos = pg_fetch_assoc($facturasanuladas);

  $controlador =new controladorAdjuntar();
  $cantidad= $controlador->mostrarClienteFacturacion($idregistro);
  $row=pg_fetch_assoc($cantidad);
  $estatusConfirmacion=$row['estatus'];
  $permisos= $_SESSION["administracion"];

  $ctrl =new controladorPagosBasico();
  $cantidad= $ctrl->sp_verpagos($idregistro);
  $countpagos=pg_num_rows($cantidad);
  $registros= $ctrl->sp_verpagos($idregistro);
  $control=1;

  $controladoranuladas =new controladorAdjuntar();
  $registroanulado= $controlador->mostrarClienteFacturasanuladas($idregistro);
  $factanul=pg_fetch_assoc($registroanulado);

  $controladorseriales =new controladorAdjuntar();
  $mostrarserial= $controlador->mostrarseriales($idregistro);
  $seriales=pg_fetch_assoc($mostrarserial);

  $controladorpago =new controladorAdjuntar();
  $infopagos=$controladorpago->sp_verpagos($idregistro);
  $datapago = pg_fetch_assoc($infopagos);

  $controladorpercances =new controladorAdjuntar();
  $cantidadpercances= $controladorpercances->spbuscarpercance($idregistro);
  $rowperc=pg_fetch_assoc($cantidadpercances);
?>
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $permisos; ?>" />
<input type="hidden" id="var" name="var" value="<?php echo $var; ?>">
<input type="hidden" id="rif" name="rif" value="<?php echo $rif; ?>">
<input type="hidden" id="banco" name="banco" value="<?php echo $banco; ?>">
<input type="hidden" id="fechacarga" name="fechacarga" value="<?php echo $fechacarga; ?>">
<input type="hidden" id="namearchivo" name="namearchivo" value="<?php echo $namearchivo; ?>">
<input type="hidden" id="idregistro" name="idregistro" value="<?php echo $idregistro; ?>">
<script type="text/javascript">mascaras();</script>
<div id="page-wrapper" >
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
         <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard" style="color:#F8F9F9;"> </i> Facturación POS: Cliente: <?php echo $datacliente['razon'];?>. RIF: <?php echo $datacliente['rif'];?>. </a>
          <br>
        </div>
       </nav>
    </div>
<?php   
        $_payFailed=0;
        $count = 0;
        $_allStatus=array();
        while($rowpagos = pg_fetch_array($registros)){
         $_CanTerminals= $row['cantidadterminales'];
        $_allStatus[$count] = $rowpagos['estatus'];
        $count++;

        if (in_array($_payFailed, $_allStatus)) {
            $control=1;
            // $_SESSION["control"]=$control;
        }else
            $control=0;
        } 
$row['costoposfacturado'] = isset($row['costoposfacturado']) ? $row['costoposfacturado'] : 0;

?>
<input type="hidden" id="countpagos"  name="permisos"  value="<?php echo $countpagos; ?>" />
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $permisos; ?>" />
<input type="hidden" id="var" name="var" value="<?php echo $var; ?>">
<input type="hidden" id="rif" name="rif" value="<?php echo $rif; ?>">
<input type="hidden" id="banco" name="banco" value="<?php echo $banco; ?>">
<input type="hidden" id="fechacarga" name="fechacarga" value="<?php echo $fechacarga; ?>">
<input type="hidden" id="namearchivo" name="namearchivo" value="<?php echo $namearchivo; ?>">
<input type="hidden" id="idregistro" name="idregistro" value="<?php echo $idregistro; ?>">
<input type="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">
<input type="hidden" id="codarea" name="codarea" value="<?php echo $area_usuario; ?>">
<input type="hidden" id="codtipousuario" name="codtipousuario" value="<?php echo $codtipousuario; ?>">
<input type="hidden" id="coordinador" name="coordinador" value="<?php echo $coordinador; ?>">
<input type="hidden" id="coordinadordos" name="coordinadordos" value="<?php echo $coordinadordos; ?>">
<input type="hidden" id="cantidadterminales" name="cantidadterminales" value="<?php echo $_CanTerminals; ?>">
<input type="hidden" id="imgfactura" name="imgfactura" value="<?php echo $row['imgfactura']; ?>">
<input type="hidden" id="estatusequipo" name="estatusequipo" value="<?php echo $row['estatusequipo']; ?>">


<style type="text/css">
.dataTables_wrapper .dt-buttons {
  /*width: 300px;*/
  float: right;
  line-height: 28px;
  font-size: 12px;
}
.error {
  width : 100%;
  padding: 0;

  font-size: 80%;
  color: white;
  background-color: #EC1717;
  border-radius: 0 0 5px 5px;

  box-sizing: border-box;
}

.error.active {
  padding: 0.3em;
}
.dataTables_wrapper .dataTables_filter{
  text-align:center;
   float: left;
}
.dataTables_wrapper .dataTables_paginate {
  display:none;
}
 {
  margin: 0;
  padding: 0;
}

.footer {
  padding: 10px;
  text-align: right;
}
.footer a {
  color: #999999;
  text-decoration: none;
}
.footer a:hover {
  text-decoration: underline;
}
.etiqueta {
  width: 120px;
  float: left;
  line-height: 28px;
  font-size: 20px;
}
.input_container {
  height: 30px;
  float: left;
}
.input_container input {
  height: 20px;
  width: 200px;
  padding: 3px;
  border: 1px solid #cccccc;
  border-radius: 0;
}
.input_container ul {
  width: 206px;
  border: 1px solid #eaeaea;
  position: absolute;
  z-index: 9;
  background: #f3f3f3;
  list-style: none;
  margin-left: 5px;
margin-top: -3px;
}
.input:invalid {
  border: 2px dashed red;
}
.input_container ul li {
  padding: 2px;
}

#country_list_id {
  display: none;
}
#country_list_id_sim {
  display: none;
}
#country_list_id_mifi {
  display: none;
}

.modal-content{
      height:400px;
      overflow:auto;
    }

 background: url("background.jpg") no-repeat;
  background-size: cover;
    min-height: 100%;
}

body{
    background:none;
}

#advanced-search-form{
    background-color: #fff;
    max-width: 800px;
    margin: 160px auto 0;
    padding: 40px;
    color: #858b8e;
    box-shadow: 6px 6px 6px rgba(0,0,0,0.2);
}

#advanced-search-form h2{
    padding-bottom: 20px;
    margin: 10px 20px;
    font-size: 24px;
}

#advanced-search-form hr{
    margin-top: 38px;
    margin-bottom: 54px;
    margin-left:3px;
    border: 1px solid #cccccc;

}

#advanced-search-form .form-group{
  margin-bottom: 20px;
    margin-left:20px;
  width: 30%;
    float:left;
    text-align: left;
}

#advanced-search-form .form-control{
    padding: 12px 20px;
    height: auto;
    border-radius: 2px;
}

#advanced-search-form .radio-inline{
    margin-left: 10px;
    margin-right: 10px;
}

#advanced-search-form .gender{
    width: 30%;
    margin-top: 30px;
    padding-left: 20px
    padding-top: 5px;
    padding-bottom: 5px;
}

#advanced-search-form .btn{
    width: 46%;
    margin: 20px auto 0;
    display: block;
    outline: none;
}

@media screen and (max-width: 800px){
    #advanced-search-form .form-group{
        width: 45%;
    }

    #advanced-search-form{
        margin-top: 0;
    }
}

@media screen and (max-width: 560px){
    #advanced-search-form .form-group{
        width: 100%;
        margin-left: 0;
    }

    #advanced-search-form h2{
        text-align: center;
    }
}

</style>



  <!--   <div class="demo-container" ng-app="AppFacturacionCliente" ng-controller="FacturacionClienteController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div> -->
     <div class="row" align="left">
      <div class="col-lg-12">
        <form method="POST"  action="?cargar=transferencia" enctype="multipart/form-data" id="aggfactura" name="subir">
        <div class="panel panel-info">
        
          <div class="panel-heading" style="text-align: center;">
            <i class="fa fa-user fa-fw"></i> Datos Afiliado
          </div>
          <div class="panel-body">
            <div class="list-group">
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Razon Social:
              <span class="pull-center text-muted small" id="razonsocialadm0"><b><?php echo  $datacliente['razon'];?></b>
              </span>
              <?php if (($area_usuario=='2'|| $area_usuario=='1') && $codtipousuario=='A') { ?>
              <input class="form-control" style="display: none; width: 20em;" type="text" name="razonsocialadm" id="razonsocialadm" maxlength="100" value="<?php echo  $datacliente['razon'];?>">
              <button type="button" id='editarrs' class="btn btn-info" style="float: right;">Editar</button>
              <button type="button" id='guardarrs' class="btn btn-info" style="float: right; display: none;">Guardar</button>
              <?php } ?>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Razon Comercial: 
              <span class="pull-center text-muted small" id='razoncladm0'><b><?php echo  $datacliente['razoncl'] ;?></b>
              </span>
              <?php if (($area_usuario=='2' || $area_usuario=='1') && $codtipousuario=='A') { ?>
              <input class="form-control" style="display: none; width: 20em;" type="text" name="razonsocialadm" id="razoncladm" maxlength="100" value="<?php echo  $datacliente['razoncl'];?>">
              <button type="button" id='editarcl' class="btn btn-info" style="float: right;">Editar</button>
              <?php } ?>
               <button type="button" id='guardarcl' class="btn btn-info" style="float: right; display: none;">Guardar</button>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> RIF: 
              <span class="pull-center text-muted small" id='rifadm0'><b><?php echo $datacliente['rif'];?></b>
              </span>
               <?php if (($area_usuario=='2'|| $area_usuario=='1') && $codtipousuario=='A') { ?>
              <input class="form-control" style="display: none; width: 20em;" type="text" name="razonsocialadm" id="rifadm" maxlength="10" placeholder="Ej: J025548906" value="<?php echo $datacliente['rif'];?>">
              <button type="button" id='editarrf' class="btn btn-info" style="float: right;">Editar</button>
               <button type="button" id='guardarrf' class="btn btn-info" style="float: right; display: none;">Guardar</button>
               <?php } ?>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Nº Afiliado:
              <span class="pull-center text-muted small"><b><?php echo  $row['nafiliacion'] ;?></b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Banco:
              <span class="pull-center text-muted small"><b><?php echo isset($row['ibp']) ? $row['ibp'] : "" ;?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Marca de POS:
              <span class="pull-center text-muted small"><b><?php echo isset($row['marcapos']) ? $row['marcapos'] : "" ;?> </b>  <?php if (($area_usuario=='1' || $area_usuario=='2') && $codtipousuario=='A' && ($row['estatusequipo']==5)) { ?>
                <input type="button" class="btn btn-link"  id="cambiarmodelo" name="cambiarmodelo" value="Cambiar Modelo" onclick="window.location='gestioncomercializacion?cargar=editar&var=<?php echo $varvolveradm1;?>'" >
                 <?php  }?> 
                    
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Tipo de POS :
              <span class="pull-center text-muted small"><b><?php echo isset($row['tipopos']) ? $row['tipopos'] : "" ;?> </b>
              </span>
              </div>
              <?php if ($row['tipoposmodificado']!='') { ?>


              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Marca de POS Nuevo:
              <span class="pull-center text-muted small"><b><?php echo isset($row['marcaposmodificado']) ? $row['marcaposmodificado'] : "" ;?> </b>                    
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Tipo de POS Nuevo:
              <span class="pull-center text-muted small"><b><?php echo isset($row['tipoposmodificado']) ? $row['tipoposmodificado'] : "" ;?> </b>
              </span>
              </div>
              <?php } ?>

              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Cantidad de POS:<?php $term=(integer)$row['cantidadterminales']; ?>
              <span class="pull-center text-muted small"><b><?php echo isset($row['cantidadterminales']) ? $row['cantidadterminales'] : "" ;?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Costo de POS:
                <span class="pull-center text-muted small" rows="4" ><b class="money"><?php echo ('Bs.S '.$row['costoposfacturado']); ?></b>
              </span>
              <!--span class="pull-center text-muted small" rows="4" ><b><?php //echo money_format('%(#10n', $row['costoposfacturado']); ?></b>
              </span-->
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Dirección:
              <span class="pull-center text-muted small" rows="4" ><b><?php echo isset($row['direccion']) ? $row['direccion'] : "" ;?></b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Estatus:
              <input type="hidden" name="estatusconfirmacion" id="estatusconfirmacion" value="<?php echo $estatusConfirmacion; ?>">
              <span class="pull-center text-muted small" rows="4" ><b><?php if($row['estatus']==1) {echo "Pagos Confirmados"; }elseif ($row['estatus']==0) {

                echo "Pagos Pendientes por Confirmar"; }elseif ($row['estatus']==2) {

                echo "Pendiente por Pagos";
                
              } ;?></b>
              </span>
              </div>

           
               
            </div> 
            <?php 
              if (($row['fechacreacion'])!='') {
               
             
            ?>
            <div class="container-fluid">
                <i class="fa fa-check fa-fw"></i> Adjuntar Factura:
              <div class="row">
               <div class="col-sm-4"> 
                <input type="file" name="archivo" id="archivo" required="required">
               <input type="hidden" id="idregistro" name="idregistro" value="<?php echo $idregistro; ?>"></div>
                <input type="hidden" id="var" name="var" value="<?php echo $var; ?>">


                <div class="col-sm-4">

                <?php 
                if (($row['imgfactura'])!='') {


                
                  $EXT=explode('.',$row['imgfactura']);

                  if ($EXT[1]=="pdf" OR $EXT[1]=="PDF") {
                  ?>
                     <a href="img_factura" target="_blank" onclick="ver_imgfactura(<?php echo $idregistro;?>)">
                    <iframe id="inlineFrameExample" title="Archivo" width="150" height="150" 
                    src="<?php  echo $row['imgfactura'];?>">
                    </iframe>
                    Ver Imagen
                  </a>
                  <?php }else{ ?>
                    <a href="img_factura" target="_blank" onclick="ver_imgfactura(<?php echo $idregistro;?>)">
                      <img  src="<?php echo $row['imgfactura']; ?>" height="150px" width="150px">
                    </a>  
                 
                  <?php } }?>

              </div>

               
             <?php if ($datos['img_factura']!='') {
              ?>
             
               <div class="col-sm-4"><input type="button" href="img_factura" target="_blank"  class="btn btn-link" id="verfactura" name="verfactura" value="Ver facturas anteriores" onclick="ver_imgfacturaanterior(<?php echo $idregistro;?>)"></div>
                 <input type="hidden" id="idregistro" name="idregistro" value="<?php echo $idregistro; ?>">
               <?php
             } else{
              ?>  

               <div class="col-sm-4"><input type="hidden" class="btn btn-success" id="verfactura" name="verfactura" value="Ver archivo"></div>
 </div> 
               <?php
              
              }

              ?>
               
             
            </div>
            <br>
             <div class="panel-footer" align="center">
        <input type="button" class="btn btn-success" name="guardar" value="Guardar" onclick="validararhivo();">
      </div>
             <!--div class="panel-footer" align="center">
                        <input type="submit" class="btn btn-success" id="guardar" name="guardar" value="Guardar" >

                        
                    </div-->

          </div>
          <?php
             } 
              

              ?>
            
         
         
        </div>
 </form>
      </div>
    </div>


<?php if ($codtipousuario!='T') { ?>
<div class="row" align="center">
      <div class="col-lg-12" align="center">
          <div class="panel panel-info">
              <div class="panel-heading" style="text-align: center;">
            <i class="fa fa-money fa-fw"></i> Pagos
          </div>
        <div class="row">
          <div class="col-lg-12" align="center">
            <table class="table table-bordered"  id="tblpagosfactpos"> 
               <thead>
                  <tr>
                    <th>p_id</th>
                    <th>idafiliado</th>
                    <th>&nbsp;&nbsp;Fecha&nbsp;Pago&nbsp;&nbsp;</th>
                    <th>Hora Pago</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Origen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Destino&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Forma Pago</th>
                    <th>Moneda</th>
                    <th>Monto USD</th>
                    <th>Factor Conversion</th>
                    <th>Monto</th>
                    <th>Referencia</th>
                    <th>Depositante</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Observacion&nbsp;Comercial&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Observacion&nbsp;Administracion&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;Estatus&nbsp;&nbsp;</th>
                    <th>Fecha Carga</th>
                    <th>enlace</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>nombrebancorigen</th>
                    <th>nombrebancdestino</th>
                  </tr>
                </thead>
                
            </table>
          </div>
        </div>
        </div>
    </div>
</div> <?php } ?>

<?php if ($area_usuario==1/*sistemas*/ ||  $area_usuario==2/*ADMINISTRACION*/ || $codtipousuario=='T') {
  
 ?>
<div class="row">  <?php if ($row['estatusequipo']==2) { ?>
   <div class="col-md-4 col-md-offset-4 "  align="center">
        <input type="button" class=" btn btn-lg btn-success" name="confirmar" value="Confirmar" onclick="confirmAllPayments()"<?php if ($control==1 || $estatusConfirmacion==1 ) { echo "disabled"; } ?>><br>
        <br><br>
    </div>
    <label><input type="radio" name="check" id="checkdev" onclick="devequipos()">Sustitución de Equipos</label><br>
    <label><input type="radio" name="check" id="checkdevtotal" onclick="devtotalequipos()">Declinación de Compra</label><br>
    
    <?php  } else if (($row['estatusequipo']==13) ||  ($row['estatusequipo']==16)) { ?>
       <div class="col-md-4 col-md-offset-4 "  align="center">
        <input type="button" class=" btn btn-lg btn-success" name="confirmar" value="Confirmar" onclick="confirmAllPayments()"<?php if ($control==1 || $estatusConfirmacion==1 ) { echo "disabled"; } ?>><br>
        <br><br>
    </div>
    <label><input type="radio" name="check" id="checkdev" onclick="devequipos()">Sustitución de Equipos</label><br>
    <label><input type="radio" name="check" id="checkdevtotal" onclick="devtotalequipos()">Declinación de Compra</label><br>
       <label><input type="radio" name="check" id="checkpercances" onclick="percances()">Percances</label><br>
   
    <?php  } else if (($row['estatusequipo']==13) ||  ($row['estatusequipo']==16)) { ?>
       <div class="col-md-4 col-md-offset-4 "  align="center">
        <input type="button" class=" btn btn-lg btn-success" name="confirmar" value="Confirmar" onclick="confirmAllPayments()"<?php if ($control==1 || $estatusConfirmacion==1 ) { echo "disabled"; } ?>>
        <br>
        <br>
        
    </div>
       <label><input type="radio" name="check" id="checkdev" onclick="devequipos()">Sustitución de Equipos</label><br>
       <label><input type="radio" name="check" id="checkdevtotal" onclick="devtotalequipos()">Declinación de Compra</label><br>
       <label><input type="radio" name="check" id="checkpercances" onclick="percances()">Percances</label><br>
</div>
<?php  } else { ?>

  <?php if ($area_usuario!=3) { ?> 
<div class="col-md-4 col-md-offset-4 "  align="center">
        <input type="button" class=" btn btn-lg btn-success" name="confirmar" value="Confirmar" onclick="confirmAllPayments()"<?php if ($control==1 || $estatusConfirmacion==1 ) { echo "disabled"; } ?>><br>
        <br><br>
    </div>
   <label><input type="radio" name="check" id="check" onclick="factanulada()">Anulación de Factura</label><br>
    <label><input type="radio" name="check" id="checkdev" onclick="devequipos()">Sustitución de Equipos</label><br>
    <label><input type="radio" name="check" id="checkdevtotal" onclick="devtotalequipos()">Declinación de Compra</label><br>
    <label><input type="radio" name="check" id="checkpercances" onclick="percances()">Percances</label><br>
   <?php  } ?>
<br>
   <?php  }   ?>



<DIV class="col-lg-12">
<div class="col-md-4 col-md-offset-4">
                <div class="panel panel-success">
                    <div class="panel-heading" align="center">
                        Facturaci&oacute;n
              </div>
                    <div class="panel-body">
                      <div class="form-group">
                        <label>N° Factura</label>
                          <input type="text" class="form-control" name="numerofactura" id="numerofactura" maxlength="7" value="<?php echo $row['nfactura'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?>>  
                          <p class="help-block">Coloque el n&uacute;mero de factura.</p>
                          <label>Fecha Factura</label>
                          <input type="date" class="form-control" name="ffactura" id="ffactura" value="<?php echo $row['fechafacturacion'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?>>
                          <p class="help-block">Coloque la fecha en que realiz&oacute; la factura.</p>
                        <label>Monto Bs.S</label>
                          <input type="text" class="form-control money" name="montofactura" id="montofactura" value="<?php echo $row['costoposfacturado'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?>>
                          <p class="help-block">Indique el monto.</p>
                          
                          <label>Serial de Equipos</label>
                         <a class="btn btn-link" type="button"  data-toggle="modal" data-target="#myModal">Agregar</a>
               <!--  
                          <label>Serial POS</label>
                          <input list="lista_id" class="form-control"  name="browser" id="serial" value="<?php echo $row['serial'];  ?>" onkeyup="autocompletar()">  
                          <br>
                           <datalist id="lista_id">
                           </datalist>  
                      <div align="center"> <span class="alert-danger" id="validarserial"></span>
                        </div>
                        <br>
                         
                          <label>Serial SimCard</label>
                          <input list="lista_id_sim" class="form-control"  name="browser" id="simcard" value="<?php echo $row['simcard'];  ?>" onkeyup="autocompletarsimcard()">  
                          <br>
                             <datalist id="lista_id_sim">
                           </datalist>
                          <div align="center"><span class="alert-danger" id="validarsimcard"></span>
                          </div>    
                          <br>

                          <label>Serial Mifi</label>
                          <input list="lista_id_mifi" class="form-control"  name="browser" id="serialm" value="<?php echo $row['serialmifi'];  ?>" onkeyup="autocompletarmifi()">
                          <br>
                             <datalist id="lista_id_mifi">
                           </datalist>
                           <div align="center"><span class="alert-danger" id="validarmifi"></span>
                          </div>   -->
                         
                      </div>
                  
                    </div>
                 
                      <div class="panel-footer" align="center">
                 <?php if (($row['estatusequipo']==2)) { ?>

                <input type="button" class="btn btn-success" id="guardarfacturacion" name="guardarfacturacion" value="Guardar" onclick="guardarFacturacion();">
               <?php } else { ?>
                 <input type="button" class="btn btn-success" id="guardarfacturacion" name="guardarfacturacion" value="Guardar" onclick="guardarFacturacion();" disabled>
                  <?php } ?>       
                    </div>
               </div>
</div>

<div>

</div>
<div id="factura" class="col-md-3 " style="display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" align="center">
                        Factura Nueva
              </div>
                    <div class="panel-body">
                      <div class="form-group">
                        <label>N° Factura (Nueva)</label>
                          <input type="text" class="form-control" name="numfactura" id="numfactura" maxlength="7" value="<?php echo $factanul['nfactura'] ?>"  onkeyup="validarfactura()">
                          <div align="center"><span class="alert-danger" id="validarfacturas"></span>
                          </div>  
                          <p class="help-block">Coloque el n&uacute;mero de factura.</p>
                          <label>Fecha Factura (Nueva)</label>
                          <input type="date" class="form-control" name="datefactura" id="datefactura">
                          <p class="help-block">Coloque la fecha en que realiz&oacute; la factura.</p>
                        <label>Monto Bs.S (Nueva)</label>
                          <input type="text" class="form-control money" name="bsfactura" id="bsfactura">
                          <p class="help-block">Indique el monto.</p>

                    <!-- 
                          <label>Serial POS</label>
                          <input list="lista_id" class="form-control"  name="browser" id="serial" value="<?php echo $row['serial'];  ?>" onkeyup="autocompletar()">
                             <datalist id="lista_id">
                    
                             </datalist>
                          
                            <br>
                          <label>Serial SimCard</label>
                          <input list="lista_id_sim" class="form-control"  name="browser" id="simcard" value="<?php echo $row['simcard'];  ?>" onkeyup="autocompletarsimcard()">
                             <datalist id="lista_id_sim">
                    
                             </datalist>
                          <br>

                          <label>Serial Mifi</label>
                          <input list="lista_id_mifi" class="form-control"  name="browser" id="serialm" value="<?php echo $row['serialmifi'];  ?>" onkeyup="autocompletarmifi()">
                             <datalist id="lista_id_mifi">
                    
                             </datalist> -->
                       
                      </div>
                  
                    </div>
                    <div class="panel-footer" align="center">
                        <input type="button" class="btn btn-primary" id="btnguardar" name="guardar" value="Guardar" onclick="guardarFacturaAnulada();">

                        
                    </div>
               </div>
</div>


<div id="devolucion" class="col-md-4" style="display: none;">
    <div class="panel panel-danger">
                    <div class="panel-heading" align="center">
                        Sustitución de Equipos
              </div>
                    <div class="panel-body">
                      <div class="form-group">
                        <label>N° Factura (Nueva)</label>
                          <input type="text" class="form-control" name="facturadev" id="facturadev" maxlength="7" value="<?php echo $row['nfactura'];  ?>" >
                          <p class="help-block">Coloque el n&uacute;mero de factura.</p>
                          <label>Fecha Factura (Nueva)</label>
                          <input type="date" class="form-control" name="fechadev" id="fechadev" value="<?php echo $row['fechafacturacion'];  ?>" >
                          <p class="help-block">Coloque la fecha en que realiz&oacute; la factura.</p>
                        <label>Monto Bs.S</label>
                          <input type="text" class="form-control money" name="montodev" id="montodev" value="<?php echo $row['costoposfacturado'];  ?>" >
                          <p class="help-block">Indique el monto.</p>

                          <label>Serial de Equipos(Nuevos)</label>
                        <center><a class="btn btn-link" type="button"  data-toggle="modal" data-target="#myModalDevolucion">Agregar</a></center>
                         <!--  <label>Serial POS (Nuevo)</label>
                          <input list="lista_id" class="form-control"  name="browser" id="serialdev" onkeyup="autocompletarserialdev()">
                          <br>
                          <datalist id="lista_id">
                          </datalist>
                           <div align="center"> <span class="alert-danger" id="validarserialdev"></span>
                        </div>              
                     
                         <label>Serial SimCard (Nuevo)</label>
                         <input list="lista_id_sim" class="form-control"  name="browser" id="simcarddev" onkeyup="autocompletarsimcard()">
                         <datalist id="lista_id_sim">
                         </datalist>
                          <br>
                          <br>
                          <label>Serial Mifi (Nuevo)</label>
                          <input list="lista_id_mifi" class="form-control"  name="browser" id="serialmdev" onkeyup="autocompletarmifi()">
                             <datalist id="lista_id_mifi">
                             </datalist>     
                            <br>
                               <label>Motivo Devolucion</label>
                          <select class="form-control"  name="descmotivo" id="descmotivo">           
                           </select>
                           <input class="form-control hidden" name="codigomotivo" id="codigomotivo">-->
                            </div>
                      </div> 
                      <div class="panel-footer" align="center">
                        <input type="button" class="btn btn-danger" id="btnguardardevolucion" name="btnguardardevolucion" value="Guardar" onclick="guardarDevolucionFactura();">
                    </div>  
                    </div>

                     </div>

          <div id="devoluciontotal" class="col-md-4" style="display: none;">
           <div class="panel panel-warning">
              <div class="panel-heading" align="center">
                        Declinaci&oacute;n de Equipo Total
              </div>
                    <div class="panel-body">
                      <div class="form-group">
                        <label>N° Factura</label>
                          <input type="text" class="form-control" name="numfactura" id="numfactura" maxlength="7"  value="<?php echo $row['nfactura']; ?>"  onkeyup="validarfactura()" disabled>
                          <div align="center"><span class="alert-danger" id="validarfacturas"></span>
                          </div>  
                          <p class="help-block">Coloque el n&uacute;mero de factura.</p>
                          <label>Fecha Factura</label>
                          <input type="date" class="form-control" name="datefactura" id="datefactura"  value="<?php echo $row['fechafacturacion']; ?>"> 
                          <p class="help-block">Coloque la fecha en que realiz&oacute; la factura.</p>
                        <label>Monto Bs.S</label>
                          <input type="text" class="form-control money" name="bsfactura" id="bsfactura"  value="<?php echo $row['costoposfacturado']; ?>" disabled> 
                          <p class="help-block">Indique el monto.</p>
                         <label>Serial de Equipos</label>
                       <a class="btn btn-link" type="button"  data-toggle="modal" data-target="#myModalDevolucionTotal">Agregar</a>
                    <!-- 
                          <label>Serial POS</label>
                          <input list="lista_id" class="form-control"  name="browser" id="serial" value="<?php echo $row['serial'];  ?>" onkeyup="autocompletar()">
                             <datalist id="lista_id">
                    
                             </datalist>
                          
                            <br>
                          <label>Serial SimCard</label>
                          <input list="lista_id_sim" class="form-control"  name="browser" id="simcard" value="<?php echo $row['simcard'];  ?>" onkeyup="autocompletarsimcard()">
                             <datalist id="lista_id_sim">
                    
                             </datalist>
                          <br>

                          <label>Serial Mifi</label>
                          <input list="lista_id_mifi" class="form-control"  name="browser" id="serialm" value="<?php echo $row['serialmifi'];  ?>" onkeyup="autocompletarmifi()">
                             <datalist id="lista_id_mifi">
                    
                             </datalist> -->
                       
                      </div>
                  
                    </div>
                    <div class="panel-footer" align="center">
                        <input type="button" class="btn btn-warning" id="btnguardardevoluciontotal" name="guardar" value="Guardar" onclick="guardarDevolucionTotal();">

                        
                    </div>
               </div>
</div>           
 <div id="percances" class="col-md-4" style="display: none;">
  <div class="panel panel-warning">
    <div class="panel-heading" align="center">
              Percances
    </div>
    <div class="panel-body">
      <div class="form-group">
          <label>Fecha de Percance</label>
          <input type="date" class="form-control" name="fechapercance" id="fechapercance"  value="<?php echo $rowperc['fechainst']; ?>"> 
          <label>Serial de Equipos</label>
          <a class="btn btn-link" type="button"  data-toggle="modal" data-target="#myModalpercances">Agregar</a>
       
      </div>
    </div>
    <div class="panel-footer" align="center">
        <input type="button" class="btn btn-warning" id="btnguardarPercance" name="guardar" value="Guardar" onclick="guardarPercance();">
    </div>
  </div>
</div>                    
               
</DIV>

</div>

<?php } ?>

 <div class="row" align="center">
          <div class="col-md-4 col-md-offset-4">
            <?php if ($busqueda=="") { ?>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarrecepcion&var=<?php echo $var1;?>'"/>
            <?php } elseif ($busqueda=="0") { ?>

              <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location='reporteestatus?cargar=generarreporteestatus&var=<?php echo  $busqueda; ?>'"/>
             
              <!--  <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarrif&var=<?php ?>'"/> -->

            <?php } else { ?>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarrif&var=<?php echo $varbusqueda;?>'"/>
            <?php } ?>
  
          </div>
        </div>

<br>
  </div>
</div>
      <?php } ?>



  <!-- Trigger the modal with a button -->
  
<!-- Modal de facturacion-->
<div class="container">

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:900px;right: 25%;">
          <div class="modal-header default">
        <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
          <center>Agregar Seriales <center>
           
      </div>


        <div class="modal-body">
              <div class="table-responsive">
           <table class="table table-striped table-bordered table-hover">
          <thead>

               <?php          
              $seriales= $controlador->mostrarseriales($idregistro);
              $row2 = pg_fetch_array($seriales);

              ?>
              
              <tr>
              <th>N°</th>
              <th>Serial POS</th>
              <th>Serial Simcard</th>
              <th>Serial MIFI</th>
              <th>Traslado del Equipo</th>        
        
        <?php 
        
      $seriales= $controlador->mostrarseriales($idregistro);
      $count=0;
      
      while ($row2=pg_fetch_array($seriales)):
      $count++; 
       ?>
           
            
       <tbody>

                <td align="center"><?php echo $count;?>
                </td>
              
                <td align="center">
                <div align="center"> <span class="alert-danger" id='<?php echo "caracterespos".$count?>' ></span>
                  </div>
                 <input list='<?php echo "lista_id".$count?>' class="form-control"  name="browser" id='<?php echo "serial".$count?>' value="<?php echo $row2['serial']?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?> onkeyup="autocompletar(<?php echo $count; ?>); ">  
                            <br>
                  <span class="pull-center text-muted small"><b><?php echo $row2['posdev'];?> </b></span>
                          <br>
                           <datalist id='<?php echo "lista_id".$count?>'>
                           </datalist>  
                      <div align="center"> <span class="alert-danger" id='<?php echo "validarserial".$count?>' ></span>
                        </div>


                </td>

                 <td align="center">
                 <input list='<?php echo "lista_id_sim".$count?>' class="form-control"  name="browser" id='<?php echo "simcard".$count?>'  value="<?php echo $row2['simcard'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?> onkeyup="autocompletarsimcard(<?php echo $count; ?>);">  
                           <br>
                  <span class="pull-center text-muted small"><b><?php echo $row2['simdev'];?> </b></span>
                          <br>
                             <datalist id='<?php echo "lista_id_sim".$count?>'>
                           </datalist>
                          <div align="center"><span class="alert-danger" id='<?php echo "validarsimcard".$count?>' ></span>
                          </div>   
                </td>
               
                <td align="center">
              <input list='<?php echo "lista_id_mifi".$count?>'class="form-control"  name="browser"  id='<?php echo "serialmifi".$count?>' value="<?php echo $row2['serialmifi'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?> onkeyup="autocompletarmifi(<?php echo $count; ?>);"> 
                           <br>
                  <span class="pull-center text-muted small"><b><?php echo $row2['mfdev'];?> </b></span>
                          <br>
                             <datalist id='<?php echo "lista_id_mifi".$count?>'>
                           </datalist>
                           <div align="center"><span class="alert-danger"id='<?php echo "validarmifi".$count?>'></span>
                          </div> 
                </td>
                 <td>
               
                  <?php if(($row2['idventa']==1 || $row2['idventa']==2 || $row2['idventa']==3) &&  $row2['idestatus']!=2 )  { ?>
                         
                          <select class="form-control" name="descdestino" id='<?php echo "codigodestino".$count ?>'
                       style="width:220px;padding-left:15px;color:#000000" disabled>           
                    <?php

                   $controlador =new controladorAdjuntar();
                   $destinoequipo=$controlador->spverdestinoequipo();
                  //  echo '<option value="0">Seleccione</option>';         
                   while ($row3=pg_fetch_array($destinoequipo)):
                              
                    ?>
                     <option value="<?php echo $row3['codigodestino']?>" <?php
                                      if ($row3['codigodestino']== $row2['idventa'])
                                      {
                                        echo "selected";
                                      }?>><?php
                                      echo $row3['descdestino'];
                                      ?>
                                    </option>
                                    
                     <?php  endwhile; ?>        
                                
                     </select>        
                       
                      <?php  } else { ?>
                      <select class="form-control" name="descdestino" id='<?php echo "codigodestino".$count ?>'
                       style="width:220px;padding-left:15px;color:#000000" >           
                    <?php

                   $controlador =new controladorAdjuntar();
                   $destinoequipo=$controlador->spverdestinoequipo();
                   // echo '<option value="0">Seleccione</option>';         
                   while ($row3=pg_fetch_array($destinoequipo)):
                              
                    ?>
                     <option value="<?php echo $row3['codigodestino']?>" <?php
                                      if ($row3['codigodestino']== $row2['idventa'])
                                      {
                                        echo "selected";
                                      }?>><?php
                                      echo $row3['descdestino'];
                                      ?>
                                    </option>
                                    
                     <?php  endwhile; ?>        
                                
                     </select>      
                        <?php  } ?>  
             
                  </td> 

<!--                   <td> <?php if (($row2['nterminal']=='' || $row2['nroafiliaci']=='') && $row2['idestatus']==0) {  ?>
                   <span class="alert-danger">Debe ingresar el número de terminal y el afiliado.</span>
      
                  </td>
               
                     <?php } ?> -->
                
                  <input class="hidden"  name="id" id='<?php echo "consecutivo".$count?>' value='<?php echo $row2['consecutivo'];?>' >
                  <input class="hidden"  name="id" id='<?php echo "idestatus".$count?>' value='<?php echo $row2['idestatus'];?>' >

                  <input class="hidden"  name="id" id='<?php echo "nterminal".$count?>' value='<?php echo $row2['nterminal'];?>' >
      
              
              </tbody>
    </div>  
          <!--   <div class="col-md-4">
            </div>
             <div class="col-md-4" class="alert alert-danger alert-dismissable col-lg-6">     
              <?php if (($row2['nterminal']=='' || $row2['nroafiliaci']=='') && $row2['idestatus']==0) {  ?>

                 <div class="alert alert-danger alert-dismissable">
                   <b>NOTA:</b>
                   Debe ingresar el número de terminal y el afiliado.
              <?php } ?>
              </div>
            </div>
            <div class="col-md-4">

            </div> -->


   <?php  endwhile; ?>

 </table>
    </div>

      
    </div>
  </div>
  </div>
 </div>


<!-- Modal de devolucion-->
<div class="container">

  <div class="modal fade" id="myModalDevolucion" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:1000px;right: 30%;">
          <div class="modal-header default">
        <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
          <center>Asignación de Equipos Nuevos (Devolución) <center>
           
      </div>


        <div class="modal-body">
              <div class="table-responsive">
           <table class="table table-striped table-bordered table-hover">
          <thead>

              <tr>
              <th>N°</th>
              <th>Serial POS</th>
              <th>Serial Simcard</th>
              <th>Serial MIFI</th>
              <th>Motivo</th>
              <th>Observaciones</th>
              <th>Traslado</th>
              </tr>
            </thead>
        
        <?php 
        
      $detalleserial= $controlador->mostrardetalleseriales($idregistro);
      $contador=1;
      
      while ($resul=pg_fetch_array($detalleserial)):
      $count++; 
       ?>
           

       <tbody>
               
                <td align="center"><?php echo $contador;?>
                </td>
              
               <td align="center">
                  <div align="center"> <span class="alert-danger" id='<?php echo "caracteresposdev".$contador?>' ></span>
                  </div>
                 <input list='<?php echo "lista_id".$contador ?>' class="form-control"  name="browser" id='<?php echo "serialdev".$contador ?>' onkeyup="autocompletarserialdev(<?php echo $contador; ?>);" > 
                 <br>
                  <span class="pull-center text-muted small"><b><?php echo $resul['serialdev'];?> </b></span>
                          <br>
                           <datalist id='<?php echo "lista_id".$contador ?>'>
                           </datalist>  
                      <div align="center"> <span class="alert-danger" id='<?php echo "validarserialdev".$contador ?>' ></span>
                        </div>
                </td>
                 <td align="center">
              <input list='<?php echo "lista_id_sim".$contador ?>'class="form-control"  name="browser"  id='<?php echo "simdev".$contador ?>' onkeyup="autocompletarsimdev(<?php echo $contador; ?>);"> 
                          <br>
                           <span class="pull-center text-muted small"><b><?php echo $resul['simdev'];?> </b></span>
                             <datalist id='<?php echo "lista_id_sim".$contador ?>'>
                           </datalist>
                           <div align="center"><span class="alert-danger"id='<?php echo "validarsimdev".$contador ?>'></span>
                          </div> 
                </td>
               
                <td align="center">
              <input list='<?php echo "lista_id_mifi".$contador ?>'class="form-control"  name="browser"  id='<?php echo "serialmdev".$contador ?>'  onkeyup="autocompletarmifidev(<?php echo $contador; ?>);">
                          <br>
                           <span class="pull-center text-muted small"><b><?php echo $resul['serialmdev'];?> </b></span>
                             <datalist id='<?php echo "lista_id_mifi".$contador ?>'>
                           </datalist>
                           <div align="center"><span class="alert-danger"id='<?php echo "validarmifidev".$contador ?>'></span>
                          </div> 
                </td>
                 <td>
                  <select class="form-control" name="descmotivo" id='<?php echo "codigomotivo".$contador ?>' style="width:180px;padding-left:15px;color:#000000">             
                                <?php

                   $controlador =new controladorAdjuntar();
                   $motivosdev=$controlador->spvermotivodev();
                            
                   while ($row3=pg_fetch_array($motivosdev)):
                              
                    ?>
                    <option value=" <?php echo $row3['codigomotivo'] ?> " >
                    <?php echo $row3['descmotivo']; ?> </option>
                                    
                     <?php  endwhile; ?>                    
                     </select>           
                                
                  </td>   
                  <td align="center">     
            <input class="form-control" id='<?php echo "observacion".$contador ?>' style="width:180px;padding-left:15px;color:#000000" required>
                    </td> 

                    <td>
                         <select class="form-control" name="descdestino" id='<?php echo "idgestiones".$contador ?>'
                       style="width:220px;padding-left:15px;color:#000000">           
                    <?php

                   $controlador =new controladorAdjuntar();
                   $destinoequipo=$controlador->spverdestinoequipo();
               
                   while ($row4=pg_fetch_array($destinoequipo)):      
                    ?>
                     <option value="<?php echo $row4['codigodestino']?>" >
                      <?php echo $row4['descdestino']; ?> </option>
                                    
                     <?php  endwhile; ?> 
                     
                                
                     </select>
                    </td>                              

         <input class="hidden"  name="id" id='<?php echo "consecutivo".$contador ?>' value='<?php echo $resul['consecutivo'];?>'>

          <input class="hidden"  name="id" id='<?php echo "id_estatus".$contador ?>' value='<?php echo $resul['id_estatus'];?>'>
          </tbody>

<?php $contador++; endwhile;  ?>
      </table>


    </div>
    </div>

      
    </div>
  </div>
  </div>
 </div>

<div class="container">

  <div class="modal fade" id="myModalDevolucionTotal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:800px;">
          <div class="modal-header default">
        <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
          <center>Declinaci&oacute;n de Compra<center>
           
      </div>


        <div class="modal-body">
              <div class="table-responsive">
           <table class="table table-striped table-bordered table-hover">
          <thead>

              <tr>
              <th>N°</th>
              <th>Serial POS</th>
              <th>Serial Simcard</th>
              <th>Serial MIFI</th>
              <th>Motivo</th>
              </tr>
            </thead>
        
        <?php 
        
      $detalleserial= $controlador->mostrardetalleseriales($idregistro);
      $contador=1;
      
      while ($resul=pg_fetch_array($detalleserial)):
      $count++; 
       ?>
           

       <tbody>
                
                <td align="center"><?php echo $contador;?>
                </td>
              
                <td align="center">
                <!--   <input type="hidden" name="serialdev" id='<?php echo "serialdevtotal".$contador ?>' value="<?php echo $resul['serialdev'] ?>"> -->
                 <input list='<?php echo "lista_id".$contador ?>' class="form-control"  name="browser" id='<?php echo "serialdevtotal".$contador ?>' value="<?php echo $resul['serialdev'] ?>"readonly> 
                 <br>
                 <!-- <span class="pull-center text-muted small"><b><?php echo $resul['serialdev'];?> </b></span>-->
                          <br>
                           <datalist id='<?php echo "lista_id".$contador ?>'>
                           </datalist>  
                      <div align="center"> <span class="alert-danger" id='<?php echo "validarserialdev".$contador ?>' ></span>
                        </div>
                </td>
                 <td align="center">
              <input list='<?php echo "lista_id_sim".$contador ?>'class="form-control"  name="browser"  id='<?php echo "simdevtotal".$contador ?>'  value="<?php echo $resul['simdev'] ?>" onkeyup="autocompletarsimdev(<?php echo $contador; ?>);" readonly> 
                          <br>
                           <!-- <span class="pull-center text-muted small"><b><?php echo $resul['simdev'];?> </b></span>-->
                             <datalist id='<?php echo "lista_id_sim".$contador ?>'>
                           </datalist>
                           <div align="center"><span class="alert-danger"id='<?php echo "validarsimdev".$contador ?>'></span>
                          </div> 
                </td>
               
                <td align="center">
              <input list='<?php echo "lista_id_mifi".$contador ?>'class="form-control"  name="browser"  id='<?php echo "serialmdevtotal".$contador ?>'  value="<?php echo $resul['serialmdev'] ?>"  onkeyup="autocompletarmifidev(<?php echo $contador; ?>);" readonly> 
                          <br>
                         <!--  <span class="pull-center text-muted small"><b><?php echo $resul['serialmdev'];?> </b></span>-->
                             <datalist id='<?php echo "lista_id_mifi".$contador ?>'>
                           </datalist>
                           <div align="center"><span class="alert-danger"id='<?php echo "validarmifidev".$contador ?>'></span>
                          </div> 
                </td>
                <td>
                  <input type="hidden" name="descmotivo" id='<?php echo "codigomotivodev".$contador ?>' value="4">
                  <input class="form-control" name="descmotivo" id='<?php echo "codigomotivodev".$contador ?>'  style="width:180px;padding-left:15px;color:#000000" placeholder="Declinación de compra" readonly>             
                                <!-- <?php

                   $controlador =new controladorAdjuntar();
                   $motivosdev=$controlador->spvermotivodev();
                            
                   while ($row3=pg_fetch_array($motivosdev)):
                              
                    ?>
                    <option value=" <?php echo $row3['codigomotivo'] ?> " >
                    <?php echo $row3['descmotivo']; ?> </option>
                                    
                     <?php  endwhile; ?>      -->   
                                
                     </select>           
                                
                  </td>                                     
<input class="hidden"  name="id" id='<?php echo "consecutivo".$contador ?>' value='<?php echo $resul['consecutivo'];?>'>
           
          </tbody>


<?php $contador++; endwhile;  ?>
      </table>


    </div>
    </div>

      
    </div>
  </div>
  </div>
 </div>

<div class="container">
  <div class="modal fade" id="myModalpercances" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" style="width:900px;right: 25%;">
        <div class="modal-header default">
          <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
          <center>Agregar Seriales <center>    
        </div>
        <div class="modal-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>

            
              
              <tr>
              <th>N°</th>
              <th>Serial POS</th>
              <th>Serial Simcard</th>
              <th>Serial MIFI</th>
              <th>Motivo</th>
              <th>Observaciones</th>    
              </tr>  
        
              </thead>
              <tbody>
              <?php 
              $contador=1;
              
              $serialesp= $controlador->mostrarseriales($idregistro);
              //$row5 = pg_fetch_array($serialesp);
              while ($row5 = pg_fetch_array($serialesp)):
                $count++; ?>  
           
            
                <tr>
                <td align="center"><?php echo $contador;?></td>
                <td align="center">
                <div align="center"> <span class="alert-danger" id='<?php echo "caracterespos".$contador;?>' ></span>
                </div>
                <input list='<?php echo "lista_id".$contador;?>' class="form-control"  name="browser" id='<?php echo "serialperc".$contador;?>' value="<?php echo $row5['serial'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?> onkeyup="autocompletar(<?php echo $contador; ?>); " disabled>  
                <br>
                <span class="pull-center text-muted small"><b><?php echo $row5['posdev'];?> </b></span>
                <br>
                <datalist id='<?php echo "lista_id".$contador;?>'></datalist>  
                <div align="center"> <span class="alert-danger" id='<?php echo "validarserial".$contador;?>' ></span>
                </div>
                </td>
                <td align="center">
                <input list='<?php echo "lista_id_sim".$contador;?>' class="form-control"  name="browser" id='<?php echo "simcardperc".$contador;?>'  value="<?php echo $row5['simcard'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?> onkeyup="autocompletarsimcard(<?php echo $contador; ?>);" disabled>  
                <br>
                <span class="pull-center text-muted small"><b><?php echo $row5['simdev'];?> </b></span>
                <br>
                <datalist id='<?php echo "lista_id_sim".$contador;?>'></datalist>
                <div align="center"><span class="alert-danger" id='<?php echo "validarsimcard".$contador;?>' ></span></div>   
                </td>
                <td align="center">
                <input list='<?php echo "lista_id_mifi".$contador;?>'class="form-control"  name="browser"  id='<?php echo "serialmifiperc".$contador;?>' value="<?php echo $row5['serialmifi'];?>" <?php if ($codtipousuario=='T') { echo "disabled"; } ?> onkeyup="autocompletarmifi(<?php echo $contador; ?>);" disabled> 
                <br>
                <span class="pull-center text-muted small"><b><?php echo $row5['mfdev'];?> </b></span>
                <br>
                <datalist id='<?php echo "lista_id_mifi".$contador;?>'></datalist>
                <div align="center"><span class="alert-danger"id='<?php echo "validarmifi".$contador;?>'></span>
                </div> 
                </td>
                <td>
                  <?php 
                   if(($row5['idestatus']==22 || $row5['idestatus']==23)){ ?>
                    <select class="form-control" name="motivopercance" id='<?php echo "codmotivo".$contador; ?>' style="width:220px;padding-left:15px;color:#000000" disabled >           
                        <?php
                        $controlador =new controladorAdjuntar();
                        $tiposperc=$controlador->spvertipopercance();
                        echo '<option value="0">Seleccione</option>';          
                        while ($row6=pg_fetch_array($tiposperc)):?>

                    <option value="<?php echo $row6['codmotivo']?>" 
                        <?php if ($row6['codmotivo']== $row5['idestatus'])
                          {
                            echo "selected";
                          }?>><?php
                            echo $row6['motivo'];
                        ?>
                    </option>              
                      <?php  endwhile; ?>                    
                  </select>   
                <?php  } else { ?>
                  <select class="form-control" name="motivopercance" id='<?php echo "codmotivo".$contador ?>' style="width:220px;padding-left:15px;color:#000000" >           
                      <?php

                     $controlador =new controladorAdjuntar();
                     $tiposperc=$controlador->spvertipopercance();
                      echo '<option value="0">Seleccione</option>';         
                     while ($row6=pg_fetch_array($tiposperc)):
                                
                      ?>
                    <option value="<?php echo $row6['codmotivo']?>" 
                      <?php if (($row5['consecutivo']==$rowperc['idg']) && ($row6['codmotivo']== $rowperc['idestatusinteliser']))
                      {
                        echo "selected";
                      }?>><?php
                        echo $row6['motivo'];
                      ?>
                    </option>
                                      
                    <?php  endwhile; ?>        
                                  
                  </select>      
                <?php  } ?>         
              </td> 
              <td align="center">     
                <input class="form-control" id='<?php echo "observacionpercanc".$contador; ?>' style="width:180px;padding-left:15px;color:#000000" required>
              </td> 
              <input class="hidden"  name="id" id='<?php echo "consecutivoperc".$contador;?>' value='<?php echo $row5['consecutivo'];?>' >
              <input class="hidden"  name="id" id='<?php echo "idestatusperc".$contador;?>' value='<?php echo $row5['idestatus'];?>' >
              </tr>
            <?php $contador++;  endwhile; ?> 
            </tbody>
          </table>  
        </div>  
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="modal fade" id="Modalpagos" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content"  style="width:1100px;right: 40%;height: 100%;">
        <div class="modal-header">
          <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
            <center>Agregar Datos de Pago</center>  
        </div>
        <div class="modal-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <div class="col-lg-12" style="margin-top:2px;">  
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <label><h5>Fecha Pago:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="date" name="fechapago" id="fechapago" class="form-control" placeholder="" tabindex="4" required>
                        <span class="error" id="errorfechapago"></span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <label><h5>Hora Pago:</h5></label>
                      </div>
                    <div class="col-xs-9 col-sm-9 col-md-9 ">
                      <p type="text" name="horapago" id="horapago" class="form-control" tabindex="3" readonly></p>
                    </div>
                  </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                       <label><h5>Banco Origen:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select id="bancoasignado" class="form-control">
                        </select>
                        <input class="form-control hidden" id="bancoorigen"  readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <label><h5>Banco Destino:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select type="select" id="todobanco" class="form-control">
                          </select>
                        <input class="form-control hidden" id="bancodestino" readonly>
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                       <label><h5>Forma pago:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select type="select" name="" id="formapago" class="form-control"></select>
                        <input class="form-control hidden" id="formapagob" >
                        <span class="error" id="errorformapago"></span>

                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <label><h5>Moneda:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select class="form-control" id="tipomoneda">
                                       <option value="">Seleccionar</option>
                                       <option value="1">Bs. S</option>
                                       <option value="2">USD</option>
                         </select>
                        <span class="error" id="errormoneda"></span>
                      </div>
                    </div>
                  <br><br>
                  <br>
                  <span></span>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <label><h5>Monto USD:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="montousd" class="form-control" tabindex="4" onkeypress="return soloNumeros(event)">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <label><h5>Factor conversión:</h5></label>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" onkeypress="return soloNumeros(event)" id="factorconversion" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <label><h5>Monto:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="texxt" onkeypress="return soloNumeros(event)" id="monto" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <label><h5>Referencia:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" onkeypress="return alpha(event)" id="referencia" class="form-control" placeholder="" tabindex="3">
                        <span class="error" id="errorreferencia"></span>
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <label><h5>Depositante:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="nombredepositante" id="nombredepositante" class="form-control" placeholder="" tabindex="4">
                        <span class="error" id="errordepositante"></span>
                        <span class="error" id="errordepositantemax"></span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <label><h5>Capture:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="imagenpago" id="imagenpago" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <label><h5>Obs. Comercial:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="observacioncomer" onkeypress="return alpha(event)" id="observacioncomer" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <label><h5>Obs. Administración:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="observacionadm" onkeypress="return alpha(event)" id="observacionadm" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <label><h5>Estatus:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="" id="" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <label><h5>fecha carga:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="celular" id="celular" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <label><h5>Registro:</h5></label>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="" id="" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <label><h5>Confirmacion:</h5></label>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="celular" id="celular" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br><br>
                </div>
              </table>
            <div class="panel-footer" align="center">
               <input type="button" class="btn btn-primary" id="btnguardar" name="guardar" value="Guardar" onclick="guardarpagospos();">            
             </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="container-fluid">
  <div class="modal fade" id="Modaleditarpagos" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content"  style="width:1100px;right: 40%;height: 100%;">
        <div class="modal-header">
          <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
            <center>Agregar Datos de Pago</center>  
        </div>
        <div class="modal-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <div class="col-lg-12" style="margin-top:2px;">  
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Fecha Pago:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorfechapagoe"></span>
                        <input type="date" name="fechapago" id="fechapagoedit" value="<?php echo $datapago['fechapago'];?>" class="form-control" placeholder="" tabindex="4" required>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <h5>Hora Pago:</h5>
                      </div>
                    <div class="col-xs-9 col-sm-9 col-md-9 ">
                      <input  name="horapago" id="horapagoedit" class="form-control" placeholder="" tabindex="3" value="<?php echo $datapago['horapago'];?>" readonly>
                    </div>
                  </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Banco Origen:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select id="bancoasignadosedit" class="form-control">
                        </select>
                        <input class="form-control hidden" value="<?php echo $datapago['bancoorigen'];?>" id="bancoorigenedit">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <h5>Banco Destino:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select id="todobancosedit" class="form-control">
                          </select>
                        <input class="form-control hidden" id="bancodestinoedit" value="<?php echo $datapago['bancodestino'];?>">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Forma pago:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorformapagoe"></span>
                        <select  name="" id="formapagosedit"  class="form-control"></select>
                        <input class="hidden" id="formapagobedit" value="<?php echo $datapago['formapago'];?>">

                      </div>
                    </div>
                    <div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <h5>Moneda:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errormonedae"></span>
                        <?php if ($datapago['tipomoneda']==1) {?>
                          <select class="form-control" id="tipomonedaedit" >
                                       <option value="">Seleccionar</option>
                                       <option value="1" selected="true">Bs. S</option>
                                       <option value="2" id="usd">USD</option>
                         </select>
                        <?php }else if ($datapago['tipomoneda']==2){?>
                          <select class="form-control" id="tipomonedaedit" >
                                       <option value="">Seleccionar</option>
                                       <option value="1" >Bs. S</option>
                                       <option value="2" selected="true">USD</option>
                         </select>
                        <?php }?>
                      </div>
                    </div>
                  <br><br>
                  <br>
                  <span></span>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Monto USD:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="montousdedit" class="form-control" value="<?php echo isset($datapago['montousd']) ? $datapago['montousd'] : "" ;?>" tabindex="4" onkeypress="return soloNumeros(event)">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Factor conversión:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" onkeypress="return soloNumeros(event)" value="<?php echo isset($datapago['factorconversion']) ? $datapago['factorconversion'] : "" ;?>" id="factorconversionedit" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Monto:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" onkeypress="return soloNumeros(event)" value="<?php echo isset($datapago['monto']) ? $datapago['monto'] : "" ;?>" id="montoedit" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Referencia:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorreferenciae"></span>
                        <input type="text" onkeypress="return alpha(event)" value="<?php echo isset($datapago['referencia']) ? $datapago['referencia'] : "" ;?>" id="referenciaedit" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Depositante:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errordepositantee"></span>
                        <span class="error" id="errordepositantemaxe"></span>
                        <input type="text" name="nombredepositante" value="<?php echo isset($datapago['nombredepositante']) ? $datapago['nombredepositante'] : "" ;?>" id="nombredepositanteedit" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Capture:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="imagenpago" id="imagenpagoedit" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Obs. Comercial:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="observacioncomer" onkeypress="return alpha(event)" id="observacioncomeredit" value="<?php echo isset($datapago['observacioncomer']) ? $datapago['observacioncomer'] : "" ;?>" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Obs. Administración:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="observacionadm" onkeypress="return alpha(event)" id="observacionadmedit" value="<?php echo isset($datapago['observacionadm']) ? $datapago['observacionadm'] : "" ;?>" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Estatus:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="" id="" class="form-control" value="<?php 
                        if (($datapago['estatus'])==0){
                            echo 'Por Comprobar';
                        } else if (($datapago['estatus'])==1){
                            echo 'Aprobado';
                        }?>" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>fecha carga:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="select" name="celular" id="celularedit" value="<?php echo isset($datapago['fechacarga']) ? $datapago['fechacarga'] : "" ;?>" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Registro:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="" id="" value="<?php echo $idregistro; ?>" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Confirmacion:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="celular" value="<?php echo isset($datapago['enlace']) ? $datapago['enlace'] : "" ;?>" id="celular" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br><br>
                </div>
              </table>
            <div class="panel-footer" align="center">
               <input type="button" class="btn btn-primary" id="btneditar" name="editar" value="Editar" onclick="editarPago();">            
             </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>
 <script languaje="javascript">
    seleccionarmotivo(); seleccionardestino(); opcionespagos(); validaciones(); validacionesedicion(); seleccionarmotvpercance();
    //window.setInterval("validacionserial(<?php echo $count?>)",1); 
</script>

<script type="text/javascript">
  var d = new Date();
  var horaactual=d.getHours()+':'+d.getMinutes();
  //alert (horaactual);
 document.getElementById('horapago').innerHTML = horaactual;
</script>

<script type="text/javascript">
  function dtCore(){
    var id_afiliado=document.getElementById('idregistro').value;
    $.fn.dataTable.ext.buttons.alert = {
    className: 'buttons-alert',
 
    action: function ( e, dt, node, config ) {
         $('#Modalpagos').modal('show');
    }
  };

  $('table#tblpagosfactpos').DataTable({
    "ajax": {
    "url": "api/SelectPagosPos.php",
          "type": "POST",
          "data": {"id_afiliado":id_afiliado}
    },
    "columns": [
      {"data": "id", className: "text-center"},
      {"data": "idafiliado"},
      {"data": "fechapago", className: "text-center" },
      {"data": "horapago"},
      {"data": "nombrebancorigen", className: "text-center" },
      {"data": "nombrebancdestino", className: "text-center" },
      {"data": "formapago", className: "text-center",
      "render" : function (data, type, full, meta, row) {
        if (data == "1" || data === "1") {
          return "TDC";
        }else if (data == "2" || data === "2"){
          return "Transferencia";
        }else if (data == "3" || data === "3"){
          return "Efectivo";
        }else if (data == "4" || data === "4"){
          return "Depósito por Taquilla";
        }else{
          return "";
        }
      }
    },
      {"data": "tipomoneda", className: "text-center",
        "render" : function (data, type, full, meta, row) {
          if (data == "1" || data === "1") {
            return "Bs.S";
          }else if (data == "2" || data === "2"){
            return "USD";
          }else{
            return "";
          }
        }
      },
      {"data": "montousd"},
      {"data": "factorconversion", className: "text-center" },
      {"data": "monto", className: "text-center" },
      {"data": "referencia", className: "text-center" },
      {"data": "nombredepositante"},
      {"data": "observacioncomer"},
      {"data": "observacionadm", className: "text-center" },
      {"data": "estatus", className: "text-center",
      "render" : function (data, type, full, meta, row) {
        if (data == "0" || data === "0") {
          return "Por aprobar";
        }else if (data == "1" || data === "1"){
          return "Aprobado";
        }else{
          return "";
        }
      }
       },
      {"data": "fechacarga"},
      {"data": "enlace"},
      {"defaultContent":"<button class='ATENDER btn btn-success' type='button' id='atender' title='Asignar'><i class='fa fa-check'></i></button>  <button class='Editar btn btn-primary' type='button' id='editar' title='Editar'><i class='fa fa-edit'></i></button>  <button class='Eliminar btn btn-danger' type='button' id='eliminarpago'><i class='fa fa-trash'></i></button>", className: "text-center"},
      {"data": "nombrebancorigen"},
      {"data": "nombrebancdestino"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 0,1,17,19,20 ],
                 "visible": false,
             }
         ],
    "scrollX": true,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'alert',
          text: '<button class="btn btn-warning" type="button">+</button>'
      }, 
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
      "oPaginate": false,
       "fixedHeader": {"header": false, "footer": false},
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
  });

  $('table#tblpagosfactpos tbody').on( 'click', 'button.ATENDER', function () {
  //alert('algo');
        var table=$('table#tblpagosfactpos').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlace=D.enlace;
        var url = "facturacionpos?cargar=confirmarPago&var="+enlace; 
        $(location).attr('href',url);

  });

  $('#tblpagosfactpos tbody').on( 'click', 'button.Eliminar', function () {
       var table=$('table#tblpagosfactpos').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idafiliado = $('#idregistro').val();
       var referencia = T.referencia;

      alertify.confirm("&#191;Desea eliminar este pago?", function (e) {
        if (e) {
          $.post('view/Administracion/facturacionpos/eliminarpagos.php',{postvariable:referencia+'/'+idafiliado},
            function(data)
            {
              alertify.success(data);
              setTimeout(function(){ window.location.reload(0); }, 1000);
            });
        } else {
          alertify.error("Acción cancelada.");
          //setTimeout(function(){ window.location.reload(0); }, 1000);
          return false;
        }
      })
      return false;

   });
  
  $('table#tblpagosfactpos tbody').on( 'click', 'button.Editar', function () {
  //alert('algo');
         $('#Modaleditarpagos').modal('show');

  });
}
</script>
