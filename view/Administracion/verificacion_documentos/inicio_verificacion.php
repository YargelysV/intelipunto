<?php
//  session_start();
$tituloPagina=$_SESSION["page"];


if (isset($_SESSION["session_user"]))
{
    $usuario=$_SESSION["session_user"];
    $ctrlcaptacion = new controladorPagosBasico();
    $registros=$ctrlcaptacion->sp_expedientesporaprobar();
    $cantidad = pg_num_rows($registros);
}

?>
<style type="text/css">
  #grid {
    height: 440px;
}
#gridContainer .dec .diff {
    color: #f00;
}
.options {
    padding: 20px;
    margin-top: 20px;
    background-color: rgba(191, 191, 191, 0.15);
}

.caption {
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
}

.option {
    margin-bottom: 10px;
}

.option > span {
    position: relative;
    top: 2px;
    margin-right: 10px;
}

.option > .dx-widget {
    display: inline-block;
    vertical-align: middle;
}

#requests .caption {
    float: left;
    padding-top: 7px;
}

#requests > div {
    padding-bottom: 5px;
}

#requests > div:after {
    content: "";
    display: table;
    clear: both;
}

#requests #clear {
    float: right;
}

#requests ul {
    list-style: none;
    max-height: 100px;
    overflow: auto;
    margin: 0;
}

#requests ul li {
    padding: 7px 0;
    border-bottom: 1px solid #dddddd;
}

#requests ul li:last-child {
    border-bottom: none;
}
.diff{

  background: red;
  color: green;
}

.Por-asignar{
    
    color:white;
    background:RED;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

.radio-btn {
  margin-right: 10px;
  font-size: 16px;
  color: #FC2F1E;
}

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #46F516;
}

input:focus + .slider {
  box-shadow: 0 0 1px #46F516;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button 
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

 On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #EE2E14;
}


</style>

<!-- Page Content -->
 <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">  
 <input class="hidden" id="banco" name="banco" value="<?php echo $banco; ?>">  
 <input class="hidden" id="areausuario" name="areausuario" value="<?php echo $_SESSION["area_usuario"]; ?>"> 

   <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12"><h1> <i class="fa fa-check-square-o " style="color:#045FB4">Documentos por Aprobar</i></h1> </div>

     <!-- empoieza -->
<div class="row" >
      <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-primary" align="center">
              <div class="panel-heading panel-primary" style="">
            <i class="fa fa-usser fa-fw" ></i> Registros
          </div>
          <br>  
<input type="hidden" name="idregistros" value="" id="idregistros" class="form-control" placeholder="" tabindex="3" >
    <?php if ($cantidad!=0) { ?>

          <div class="col-12">
        <table class="table table-bordered"  id="aprobacionclientes"> 
           <thead>
              <tr>
                
                <th>N°</th>
                <th>ID</th>
                <th>Aprobación</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Motivo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Nº&nbsp;&nbsp;Afiliación&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Ver</th>
                <th>prefijo</th>

              </tr>
            </thead>
        </table>
 
      </div>

        </div>
    </div>

          <?php   } else{ ?>

        <div class="alert alert-danger" class="col-md-10">
                               Actualmente no hay registros pendientes.
                            </div>

       <?php   } ?>



  <script type="text/javascript">
function modales(){
  // Add minus icon for collapse element which is open by default
  $(".collapse.in").each(function(){
    $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
  });

  // Toggle plus minus icon on show hide of collapse element
  $(".collapse").on('show.bs.collapse', function(){
    $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hide.bs.collapse', function(){
    $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });
 
  }
  </script>
 
  <script type="text/javascript">
  function dtCore(){

  var banco= document.getElementById('banco').value;
  var areausuario= document.getElementById('areausuario').value;
  var usuario =document.getElementById('usuario').value;

  $('table#aprobacionclientes').DataTable({
    "ajax": {
    "url": "view/Administracion/verificacion_documentos/registroPorAprobarExp.php"
    },
    "columns": [

     
      {"data": "sec", className: "text-center"},
      {"data": "consecutivo", className: "text-center" },
      {"data": "idstatus", 
        render : function ( data, type, full, meta ) {
          //console.log(full['idregistro']);
          if ((data ==1 )){

              return ' <div id="a2" style="display: flex;justify-content: space-around;"><label>Si <input type="checkbox" id="estatus" class="check-aprobacion"></label><label>No <input type="checkbox" id="estatus" class="check-aprobacion2"></label>';
          }else 
            return '<label class="switch" disabled id="switch"><input checked type="checkbox" id="estatus" disabled value="0" class="check-aprobacion"><span class="slider round"></span></label><label class="switch" id="magic_switch" style="display: none;"><input type="checkbox" id="switch_check"><span class="slider round"></span>';
          
        }
      },
     
      
      {"data": "consecutivo",
        render : function ( data, type, full, meta ) {
          //console.log(data);
      
       
          return `<div id="${data}"><label>-</label></div> <div id="${data}2"><label></label></div>`;  
          
          
        }
      },
      
      //return '<input type="checkbox" class="check-buttons" checked value="0">',
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "tlf1", className: "text-center" },
      {"data": "codbanco", className: "text-center"},
      {"data": "numafiliado", className: "text-center" },
      {"data": "idstatus", 
        render : function (data) {
         
            return "<button class='Editar btn btn-primary' onclick='modales()' data-toggle='modal' data-target='#ModalExp22' type='button' id='modalbusc' title='Editar'><i class='fa fa-search'></i></button>";
       
        }
      },
      {"data": "prefijo", className: "text-center" },

    ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',
            exportOptions : {
              columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21] 
              }         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });
// 
// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
$('table#aprobacionclientes tbody').on( 'click', 'input.check-aprobacion', function () {
//alert('algo');
  var table=$('#aprobacionclientes').DataTable();
  var T =  table.row($(this).parents("tr")).data();
  var iconsecutivo = T.consecutivo;
// // 
var checks=1;
  console.log(checks);

// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
  if (checks==1) {
    var estatus=1;
    Swal.fire({
      title: 'Desea realizar la aprobación?',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Acepto!',
      cancelButtonText: 'No, cancelar!',
      }).then((result) => {
        if (result.value==true) {
          $.ajax({
                        url : "view/Administracion/verificacion_documentos/aprobado.php",
                        type : "POST",
                        data : {"iconsecutivo":iconsecutivo, "usuario":usuario},
                        success: function(data){
                          if (data ==1) {
                            
                _Text = "El estatus ha sido actualizado";
                _Type = "success";
                  Swal.fire({
                    text : _Text,
                    timer: 1000,
                    type : _Type,
                    onBeforeOpen: function (){
                      swal.showLoading()
                    }
                  }).then((result)=>{
                    var table=$('#aprobacionclientes').DataTable();
                    table.ajax.reload(); 
                  });
              }else
                swal(data);
            }
        })
          .fail(function(){
          swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
        });
      } else {
        _Title = "Cancelado!";
        _Text = "La actualización ha sido cancelado";
        _Type = "success";
        Swal.fire({
          text : _Text,
          title: _Title,
          timer: 1000,
          type : _Type,
        }).then((result)=>{                                       
           var table=$('#aprobacionclientes').DataTable();
          table.ajax.reload(); 
        });  
                    // 
      }
    });
  }
      });


// a1

$('table#aprobacionclientes tbody').on( 'click', 'input.check-aprobacion2', function () {
//alert('algo');
  var table=$('#aprobacionclientes').DataTable();
  var T =  table.row($(this).parents("tr")).data();
  var iconsecutivo = T.consecutivo;
// // 
var checks=1;
  console.log(checks);

// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
  if (checks==1) {
    var estatus=1;

    // sudor sangre y lagrimas
    $("#" + iconsecutivo).html('<div class="" style="width: 100%;"><select class="form-control1 form-control"  name="motrechazo" id="opciones">   </select>   </div>');  
    //alert(iconsecutivo);
  
$.ajax({
                        url : "view/Administracion/verificacion_documentos/estatusRechazo.php",
                        type : "POST",
                        data : {"iconsecutivo":iconsecutivo},
                        success: function(data){
                          $("#opciones").html(data)
                            }

  });
 
  }
      });
      
 //  a
 
      $('table#aprobacionclientes tbody').on( 'change', 'select.form-control1', function () {
     
       var table=$('#aprobacionclientes').DataTable();
       var tipo=document.getElementById('opciones').value;
       var T =  table.row($(this).parents("tr")).data();
       var iconsecutivo = T.consecutivo;
       var idprefijo = T.prefijo;
       estatusdes= opciones.value;
      //  alert (estatusdes);
      //  alert (iconsecutivo);
      //  alert (usuario);

       if( estatusdes != 3){
Swal.fire({
      title: 'Desea realizar la desaprobacion??',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Acepto!',
      cancelButtonText: 'No, cancelar!',
      }).then((result) => {
        if (result.value==true) {
          $.ajax({
                        url : "view/Administracion/verificacion_documentos/desaprobado.php",
                        type : "POST",
                        data : {"iconsecutivo":iconsecutivo, "estatusdes":estatusdes, "usuario":usuario},
                        success: function(data){
                          if (data) {
                            
                _Text = "El estatus ha sido actualizado";
                _Type = "success";
                  Swal.fire({
                    text : _Text,
                    timer: 1000,
                    type : _Type,
                    onBeforeOpen: function (){
                      swal.showLoading()
                    }
                  }).then((result)=>{
                    var table=$('#aprobacionclientes').DataTable();
                    table.ajax.reload(); 
                  });
              }else
                swal(data);
            }
        })
          .fail(function(){
          swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
        });
      } else {
        _Title = "Cancelado!";
        _Text = "La actualización ha sido cancelado";
        _Type = "success";
        Swal.fire({
          text : _Text,
          title: _Title,
          timer: 1000,
          type : _Type,
        }).then((result)=>{                                       
           var table=$('#aprobacionclientes').DataTable();
          table.ajax.reload(); 
        });  
                    
      }
    });
  }else { 

    var estatus=1;

// sudor sangre y lagrimas para examinar esto manana
$("#" + iconsecutivo + 2).html('<div class="" style="width: 100%;"><select class="form-control2 form-control" style="width:94%"  name="motrechazo" id="opcionesc">   </select>   </div>');  
//alert(iconsecutivo);

$.ajax({
                    url : "view/Administracion/verificacion_documentos/estatusRechazo2.php",
                    type : "POST",
                    data : {"iconsecutivo":iconsecutivo, "estatusdes":estatusdes, "idprefijo":idprefijo},
                    success: function(data2){
                      $("#opcionesc").html(data2)
                        }

});


  }






      });     
// aqui va el segundo select

$('table#aprobacionclientes tbody').on( 'click', 'input.check-aprobacion2', function () {
//alert('algo');
  var table=$('#aprobacionclientes').DataTable();
  var T =  table.row($(this).parents("tr")).data();
  var iconsecutivo = T.consecutivo;
// // 
var checks=1;
  console.log(checks);

// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
  if (checks==1) {
    var estatus=1;

    // sudor sangre y lagrimas
    $("#" + iconsecutivo).html('<div class="" style="width: 100%;"><select class="form-control1 form-control" style="margin-bottom: 10px;" name="motrechazo" id="opciones">   </select>   </div>');  
    //alert(iconsecutivo);
  
$.ajax({
                        url : "view/Administracion/verificacion_documentos/estatusRechazo.php",
                        type : "POST",
                        data : {"iconsecutivo":iconsecutivo},
                        success: function(data){
                          $("#opciones").html(data)
                            }

  });
 
  }
      });
      
 //  a
 
      $('table#aprobacionclientes tbody').on( 'change', 'select.form-control2', function () {
     
       var table=$('#aprobacionclientes').DataTable();
       var tipo=document.getElementById('opciones').value;
       var tipo=document.getElementById('opcionesc').value;
       var T =  table.row($(this).parents("tr")).data();
       var iconsecutivo = T.consecutivo;
       var idprefijo = T.prefijo;
       estatusdes= opciones.value;
       estatusdesc= opcionesc.value;
      //  alert (estatusdes);
      //  alert (estatusdesc);

       if( estatusdes != 0){
Swal.fire({
      title: 'Desea realizar la desaprobacion??',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Acepto!',
      cancelButtonText: 'No, cancelar!',
      }).then((result) => {
        if (result.value==true) {
          $.ajax({
                        url : "view/Administracion/verificacion_documentos/desaprobadodocs.php",
                        type : "POST",
                        data : {"iconsecutivo":iconsecutivo, "estatusdes":estatusdes, "estatusdesc":estatusdesc, "usuario":usuario},
                        success: function(data){
                          if (data ==1) {
                            
                _Text = "El estatus ha sido actualizado";
                _Type = "success";
                  Swal.fire({
                    text : _Text,
                    timer: 1000,
                    type : _Type,
                    onBeforeOpen: function (){
                      swal.showLoading()
                    }
                  }).then((result)=>{
                    var table=$('#aprobacionclientes').DataTable();
                    table.ajax.reload(); 
                  });
              }else
                swal(data);
            }
        })
          .fail(function(){
          swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
        });
      } else {
        _Title = "Cancelado!";
        _Text = "La actualización ha sido cancelado";
        _Type = "success";
        Swal.fire({
          text : _Text,
          title: _Title,
          timer: 1000,
          type : _Type,
        }).then((result)=>{                                       
           var table=$('#aprobacionclientes').DataTable();
          table.ajax.reload(); 
        });  
                    
      }
    });
  }

      });     

// Esta parte acaba la funcion que desaprueba?????
$('table#aprobacionclientes tbody').on( 'click', 'button.Editar', function () {
  //alert('algo');ç
  var table=$('#aprobacionclientes').DataTable();
    var D =  table.row($(this).parents("tr")).data();
    var iafiliado=D.numafiliado;
    $("#iafiliado").val(iafiliado);
    // 
    var codbanco=D.codbanco;
    $("#codbanco").val(codbanco);
    // 
    var consecutivo=D.consecutivo;
    $("#consecutivo").val(consecutivo);
   // alert(codbanco);    //alert(iafiliado);
// 
if (codbanco == "Ubiipago"){
$.ajax({
          url : "view/Administracion/verificacion_documentos/verExpedientes.php",
          type : "POST",
          data : {"iafiliado":iafiliado, "codbanco":codbanco},
          success: function(data){
          // alert(data);
          $("#tablaa").html(data)
              }

  });
  // 
}else{

  $.ajax({
          url : "view/Administracion/verificacion_documentos/verExpedientes.php",
          type : "POST",
          data : {"consecutivo":consecutivo},
          success: function(data){
          // alert(data);
          $("#tablaa").html(data)
              }

  });
  // 
}

  });
}
// final
  
  </script>
<!--  -->
  
<div class="modal fade" id="ModalExp22" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static"  
  data-keyboard="false">  
    <div class="modal-dialog modal-lg" role="document">  
      <div class="modal-content">  
        <div class="modal-header yellow">  
          <button type="button" class="btn btn-danger btn-circle btn-lg f" data-dismiss="modal" aria-label="Close">  
            <span aria-hidden="true">&times;</span>  
          </button>  
        </div>  
        <div class="modal-body">  
          <div class="panel-body">  
            <!-- <input type="text" id="id_cliente" class="hidden" value="<?php //echo $coddocumento; ?>">  
            <input type="text" id="id_registro" class="hidden" value="<?php //echo $id_registro; ?>">   -->
<!--  -->
            <div class="col-lg-14">  
              <div class="panel panel-primary">  
                <!-- <div class="panel-heading" style="text-align: center;color:#ffffff;">  
                   <h4>Historico de subida de Expedientes Ubii:</h4>  
                </div>   -->
                <div class="panel-body">  
                 <div id="tablaa"></div>
                </div> <!-- End panel-body -->  
              </div>  
            </div>  
          </div>  
        </div>  
      </div>  
    </div> <!-- End div modal -->  
  </div>  



</div>  
         </div>

       </div>

  </div>