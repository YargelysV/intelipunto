<?php
  $facturacion=$_SESSION["administracion"];
  $usuario=$_SESSION["session_user"];
  $ejecutivoin=$_SESSION["ejebanco"];
  $tipousuario=$_SESSION["codtipousuario"];
  $coordinador=$_SESSION["coordbanco"];
  $coordinadordos=$_SESSION["coordinadordos"];
  // $cliente = isset($array[4]) ? $array[4] : "";
  //$fecha = isset($array[5]) ? $array[5] : "";

  $controladorc =new controladorAdjuntar();
  $comerciali=$controladorc->spverclientefacturacionpagos($usuario, $ejecutivoin, $tipousuario, $coordinador,$coordinadordos);
  $databanco = pg_fetch_assoc($comerciali);
  $count = pg_num_rows($comerciali);

  if ($count==0) {  ?>
?>

<script type="text/javascript">
redirectResult();
</script>

<?php } else { ?>
  
<style type="text/css">
  .Pago-Confirmado{
    
    color:white;
    background:#01DF3A;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

  .Pendiente-Por-Pagos{
     
    color:#f1f1f1;
    background:orange;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

  .Pendiente-Por-Confirmar{
    
    color:#f1f1f1;
    background:red;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container; 

  }

  .Inteliservices{
    
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

    .Cliente-Inteligensa{
    
    color:#f1f1f1;
    background:#62ABCD;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }
    .Migracion{
    
    color:#f1f1f1;
    background:#C94E71;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

    .Comodato{
    
    color:#f1f1f1;
    background:#BA55D3;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;
    }

</style>
<input type="hidden" id="fechadetalle" name="fechadetalle" value="<?php echo $fechadetalle; ?>">
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>">
<input type="hidden" id="namearchivo" name="namearchivo" value="<?php echo $namearchivo; ?>">
<input type="hidden" id="databanco" name="databanco" value="<?php echo  $databanco['ibp']; ?>">
<input type="hidden" id="masiva" name="masiva" value="<?php echo  $masiva; ?>">
<input type="hidden" id="permisos" name="permisos" value="<?php echo  $facturacion; ?>">
<input type="hidden" id="tipousuario"  name="tipousuario"  value="<?php echo $tipousuario; ?>" />
<input type="hidden" id="ejecutivo"  name="ejecutivo"  value="<?php echo $ejecutivoin; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="coordinador"  name="coordinador"  value="<?php echo $coordinador; ?>" />
<input type="hidden" id="coordinadordos"  name="coordinadordos"  value="<?php echo $coordinadordos; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-clipboard" style="color:#F8F9F9;"> </i> Facturación POS: Búsqueda Detallada  </a>
        </div>
       </nav>
        <b style="color:#5DADE2;">Búsqueda: <?php echo $databanco['ibp'];?></b>
       <br>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="busquedafactpos" style="width: 100%"> 
           <thead>
              <tr>
                <th>correlativo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;Pago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>enlace</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Acción&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;del&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Ejecutivo Asignado</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;Estatus&nbsp;Cliente&nbsp;</th>
                <th>N°&nbsp;Afiliado</th>
                <th>Tipo de Pos</th>
                <th>Cantidad de Pos</th>
                <th>Fecha de Recepción</th>
                <th>Fecha de Carga</th>
                <th>Nombre Archivo</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Banco</th>
              </tr>
            </thead>
            <tfoot>
            
                <th colspan="4" style="text-align:left">Total:</th>
            
          </tfoot>
        </table>
      </div>
    </div>

    </div>
  </div>
</div>
<?php  } ?>

<script type="text/javascript">
  function dtCore(){
  tipousuario= document.getElementById('tipousuario').value;
  ejecutivo= document.getElementById('ejecutivo').value;
  usuario= document.getElementById('usuario').value;
  coordinador= document.getElementById('coordinador').value;
  coordinadordos= document.getElementById('coordinadordos').value;
  //alert(namearchivo);
    //api/selectComercializacionResultadoGestion.php?"id_registro=12348&namearchivo=100X100BANCO_10052019_001.CSV


  $('table#busquedafactpos').DataTable({
    "ajax": {
    "url": "api/SelectPagosXConfirmar.php",
          "type": "POST",
          "data": {"tipousuario":tipousuario,"ejecutivo":ejecutivo,
          "usuario":usuario,"coordinador":coordinador, "coordinadordos":coordinadordos}
    },
    "columns": [
      {"data": "correlativo", className: "text-center"},
      {"data": "estatuspago", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data == "1" || data === "1") {
        return "<p class='Pago-Confirmado'>Pago Confirmado</p>";
      }else if (data == "2" || data === "2"){
        return "<p class='Pendiente-Por-Pagos'>Pendiente Por Pagos</p>";
      }else if (data == "0" || data === "0"){
        return "<p class='Pendiente-Por-Confirmar'>Pendiente Por Confirmar</p>";
      }else{
        return "";
      }
      }
  },
      {"data": "enlaceafiliado" },
      {"defaultContent":"<a class='editar' type='href' title='Ver'>Datos Facturación</a>", className: "text-center"},
      {"data": "descequipo", className: "text-center" },
      {"data": "ejecutivo", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "estatusmigra", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if       (data == "0" || data === "0" || data === "" ||  data == "" || data==null) {
        return "<p class='Cliente-Inteligensa'>Cliente Inteligensa</p>";

      }else if (data == "1" || data === "1"){
        return "<p class='Migracion'>Migración</p>";

      }else if (data == "2" || data === "2"){
        return "<p class='Comodato'>Comodato</p>";

      }else{
        return "";
      }} },
      {"data": "nafiliacion", className: "text-center" },
      {"data": "tipopos"},
      {"data": "cantidadterminales"},
      {"data": "fecharecepcion", className: "text-center" },
      {"data": "idate", className: "text-center" },
      {"data": "namearchivo", className: "text-center" },
      {"data": "direccion"},
      {"data": "ibp"}
    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
           {
                 "targets": [ 2,15 ],
                 "visible": false,
             }
         ],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',        
         
        exportOptions : {
        columns: [ 0, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
      }
    }],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      

  });

  $('table#busquedafactpos tbody').on( 'click', 'a.editar', function () {
  //alert('algo');
        var table=$('table#busquedafactpos').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        var enlaceafiliado=D.enlaceafiliado;
        var estatusmigra=D.estatusmigra;
        var url = "facturacionpos?cargar=facturarcliente&var="+enlaceafiliado+'/'+estatusmigra; 
        $(location).attr('href',url);

  });
}
  </script>

