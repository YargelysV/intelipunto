<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $cliente= isset($array[0]) ? $array[0] :0;
  $fecha= isset($array[1]) ? $array[1] :"";

  $controladortrans =new controladorAdjuntar();
  $trans=$controladortrans->usuario($usuario);
  $row=pg_fetch_assoc($trans);

  $controladorclient =new controladorAdjuntar();
  $comercia= $controladorclient->spverarchivodiariocliente($cliente, $fecha);
  $databanco = pg_fetch_assoc($comercia);
?>

 
 <input type="hidden" id="cliente"  name="cliente"  value="<?php echo $cliente; ?>" />
 <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
 <input type="hidden" id="nombre" name="nombre" value="<?php echo $var; ?>"/>
 
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
          <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard" style="color:#F8F9F9;"> </i>  DATA CLIENTES FACTURACIÓN. Cliente: <?php echo $databanco['nbanco'];?></a>
           <br>
         </div>
       </nav>
    </div>
    <?php if (($row['area']=='2')|| ($_SESSION["superusuario"]=='A')) {?>
      <div align="left">
       <input class="btn btn-success" type="button" name="generar" value="Generar Archivo CLIENTE (.Mac)" onclick="generararchivo(eliminararchivo)" />
    </div>
    <div align="left">
      <a  class="btn btn-success" id="descarga" style="display: none" href="Descargas/CLIENTES<?php echo $cliente ?>.Mac" download="CLIENTES<?PHP echo $cliente; ?>.Mac">Generar Archivo CLIENTE (.Mac) </a>
    </div> 
    <?php } ?>

    <div class="row" align="center">
      <div class="demo-container" ng-app="AppClienteFacturacion" ng-controller="ClientesFacturacionController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div>
      <div class="row">
        <div>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='preparacionCliente.php'"/>
        </div>
      </div>
    </div>
  </div>
</div>
  
