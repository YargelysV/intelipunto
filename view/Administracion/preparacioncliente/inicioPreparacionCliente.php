<?php
if (isset($_SESSION["session_user"]))
{
$usuario=$_SESSION["session_user"];
$_SESSION["ESTATUS"]=0;
?>

  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-file-text-o" style="color:red;"> </i> Data Clientes Facturación</h1>
        </div>

        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-yellow">
              <div class="panel-heading">
              Preparaci&oacute;n de la Data de los Clientes a Facturar
              </div>
              <div class="panel-body">
                <div>
                  <div class="col-lg-5">Banco:</div>
                  <div class="col-lg-5">
                    <select name="clientes" id="clientes" required>
                    <option value="">Seleccione Banco</option>
                    </select>
                  </div>
                </div>
                <div>
                    <div class="col-lg-5">Fecha de Recepci&oacute;n:</div>
                    <div class="col-lg-5">
                      <select name="fecha" id="fecha" required>
                      <option value="">Seleccione Fecha</option>
                      </select>
                    </div>
                </div>
              </div>
               <input type="hidden" id="valor" value="6" name="valor"/>
              <script languaje="javascript">
              generar();
              </script>
            <div class="panel-footer">
              <p style="text-align: center;">
              <input class="btn btn-success" type="button" name="Buscar" value="Buscar" onClick="buscarpreparacioncliente()"/>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
else{
  header("Location:../salir");
}
?>

