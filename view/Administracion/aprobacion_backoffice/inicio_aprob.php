<?php
//  session_start();
$tituloPagina=$_SESSION["page"];


if (isset($_SESSION["session_user"]))
{
    $usuario=$_SESSION["session_user"];
    $ctrlcaptacion = new controladorPagosBasico();
    $registros=$ctrlcaptacion->sp_registrosporaprobar();
    $cantidad = pg_num_rows($registros);
}

?>
<style type="text/css">
  #grid {
    height: 440px;
}
#gridContainer .dec .diff {
    color: #f00;
}
.options {
    padding: 20px;
    margin-top: 20px;
    background-color: rgba(191, 191, 191, 0.15);
}

.caption {
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
}

.option {
    margin-bottom: 10px;
}

.option > span {
    position: relative;
    top: 2px;
    margin-right: 10px;
}

.option > .dx-widget {
    display: inline-block;
    vertical-align: middle;
}

#requests .caption {
    float: left;
    padding-top: 7px;
}

#requests > div {
    padding-bottom: 5px;
}

#requests > div:after {
    content: "";
    display: table;
    clear: both;
}

#requests #clear {
    float: right;
}

#requests ul {
    list-style: none;
    max-height: 100px;
    overflow: auto;
    margin: 0;
}

#requests ul li {
    padding: 7px 0;
    border-bottom: 1px solid #dddddd;
}

#requests ul li:last-child {
    border-bottom: none;
}
.diff{

  background: red;
  color: green;
}

.Por-asignar{
    
    color:white;
    background:RED;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }

.radio-btn {
  margin-right: 10px;
  font-size: 16px;
  color: #FC2F1E;
}

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #46F516;
}

input:focus + .slider {
  box-shadow: 0 0 1px #46F516;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button 
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

 On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #EE2E14;
}


</style>

<!-- Page Content -->
 <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">  
 <input class="hidden" id="banco" name="banco" value="<?php echo $banco; ?>">  
 <input class="hidden" id="areausuario" name="areausuario" value="<?php echo $_SESSION["area_usuario"]; ?>"> 

   <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12"><h1> <i class="fa fa-check-square-o " style="color:#045FB4">Clientes por Aprobar</i></h1> </div>
     
<div class="row" >
      <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-primary" align="center">
              <div class="panel-heading panel-primary" style="">
            <i class="fa fa-usser fa-fw" ></i> Registros
          </div>
          <br>  
<input type="hidden" name="idregistros" value="" id="idregistros" class="form-control" placeholder="" tabindex="3" >
    <?php if ($cantidad!=0) { ?>

          <div class="col-12">
        <table class="table table-bordered"  id="aprobacionclientes"> 
           <thead>
              <tr>
                
                <th>N°</th>
                <th>ID</th>
                <th>Aprobación</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Correo</th>                
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Nº&nbsp;&nbsp;Afiliación&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Nº&nbsp;&nbsp;Terminal&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Módelo&nbsp;&nbsp;de&nbsp;&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nº&nbsp;&nbsp;Factura</th>
                <th>Serial&nbsp;del&nbsp;Equipo</th>  

              </tr>
            </thead>
        </table>
 
      </div>

        </div>
    </div>

          <?php   } else{ ?>

        <div class="alert alert-danger" class="col-md-10">
                               Actualmente no hay registros pendientes.
                            </div>

       <?php   } ?>




</div>  
         </div>

       </div>

  </div>


 
  <script type="text/javascript">
  function dtCore(){

  var banco= document.getElementById('banco').value;
  var areausuario= document.getElementById('areausuario').value;

  $('table#aprobacionclientes').DataTable({
    "ajax": {
    "url": "view/Administracion/aprobacion_backoffice/registrosxaprobarbanesco.php"
    },
    "columns": [

      {"data": "sec", className: "text-center"},
      {"data": "consecutivo", className: "text-center" },
      {"data": "idstatus", 
        render : function ( data, type, full, meta ) {
          //console.log(full['idregistro']);
          if ((data ==0 )){

              return '<label class="switch" id="switch"><input type="checkbox" id="estatus" value="1" class="check-aprobacion"><span class="slider round"></span></label><label class="switch" id="magic_switch" style="display: none;"><input type="checkbox" id="switch_check"><span class="slider round"></span>';
          }else 
            return '<label class="switch" disabled id="switch"><input checked type="checkbox" id="estatus" disabled value="0" class="check-aprobacion"><span class="slider round"></span></label><label class="switch" id="magic_switch" style="display: none;"><input type="checkbox" id="switch_check"><span class="slider round"></span>';
          
        }
      },
      //return '<input type="checkbox" class="check-buttons" checked value="0">',
      {"data": "razonsocial", className: "text-center" },
      {"data": "coddocumento", className: "text-center" },
      {"data": "tlf1", className: "text-center" },
      {"data": "correo", className: "text-center" },
      {"data": "codbanco", className: "text-center"},

      {"data": "numafiliado", className: "text-center" },
      {"data": "terminal", className: "text-center" },
      {"data": "descmodelo", className: "text-center"},
      {"data": "factura", className: "text-center" },
      {"data": "serialp", className: "text-center" }
      

    ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',
            exportOptions : {
              columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21] 
              }         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });


$('table#aprobacionclientes tbody').on( 'click', 'input.check-aprobacion', function () {
//alert('algo');
  var table=$('#aprobacionclientes').DataTable();
  var T =  table.row($(this).parents("tr")).data();
  var idregistro = T.consecutivo;
  var status = T.status;
  var usuariocarga = $("#usuario").val();
  var checks=$(this).val();
  console.log(checks);
  
  if (checks==1) {
    var estatus=1;
    Swal.fire({
      title: 'Desea realizar el cambio de estatus?',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Acepto!',
      cancelButtonText: 'No, cancelar!',
      }).then((result) => {
        if (result.value==true) {
          $.ajax({
            url : "view/Administracion/aprobacion_backoffice/guardarestatus.php",
            type : "POST",
            data : {"idregistro":idregistro,"usuariocarga":usuariocarga,"estatus":estatus},
            success: function(data){
              if(data==1){
                _Text = "El estatus ha sido actualizado";
                _Type = "success";
                  Swal.fire({
                    text : _Text,
                    timer: 1000,
                    type : _Type,
                    onBeforeOpen: function (){
                      swal.showLoading()
                    }
                  }).then((result)=>{
                    var table=$('#aprobacionclientes').DataTable();
                    table.ajax.reload(); 
                  });
              }else
                swal(data);
            }
        })
          .fail(function(){
          swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
        });
      } else {
        _Title = "Cancelado!";
        _Text = "La actualización ha sido cancelado";
        _Type = "success";
        Swal.fire({
          text : _Text,
          title: _Title,
          timer: 1000,
          type : _Type,
        }).then((result)=>{                                       
           var table=$('#aprobacionclientes').DataTable();
          table.ajax.reload(); 
        });              
      }
    });
  }else
    var estatus=0;
    Swal.fire({
      title: 'Desea realizar el cambio de estatus?',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Acepto!',
      cancelButtonText: 'No, cancelar!',
      }).then((result) => {
        if (result.value==true) {
          $.ajax({
              url : "view/Administracion/aprobacion_backoffice/guardarestatus.php",
              type : "POST",
              data : {"idregistro":idregistro,"usuariocarga":usuariocarga,"estatus":estatus},
              success: function(data){
                if(data==1){
                  _Text = "El estatus ha sido actualizado";
                  _Type = "success";
                    Swal.fire({
                      text : _Text,
                      timer: 1000,
                      type : _Type,
                      onBeforeOpen: function (){
                        swal.showLoading()
                      }
                    }).then((result)=>{
                      var table=$('#aprobacionclientes').DataTable();
                      table.ajax.reload(); 
                    });
                }else
                  swal(data);
              }
          })
            .fail(function(){
            swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
          });
        } else {
          _Title = "Cancelado!";
          _Text = "La modificación ha sido cancelado";
          _Type = "success";
          Swal.fire({
            text : _Text,
            title: _Title,
            timer: 1000,
            type : _Type,
          }).then((result)=>{                                       
             var table=$('#aprobacionclientes').DataTable();
            table.ajax.reload(); 
          });              
        }
      });
});


}
  </script>

