<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] :"";
  $fecha= isset($array[1]) ? $array[1] :"";
  //isset($_GET['cargar']) ? $_GET['cargar'] : ""
  //busqueda generalizada por  banco
  $controladorclient =new controladorClientePotencial();
  $comercia= $controladorclient->sp_buscarclienterecibidos($banco, $fecha);
  $databanco = pg_fetch_assoc($comercia);
?>
 
 <input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
 <input type="hidden" id="fecha"  name="fecha"  value="<?php echo $fecha; ?>" />
 
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#C70039;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9; margin-right:200px;">  <i class="fa fa-cloud-upload" style="color:#F8F9F9;"> </i> CLIENTES - Busqueda General: </a>
         <br> <b style="color:#5DADE2;"> Búsqueda: <?php if( $banco!='') echo $databanco['ibp']; else  echo $fecha; ?></b>
        </div>
       </nav>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="busquedageneral" style="width: 100%"> 
          <thead>
            <tr>
              <th>enlace</th>
              <th>Consecutivo</th>
              <th>Acción</th>
              <th>codigobanco</th>
              <th>Banco</th>
              <th>Fecha de Recepción</th>
              <th>usuario</th>
              <th>Fecha de Carga</th>
              <th>Nombre del Archivo</th>
              <th>Cant. de Registros</th>
              <th>Cant. de POS</th>
            </tr>
          </thead>
          <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>            
          </tfoot>
        </table>
      </div>
    </div>
    

        <div class="col-md-5 col-md-offset-5">
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='ClientesPotenciales.php'"/>
        </div>
       
    
          <!--div align="right">
                <a href="view/ClientePotencial/recepcion/reporte_clientes_pdf?cliente=<?php// echo  $banco;?>" target="_blank"><img src="img/pdf.png" height="50" width="60">  <b style="color:red;">Descargar</b> </a> 
          </div-->
    
  </div>
</div>
<script type="text/javascript">
  function dtCore() {
    var banco =$("#banco").val();
    var fecha =$("#fecha").val();
    //alert(banco); 
    $('#busquedageneral').DataTable({
    "bDeferRender": true,
    "sPaginationType": "full_numbers",

    
    "ajax": {
    "url": "api/selectClientesPotenciales.php",
          "type": "POST",
          "data": {"banco":banco,"fecha":fecha}
    },
    "columns": [
    {"data": "enlace" },
    {"data":"correlativo", className: "text-center"},
    {"defaultContent":"<a class='editar' type='href' title='Ver'>Ver Detalle</a>", className: "text-center"},
    {"data": "codigobanco" },
    {"data": "ibp", className: "text-center" },
    {"data": "fecharecepcion", className: "text-center" },
    {"data": "usuario", className: "text-center" },
    {"data": "fechacarga", className: "text-center" },
    {"data": "namearchivo" },
    {"data": "sizearchivo", className: "text-center" },
    {"data": "cantidad", className: "text-center" }
    ],
    "order" : [[1, "asc"]],
    "columnDefs": [
             {
                 "targets": [ 0, 3, 6, 10 ],
                 "visible": false,
             }
         ],

      "scrollX": true,
      "scrollY": false,
      "info":     false,
      "scrollCollapse": false,

      dom: 'Bfrtip',
      buttons: [
       'excel'
      ],
      "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },

      language:{
        "sProcessing":     "Procesando...",
         "sLengthMenu":     "Mostrar _MENU_ registros",
         "sZeroRecords":    "No se encontraron resultados",
         "sEmptyTable":     "Ningún dato disponible en esta tabla",
         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix":    "",
         "sSearch":         "Buscar:",
         "Print":           "Imprimir",
         "sUrl":            "",
         "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
         "oPaginate": {
           "sFirst":    "Primero",
           "sLast":     "Último",
           "sNext":     "Siguiente",
           "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }

    });
    $('table#busquedageneral tbody').on( 'click', 'a.editar', function () {
    //alert('algo');
          var table=$('table#busquedageneral').DataTable();
          var D =  table.row($(this).parents("tr")).data();
          var enlace=D.enlace;
          var url = "clientesPotenciales?cargar=buscarrecepcion&var="+enlace; 
          $(location).attr('href',url);

    });
}
  </script>