<?php
set_time_limit(0);
//session_start();
//error_reporting(0);
//para que el usuario no ingrese por la url
$read = fopen($_SERVER['HTTP_REFERER'], "r") or die(header("Location:../../../salir")); 

require_once('../../../inc/tcpdf/tcpdf.php');
require("../../../controller/ControladorClientePajax.php");


	$var=($_REQUEST['var']);
	$array = explode("/",$var);
	$banco= $array[0];
	$fechadetalle=$array[1];
	$masiva = isset($array[2]) ? $array[2] : "";
  	$namearchivo = isset($array[3]) ? $array[3] : "";

	$controladortrans =new controladorClientePotencial();
	$trans=$controladortrans->verclientepotencial($banco,$fechadetalle, $namearchivo);
	
	$controladortranscliente =new controladorClientePotencial();
	$trans1=$controladortranscliente->verclientepotencial($banco,$fechadetalle, $namearchivo);
	
	$cantidad_total = pg_num_rows($trans1);
	$clientes = pg_fetch_assoc($trans1);
	$ncliente=$clientes['ibp'];
	$fecharec=$clientes['fecharecepcion'];
	$fechacarga=$clientes['idate'];
	$fecha= date("j-n-Y");
	
// create new PDF document
//ob_clean();
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
ob_clean(); 
// set document information
$pdf->SetCreator(PDF_CREATOR);


class XTCPDF extends TCPDF {
	//Cabecera de página
	function Header()
	{
	$this->setJPEGQuality(75);
	//image(posicion izq-der, arr-ab, ancho, alto)
	$this->Image('../../../img/Intelipunto.jpg',10,10, 50,40, 'JPG','', '', true, 150, '', false, false, 0, false, false, false);
	}
	
	//Pie de página
function Footer()
{
$this->tbl_pie = <<<EOD
<!--table border="0" width="750">
	<tbody>
		<tr>
			<td  class="hidden" align="left" width="250"><font size="8">03-GRA-059/Versi&oacute;n 1. </font></td>
			<td  class="hidden" align="right" width="250"><font size="8">RIF: J-00291615-0  </font> </td>
		</tr>
	</tbody>
</table-->
EOD;

$this->SetY(-50); 
$this->SetX(-200); 
$this->writeHTML($this->tbl_pie, true, false, false, false, '');

	//Posición: a 1,5 cm del final
		$this->SetY(-15); 
		//Arial italic 8
		$this->SetFont('helvetica','I',8);
		//Número de página
		$this->Cell(0,4,'Página '.$this->getAliasNumPage().' de '. $this->getAliasNbPages(),0,0,'C'); 
	}
}

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf=new XTCPDF();


//encabezado/
$tbl_encabezado = <<<EOD
	<table border="0" align="center" width="750">
		<tr>
			<td height="20" colspan="2"><strong> <font size="14">  $ncliente </font> </strong></td>
		</tr>
		<tr>
			<td height="20" colspan="2" width="750">
			<font size="12"> <strong>Detalles Clientes Fecha de Recepción: $fecharec  </strong> </font>
			</td>
		</tr>
		<tr>
			<td height="20" colspan="2" width="750">
			<font size="12"> <strong>Fecha de Carga en el Sistema: $fechacarga  </strong> </font>
			</td>
		</tr>
	</table>
EOD;


//CABECERA TABLA/
$tbl_head = <<<EOD
<table border="1" width="750">
	<tr>
		<td align="center" width="30">N&deg;</td>
		<td align="center" width="80">Tipo de Pos</td>
		<td align="center" width="80">Cantidad de Pos</td>
		<td align="center" width="229">Razón Social</td>
		<td align="center" width="80">Rif / CI</td>
		<td align="center" width="90">Nº Afiliado</td>
		<td align="center" width="150">Nombre del Archivo</td>
		

	</tr>	
</table>	
EOD;

// PAGINA 1
$pdf->AddPage('L','A4');  //CARTA
$pdf->SetMargins(0, 0, 0); 
$pdf->SetLeftMargin(15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->Ln(15);
$pdf->SetFont('times','B',10);
$pdf->writeHTML($tbl_encabezado, true, false, false, false, '');
$pdf->Ln(10);
$pdf->writeHTML($tbl_head, true, false, false, false, '');
$pdf->Ln(-4.5);

//Parametros para las funciones Cell () y MultiCell()
    $fill = 1;
    $border=1;
    $ln=0;
    $fill = 0;
    $align='T';
    $link=0;
    $stretch=0;
    $ignore_min_height=0;
    $calign='T';
    $valign='T';
  //  $height=6;//alto de cada columna

//cuerpo
$registros=0;	
$controlhoja=0;	
while($row = pg_fetch_array($trans)):
	
	if ($registros==$controlhoja+7)
	{
	$controlhoja=$registros;
	$pdf->AddPage('L','A4');
	$pdf->SetMargins(0, 0, 0); 
	$pdf->SetLeftMargin(15);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->Ln(25);
	$pdf->SetFont('times','B',10);
	$pdf->writeHTML($tbl_encabezado, true, false, false, false, '');
	$pdf->Ln(10);
	$pdf->writeHTML($tbl_head, true, false, false, false, '');
	$pdf->Ln(-4.5);
	}	
	

	$ancho=63.5;
    $altura=16;
    $cantidad_lineas= strlen($row['direccion']);
    if($cantidad_lineas > $ancho)
    {
        $cant_espacios = $cantidad_lineas/$ancho;
        $rendondear=round($cant_espacios,2);
        $altura=$altura*$rendondear;
    }//Fin del iff

	$registros++;
	$pdf->SetFont('times','B',10);
	$pdf->Cell(10.6,16,$registros,1,0,'C');  
	$pdf->Cell(28.2,16,$row['tipopos'],1,0,'C');
	$pdf->Cell(28.2,16,$row['cantidadterminales'],1,0,'C');
	$pdf->MultiCell(80.8,16,$row['razonsocial'], 1, 'C', 0, 0, '', '', true);
	$pdf->Cell(28.2,16,$row['coddocumento'],1,0,'C');
	$pdf->Cell(31.8,16,$row['nafiliacion'],1,0,'C');
	$pdf->Cell(52.8,16,$row['namearchivo'],1,0,'C');	
	//$pdf->MultiCell($ancho, $altura, $row["estado"],$border,$align,$fill,$ln);
	//$pdf->Cell(58.8,16,trim($row['direccion']),1,0,'C');	
	
	$pdf->Ln(16);

endwhile;


$pdf->Output('DetalleArchivosCargado-'.$ncliente.'-'.$fecharec.'.pdf', 'I');
ob_end_clean();

?>