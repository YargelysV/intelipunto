<?php
error_reporting(0);
set_time_limit(0);
if (isset($_SESSION["session_user"]))
{

 date_default_timezone_set('America/Caracas');
 $ahora = date("Y-n-j H:i:s");

function sanear_string($string)
{
 
    $string = trim($string);
 
 /*   $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );*/
 
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("¨", "º", "-", "~",
             "#", "|", "!", 
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":"),
        '',
        $string
    );
 
 
    return $string;
}

	$control = 0;
	if ($_SESSION["comprobar"]==1)
	{
		$control = 1;
		$_SESSION["size"]='';
		$_SESSION["name"]='';
		$_SESSION["type"]='';
	}

	if (isset($_REQUEST['error']))
	{
		$_SESSION['errores']=$_REQUEST['error'];
	}
	//if (isset($_POST['enviar']))
	//{
		$_SESSION['enviar']=$_FILES['archivo']['tmp_name'];

		$size=$_FILES['archivo']['size'];
		$name=$_FILES['archivo']['name'];
		$type=$_FILES['archivo']['type'];

		$hoy = date("dmY");
		$array = explode("_", $name);
		$nombrebanco= isset($array[0]) ? $array[0] : "";
		$fechaarchivo=isset($array[1]) ? $array[1] : "";
		$versiondia=isset($array[2]) ? $array[2] : "";

	//	$regexp = '/^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+ /i';

		//BUSCAR SI EL ARCHIVO YA FUE REGISTRADO CON ESOS NOMBRES
			$controladorarchivo =new controladorClientePotencial();
			$narchivo=$controladorarchivo->verarchivocargado($name);

		if ($nombrebanco=="" || $fechaarchivo=="" || $versiondia=="")
		{
			echo "<script>alertify.alert('El nombre del archivo es incorrecto, debe contener el formato \"Nombre de Banco_Fecha(DDMMAAAA)_Versión\" Ej: (BNC_".$hoy."_1) ', function () {
				window.location.assign('clientesPotenciales.php?cargar=adjuntar');});</script>";
		}
		else if ($fechaarchivo!=$hoy) {
			echo "<script>alertify.alert('El nombre del archivo es incorrecto, La fecha debe coincidir con el Dia de Hoy. Ej: (BNC_".$hoy."_1) ', function () {
				window.location.assign('clientesPotenciales.php?cargar=adjuntar');});</script>";
		}
		else if (($narchivo) && ($_SESSION['guardar']==0)) {
			echo "<script>alertify.alert('El archivo ya existe, verifique e intente nuevamente. Recuerde Cambiar la versión si es el mismo cliente Ej: (BNC_".$hoy."_2) ', function () {
				window.location.assign('clientesPotenciales.php?cargar=adjuntar');});</script>";
		}
		else{

			$date = date("Y-n-j H:i:s");
			$archivo = $_FILES['archivo']['tmp_name'];
			$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
			$cont=1;
			$control = 0;

			
			// validacion al intentar cargar otra vez el mismo archivo
		 	if($_SESSION["name"]==$name && $_SESSION["size"]==$size)
			{
		 		?>
				<!--script languaje="javascript">
				alertify.alert('Error Cargando el Archivo, el archivo que intenta cargar ya fue cargado en el Sistema. Verifique e intente nuevamente.');
				</script-->
				<?php
			}

			if($_SESSION["name"]<>$name && $_SESSION["size"]<>$size || $_SESSION['control']==1)
			{

				
				$_SESSION["size"]=$size;
				$_SESSION["name"]=$name;
				$_SESSION["type"]=$type;
		
				//BUSCAR TODOS LOS PREFIJOS DE LOS BANCOS
				$controladorbanco =new controladorClientePotencial();
				$prefijobanco=$controladorbanco->verprefijobanco();
				$pre = 0;
				$opcionesprefijo=array();
				while($prefijo = pg_fetch_array($prefijobanco)){
				$opcionesprefijo[$pre] = $prefijo['codigobanco'];
				//echo $opcionesprefijo[$pre]."<br>";
				$pre++;
				}

				$controladoract =new controladorClientePotencial();
				$actividadecon=$controladoract->veractividadeconomica();
				$act = 0;
				$opcionesactividad=array();
				while($actividad = pg_fetch_array($actividadecon)){
				$opcionesactividad[$act] = $actividad['desc_actividad'];
				//echo $opcionesprefijo[$pre]."<br>";
				$act++;
				}

				/*$controladoract =new controladorClientePotencial();
				$com=$controladoract->vercategoriacomercio();
				$categ = 0;
				$opcioncategoria=array();
				while($categoria = pg_fetch_array($com)){
				$opcioncategoria[$categ] = $categoria['desc_codigo'];
				//echo $opcionesprefijo[$pre]."<br>";
				$categ++;
				}

				$controladoract =new controladorClientePotencial();
				$val=$controladoract->verestatusdocumentos();
				$documentos = 0;
				$opcionestatusdoc=array();
				while($accion = pg_fetch_array($val)){
				$opcionestatusdoc[$documentos] = $accion['desc_estatus'];
				//echo $opcionesprefijo[$pre]."<br>";
				$documentos++;
				}

				$controladoract =new controladorClientePotencial();
				$sub=$controladoract->versubcategoriacomercio();
				$subcateg = 0;
				$opcionsubcategoria=array();
				while($subcategoria = pg_fetch_array($sub)){
				$opcionsubcategoria[$subcateg] = $subcategoria['desc_cod'];
				//echo $opcionesprefijo[$pre]."<br>";
				$subcateg++;
				}*/

				//BUSCAR TODOS LOS TIPOS DE POS
				$controladortipopos =new controladorClientePotencial();
				$tipopos=$controladortipopos->vertipodepos();
				$tip = 0;
				$opcionestipopos=array();
				while($pos = pg_fetch_array($tipopos)){
				$opcionestipopos[$tip] = $pos['tipopos'];
				//echo $opcionestipopos[$tip]."<br>";
				$tip++;
				}


				//BUSCAR DIRECCION
				$controladordireccion =new controladorClientePotencial();
				$tipodireccion=$controladordireccion->verificardireccion();
				$direccioni = 0;
				$opcionesdireccion=array();
				while($estatdireccion = pg_fetch_array($tipodireccion)){
				$opcionesdireccion[$direccioni] = $estatdireccion['direccion'];
				$direccioni++;
				}
				
				//BUSCAR TODOS LOS ESTADOS
				$controladorestados =new controladorClientePotencial();
				$tipoestados=$controladorestados->verestados();
				$estados = 0;
				$opcionestipoestados=array();
				while($estat = pg_fetch_array($tipoestados)){
				$opcionestipoestados[$estados] = $estat['estado'];
				$estados++;
				}

				//BUSCAR TODOS LOS MUNICIPIOS
				$controladormunicipio =new controladorClientePotencial();
				$tipomunicipios=$controladormunicipio->vermunicipios();
				$municipios = 0;
				$opcionestipomunicipios=array();
				while($municip = pg_fetch_array($tipomunicipios)){
				$opcionestipomunicipios[$municipios] = $municip['municipio'];
				$municipios++;
				}

				//BUSCAR TODAS LAS PARROQUIAS
				$controladorparroquia =new controladorClientePotencial();
				$tipoparroquias=$controladorparroquia->verparroquia();
				$parroquias = 0;
				$opcionestipoparroquias=array();
				while($parro = pg_fetch_array($tipoparroquias)){
				$opcionestipoparroquias[$parroquias] = $parro['parroquia'];
				//echo $opcionestipoparroquias[$parroquias]."<br>";
				$parroquias++;
				}

				//BUSCAR TODOS LOS TIPOS DE LINEA
			/*	$controladortipolinea =new controladorClientePotencial();
				$tipolinea=$controladortipolinea->vertipodelinea();
				$linea = 0;
				$opcionestipolinea=array();
				while($line = pg_fetch_array($tipolinea)){
				$opcionestipolinea[$linea] = $line['nameproveedor'];
				$linea++;
				}

				//BUSCAR TODOS LOS TIPOS DE NEGOCIO
				$controladortiponegocio =new controladorClientePotencial();
				$tiponegocio=$controladortiponegocio->vertipodemodelo();
				$negocio = 0;
				$opcionestiponegocio=array();
				while($nego = pg_fetch_array($tiponegocio)){
				$opcionestiponegocio[$negocio] = $nego['modelonegocio'];
				$negocio++;
				}
			*/
				//BUSCAR TODOS LOS TIPOS DE CLIENTE
				$controladortipocliente =new controladorClientePotencial();
				$tipocliente=$controladortipocliente->vertipocliente();
				$client = 0;
				$opcionestipocliente=array();
				while($clie = pg_fetch_array($tipocliente)){
				$opcionestipocliente[$client] = $clie['desctipocliente'];
				$client++;
				}	
				
				//BUSCAR SI EL REGISTRO ESTA REPETIDO (rif,banco,fechaenvio)
				$controladorclientesrep =new controladorClientePotencial();
				$clienterep=$controladorclientesrep->verclienterepetidos();
				$clientact = 0;
				$clienteactivos=array();
				while($cliea = pg_fetch_array($clienterep)){
				$clienteactivos[$clientact] = $cliea['codbanco'].$cliea['rif']/*.$cliea['modelo']*/.$cliea['est'].$cliea['mun'].$cliea['parr'];
				//echo $clienteactivos[$clientact]."<br>";
				$clientact++;
				}	
				//BUSCAR SI EL NUMERO DE AFILIADO EXISTE
				$controladorafiliado =new controladorClientePotencial();
				$nafiliado=$controladorafiliado->verafiliados();
				$tipafiliado = 0;
				$opcionesafiliado=array();
				while($posafiliado = pg_fetch_array($nafiliado)){
				$opcionesafiliado[$tipafiliado] = $posafiliado['nafiliacion'];
				//echo $opcionesafiliado[$tipafiliado]."<br>";
				$tipafiliado++;
				}
	
				//Busca los ejecutivos de venta
				$controladorejecutivo =new controladorClientePotencial();
				$nejecutivo=$controladorejecutivo->verejecutivos();
				$tipejecutivo = 0;
				$opcionesejecutivo=array();
				while($nameejecutivo = pg_fetch_array($nejecutivo)){
				$opcionesejecutivo[$tipejecutivo] = $nameejecutivo['nvendedor'];
				//echo $opcionesejecutivo[$tipejecutivo]."<br>";
				$tipejecutivo++;
				}


				?>
	
				<div class="sub-content" align="center" id="errores" style="display: none;">
					<div id="page-wrapper">
		           	  	<div class="container-fluid">
		          	    	<div class="row">

		          	    	<div class="col-lg-12">
	                       	<h1 class="page-header"><i class="fa fa-cloud-upload"></i> <b>Clientes Potenciales</b> </h1>
	                    	</div>
								<?php
								$cont=0;
								$date = date("Y-n-j H:i:s");
								$archivo = $_FILES['archivo']['tmp_name'];
								$fichero1 = fopen($archivo,"r") or die("problemas al abrir el archivo .txt");
								$control=0;
								while($data = fgetcsv($fichero1,10000,";"))
								{
									$cont++;
									if ($cont==2)
									{
										
											if ((utf8_encode(trim($data[0]))!="Consecutivo")||(utf8_encode(trim($data[1]))!="Nombre del Banco") ||(utf8_encode(trim($data[2]))!="Tipo de Cliente") ||(utf8_encode(trim($data[3]))!="Cantidad de Puntos de Venta")||(utf8_encode(trim($data[4]))!="Razón Social / Nombre del Afiliado") || (utf8_encode(trim($data[5]))!="Razón Comercial")||(utf8_encode(trim($data[6]))!="Rif/C.I.")||(utf8_encode(trim($data[7]))!="Actividad Económica de la Empresa")||(utf8_encode(trim($data[8]))!="Nombre del Representante Legal")||(utf8_encode(trim($data[9]))!="Correo Electrónico")||(utf8_encode(trim($data[10]))!="Telefono (1)")||(utf8_encode(trim($data[11]))!="Telefono (2)")||(utf8_encode(trim($data[12]))!="Dirección de Instalación") ||(utf8_encode(trim($data[13]))!="Estado") ||(utf8_encode(trim($data[14]))!="Municipio") ||(utf8_encode(trim($data[15]))!="Parroquia") ||(utf8_encode(trim($data[16]))!="Agencia") ||(utf8_encode(trim($data[17]))!="Afiliado")||(utf8_encode(trim($data[18]))!="Ejecutivo"))
												{
										echo " <script>alertify.alert('El orden del formato es incorrecto', function () {
										window.location.assign('clientesPotenciales.php?cargar=adjuntar');});</script>";	
										$control=2;
										}
									}

									
									else if (($cont>2) && ($control!=2))
									{	
										//if  

										//echo ($data[1]).($data[2]).($data[9]).($data[20]).($data[21]).($data[24])."<br>";
										//echo $data[3];
										if (($data[0])!=($cont-2)){
										echo '<h4 style="color:red;">'.'ERROR en campo "Correlativo" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}
                                          
						
										$_rif=$data[6];
										$_rif=ucwords($_rif);
									

										if((in_array(($data[1])/*.($data[3]).*/.($_rif).($data[13]).($data[14]).($data[15]), $clienteactivos)))
								
										{
										echo '<h4 style="color:red;">'.'ERROR  "Registro ya existe en nuestro sistema" en la linea '.$cont.' verifique la información e intente nuevamente. </h4>
											- Rif / C.I.: '.$data[6].'<br>
											- Razón Social / Nombre del Afiliado: '.$data[4].'<br>
											- Dirección:'.$data[12].' '.$data[13].' '.$data[14].' '.$data[15].'<br><br>
											';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}

										if((strlen(trim($data[0])) == '') && (($data[1])!=""))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Consecutivo" en la linea '.$cont.' debe ser un correlativo y no debe estar en blanco.</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}
										
										// if((!(preg_match ('/\d{1,2}(\/|-)\d{1,2}(\/|-)\d{4}/', $data[1]))) || (strlen($data[1]) == ''))

										// {
										// echo '<h4 style="color:red;">'.'ERROR en campo "Fecha de Envío del Archivo" en la linea '.$cont.', El formato debe ser de la siguiente manera: DD-MM-AAAA.</h4><br>';
										// $control = 1;	
										// $_SESSION["comprobar"]=$control;
										// }


										if((strlen(trim($data[1])) == '')  && (($data[0])!="")
										|| (!(in_array(($data[1]), $opcionesprefijo))))
										{ 
										echo '<h4 style="color:red;">'.'ERROR en campo "Prefijo del Banco" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

										if(($data[1]) == 134  || (($data[1])===134))
										{ 
										 echo '<h4 style="color:red;">'.'Error en la linea '.$cont.', los registros de Banesco se deben cargar en el módulo Captación de Clientes</h4><br>';
											//echo $data[2];

										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}
										/*if((
										(!(in_array(($data[3]), $opcionestipopos)))
										&& (($data[3])!='Fijo') && (($data[3])!='Inalambrico')&& (($data[3])!='') && (($data[3])!='LAN'))
										)
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Tipo de POS" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}*/
									
										if(((strlen(trim($data[2])) == '') &&  (($data[0])!=""))
											|| (!(in_array((trim($data[2])), $opcionestipocliente))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Tipo de Cliente" en la linea '.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}
										
										if((strlen(trim($data[3])) == '') && (($data[0])!="")||(strlen($data[3])>3))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Cantidad de Puntos de Venta" en la linea '.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}
										if((strlen(trim($data[4])) == '') && (($data[0])!="") || (strlen(trim($data[4]))<3 || strlen($data[4])>199))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Razón Social / Nombre del Afiliado" en la linea '.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;	
										}
									/*	if((strlen($data[7]) == ''))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Razón Comercial'.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;	
										}*/
										
										//echo $data[2];
											if((strlen(trim($data[6])) == '') || (!(preg_match("/^[JGVECP][0-9]{9}\b/",$data[6]))))
											{
											echo '<h4 style="color:red;">'.'ERROR en campo "Rif / C.I." en la linea '.$cont.'. Este debe contener una letra en mayuscula seguido de 9 numeros.</h4><br>';
											$control = 1;	
											$_SESSION["comprobar"]=$control;
											}
											
											if(((strlen(trim($data[7])) === '') &&  (($data[0])!==""))
											|| (!(in_array((trim($data[7])), $opcionesactividad))))
											{
											echo '<h4 style="color:red;">'.'ERROR en campo "Actividad Económica de la Empresa" en la linea '.$cont.'</h4><br>';
											$control = 1;	
											$_SESSION["comprobar"]=$control;
											}

										if((strlen(trim($data[8])) == '') && (($data[0])!="") || (strlen($data[8])>100))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Nombre del Representante Legal" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}
										if((strlen($data[9]) != '') && (!(preg_match("/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/",$data[9]))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Correo Electrónico" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}
										
										if((!(preg_match("/^([0-9]{4})(-)([0-9]{7})$/", $data[10]))) || (strlen($data[10]) == '') || (strlen($data[10])>14))
										{

										echo '<h4 style="color:red;">'.'ERROR en campo "Teléfono (1)" en la linea '.$cont.', El Teléfono debe ser de la forma (0000-0000000)</h4><br>';

										$control = 1;
										$_SESSION["comprobar"]=$control;
										}

										if(((strlen($data[11]) != null) && (!(preg_match("/^([0-9]{4})(-)([0-9]{7})$/", $data[11])))) || (strlen($data[11])>14)) 
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Telefono (2)" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

										if((strlen($data[12]) == '') && (($data[0])!="") || (strlen($data[12])>500))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Dirección de Instalación" en la linea '.
										$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}


										if((strlen($data[13]) !=null) && (($data[0])!="")
										 && (!(in_array(utf8_encode($data[13].' '.$data[14].' '.$data[15]), $opcionesdireccion))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Estado" "Municipio" y "Parroquia" no coinciden en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

										/*if((strlen($data[13]) == '') && (($data[0])!=""))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Dirección de Instalación" en la linea '.
										$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}*/

										if((strlen($data[13]) !=null) && (($data[0])!="")
										 && (!(in_array(utf8_encode($data[13]), $opcionestipoestados))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Estado" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}
										if((strlen($data[14]) !=null)  && (($data[0])!="")
										&& (!(in_array(utf8_encode($data[14]), $opcionestipomunicipios))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Municipio" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}
										
										if((strlen($data[15]) !=null)  && (($data[0])!="")
										&& (!(in_array(utf8_encode($data[15]), $opcionestipoparroquias))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Parroquia" en la linea '.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										} 

										/*if((strlen($data[17]) == ''))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Agencia" en la linea '.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}*/
										
										/*
										if((strlen($data[27]) == ''))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Nº de Afiliación" en la linea '.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}*/
										//echo $data[27]."hola";*/
									/*	if(((strlen($data[18]) != '')) && (($data[0])!=""))
										and ((in_array(($data[18]), $opcionesafiliado))))
										{
										echo '<h4 style="color:red;">'.'ERROR Nº duplicado en campo "Nº de Afiliación" en la linea '.$cont.' </h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}*/

									/*	if(strlen($data[18])>10)
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Nº de Afiliacion" en la linea '.$cont.'</h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}*/
										if(((strlen(trim($data[18])) == ''))
										|| (!(in_array(($data[18]), $opcionesejecutivo))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo Ejecutivo en la linea '.$cont.' </h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}
										/*if(((strlen($data[21]) !=null))
										&& (!(in_array(($data[21]), $opcionesejecutivo))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo Ejecutivo Call Center en la linea '.$cont.' </h4><br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}*/
										if((strlen(trim($data[21])) !=null)  && 
										(!(in_array(($data[21]), $opcionesprefijo))))
										{ 
										echo '<h4 style="color:red;">'.'ERROR en campo "Banco Asignado" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

									/*	if((($data[1]) == 100  || (($data[1])===100)) && (($data[19]) =='' || ($data[20]) =='')) 
										{											

										 echo '<h4 style="color:red;">'.'Error en la linea '.$cont.', debe agg inf</h4><br>';
											echo $data[1];

										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}

										if ((($data[1]) == 100  || (($data[1])===100)) && (!(in_array((trim($data[19])), $opcioncategoria))))
											{
											echo '<h4 style="color:red;">'.'ERROR en campo "Categoria de Comercio" en la linea '.$cont.'</h4><br>';
											$control = 1;	
											$_SESSION["comprobar"]=$control;
											} 

										if ((($data[1]) == 100  || (($data[1])===100)) && (!(in_array((trim($data[20])), $opcionsubcategoria))))
											{
											echo '<h4 style="color:red;">'.'ERROR en campo "Subcategoria de Comercio" en la linea '.$cont.'</h4><br>';
											$control = 1;	
											$_SESSION["comprobar"]=$control;
											} 

										/*if ((($data[1]) == 100  || (($data[1])===100)) && (!(in_array((trim($data[21])), $opcionestatusdoc))))
											{
											echo '<h4 style="color:red;">'.'ERROR en campo "Accion" en la linea '.$cont.'</h4><br>';
											$control = 1;	
											$_SESSION["comprobar"]=$control;
											} */	
									}
								}//fin while
								?>
									<table border="0" width="300" align="center">
										<tr>
											<td height="100" colspan="2" align="center">
											<font face="impact" size="5" color="red">
											Posibles Causas de error:<br>
											</font>
											</td>
										</tr>
											<td height="100" colspan="2" align="center">
											<font face="arial" size="4" color="green">
											- Verifique archivo .CSV según errores detallados<br>
											- Consulte con el administrador del Sistema.<br>
											 </font>
											</td>
										</tr>
										<tr>
											<td height="100" colspan="1" align="center">
											<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='clientesPotenciales?cargar=adjuntar'"/>
											</td>
										</tr>
									</table>
							</div>
						</div>
					</div>
				</div>		
				<?php //hasta aqui el div de error
							if($control == 0 && $_SESSION['control']!=1)
							{
								//echo "entro";
								$date = date("Y-n-j H:i:s");
								$archivo = $_FILES['archivo']['tmp_name'];
								$fichero2 = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
							
								$controlador1 =new controladorClientePotencial();
								$c=0;
								$duplicado=0;
								$contduplicados=0;
								while($data1 = fgetcsv($fichero2,10000,";"))
								{
									//echo "cccc";
									$c++;
								if ($c>2)
									{
									
									if($data1[13]!='')
									$estado=$data1[13];
									else
									$estado=0;	

									if($data1[14]!='')
									$municipio=$data1[14];
									else
									$municipio=0;	

									if($data1[15]!='')
									$parroquia=$data1[15];
									else
									$parroquia=0;	
							
									if($data1[21]!='')
									$bancoasig=$data1[21];
									else
									$bancoasig=$data1[1];	
							

										$insertar=$controlador1->insertar_tabltranscliente((trim($data1[0])),$ahora,(trim($data1[1])),utf8_encode(trim($data1[2])),utf8_encode(trim($data1[3])),utf8_encode(trim($data1[4])),utf8_encode(trim($data1[5])),utf8_encode(trim(sanear_string($data1[6]))),utf8_encode(trim(sanear_string($data1[7]))),utf8_encode(trim($data1[8])),utf8_encode(trim(sanear_string($data1[9]))),utf8_encode(trim(sanear_string($data1[10]))),utf8_encode(trim(sanear_string($data1[11]))),utf8_encode(trim($data1[12])),utf8_encode(trim(sanear_string($estado))),utf8_encode(trim(sanear_string($municipio))),utf8_encode(trim(sanear_string($parroquia))),utf8_encode(trim($data1[16])),utf8_encode(trim($data1[17])),utf8_encode(trim($data1[18])), $_SESSION["session_user"],$name,$size,$ahora,utf8_encode(trim($bancoasig)),utf8_encode(trim($data1[6])));

										if($insertar == 0){
										$duplicado=1;
										$contduplicados++;
								         
									         // break;
									  	}

									}
								}//fin del while
								$_SESSION["comprobar"]=0;
							}
							$_SESSION['control']=$control;	
			
			} //if del archivo
		//}//POST ENVIAR	


			
			      $_validacionrazonsocial= new controladorClientePotencial;
				  $val_raz_soc= $_validacionrazonsocial->validador_duplicidad();
		          $val=pg_num_rows($val_raz_soc);
		        

		$control = isset($control) ? $control : ""; 
		if($control == 1 && $_SESSION['control']==1)
		{
			?>
			<script languaje="javascript">
			errores();
			</script>
			<?php
		}
		
		else if(($control==0) || (($_SESSION['control']!=1) && ($control!=2)) )
		{		

	    		if ($duplicado==1) {

	    		if ($contduplicados==1) {
	    		echo"<script type='text/javascript'>alertify.error('Error, se encontró un ($contduplicados) registro duplicado, por favor verificar la data'); </script>";
	    				
	    			}else{	
				echo"<script type='text/javascript'>alertify.error('Error, se encontraron $contduplicados registros duplicados, por favor verificar la data'); </script>";
				}

				}
		        
                $controladortranscliente =new controladorClientePotencial();
			    $trans1=$controladortranscliente->vertblverificacionarchivo();
			    $data = pg_num_rows($trans1);

                //muestro toda la informacion de la tabla de transferencia
                $controladortranscliente =new controladorClientePotencial();
                $trans2=$controladortranscliente->vertablatransclientepotencial();

			    //BUSCAR la informacion de datos cargados para el mismo cliente en la ultima semana
				$controladorclientecargados =new controladorClientePotencial();
				$clientescargados=$controladorclientecargados->verclientepotencialcargados();
		        	
		    //   }else {
			// 	//BUSCAR la informacion de la tabla de transferencia
			// 	$controladortranscliente =new controladorClientePotencial();
			// 	$trans1=$controladortranscliente->vertablatransclientepotencial();

			// 	//BUSCAR la informacion de datos cargados para el mismo cliente en la ultima semana
			// 	$controladorclientecargados =new controladorClientePotencial();
			// 	$clientescargados=$controladorclientecargados->verclientepotencialcargados();
			// }
			?>
		
	        <div id="page-wrapper">
	            <div class="container-fluid">
	                <div class="row"-->
	                    <div class="col-lg-12">
	                     <?php if ($_SESSION['guardar']==0) {?>	
	                     	 <!-- si tiene filas duplicadas en el archivo-->
	                       	<h1 class="page-header"><i class="fa fa-cloud-upload"></i> <b>Clientes Potenciales</b></h1>
                          	<?php if ((($val==null) && ($duplicado==1)) || (($val==null) && ($data!=0))) { ?> 
	                        <div class="col-lg-12" style="text-align: center;">
	                        <h3 class="page-header" style="color:red;"><strong>Error, Los siguientes registros estan duplicados en el archivo </strong></h3>
	                        <?php if ($_SESSION['guardar']==0) {?>			
	                        <input name="enviar" class="btn btn-lg btn-warning" id="" type="button" value="Eliminar duplicados y cargar" onclick= "eliminarduplicadoscargar()"/>
	                        <hr>
	                       </div>
	                       
	                        <?php } ?>
	                         
	                        <?php  }

	                        if ($val!=null && $data==0) {?>	
	                        	 <!-- SI EL REGISTRO YA EXISTE EN EL SISTEMA-->
	                        	 <div class="col-lg-12" style="text-align: center;">
	                        <h3 class="page-header" style="color:red;"><strong>Error, intentas cargar registros que ya están en nuestra base de datos. </strong></h3>
	                        <?php if ($_SESSION['guardar']==0) {?>			
	                        <input name="enviar" class="btn btn-lg btn-warning" id="" type="button" value="Eliminar duplicados y cargar" onclick= "eliminarduplicadosbd()"/> 
	                        <input name="enviar" class="btn btn-lg btn-warning" id="" type="button" value="Cargar sin eliminar" onclick= "cargarduplicado()"/>
	                        <hr>
	                       </div>
	                       
	                        <?php } ?>
	                         
	                        <?php  } 
	                          if ($val!=null && $data!=0) {?>	
	                          	 <!-- REGISTROS EXISTENTES EN EL SISTEMA Y ADEMAS DUPLICIDAD DENTRO DEL ARCHIVO-->
	                        	 <div class="col-lg-12" style="text-align: center;">
	                        <h3 class="page-header" style="color:red;"><strong>Error, intentas cargar registros que ya están en nuestra base de datos, además existe duplicidad dentro de su archivo.
	                        	
	                        </strong></h3>
	                        <?php if ($_SESSION['guardar']==0) {?>			
	                        <input name="enviar" class="btn btn-lg btn-warning" id="" type="button" value="Eliminar solo duplicados en archivo y cargar" onclick= "eliminarduplicadoscargar()"/>
	                        <input name="enviar" class="btn btn-lg btn-warning" id="" type="button" value="Eliminar duplicados y cargar" onclick= "eliminarduplicados2cargar()"/>
	                        <hr>
	                       </div>
	                       
	                        <?php } ?>
	                         
	                        <?php  } else {  ?>

	                       
	                       <h3 class="page-header"><strong>Los siguientes datos ser&aacute;n cargados al sistema, &iquest;Desea proceder? </strong></h3>

	                       

                            <?php  } ?>
 
	                     <?php } else if ($_SESSION['guardar']==1) {?>
	                         <h1 class="page-header"><i class="fa fa-cloud-upload"></i> <b>Clientes Potenciales </b> </h1>
	                         <h3 class="page-header" style="text-align:center;background:#01df3a; color:white; border-top-left-radius: 0.5em; padding-top: 0.5em;"><strong>Datos procesados de manera exitosa!.</strong></h3>
	                     <?php } ?>
	                    </div>
                        
                        <!-- init registros duplicados -->
                              <?php 	if (($duplicado==1) or ($data!=0)) {  ?>
	                    <div class="row">
		            		 <div class="col-lg-12">
		            	 		<div class="panel panel-primary">
		                        	<div class="panel-heading" style="text-align: center;">
				                        <?php if ($_SESSION['guardar']==0  ) {?>	
				                        <h3 class="page-header"><strong>Resumen de Datos Duplicados en Archivo </strong></h3>
				                     	<?php } else if ($_SESSION['guardar']==1) {?>
				                        <h3 class="page-header"><strong>Resumen de Datos Cargados</strong></h3>
				                     	<?php } ?>
			                        </div>
		                       		 <!-- /.panel-heading -->
		                        	<div class="panel-body">
		                            	<div class="table-responsive">
			                                <table class="table table-striped table-bordered table-hover">
			                                    <thead>
			                                    <tr>
													<th>N&deg;</th>
													<th align="center">Fecha Recepción</th>
													<th align="center">Nombre de Banco</th>
													<th align="center">Tipo de Pos</th>
													<th align="center">Cantidad de Pos</th>
													<th align="center">Tipo de CLiente</th>
													<th align="center">Razón Social</th>
													<th align="center">Rif / CI</th>
													<th align="center">Afiliado</th>
													<th align="center">Dirección</th>
												</tr>
			                                    </thead>
			                                    <tbody>
			                                    	<?php  $TOTAL=0;
													$CONT=1;
													while($rows = pg_fetch_array($trans1)):	?>
													<tr>
														<?php $_SESSION['cliente']=$row['codigobanco'];  ?>
														<td align="center"><?php echo $CONT ?> </td>
														<td align="center"><?php echo $rows['fecharecepcion'];?> </td>
														<td align="center"><?php echo $rows['ibp'];?> </td>
														<td align="center"><?php echo $rows['tipopos'];?> </td>
														<td align="center"><?php echo $rows['cantidadterminales'];?> </td>
														<td align="center"><?php echo $rows['tipocliente'];?> </td>
														<td align="center"><?php echo $rows['razonsocial'];?> </td>
														<td align="center"><?php echo $rows['coddocumento'];?> </td>
														<td align="center"><?php echo $rows['afiliado'];?> </td>
														<td align="center"><?php echo $rows['est'].' '.$rows['mun'].' '.$rows['parr'];?> </td>
													</tr>
													<?php
													$CONT++;
													endwhile;
													?>
												</tbody>
			                                </table>
			                            </div>
		    	                    </div>
		        	            </div>
		        	        </div>
		            	</div> 
	<?php } ?>
		            	               <?php 	if ($val!=null)//si varible val es distinto de blanco existen duplicados
		            	                {  ?>
	                    	<div class="row">
		            		 <div class="col-lg-12">
		            	 		<div class="panel panel-primary">
		                        	<div class="panel-heading" style="text-align: center;">
				                        <?php if ($_SESSION['guardar']==0  ) {?>	
				                        <h3 class="page-header"><strong>Los siguientes registros ya existen en nuestro sistema.</strong></h3>
				                     	<?php } else if ($_SESSION['guardar']==1) {?>
				                        <h3 class="page-header"><strong>Resumen de Datos Cargados</strong></h3>
				                     	<?php } ?>
			                        </div>
		                       		 <!-- /.panel-heading -->
		                        	<div class="panel-body">
		                            	<div class="table-responsive">
			                                <table class="table table-striped table-bordered table-hover">
			                                    <thead>
			                                    <tr>
													<th>N&deg;</th>
													<th align="center">Fecha Recepción</th>
													<th align="center">Nombre de Banco</th>
													<th align="center">Tipo de Pos</th>
													<th align="center">Cantidad de Pos</th>
													<th align="center">Tipo de Cliente</th>
													<th align="center">Razón Social</th>
													<th align="center">Rif / CI</th>
													<th align="center">Afiliado</th>
													<th align="center">Dirección</th>
												</tr>
			                                    </thead>
			                                    <tbody>
			                                    	<?php  $TOTAL=0;
													$CONT=1;
													while($rows = pg_fetch_array($val_raz_soc)):	?>
													<tr>
														<?php $_SESSION['cliente']=$row['codigobanco'];  ?>
														<td align="center"><?php echo $CONT ?> </td>
														<td align="center"><?php echo $rows['fecharecepcion'];?> </td>
														<td align="center"><?php echo $rows['ibp'];?> </td>
														<td align="center"><?php echo $rows['tipopos'];?> </td>
														<td align="center"><?php echo $rows['cantidadterminales'];?> </td>
														<td align="center"><?php echo $rows['tipocliente'];?> </td>
														<td align="center"><?php echo $rows['razonsocial'];?> </td>
														<td align="center"><?php echo $rows['coddocumento'];?> </td>
														<td align="center"><?php echo $rows['afiliado'];?> </td>
														<td align="center"><?php echo $rows['est'].' '.$rows['mun'].' '.$rows['parr'];?> </td>
													</tr>
													<?php
													$CONT++;
													endwhile;
													?>
												</tbody>
			                                </table>
			                            </div>
		    	                    </div>
		        	            </div>
		        	        </div>
		            	</div> 
		            	<!-- end registros duplicados -->
		            	<?php } ?>

		   				<div class="row">
		            		 <div class="col-lg-12">
		            	 		<div class="panel panel-primary">
		                        	<div class="panel-heading" style="text-align: center;">
				                        <?php if ($_SESSION['guardar']==0  ) {?>	
				                        <h3 class="page-header"><strong>Resumen de Datos a Cargar Toda la Data</strong></h3>
				                     	<?php } else if ($_SESSION['guardar']==1) {?>
				                        <h3 class="page-header"><strong>Resumen de Datos Procesados</strong></h3>
				                     	<?php } ?>
			                        </div>
		                       		 <!-- /.panel-heading -->
		                        	<div class="panel-body">
		                            	<div class="table-responsive">
			                                <table class="table table-striped table-bordered table-hover">
			                                    <thead>
			                                    <tr>
													<th>N&deg;</th>
													<th align="center">Fecha Recepción</th>
													<th align="center">Nombre de Banco</th>
													<th align="center">Tipo de Pos</th>
													<th align="center">Cantidad de Pos</th>
													<th align="center">Tipo de CLiente</th>
													<th align="center">Razón Social</th>
													<th align="center">Rif / CI</th>
													<th align="center">Afiliado</th>
													<th align="center">Dirección</th>
													<th align="center">Ejecutivo</th>

												</tr>
			                                    </thead>
			                                    <tbody>
			                                    	<?php  $TOTAL=0;
													$CONT=1;
													while($row = pg_fetch_array($trans2)):	?>
													<tr>
														<?php $_SESSION['cliente']=$row['codigobanco'];  ?>
														<td align="center"><?php echo $CONT ?> </td>
														<td align="center"><?php echo $row['fecharecepcion'];?> </td>
														<td align="center"><?php echo $row['ibp'];?> </td>
														<td align="center"><?php echo $row['tipopos'];?> </td>
														<td align="center"><?php echo $row['cantidadterminales'];?> </td>
														<td align="center"><?php echo $row['tipocliente'];?> </td>
														<td align="center"><?php echo $row['razonsocial'];?> </td>
														<td align="center"><?php echo $row['coddocumento'];?> </td>
														<td align="center"><?php echo $row['afiliado'];?> </td>
														<td align="center"><?php echo $row['est'].' '.$row['mun'].' '.$row['parr'];?> </td>
														<td align="center"><?php echo $row['ejecutivo'];?> </td>
													</tr>
													<?php
													$CONT++;
													endwhile;
													?>
												</tbody>
			                                </table>
			                            </div>
		    	                    </div>
		        	            </div>
		        	        </div>
		            	</div>
                     
		            	<div class="row">
		            		 <div class="col-lg-12">
		            	 		<div class="panel panel-primary">
		                        	<div class="panel-heading" style="text-align: center;">
				                        <h3 class="page-header"><strong>Resumen Datos Cargados (Últimos 30 Días) </strong>
				                        </h3>
				                    </div>
		                       		 <!-- /.panel-heading -->
		                        	<div class="panel-body">
		                            	<div class="table-responsive">
			                                <table class="table table-striped table-bordered table-hover">
			                                    <thead>
			                                    <tr>
													<th>N&deg;</th>
													<th align="center">Fecha de Envío del Archivo</th>
													<th align="center">Fecha de Carga en el Sistema</th>
													<th align="center">Cantidad de Registros</th>
													<th align="center">Nombre de Archivo</th>
												</tr>
			                                    </thead>
			                                    <tbody>
			                                    	<?php  
													$contcargados=1;
													$rowfilas = pg_num_rows($clientescargados);
													if ($rowfilas>0)
													{
													while($rowcargados = pg_fetch_array($clientescargados)):	?>
													<tr>
														<td align="center"><?php echo $contcargados ?> </td>
														<td align="center"><?php echo $rowcargados['fechaenvio'];?> </td>
														<td align="center"><?php echo $rowcargados['fechacarga'];?> </td>
														<td align="center"><?php echo $rowcargados['cantidadtotal'];?> </td>
														<td align="center"><?php echo $rowcargados['archivo'];?> </td>
													</tr>
													<?php
													$contcargados++;
													endwhile;
													}
													else
													{
													?>
													<tr>
														<td align="center" colspan="5"> No existen registros para este cliente en los últimos 30 Días. </td>
													</tr>
													<?php 
													}
													?>
												</tbody>
			                                </table>
			                            </div>
		    	                    </div>
		        	            </div>
		        	        </div>
		            	</div>	

		   			 	<div class="row">
			     		  	<div class="col-md-5 col-md-offset-5">
					            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='clientesPotenciales.php?cargar=adjuntar'"/>
								<?php if (($_SESSION['guardar']==0) && ($data==0) && ($val==null)) {?>			
								<input name="enviar" class="btn btn-success" id="ocultocargar" type="button" value="Procesar" onclick= "cargar()"/>
								<?php } ?>
							</div>
		  				</div>
		 				<br>
	            	</div>
	        	</div>
	   		</div>
	    	<?php
		} //fin del if control
	}//validacion del nombre del archivo
}// session user
else{
	header("Location:salir");
}
?>
