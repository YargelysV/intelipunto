<?php
set_time_limit(0);
//session_start();
//error_reporting(0);
//para que el usuario no ingrese por la url
$read = fopen($_SERVER['HTTP_REFERER'], "r") or die(header("Location:../../../salir")); 

require_once('../../../inc/tcpdf/tcpdf.php');
require("../../../controller/ControladorClientePajax.php");


	$banco=$_REQUEST["cliente"];

	$controladortrans =new controladorClientePotencial();
	$trans=$controladortrans->sp_buscarclienter($banco);
	
	$controladortranscliente =new controladorClientePotencial();
	$trans1=$controladortranscliente->sp_buscarclienter($banco);
	
	$cont=0;
	$cantidad_total = pg_num_rows($trans1);
	$clientes = pg_fetch_assoc($trans1);
	$ncliente=$clientes['ibp'];
	$fecha= date("j-n-Y");
	
// create new PDF document
//ob_clean();
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
ob_clean(); 
// set document information
$pdf->SetCreator(PDF_CREATOR);


class XTCPDF extends TCPDF {
	//Cabecera de página
	function Header()
	{
	$this->setJPEGQuality(75);
	//image(posicion izq-der, arr-ab, ancho, alto)
	$this->Image('../../../img/Intelipunto.jpg',10,10, 50,40, 'JPG','', '', true, 150, '', false, false, 0, false, false, false);
	}
	
	//Pie de página
function Footer()
{
$this->tbl_pie = <<<EOD
<!--table border="0" width="750">
	<tbody>
		<tr>
			<td  class="hidden" align="left" width="250"><font size="8">03-GRA-059/Versi&oacute;n 1. </font></td>
			<td  class="hidden" align="right" width="250"><font size="8">RIF: J-00291615-0  </font> </td>
		</tr>
	</tbody>
</table-->
EOD;

$this->SetY(-50); 
$this->SetX(-200); 
$this->writeHTML($this->tbl_pie, true, false, false, false, '');

	//Posición: a 1,5 cm del final
		$this->SetY(-15); 
		//Arial italic 8
		$this->SetFont('helvetica','I',8);
		//Número de página
		$this->Cell(0,4,'Página '.$this->getAliasNumPage().' de '. $this->getAliasNbPages(),0,0,'C'); 
	}
}

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf=new XTCPDF();


//encabezado/
$tbl_encabezado = <<<EOD
	<table border="0" align="center" width="750">
		<tr>
			<td height="20" colspan="2"><strong> <font size="14">  $ncliente </font> </strong></td>
		</tr>
		<tr>
			<td height="20" colspan="2" width="750">
			<font size="12"> <strong>Archivos enviados hasta el dia $fecha  </strong> </font>
			</td>
		</tr>
	</table>
EOD;


//CABECERA TABLA/
$tbl_head = <<<EOD
<table border="1" width="750">
	<tr>
		<td align="center" width="50">N&deg;</td>
		<td align="center" width="100">Fecha de Recepción</td>
		<td align="center" width="100">Fecha de Carga en el Sistema</td>
		<td align="center" width="100">Usuario</td>
		<td align="center" width="300">Nombre del Archivo</td>
		<td align="center" width="100">N&deg; de Registros</td>
	</tr>	
</table>	
EOD;

// PAGINA 1
$pdf->AddPage('L','A4');  //CARTA
$pdf->SetMargins(0, 0, 0); 
$pdf->SetLeftMargin(15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->Ln(15);
$pdf->SetFont('times','B',10);
$pdf->writeHTML($tbl_encabezado, true, false, false, false, '');
$pdf->Ln(10);
$pdf->writeHTML($tbl_head, true, false, false, false, '');
$pdf->Ln(-4.5);

//cuerpo
$registros=0;	
$controlhoja=0;	
while($row = pg_fetch_array($trans)):
	
	if ($registros==$controlhoja+14)
	{
	$controlhoja=$registros;
	$pdf->AddPage('L','A4');
	$pdf->SetMargins(0, 0, 0); 
	$pdf->SetLeftMargin(15);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->Ln(25);
	$pdf->SetFont('times','B',10);
	$pdf->writeHTML($tbl_encabezado, true, false, false, false, '');
	$pdf->Ln(10);
	$pdf->writeHTML($tbl_head, true, false, false, false, '');
	$pdf->Ln(-4.5);
	}	

	$registros++;
	$cont+=$row['cantidad'];
	$pdf->SetFont('times','B',10);
	$pdf->Cell(17.6,9,$registros,1,0,'C');  
	$pdf->Cell(35.4,9,$row['fecharecepcion'],1,0,'C');
	$pdf->Cell(35.4,9,$row['fechacarga'],1,0,'C');
	$pdf->Cell(35,9,$row['usuario'],1,0,'C');
	$pdf->Cell(106,9,$row['namearchivo'],1,0,'C');
	$pdf->Cell(35.2,9,$row['cantidad'],1,0,'C');	
	$pdf->Ln(9);


endwhile;


$tbl_footer = <<<EOD
<table border="1">
	<tr>
		<td align="right" colspan="5" width="350"></td>
		<td align="center" colspan="1" width="300"><strong>TOTAL REGISTROS</strong></td>
		<td align="center" width="100"> $cont </td>
	</tr>
</table>
EOD;

$pdf->writeHTML($tbl_footer, true, false, false, false, '');
$pdf->Output('ArchivosCargados-'.$ncliente.'-'.$fecha.'.pdf', 'I');
ob_end_clean();

?>