<?php
if (isset($_SESSION["session_user"]))
{
$usuario=$_SESSION["session_user"];
$_SESSION["ESTATUS"]=0;

?>

<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
			 <h1 class="page-header"><i class="fa fa-cloud-upload"></i> Clientes </h1>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div  class="panel panel-info">
				    <div class="panel-heading">
				       Recepci&oacute;n Clientes
				    </div>
					<div class="panel-body">
						<div>
						<div class="col-lg-5">Banco:</div>
							<div class="col-lg-5">
								<select name="clientes" id="clientes" required>
								<option value="">Seleccione Banco</option>
								</select>
							</div>
							<br>
						</div>
						<div>
							<div class="col-lg-5">Fecha de Recepción:</div>
								<div class="col-lg-5">
									<select name="fecha" id="fecha" required>
									<option value="">Seleccione Fecha</option>
									</select>
								</div>
							</div>
						</div>


					<input type="hidden" id="valor" value="1" name="valor"/>
					<script languaje="javascript">
					generar();
					</script>

					<div class="panel-footer">
						<p style="text-align: center;">
						<input class="btn btn-success" type="button" name="Buscar" value="Buscar" onClick="buscarclientepotencial()"/>
						<?php if ($_SESSION["clientespotenciales"]=='W') {?>
						<input class="btn btn-warning" type="button" name="crear" value="Adjuntar Archivo" onClick="window.location.href='?cargar=adjuntar'"/>

						<?php if ($_SESSION["superusuario"]=='A') {?>
						<input class="btn btn-danger" type="button" name="eliminar" value="Eliminar"
						onClick="eliminar_archivo_ClientePotencial()" />


						<?php if ($_SESSION["superusuario"]=='A') {?>

						<input class="btn btn-primary" type="button" id="ocultorestaurar" name="ocultorestaurar" value="Restaurar" onClick="restaurar()"/>
						<?php }  } } ?>
						</p>
					</div>
				</div>
			</div>
			</div>
			<div class="row" id="restaurarocultar" style="display: none;">
				<input type="hidden" id="usuario" value="<?php echo $usuario ?>" name="usuario"/>
				<div class="col-lg-12">
				    <h1  class="page-header"><i class="fa fa-database"></i> Restaurar Clientes </h1>
				</div>
				<div class="row">
				  	<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-info">
						    <div class="panel-heading">
						       Seleccione archivo de Clientes a restaurar
						    </div>
						  	<div class="panel-body">
								<div>
							 		<div class="col-lg-5">Banco:</div>
						 			<div class="col-lg-5">
									<select name="clientesrestaurar" id="clientesrestaurar" required>
						 			<option value="">Seleccione Banco</option>
									</select>
									</div>
										<br>
								</div>

								<div>
									<div class="col-lg-5">Fecha a restaurar:</div>
									<div class="col-lg-5">
									<select name="fecharestaurar" id="fecharestaurar" required>
						 			<option value="">Seleccione Fecha</option>
									</select>

									</div>
								</div>
									<br>
								<div>
									<div class="col-lg-5">Nombre del archivo:</div>
									<div class="col-lg-5">
									<select name="nombrearchivo" id="nombrearchivo" required>
						 			<option value="">Seleccione Archivo</option>
									</select>

									</div>
								</div>
							</div>
							<input type="hidden" id="valor" value="1" name="valor"/>
							<script languaje="javascript">
							generarrestaurar();
							</script>
						
						<div class="panel-footer">
							<p style="text-align: center;">
							<input class="btn btn-primary" type="button" id="restaurara" name="restaurara" value="Restaurar Archivo" onClick="restaurararchivo()"/></p>
						</div>
					</div>
				</div>
				</div>
				</div>



				<div class="row" id="eliminarocultar" style="display: none;">
				
				<input type="hidden" id="usuario" value="<?php echo $usuario ?>" name="usuario"/>
				<div class="col-lg-12">
				    <h1  class="page-header"><i class="fa fa-database"></i> Eliminar Clientes </h1>
				</div>
				<div class="row">
				  	<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-info">
						    <div class="panel-heading">
						       Seleccione archivo de Clientes a eliminar
						    </div>
						  	<div class="panel-body">
								<div>
							 		<div class="col-lg-5">Banco:</div>
						 			<div class="col-lg-5">
									<select name="clienteaeliminar" id="clienteaeliminar" required>
						 			<option value="">Seleccione Banco</option>
									</select>
									</div>
										<br>
								</div>

								<div>
									<div class="col-lg-5">Fecha a eliminar:</div>
									<div class="col-lg-5">
									<select name="fecharaeliminar" id="fecharaeliminar" required>
						 			<option value="">Seleccione Fecha</option>
									</select>

									</div>
									<br>

									<div>
									<div class="col-lg-5">Nombre del Archivo:</div>
									<div class="col-lg-5">
									<select name="archivoaeliminar" id="archivoaeliminar" required>
						 			<option value="">Seleccione Archivo</option>
									</select>
									</div>
								</div>
							</div>
							<input type="hidden" id="valor" value="1" name="valor"/>
							<script languaje="javascript">
							 generareliminarCL();
							</script>
						</div>
						<div class="panel-footer">
							<p style="text-align: center;">
							<input class="btn btn-primary" type="button" id="restaurara" name="restaurara" value="Eliminar Archivo" onclick="eliminararchivoclientePotencial('<?php echo $_SESSION["superusuario"];?>','<?php echo $_SESSION["session_user"];  ?>')"/></p>
						</div>
					</div>
				</div>
			</div>
		
	</div>
</div>




	<?php
}
	else{
	header("Location:../salir");
	}


?>

