<?php
	$usuario=$_SESSION["session_user"];
	$var=($_REQUEST['var']);
	$array = explode("/",$var);
	$banco= $array[0];
	$fechadetalle=$array[1];
  $masiva = isset($array[2]) ? $array[2] : "";
  $namearchivo = isset($array[3]) ? $array[3] : "";
  $cliente = isset($array[4]) ? $array[4] : "";
  $fecha = isset($array[5]) ? $array[5] : "";

  $retroceder=$cliente.'/'.$fecha;

    $controladortranscliente =new controladorClientePotencial();
    $trans=$controladortranscliente->verclientepotencialbuscar($banco,$fechadetalle,$namearchivo);
    $row1 = pg_fetch_assoc($trans);

?>
<style type="text/css">.table.dataTable.dataTable_width_auto { width: auto; }  }</style>
<input type="hidden" id="fechadetalle" value="<?php echo $fechadetalle; ?>">
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="namearchivo"  name="namearchivo"  value="<?php echo $namearchivo; ?>" />
<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $row1['ibp']; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-cloud-upload" style="color:#F8F9F9;"> </i> Clientes Potenciales - Búsqueda Detallada:  </a>
        <br> <b style="color:#5DADE2;"> <?php echo $row1['ibp'];?></b>
        </div>
       </nav>
    </div>
    <br>
    <div class="row">
          <div class="col-12">
            <table class="table table-bordered dataTable_width_auto"  id="busquedadetalladacp" style="width: 100%"> 
               <thead>
                  <tr>
                    <th>enlaceafiliado</th>
                    <th>correlativo</th>
                    <th>Fecha&nbsp;Recepción</th>
                    <th>Fecha&nbsp;Carga</th>
                    <th>codbanco</th>
                    <th>ibp</th>
                    <th>Tipo&nbsp;de&nbsp;POS</th>
                    <th>Cant.</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;Afiliado&nbsp;&nbsp;</th>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Nombre del Archivo</th>
                    <th>Ejecutivo</th>

                  </tr>
                </thead>
                
            </table>
          </div>
        </div>

        <div class="row" align="center">
                <div class="col-md-4 col-md-offset-4">
                   <?php if ($masiva!="") {?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='?cargar=buscarcliente&var=<?php echo $retroceder;?>'"/>
                   <?php } else { ?>
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='clientesPotenciales.php'"/>
                   <?php }  ?>
                </div>
        </div>

       <!--    <div align="right">
             <a href="view/ClientePotencial/recepcion/reporte_clientedetallado_pdf?var=<?php echo  $var;?>" target="_blank"><img src="img/pdf.png" height="50" width="60">  <b style="color:red;">Descargar</b> </a>
          </div> -->
    </div>
  </div>
</div>

<script type="text/javascript">
  function dtCore() {
    var banco =$("#banco").val();
    var fechadetalle =$("#fechadetalle").val();
    var namearchivo =$("#namearchivo").val();
    $('#busquedadetalladacp').DataTable({
      "bDeferRender": true,
      "sPaginationType": "full_numbers",
    "ajax": {
    "url": "api/selectClientesPotencialesDetallada.php",
          "type": "POST",
          "data": {"banco":banco,"fechadetalle":fechadetalle,"namearchivo":namearchivo}
    },
      "AutoWidth": false,
    "columns": [
    {"data": "enlaceafiliado" },
    {"data": "correlativo", className: "text-center"},
    {"data": "fecharecepcion", className: "text-center" },
    {"data": "idate" },
    {"data": "codigobanco", className: "text-center" },
    {"data": "ibp", className: "text-center" },
    {"data": "tipopos", className: "text-center" },
    {"data": "cantidadterminales", className: "text-center" },
    {"data": "razonsocial" },
    {"data": "coddocumento", className: "text-center" },
    {"data": "nafiliacion", className: "text-center" },
    //{"data": "direccion"},
    {"data": "direccion"},
    {"data": "namearchivo", className: "text-center" },
    {"data": "ejecutivo", className: "text-center" }
    ],
    "order" : [[1, "asc"]],

    "columnDefs": [
             {
                 "targets": [ 0, 4, 5, 7],
                 "visible": false,
             },
           { "width": "100%", "targets": 11, }
         ],
      "scrollX": true,
      "scrollY": true,
      "info":     false,
      "scrollCollapse": false,

      dom: 'Bfrtip',
      buttons: ['excel'
      ],
      language:{
        "sProcessing":     "Procesando...",
         "sLengthMenu":     "Mostrar _MENU_ registros",
         "sZeroRecords":    "No se encontraron resultados",
         "sEmptyTable":     "Ningún dato disponible en esta tabla",
         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix":    "",
         "sSearch":         "Buscar:",
         "Print":           "Imprimir",
         "sUrl":            "",
         "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
         "oPaginate": {
           "sFirst":    "Primero",
           "sLast":     "Último",
           "sNext":     "Siguiente",
           "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }

    });
  }
</script>