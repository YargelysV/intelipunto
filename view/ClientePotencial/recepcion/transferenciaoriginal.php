<?php
set_time_limit(0);
//$control = isset($control) ? $control : "";

echo $_SESSION["size"].'-'.$_SESSION["name"].'-'.$_SESSION["type"].'-'.$_SESSION['guardar'].'-'.$_SESSION['error'].'-'.$_SESSION['control'].'-'.$_SESSION["errores"];

if (isset($_SESSION["session_user"]))
{
	/*if (isset($_REQUEST["control"]))
	{
			$control = 0;
			$_SESSION['control']=0;
			$_SESSION["size"]='';
			$_SESSION["name"]='';
			$_SESSION["type"]='';
	}*/
	/*if (isset($_REQUEST['error']))
	{
		$_SESSION['errores']=$_REQUEST['error'];
	}*/
	if (isset($_POST['enviar']))
	{
		//print_r($_FILES);
		$size=$_FILES['archivo']['size'];
		$name=$_FILES['archivo']['name'];
		$type=$_FILES['archivo']['type'];

		$date = date("Y-n-j H:i:s");
		$archivo = $_FILES['archivo']['tmp_name'];
		$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
		$cont=1;
		$control = 0;

		
		// validacion al intentar cargar otra vez el mismo archivo
	 	if($_SESSION["name"]==$name && $_SESSION["size"]==$size)
		{
	 		?>
			<script languaje="javascript">
			alertify.alert('Error Cargando el Archivo, el archivo que intenta cargar ya fue cargado en el Sistema. Verifique e intente nuevamente.');
			</script>
			<?php
		}
	  	if($_SESSION["name"]<>$name && $_SESSION["size"]<>$size || $_SESSION['control']==1)
		{
			$_SESSION["size"]=$size;
			$_SESSION["name"]=$name;
			$_SESSION["type"]=$type;
	
			//BUSCAR TODOS LOS PREFIJOS DE LOS BANCOS
			$controladorbanco =new controladorClientePotencial();
			$prefijobanco=$controladorbanco->verprefijobanco();
			$pre = 0;
			$opcionesprefijo="";
			while($prefijo = pg_fetch_array($prefijobanco)){
			$opcionesprefijo[$pre] = $prefijo['codigobanco'];
			//echo $opcionesprefijo[$pre]."<br>";
			$pre++;
			}

			//BUSCAR TODOS LOS TIPOS DE POS
			$controladortipopos =new controladorClientePotencial();
			$tipopos=$controladortipopos->vertipodepos();
			$tip = 0;
			$opcionestipopos="";
			while($pos = pg_fetch_array($tipopos)){
			$opcionestipopos[$tip] = $pos['tipopos'];
			$tip++;
			}

			//BUSCAR TODOS LOS TIPOS DE LINEA
			$controladortipolinea =new controladorClientePotencial();
			$tipolinea=$controladortipolinea->vertipodelinea();
			$linea = 0;
			$opcionestipolinea="";
			while($line = pg_fetch_array($tipolinea)){
			$opcionestipolinea[$linea] = $line['nameproveedor'];
			$linea++;
			}
			?>
	
			<div class="sub-content" align="center" id="errores" style="display: none;">
				<div id="page-wrapper">
	           	  	<div class="container-fluid">
	          	    	<div class="row"  >
							<?php
							$cont=0;
							$control=0;
							while($data = fgetcsv($fichero,5000,";"))
							{
								$cont++;
			
								if ($cont==3)
								{
									
									if ((utf8_encode($data[0])!="Consecutivo")||(utf8_encode($data[1])!="Fecha de Recepción del Archivo") ||(utf8_encode($data[2])!="Prefijo del Banco") ||(utf8_encode($data[3])!="Tipo de POS") || (utf8_encode($data[4])!="Tipo de Línea") ||(utf8_encode($data[5])!="Nº de Afiliación") ||(utf8_encode($data[6])!="Razón Social / Nombre del Afiliado") ||(utf8_encode($data[7])!="Rif/C.I.")||(utf8_encode($data[8])!="Actividad Económica de la Empresa")||(utf8_encode($data[9])!="Nombre del Representante Legal")||(utf8_encode($data[10])!="C.I. del Representante Legal")||(utf8_encode($data[11])!="Rif del Representante Legal")||(utf8_encode($data[12])!="Correo Electrónico")||(utf8_encode($data[13])!="N° Telefónico Móvil del Representante Legal")||(utf8_encode($data[14])!="Telefono (1)")||(utf8_encode($data[15])!="Telefono (2)") ||(utf8_encode($data[16])!="Nº Cuenta Recaudadora POS") ||(utf8_encode($data[17])!="Calle ó Avenida") ||(utf8_encode($data[18])!="Localidad") ||(utf8_encode($data[19])!="Sector") ||(utf8_encode($data[20])!="Urbanización") ||(utf8_encode($data[21])!="Local N°") ||(utf8_encode($data[22])!="Estado") ||(utf8_encode($data[23])!="Codigo Postal") ||(utf8_encode($data[24])!="Punto de Referencia"))
									{
									echo "<script>alertify.alert('El orden del formato es incorrecto', function () {
									window.location.assign('clientesPotenciales.php?cargar=adjuntar');});</script>";	
									$control = 2;
									}
								}
								else if (($cont>3) && ($control!=2))
								{
									if((strlen($data[0]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Consecutivo" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[1]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Fecha de Recepción del Archivo" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[2]) == '') || (!(in_array(($data[2]), $opcionesprefijo))))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Prefijo del Banco" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[3]) == '') || (!(in_array(($data[3]), $opcionestipopos))))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Tipo de POS" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[4]) == '')|| (!(in_array(($data[4]), $opcionestipolinea))))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Tipo de Línea" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;	
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[5]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Nº de Afiliación" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;	
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[6]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Razón Social / Nombre del Afiliado" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;	
									}
									if((strlen($data[7]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Rif / C.I." en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[8]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Actividad Económica de la Empresa" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[9]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Nombre del Representante Legal" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[10]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "C.I. del Representante Legal" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[11]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Rif del Representante Legal" en la linea '.
									$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[12]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Correo Electrónico" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[13]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "N° Telefónico Móvil del Representante Legal" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[14]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Telefono (1)" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;	
									$_SESSION["comprobar"]=$control;
									}
									/*if((strlen($data[15]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Telefono (2)" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}*/
									if((strlen($data[16]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Nº Cuenta Recaudadora POS" en la linea '.
									$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[17]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Calle ó Avenida" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[18]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Localidad" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[19]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Sector" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;	
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[20]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Urbanización" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;	
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[21]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Local N°" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;	
									$_SESSION["comprobar"]=$control;
									}
									if((strlen($data[22]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Estado" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;	
									}
									if((strlen($data[23]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Codigo Postal" en la linea '.$cont.'</h4><br>';
									$control = 1;
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;	
									}
									if((strlen($data[24]) == ''))
									{
									echo '<h4 style="color:red;">'.'ERROR en campo "Punto de Referencia" en la linea '.$cont.'</h4><br>';
									$control = 1;	
									$_SESSION['error']=1;
									$_SESSION["comprobar"]=$control;
									}
								}	
							}//fin while

							if ($_SESSION['errores']==2)
							{
							?>
							<table border="0" width="300" align="center">
								<tr>
									<td height="100" colspan="2" align="center">
									<font face="impact" size="5" color="red">
									Posibles Causas de error:<br>
									</font>
									</td>
								</tr>
									<td height="100" colspan="2" align="center">
									<font face="arial" size="4" color="green">
									- Verifique archivo .CSV según errores detallados<br>
									- Consulte con el administrador del Sistema.<br>
									 </font>
									</td>
								</tr>
								<tr>
									<td height="100" colspan="1" align="center">
									<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='clientesPotenciales?cargar=adjuntar'"/>
									</td>
								</tr>
							</table>
							<?php
							}else
							{
								?>
								<table border="0" width="300" align="center">
									<tr>
										<td height="100" colspan="2" align="center">
										<font face="impact" size="5" color="red">
										Posibles Causas de error:<br>
										</font>
										</td>
									</tr>
										<td height="100" colspan="2" align="center">
										<font face="arial" size="4" color="green">
										- Verifique archivo .CSV según errores detallados<br>
										- Consulte con el administrador del Sistema.<br>
										 </font>
										</td>
									</tr>
									<tr>
										<td height="100" colspan="1" align="center">
										<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='clientesPotenciales?cargar=adjuntar'"/>
										</td>
									</tr>
								</table>
							<?php
							}
							?>				
						</div>
					</div>
				</div>
			</div>		

			<?php	
		
				if($control == 0 && $_SESSION['control']!=1)
			{
				$date = date("Y-n-j H:i:s");
				$archivo = $_FILES['archivo']['tmp_name'];
				$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
				$controlador1 =new controladorClientePotencial();
				$cont=0;
				while($data = fgetcsv($fichero,5000,";"))
				{
					$cont++;
					if ($cont>3)
					{
					$insertar=$controlador1->insertar_tabltranscliente(utf8_encode(trim($data[0])),utf8_encode(trim($data[1])),utf8_encode(trim($data[2])),utf8_encode(trim($data[3])),utf8_encode(trim($data[4])),utf8_encode(trim($data[5])),utf8_encode(trim($data[6])),utf8_encode(trim($data[7])),utf8_encode(trim($data[8])),utf8_encode(trim($data[9])),utf8_encode(trim($data[10])),utf8_encode(trim($data[11])),utf8_encode(trim($data[12])),utf8_encode(trim($data[13])),utf8_encode(trim($data[14])),utf8_encode(trim($data[15])),utf8_encode(trim($data[16])),utf8_encode(trim($data[17])),utf8_encode(trim($data[18])),utf8_encode(trim($data[19])),utf8_encode(trim($data[20])),utf8_encode(trim($data[21])),utf8_encode(trim($data[22])),utf8_encode(trim($data[23])),utf8_encode(trim($data[24])), $_SESSION["session_user"],$name,$size);
					}
				}//fin del while
			}//fin del control=0;
		}//IF DEL ARCHIVO
	$_SESSION['control']=$control;		
	}//POST ENVIAR		
		echo $control."sssdsdsd";
	$control = isset($control) ? $control : ""; 
	if($control == 1 && $_SESSION['control']==1)
	{
		?>
		<script languaje="javascript">
		errores();
		</script>
		<?php
	}


?>
<?php
	
	$control = isset($control) ? $control : ""; 
	if(($control==0) || ($_SESSION['control']==1))
	{		
			//BUSCAR la informacion de la tabla de transferencia
			$controladortranscliente =new controladorClientePotencial();
			$trans1=$controladortranscliente->vertablatransclientepotencial();
		?>
		
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row"-->
                    <div class="col-lg-12">
                     <?php if ($_SESSION['guardar']==0) {?>	
                        <h3 class="page-header"><strong>Los siguientes datos ser&aacute;n cargados al sistema, &iquest;Desea proceder?</strong></h3>
                     <?php } else if ($_SESSION['guardar']==1) {?>
                         <h3 class="page-header"><strong>Datos procesados en el Sistema!.</strong></h3>
                     <?php } ?>
                    </div>
	   				<div class="row">
	            		 <div class="col-lg-12">
	            	 		<div class="panel panel-primary">
	                        	<div class="panel-heading" style="text-align: center;">
			                        <?php if ($_SESSION['guardar']==0) {?>	
			                        <h3 class="page-header"><strong>Resumen de Datos Cargados</strong></h3>
			                     	<?php } else if ($_SESSION['guardar']==1) {?>
			                        <h3 class="page-header"><strong>Resumen de Datos Procesados</strong></h3>
			                     	<?php } ?>
		                        </div>
	                       		 <!-- /.panel-heading -->
	                        	<div class="panel-body">
	                            	<div class="table-responsive">
		                                <table class="table table-striped table-bordered table-hover">
		                                    <thead>
		                                    <tr>
												<th>N&deg;</th>
												<th align="center">Fecha Recepción</th>
												<th align="center">Nombre de Banco</th>
												<th align="center">Tipo de Pos</th>
												<th align="center">Nº de afiliación</th>
												<th align="center">Razón Social</th>
												<th align="center">Rif / CI</th>

											</tr>
		                                    </thead>
		                                    <tbody>
		                                    	<?php  $TOTAL=0;
												$CONT=1;
												while($row = pg_fetch_array($trans1)):	?>
												<tr>
													<td align="center"><?php echo $row['id_consecutivo'];?> </td>
													<td align="center"><?php echo $row['fecharecepcion'];?> </td>
													<td align="center"><?php echo $row['ibp'];?> </td>
													<td align="center"><?php echo $row['tipopos'];?> </td>
													<td align="center"><?php echo $row['nafiliacion'];?> </td>
													<td align="center"><?php echo $row['razonsocial'];?> </td>
													<td align="center"><?php echo $row['coddocumento'];?> </td>
												</tr>
												<?php
												endwhile;
												?>
											</tbody>
		                                </table>
		                            </div>
	    	                    </div>
	        	            </div>
	        	        </div>
	            	</div>
	   			 	<div class="row">
		     		  	<div class="col-md-5 col-md-offset-5">
				            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='clientesPotenciales.php?cargar=adjuntar'"/>
							<?php if ($_SESSION['guardar']==0) {?>			
							<input name="enviar" class="btn btn-success" id="ocultocargar" type="button" value="Procesar" onclick= "cargar()"/>
							<?php } ?>
			  			</div>
	  				</div>
	 				<br>
            	</div>
        	</div>
   		</div>
    	<?php
	}//fin del if control
	
}// session user
else{
	header("Location:salir");
}
?>