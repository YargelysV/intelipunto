	<?php

	$usuario=$_SESSION["session_user"];
	$codtipousuario=$_SESSION["superusuario"];
  	//$var=($_REQUEST['var']);
  	$_SESSION["session_gestionmantenimiento"]='1';
  	$gestion=$_SESSION["gestionmantenimiento"];

 	$dir= isset($array[1]) ? $array[1] : "0" ;
 	 // $_SESSION['value_perfil']=$perfil;
	$modulovar= isset($_SESSION['modulo']   ) ? $_SESSION['modulo']  : "" ;
   
    $controladorperfil =new controladorGestionMantenimiento();
    $perfilgeo=$controladorperfil->spbuscarperfil();
    $row=pg_fetch_assoc($perfilgeo);
/*	$controlzona= new controladorGestionMantenimiento();
	$bancos=$controlzona->sp_bancos();
	$total1 = pg_num_rows($bancos);

	$controlgestion= new controladorGestionMantenimiento();
	$zona=$controlgestion->sp_zona();
	$total = pg_num_rows($zona);*/
		// $controlzona= new controladorGestionMantenimiento();
		// $precio= $controlzona->sp_preciozona($tipozona, $preciozona);
		// $total1= pg_num_rows($precio);
	?>

<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="codtipousuario"  name="codtipousuario"  value="<?php echo $codtipousuario; ?>" />


<div id="page-wrapper">
   <div class="container-fluid">
	  <div class="row">
		<nav class="navbar navbar-default" style="background-color:#337AB7;">
			<div class="navbar-header">
			<a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-dollar" style="color:#04B431;"> </i> Servicio de Mantenimiento</a>
			 </div>
		</nav>
	  </div> 
	 <div class="row" align="center">
	    <div class="col-12">
	       <table class="table table-bordered"  id="gestionzona" style="width: 100%"> 
	           <thead>
	              <tr>
	                <th>Nº</th>
	                <th>Estado</th>
	                <th>Municipio</th>
	                <th>Parroquia</th>
	                <th>Tipo de Zona</th>
	                <th>Accion</th>
	                <th></th>
	              </tr>
	            </thead>
	            <tfoot>
	               <th colspan="4" style="text-align:left">Total:</th>
	            </tfoot>
	        </table>
	    </div>
	  </div>
	</div>
  </div>


<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        <form id="formZonas" method="post">    
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-9">
                    	<input type="hidden" id="parroquia"  name="parroquia">
                        <div class="form-group">
                        <label for="" class="col-form-label">Tipo de Zona</label>
                        <select type="select" id="tipodezona">
                        </select>
                        <input type="hidden" name="tipo_zona" id="tipo_zona" class="form-control" value="" placeholder="" tabindex="4">
                        

                        </div>
                    </div>    
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnGuardar" class="btn btn-dark">Guardar</button>
            </div>
        </form>    		
        </div>
    </div>
</div>	


<script type="text/javascript">
  function dtCore(){

  	var codzonas= document.getElementById('tipodezona').value;
  	var codtipousuario= document.getElementById('codtipousuario').value;
  
  	//alert(codzonas);
  
    tablaZonas = $('table#gestionzona').DataTable({
      "ajax": {
      "url": "api/selectGestionZona.php",
            "type": "POST",
            "data": {opcion:1}
      },
      "columns": [
        {"data": "idparroquias", className: "text-center"},
        {"data": "iestados", className: "text-center"},
        {"data": "imunicipios", className: "text-center"},
        {"data": "iparroquias", className: "text-center" },
        {"data": "desczona", className: "text-center" },

        {"defaultContent":"",
				render : function ( data, type, full, meta ) {
	              var codusuario = $("#codtipousuario").val();
	              if (codusuario == "A" || codusuario === "A") {
	              	return "<button class='Editar btn btn-primary' type='button' id='editar' title='Editar'><i class='fa fa-edit'></i></button>";
	              }else{
	              	return "-";
	              }
	          }
      		},
        {"data": "idzona", className: "text-center" }],

      "order" : [[0, "asc"]],

      "columnDefs": [
		           {
		                 "targets": [6],
		                 "visible": false,
		             }
		         ],

	    "scrollX": 2900,
	    "scrollY": false,
	    "info": false,
	    "scrollCollapse": false,               

           dom: 'Bfrtip', 
           buttons: [
              'excel'              
            ],
            
             "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });


$(document).on("click", 'button.Editar', function(){	
    var table=$('table#gestionzona').DataTable();
    var D =  table.row($(this).parents("tr")).data();

	fila = $(this).closest("tr");         
    izona = parseInt(fila.find('td:eq(6)').text()); //capturo el ID  
    idparroquias = fila.find('td:eq(0)').text();

        var codZona=D.idzona;
        var ID=D.idparroquias;
        //alert(ID);

        $.post( 'api/selectzona.php',{postvariable: codZona}).done( function(respuesta){
          $('#tipodezona').html(respuesta);   
        });

        $("input#tipodezona").val("");

        $("#parroquia").val(idparroquias);
	    $(".modal-header").css("background-color", "#17a2b8");
	    $(".modal-header").css("color", "white" );
	    $(".modal-title").text("Asignación de Zonas");		
	    $('#modalCRUD').modal('show');	
	    //$('#codtecnicodiv').Attr('disabled');	   
});


$('#formZonas').submit(function(e){                         
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
	var idzona=document.getElementById('tipodezona').value;
	var idparroq=document.getElementById('parroquia').value;
	var usuario=document.getElementById('usuario').value;

		//alert(idzona);
		//alert(idparroq);
        $.ajax({
          url: "api/EditarGestionZona.php",
          type: "POST",
          datatype:"json",    
          data:  {"idzona":idzona, "idparroq":idparroq,"usuario":usuario},
          success: function(data) {
            tablaZonas.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});
    


}
</script>












