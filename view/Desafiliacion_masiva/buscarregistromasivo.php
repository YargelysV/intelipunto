<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $busqueda=$array[0];

 // $_SESSION['volver']=$POS.'/'.$fechainicial.'/'.$fechafinal;
  $controladorc =new controladorDesafiliacion();
  $resultados= $controladorc->busquedamasiva($busqueda);
  $databanco = pg_fetch_assoc($resultados);

  $count = pg_num_rows($resultados);

?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="hidden" id="busqueda"  name="busqueda"  value="<?php echo $busqueda; ?>" />


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Registros de Desafiliación:</a>
             </div>
       </nav>
      <b style="color:#5DADE2;"></b>    <b style="color:#5DADE2;">Búsqueda:<?php echo $busqueda; ?></b> 
    
    </div>
    <div class="row"  align="center">

          <div class="col-12">
        <table class="table table-bordered"  id="busqueda_masiva"> 
           <thead>
              <tr>
                
                <th>N°</th>
                <th>Cliente</th>
                <th>N° Serial</th>
                <th>Estatus</th>
                <th>Razon Social</th>
                <th>Documento</th>
                <th>Registrado por</th>
                <th>fecha de desafiliación</th>
                <th>Observaciones</th>
                
        
              </tr>
            </thead>
                
        </table>
 
      </div>

      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='desafiliacion'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var busqueda= document.getElementById('busqueda').value;


  $('table#busqueda_masiva').DataTable({
    "ajax": {
    "url": "view/Desafiliacion_masiva/selectdesafmasiva.php",  
          "type": "POST",
          "data": {"busqueda":busqueda}
    },
    "columns": [
      
      {"data": "id", className: "text-center"},
      {"data": "cliente", className: "text-center"},
      {"data": "seriales", className: "text-center" },
      {"data": "descmotivo", className: "text-center" },
      {"data": "razonsocial", className: "text-center"},
      {"data": "documento", className: "text-center" },
      {"data": "usuario", className: "text-center"},
      {"data": "fecha", className: "text-center" },
      {"data": "iobservacion", className: "text-center" }
    ],

    dom: 'Bfrtip',
     buttons: [
     {
            extend: 'excelHtml5',
            text: 'Exportar Excel', 
            title: 'Reporte Equipos Activos (Instalados)', 
               
        }
     ],

    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });


}
  </script>