<?php
if (isset($_SESSION["session_user"]))
{
	$_SESSION["session_ejecutivo"]="1";

	$ejecutivo=$_SESSION["ejecutivo"];

?>



 <script type="text/javascript">
        $(function () {
            $('#importDialog').modal({ 'show': false });
        });
</script>

<input type="" id="permisos" class="hidden" name="permisos"  value="<?php echo $ejecutivo; ?>" />
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-default" style="background-color:#337AB7;">
		        <div class="navbar-header" >
			        <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-bank"> </i> Ejecutivos Activos<b style="color:#FF5733;">
			        </b></a>
		        </div>
       		</nav>
			<div class="row">
		        <div class="col-lg-12" align="center">
		            <table class="table table-bordered"  id="tblid_ejecutivos"> 
		               <thead>
		                  <tr>
		                    <th>Nro.</th>
		                    <th>enlace</th>
		                    <th>Acción</th>
		                    <th>Doc.&nbsp;Indentidad</th>
		                    <th>Alias&nbsp;Vendedor</th>
		                    <th>Nombres</th>
		                    <th>Apellidos</th>
		                  </tr>
		                </thead>
		            
                    <tfoot>
                    <tr>
                        <th colspan="4" style="text-align:left">Total:</th>
                    </tr>
                </tfoot>
               </table> 
		         </div>
		    </div>
    	</div>
    </div>
</div>
<!--Modal para CRUD-->
<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        <form id="formUsuarios">    
            <div class="modal-body">
                <div class="row">
                	<input type="hidden" class="form-control" id="idtipoejecutivo">
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Tipo&nbsp;Doc.</label>
                    <select id="tipodocagregar">
                                       <option value="">Seleccionar</option>
                                       <option value="V">V</option>
                                       <option value="E">E</option>
                                       <option value="P">P</option>
                    </select>
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Doc. Indentidad</label>
                    <input type="text" class="form-control" id="documentoagregar">
                    </div> 
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Alias Vendedor</label>
                    <input type="text" class="form-control" id="aliasvend">
                    </div> 
                    </div>      
                </div>
                <div class="row"> 
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Nombres</label>
                    <input type="text" class="form-control" id="nombreagregar">
                    </div>               
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Apellidos</label>
                    <input type="text" class="form-control" id="apellidoagregar">
                    </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group" id="codtecnicodiv">
                        <label for="" class="col-form-label">Cod. Vendedor</label>
                        <input type="text" class="form-control" id="codvendedor">
                        </div>
                    </div>    
                </div> 
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                        <label for="" class="col-form-label">Tipo Ejecutivo</label>
                        <select id="tipoejecutivo">
                                       <option value="INTELIGENSA">INTELIGENSA</option>
                                       <option value="BANCO">BANCO</option>
                    </select>
                        </div>
                    </div>    
                </div>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnGuardar" class="btn btn-dark">Guardar</button>
            </div>
        </form>    
        </div>
    </div>
</div>		


<?php
}

else{
		header("Location:../salir");
}
?>

<script type="text/javascript">
  	function dtCore(){
  		
		tablaEjecutivos = $('#tblid_ejecutivos').DataTable({  
		    "ajax":{            
		        "url": "api/Ejecutivoxbanco.php", 
		        "method": 'POST', //usamos el metodo POST
		        "data":{opcion:1}
	    	},
		    "columns":[
		        {"data": "correlativo", className: "text-center"},
		      	{"data": "enlace"},
		      	{"defaultContent":"<a class='activarenlace' type='href' title='Ver'>Asignar Bancos</a>", className: "text-center"},
		      	{"data": "documento", className: "text-center" },
		      	{"data": "aliasvendedor", className: "text-center" },
		      	{"data": "nombre", className: "text-center" },
		      	{"data": "apellido", className: "text-center" }
			    ],
			    "order" : [[0, "asc"]],

			    "columnDefs": [
			           {
			                 "targets": [ 1],
			                 "visible": false,
			             }
			         ],
			    "scrollX": false,
			    "scrollY": false,
			    "info":     false,
			    "scrollCollapse": false,
			    
			    dom: 'Bfrtip',
			    buttons: [
					'excelHtml5'
					],

            "footerCallback": function ( tfoot, data, start, end, display ) {
                var api = this.api();
                var lastRow = api.rows().count();
                for (i = 0; i < api.columns().count(); i++) {
                  $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
                }
                // Update footer
                $( api.column( 0 ).footer() ).html(
                    'Total:'+lastRow 
                );
            },

				language:{
			      "sProcessing":     "Procesando...",
			       "sLengthMenu":     "Mostrar _MENU_ registros",
			       "sZeroRecords":    "No se encontraron resultados",
			       "sEmptyTable":     "Ningún dato disponible en esta tabla",
			       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			       "sInfoPostFix":    "",
			       "sSearch":         "Buscar:",
			       "Print":           "Imprimir",
			       "sUrl":            "",
			       "sInfoThousands":  ",",
			        "sLoadingRecords": "Cargando...",
			      "oPaginate": false,
			       "fixedHeader": {"header": false, "footer": false},
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			      }
			    }
	  	});
		    
var fila; 
$('#formUsuarios').submit(function(e){                         
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
   	var tipodoc=document.getElementById('tipodocagregar').value;
    var documento=document.getElementById('documentoagregar').value;
    var aliasvend=document.getElementById('aliasvend').value;
    var nombre=document.getElementById('nombreagregar').value;
    var apellido=document.getElementById('apellidoagregar').value;
    var codvendedor=document.getElementById('codvendedor').value;
    var tipoejecutivo=document.getElementById('tipoejecutivo').value;
    //alert(tipodoc);                  
        $.ajax({
          url: "api/Ejecutivoxbanco.php",
          type: "POST",
          datatype:"json",    
          data:  {"tipodoc":tipodoc, "documento":documento, "aliasvend":aliasvend, "nombre":nombre, "apellido":apellido, "codvendedor":codvendedor, "opcion":opcion, "tipoejecutivo":tipoejecutivo},
          success: function(data) {
            tablaEjecutivos.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});
        
$('table#tblid_ejecutivos tbody').on( 'click', 'a.activarenlace', function () {
//alert('algo');
    var table=$('table#tblid_ejecutivos').DataTable();
    var D =  table.row($(this).parents("tr")).data();
    var enlace=D.enlace;
    var url = "ejecutivobanco?cargar=agregarbancoejec&var="+enlace; 
    $(location).attr('href',url);

});

//Editar        
$(document).on("click", ".btnEditar", function(){		        
    opcion = 3;//editar
    fila = $(this).closest("tr");	        
    codvendedor = fila.find('td:eq(7)').text(); //capturo el ID		            
    tipodoc = fila.find('td:eq(2)').text();
    documento = fila.find('td:eq(3)').text();
    aliasvend = fila.find('td:eq(4)').text();
    nombre = fila.find('td:eq(5)').text();
    apellido = fila.find('td:eq(6)').text();
    trabajo = fila.find('td:eq(8)').text();
    //alert(tecnico);
    $("#tipodocagregar").val(tipodoc);
    $("#documentoagregar").val(documento);
    $("#aliasvend").val(aliasvend);
    $("#nombreagregar").val(nombre);
    $("#apellidoagregar").val(apellido);
    $("#tipoejecutivo").val(trabajo);
    $("#codvendedor").val(codvendedor);
    $(".modal-header").css("background-color", "#17a2b8");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Editar Ejecutivo");		
    $('#modalCRUD').modal('show');	
    //$('#codtecnicodiv').Attr('disabled');	   
});

}
</script>
