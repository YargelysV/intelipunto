<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array =explode("/",$var);
  $documento= $array[0];
  $vendedor= $array[1];
 // $banco=$array[3];
  $banco= isset($array[3]) ? $array[3] :"";
  $ejecutivo=$_SESSION["ejecutivo"];

  $controlador =new ControladorEjecutivo();
  $bancoasignado=$controlador->verejecutivoxbanco($vendedor);
  $row= pg_fetch_assoc($bancoasignado);

  ?>

<input type="" id="permisos" class="hidden"  name="permisos"  value="<?php echo $ejecutivo; ?>" />
<input type="hidden" id="documento"  name="documento"  value="<?php echo $documento; ?>" />
<input type="hidden" id="vendedor"  name="vendedor"  value="<?php echo $vendedor; ?>" />
<input type="hidden" id="banco"  name="banco"  value="<?php echo $banco; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />



<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-bank"> </i> Ejecutivos<b style="color:#FF5733;">
          </b></a>
         </div>
      </nav>
    </div>

 <div class="row" align="center"> 
     <div class="col-lg-10" align="left">
      <div class="panel panel-info">
        <div class="panel-heading" style="text-align: center;">
          <i class="fa fa-users"></i> Datos Ejecutivo
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="list-group">
            <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Cédula:
              <span class="pull-center text-muted small"><b><?php echo $row['documento'];?> </b>
              </span>
             </div>
           <div class="list-group-item">
            <i class="fa fa-check fa-fw"></i> Nombres:
            <span class="pull-center text-muted small"><b><?php echo $row['nombre'];?> </b>
            </span>
          </div>
           <div class="list-group-item">
            <i class="fa fa-check fa-fw"></i> Apellidos:
            <span class="pull-center text-muted small"><b><?php echo $row['apellido'];?> </b>
            </span>
          </div>
        
    </div>
 <br>


  <div class="row" align="center">
    <div class="panel-heading" style="color:blue;">
      Bancos Asignados
    </div>
    <div class="row">
      <div class="col-lg-12" align="center">
          <table class="table table-bordered"  id="tblejecutivosbancos"> 
             <thead>
                <tr>
                  <th>Consecutivo</th>
                  <th>Código Banco</th>
                  <th>Nombre del Banco</th>
                  <th>documento</th>
                  <th></th>
                </tr>
              </thead>
          </table>
        </div>
      </div>
  </div> 
      <br>

       <br>
      <div class="row">
        <div class="col-md-5 col-md-offset-5">
        <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='ejecutivobanco.php'"/>
        </div>
      </div>
<!--Modal para CRUD-->
<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        <form id="formEjecutivo">    
            <div class="modal-body">
                <div class="row">
                  <input type="hidden" class="form-control" id="documentoagregar">
                  <div class="row">
                    <div class="col-lg-9">
                      <div class="form-group">
                        <label for="" class="col-form-label">Agregar Banco</label>
                        <select id="bancoasignado" class="form-control"></select>
                        <input class="form-control hidden" id="bancoorigen">
                      </div>
                    </div>    
                  </div>               
                </div>
              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnGuardar" class="btn btn-dark">Guardar</button>
            </div>
        </form>    
        </div>
    </div>
</div>

<script type="text/javascript">
  selectbancoxejecutivo();
</script>

<script type="text/javascript">
    function dtCore(){
      $.fn.dataTable.ext.buttons.alert = {
        className: 'buttons-alert',
        action: function ( e, dt, node, config ) {
             $('#modalCRUD').modal('show');
        }
      };
    var vendedor=document.getElementById('vendedor').value;  
    tablaUsuarios = $('#tblejecutivosbancos').DataTable({  
        "ajax":{            
            "url": "api/selectEjecutivoBanco.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:1, "vendedor": vendedor}
        },
        "columns":[
            {"data": "correlativo"},
            {"data": "banco"},
            {"data": "nombrebanco", className: "text-center" },
            {"data": "documento", className: "text-center" },
            {"defaultContent":" <button class='btn btn-danger btn-sm btnBorrar' type='button' id='eliminarpago'><i class='fa fa-trash'></i></button>", className: "text-center"}
          ],
          "order" : [[1, "asc"]],

          "columnDefs": [
                 {
                       "targets": [3],
                       "visible": false,
                   }
               ],
          "scrollX": true,
          "scrollY": false,
          "info":     false,
          "scrollCollapse": false,
          
          dom: 'Bfrtip',
          buttons: [
          {
              extend: 'alert',
              text: '<button class="btn btn-warning" id="btnNuevo" type="button">+</button>'
            }, 
          ],
        language:{
            "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ registros",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "Print":           "Imprimir",
             "sUrl":            "",
             "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
            "oPaginate": false,
             "fixedHeader": {"header": false, "footer": false},
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          }
      });
        
var fila; 
$('#formEjecutivo').submit(function(e){                         
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    var vendedor=document.getElementById('vendedor').value;
    var bancoorigen=document.getElementById('bancoasignado').value;
    var usuario=document.getElementById('usuario').value;
    //alert(tipodoc);                  
        $.ajax({
          url: "api/selectEjecutivoBanco.php",
          type: "POST",
          datatype:"json",    
          data:  {"vendedor":vendedor, "bancoorigen":bancoorigen, "usuario":usuario, "opcion":opcion},
          success: function(data) {
            tablaUsuarios.ajax.reload(null, false);
           }
        });             
    $('#modalCRUD').modal('hide');                                
});
  
$("#btnNuevo").click(function(){
    opcion = 2; //alta           
    user_id=null;
    $("#formEjecutivo").trigger("reset");
    $(".modal-header").css( "background-color", "#007bff");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("Asignar Banco");
    $('#modalCRUD').modal('show');  
    //$('#codtecnicodiv').removeAttr('disabled');    
});

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);    
    var usuario=document.getElementById('usuario').value;    
    var vendedor=document.getElementById('vendedor').value;
    bancoorigen =  parseInt($(this).closest('tr').find('td:eq(1)').text()) ;  
    opcion = 4; //eliminar    

    //alert (bancoorigen);    
    alertify.confirm("&#191;Desea eliminar este banco?", function (e){               
      if (e) {            
        $.ajax({
          url: "api/selectEjecutivoBanco.php",
          type: "POST",
          datatype:"json",    
          data:  {"opcion":opcion, "bancoorigen":bancoorigen, "vendedor":vendedor},    
          success: function() {
              tablaUsuarios.row(fila.parents('tr')).remove().draw();                  
           }
        }); 
      }
    });
  });
}
</script>

          
          

