<?php
  $usuario=$_SESSION["session_user"];

  $controlador =new controladorBanesco();
  $cantidad= $controlador->banescoafiliados();
  $result=pg_fetch_assoc($cantidad);
  $count = pg_num_rows($cantidad);
?>

<?php 
if ($count==0) {  ?>

<script type="text/javascript">
function dtCore(){}
redirectResultB();
</script>
<?php } else { ?>

<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />
<input type="hidden" id="estatusllaves" name="estatusllaves" value="<?php echo  $datalote['status']; ?>">
<input type="hidden" id="idstatus" name="idstatus" value="<?php echo  $result['estatushistorico']; ?>">

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-clipboard x3" style="color:#F8F9F9;"> </i> Desafiliación de Equipos: </a>
        </div>
       </nav>
    </div>
    <div class="row" align="center">
      <div class="col-12" id="tableapp">
        <table class="table table-bordered"  id="desafiliarbanescotodo" style="width: 100%;background-color:#F8F9F9;"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>ID</th>
                <th>RIF</th>
                <th>Razón Social</th>
                <th>Representante Legal</th>
                <th>Actividad Económica</th>
                <th>Afiliado</th>
                <th>Serial</th>
                <th>N° Terminal</th>
                <th>Desafiliar</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desincorporación&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
            </tfoot>
        </table>
      </div>
      <div class="row">
        <div>
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='desaf-desincorpBanesco.php'"/>
        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
  function dtCore(){


  var usuariocarga= document.getElementById('usuario').value;
  //var today = new Date();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

        if(dd < 10)
          dd = '0' + dd;

        if(mm < 10)
          mm = '0' + mm;

        today = yyyy + '-' + mm + '-' + dd;


    $('table#desafiliarbanescotodo').DataTable({
      "ajax": {
      "url": "view/desaf-desincorpBanesco/desaf/selectregistrosafiltodos.php",
            "type": "POST"
      },
      "columns": [
        {"data": "consecutivo", className: "text-center"},
        {"data": "idconsecutivo", className: "text-center"},
        {"data": "coddocumento", className: "text-center"},
        {"data": "razonsocial", className: "text-center" },
        {"data": "representantelegal", className: "text-center" },
        {"data": "actividadeconomica", className: "text-center"},
        {"data": "afiliado", className: "text-center" },
        {"data": "serialpos", className: "text-center" },
        {"data": "terminal", className: "text-center" },
        {"data": "estatushistorico", 
          render : function ( data, type, full, meta ) {
           // console.log(data);
            if (data==5 || data=="5" || data==="5") {

                return '<input type="checkbox" class="check-buttons" checked value="5" disabled>';

            } else if(data ==6 || data =="6" || data ==="6"){

              return '<input type="checkbox" class="check-buttons"  value="6" disabled>';

            } else if (data!=5 || data!="5" || data!=6 || data!="6"){

                return '<input type="checkbox" class="check-buttons" value="5" >';
            }
         }
           // if ((data == 1 || data == "1" || data === "1" || data == 2 || data == "2" || data === "2")) {
           //    return '<input type="checkbox" class="check-buttons" id="llaves" checked value="1" >';
           //  }else 
           //    return '<input type="checkbox" class="check-buttons" id="llaves" value="0">';
                
             , className: "text-center"  },

        {"data": "estatushistorico", 
          render : function ( data, type, full, meta ) {

              var serialpos =full['serialpos'];
              var id =full['idconsecutivo'];
              var motivoestatus =full['motivosdesincorporacion'];
              //alert(motivoestatus);
              var currentCell = $("table#desafiliarbanescotodo").DataTable().cells({"row":meta.row, "column":meta.col}).nodes(0);
              var contador=1;

                
            //console.log(full.idestatusconfig);
            if (motivoestatus== "null" || motivoestatus== null) {
                        
                $.ajax({
                url : "view/desaf-desincorpBanesco/desaf/selectestatusdes.php",
                type:'POST',
                dataType: 'html',
                data: {"id":motivoestatus, "serialpos":serialpos, "data":data}
                }).done(function (data) {                                                                              
                     $(currentCell).html(data);
                         
                }); 
                    return '';
            } else if (data == 6 || data == "6" || data === "6") {

                $.ajax({
                url : "view/desaf-desincorpBanesco/desaf/selectestatusdes.php",
                type:'POST',
                dataType: 'html',
                data: {"id":motivoestatus, "serialpos":serialpos, "data":data}
                }).done(function (data) {                                                                              
                    $(currentCell).html(data);
                }); 
                    return ''
                            
            } else if (data == 5 || data == "5" || data === "5") {

                return '<select id="agg'+full['serial']+'" name="agg" class="form-control" style="width:195px;" disabled><option value="0" selected="select">Seleccione</option><option value="1">Solicitado por el cliente Inteligensa</option><option value="2">Solicitado por Banesco</option><option value="3">Error en carga</option><option value="4">Falla en dispositivo</option></select>'
                            
            } else 

                return '<select id="agg'+full['serial']+'" name="agg" class="form-control" style="width:195px;"><option value="0" selected="select">Seleccione</option><option value="1">Solicitado por el cliente Inteligensa</option><option value="2">Solicitado por Banesco</option><option value="3">Error en carga</option><option value="4">Falla en dispositivo</option></select>'

            } 
        },
        ],

      "order" : [[0, "asc"]],

            
            
            "scrollX": true,
            "scrollY": false,
            "info":     false,
            "scrollCollapse": false,
            

            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },


            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": false,
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });

    $('#desafiliarbanescotodo tbody').on( 'click', 'input.check-buttons', function () {
      //alert('algo');
       var table=$('#desafiliarbanescotodo').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var idconsec = T.idconsecutivo;
       var serialpos = T.serialpos;       
       var usuariocarga = $("#usuario").val();
       //var statusllaves= document.getElementById('llaves').value;
       var checks=$(this).val();

       //alert(statusllaves);
      if (checks) {
        
         Swal.fire({
            title: '¿Desea realizar la desafiliación de este registro?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/desaf-desincorpBanesco/desaf/guaradardesafiliacion.php",
                        type : "POST",
                        data : {"serialpos":serialpos,"usuariocarga":usuariocarga,"idconsec":idconsec},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#desafiliarbanescotodo').DataTable();
                                    table.ajax.reload(); 

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#desafiliarbanescotodo').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });
        }
    });

   $('table#desafiliarbanescotodo tbody').on( 'click', 'button.ATENDER', function () {
  //alert('algo');
      var table=$('table#desafiliarbanescotodo').DataTable();
      var T =  table.row($(this).parents("tr")).data();
      var idconsec = T.idconsecutivo;
      var serialpos = T.serialpos;       
      var usuariocarga = $("#usuario").val();
      var select = $("#agg"+T.serialpos).val();
      $('#agg'+T.serialpos).prop('disabled', true);
      //alert(select);

    if(select==0 || select=='0' || select===0 || select==null){
      alertify.alert("DEBE SELECCIONAR UN ESTATUS DE DESINCORPORACIÓN!.");

    }else{
      Swal.fire({
            title: '¿Desea realizar la desafiliación de este registro?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
            }).then((result) => {
                if (result.value==true) {

                    $.ajax({
                        url : "view/desaf-desincorpBanesco/desaf/guaradardesincorporacion.php",
                        type : "POST",
                        data : {"serialpos":serialpos,"usuariocarga":usuariocarga,"idconsec":idconsec, "select":select},
                        success: function(data){
                            if(data==1){
                        
                                _Title = "¡Enhorabuena!";
                                _Text = "El estatus ha sido actualizado";
                                _Type = "success";

                              Swal.fire({
                                    text : _Text,
                                    title: _Title,
                                    timer: 1000,
                                    type : _Type,

                                    onBeforeOpen: function (){
                                        swal.showLoading()
                                    }
                                }).then((result)=>{
                                    
                                    var table=$('#desafiliarbanescotodo').DataTable();
                                    table.ajax.reload(); 
                                    $('#agg'+T.serialpos).prop('disabled', true);

                                });
                           
                        }else
                        swal(data);
                        }
                    })
                    .fail(function(){
                        swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
                    });
                } else {
                                    _Title = "Cancelado!";
                                    _Text = "La actualización ha sido cancelado";
                                    _Type = "success";

                                  Swal.fire({
                                        text : _Text,
                                        title: _Title,
                                        timer: 1000,
                                        type : _Type,
                                    }).then((result)=>{                                       
                                       var table=$('#desafiliarbanesco').DataTable();
                                        table.ajax.reload(); 
                                    });              
                }
            });
    }

  });   

}
</script>

<?php } ?>
