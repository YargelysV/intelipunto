<?php
  
  $usuario=$_SESSION["session_user"];
  $variable=($_REQUEST['var']);
  $array = explode("/", $variable);
  $mess=$array[0];
  $array1 = explode("-", $mess);
  $anio=$array1[0];
  $meses=$array1[1];
  $empleado=$array[1];
  $controlador = new ControladorControlAcceso();
  $resultados= $controlador->sp_buscaraccesos($meses,$anio,$empleado);
  $dataacceso = pg_fetch_assoc($resultados);
  $count = pg_num_rows($resultados);
  echo $meses;
 
?>

<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>
<input type="hidden" id="meses"  name="mes"  value="<?php echo $meses; ?>" />
<input type="hidden" id="meses"  name="mes"  value="<?php echo $meses; ?>" />
<input type="hidden" id="anio"  name="anio"  value="<?php echo $anio; ?>" />
<input type="hidden" id="empleado"  name="empleado"  value="<?php echo $empleado; ?>" />
 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;">Busqueda de Horarios: </a>
          <br>
          <?php  if($empleado!='' && $empleado!=0 && $mess!='' && $mess!='00'){?>  
              <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $mess.'/'.$dataacceso['nombre'].' '.$dataacceso['apellido'];?></b>
            <?php }else if($mess!='' && $mess!=00){?>
              <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $mess;?></b>
            <?php }else if($empleado!='' && $empleado!=0){?>  
              <br> <b style="color:#5DADE2;">Búsqueda:<?php echo $dataacceso['nombre'].' '.$dataacceso['apellido'];?></b>
            <?php }else if($mess=00 || $empleado=0){?>  
              <br> <b style="color:#5DADE2;">Búsqueda: </b> ?>
            <?php } ?>
        </div>
       </nav>
    </div>
    <div class="row">
    <div class="col-12">
        <table class="table table-bordered"  id="tblbuscarcontrolaccesos"> 
           <thead>
              <tr>               
                <th>&nbsp;&nbsp;&nbsp;Hora&nbsp;Llegada&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;Hora&nbsp;Salida&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Fecha&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;ID&nbsp;Usuario&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apellido&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tiempo&nbsp;Mora&nbsp;Dia&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acumulado&nbsp;Mora&nbsp;Mes&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Faltas</th>
                <th>Promedio de Llegada</th>
       </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
          </tfoot>
        </table>
 
      </div> 
      <br>

        <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='buscadorcontrolacceso'">Volver</button>
        </div>

    </div>
  </div>
</div>

<?php  } ?>

  <script type="text/javascript">
  function dtCore(){

  var meses= document.getElementById('meses').value;
  var anio= document.getElementById('anio').value;
  var empleado= document.getElementById('empleado').value;
  //alert(empleado);
  $('table#tblbuscarcontrolaccesos').DataTable({
    "ajax": {
    "url": "api/SelectBuscarControlAccesos.php",  
          "type": "POST",
          "data": {"meses":meses,"anio":anio,"empleado":empleado}
    },
    "columns": [
      
      {"data": "horallegada", className: "text-center"},
      {"data": "horasalida", className: "text-center" },
      {"data": "fechatime", className: "text-center" },
      {"data": "idusuario", className: "text-center" },
      {"data": "nombre",  className: "text-center"},
      {"data": "apellido", className: "text-center" },
      {"data": "tiempo_mora_dia", className: "text-center" },
      {"data": "tiempo_mora_mes", className: "text-center" },
      {"data": "faltas", className: "text-center" },
      {"data": "tiempo_total_lleg", className: "text-center" }
      
    ],
    "order" : [[3, "asc"]],
    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    "pageLength": 20,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend:'excelHtml5',          
        }
    ],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

}
  </script>