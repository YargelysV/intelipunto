<?php
//  session_start();
if (isset($_SESSION["session_user"]))
{
  $usuario=$_SESSION["session_user"];
}

?>
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <h1 class="page-header"><i class="fa fa-hand-o-right x4" style="color:#045FB4"> </i>  Busqueda de accesos de Inteligensa</h1>
      </div>


      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading text-center">
              - BUSCADOR -
            </div>

          <div class="panel-body"> 
            <div>
              <div class="col-lg-3">Elige los días feriados/ Fines de semanas:</div>               
              <div id="root"></div>
              <br>
            </div>
            
          </div>
          <div class="panel-footer">
          <p style="text-align: center;">
             <input class="btn btn-success" type="button" name="Guardar" value="Guardar" onclick="lolo()" />
          </p>
            </div>
        
            
           <input type="hidden" id="valor" value="1" name="valor"/>
           <input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"];?>' name="usuario"/>
           <input type="hidden" id="usuario" value='<?php echo $_SESSION["session_user"];?>' name="usuario" readonly/>
           <input type="hidden" id="area" value='<?php echo $_SESSION["area_usuario"];?>' name="area" readonly/>
          </div>
        </div>
        
    </div>
  </div>
</div>

<div class="container" style="width:600px">
      <div class="col-lg-12">
          <div class="panel panel-info">
              <div class="panel-heading" style="text-align: center;">
            <i class="fa fa-calendar"></i> Feriados y Fines de semana
          </div>
        <div class="row">
          <div class="col-lg-12">
            <table class="table table-bordered"  id="tblferiados"> 
               <thead>
                  <tr>
                    <th>Nº</th>
                    <th>Fecha</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                
            </table>
          </div>
        </div>
        </div>
    </div>
</div>

<script type="text/javascript">

  function dtCore(){
  $('table#tblferiados').DataTable({
    "ajax": {
    "url": "api/Selectmostrarferiados.php"
    },
    "columns": [
      {"data": "consecutivo"},
      {"data": "fechas", className: "text-center" },
      {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='Eliminar btn btn-danger' type='button' id='eliminarferiados'><i class='fa fa-trash'></i></button></div></div>", className: "text-center" }
    ],
    "order" : [[0, "asc"]],
    "info":     false,  

    "columnDefs": [
           { "sWidth": "10%", "aTargets": [ -1 ] },
           { "sWidth": "10%", "aTargets": [ -3 ] }
         ],

    dom: 'Bfrtip',
    buttons: [
        {
            extend:'excelHtml5',          
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
      "oPaginate": false,
       "fixedHeader": {"header": false, "footer": false},
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
  });

  $('#tblferiados tbody').on( 'click', 'button.Eliminar', function () {
       var table=$('table#tblferiados').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var fecha = T.fechas;
       //alert(fecha);
      alertify.confirm("&#191;Desea eliminar esta fecha?", function (e) {
        if (e) {
          $.post('view/ControlAcceso/eliminarferiados.php',{postvariable:fecha},
            function(data)
            {
              alertify.success(data);
              setTimeout(function(){ window.location.reload(0); }, 1000);
            });
        } else {
          alertify.error("Acción cancelada.");
          //setTimeout(function(){ window.location.reload(0); }, 1000);
          return false;
        }
      })
      return false;

  });
}
</script>

