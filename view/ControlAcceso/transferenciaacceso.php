<?php
error_reporting(0);
set_time_limit(0);
if (isset($_SESSION["session_user"]))
{

 date_default_timezone_set('America/Caracas');
 $ahora = date("Y-n-j H:i:s");

function sanear_string($string)
{
 
    $string = trim($string);
 
 /*   $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );*/
 
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("¨", "º", "-", "~",
             "#", "|", "!", 
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":"),
        '',
        $string
    );
 
 
    return $string;
}

	$control = 0;
	if ($_SESSION["comprobar"]==1)
	{
		$control = 1;
		$_SESSION["size"]='';
		$_SESSION["name"]='';
		$_SESSION["type"]='';
	}

	if (isset($_REQUEST['error']))
	{
		$_SESSION['errores']=$_REQUEST['error'];
	}
	
	$_SESSION['enviar']=$_FILES['archivo']['tmp_name'];

	$size=$_FILES['archivo']['size'];
	$name=$_FILES['archivo']['name'];
	$type=$_FILES['archivo']['type'];

		
	$array = explode("_", $name);
	$nombre= isset($array[0]) ? $array[0] : "";
	$mesarchivo=isset($array[1]) ? $array[1] : "";
	$añoarchivo=isset($array[2]) ? $array[2] : "";
	$bMeses = array("void","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$fecha=getdate();
	$meses = $bMeses[$fecha["mon"]];
	$año=$fecha['year'];
	$dia=$fecha['mday'];
	//echo $name."<br>"; 

	//BUSCAR SI EL ARCHIVO YA FUE REGISTRADO CON ESOS NOMBRES
	$controladorarchivo =new ControladorControlAcceso();
	$narchivo=$controladorarchivo->verarchivoacceso($name);
	$existe=pg_fetch_array($narchivo);
	echo $existe['verarchivoacceso'];


	//BUSCAR SI LA FECHA DEL MES FUE CARGADA
	$controladorafiliado =new ControladorControlAcceso();
	$fechacargada=$controladorarchivo->verfechacargada();
	$fechaactiva = 0;
	$fechasactivas=array();
	while($factiva = pg_fetch_array($fechacargada)){
	$fechasactivas[$fechaactiva] =$factiva['spverfechaaccesocargado'];
//	echo $fechasactivas[$fechaactiva]."<br>";
	$fechaactiva++;
	}

	/*if ($nombre=="" || $mesarchivo=="" || $añoarchivo=="" || $nombre!="Control" )
	{
		echo "<script>alertify.alert('El nombre del archivo es incorrecto, debe contener el formato \"Control_Mes_Año\" Ej: (Control_".$meses."_".$año.") ', function () {
				window.location.assign('controlaccesos.php?cargar=adjuntarcontrolaccesos');});</script>";
	/*}else if (($dia>5 && $mesarchivo!=$meses )|| $añoarchivo!=$año) {
		echo "<script>alertify.alert('El nombre del archivo es incorrecto, La fecha debe coincidir con el Mes y Fecha actuales. Ej: (Control_".$meses."_".$año.")' , function () {
				window.location.assign('controlaccesos.php?cargar=adjuntarcontrolaccesos');});</script>";
	}else*/
	 if ($existe['verarchivoacceso'] == "t") {
		echo "<script>alertify.alert('El archivo que intenta adjuntar se encuentra cargado en sistema.' , function () {
				window.location.assign('controlaccesos.php?cargar=adjuntarcontrolaccesos');});</script>";
	}else{
		$date = date("Y-n-j H:i:s");
		$archivo = $_FILES['archivo']['tmp_name'];
		$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
		

		
	?>
	<div class="sub-content" align="center" >
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" id="errores">
                <?php
                	$cont=0;
					$control=0;
					while($data = fgetcsv($fichero,10000,";"))
					{
						$cont++;
						if($cont==1)
						{
							/*echo utf8_encode($data[0])."<br>";
							echo utf8_encode($data[1])."<br>";
							echo utf8_encode($data[2])."<br>";
							echo utf8_encode($data[3])."<br>";
							echo utf8_encode($data[4])."<br>";
							echo utf8_encode($data[5])."<br>";
							echo utf8_encode($data[6])."<br>";
							echo utf8_encode($data[7])."<br>";
							echo utf8_encode($data[8])."<br>";
							echo utf8_encode($data[9])."<br>";*/
							if ((utf8_encode($data[0])!="tiempo") ||(utf8_encode($data[1])!="idusuario") ||(utf8_encode($data[2])!="nombre") || (utf8_encode($data[3])!="apellido") ||(utf8_encode($data[4])!="numerotarjeta")||(utf8_encode($data[5])!="dispositivo")||(utf8_encode($data[6])!="puntoevento")||(utf8_encode($data[7])!="verificacion")||(utf8_encode($data[8])!="estado")||(utf8_encode($data[9])!="evento")||(utf8_encode($data[10])!="notas"))
							{
								echo "<script>alertify.alert('El orden del formato es incorrecto', function () {
									window.location.assign('controlaccesos.php?cargar=adjuntarcontrolaccesos');});</script>";	
									$control=2;
							}
						}
							
						else if (($cont>1) && ($control!=2))
						{
							
							
							
							//echo $array2[0]."<br>";
							//echo $array2[1]."<br>";
							
							/*echo utf8_encode($data[0])."<br>";
							echo utf8_encode($data[1])."<br>";
							echo utf8_encode($data[2])."<br>";
							echo utf8_encode($data[3])."<br>";
							echo utf8_encode($data[4])."<br>";
							echo utf8_encode($data[5])."<br>";
							echo utf8_encode($data[6])."<br>";
							echo utf8_encode($data[7])."<br>";
							echo utf8_encode($data[8])."<br>";
							echo utf8_encode($data[9])."<br>";*/

							if((strlen($data[0]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo Tiempo en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}
							if((in_array($añomes, $fechasactivas)))
							{
								echo '<h4 style="color:red;">'.'ERROR  "La fecha de registro que intenta cargar se encuentra en sistema, debe eliminar el registro para cargar nuevamente".<br> En la linea '.$cont.' verifique la información e intente nuevamente. </h4> 
									- Fecha que intenta cargar '.$data[0].'<br><br>
									';
								$control = 1;
							}

						}
					}
					if($control==1){
						?>
						<table border="0" width="300" align="center">
							<tr>
								<td height="100" colspan="2" align="center">
									<font face="impact" size="5" color="red">
									Posibles Causas de error:<br>
									</font>
								</td>
							</tr>
							<tr>
								<td height="100" colspan="2" align="center">
									<font face="arial" size="4" color="green">
									- Verifique archivo .CSV<br>
									- Debe contener informaci&oacute;n en la primera columna.<br>
									- Ningun campo debe venir en blanco, y debe estar delimitado las columnas por punto y coma (;) .<br>
									- Consulte con el administrador del Sistema.<br>
									 </font>
								</td>
							</tr>
							<tr>
								<td height="100" colspan="1" align="center">
									<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='Serie?cargar=adjuntar'"/>
								</td>
							</tr>
						</table>
					<?php	
					}
                ?>	
                </div>
        </div>
    </div>	
<?php
	if($control == 0){
		$archivo = $_FILES['archivo']['tmp_name'];
		$fichero1 = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
		$count=0;
		$controlador1 =new ControladorControlAcceso();
		while($data1 = fgetcsv($fichero1,10000,";"))
		{
			$count++;
			if($count>1)
			{
			
				if ($data1[0]!='')
					$array2=explode(" ", $data1[0]);
					$arrayfecha=explode("/",$array2[0]);
					$formatfecha=$arrayfecha[2]."-".$arrayfecha[1]."-".$arrayfecha[0]." ".$array2[1];
					$date=date_create($formatfecha);
					$tiempo=date_format($date, 'Y-m-d H:i:s');
					$insertar=$controlador1->insertar_tblacceso($tiempo,$data1[1],$data1[2],utf8_encode($data1[3]),$data1[4],$data1[5],$data1[6],utf8_encode($data1[7]),$data1[8],$data1[9],$name);

			}
		}
		echo "<script>alertify.alert('Archivo Cargado con &Eacute;xito', function () {
					window.location.assign('ControlAccesos.php');});</script>";	
	}
}
}
?>