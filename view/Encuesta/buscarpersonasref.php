<?php
  $usuario=$_SESSION["session_user"];
  $idcliente=($_REQUEST['var']);
  
  //$idcliente= $array[0];
//echo $idcliente;
  $controladorc =new controladorEncuesta();
  $comerciali=$controladorc->spbuscarpersonareferida($idcliente);
  $databanco = pg_fetch_assoc($comerciali);
 ?>

<input type="hidden" id="telefono"  name="telefono"  value="<?php echo $databanco['numeroref']; ?>" />
<input type="hidden" id="nombreref"  name="nombreref"  value="<?php echo $databanco['nombrereferido']; ?>" />
<input type="hidden" id="apellidoref"  name="apellidoref"  value="<?php echo $databanco['apellido']; ?>" />
<input type="hidden" id="idregistro"  name="idregistro"  value="<?php echo $idcliente; ?>" />
<input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
     <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-edit"> </i> Encuesta / Realizar Encuesta:<b style="color:#FF5733;">
          </b></a>
          <br>
        </div>
       </nav>
    <div class="col-lg-8">
          <div class="panel panel-info">
            <div class="panel-heading" style="text-align: center;">
              <i class="fa fa-user fa-fw"></i>Datos Básicos
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="list-group">
                <div class="list-group-item">
            <i class="fa fa-check fa-fw"></i> Nombre y Apellido (Persona Referida):
           <span class="pull-center text-muted small"  ><b><?php echo $databanco['nombreref'];?></b>
            </span>
            </div>
            <div class="list-group-item">
            <i class="fa fa-check fa-fw"></i>N° de Teléfono (Persona Referida):
            <span class="pull-center text-muted small"  ><b><?php echo $databanco['numeroref'];?></b>
            </span>
            </div>
            <div class="list-group-item">
            <i class="fa fa-check fa-fw"></i> Nombre y Apellido (Representatnte legal):
            <span class="pull-center text-muted small"><b><?php echo  $databanco['representantelegal'] ;?></b>
            </span>
            </div>
            <div class="list-group-item" >
            <i class="fa fa-check fa-fw"></i> Nombre y Apellido (Persona Contacto):
            <span class="pull-center text-muted small"><b><?php echo  $databanco['personacontact'] ;?></b>
            </span>
            </div>
            <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Estatus:
              <span class="pull-center text-muted small" ><b><?php echo $databanco['estaturef'];?></b>
            </span>
              </div>



            </div>
            <!-- /.list-group -->
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        <p style="text-align: center;">
        <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href ='personasreferidas.php'"/>
        </p>
        </div>      <!-- /.end div 4 -->
      <!-- end panel datos básicos -->

      <div class="col-lg-4">
        <div class="panel panel-default">
          <div class="panel-heading" style="text-align: center;">
            <i class="fa  fa-edit fa-fw"></i> Editar
          </div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="list-group">
               <b class="list-group-item">
             
                 <!-- Button trigger modal datos de contacto-->
                <button type="button" class="btn btn-primary btn-block" id="btncontactmodal" id="btncontactmodal" onclick="enviarcaptacion()">
                Registar En Captación
              </button>

            </b>
            
            <b class="list-group-item"  style="text-align: center;">
               <p style="text-align: center;"> Indique si fue contactado: </p>
             <label class="switch" id="switch">
                <input type="checkbox" id="estatus" onchange="personarefcpntactado();">
                <span class="slider round"></span>
              </label>
              <label class="switch" id="magic_switch" style="display: none; text-align: center;">
                  <input type="checkbox" id="magic_switch_check" checked disabled="disabled">
                  <span class="slider round"></span>
              </label>
               </b>

                
          </div>
          <!-- /.list-group -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>      <!-- /.end div 4 -->

