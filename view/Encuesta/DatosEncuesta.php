<?php 
   $tipomodulo= $_SESSION["encuesta"];
   $var=($_REQUEST['var']);
   $array = explode("/",$var);
   $nroafiliacion= $array[0];
   $namearchivo= $array[1];
   $fechacarga= $array[2];
   $idcliente= $array[3];
   $rif= isset($array[4]) ? $array[4]  : "" ;
   $banco= isset($array[5] ) ? $array[5] : "" ;
   $busqueda=isset($array[6] ) ? $array[6] : "" ;
   $bancoparametrizacion=isset($array[7] ) ? $array[7] : "" ;
   $estatusparametrizacion=isset($array[8] ) ? $array[8] : "" ;
  if (isset($_SESSION["session_user"]))
  {
  $usuario=$_SESSION["session_user"];
  }

  $fechaBusqueda = isset($_SESSION["fecha"])? $_SESSION["fecha"]:"";
   $urlretorno = $banco.'/'. $fechaBusqueda;
   $fecha = '2019-01-01';
   $bfecha=explode("-", $fechacarga);
   $fechaenlace=$bfecha[2].'-'.$bfecha[1].'-'.$bfecha[0];
  //$rif=$array[4];
   //$banco=$array[5];
   //$tituloPagina=$_SESSION["page"];
   $usuario=$_SESSION["session_user"];
   $busrecepcion= isset($_SESSION['bsqrecepcion']  ) ? $_SESSION['bsqrecepcion']  : "" ;
   $buscarrif= isset($_SESSION['buscarrif']  ) ? $_SESSION['buscarrif']  : "" ;

// $re=$_REQUEST["retorno"];

   $controlador =new controladorEncuesta();
   $datosencueta=$controlador->mostrardetallencuesta($idcliente);
   $total = pg_num_rows($datosencueta);
   $row = pg_fetch_assoc($datosencueta);
   $co=$controlador->sp_verregistroencuesta($idcliente);
   $d= pg_fetch_assoc($co);
   $activado=$d["activo"];
   $id_registro_session=$d["idcliente"];
   //echo $id_registro_session;
   $_SESSION["id_registro_sessionenc"]= $id_registro_session;

   $consultaestatus=$controlador->sp_estatusactual($idcliente);
   $estatusactual=pg_fetch_assoc($consultaestatus);

   $consultahistorial=$controlador->sp_registrohistorial($idcliente);
   $cantidadregistros=pg_fetch_assoc($consultahistorial);

   $consultaregistro=$controlador->sp_cantidaderegistros($idcliente);
   $cantidadpreguntas=pg_fetch_assoc($consultaregistro);

   $consultacontacto=$controlador->sp_verpcontacto($idcliente);
   $upcontacto = pg_fetch_assoc($consultacontacto);

   $enlace= $row['nafiliacion'].'/'.$namearchivo.'/'.$fechacarga.'/'.$idcliente.'/'.$rif.'/'.$banco.'/'.$busqueda.'/'.$bancoparametrizacion.'/'.$estatusparametrizacion;

   $enlareresultado=$banco.'/'.$idcliente.'/'.$namearchivo.'/'.$fechacarga.'/'.$rif.'/'.$busqueda.'/'.$bancoparametrizacion.'/'.$estatusparametrizacion.'/'.$nroafiliacion;
   
   $datainventario=$controlador->mostrarinventarioposafiliado($idcliente); 
   $totalposengestion = pg_num_rows($datainventario);

   $validacionDireccion = $controlador->verdireccionInstalacion($idcliente);
   $validacion= pg_fetch_array($validacionDireccion);
   $valor = $validacion['spverdireccioninstalacion'];

   $varinst = $controlador -> sp_buscartipousuario($usuario);
   $varvarvol = pg_fetch_array($varinst);

  
   $retroceder=$nroafiliacion.'/'.$namearchivo;

//enlaces volver
   $volver1=$banco.'/'.$fechaBusqueda.'/'.$namearchivo.'/'.$busqueda;
   $volver2=$banco.'/';

$varvolveradm=$idcliente.'/'.$rif.'/'.$banco.'/'.$fechacarga.'/'.$namearchivo.'/'.$varvarvol['tipousuario'].'/'.$varvarvol['tipoejecutivo'].'/'.$usuario.'/'.'2';

$varvolver=isset($_SESSION['configpos']) ? $_SESSION['configpos'] : ""; ;

 if ($activado==0) {
    
  /*Coloca el registro en estatus activo*/
  $activo=1;
  $ctrl =new controladorEncuesta();
  $actregistro=$ctrl->sp_actividadencuesta($idcliente,$activo,$usuario);
   }
   if($activado==1)
   { 
$_SESSION["id_registro_sessionenc"]="0";
    ?>
 <script type="text/javascript">

    var buscarrif="<?php echo $buscarrif; ?>";
    var busrecepcion="<?php echo $busrecepcion; ?>";

    alertify.error("Los datos del Afiliado estan siendo editados por otro usuario, por favor seleccione otro afiliado");
    if (busrecepcion=="" && buscarrif!=""){

    setTimeout(function(){  window.location.href ='?cargar=buscarrifencuesta&var='+buscarrif; }, 6000); 
      }else{
      setTimeout(function(){  window.location.href ='?cargar=buscarbancoencuesta&var=<?php echo $busrecepcion; ?>'}, 7000); 
           }

 </script>                         
  <?php }
?>
<script languaje="javascript">
     mascaras();
  </script>
<style>
/*encuesta*/
/* oculto los elementos */
#respuesta2-3,#respuesta7-3,#datosreferencia {
display: none;
}
/* lo muestros si está seleccionado*/
#respuesta2-1:checked ~ #respuesta2-3,#respuesta7-2:checked ~ #respuesta7-3,#respuesta8-1:checked ~ #datosreferencia {
  display: block;
}

textarea {
    width: 300px;
    height: 80px;
    overflow-y: scroll;
    resize: none;
    align-self: center;
    vertical-align: center;
}

#respuesta13-1{
  width: 500px;
}

#respuesta1-1{
  padding: 5px 0 5px 60px;
}
.clearfix{
    float: none;
    clear: both;
}

</style>

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-phone-square" style="color:#F8F9F9;"> </i> Encuesta de Satisfaccion - Cliente: <?php echo $row['coddocumento']." ".$row['razonsocial'];?>  </a>
        </div>
       </nav>
      </div>
      <?php 
        if (($estatusactual['sp_estatusactual']==1)AND ($cantidadpreguntas['sp_cantidaderegistros']>=12)) {
      ?>
      <div class="alert alert-success alert-dismissable">
        La encuesta fue completada para este cliente . 
      </div>
      <?php
        } else if ($estatusactual['sp_estatusactual']==4) {
      ?>
      <div class="alert alert-danger alert-dismissable">
        <b>El cliente a rechazado la encuesta.</b>
      </div>
      <?php
        } else {
      ?>
      <div class="alert alert-danger alert-dismissable">
        Debe completar la encuesta.
        <a href="#" data-toggle="modal" data-target="#modalencuesta" class="alert-link">Realizar la encuesta.</a>
      </div>
      <?php
        }
      ?>  
      <!--nuevo bloque-->
      <div class="row">
        <!--Panel datos basicos-->
        <div class="col-lg-8">
          <div class="panel panel-info">
            <div class="panel-heading" style="text-align: center;">
              <i class="fa fa-user fa-fw"></i>Datos Básicos
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="list-group">
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Rif:
                  <span class="pull-center text-muted small"><b><?php echo $row['coddocumento'];?></b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Nº Afiliado:
                  <span class="pull-center text-muted small"><b><?php echo $row['nafiliacion']; ?></b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Razón Social:
                  <span class="pull-center text-muted small"><b><?php echo $row['razonsocial'];?></b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> N° Factura:
                  <span class="pull-center text-muted small"><b><?php echo $row['factura'];?></b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Representante Legal:
                  <span class="pull-center text-muted small"><b><?php echo  $row['rlegal'] ;?></b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Marca de Pos:
                  <span class="pull-center text-muted small"><b><?php echo isset($row['marcapos']) ? $row['marcapos'] : "" ;?> </b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Tipo de Pos:
                  <span class="pull-center text-muted small"><b><?php echo isset($row['tipopos']) ? $row['tipopos'] : "" ;?> </b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Cantidad de Pos:
                  <span class="pull-center text-muted small"><b><?php echo isset($row['cant_pos_confirm']) ? $row['cant_pos_confirm'] : "" ;?> </b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Dirección:
                 <span class="pull-center text-muted small" rows="4" ><b><?php echo $row['direccion'];?></b>
                  </span>
                </div>
                <div class="list-group-item">
                  <i class="fa fa-check fa-fw"></i> Estatus:
                  <span class="pull-center text-muted small" rows="4" ><b><?php echo $row['descestatus'];?></b>
                  </span>
                </div>
              </div>
              <!-- /.list-group -->
            </div>
            <!-- /.panel-body -->
          </div>
          <!-- /.panel -->
          <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <?php if ($busqueda==1) { ?>
                <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa.php?cargar=buscarbancoencuesta&var=<?php echo $volver1; ?>'">Volver</button>
              <?php } elseif ($busqueda==2) { ?>
                <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa.php?cargar=buscarbancoencuesta&var=<?php echo $volver2; ?>'">Volver</button>
              <?php } elseif ($busqueda==3) { ?>
                <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa.php?cargar=buscarrifencuesta&var=<?php echo $rif; ?>'">Volver</button>
              <?php } elseif ($busqueda==4) {?>
                <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa.php?cargar=buscarbancfechaencuesta&var=<?php echo $banco.'/'.$fechaBusqueda; ?>'">Volver</button>
              <?php }  ?>
            </div>
          </div>
        </div>
        <!-- end panel datos básicos -->

        <!-- Panel gestion y encuesta -->
        <div class="col-lg-4">
          <div class="panel panel-default">
            <div class="panel-heading" style="text-align: center;">
            <i class="fa  fa-edit fa-fw"></i> Editar
            </div>
            <div class="panel-body">
              <div class="list-group">
                <b class="list-group-item">
                  <fieldset>
                    <button type="button" onclick="modales();" class="btn btn-default btn-block" data-toggle="modal" id="btncontactmodal" data-target="#modalencuesta" >
                      Gestión Telefónica
                    </button>
                  </fieldset>
                </b>
                <b class="list-group-item">
                  <button type="button" onclick="window.location.href='reporteregistrosencuesta?cargar=DatosEncuestaReporte&var=<?php echo $enlareresultado; ?>'" class="btn btn-primary btn-block"  id="" <?php if ($estatusactual['sp_estatusactual']!=1) { ?> disabled <?php } ?> >
                    Resultado de la encuesta
                  </button> 
                </b>
                <b class="list-group-item">
                  <button type="button" onclick="window.location.href='reporteregistrosencuesta?cargar=reportehistorialencuesta&var=<?php echo $enlace; ?>'+'/'+'1'" class="btn btn-primary btn-block"  id="" <?php if ($cantidadregistros['sp_registrohistorial']<1) { ?> disabled <?php } ?> >
                    Historial de llamadas
                  </button>
                </b>
              </div>
              <button type="button" onclick="window.location.href='encuestaa?cargar=personaref&var=<?php echo $enlareresultado; ?>'" class="btn btn-default btn-block" >
                      Registrar referido
              </button>
            </div>
          </div>
        </div>
      </div>
      <!--MODAL REGISTRO Y ENCUESTA-->
      <div class="modal fade" id="modalencuesta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header default">
              <div style="float: left;">
                <button type="button" id="cerrar" value="10" onclick="SalirPermanecer(this)" class="btn btn-danger btn-circle btn-lg f" aria-label="Close" >
                <span aria-hidden="true">x</span>
                </button>
              </div>
              <div style=" width: 92%;">
                <h4 align="center" style="color:blue;"> Gestión Telefónica</h4>
              </div>
            </div>
            <div class="modal-body" style="text-align: center;">
              <!-- inicio panel datos de contacto -->
              <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <span class="alert-danger" id="resultcorreo"></span>
                      <h4 class="panel-title">
                        <button class="btn btn-warning" data-toggle="collapse" href="#collapsedatoscontacto"><span class="glyphicon glyphicon-plus"></span> Datos de Contacto</button>
                      </h4>
                  </div>
                  <div id="collapsedatoscontacto" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="form-group col-xs-4">
                        <input type="text" id="id_cliente" class="hidden" value="<?php echo $idcliente; ?>">
                        <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>" readonly>
                        <label>Teléfono #1</label>
                        <input class="form-control phone" type="tel" value="<?php echo isset($row['telf1']) ? $row['telf1'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Teléfono #2</label>
                        <input class="form-control phone" type="tel" value="<?php echo isset($row['telf2']) ? $row['telf2'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Teléfono #3</label>
                        <input class="form-control phone" type="tel"  name="tlfmovil1" onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" minlength="11" maxlength="11"  id="tlfmovil1" value="<?php echo isset($row['tlf_movil1']) ? $row['tlf_movil1'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Teléfono #4</label>
                        <input class="form-control phone" type="tel" name="tlfmovil2" id="tlfmovil2" onkeypress="return soloNumeros(event)" onkeyup="modificaciones();" minlength="11" maxlength="11" value="<?php echo isset($row['tlf_movil2']) ? $row['tlf_movil2'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Email Corporativo</label>
                        <input class="form-control" onkeyup="modificaciones();validarEmail(this);" type="email" name="email1" id="email1" value="<?php echo isset($row['email_corporativo']) ? $row['email_corporativo'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Email Comercial</label>
                        <input class="form-control" onkeyup="modificaciones();validarEmail(this)" name="email2" type="email"  id="email2" value="<?php echo isset($row['email_comercial']) ? $row['email_comercial'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Email Personal</label>
                        <input class="form-control" onkeyup="modificaciones();validarEmail(this)" name="email3" id="email3" value="<?php echo isset($row['email_personal']) ? $row['email_personal'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Repr. Legal</label>
                        <input class="form-control form-horizontal" type="text"onkeypress="return soloLetras(event)" onkeyup="modificaciones()" name="rlegal" id="rlegal" maxlength="50" value="<?php echo isset($row['rlegal']) ? $row['rlegal'] : "" ;?>" readonly>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Persona Contacto </label>
                        <input class="form-control form-horizontal" type="text" onkeypress="return soloLetras(event)" onkeyup="modificaciones()" name="pcontacto" id="pcontacto" maxlength="50" value="<?php echo isset($upcontacto['sp_verpcontacto']) ? $upcontacto['sp_verpcontacto'] : "" ;?>" autocomplete='off' <?php if ($estatusactual['sp_estatusactual']==1 || $estatusactual['sp_estatusactual']==4) { ?> readonly <?php } ?> >
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Estatus</label>
                        <select class="form-control form-horizontal"  name="estatusgestion" id="estatusgestion" onchange="modificaciones()" required <?php if ($estatusactual['sp_estatusactual']==1 || ($estatusactual['sp_estatusactual']==4 && $_SESSION["codtipousuario"]=="O")) { ?> disabled <?php } ?>>
                        </select>
                        <input type="hidden" id="valor" value="1" name="valor"/>
                        <input class="form-control hidden" id="estatusactual" value="<?php echo isset($estatusactual['sp_estatusactual']) ? $estatusactual['sp_estatusactual'] : "" ;?>" readonly>
                        <script languaje="javascript"> 
                        generarestatusencuesta(); 
                        </script>
                      </div>
                      <div class="form-group col-xs-8">
                        <label>Observaciones</label>
                        <input class="form-control form-horizontal" type="text" value="" id="observaciones" onchange="modificaciones()" autocomplete='off' <?php if ($estatusactual['sp_estatusactual']==1 || $estatusactual['sp_estatusactual']==4) { ?> readonly <?php } ?>>
                      </div>  
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary" type="submit" id="updatoscontacto" name="Actualizar" value="Actualizar Campos" onClick="contactogestionencuesta();" <?php if ((($estatusactual['sp_estatusactual']==1) AND ($cantidadpreguntas['sp_cantidaderegistros']>=12)) || ($estatusactual['sp_estatusactual']==4 && $_SESSION["codtipousuario"]=="O")) { ?> disabled <?php } ?> >Guardar</button>
                    </div>
                  </div>
                </div> 
              </div>
              <!-- end panel datos de contacto -->
              <!-- Inicio encuesta -->
              <div class="panel-group">
                <div class="panel panel-default" id="">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <button class="btn btn-warning" data-toggle="collapse" href="#accordionequipos_costos"<?php if ((($estatusactual['sp_estatusactual']==1) AND ($cantidadpreguntas['sp_cantidaderegistros']>=12))|| ($estatusactual['sp_estatusactual']==4)) { ?> disabled <?php } ?> ><span class="glyphicon glyphicon-plus"></span> Encuesta de Satisfacción
                      </button>
                    </h4>
                  </div>
                  <div id="accordionequipos_costos" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="list-group" style="display: inline-block; text-align: left; align-content: left; align-items: left;">
                        <div class="" style="display: inline-block; text-align: left; align-content: left; align-items: left;">
                          <ul class="encuesta" style="list-style: none;">
                            <input type="text" class="hidden" id="pregunta" value="1">
                            <input type="text" class="hidden" id="banco" value="<?php echo $banco;?>">
                            <form class="form-horizontal" method="POST" action="view/encuesta/guardarencuesta.php" autocomplete="off" id="encuesta-s">
                              <li>
                                 <label><b>1.-¿Se siente usted satisfecho con la atención brindada en Inteligensa?</b></label>
                                 <br>
                                 <input type="radio" name="pregunta1" id="respuesta1-1" value="1"><label for="respuesta1-1">Si</label><br>
                                 <input type="radio" name="pregunta1" id="respuesta1-2" value="2"><label for="respuesta1-2">No</label><br>
                                 <input type="radio" name="pregunta1" id="respuesta1-3" value="11"><label for="respuesta1-3">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>2.-¿Su equipo actualmente presenta alguna incidencia?</b></label>
                                <br>
                                <input type="radio" name="pregunta2" id="respuesta2-1" value="1"><label for="respuesta2-1" style="align-self: center; text-decoration-color: #0000000; left: 0">Si</label><br>
                                <textarea type="textarea" name="pregunta2" id="respuesta2-3" form="encuesta-s" onkeypress="return alpha(event)"></textarea> 
                                <input type="radio" name="pregunta2" id="respuesta2-2" value="2"><label for="respuesta2-2" style="align-self: center;">No</label><br>
                                <input type="radio" name="pregunta2" id="respuesta2-4" value="11"><label for="respuesta2-4">No sabe/No contesta</label><br>
                                <br>
                              </li>
                              <li>
                                <label><b>3.-¿Se siente satisfecho con la solución que le dio el técnico referente a su caso? </b></label> 
                                <br>
                                <input type="radio" name="pregunta3" id="respuesta3-1" value="1"><label for="respuesta3-1">Si</label><br>
                                <input type="radio" name="pregunta3" id="respuesta3-2" value="2"><label for="respuesta3-2">No</label><br>
                                <input type="radio" name="pregunta3" id="respuesta3-3" value="11"><label for="respuesta3-3">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>4.-¿Inteligensa cumplió con los tiempos de respuesta acordados al momento de la negociación?</b></label> 
                                <br>
                                <input type="radio" name="pregunta4" id="respuesta4-1" value="1"><label for="respuesta4-1" style="align-self: center; text-decoration-color: #0000000">Si</label><br>
                                <input type="radio" name="pregunta4" id="respuesta4-2" value="2"><label for="respuesta4-2" style="align-self: center;">No
                                </label><br>
                                <input type="radio" name="pregunta4" id="respuesta4-3" value="11"><label for="respuesta4-3">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>5.-¿Cómo ha sido la atención por parte de Inteligensa para resolver sus dudas, quejas y/o reclamos?</b></label> 
                                <br>
                                <input type="radio" name="pregunta5" id="respuesta5-5" value="3"><label for="respuesta5-5" style="align-self: center; text-decoration-color: #0000000">Excelente</label><br>
                                <input type="radio" name="pregunta5" id="respuesta5-1" value="4"><label for="respuesta5-1" style="align-self: center; text-decoration-color: #0000000">Buena</label><br>
                                <input type="radio" name="pregunta5" id="respuesta5-2" value="5"><label for="respuesta5-2" style="align-self: center;">Regular</label><br>
                                <input type="radio" name="pregunta5" id="respuesta5-3" value="6"><label for="respuesta5-3" style="align-self: center;">Mala</label><br>
                                <input type="radio" name="pregunta5" id="respuesta5-4" value="11"><label for="respuesta5-4">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>6.-¿Qué mejoraría de nuestro servicio?</b></label> 
                                <br>
                                <textarea type="textarea" name="pregunta6" id="respuesta6"  form="encuesta-s" onkeypress="return alpha(event)"></textarea>
                              </li>
                              <li>
                                <label><b>7.-¿Tiene pensado volver adquirir un equipo de punto de venta con otro de nuestros bancos aliados? Si la respuesta es "No" indique el por qué</b></label> 
                                <br>
                                <input type="radio" name="pregunta7" id="respuesta7-1" value="1"><label for="respuesta7-1" style="align-self: center; text-decoration-color: #0000000;">Si</label><br>
                                <input type="radio" name="pregunta7" id="respuesta7-2" value="2"><label for="respuesta7-2" style="align-self: center;">No</label><br>
                                <textarea type="textarea" name="pregunta7" id="respuesta7-3" form="encuesta-s" onkeypress="return alpha(event)"></textarea>
                                <input type="radio" name="pregunta7" id="respuesta7-4" value="11"><label for="respuesta7-4">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>8-.¿Cómo fue la atención para resolver sus dudas?</b></label> 
                                <br>
                                <input type="radio" name="pregunta8" id="respuesta8-1" value="3"><label for="respuesta8-1" style="align-self: center; text-decoration-color: #0000000">Excelente</label><br>
                                <input type="radio" name="pregunta8" id="respuesta8-2" value="4"><label for="respuesta8-2" style="align-self: center;">Buena</label><br>
                                <input type="radio" name="pregunta8" id="respuesta8-3" value="5"><label for="respuesta8-3" style="align-self: center;">Regular</label><br>
                                <input type="radio" name="pregunta8" id="respuesta8-4" value="6"><label for="respuesta8-4" style="align-self: center;">Mala</label><br>
                                <input type="radio" name="pregunta8" id="respuesta8-5" value="11"><label for="respuesta8-5">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>9-.¿Al momento de comprar el punto de venta le fue informado que debía cancelar y domiciliar el servicio de post venta?</b></label> 
                                <br>
                                <input type="radio" name="pregunta9" id="respuesta9-1" value="1"><label for="respuesta9-1" style="align-self: center; text-decoration-color: #0000000">Si</label><br>
                                <input type="radio" name="pregunta9" id="respuesta9-2" value="2"><label for="respuesta9-2" style="align-self: center;">No</label><br>
                                <input type="radio" name="pregunta9" id="respuesta9-3" value="11"><label for="respuesta9-3">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>10-. ¿Cómo describiría la experiencia de nuestros agentes Intelipunto?</b></label>
                                <br>
                                <input type="radio" name="pregunta10" id="respuesta10-4" value="3"><label for="respuesta10-4" style="align-self: center; text-decoration-color: #0000000">Excelente</label><br>
                                <input type="radio" name="pregunta10" id="respuesta10-1" value="4"><label for="respuesta10-1" style="align-self: center; text-decoration-color: #0000000">Buena</label><br>
                                <input type="radio" name="pregunta10" id="respuesta10-5" value="5"><label for="respuesta10-5" style="align-self: center;">Regular</label><br>
                                <input type="radio" name="pregunta10" id="respuesta10-2" value="6"><label for="respuesta10-2" style="align-self: center;">Mala</label><br>
                                <input type="radio" name="pregunta10" id="respuesta10-3" value="11"><label for="respuesta10-3">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>11-.¿Cómo conoció Intelipunto?</b></label> 
                                <br>
                                <input type="radio" name="pregunta11" id="respuesta11-1" value="7"><label for="respuesta11-1" style="align-self: center; text-decoration-color: #0000000">Recomendacion</label><br>
                                <input type="radio" name="pregunta11" id="respuesta11-2" value="8"><label for="respuesta11-2" style="align-self: center;">Redes sociales</label><br>
                                <input type="radio" name="pregunta11" id="respuesta11-3" value="9"><label for="respuesta11-3" style="align-self: center;">Vallas</label><br>
                                <input type="radio" name="pregunta11" id="respuesta11-4" value="10"><label for="respuesta11-4" style="align-self: center;">Internet</label><br>
                                <input type="radio" name="pregunta11" id="respuesta11-5" value="11"><label for="respuesta11-5">No sabe/No contesta</label><br>
                              </li>
                              <li>
                                <label><b>12-.Sugerencias o comentarios que tenga el cliente</b></label> 
                                <br>
                                <textarea type="textarea" name="pregunta12" id="respuesta12" form="encuesta-s" onkeypress="return alpha(event)"></textarea><br>
                              </li>
                              <li>
                                <h1 align="center">¿Desea culminar la Encuesta?</h1>
                              </li>
                            </form>
                          </ul>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="clearfix"></div> 
                    </div>
                    <div class="panel-footer">
                      <div class="botonprev" style="text-align: left;  align-self: left; float: left;">
                        <li class="btn btn-primary" id="leftq" ><span class="PREV" >PREV</span></li>
                      </div> 
                      <div class="botonnext" style="text-align: right;  align-self: right;">
                        <li class="btn btn-primary" id="rightq"><span class="NEXT" >NEXT</span></li>
                      </div>
                      <div class="finish" style="text-align: right;  align-self: right;">
                        <li class="btn btn-primary" id="finishq"><span class="end" >Finalizar</span></li>
                      </div>
                    </div> 
                  </div>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
      <!--MODAL AGREGAR PERSONAS REFERIDAS-->
      <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalref">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header default">
              <div style="float: left;">
                <button type="button" id="cerrar" value="9" onclick="SalirPermanecer(this)" class="btn btn-danger btn-circle btn-lg f" aria-label="Close" >
                <span aria-hidden="true">x</span>
                </button>
              </div>
              <div style=" width: 92%;">
                <h4 align="center" style="color:blue;">Club Intelipunto - Referidos</h4>
              </div>
            </div>
            <div class="modal-body" style="text-align: center;">
              <div class="panel-group" style=" width:50%;">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <span class="alert-danger" id="resultcorreo"></span>
                      <h4 class="panel-title">
                         Datos referido                      
                      </h4>
                  </div>
                  <div class="panel-body">
                    <div class="form-group col-xs-6">
                      <label style="align-self: left;">Nombre</label>
                        <input class="form-control " type="text" value="" onkeypress="return soloLetras(event)" onkeyup="modificaciones()" name="" id="nombreref" maxlength="10" style="text-align: center;">
                    </div>
                    <div class="form-group col-xs-6">
                      <label style="align-self: left;">Apellido</label>
                        <input class="form-control " type="text" value="" onkeypress="return soloLetras(event)" onkeyup="modificaciones()" name="" id="apellidoref" maxlength="10" style="text-align: center;">
                    </div>
                    <div class="form-group col-lg-12" >
                      <label style="align-self: left;">Numero referido</label>
                        <input class="form-control phone" type="tel" value="" keypress="return soloNumeros(event)" onkeyup="modificaciones()" minlength="11" maxlength="11" style="text-align: center;" id="telefonoref">
                    </div>
                  </div>
                  <div class="panel-footer">
                    <button class="btn btn-primary" type="submit" id="updatoscontacto" name="Actualizar" value="Actualizar Campos" onClick="guardarreferido();">Registrar</button>
                  </div>
                </div>
              </div>
              <div class="panel-group">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                         Referidos de <?php echo $row['razonsocial']; ?>                     
                      </h4>
                  </div>
                  <div class="panel-body">
                  </div>
                </div>
              </div>     
            </div>
          </div>
        </div>
      </div>
      <!--MODAL AGREGAR PERSONAS REFERIDAS-->
      <div class="container-fluid">
        <div class="modal fade" id="Modaleditarpagos" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content"  style="width:1100px;right: 40%;height: 100%;">
              <div class="modal-header">
                <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
                  <center>Agregar Datos de Pago</center>  
              </div>
              <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <div class="col-lg-12" style="margin-top:2px;">  
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Fecha Pago:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <span class="error" id="errorfechapagoe"></span>
                              <input type="date" name="fechapago" id="fechapagoedit" value="" class="form-control" placeholder="" tabindex="4" required>
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3 ">
                              <h5>Hora Pago:</h5>
                            </div>
                          <div class="col-xs-9 col-sm-9 col-md-9 ">
                            <input type="time" name="horapago" id="horapagoedit" class="form-control" placeholder="" tabindex="3">
                          </div>
                        </div>
                        </br>
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Banco Origen:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <select id="bancoasignadosedit" class="form-control">
                              </select>
                              <input class="form-control hidden" value="" id="bancoorigenedit">
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3 ">
                              <h5>Banco Destino:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <select id="todobancosedit" class="form-control">
                                </select>
                              <input class="form-control hidden" id="bancodestinoedit" value="">
                            </div>
                          </div>
                        </br>
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Forma pago:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <span class="error" id="errorformapagoe"></span>
                              <select  name="" id="formapagosedit"  class="form-control"></select>
                              <input class="hidden" id="formapagobedit" value="">

                            </div>
                          </div>
                          <div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3 ">
                              <h5>Moneda:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <span class="error" id="errormonedae"></span>
                                <select class="form-control" id="tipomonedaedit" >
                                             <option value="">Seleccionar</option>
                                             <option value="1" selected="true">Bs. S</option>
                                             <option value="2" id="usd">USD</option>
                               </select>
                            </div>
                          </div>
                        </br>
                        <br>
                        <span></span>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Monto USD:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" id="montousdedit" class="form-control" value="<?php echo isset($datapago['montousd']) ? $datapago['montousd'] : "" ;?>" tabindex="4" onkeypress="return soloNumeros(event)">
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="col-xs-3 col-sm-3 col-md-3 ">
                                <h5>Factor conversión:</h5>
                              </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" onkeypress="return soloNumeros(event)" value="" id="factorconversionedit" class="form-control" placeholder="" tabindex="3">
                            </div>
                          </div>
                        </br>
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Monto:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" onkeypress="return soloNumeros(event)" value="" id="montoedit" class="form-control" placeholder="" tabindex="4">
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3 ">
                                <h5>Referencia:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <span class="error" id="errorreferenciae"></span>
                              <input type="text" onkeypress="return alpha(event)" value="" id="referenciaedit" class="form-control" placeholder="" tabindex="3">
                            </div>
                          </div>
                        </br>
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Depositante:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <span class="error" id="errordepositantee"></span>
                              <span class="error" id="errordepositantemaxe"></span>
                              <input type="text" name="nombredepositante" value="" id="nombredepositanteedit" class="form-control" placeholder="" tabindex="4">
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3 ">
                                <h5>Capture:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" name="imagenpago" id="imagenpagoedit" class="form-control" placeholder="" tabindex="3">
                            </div>
                          </div>
                        </br>
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Obs. Comercial:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" name="observacioncomer" onkeypress="return alpha(event)" id="observacioncomeredit" value="" class="form-control" placeholder="" tabindex="4">
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3 ">
                                <h5>Obs. Administración:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" name="observacionadm" onkeypress="return alpha(event)" id="observacionadmedit" value="" class="form-control" placeholder="" tabindex="3">
                            </div>
                          </div>
                        </br>
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Estatus:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" name="" id="" class="form-control" value="" placeholder="" tabindex="4" readonly>
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3 ">
                                <h5>fecha carga:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="select" name="celular" id="celularedit" value="" class="form-control" placeholder="" tabindex="3" readonly>
                            </div>
                          </div>
                        </br>
                        <br>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                              <h5>Registro:</h5>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" name="" id="" value="" class="form-control" placeholder="" tabindex="4" readonly>
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6">
                              <div class="col-xs-3 col-sm-3 col-md-3 ">
                                <h5>Confirmacion:</h5>
                              </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 ">
                              <input type="text" name="celular" value="" id="celular" class="form-control" placeholder="" tabindex="3" readonly>
                            </div>
                          </div>
                        </br>
                      </div>
                    </table>
                  <div class="panel-footer" align="center">
                     <input type="button" class="btn btn-primary" id="btneditar" name="editar" value="Editar" >            
                   </div>
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>
