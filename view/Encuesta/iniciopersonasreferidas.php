<?php
  if (isset($_SESSION["session_user"]))
  {
    $usuario=$_SESSION["session_user"];
  }   

      $controlador =new controladorEncuesta();
      $resultados=$controlador->spbuscarpersonasreferidas();
      $databanco = pg_fetch_assoc($resultados);
      $count = pg_num_rows($resultados);
 
  ?>
<style type="text/css">
.No-Contactado{
    color:#f1f1f1; 
    background:orange;
    border-radius:0.2em;
    padding:.3em;
    container;
  }

    .Contactado{
    color:white; 
    background:#01DF3A;
    border-radius:0.2em;
    padding:.3em;
    container;
  }


  </style>

  <?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectpersonref();
</script>
<?php }else { ?>


 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="fa fa-edit"> </i> Encuesta: Personas Referidas<b style="color:#FF5733;">
          </b></a>
        </div>
       </nav>
    </div>
    <div class="row" align="center">
      <div class="col-12">
        <table class="table table-bordered"  id="personasreferidas"> 
           <thead>
              <tr>               
                <th>N°</th>
                 <th>ID</th>
                <th>enlace</th>
                <th>Acción</th>
                <th>&nbsp;&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Servicio&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Nombre&nbsp;Referido&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;Referido&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;(Cliente)&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Representante&nbsp;Legal&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Teléfono 1&nbsp;&nbsp;(Cliente)&nbsp;</th>
                <th>&nbsp;&nbsp;Teléfono 2&nbsp;&nbsp;(Cliente)&nbsp;</th>
                <th>&nbsp;&nbsp;Banco&nbsp;&nbsp;Encuestado</th>
                <th>idbanco</th>
              </tr>
           </thead>
        </table>
 
      </div> 
    </div>
      <br>

<?php  } ?>


  <script type="text/javascript">
  function dtCore(){


 // var banco= document.getElementById('banco').value;
/*  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;*/

  $('table#personasreferidas').DataTable({
    "ajax": {
    "url": "api/Selectbuscarperaonasref.php",  
          "type": "POST"
    },
    "columns": [
      
      {"data": "correlativo", className: "text-center"},
      {"data": "idclientes", className: "text-center" },
      {"data": "idreferido", className: "text-center" },
      {"defaultContent": "<p class='abrir'><a class='abrir' type='href' title='Ver'>Ver Detalles</a></p>", className: "text-center" },
      {"data": "estatus", className: "text-center",
      "render" : function (data, type, full, meta, row) {
         if (data == "1" || data === "1"){
          return "<p class='Contactado'>Contactado</p>";
        }else if (data == "2" || data === "2"){
          return "<p class='No-Contactado'>No Contactado</p>";
        }else { 
          return "";}
        }
      },

      {"data": "nombreref", className: "text-center"},
      {"data": "numeroref", className: "text-center" },
      {"data": "razonsocialenc", className: "text-center" },
      {"data": "rifencuestado"},
      {"data": "nombreenc"},
      {"data": "telfenc", className: "text-center" },
      {"data": "tlfenc2", className: "text-center" },
      {"data": "bancoencuestado", className: "text-center" },
      {"data": "idbanco", className: "text-center" }
      
      
      
      
      
      
    ],
    "order" : [[0, "asc"]],
 "columnDefs": [
                 {
                       "targets": [1,2,13],
                       "visible": false,
                   }
               ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });


    $('table#personasreferidas tbody').on( 'click', 'a.abrir', function () {
    //alert('algo');
          var table=$('table#personasreferidas').DataTable();
          var D =  table.row($(this).parents("tr")).data();
          var idreferido=D.idreferido;
         
          var url = "CaptacionCliente?cargar=iniciocaptacioncliente&var="+idreferido; 
          $(location).attr('href',url);

    });

}

  </script>





