<?php
/*buscarrif.php view*/
 $encuesta=$_SESSION["encuesta"];
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
   $_SESSION['buscarrif']=$var;
  $_SESSION['bsqrecepcion']='';
  $array = explode("/",$var);
  $rif= $array[0];
  
  
  $controladorclient =new controladorEncuesta();
  $comercia= $controladorclient->spbuscarafiliadorifencuesta($rif);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);

?>
<?php
if ($count==0) {  ?>

<script type="text/javascript">
redirectResult();
</script>
<?php } else { ?>

<input type="hidden" id="nrorif"  name="rif"  value="<?php echo $rif; ?>" />
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $tecnicos; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="" style="color:#F8F9F9;"> </i> Busqueda por Rif: Resultados para la Encuesta </a>
         <br>
         <b style="color:#5DADE2;"> Búsqueda: <?php echo $rif; ?> </b>
        </div>
       </nav>
    </div>
    <div class="row">
    <div class="demo-container" ng-app="AppEncuestaRif" ng-controller="EncuestRifController">
        <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='encuestaa.php'"/>
        </div>
      </div>

    </div>
  </div>
</div>
<?php  } ?>
