<?php 
  $tipomodulo= $_SESSION["encuesta"];
   $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= isset($array[0]) ? $array[0] : "";
  $idcliente= isset($array[1])? $array[1] : "";
  $namearchivo= isset($array[2]) ? $array[2] : "";
  $fechacarga= isset ($array[3]) ? $array[3] : "";
  $rif= isset($array[4]) ? $array[4]  : "" ;
  $busqueda=isset($array[5] ) ? $array[5] : "" ;
  $bancoparametrizacion=isset($array[6] ) ? $array[6] : "" ;
  $estatusparametrizacion=isset($array[7] ) ? $array[7] : "" ;
  $nroafiliacion=isset($array[8] ) ? $array[8] : "" ;
   if (isset($_SESSION["session_user"]))
  {
  $usuario=$_SESSION["session_user"];
  }
  $_SESSION['idcliente']=$idcliente;
  $controlador =new controladorEncuesta();
  $datosencueta=$controlador->mostrardetallencuesta($idcliente);
  $row = pg_fetch_assoc($datosencueta);
  $enlace= $nroafiliacion.'/'.$namearchivo.'/'.$fechacarga.'/'.$idcliente.'/'.$rif.'/'.$banco.'/'.$busqueda.'/'.$bancoparametrizacion.'/'.$estatusparametrizacion;
?>
<script languaje="javascript">
     mascaras();
  </script>
<input type="text" id="idcliente" class="hidden" value="<?php echo $idcliente; ?>">
<input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>" readonly>
<input type="text" id="banco" class="hidden" value="<?php echo $banco; ?>">

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> <i class="fa fa-phone-square" style="color:#F8F9F9;"> </i> Registro referidos - Cliente: <?php echo $row['coddocumento']." ".$row['razonsocial'];?>  </a>
        </div>
       </nav>
      </div>
      <!--nuevo bloque-->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading" style="text-align: center;">
            <i class="fa  fa-edit fa-fw"></i> Registrar referido
            </div>
            <div class="panel-body">
            <div class="row">
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Nombre</label>
                  <input class="form-control" type="text" onkeypress="return soloLetras1(event)" onkeyup="modificaciones()" name="nombreref" id="nombreref" maxlength="50" autocomplete='off' required>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Apellido</label>
                  <input class="form-control" type="text" onkeypress="return soloLetras1(event)" onkeyup="modificaciones()" name="apellidoref" id="apellidoref" maxlength="50" autocomplete='off' required>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Teléfono</label>
                  <input class="form-control phone" type="tel" onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" name="telefonoref" id="telefonoref" minlength="15" maxlength="15" autocomplete='off' required>
                </div>
              </div>
              <div class="col-lg-3">
                <br>
                <input class="btn btn-primary btn-block"  type="button" name="editar" id="" value="Guardar" onclick="guardarreferido();"/> 
              </div>
            </div>
          </div>
          </div>
          <div class="panel-group">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h4 class="panel-title" align="center">
                  Referidos de <?php echo $row['razonsocial']; ?>                     
                </h4>
              </div>
              <div class="panel-body">
                <div class="row" align="center">
                  <!-- <div ng-app="AppregistroPersonasRef" ng-controller="registroPersonasRefcontroller" align="center">
                    <div class="demo-container">
                      <div id="gridContainer" dx-data-grid="gridOptions"></div>
                    </div>
                  </div> -->
                  <div class="col-12">
        <table class="table table-bordered"  id="registro_personasreferidas"> 
           <thead>
              <tr>        
                <th></th>     
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Número de Teléfono</th>
                <th>Ejecutivo</th>
                <th>Fecha</th>  
                <th></th>
                <th></th>            
              </tr>
           </thead>
        </table>
 
      </div> 
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4" align="center" >
          <?php if ($busqueda==1 || $busqueda==2 || $busqueda==3 || $busqueda==4) { ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='encuestaa?cargar=DatosEncuesta&var=<?php echo $enlace; ?>'">Volver</button>
          <?php } else if ($codbanco!=0){ ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta?cargar=buscarregistros&var=<?php echo $codbanco; ?>'">Volver</button>
          <?php } else { ?>
            <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='reporteregistrosencuesta?cargar=buscarregistros&var=<?php echo 0; ?>'">Volver</button>
          <?php } ?>  
        </div>
      </div>  
    </div>
  </div>
</div> 
</div>    

  <div class="container-fluid">
  <div class="modal fade" id="Modal_referidos" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <center>Editar personas referidas</center>
            </div>
   <input type="hidden" name="id_consecutivo" value="" id="id_consecutivo" class="form-control" placeholder="" tabindex="3" readonly>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Nombre</label>
                     <input type="text" class="form-control" id="nombre_ref">
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Apellido</label>
                    <input type="text" class="form-control" id="apellido_ref">
                    </div> 
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Teléfono</label>
                     <input type="text" class="form-control" id="telefono_ref">
                    </div>
                    </div>    
                </div>
           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <input type="button" class="btn btn-dark" id="btneditar" name="editar" value="Guardar" onclick="editarPersonasReferidas();">  
            </div>
       
        </div>
    </div>
  </div>
</div> 


  <script type="text/javascript">
  function dtCore(){


 var idcliente= document.getElementById('idcliente').value;
/*  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;*/

  tblpersonas= $('table#registro_personasreferidas').DataTable({
    "ajax": {
    "url": "api/personareferida.php",  
          "type": "POST",
          "data": {opcion:1, "idcliente":idcliente}
    },
    "columns": [

      {"data": "id_consecutivo", className: "text-center" }, 
      {"data": "nombre_ref", className: "text-center" }, 
      {"data": "apellido_ref", className: "text-center"},  
      {"data": "telefono_ref", className: "text-center"},
      {"data": "usuario_personaref", className: "text-center"},  
      {"data": "fecha_registro", className: "text-center"},
      {"defaultContent":" <button class='btn btn-danger btn-sm btnBorrar' type='button' id='eliminarpago'><i class='fa fa-trash'></i></button>", className: "text-center"},
      {"defaultContent":" <button class='Editar btn btn-primary' type='button' id='editar_referido' title='Editar'><i class='fa fa-edit'></i></button>"},

    ],
    "order" : [[0, "asc"]],

   "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend:'excelHtml5',
            text: 'Excel',          
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }     
  });


$(document).on("click", ".btnBorrar", function(){
    fila = $(this);        
    id_consecutivo =  parseInt($(this).closest('tr').find('td:eq(0)').text()) ;  
    opcion = 4; //eliminar    
    alert (id_consecutivo);    
    alertify.confirm("&#191;Desea eliminar este registro?", function (e){               
      if (e) {            
        $.ajax({
          url: "api/personareferida.php",
          type: "POST",
          datatype:"json",    
          data:  {"opcion":opcion, "id_consecutivo":id_consecutivo},    
          success: function() {
              tblpersonas.row(fila.parents('tr')).remove().draw();                  
           }
        }); 
      }
    });
  });

     $('table#registro_personasreferidas tbody').on( 'click', 'button.Editar', function () {

        var table=$('table#registro_personasreferidas').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        $("input#id_consecutivo").val(D.id_consecutivo)
        $("input#nombre_ref").val(D.nombre_ref)
        $("input#apellido_ref").val(D.apellido_ref)
        $("input#telefono_ref").val(D.telefono_ref)

        $(".modal-header").css( "background-color", "#17a2b8");
        $(".modal-header").css( "color", "white" );


        $('#Modal_referidos').modal('show');
  });

}
  </script>