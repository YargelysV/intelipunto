<?php
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array =explode("/",$var);
  $cliente= $array[0];
  $documento = $array[1];
  $cbanco= isset($array[2]) ? $array[2] :"";

  $bsqrecepcion= isset($_SESSION['bsqrecepcion']  ) ? $_SESSION['bsqrecepcion']  : "" ;
  $buscarrif= isset($_SESSION['buscarrif']  ) ? $_SESSION['buscarrif']  : "" ;
  //$buscarind= isset($_SESSION['buscarind']  ) ? $_SESSION['buscarind']  : "" ;

  $controlador =new ControladorServicioMantenimiento();
  $servicio=$controlador->mostrarservicio($cliente, $documento);
  $row = pg_fetch_assoc($servicio);

  ?>

  
<input type="hidden" id="cliente"  name="cliente"  value="<?php echo $cliente; ?>" />
<input type="hidden" id="documento"  name="documento"  value="<?php echo $documento; ?>" />
<input type="hidden" id="razonsocial"  name="razonsocial"  value="<?php echo $row['razonsocial']; ?>">
<input type="hidden" id="cbanco"  name="cbanco"  value="<?php echo $cbanco; ?>">
<input type="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"> Servicio Mantenimiento<b style="color:#FF5733;"></b></a> 
          <br>
        </div>
       </nav>
    </div>
    
 <div class="row" align="center"> 
     <div class="col-lg-10" align="left">
      <div class="panel panel-info">
                    <div class="panel-heading" style="text-align: center;">
                    Datos del Afiliado
                    </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
              <div class="list-group">
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> ID:
              <span class="pull-center text-muted small"><b><?php echo $row['cliente'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Razón Social:
              <span class="pull-center text-muted small"><b><?php echo $row['razonsocial'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Documento:
              <span class="pull-center text-muted small"><b><?php echo $row['documento'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Nº de Afiliacion:
              <span class="pull-center text-muted small"><b><?php echo $row['nafiliacion'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Banco:
              <span class="pull-center text-muted small"><b><?php echo $row['ibp'];?> </b>
              </span>
              </div>
              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Estatus de Domiciliacion del Cliente:
              <span class="pull-center text-muted small"><b><?php echo $row['descestatus_dom'];?> </b>
              </span>
              </div>

              <div class="list-group-item">
              <i class="fa fa-check fa-fw"></i> Serial del POS:
              <button type="button" class="btn btn-link" data-toggle="modal" id="btninf_adicional"
                data-target="#inf_adicional" value="Agregar">Ver</button>
        
              </div>

              </div>

           </div>

       </div> 

     </div>
  </div>



<div class="row" >
  <div class="container-fluid">  
    <div class="col-lg-8 col-lg-offset-1">
      <input type="hidden" id="cliente"  name="cliente"  value="<?php echo $cliente; ?>" />
      <input type="hidden" id="documento"  name="documento"  value="<?php echo $documento; ?>" />
      <input type="hidden" id="razonsocial"  name="razonsocial"  value="<?php echo $row['razonsocial']; ?>">
      <input type="hidden" id="cbanco"  name="cbanco"  value="<?php echo $row['ibp']; ?>">
      <input type="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">


          <div class="panel panel-info" align="center">
                    <div class="panel-heading">Datos de Domiciliación</div>
                        
                    
                <div class="panel-body">
                    <div class="form-group">
                      <div class="col-lg-5">
                          <div class="form-group">
                            <label>Modalidad de pago</label>
                            <select class="form-control" name="codformap" id="codformap"> 
                                      
                          <?php

                         $controlador =new ControladorServicioMantenimiento();
                         $clientestatus=$controlador->sp_vermodalidadpago();
                         echo '<option value="">Seleccione</option>';         
                         while ($row3=pg_fetch_array($clientestatus)):
                                    
                          ?>
                            <option value="<?php echo $row3['codformap']?>" <?php
                             if ($row3['codformap']== $row['codformapago'])
                              {
                                echo "selected";
                              }?>><?php
                                echo $row3['nformap'];
                                ?>
                            </option>
                           <?php  endwhile; ?>        
                                      
                           </select>  
                          </div>
                      </div> 
                   

                       <div class="col-lg-5">
                        <div class="form-group">
                            <label>N° Cuenta</label>
                              <input type="text" class="form-control" name="ncuenta" id="ncuenta" maxlength="20" value="<?php echo $row['ncuenta'];?>" disabled>
                        </div>
                       </div> 

                      <div class="col-lg-5">
                        <div class="form-group">
                          <label>Estatus de Domiciliación</label>
                            <select class="form-control" name="codigoestatus" id="codigoestatus"> 
                                
                            <?php
                            $controlador =new ControladorServicioMantenimiento();
                            $clientestatus=$controlador->spverestatuscliente();
                            echo '<option value="">Seleccione</option>';         
                            while ($row3=pg_fetch_array($clientestatus)):
                             ?>   
                      
                            <option value="<?php echo $row3['codigoestatus']?>" <?php
                                if ($row3['codigoestatus']== $row['codestatus_dom'])
                              {
                                echo "selected";
                              }?>><?php
                                echo $row3['desc_estatus'];
                            ?>
                            </option>
                              <?php  endwhile; ?>        
                                  
                              </select>  
                        </div>
                      </div> 

                      <div class="col-lg-5">
                        <div class="form-group">
                      <label>Segmentación en la cartera POS:</label>
                       <br> 
                       <br>


                       <?php if ($row['id_actividad']==0){ ?>
                        <b>Activo:<input type="radio" name="actividad" id="actividad" value="0" checked/></b>
                        <b>Inactivo:<input type="radio" name="actividad" id="actividad" value="1" /></b>

                        <?php } else {?> 

                        <b>Activo:<input type="radio" name="actividad" id="actividad" value="0"></b>
                        <b>Inactivo:<input type="radio" name="actividad" id="actividad" value="1" checked/></b>

                     <?php } ?>
                       </div>
                      </div> 

                    </div>

                   </div>
                <div class="row" align="center">
                   <button type="button" class="btn btn-primary" onclick="guardardatoservicio();">Guardar</button>      
                </div>  
                <br>    
               </div>         
</div>

</div>
            
</div> 

      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <?php if ($bsqrecepcion!=''&& $buscarrif==""){   ?>
            <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='?cargar=buscarmantenimiento&var=<?php echo $bsqrecepcion; ?>'">Volver</button>
          <?php } else{  ?>
            <button type="button" class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='?cargar=buscarrif&var=<?php echo $buscarrif; ?>'">  Volver</button>
          <?php  } ?>

          </div>
      </div>

  <div class="modal fade" id="inf_adicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static"
  data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header yellow">
        <!--   <h5 class="modal-title" id="exampleModalLabel" color="blue">Resultado de la Gestión  </h5> -->
          <button type="button" class="btn btn-danger btn-circle btn-md f" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="modal-body">
          <div class="panel-body">

            <div class="col-lg-14">
              <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center;color:#ffffff;">
                    <h4>Seriales Asignados</h4>
                </div>
                <!-- /.panel-heading -->
              <div class="panel-body">       
        <div class="row">
          <div class="col-14">
            <table class="table table-bordered"  id="detalles_equipos" style="width: 80%"> 
              <thead>
                <tr>
                  <th>N°</th>
                  <th>ID</th>
                  <th>Serial</th>
                  <th>Estatus</th> 
                </tr>
              </thead>
            </table>
          </div>
        </div>
            </div> <!-- End panel-body -->
          </div>
        </div>
      </div>
    </div> <!-- End div modal -->
    </div>
  </div>
</div>



  <script languaje="javascript">
 seleccionarestatuscliente();  seleccionarbancoorigen();
  </script>



<script type="text/javascript">
  function dtCore() {
    cliente= document.getElementById("cliente").value;
    documento= document.getElementById("documento").value;
    //alert(banco); 
    $('#detalles_equipos').DataTable({

    
    "ajax": {
    "url": "view/ServicioMantenimiento/SelectDetallesEquipos.php",
          "type": "POST",
          "data": {"cliente":cliente, "documento":documento}
    },
    "columns": [
    {"data": "correlativo" },
    {"data": "idd" },
    {"data":"seriales"},
    {"data": "descestatus" }
    ],
    "order" : [[0, "asc"]],

      "info":     false,
      "scrollCollapse": false,
      "bPaginate": false,

      dom: 'Bfrtip',
      buttons: [ 'print'
      ],

      language:{
        "sProcessing":     "Procesando...",
         "sLengthMenu":     "Mostrar _MENU_ registros",
         "sZeroRecords":    "No se encontraron resultados",
         "sEmptyTable":     "Ningún dato disponible en esta tabla",
         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix":    "",
         "sSearch":         "Buscar:",
         "Print":           "Imprimir",
         "sUrl":            "",
         "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
         "oPaginate": {
           "sFirst":    "Primero",
           "sLast":     "Último",
           "sNext":     "Siguiente",
           "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }
    });

}
 
</script>