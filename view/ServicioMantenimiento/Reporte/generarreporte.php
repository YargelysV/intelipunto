<?php
  $servicio=$_SESSION["serviciomantenimiento"];
  $usuario=$_SESSION["session_user"];
  //$var=($_REQUEST['var']);
  //$_SESSION['bsqrecepcion']=$var;

  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $status= isset($array[0]) ? $array[0] :"";
  $modalidad= isset($array[1]) ? $array[1] :"";

  $controladorclient =new ControladorServicioMantenimiento();
  $comercia= $controladorclient->spbuscarreportedomiciliacion($status, $modalidad);
  $databanco = pg_fetch_assoc($comercia);
  $count = pg_num_rows($comercia);


  if ($count==0) {  ?>
?>

<script type="text/javascript">
redirectResult();
</script>

<?php }  else { ?>
?>


<input type="hidden" id="status"  name="status"  value="<?php echo $status; ?>" />
<input type="hidden" id="modalidad"  name="modalidad"  value="<?php echo $modalidad; ?>" />
<input type="hidden" id="permisos"  name="permisos"  value="<?php echo $servicio; ?>" />

 <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header" >
         <a class="navbar-brand" href="#" style="color:#F8F9F9;"><i class="" style="color:#F8F9F9;"> </i> Registros de Domiciliación. <!-- <?php if ($cliente=='0') {
       echo 'Todos los bancos';
     } else { echo $databanco['ibpbanco']; } ?>  --></a>
         <br>
         
        </div>
       </nav>
    </div>

      <div class="row">
      <div class="col-12">
        <table class="table table-bordered"  id="reporte_domiciliacion" style="width: 100%"> 
           <thead>
              <tr>
                <th>Consecutivo</th>
                <th>ID</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Rif/CI&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;Estatus&nbsp;de&nbsp;Servicio&nbsp;&nbsp;</th>
                <th>&nbsp;Modalidad&nbsp;</th>
                <th>Marca POS</th>
                <th>Tipo POS</th>
                
                
              </tr>
            </thead>
            <tfoot>
                <th colspan="4" style="text-align:left">Total:</th>
          </tfoot>
        </table>
      </div>
    </div>
      <div class="row">
          <div class="col-md-5 col-md-offset-5">
             <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='reporteserviciodomiciliacion.php'"/>
          </div>
      </div>

  </div>
</div>

<?php  } ?>


<script type="text/javascript">
  function dtCore(){
    
  var status=document.getElementById('status').value;
  var modalidad= document.getElementById('modalidad').value;

    $('table#reporte_domiciliacion').DataTable({
      "ajax": {
      "url": "view/ServicioMantenimiento/selectDomiciliacionReporte.php",
            "type": "POST",
            "data": {"status":status, "modalidad":modalidad}
      },
      "columns": [
        {"data": "sec", className: "text-center"},
        {"data": "cliente", className: "text-center"},
        {"data": "razon_social", className: "text-center" },
        {"data": "documento", className: "text-center" },
        {"data": "banco", className: "text-center" },
        {"data": "descpago", className: "text-center"},
        {"data": "descmodalidad", className: "text-center" },
        {"data": "marcaposs", className: "text-center" },
        {"data": "tipoposs", className: "text-center" }
      ],
      "order" : [[0, "asc"]],

            // "columnDefs": [
            //        {
            //              "targets": [ 1 ],
            //              "visible": false,
            //          }
            //      ],

            "scrollX": true,
            "scrollY": false,
            "info":     false,
            "scrollCollapse": false,
            
          //   dom: 'Bfrtip',
          //   buttons: [
          // {
          //   extend: 'excelHtml5', footer: true,
          //   text: 'Excel',        
          //   exportOptions : {
          //     columns: [ 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
          //   }
          // }],
            
            "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 1 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
            language:{
              "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ registros",
               "sZeroRecords":    "No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
               "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "Print":           "Imprimir",
               "sUrl":            "",
               "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
              "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
               "fixedHeader": {"header": false, "footer": false},
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
        });

}
  </script>
