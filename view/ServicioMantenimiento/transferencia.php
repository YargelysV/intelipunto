<?php
set_time_limit(0);
if (isset($_SESSION["session_user"]))
{

	/*echo "total var:".$total."<br>";
	echo "name var:".$name."<br>";
	echo "size var:".$size."<br>";*/
	$type=$_FILES['archivo']['type'];				
	$tipo="application/vnd.ms-excel";

	//Buscar tipos de Pos
	$controladortipopos =new ControladorServicioMantenimiento();
	$tipopos=$controladortipopos->vertipodepos();
	$tip = 0;
	$opcionestipopos=array();
	while($pos = pg_fetch_array($tipopos)){
	$opcionestipopos[$tip] = $pos['tipopos'];
	$tip++;
	}

	// $controladormarcapos =new ControladorServicioMantenimiento();
	// $marcapos=$controladortipopos->vermarcadepos();
	// $marc = 0;
	// $opcionestipopos=array();
	// while($mar = pg_fetch_array($marcapos)){
	// $opcionesmarcapos[$marc] = $mar['marcapos'];
	// $marc++;
	// }

	//BUSCAR TODOS LOS PREFIJOS DE LOS BANCOS
	$controladorbanco =new ControladorServicioMantenimiento();
	$prefijobanco=$controladorbanco->verprefijobanco();
	$pre = 0;
	$opcionesprefijo=array();
	while($prefijo = pg_fetch_array($prefijobanco)){
	$opcionesprefijo[$pre] = $prefijo['codigobanco'];
	//echo $opcionesprefijo[$pre]."<br>";
	$pre++;
	}

	$controladorclientesrep =new ControladorServicioMantenimiento();
	$clienterep=$controladorclientesrep->verclientesindividual();
	$clientact = 0;
	$clienteactivos=array();
	while($cliea = pg_fetch_array($clienterep)){
		$clienteactivos[$clientact] = $cliea['numterminal'];
				//echo $clienteactivos[$clientact]."<br>";
		$clientact++;
	}


	if ($type!=$tipo)
	{
		echo "<script>alertify.alert('Error Cargando el Archivo, Verifique e intente nuevamente.', function () {
			window.location.assign('serviciomantenimiento.php');});</script>";
	}
	else{
					$date = date("Y-n-j H:i:s");
					$archivo = $_FILES['archivo']['tmp_name'];
					$fichero = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
?>
	<div class="sub-content" align="center" >
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" id="errores">
<?php
					$cont=0;
					$control=0;
					while($data = fgetcsv($fichero,10000,";"))
					{
						$cont++;
						if($cont==1)
					
						{
							/*echo utf8_encode($data[0])."<br>";
							echo utf8_encode($data[1])."<br>";
							echo utf8_encode($data[2])."<br>";
							echo utf8_encode($data[3])."<br>";
							echo utf8_encode($data[4])."<br>";
							echo utf8_encode($data[5])."<br>";
							echo utf8_encode($data[6])."<br>";*/
										if ((utf8_encode($data[0])!="N° de Afiliacion") ||(utf8_encode($data[1])!="Razon Social") ||(utf8_encode($data[2])!="N° de Documento") || (utf8_encode($data[3])!="N° de Terminal") ||(utf8_encode($data[4])!="Serial POS") ||(utf8_encode($data[5])!="Banco") ||(utf8_encode($data[6])!="Tipo POS")||(utf8_encode($data[7])!="Marca POS"))
										{
										echo "<script>alertify.alert('El orden del formato es incorrecto', function () {
										window.location.assign('serviciomantenimiento.php?cargar=adjuntar');});</script>";	
										$control=2;
										}
						}
							
						else if (($cont>1) && ($control!=2))
						
						{
							if((in_array(($data[3]), $clienteactivos)))
										{
										echo '<h4 style="color:red;">'.'ERROR  "Registro ya existe en nuestro sistema" en la linea '.$cont.' verifique la información e intente nuevamente. </h4>
											- N° de Terminal: '.$data[3].'<br>';
										$control = 1;
										$_SESSION["comprobar"]=$control;
										}	

							
							if((strlen($data[0]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo N° DE AFILIACION en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							if((strlen($data[1]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo RAZON SOCIAL en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

							if((strlen($data[2]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo N° DE DOCUMENTO en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}

						
							if((strlen($data[3]) == ''))
							{
							echo '<h4 style="color:red;">'.'ERROR en campo N° DE TERMINAL en la linea '.$cont.'</h4><br>';
							$control = 1;	
							}


							if((strlen($data[6]) == '') || (!(in_array(($data[6]), $opcionestipopos))))
										{
										echo '<h4 style="color:red;">'.'ERROR en campo "Tipo de POS" en la linea '.$cont.'</h4><br>';
										$control = 1;	
										$_SESSION["comprobar"]=$control;
										}
						}		
					}	
	if($control==1)
	{
?>
	<table border="0" width="300" align="center">
				<tr>
					<td height="100" colspan="2" align="center">
					<font face="impact" size="5" color="red">
					Posibles Causas de error:<br>
					</font>
					</td>
				</tr>
					<td height="100" colspan="2" align="center">
					<font face="arial" size="4" color="green">
					- Verifique archivo .CSV<br>
					- Debe contener informaci&oacute;n en la primera columna.<br>
					- Ningun campo debe venir en blanco, y debe estar delimitado las columnas por punto y coma (;) .<br>
					- Consulte con el administrador del Sistema.<br>
					 </font>
					</td>
				</tr>
			<tr>
					<td height="100" colspan="1" align="center">
		<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='serviciomantenimiento?cargar=adjuntar'"/>
					</td>
				</tr>
		</table>
<?php
}
?>				</div>
			</div>
		</div>
	</div>				
<?php
					if($control == 0){
					$date = date("Y-n-j H:i:s");
					$archivo = $_FILES['archivo']['tmp_name'];
					$fichero1 = fopen($archivo,"r") or die("problemas al abrir el archivo .csv");
					$count=0;
					$controlador1 =new ControladorServicioMantenimiento();
					while($data1 = fgetcsv($fichero1,10000,";"))
					{
						$count++;
						if($count>1)
						{
							if ($data1[7]=='')
								 $data1[7]='0';

						echo 'faf';		
						$insertar=$controlador1->insertar_carga(trim($data1[0]),trim($data1[1]),trim($data1[2]),trim($data1[3]),trim($data1[4]),trim($data1[5]),trim($data1[6]),trim($data1[7]),$_SESSION["session_user"]);
						}
					}
					echo "<script>alertify.alert('Archivo Cargado con &Eacute;xito', function () {
					 window.location.assign('serviciomantenimiento.php');});</script>";	
					}	


	}
}
?>