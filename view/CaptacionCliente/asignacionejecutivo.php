<?php
//  session_start();
$tituloPagina=$_SESSION["page"];


if (isset($_SESSION["session_user"]))
{
    $usuario=$_SESSION["session_user"];
}

?>
<style type="text/css">
  #grid {
    height: 440px;
}

.options {
    padding: 20px;
    margin-top: 20px;
    background-color: rgba(191, 191, 191, 0.15);
}

.caption {
    margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
}

.option {
    margin-bottom: 10px;
}

.option > span {
    position: relative;
    top: 2px;
    margin-right: 10px;
}

.option > .dx-widget {
    display: inline-block;
    vertical-align: middle;
}

#requests .caption {
    float: left;
    padding-top: 7px;
}

#requests > div {
    padding-bottom: 5px;
}

#requests > div:after {
    content: "";
    display: table;
    clear: both;
}

#requests #clear {
    float: right;
}

#requests ul {
    list-style: none;
    max-height: 100px;
    overflow: auto;
    margin: 0;
}

#requests ul li {
    padding: 7px 0;
    border-bottom: 1px solid #dddddd;
}

#requests ul li:last-child {
    border-bottom: none;
}

</style>

<!-- Page Content -->
 <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">  


<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12"><h1> <i class="fa fa-phone-square" style="color:#045FB4"> Asignación Cliente </i></h1> </div>
     

<div class="row" align="center">
      <div class="col-lg-12" align="center">
          <div class="panel panel-info">
              <div class="panel-heading panel-primary" style="">
            <i class="fa fa-usser fa-fw" ></i> Registros 
          </div>
        <div ng-app="AppCaptacion" ng-controller="CaptacionController" align="center">
      <div class="demo-container">
         <div id="gridContainer" dx-data-grid="gridOptions"></div>
      </div>
      </div>
        </div>
    </div>
</div>

                    
                    
           </div>
      </div>
 </div>

<!--            
     </div>
  </div>
</div> -->

 <script languaje="javascript">
    seleccionarbancointeres();
  </script>

   <script languaje="javascript">
    function mayoracero() {
     var validacion= $('#cantidadpos').val();
      if (validacion<=0) {
        alertify.error('La cantidad del POS debe ser mayor a cero');
       
      }
      else{

        return true;
      
      }

     }
  </script>