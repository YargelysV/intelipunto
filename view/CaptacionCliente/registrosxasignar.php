<?php
//  session_start();
$tituloPagina=$_SESSION["page"];


if (isset($_SESSION["session_user"]))
{
    $usuario=$_SESSION["session_user"];
    $banco=$_REQUEST["var"];
    $ctrlcaptacion = new controladorCaptacion();
    $registros=$ctrlcaptacion->sp_registrosasignacionejecutivo($banco);
    $cantidad = pg_num_rows($registros);
}

?>
<style type="text/css">
  #grid {
    height: 440px;
}
#gridContainer .dec .diff {
    color: #f00;
}
.options {
    padding: 20px;
    margin-top: 20px;
    background-color: rgba(191, 191, 191, 0.15);
}

.caption {
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
}

.option {
    margin-bottom: 10px;
}

.option > span {
    position: relative;
    top: 2px;
    margin-right: 10px;
}

.option > .dx-widget {
    display: inline-block;
    vertical-align: middle;
}

#requests .caption {
    float: left;
    padding-top: 7px;
}

#requests > div {
    padding-bottom: 5px;
}

#requests > div:after {
    content: "";
    display: table;
    clear: both;
}

#requests #clear {
    float: right;
}

#requests ul {
    list-style: none;
    max-height: 100px;
    overflow: auto;
    margin: 0;
}

#requests ul li {
    padding: 7px 0;
    border-bottom: 1px solid #dddddd;
}

#requests ul li:last-child {
    border-bottom: none;
}
.diff{

  background: red;
  color: green;
}

.Por-asignar{
    
    color:white;
    background:RED;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }
</style>

<!-- Page Content -->
 <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">  
 <input class="" id="banco" name="banco" value="<?php echo $banco; ?>">  


   <div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12"><h1> <i class="fa fa-phone-square" style="color:#045FB4">Asignar Clientes a Ejecutivos</i></h1> </div>
     
<div class="row" align="center">
      <div class="col-lg-12" align="center">
          <div class="panel panel-primary">
              <div class="panel-heading panel-primary" style="">
            <i class="fa fa-usser fa-fw" ></i> Registros
          </div>
          <br>  

    <?php if ($cantidad!=0) { ?>

          <div class="col-12">
        <table class="table table-bordered"  id="asignacioncaptacion"> 
           <thead>
              <tr>
                <th></th>
                <th>N°</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Nº Afiliación</th>
                <th>Ejecutivo&nbsp;Asignado</th>  
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Medio&nbsp;Contacto&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Interés&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Usuario Carga</th>
                <th>Cantidad de POS</th>
                <th>Correo</th>                
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Tipo Cliente</th>
                <th>id</th>
              
              </tr>
            </thead>
        </table>
 
      </div>

        </div>
    </div>

          <?php   } else{ ?>

        <div class="alert alert-danger">
                               Error no existen resultados para la búsqueda.
                            </div>

       <?php   } ?>




</div>  
         </div>

       </div>

                 <div class="row">
        <div class="col-md-5 col-md-offset-5" >
            <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='captacioncliente?cargar=buscarcliente'"/>
        </div>
      </div>
  </div>



  <div class="container-fluid">
  <div class="modal fade" id="Modal_asignacion" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content"  style="width:1100px;right: 40%;height: 100%;">
        <div class="modal-header">
          <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
            <center>Captación Cliente-Registro</center> 
        </div>
        <div class="modal-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <div class="col-lg-12" style="margin-top:2px;">  
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Nº:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="consecutivo" id="consecutivo" value="" class="form-control" tabindex="4"readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <h5>Nombre</h5>
                      </div>
                    <div class="col-xs-9 col-sm-9 col-md-9 ">
                      <input  name="nombrecaptacion" id="nombrecaptacion" class="form-control" tabindex="3" value="" readonly>
                    </div>
                  </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Apellido:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="apellidocaptacion" class="form-control"  value="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Rif ó Cédula:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="documentocaptacion" class="form-control"  value="" tabindex="4" readonly>
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Nº Afiliado:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="afiliadocaptacion" class="form-control" value="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Asignar Ejecutivo</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                    <select type="select" id="desc_ejecutivo" class="form-control">
                          </select>
                   <input type="hidden" id="ejecutivocaptacion" class="form-control" value="" tabindex="4">
                      </div>
                    </div>
                  <br><br>
                  <br>
                  <span></span>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Teléfono:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errortelefono"></span>
                      <input type="text" value="" id="telefonocaptacion" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Razón Social:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorrazon"></span>
                        <input type="text"value="" id="razonsocialcaptacion" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Medio Contacto:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                       <select type="select" id="mcontacto" class="form-control" disabled>
                          </select>
                        <input type="hidden" name="medio_contacto" id="medio_contacto"   value="" class="form-control" value="" placeholder="" tabindex="4" > 
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Banco Interés:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="obancointeres" value="" id="obancointeres" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Banco Asignado:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select type="select" id="asignarbanco" class="form-control" disabled>
                          </select>
                        <input type="hidden" name="codbancoasignado" id="codbancoasignado" class="form-control"  placeholder="" tabindex="3" >
                      </div>
                    </div>
                  <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Usuario:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="usuario" value="" id="usuario" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Cantidad de POS:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcantidad"></span>
                        <input type="text" name="cantidadposscap" id="cantidadposscap" value="" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Correo:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcorreo"></span>
                        <input type="text" name="emailcap" id="emailcap" value="" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Dirección:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errordireccion"></span>
                        <input type="select" name="direccioncap" id="direccioncap" value="" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Tipo Cliente:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcliente"></span>
                        <select type="select" id="tcliente" class="form-control" disabled>
                          </select>
                        <input type="hidden" name="tipocliente" id="tipocliente" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    
                    <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Estatus:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="" value="" id="estatuscaptacion" class="form-control" placeholder="" tabindex="3" readonly >
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Correlativo:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="idregistro" value="" id="idregistro" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br>
                
              </table>
            <div class="panel-footer" align="center">
               <input type="button" class="btn btn-primary" id="btneditar" name="editar" value="Editar" onclick="asignacionejecutivo();">            
             </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  
seleccionarejecutivo();

</script>
 
  <script type="text/javascript">
  function dtCore(){

  var banco= document.getElementById('banco').value;


  $('table#asignacioncaptacion').DataTable({
    "ajax": {
    "url": "api/crudAsignacion.php",  
          "type": "POST",
          "data": {"banco":banco}
    },
    "columns": [
      
      {"defaultContent":" <button class='Editar btn btn-primary' type='button' id='editar' title='Editar'><i class='fa fa-edit'></i></button>"},
      {"data": "correlativo", className: "text-center"},
      {"data": "onombre", className: "text-center" },
      {"data": "oapellido", className: "text-center" },
      {"data": "ocoddocumento", className: "text-center" },
      {"data": "onafiliado", className: "text-center" },
      {"data": "oejecutivo", className: "text-center" ,
      "render" : function (data, type, full, meta, row) {
      if (data!=null) {
        return '<p class="">'+data+'</p>';
      }else {
        return "<p class='Por-asignar'>POR ASIGNAR</p>";
      }}},
      {"data": "otlf1", className: "text-center" },
      {"data": "orazonsocial", className: "text-center" },
      {"data": "omediocontacto", className: "text-center" , 
      "render" : function (data, type, full, meta, row) {
      if (data==5 || data===5) {
        return '<p class="">Cliente Potencial</p>';
      }else {
        return "<p>"+data+"</p>";
      }}},
      {"data": "obancointeres", className: "text-center" },
      {"data": "descbanco", className: "text-center" },
      {"data": "ousuario" , className: "text-center" },      
      {"data": "ocantposasignados", className: "text-center" },
      {"data": "ocorreorepresentantelegal", className: "text-center" },
      {"data": "odireccion_instalacion"},
      {"data": "otipocliente", className: "text-center" },     
      {"data": "oid_consecutivo", className: "text-center" },
      {"data": "ocodigobanco", className: "text-center" },
      {"data": "omediocontacto", className: "text-center" }
      

    ],
    "order" : [[0, "asc"]],
  "columnDefs": [
                 {
                       "targets": [18, 19],
                       "visible": false,
                   }
               ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5', footer: true,
            text: 'Excel',
            exportOptions : {
              columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21] 
              }         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

      $('table#asignacioncaptacion tbody').on( 'click', 'button.Editar', function () {

        var table=$('table#asignacioncaptacion').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        $("input#consecutivo").val(D.correlativo)
        $("input#nombrecaptacion").val(D.onombre)
        $("input#apellidocaptacion").val(D.oapellido)
        $("input#documentocaptacion").val(D.ocoddocumento)
        $("input#afiliadocaptacion").val(D.onafiliado)
        $("input#ejecutivocaptacion").val(D.oejecutivo)
        $("input#telefonocaptacion").val(D.otlf1)
        $("input#razonsocialcaptacion").val(D.orazonsocial)
        $("input#medio_contacto").val(D.omediocontacto)
        $("input#bcointeres").val(D.obancointeres)
        $("input#bancoasignado").val(D.descbanco)
        $("input#codbancoasignado").val(D.ocodigobanco)
        $("input#usuario").val(D.ousuario)
        $("input#cantidadposscap").val(D.ocantposasignados)
        $("input#emailcap").val(D.ocorreorepresentantelegal)       
        //$("input#medio_contacto").val(D.descontacto)
        $("input#direccioncap").val(D.odireccion_instalacion)
        $("input#tipocliente").val(D.otipocliente)
        
         
        $("input#idregistro").val(D.oid_consecutivo) // validar el banco  con el ID del registro
        var codBam=D.ocodigobanco;

        $.post( 'view/CaptacionCliente/bancosintereseditar.php',{postvariable: codBam}).done( function(respuesta){
          $('#asignarbanco').html(respuesta);   
        });

        $("input#asignarbanco").val("");

        $("input#idregistro").val(D.oid_consecutivo) // validar el medio de contacto con el ID del registro
        var codmedio=D.omediocontacto;

        $.post( 'view/CaptacionCliente/mediocontactoeditar.php',{postvariable: codmedio}).done( function(respuesta){
          $('#mcontacto').html(respuesta);   
        });

        $("input#mcontacto").val("");


        $("input#idregistro").val(D.oid_consecutivo) // validar el tipo de cliente con el ID del registro
        var codcliente=D.otipocliente;

        $.post( 'view/CaptacionCliente/tipoclienteeditar.php',{postvariable: codcliente}).done( function(respuesta){
          $('#tcliente').html(respuesta);   
        });

        $("input#tcliente").val("");

        $("input#idregistro").val(D.oid_consecutivo) // validar el tipo de cliente con el ID del registro
        var codejec=D.oejecutivo;

        $.post( 'view/CaptacionCliente/ejecutivoasignado.php',{postvariable: codejec}).done( function(respuesta){
          $('#desc_ejecutivo').html(respuesta);   
        });

        $("input#desc_ejecutivo").val("");

        $('#Modal_asignacion').modal('show');


  });

}
  </script>

