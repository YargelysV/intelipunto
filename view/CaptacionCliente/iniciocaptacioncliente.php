<?php
//  session_start();
$tituloPagina=$_SESSION["page"];
if (isset($_SESSION["session_user"]))
{
    $usuario=$_SESSION["session_user"];
}
?>
<style type="text/css">
  #grid {
    height: 440px;
}
.options {
    padding: 20px;
    margin-top: 20px;
    background-color: rgba(191, 191, 191, 0.15);
}
.caption {
    margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
}
.option {
    margin-bottom: 10px;
}
.option > span {
    position: relative;
    top: 2px;
    margin-right: 10px;
}
.option > .dx-widget {
    display: inline-block;
    vertical-align: middle;
}
#requests .caption {
    float: left;
    padding-top: 7px;
}
#requests > div {
    padding-bottom: 5px;
}
#requests > div:after {
    content: "";
    display: table;
    clear: both;
}
#requests #clear {
    float: right;
}
#requests ul {
    list-style: none;
    max-height: 100px;
    overflow: auto;
    margin: 0;
}
#requests ul li {
    padding: 7px 0;
    border-bottom: 1px solid #dddddd;
}
#requests ul li:last-child {
    border-bottom: none;
}

.Por-asignar{
    
    color:white;
    background:RED;
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
  }


  .asignado{
     
    color:white;
    background:'#01DF3A';
    text-align:center;
    border-radius:0.2em;
    padding:.3em;
    container;

  }

  .error {
  width : 100%;
  padding: 0;

  font-size: 80%;
  color: white;
  background-color: #EC1717;
  border-radius: 0 0 5px 5px;

  box-sizing: border-box;
}
</style>
<script>
    function mascaras(){
      $('.phone').mask('(0000)-000-0000');
      $('.money').mask('#.##0,00', {reverse: true});
    }
</script>
<!-- Page Content -->
<?php if(empty($_REQUEST['var'])){
 ?>
 <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">
 <input type="hidden" id="idregistro"  name="idregistro"  value="0" /> 
 <input type="hidden" id="idorigen"  name="idorigen"  value="0" />  
 
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12"><h1> <i class="fa fa-bullhorn x4" style="color:#045FB4"> Captación Cliente </i></h1> </div>    
<div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading primary" style="text-align: center;">
                           Registro de Clientes Potenciales
            
                        </div>
                    <div class="panel-body">
                      <div class="row">
                       <?php /*echo "área:".$_SESSION["area_usuario"]; ?> ||
                        <?php echo "usuario: ".$usuario; */ ?>            
  <input type="hidden" class="form-control" id="bancoxdefecto" name="bancoxdefecto" value="<?php if($usuario=='INTELIPUNTO'){echo "999"; }elseif($_SESSION["area_usuario"]==10 && $usuario!='INTELIPUNTO'){echo "997"; }else{echo ""; }  ?>" >
                                  <div class="col-lg-3">
                                   <div class="form-group">
                                    <label>Nombre</label>
                                      <input class="form-control" type="text"
                                onkeypress="return soloLetras1(event)" onkeyup="modificaciones()" name="nombre" id="nombre" maxlength="50">
                                  </div>
                                 </div>
                                  <div class="col-lg-3">             
                                   <div class="form-group">
                                     <label>Apellido</label>
                                      <input class="form-control" type="text"
                                onkeypress="return soloLetras1(event)" onkeyup="modificaciones()" name="apellido" id="apellido" maxlength="50">
                                    </div>  
                                  </div>
                                   <div class="col-lg-3"> 
                                    <label>Rif Cliente</label>
                                    <div class="form-inline">
                                    <div class="form-group">
                                         <select class="form-control" name="tipodoc" id="tipodoc">
                                   <!--  <option value="">Seleccione</option> -->
                                         <option value="V">V</option>
                                         <option value="E">E</option>
                                         <option value="J">J</option>
                                         <option value="P">P</option>
                                         <option value="G">G</option>
                                        </select>
                                      </div>
                                      <div class="form-group"> 
                                        <input class="form-control" type="text" name="rif" id="rif" maxlength="9" onkeypress="return soloNumeros(event)">
                                    </div>
                                  </div> 
                                  </div> 
                                  <div class="col-lg-3"> 
                                   <div class="form-group">
                                    <label>Teléfono</label>
                                      <input class="form-control phone" type="tel"
                                  onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" name="telf" id="telf" minlength="15" maxlength="15">
                                      </div>
                                     </div> 
                                    </div> 
                                    <div class="row">
                                    <div class="col-lg-3">  
                                     <div class="form-group">
                                         <label>Razón social</label>
                                          <input class="form-control" type="text" name="Razon_social" id="Razon_social" maxlength="199">
                                    </div>
                                   </div>
                                    <div class="col-lg-3"> 
                                     <div class="form-group">
                                        <label>Bancos de Interes</label>
                                          <input class="form-control" type="text" name="bancosinteres" id="bancosinteres" maxlength="199">
                                    </div>
                                   </div> 
                                  <div class="col-lg-3"> 
                                    <div class="form-group">
                                     <label>Banco Asignado</label>
                                         <select class="form-control" onchange="tipopos()" name="bancos" id="bancos">
                                         <option value="">Seleccione Banco</option>
                                        </select>
                                      </div>
                                    </div> 
                                  <div class="col-lg-3"> 
                                    <div class="form-group">
                                     <label>Tipo de POS</label>
                                         <select  id="tipopos" class="form-control" name="tipopos" required>
                                         <option value="">Seleccione POS</option>
                                        </select>
                                      </div>
                                    </div> 
                                    </div> 
                                   <div class="row">
                                  <div class="col-lg-3">
                                   <div class="form-group">
                                    <label>Cantidad de POS</label>
                                    <input class="form-control" type="text"
                                    onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" name="cantidadpos" id="cantidadpos" maxlength="50" onchange="mayoracero()">
                                  </div>
                                 </div>
                                  <div class="col-lg-3">             
                                   <div class="form-group">
                                     <label>Correo</label>
                                      <input class="form-control" type="email" onkeyup="modificaciones();" name="Correo" id="Correo" maxlength="50">
                                    </div>  
                                  </div>
                                   <div class="col-lg-3"> 
                                     <div class="form-group">
                                       <label>Dirección</label>
                                        <input class="form-control" type="text" name="Direccion" id="Direccion" maxlength="250">
                                    </div>
                                  </div>  
                                  <div class="col-lg-3"> 
                                   <div class="form-group">
                                    <label>Medio de Contacto</label>
                                      <select  id="medio_contacto" class="form-control"  name="medio_contacto" required>
                                         <option value="">Seleccione medio contacto</option>
                                        </select>
                                      </div>
                                     </div> 
                                    </div> 
                                <div class="row">
                                  <div class="col-lg-3">
                                   <div class="form-group">
                                    <label>Tipo cliente</label>
                                      <select  id="tipo_cliente" class="form-control"  name="tipo_cliente" required>
                                         <option value="">Seleccione tipo cliente</option>
                                      </select>
                                  </div>
                                 </div>
                                 <div class="col-lg-3"> 
                                     <div class="form-group">
                                       <label>Numero de Afiliación</label>
                                        <input class="form-control" type="text" name="n_afiliacion" id="n_afiliacion" maxlength="250">
                                    </div>
                                  </div> 
                                </div> 
                             </div>


   <input class="center-block btn btn-primary"  type="button" name="guardarcaptacion" id="guardarcaptacion" value="Guardar" onClick="RegistroCliente()"/> <br>
           
                       </div>
                 </div>
           </div>

           <div class="row" align="center">
      <div class="col-lg-12" align="center">
          <div class="panel panel-info">
              <div class="panel-heading panel-primary" style="">
            <i class="fa fa-usser fa-fw" ></i> Registros
          </div>
          <div class="alert alert-danger right">
            Debe completar todos los campos para poder realizar la asignación del banco
          </div> 
        
          <div class="col-12">
        <table class="table table-bordered"  id="busquedacaptacion"> 
           <thead>
              <tr>
                <th></th>
                <th>Fecha&nbsp;Carga</th>
                <th>Hora Carga</th>
                <th>Usuario Carga</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;</th>
                <th>Nº Afiliación</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Interés&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ejecutivo&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;</th>              
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Cantidad de POS</th>
                <th>Correo</th>
                <th>Medio Contacto</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Tipo Cliente</th>
              
              </tr>
            </thead>
        </table>
 
      </div>

        </div>
    </div>
</div>  
     </div>
  </div>

  <div class="container-fluid">
  <div class="modal fade" id="Modaleditar_registro" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content"  style="width:1100px;right: 40%;height: 100%;">
        <div class="modal-header">
          <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
            <center>Captación Cliente-Registro</center>  
        </div>
        <div class="modal-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <div class="col-lg-12" style="margin-top:2px;">  
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Fecha Carga:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="date" name="fecharegistro" id="fecharegistro" value="" class="form-control" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <h5>Hora Carga:</h5>
                      </div>
                    <div class="col-xs-9 col-sm-9 col-md-9 ">
                      <input  name="horarecepcion" id="horarecepcion" class="form-control" tabindex="3" value="">
                    </div>
                  </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Usuario Carga:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="usuario" class="form-control" disabled value="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Nombre:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errornombre"></span>
                        <input type="text" id="nombres" class="form-control"  value="" tabindex="4" >
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Apellido:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorapellido"></span>
                        <input type="text" id="apellidos" class="form-control" value="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Rif ó Cédula:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errordocumento"></span>
                   <input type="text" id="documento" class="form-control" value="" tabindex="4">
                      </div>
                    </div>
                  <br><br>
                  <br>
                  <span></span>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Número de Afiliación:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="afiliacion" class="form-control" value="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Teléfono:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errortelefono"></span>
                        <input type="text" value="" id="telefono" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Razón Social:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorrazon"></span>
                        <input type="text"value="" id="razonsocial" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Banco Interés:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorbancointeres"></span>
                        <input type="text"  value="" id="bancointeres" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Ejecutivo Asignado:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="ejecutivo" value="" id="ejecutivo" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Banco Asignado:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select type="select" id="asignarbanco" class="form-control">
                          </select>
                        <input type="hidden" name="codbancoasignado" id="codbancoasignado" class="form-control"  placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Cantidad de POS:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcantidad"></span>
                        <input type="text" name="cantidadposs" id="cantidadposs" value="" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Correo:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcorreo"></span>
                        <input type="text" name="email" id="email" value="" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Medio Contacto:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errormedio"></span>
                        <select type="select" id="mcontacto" class="form-control">
                          </select>
                        <input type="hidden" name="medio_contacto" id="medio_contacto"   value="" class="form-control" value="" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Dirección:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errordireccion"></span>
                        <input type="select" name="direccion" id="direccion" value="" class="form-control" placeholder="" tabindex="3" >
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Tipo Cliente:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcliente"></span>
                        <select type="select" id="tcliente" class="form-control">
                          </select>
                        <input type="hidden" name="tipocliente" id="tipocliente" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Usuario:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="usuario" value="" id="usuario" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                    <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Estatus:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="" value="" id="estatus" class="form-control" placeholder="" tabindex="3" readonly >
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Correlativo:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="idregistro" value="" id="idregistro" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br>
                
              </table>
            <div class="panel-footer" align="center">
               <input type="button" class="btn btn-primary" id="btneditar" name="editar" value="Editar" onclick="editarRegistroCaptacion();">            
             </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>

 <script languaje="javascript">
    validacioncaptacion(); seleccionarbancocaptacion();

</script>


<script type="text/javascript">
  function dtCore(){

  var usuario= document.getElementById('usuario').value;


  $('table#busquedacaptacion').DataTable({
    "ajax": {
    "url": "api/crudCaptacion.php",  
          "type": "POST",
          "data": {"usuario":usuario}
    },
    "columns": [
      
      {"defaultContent":" <button class='Editar btn btn-primary' type='button' id='editar' title='Editar'><i class='fa fa-edit'></i></button>"},
      {"data": "ofecharegistro", className: "text-center"},
      {"data": "horarecepcion", className: "text-center" },
      {"data": "ousuario", className: "text-center" },
      {"data": "onombre", className: "text-center" },
      {"data": "oapellido", className: "text-center" },
      {"data": "ocoddocumento", className: "text-center" },
      {"data": "onafiliado", className: "text-center" },
      {"data": "otlf1", className: "text-center" },
      {"data": "orazonsocial", className: "text-center" },
      {"data": "obancointeres", className: "text-center" },
      {"data": "oejecutivo", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data!=null) {
        return '<p class="">'+data+'</p>';
      }else {
        return "<p class='Por-asignar'>POR ASIGNAR</p>";
      }}},
      {"data": "descbanco", className: "text-center" },
      {"data": "ocantposasignados", className: "text-center" },
      {"data": "ocorreorepresentantelegal", className: "text-center" },
      {"data": "descontacto", className: "text-center" },
      {"data": "odireccion_instalacion", className: "text-center" },
      {"data": "otipocliente", className: "text-center" },
      {"data": "ocodigobanco", className: "text-center" },
      {"data": "idregistro", className: "text-center" },
      {"data": "omediocontacto", className: "text-center" }

    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
                 {
                       "targets": [18, 19,20],
                       "visible": false,
                   }
               ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
           extend: 'excelHtml5', footer: true,
            text: 'Excel',
            exportOptions : {
              columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21] 
              }         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

    $('table#busquedacaptacion tbody').on( 'click', 'button.Editar', function () {

        var table=$('table#busquedacaptacion').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        $("input#fecharegistro").val(D.ofecharegistro)
        $("input#horarecepcion").val(D.horarecepcion)
        $("input#nombres").val(D.onombre)
        $("input#apellidos").val(D.oapellido)
        $("input#documento").val(D.ocoddocumento)
        $("input#afiliado").val(D.onafiliado)
        $("input#telefono").val(D.otlf1)
        $("input#razonsocial").val(D.orazonsocial)
        $("input#bancointeres").val(D.obancointeres)
        $("input#ejecutivo").val(D.oejecutivo)
        $("input#bancoasignado").val(D.descbanco)
        $("input#codbancoasignado").val(D.ocodigobanco)
        $("input#cantidadposs").val(D.ocantposasignados)
        $("input#email").val(D.ocorreorepresentantelegal)
        $("input#medio_contacto").val(D.omediocontacto)
        //$("input#medio_contacto").val(D.descontacto)
        $("input#direccion").val(D.odireccion_instalacion)
        $("input#tipocliente").val(D.otipocliente)
        $("input#usuario").val(D.ousuario)

        
         
        $("input#idregistro").val(D.idregistro) // validar el banco  con el ID del registro
        var codBam=D.ocodigobanco;

        $.post( 'view/CaptacionCliente/bancosintereseditar.php',{postvariable: codBam}).done( function(respuesta){
          $('#asignarbanco').html(respuesta);   
        });

        $("input#asignarbanco").val("");

        $("input#idregistro").val(D.idregistro) // validar el medio de contacto con el ID del registro
        var codmedio=D.omediocontacto;

        $.post( 'view/CaptacionCliente/mediocontactoeditar.php',{postvariable: codmedio}).done( function(respuesta){
          $('#mcontacto').html(respuesta);   
        });

        $("input#mcontacto").val("");


        $("input#idregistro").val(D.idregistro) // validar el tipo de cliente con el ID del registro
        var codcliente=D.otipocliente;

        $.post( 'view/CaptacionCliente/tipoclienteeditar.php',{postvariable: codcliente}).done( function(respuesta){
          $('#tcliente').html(respuesta);   
        });

        $("input#tcliente").val("");

        $('#Modaleditar_registro').modal('show');


  });

}

  </script>

 <script languaje="javascript">
    seleccionarbancointeres(); mascaras();
  </script>

   <script languaje="javascript">
    function mayoracero() {
     var validacion= $('#cantidadpos').val();
      if (validacion<=0) {
        alertify.error('La cantidad del POS debe ser mayor a cero');
       
      }
      else{

        return true;
      
      }

     }
  </script>

<?php }elseif (isset($_REQUEST['var'])) {
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $idcliente= $array[0];
  //echo $idcliente;
  $controlador =new controladorCaptacion();
  $resultados=$controlador->spbuscarpersonasreferidas($idcliente);
  $databanco = pg_fetch_assoc($resultados);
  $estatusref= $databanco['estaturefcod'];

 ?>
 <input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">
 <input type="hidden" id="idregistro"  name="idregistro"  value="<?php echo $idcliente; ?>" />
 <input type="hidden" id="idorigen"  name="idorigen"  value="1" />
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12"><h1> <i class="fa fa-bullhorn x4" style="color:#045FB4"> Captación Cliente </i></h1> </div>    
<div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading primary" style="text-align: center;">
                           Registro de Clientes Potenciales
            
                        </div>
                    <div class="panel-body">
                      <div class="row">
                       <?php /*echo "área:".$_SESSION["area_usuario"]; ?> ||
                        <?php echo "usuario: ".$usuario; */ ?>            
  <input type="hidden" class="form-control" id="bancoxdefecto" name="bancoxdefecto" value="<?php if($usuario=='INTELIPUNTO'){echo "999"; }elseif($_SESSION["area_usuario"]==10 && $usuario!='INTELIPUNTO'){echo "997"; }else{echo ""; }  ?>" >
                                  <div class="col-lg-3">
                                   <div class="form-group">
                                    <label>Nombre</label>
                                      <input class="form-control" type="text"
                                onkeypress="return soloLetras1(event)" onkeyup="modificaciones()" name="nombre" id="nombre" maxlength="50" value="<?php echo  $databanco['nombrereferido']; ?>">
                                  </div>
                                 </div>
                                  <div class="col-lg-3">             
                                   <div class="form-group">
                                     <label>Apellido</label>
                                      <input class="form-control" type="text"
                                onkeypress="return soloLetras1(event)" onkeyup="modificaciones()" name="apellido" id="apellido" maxlength="50" value="<?php echo  $databanco['apellido']; ?>">
                                    </div>  
                                  </div>
                                   <div class="col-lg-3"> 
                                    <label>Rif Cliente</label>
                                    <div class="form-inline">
                                    <div class="form-group">
                                         <select class="form-control" name="tipodoc" id="tipodoc">
                                   <!--  <option value="">Seleccione</option> -->
                                         <option value="V">V</option>
                                         <option value="E">E</option>
                                         <option value="J">J</option>
                                         <option value="P">P</option>
                                         <option value="G">G</option>
                                        </select>
                                      </div>
                                      <div class="form-group"> 
                                        <input class="form-control" type="text" name="rif" id="rif" maxlength="9" onkeypress="return soloNumeros(event)">
                                    </div>
                                  </div> 
                                  </div> 
                                  <div class="col-lg-3"> 
                                   <div class="form-group">
                                    <label>Teléfono</label>
                                      <input class="form-control phone" type="tel"
                                  onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" name="telf" id="telf" minlength="15" maxlength="15" value="<?php echo  $databanco['numeroref']; ?>">
                                      </div>
                                     </div> 
                                    </div> 
                                    <div class="row">
                                    <div class="col-lg-3">  
                                     <div class="form-group">
                                         <label>Razón social</label>
                                          <input class="form-control" type="text" name="Razon_social" id="Razon_social" maxlength="199">
                                    </div>
                                   </div>
                                    <div class="col-lg-3"> 
                                     <div class="form-group">
                                        <label>Bancos de Interes</label>
                                          <input class="form-control" type="text" name="bancosinteres" id="bancosinteres" maxlength="199">
                                    </div>
                                   </div> 
                                  <div class="col-lg-3"> 
                                    <div class="form-group">
                                     <label>Banco Asignado</label>
                                         <select class="form-control" name="bancos" id="bancos">
                                         <option value="">Seleccione Banco</option>
                                        </select>
                                      </div>
                                    </div> 
                                  <div class="col-lg-3"> 
                                    <div class="form-group">
                                     <label>Tipo de POS</label>
                                         <select  id="tipopos" class="form-control"  name="tipopos" required>
                                         <option value="">Seleccione POS</option>
                                        </select>
                                      </div>
                                    </div> 
                                    </div> 
                                   <div class="row">
                                  <div class="col-lg-3">
                                   <div class="form-group">
                                    <label>Cantidad de POS</label>
                                    <span class="error" id="errorcantidad"></span>
                                     <input class="form-control" type="text"
                                  onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" name="cantidadpos" id="cantidadpos" maxlength="50" onchange="mayoracero()">
                                  </div>
                                 </div>
                                  <div class="col-lg-3">             
                                   <div class="form-group">
                                     <label>Correo</label>
                                      <input class="form-control" type="email" onkeyup="modificaciones();" name="Correo" id="Correo" maxlength="50">
                                    </div>  
                                  </div>
                                   <div class="col-lg-3"> 
                                     <div class="form-group">
                                       <label>Dirección</label>
                                        <input class="form-control" type="text" name="Direccion" id="Direccion" maxlength="250">
                                    </div>
                                  </div>  
                                  <div class="col-lg-3"> 
                                   <div class="form-group">
                                    <label>Medio de Contacto</label>
                                      <select  id="medio_contacto" class="form-control"  name="medio_contacto" required>
                                         <option value="">Seleccione medio contacto</option>
                                        </select>
                                      </div>
                                     </div> 
                                    </div> 
                                <div class="row">
                                  <div class="col-lg-3">
                                   <div class="form-group">
                                    <label>Tipo cliente</label>
                                      <select  id="tipo_cliente" class="form-control"  name="tipo_cliente" required>
                                         <option value="">Seleccione tipo cliente</option>
                                      </select>
                                  </div>
                                 </div>
                                 <div class="col-lg-3"> 
                                     <div class="form-group">
                                       <label>Numero de Afiliación</label>
                                        <input class="form-control" type="text" name="n_afiliacion" id="n_afiliacion" maxlength="250">
                                    </div>
                                  </div> 
                                </div> 
                <b class="list-group-item"  style="text-align: center;">
                <p style="text-align: center;"> Indique si fue contactado: </p>
                <label class="switch" id="switch">
                  <input type="checkbox" id="estatus" value="<?php echo  $databanco['estaturefcod']; ?>" onchange="personarefcpntactado();">
                  <span class="slider round"></span>
              </label>
              <label class="switch" id="magic_switch" style="display: none; text-align: center;">
                  <input type="checkbox" id="magic_switch_check" checked disabled="disabled">
                  <span class="slider round"></span>
              </label>
               </b>
          </div>

  <div class="col-md-5 col-md-offset-5">   
    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='personasreferidas.php'"/> 
    <?php if ($estatusref==2) {?>
      <input class="btn btn-primary" disabled type="button" name="guardarcaptacion" id="guardarcaptacion" value="Guardar" onClick="RegistroCliente()"/> <br> 
    <?php } else if ($estatusref==1) {  ?>       
       <input class="btn btn-primary" type="button" name="guardarcaptacion" id="guardarcaptacion" value="Guardar" onClick="RegistroCliente()"/> <br>
  
    <?php } ?>     
    </div>  
<div class="row" align="center">
      <div class="col-lg-12" align="center">
          <div class="panel panel-info">
              <div class="panel-heading panel-primary" style="">
            <i class="fa fa-usser fa-fw" ></i> Registros
          </div>
          <div class="alert alert-danger right">
            Debe completar todos los campos para poder realizar la asignación del banco
          </div>
                    <div class="col-12">
        <table class="table table-bordered"  id="busquedacaptacion"> 
           <thead>
              <tr>
                <th></th>
                <th>Fecha&nbsp;Carga</th>
                <th>Hora Carga</th>
                <th>Usuario Carga</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;</th>
                <th>Nº Afiliación</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Interés&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ejecutivo&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;</th>              
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Cantidad de POS</th>
                <th>Correo</th>
                <th>Medio Contacto</th>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>Tipo Cliente</th>
              
              </tr>
            </thead>
        </table>
 
      </div>

        </div>
    </div>
</div>            
                       </div>
                 </div>
           </div>
     </div>
  </div>
</div>

<div class="container-fluid">
  <div class="modal fade" id="Modaleditar_registro" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content"  style="width:1100px;right: 40%;height: 100%;">
        <div class="modal-header">
          <button type="button" class="btn btn-danger btn-circle btn-lg pull-right"  data-dismiss="modal">&times;</button>
            <center>Captación Cliente-Registro</center> 
        </div>
        <div class="modal-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <div class="col-lg-12" style="margin-top:2px;">  
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Fecha Carga:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="date" name="fecharegistro" id="fecharegistro" value="" class="form-control" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                        <h5>Hora Carga:</h5>
                      </div>
                    <div class="col-xs-9 col-sm-9 col-md-9 ">
                      <input  name="horarecepcion" id="horarecepcion" class="form-control" tabindex="3" value="">
                    </div>
                  </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Usuario Carga:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="usuario" class="form-control" disabled  value="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Nombre:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errornombre"></span>
                        <input type="text" id="nombres" class="form-control"  value="" tabindex="4" >
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Apellido:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorapellido"></span>
                        <input type="text" id="apellidos" class="form-control" value="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Rif ó Cédula:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errordocumento"></span>
                   <input type="text" id="documento" class="form-control" value="" tabindex="4">
                      </div>
                    </div>
                  <br><br>
                  <br>
                  <span></span>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Número de Afiliación:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" id="afiliacion" class="form-control" value="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Teléfono:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errortelefono"></span>
                        <input type="text" value="" id="telefono" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Razón Social:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorrazon"></span>
                        <input type="text"value="" id="razonsocial" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Banco Interés:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorbancointeres"></span>
                        <input type="text"  value="" id="bancointeres" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Ejecutivo Asignado:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="ejecutivo" value="" id="ejecutivo" class="form-control" placeholder="" tabindex="4" readonly>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Banco Asignado:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <select type="select" id="asignarbanco" class="form-control">
                          </select>
                        <input type="hidden" name="codbancoasignado" id="codbancoasignado" class="form-control"  placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Cantidad de POS:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcantidad"></span>
                        <input type="text" name="cantidadposs" id="cantidadposs" value="" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Correo:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcorreo"></span>
                        <input type="text" name="email" id="email" value="" class="form-control" placeholder="" tabindex="3">
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Medio Contacto:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errormedio"></span>
                        <select type="select" id="mcontacto" class="form-control">
                          </select>
                        <input type="hidden" name="medio_contacto" id="medio_contacto"   value="" class="form-control" value="" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Dirección:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errordireccion"></span>
                        <input type="select" name="direccion" id="direccion" value="" class="form-control" placeholder="" tabindex="3" >
                      </div>
                    </div>
                  <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <h5>Tipo Cliente:</h5>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <span class="error" id="errorcliente"></span>
                        <select type="select" id="tcliente" class="form-control">
                          </select>
                        <input type="hidden" name="tipocliente" id="tipocliente" class="form-control" placeholder="" tabindex="4">
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Usuario:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="usuario" value="" id="usuario" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                    <br><br>
                  <br>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Estatus:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="" value="" id="estatus" class="form-control" placeholder="" tabindex="3" readonly >
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="col-xs-3 col-sm-3 col-md-3 ">
                          <h5>Correlativo:</h5>
                        </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 ">
                        <input type="text" name="idregistro" value="" id="idregistro" class="form-control" placeholder="" tabindex="3" readonly>
                      </div>
                    </div>
                  <br>
                
              </table>
            <div class="panel-footer" align="center">
               <input type="button" class="btn btn-primary" id="btneditar" name="editar" value="Editar" onclick="editarRegistroCaptacion();">            
             </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" align="center">
                <div class="col-md-4 col-md-offset-4">
                 
                    <input type="button" class="btn btn-default"  name="volver" value="Volver" onClick="window.location.href='personasreferidas.php'"/>
                  
                </div>
            </div>

 <script languaje="javascript">
    validacioncaptacion(); seleccionarbancocaptacion();

</script>

<script type="text/javascript">
  function dtCore(){

  var usuario= document.getElementById('usuario').value;


  $('table#busquedacaptacion').DataTable({
    "ajax": {
    "url": "api/crudCaptacion.php",  
          "type": "POST",
          "data": {"usuario":usuario}
    },
    "columns": [
      
      {"defaultContent":" <button class='Editar btn btn-primary' type='button' id='editar' title='Editar'><i class='fa fa-edit'></i></button>"},
      {"data": "ofecharegistro", className: "text-center"},
      {"data": "horarecepcion", className: "text-center" },
      {"data": "ousuario", className: "text-center" },
      {"data": "onombre", className: "text-center" },
      {"data": "oapellido", className: "text-center" },
      {"data": "ocoddocumento", className: "text-center" },
      {"data": "onafiliado", className: "text-center" },
      {"data": "otlf1", className: "text-center" },
      {"data": "orazonsocial", className: "text-center" },
      {"data": "obancointeres", className: "text-center" },
      {"data": "oejecutivo", className: "text-center", 
      "render" : function (data, type, full, meta, row) {
      if (data!=null) {
        return "<p class='asignado'>"+data+"</p>";
      }else {
        return "<p class='Por-asignar'>POR ASIGNAR</p>";
      }}},
      {"data": "descbanco", className: "text-center" },
      {"data": "ocantposasignados", className: "text-center" },
      {"data": "ocorreorepresentantelegal", className: "text-center" },
      {"data": "descontacto", className: "text-center" },
      {"data": "odireccion_instalacion", className: "text-center" },
      {"data": "otipocliente", className: "text-center" },
      {"data": "ocodigobanco", className: "text-center" },
      {"data": "idregistro", className: "text-center" },
      {"data": "omediocontacto", className: "text-center" }

    ],
    "order" : [[0, "asc"]],

    "columnDefs": [
                 {
                       "targets": [18, 19,20],
                       "visible": false,
                   }
               ],

    "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            text: 'Copy to div',         
        }
    ],
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });

 $('table#busquedacaptacion tbody').on( 'click', 'button.Editar', function () {

        var table=$('table#busquedacaptacion').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        $("input#fecharegistro").val(D.ofecharegistro)
        $("input#horarecepcion").val(D.horarecepcion)
        $("input#nombres").val(D.onombre)
        $("input#apellidos").val(D.oapellido)
        $("input#documento").val(D.ocoddocumento)
        $("input#afiliado").val(D.onafiliado)
        $("input#telefono").val(D.otlf1)
        $("input#razonsocial").val(D.orazonsocial)
        $("input#bancointeres").val(D.obancointeres)
        $("input#ejecutivo").val(D.oejecutivo)
        $("input#bancoasignado").val(D.descbanco)
        $("input#codbancoasignado").val(D.ocodigobanco)
        $("input#cantidadposs").val(D.ocantposasignados)
        $("input#email").val(D.ocorreorepresentantelegal)
        $("input#medio_contacto").val(D.omediocontacto)
        //$("input#medio_contacto").val(D.descontacto)
        $("input#direccion").val(D.odireccion_instalacion)
        $("input#tipocliente").val(D.otipocliente)
        $("input#usuario").val(D.ousuario)
         
        $("input#idregistro").val(D.idregistro) // validar el banco  con el ID del registro
        var codBam=D.ocodigobanco;

        $.post( 'view/CaptacionCliente/bancosintereseditar.php',{postvariable: codBam}).done( function(respuesta){
          $('#asignarbanco').html(respuesta);   
        });

        $("input#asignarbanco").val("");

        $("input#idregistro").val(D.idregistro) // validar el medio de contacto con el ID del registro
        var codmedio=D.omediocontacto;

        $.post( 'view/CaptacionCliente/mediocontactoeditar.php',{postvariable: codmedio}).done( function(respuesta){
          $('#mcontacto').html(respuesta);   
        });

        $("input#mcontacto").val("");


        $("input#idregistro").val(D.idregistro) // validar el tipo de cliente con el ID del registro
        var codcliente=D.otipocliente;

        $.post( 'view/CaptacionCliente/tipoclienteeditar.php',{postvariable: codcliente}).done( function(respuesta){
          $('#tcliente').html(respuesta);   
        });

        $("input#tcliente").val("");

        $('#Modaleditar_registro').modal('show');


  });

}

  </script>

 <script languaje="javascript">
    seleccionarbancointeres(); mascaras();
  </script>
<script type="text/javascript">
  if (document.getElementById("estatus").value==='1'){
    document.getElementById("estatus").checked = true;
  }else{
 document.getElementById("estatus").checked = false;
 }
</script>
   <script languaje="javascript">
    function mayoracero() {
     var validacion= $('#cantidadpos').val();
      if (validacion<=0) {
        alertify.error('La cantidad del POS debe ser mayor a cero');
       
      }
      else{

        return true;
      
      }

     }
  </script>

<?php } ?>
