<?php
//  session_start();
$tituloPagina=$_SESSION["page"];
if (isset($_SESSION["session_user"]))
{
  $usuario=$_SESSION["session_user"];
  $tipousuario = $_SESSION["superusuario"];

}
$g1=1;

//construct data grafica tipo Dona
$controladorcomer =new ControladorComercializacion();
$comercializacion=$controladorcomer->sp_graficacontacto($usuario);
$data = array();
$arrayColors = array();
$sincontacto="Sin Contactar";
$contactado="Contactado";
$venta="Venta con Fondos";
$facturado="Facturado";
$red='#FF0000';//sin contactar
$orange='#FFA500';//contactado
$purple='#800080';//venta con fondos
$blue='#04B4AE';//facturado
while($row = pg_fetch_array($comercializacion))
{
 
   if ($row["descripcion"]==$contactado) {
      $arrayColors [] = array(
      $orange, 
       );
   } 
   if ($row["descripcion"]==$venta) {
      $arrayColors [] = array(
      $purple, 
       );
   } 
    if ($row["descripcion"]==$sincontacto) {
       $arrayColors [] = array(
       $red, 
        );
    }
   if ($row["descripcion"]==$facturado) {
      $arrayColors [] = array(
      $blue, 
       );
   } 

        
 $data[] = array(

  'label'  => $row["descripcion"],
  'value'  => $row["cantidad"],
        
 );
}
if ($data=="") {
    $g1=0;
}

$data = json_encode($data);
$arrayColors = json_encode($arrayColors);
 print_r($data);

//construct data grafica bar-chart
$ctrlcomer =new ControladorComercializacion();
$comer=$ctrlcomer->sp_graficaregistrosxanho($usuario);
$datosbar = array();
$anio='';
while($rows = pg_fetch_array($comer))
{
if ($anio!=$rows["period"]){
     $anio=$rows["period"];
  
  }  
 $datosbar[] = array(

  'year'  => $anio,
  'label'  => $rows["banco"],
  'value'  => $rows["cantidad"],
      
 );
}
$datosbar = json_encode($datosbar);
// print_r($datosbar);


//grafica tipo linea grafica ventas por años
$ctrlcomer =new ControladorComercializacion();
$comer=$ctrlcomer->sp_nocontactografica($usuario);
$line = array();

while($r = pg_fetch_array($comer))
{

 $line[] = array(

  'year'  => $r["year"],
  'value'  => $r["value"],     

 );
}
$line = json_encode($line);
// print_r($line);

//banco anhio
$ctrlcomer1 =new ControladorComercializacion();
$comer1=$ctrlcomer1->sp_graficarbancoanio();
$area = array();
$cont=0;
while($result = pg_fetch_array($comer1))
{
$cont++;
 $area[] = array(

  'year'  => $result["year"],
  'banco1' => $result["banco1"],
  'banco2' => $result["banco2"],
  'banco3' => $result["banco3"],
  'banco4' => $result["banco4"],
  'banco5' => $result["banco5"],
  'banco6' => $result["banco6"],
  'banco7' => $result["banco7"],
  'banco8' => $result["banco8"],
  'banco9' => $result["banco9"],
  'banco10' => $result["banco10"]
 
 );
 // var_dump($result);
}
$area = json_encode($area);
// print_r($area);

//valida fechas
$anio=date("Y");
$anio1=date("Y");
$mes=date("m");
$mes1=date("m");
$dia_desde="01";
$fecha_desde="";
$fecha_hasta="";
if(($anio % 4 == 0) && ($anio % 100 != 0) || ($anio % 400 == 0)){
    if ($mes=="02") {
    $dia_hasta="29";
    }
   
}else{

    $bisiesto="nobisiesto";
    $dia_hasta="28";
}

if ($mes=="04" || $mes=="06" || $mes=="09" ||$mes=="11") {
     $dia_hasta="30";
}
if($mes=="01" || $mes=="03" || $mes=="05" || $mes=="07" || $mes=="08" || $mes=="10" || $mes=="12"){
    $dia_hasta="31";
}
$fecha_desde=$anio."-".$mes."-".$dia_desde;
$fecha_hasta=$anio1."-".$mes1."-".$dia_hasta;
//construct data grafica tipo Dona mensual
$ctrlcomercial =new ControladorComercializacion();
$comercial=$ctrlcomercial->sp_graficacontactomensual($usuario,$fecha_desde,$fecha_hasta);
$datosmes = array();
$arrayColors1 = array();
$sincontacto1="Sin Contactar";
$contactado1="Contactado";
$venta1="Venta con Fondos";
$facturado1="Facturado";
$red1='#FF0000';//sin contactar
$orange1='#FFA500';//contactado
$purple1='#800080';//venta con fondos
$blue1='#04B4AE';//facturado
while($rows1 = pg_fetch_array($comercial))
{
    
 if ($rows1["descripcion"]==$contactado1) {
      $arrayColors1 [] = array(
      $orange1, 
       );
   } 
   if ($rows1["descripcion"]==$venta1) {
      $arrayColors1 [] = array(
      $purple1, 
       );
   } 
    if ($rows1["descripcion"]==$sincontacto1) {
       $arrayColors1 [] = array(
       $red1, 
        );
    }
   if ($rows1["descripcion"]==$facturado1) {
      $arrayColors1 [] = array(
      $blue1, 
       );
   } 

 $datosmes[] = array(
 
  'label'  => $rows1["descripcion"],
  'value'  => $rows1["cantidad"],      

 );
}
$datosmes = json_encode($datosmes);
$arrayColors1 = json_encode($arrayColors1);

?>
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header" style="color:#00ADA8;"><i class="fa fa-tasks fa-fw" ></i> Dashboard</h3>
      </div>
<!-- fechas para grafica dona mensual ajax -->
<input type="text" name="" class="hidden" id="fechadesde" value="<?php echo $fecha_desde; ?>">
<input type="text" name="" class="hidden" id="fechahasta" value="<?php echo $fecha_hasta; ?>">
        <div class="">
        <?php if ($tipousuario=="A"){  ?>
                  <div class="col-md-12" style="text-align: center;">
                    <div class="form-group">
                        <label for="usuario">Seleccione Ejecutivo:</label>
                       <select class="form-control" id="usuario" style="width: 25%; margin-left: 38%;">
                       </select>
                       </div>
                   </div>
        <?php  } ?>

                   <div class="row">
                   <div class="col-md-6">
                          <!-- /.col-lg-6 -->
               <input type="text" id="valor" class="hidden" value="11">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center;background-color: #00ADA8;color:white;">
                           <b> Gráfica - Total por Estatus de Gestión</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" id="donut_chart_total">
                            <?php if ($g1==0){ ?>
                               <p class="alert alert-warning"> No tiene Gestiones Registradas, por tanto no se puede gráficar </p> 
                            <?php } else { ?>
                            <div id="morris_donut_chart"></div>
                             <?php } ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                   </div>  

                <div class="col-md-6">
                              <!-- /.col-lg-6 -->
                   <input type="text" id="valor" class="hidden" value="11">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="text-align: center;background-color: #00ADA8;color:white;">
                               <b> Gráfica - Mensual por Estatus de Gestión  </b>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body" id="donut_chart_mes">
                                <div id="morris_donut_chart_mes"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                       </div>  
                 
                   <div class="col-md-6">
                         <!-- /.col-lg-6 -->
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center;background-color: #00ADA8;color:white;">
                         <b>  Gráfica - Record de Ventas Facturadas por Año</b>
                        </div>
                         
                        <!-- /.panel-heading -->
                        <div class="panel-body" id="Line_chart_record">
                            <div id="Line_chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                   </div> 

                   <div class="col-md-6">
                          <!-- /.col-lg-6 -->
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center;background-color: #00ADA8;color:white;">
                           <b> Gráfica - Cantidad de Registros por Banco & por Año</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-area-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                   </div>   

                </div>


             </div> <!-- end container-->
                      
     <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>Clientes!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver detalle</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">12</div>
                                    <div>Ventas Exitosas</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalle</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">124</div>
                                    <div>Contactados</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalle</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">13</div>
                                    <div>Sin Contactar</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver detalle</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>  <!-- /.row -->   

    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">generarcliente();</script>

<script type="text/javascript">

 

$(document).ready(function(){

      var area_chart = Morris.Area({
        element: 'morris-area-chart',
        data:  <?php echo $area; ?> ,
        xkey: 'year',
        ykeys:['banco1', 'banco2', 'banco3','banco4','banco5','banco6','banco7','banco8','banco9','banco10'],
        labels: ['100% Banco, Banco Universal C.A.','Bancaribe C.A. Banco Universal','Banco Activo, Banco Universal', 'Banco de Venezuela S.A.C.A. Banco Universal', 'Banco Nacional de Credito, C.A Banco Universal','Banco Occidental de Descuento C.A','Banco Plaza, Banco Universal','Banco Venezolano De Credito S.A.','BBVA Provincial','Mercantil C.A., Banco Universal'], 
        pointSize: 2,
        hideHover: 'auto',
        fillOpacity: 0.6,
        behaveLikeLine: true,
        lineColors:['#800080','#FF0000','#FFA500','#04B4AE','green','red','#136798','#2EFE64','yellow','#F397CA'],
        
        resize: true
    });

 var morris_donut_chart_mes = Morris.Donut({
     element: 'morris_donut_chart_mes',
     data: <?php echo $datosmes; ?>,
    colors: <?php echo $arrayColors1; ?>,
  resize:true
});

 var morris_donut_chart = Morris.Donut({
     element: 'morris_donut_chart',
     data: <?php echo $data; ?>,
     colors:<?php echo $arrayColors; ?>,

  //   colors: [
  //   '#800080',//venta con fondos
  //   '#FF0000',//sin contactar
  //   '#FFA500',//contactado
  //   '#04B4AE'//facturado
  // ],
  resize:true
});
  
var Line_chart = Morris.Line({

element: 'Line_chart',
data: <?php echo $line; ?>,
  // The name of the data record attribute that contains x-values.
  xkey: 'year',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Facturados'],
  lineColors:['#04B4AE',
  '#800080'],
   resize: true
});

//select ajax para grafica tipo Dona historico general
  $("#usuario").on("change", function(){
   var selectedID = $(this).val(); 
   $(".alert").remove();
   $("#morris_donut_chart").show();
   $.ajax({
          type: "POST",
          url: "view/comercializacion/gestion/ajax.php?Area_chart="+selectedID,
          data: 0,
          dataType: 'json',
          success: function(data){
            if (data=="") { 
         alertify.error("No tiene Gestiones Registradas, por tanto no se puede gráficar");
           $('<p></p>', {
               text: 'No se encontraron datos a gráficar.',
               class: 'alert alert-danger',
               style: 'text-align: center'
           }).appendTo('#donut_chart_total'); 
            $('#donut_chart_total').attr('style', 'height:384px');
            $("#morris_donut_chart").hide();

            }
            morris_donut_chart.setData(data);
             // morris_donut_chart.setColors(arrayColors);
             /* setCookie('numbers',data,3);
              $('.flash').show();
              $('.flash').html("Template Updated")*/
          }
        });
});

  //select ajax para grafica tipo Dona historico del mes actual 
  $("#usuario").on("change", function(){
   var selectedID = $(this).val(); 
   var fechadesde = $("#fechadesde").val();
   var fechahasta = $("#fechahasta").val(); 
   var variablearray=selectedID+'/'+fechadesde+'/'+fechahasta;  
     $(".alert").remove();
     $("#morris_donut_chart_mes").show();
    
   $.ajax({
          type: "POST",
          url: "view/comercializacion/gestion/ajax_donutmes.php?Donut_chart_mes="+variablearray,
          data: 0,
          dataType: 'json',
          success: function(data){
              if (data=='') {
                alertify.error('No existen gestiones realizadas el mes actual, por tanto la gráfica estará en blanco');
              //la etiquta que queremos crear 
              $('<p></p>', {
              //atributo que desea que contenga
                  text: 'No se encontraron datos a gráficar',
                  class: 'alert alert-danger',
                  style: 'text-align: center'
              //el contenidor donde desea insertar ejemplo dentro de un div
              }).appendTo('#donut_chart_mes'); 
            $('#donut_chart_mes').attr('style', 'height:384px');
            $("#morris_donut_chart_mes").hide();
              
          }
          if (data!="") {
              $( ".alert").remove();
          }
         
            morris_donut_chart_mes.setData(data);


          }
        });
});

//select ajax para grafica tipo Line
    $("#usuario").on("change", function(){
     var selectedID = $(this).val(); 
     $(".alert").remove();
     $("#Line_chart").show();
     $.ajax({
            type: "POST",
            url: "view/comercializacion/gestion/ajax_line.php?Line_chart="+selectedID,
            data: 0,
            dataType: 'json',
            success: function(data){

            if (data=='') {

            alertify.error('Aun no tiene Ventas Facturadas, por tanto no se puede parametrizar historico por años');

            $('<p></p>', {
              //atributo que desea que contenga
                  text: 'No se encontraron datos a gráficar',
                  class: 'alert alert-danger',
                  style: 'text-align: center'
              //el contenidor donde desea insertar ejemplo dentro de un div
              }).appendTo('#Line_chart_record'); 

            $('#Line_chart_record').attr('style', 'height:384px');
            $("#Line_chart").hide();

            }

            if (data!="") {
                $(".alert").remove();
            }

              Line_chart.setData(data);

               // callback(selectedID);
               /* setCookie('numbers',data,3);
                $('.flash').show();
                $('.flash').html("Template Updated")*/
            }

          });
  
  });


});
</script>
