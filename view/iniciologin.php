<?php
//
error_reporting(0);
$errorcaptcha=0;

$mi_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$name=$_SERVER['REQUEST_URI'];

    if (isset($_POST['enviar']))
    {
      if($_POST['csrf_token']!=$_COOKIE['index']){
  
        return header("Location:salir.php");
      } 
      session_start();
      session_set_cookie_params(0, "/", $_SERVER["HTTP_HOST"], 0); 
      function getip() {
        if(getenv("HTTP_CLIENT_IP")) {
          $ip = getenv("HTTP_CLIENT_IP");
        } elseif(getenv("HTTP_X_FORWARDED_FOR")) {
          $ip = getenv("HTTP_X_FORWARDED_FOR");
        } else {
          $ip = getenv("REMOTE_ADDR");
        }
        $ip = htmlspecialchars(substr($ip,0,15), ENT_QUOTES);
        return $ip;
      }

        $sid  = htmlspecialchars(substr(session_id(),0,32), ENT_QUOTES);
        $code = $_POST['code'];

        $controlador1 =new controladorUsuario();
        $c=$controlador1->verificarcaptcha($sid, getip(), $code);
        if ($c==0){
            //echo "c";          
            $errorcaptcha=1;
        }

        else { 

        $controlador =new controladorUsuario();
        $r=$controlador->verificar($_POST['username'], $_POST['secretkey']);
        $_SESSION["session_var"]=$r;
        //echo $r;
         if($r==0){
           header('Location:error?mensaje=3');
         }
         else
         {

            $controladorintentos =new controladorUsuario();
            $controladorintentosrenew =new controladorUsuario();
            if($r==4){
                   header('Location:error?mensaje=4');
            }

            elseif($r==5){
                   //inactivo
              
                  $result=$controladorintentosrenew->sp_resetactividad($_POST['username']);
                   
                   header('Location:error?mensaje=5');
            }

            elseif(($name=="/Intelipunto/" || $name=="/Intelipunto/index" || $name=="/intelipunto/index" || $name=="/intelipunto/") && $r==6){
                   //USUARIO BANESCO
                   header('Location:error?mensaje=6');
            }

            
            else
            {
              if($r==1){
                   $_SESSION["contrasena"]='1';
                   header('Location:password');
                    $result=$controladorintentosrenew->cargarintentosrenew($_POST['username']);
              }

              if($r==3){
                   $result=$controladorintentos->cargarintentos($_POST['username']);
                   header('Location:error?mensaje=3');
              }
              if($r==2){
                 $_SESSION["contrasena"]='0';
                   $result=$controladorintentosrenew->cargarintentosrenew($_POST['username']);

                   
                    
                    $controladorlogin =new controladorUsuario();
                    $ejecutivologin=$controladorlogin->sp_usuariologin($_POST['username']);
                   header('Location:home');
              }

            }

         }
        
}
    }else{
      $csrf_token = bin2hex(random_bytes(32));
      setcookie("index", $csrf_token);
    }

?>

<body class="">
  <!-- Start Page Loading -->
  <?php 

  if ($errorcaptcha==1) {

    echo "<script>alertify.alert('Captcha incorrecto, Verifique e intente nuevamente!.');</script>";
   
  } ?>  

  <!-- End Page Loading -->

  <div class="padre">
  <div class="hijo"></div>
</div>
<br>
<br>
<br>
<br>
<br>
  <div id="login-page" class="row">
    <div class="padre">
      <form class="login-form hijo" id="myform" action="" method="post">
        <div class="row">
          <div class="input-field col s12 center">
            <img src="img/logo-app.jpg" alt="" class="responsive-img valign profile-image-login">
            <!--h3 class="center login-form-text">Intelipunto</h3-->
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
               <!--   <i class="mdi-action-lock-outline prefix"></i> -->
               <input type="hidden" name="csrf_token" id="csrf_token" value="<?php echo $csrf_token; ?>">
            <input id="username" name="username" onkeypress="return soloLetras(event)" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  minlength="3" maxlength="20"  type="text" required>
            <label for="username" class="center-align">Usuario</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
          <!--   <i class="mdi-action-lock-outline prefix"></i> -->
            <input id="secretkey" name="secretkey" type="password" minlength="6" maxlength="20" autocomplete="off" required>
            <label for="password" class=""> Contraseña</label>
            
          </div>
        </div>
       <!--  <div class="row">
          <div class="input-field col s12 m12 l12  login-text">
              <input type="checkbox" id="remember-me">
              <label for="remember-me">Remember me</label>
          </div>
        </div> -->
         <div class="row">
            <div class="input-field col s12">
<!--       <div style="padding-left: 20%;"> -->
        <img
          style="float:left; margin: 5px;"
          src="captcha/captcha.php"
          width="30%"
          height="30%"
          border="0"
          alt="Enter the code shown in the picture"
        >

        <input id="captcha" type="text" name="code" maxlength="5" style="margin: 4px;width: 60%; margin-top: 0.8%;" autocomplete="off" placeholder=" &#8592; Repite el n&uacute;mero"; required>
    </div>
    </div>
        <div class="row">
          <div class="input-field col s12">
          <input type="submit" name="enviar" value="Iniciar sesi&oacute;n" class="btn waves-effect #ff6d00 orange accent-4 waves-light col s12"/>
          </div>
        </div>

      </form>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="materialize/js/jquery-1.11.2.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
  <!--prism-->
  <script type="text/javascript" src="materialize/js/prism.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="materialize/js/perfect-scrollbar.min.js"></script>

  <script type="text/javascript" src="materialize/js/plugins.min.js"></script>

  <script type="text/javascript" src="materialize/js/custom-script.js"></script>
  <div class="hiddendiv common"></div>


  </body>
</html>

