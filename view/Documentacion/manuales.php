
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                        <h3  class="titulo_manuales" ><i class="fa fa-file-pdf-o" style="color: red;"></i> Manuales de Usuario</h3>
                        

    <style type="text/css">
    	.manual{
    		color: #333333;
			font-family: "georgia";
			font-style: italic;
			background:#81818126;

    	}
   	

    	.H_manual:hover{
    		color:red;
    		text-decoration: none;
    		
    	}

    	.H_manual:active{
    		
    		text-decoration: none;
    		
    	}

    	.titulo_manuales{
    		font-family: inherit;
    		font-weight: 500;
    		line-height: 1.1;
    		color: inherit;
   			font-size: 36px;
   			border-bottom: 1px solid #eee;
        	padding-bottom: 9px;
    		margin: 40px 0 20px;
   
    	}
    	.Modulo_ {
    		color: #333333;
			font-family: "georgia";
			font-style: italic;
    	}
    	

    	
    </style>
         
  	<table class="table table-bordered">

    <thead>
      <tr>
      	<th  style="text-align: center; ">Módulo</th>
        <th style="text-align: center; ">Descripción</th>
        <th  style="text-align: center; ">Acción</th>

        
      </tr>
    </thead>
    <tbody>

     <?php  
       $usuario=$_SESSION["session_user"];
       require_once("controller/controlador.php");
       $controlador =new controladorUsuario();
       $dataUsser=$controlador->ver($usuario); 
     ?>
     
   	<tr>
   		<td class="Modulo_" style="text-align: center;">Acceso al sistema Intelipunto.</td>
        <td class="manual">2018-SIS-001.US. MANUAL DE ACCESO AL SISTEMA DE INTELIPUNTO</td>
        <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;" > <a class="H_manual" href="manuales/2018-SIS-001.AI. MANUAL DE ACCESO AL SISTEMA INTELIPUNTO..pdf" target="_blank">Descargar</a></td>
        
      </tr>

      <?php if($dataUsser["codtipousuario"]==='E' || $dataUsser["area"]===3 || $dataUsser["codtipousuario"]==='A'){ ?> 
         <tr>
       	  <td class="Modulo_" style="text-align: center;">Clientes Potenciales</td>
          <td class="manual">2019-SIS-030.CP.CLIENTES POTENCIALES</td>
          <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-030.CP. CLIENTES POTENCIALESRevisado.pdf" target="_blank">Descargar</a></td>
        </tr>
        
        <tr>
        
          <td class="Modulo_" style="text-align: center;">Comercializacion - Ejecutivo de Ventas</td>
          <td class="manual">2019-SIS-031.CC.COMERCIALIZACION AREA COMERCIAL</td>
          <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-031.CC.COMERCIALIZACION AREA COMERCIAL.pdf" target="_blank">Descargar</a></td>        
        </tr> 

        <tr>
          <td class="Modulo_" style="text-align: center;">Administración - Ejecutivo de Ventas</td>
          <td class="manual" >2019-SIS-034.AE.ADMINISTRACION EJECUTIVO</td>
          <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-034.AE.ADMINISTRACION EJECUTIVO.pdf" target="_blank">Descargar</a></td>  
        </tr>
      <?php } ?>
      <?php if($dataUsser["area"]===1 || $dataUsser["codtipousuario"]==='A'){ ?>
        <tr>
      	  <td class="Modulo_" style="text-align: center;">Administración - Administrador</td>
          <td class="manual">2019-SIS-033.AD.ADMINISTRACION ADMINISTRADOR</td>
          <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-033.AD.ADMINISTRACION ADMINISTRADOR.pdf" target="_blank">Descargar</a></td>        
        </tr>  
      <?php } ?>

      <?php if($dataUsser["coordbanco"]>0 || $dataUsser["codtipousuario"]==='A'){ ?>
        <tr>
        	<td class="Modulo_" style="text-align: center;">Captación Clientes Gerentes</td>
          <td class="manual">2019-SIS-040.CCG.CAPTACION DE CLIENTES GERENTES</td>
          <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-040.CCG.CAPTACION DE CLIENTES GERENTES.pdf" target="_blank">Descargar</a></td>        
        </tr> 
      <?php } ?>

<?php if($_SESSION["doc_servicio_tecnico"]===1 ){ ?>
        <!-- <tr>
        	<td class="Modulo_" style="text-align: center;">Servicio Tecnico</td>
        <td class="manual">Servicio Tecnico</td>
        <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="" target="_blank">Descargar</a></td>        
      </tr> -->
<?php } ?>

      <tr>
        <td class="Modulo_" style="text-align: center;">Captación Clientes</td>
        <td class="manual">2019-SIS-039.CCE.CAPTACION DE CLIENTES EJECUTIVOS</td>
        <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-039.CCE.CAPTACION DE CLIENTES EJECUTIVOS.pdf" target="_blank">Descargar</a></td>
     	</tr> 
      <tr>
        <td class="Modulo_" style="text-align: center;">Encuesta</td>
        <td class="manual">2020-SIS-043.ENC.ENCUESTA</td>
        <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2020-SIS-043.ENC.ENCUESTA.pdf" target="_blank">Descargar</a></td>
      </tr>

    	<?php if(($dataUsser['area']=='SISTEMA')||($dataUsser['area']=='ADMINISTRACIÓN')){ ?>
        <tr>
          <td class="Modulo_" style="text-align: center;">Activación de Equipos</td>
          <td class="manual">2021-SIS-044.AE. ACTIVACIÓN DE EQUIPOS</td>
          <td style="text-align: center; "><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2021-SIS-044.AE. ACTIVACIÓN DE EQUIPOS.pdf" target="_blank">Descargar</a></td>        
        </tr> 
      <?php } ?>

      <?php if($dataUsser['area']=='SISTEMA'){ ?>
        <tr>
        	<td class="Modulo_" style="text-align: center;">Control de Accesos</td>
          <td class="manual">2021-SIS-045.CA.CONTROL ACCESOS</td>
          <td style="text-align: center;"><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2021-SIS-045.CA.CONTROL ACCESOS.pdf" target="_blank">Descargar</a></td>        
        </tr> 
      <?php } ?>

      <?php if($dataUsser['area']=='SISTEMA' || $dataUsser["codtipousuario"]==='E' || $dataUsser["area"]===3 || $dataUsser["codtipousuario"]==='A'){ ?>
            <tr>
           	<td class="Modulo_" style="text-align: center;">Servicio Tecnico</td>
              <td class="manual">2019-SIS-041.STE.SERVICIO TECNICO EJECUTIVOS</td>
              <td style="text-align: center;"><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-041.STE.SERVICIO TECNICO EJECUTIVOS.pdf" target="_blank">Descargar</a></td>        
            </tr>
      <?php } ?>

      <?php if($dataUsser['area']=='SISTEMA' || $dataUsser["codtipousuario"]==='T'){ ?>    
        <tr>
        	<td class="Modulo_" style="text-align: center;">Servicio Tecnico</td>
          <td class="manual">2019-SIS-042.STAS.SERVICIO TECNICO ALL SOLUTIONS</td>
          <td style="text-align: center;"><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="manuales/2019-SIS-042.STAS.SERVICIO TECNICO ALL SOLUTIONS.pdf" target="_blank">Descargar</a></td>       
        </tr>
      <?php } ?>

<?php if($_SESSION["doc_reporte_ejecutivo"]===1){ ?>
   <!--  <tr>
     	<td class="Modulo_" style="text-align: center;">Reporte Ejecutivos</td>
        <td class="manual">Reporte Ejecutivos</td>
        <td style="text-align: center;"><i class="fa fa-file-pdf-o" style="color: red;"> <a class="H_manual" href="" target="_blank">Descargar</a></td>        
      </tr> -->
      <?php } ?>

    </tbody>
  </table>
</div>

          