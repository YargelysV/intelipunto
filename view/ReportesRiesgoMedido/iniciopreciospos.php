<?php 
  if (isset($_SESSION["session_user"]))
  {
  $usuario=$_SESSION["session_user"];
  }
  $controladorc =new controladorRiesgo();
  $resultados= $controladorc->sp_verpreciopos();
  $montopos = pg_fetch_assoc($resultados);
?>
<script languaje="javascript">
     mascaras();
</script>
<input class="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>" readonly>
<input type="text" id="banco" class="hidden" value="<?php echo $banco; ?>">

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
    <div class="row">
      <nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand"  style="color:#F8F9F9;">  Registro Precio</a>
        </div>
       </nav>
      </div>
      <!--nuevo bloque-->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading" style="text-align: center;">
            <i class=""></i> Registrar Precios de POS
            <i class=""></i> Registrar Precios de POS
            <i class=""></i> Registrar Precios de POS
            </div>
            <div class="panel-body">
            <div class="row">
              <div class="col-lg-3">
                <div class="">
                  <label>Modelo</label>
                  <select class="form-control" onclick="selectpreciopos()" name="idmodelopos" id="idmodelopos" >
                  </select>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Tipo</label>
                  <select class="form-control" onchange="selecttipos()" name="idtipopos" id="idtipopos" >
                  </select>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Monto USD</label>
                  <input class="form-control phone" type="numeric" onkeypress="return soloNumeros(event)" onkeyup="modificaciones()" name="montousd" id="montousd" minlength="15" maxlength="15" autocomplete='off' required>
                </div>
              </div>
              <div class="col-lg-3">
                <br>
                <input class="btn btn-primary btn-block"  type="button" name="editar" id="" value="Guardar" onclick="guardarpreciopos();"/> 
              </div>
            </div>
          </div>
          </div>
          <div class="panel-group">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h4 class="panel-title" align="center">
                  Precios Registrados                     
                </h4>
              </div>
              <div class="panel-body">
                <div class="row" align="center">
                  <!-- <div ng-app="AppregistroPersonasRef" ng-controller="registroPersonasRefcontroller" align="center">
                    <div class="demo-container">
                      <div id="gridContainer" dx-data-grid="gridOptions"></div>
                    </div>
                  </div> -->
                  <div class="col-12">
        <table class="table table-bordered"  id="preciospos"> 
           <thead>
              <tr>        
                <th></th>
                <th>Cod</th>   
                <th>codmarca</th>
                <th>codtipo</th>  
                <th>Modelo</th>
                <th>Tipo</th>
                <th>Monto USD</th>
                <th>Fecha</th>
                <th>Usuario</th> 
                <th></th>         
              </tr>
           </thead>
        </table>
 
      </div> 
                </div>
              </div>
            </div>
        </div>
      </div>  
    </div>
    </div>
  </div>
</div>    

<div class="container-fluid">
  <div class="modal fade" id="Modal_precios" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <center>Editar Precios Registrados/center>
            </div>
   <input type="hidden" name="id_consecutivo" value="" id="id_consecutivo" class="form-control" placeholder="" tabindex="3" readonly>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group">
                    <input type="text" class="form-control" id="correlativo">
                    <label for="" class="col-form-label">Modelo</label>
                     <input type="text" class="form-control" id="marcapos">
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Tipo</label>
                    <input type="text" class="form-control" id="tipopos">
                    </div> 
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Monto USD</label>
                     <input type="text" class="form-control" id="valordolar">
                    </div>
                    </div>    
                </div>
           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <input type="button" class="btn btn-dark" id="btneditar" name="editar" value="Guardar" onclick="editarPreciosPos();">  
            </div>
       
        </div>
    </div>
  </div>
</div> 


  <script type="text/javascript">
  function dtCore(){
/*  var fechadetalle= document.getElementById('fechadetalle').value;
  var namearchivo= document.getElementById('namearchivo').value;*/

  tblpersonas= $('table#preciospos').DataTable({
    "ajax": {
    "url": "api/PreciosPos.php"
    },
    "columns": [
      {"data": "sec", className: "text-center"},
      {"data": "correlativo", className: "text-center"}, 
      {"data": "marcapos", className: "text-center" }, 
      {"data": "tipopos", className: "text-center"},  
      {"data": "descmarca", className: "text-center"},
      {"data": "desctipo", className: "text-center"},  
      {"data": "valordolar", className: "text-center"},
      {"data": "fechaact", className: "text-center"},  
      {"data": "usuario", className: "text-center"},
      {"defaultContent":" <button class='Editar btn btn-primary' type='button' id='editar_precio' title='Editar'><i class='fa fa-edit'></i></button>"},

    ],
    "order" : [[0, "asc"]],
    "columnDefs": [
           {
                 "targets": [ 1,2,3 ],
                 "visible": false,
             }
         ],
   "scrollX": 2900,
    "scrollY": false,
    "info":     false,
    "scrollCollapse": false,
    
    dom: 'Bfrtip',
    buttons: [
        {
            extend:'excelHtml5',
            text: 'Excel',          
        }
    ],
    
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }     
  });

  $('table#preciospos tbody').on( 'click', 'button.Editar', function () {

        var table=$('table#preciospos').DataTable();
        var D =  table.row($(this).parents("tr")).data();
        $("input#correlativo").val(D.correlativo)
        $("input#marcapos").val(D.marcapos)
        $("input#tipopos").val(D.tipopos)
        $("input#valordolar").val(D.valordolar)
        $(".modal-header").css( "background-color", "#17a2b8");
        $(".modal-header").css( "color", "white" );


        $('#Modal_precios').modal('show');
  });

}
  </script>