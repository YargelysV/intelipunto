<?php 
  $usuario=$_SESSION["session_user"];
  $var=($_REQUEST['var']);
  $array = explode("/",$var);
  $banco= $array[0]; 
  $controlador =new ControladorActivacion();
  $resultados=$controlador->sp_buscaractivacion($banco);
  $databanco = pg_fetch_assoc($resultados);
  $count = pg_num_rows($resultados);
  if ($count==0) { 
//coment
   ?>


<script type="text/javascript">
redirectResult();
</script>
<?php }else { ?>
    
<input type="hidden" id="banco" name="banco" value="<?php echo $banco; ?>">
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-default" style="background-color:#337AB7;">
        <div class="navbar-header">
         <a class="navbar-brand" href="#" style="color:#FFFFFF;"> <i class="fa fa-gears" style="color:#F8F9F9;"> </i> Activación de Equipos</a>
        </div>
      </nav>
      <b style="color:#5DADE2;"></b>    <b style="color:#5DADE2;">Búsqueda:<?php if ($banco=='0') {
       echo 'Todos los bancos';
      } else { echo $databanco['nbanco']; } ?>
      </b>  
    </div> 
	   <div class="container"> 
	    </div>
			<div class="row">
	          <div class="col-lg-12" align="center">
	            <table id="tblactivacionequipos" class="table table-bordered"   > 
	               <thead>
	                  <tr>
	                    <th>N°</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;RIF&nbsp;ó&nbsp;Cédula&nbsp;&nbsp;</th>
                        <th>Acción</th>
                        <th>ID</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Razón&nbsp;social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco&nbsp;Asignado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>&nbsp;&nbsp;Afiliado&nbsp;&nbsp;</th>
                        <th>Cantidad&nbsp;Terminales</th>
                        <th>Tipo&nbsp;de&nbsp;POS</th>
                        <th>Nº&nbsp;Factura</th>
                        <th>cod banco</th>
	                  </tr>
	                </thead>
                    <tfoot>
                        <th colspan="4" style="text-align:left">Total:</th> 
                    </tfoot>
	            </table>
	          </div>
            <div class="col-md-4 col-md-offset-4" align="center" >
    <button type="button"class="btn btn-default" name="volver" value="Volver" onclick="window.location.href='activacionequipos'">Volver</button>
        </div>
	        </div>		
		</div>
	</div>

		
<?php

	}

?>

<script type="text/javascript">
function dtCore(){
  var banco= document.getElementById('banco').value;

  $('table#tblactivacionequipos').DataTable({
    "ajax": {
    "url": "api/SelectActivacionEquipos.php",  
          "type": "POST",
          "data": {"banco":banco}
    },
    "columns": [
      
      {"data": "sec", className: "text-center"},
      {"data": "coddocumento", className: "text-center" },
      {"defaultContent": "<p class='abrir'><a class='abrir' type='href' title='Ver'>Activar</a></p>", className: "text-center" },
      {"data": "consecutivo", className: "text-center" },
      {"data": "razonsocial", className: "text-center" },
      {"data": "nbanco", className: "text-center" },
      {"data": "numafiliado"},          
      {"data": "cantidadpos", className: "text-center" },
      {"data": "tipoposs", className: "text-center" },
      {"data": "factura", className: "text-center" },
      {"data": "codbanco", className: "text-center" }
    ],
    "order" : [[0, "asc"]],
  "columnDefs": [
                 {
                       "targets": [10],
                       "visible": false,
                   }
               ],
    "scrollX": 2900,
    "scrollY": false,
    "info": false,
    "scrollCollapse": false,
    dom: 'Bfrtip',
    buttons: [
       
            'excel'         
       
    ],
    "footerCallback": function ( tfoot, data, start, end, display ) {
            var api = this.api();
            var lastRow = api.rows().count();
            for (i = 0; i < api.columns().count(); i++) {
              $(tfoot).find('th').eq(i).html(api.cell(lastRow-1,i).data());
            }
            // Update footer
            $( api.column( 0 ).footer() ).html(
                'Total:'+lastRow 
            );
        },
    language:{
      "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "Print":           "Imprimir",
       "sUrl":            "",
       "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
       "oPaginate": {
         "sFirst":    "Primero",
         "sLast":     "Último",
         "sNext":     "Siguiente",
         "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
      
  });
        
$('table#tblactivacionequipos tbody').on( 'click', 'a.abrir', function () {
//alert('algo');
    var banco= document.getElementById('banco').value;
    var table=$('table#tblactivacionequipos').DataTable();
    var D =  table.row($(this).parents("tr")).data();
    var consecutivo=D.consecutivo;
    var variable=consecutivo+'/'+banco;
    var url = "activacionequipos?cargar=gestionaractivacion&var="+variable; 
    $(location).attr('href',url);

});

}
</script>
