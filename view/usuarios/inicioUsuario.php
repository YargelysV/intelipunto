<?php
    $usuario=$_SESSION["session_user"];
    $controlador =new controladorUsuario();
    $resultado= $controlador->sp_buscarusuarios();
    $row = pg_fetch_assoc($resultado);
?>
        

<div id="page-wrapper">
    <div class="container-fluid">
         <div class="row">
                <div class="col-lg-12">
                     <h1 class="page-header"><i class="fa fa-user fa-fw"></i> Usuarios</h1>
                </div>

                <table class="table table-bordered table-striped"   id="datatable_users" style="width: 100%"> 
                   <thead>
                      <tr>
                      <th>Nº</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Nº de C&eacute;dula&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Usuario&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Estatus&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Acci&oacute;n&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;M&oacute;dulos&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Clave&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Desbloquear&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;Actividad&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>Oficinas</th>
                      <th>Oficinas Asignadas</th>
                      <th>Región Adicional</th>
                      <th>acceso</th>
                      </tr>
                   </thead>
                </table>

         </div>
    </div>
</div>


    <div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header" align="center">
              <h4 class="modal-title" id="exampleModalLabel" ></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
            </div> 
           
           <form id="formRegionesAdic" method="post" >
                <div class="col-lg-12">  
                    <br>
                <input type="hidden" name="usuario_acceso" value="<?php echo $usuario_acceso; ?>">
                <input type="hidden" name="login" id="login" value="<?php echo $login; ?>">
              
                  <label>Sucursal</label>
                     <select class="form control" name="oficinareg[]" id="oficinareg"  multiple="multiple" style="width:180px;padding-left:15px;color:#000000" >
                     </select>               
                </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnGuardarSuc" class="btn btn-primary">Guardar</button>
              </div>    
          </form>
            </div>

      </div>
  </div>



    <div style='text-align:center;'>
    <input type="button" class="btn btn-success"  name="crear" value="Registrar Nuevo" onClick="window.location.href='?cargar=crear'"/>
    </div>


<script type="text/javascript">
   function dtCore() {
   tableusuarios= $('#datatable_users').DataTable({
    "ajax": {
    "url": "view/usuarios/selectUsuarios.php",
          "type": "POST",
    },
    "columns": [
    {"data": "secuencial" },
    {"data":"coddocument", className: "text-center"},
    {"data": "login", className: "text-center"},
    {"data": "nombre", className: "text-center" },
    {"data": "apellido", className: "text-center" },
    {"data": "estatus", className: "text-center",
    "render" : function (data, type, full, meta, row) {
          if (data == "1" || data === "1") {
            return "<p>Activo</p>";
          }else if (data == "2" || data === "2"){
            return "<p>Bloqueado</p>";
          }else{
            return "Inactivo";
          }
          }
    },
    {"defaultContent":"<a class='ver' type='href' title='Ver'>Detalles</a>/<a class='editar' type='href' title='Editar'>Editar</a>", className: "text-center"},
    
    {"defaultContent":"<a class='modulos' type='href' title='Ver'>Ver</a>", className: "text-center"},

    {"defaultContent":" <button class='Reiniciar btn btn-wringer' type='button' id='reiniciar'><img src='img/descarga.png' height='20' width='25'></i></button>", className: "text-center"},

    {"data": "estatus", className: "text-center",
    "render" : function (data, type, full, meta, row) {
          if (data == "2" || data === "2") {
            return '<form><button class="Desbloquear btn btn-wringer" type="button" id="desbloquear"><img src="img/desbloquear.jpg" height="20" width="20"></i></button></form>'
          }else{
            return "-";
          }
          }
    },
    /*{"defaultContent":" <button class='Desbloquear btn btn-wringer' type='button' id='desbloquear'><img src='img/desbloquear.jpg' height='20' width='20'></i></button>", className: "text-center"},*/
    {"data": "actividad",
     render : function ( data, type, full, meta ) {  
            if (data == "1" || data === "1") {
              return '<form><label>On<div class="form-group"><input  class="radio" type="radio" name="actividadd" checked value="1" ></label> <label> &nbsp;Off<input class="radio" type="radio"  name="actividadd" value="0"></label></div></form>';
            }else if (data == "0" || data === "0"){
              return '<form><div class="form-group"><label>On<input type="radio" name="actividadd" value="1" disabled></label> <label> &nbsp;Off<input type="radio" name="actividadd" checked value="0" disabled></label></div></form>';
            }else
              return "";
          }
        },
    {"defaultContent":"<a class='oficina' type='href' title='Ver'>Ver Oficinas</a>", className: "text-center"},
    {"defaultContent":"<a class='reporte' type='href' title='Ver'>Ver</a>", className: "text-center"},
    {"defaultContent":"<a class='region' type='href' title='Ver'>Ver Región</a>", className: "text-center"},
    {"data": "acceso" }

    ],
    "order" : [[2, "asc"]],
    "columnDefs": [
           {
                 "targets": [14],
                 "visible": false,
                  
             }
         ],

      "scrollX": 2000,
      "scrollY": false,
      "info":     false,
      "scrollCollapse": false,

      language:{
        "sProcessing":     "Procesando...",
         "sLengthMenu":     "Mostrar _MENU_ registros",
         "sZeroRecords":    "No se encontraron resultados",
         "sEmptyTable":     "Ningún dato disponible en esta tabla",
         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix":    "",
         "sSearch":         "Buscar:",
         "Print":           "Imprimir",
         "sUrl":            "",
         "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
         "oPaginate": {
           "sFirst":    "Primero",
           "sLast":     "Último",
           "sNext":     "Siguiente",
           "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }

    });


    $('table#datatable_users tbody').on( 'click', 'a.modulos', function () {
       var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;
       var acceso = T.acceso;

        var url = "usuarios?cargar=modulos&login="+login+"&acceso="+acceso; 
        $(location).attr('href',url);

  });

    $('table#datatable_users tbody').on( 'click', 'a.ver', function () {
       var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;

       var url = "usuarios?cargar=ver&login="+login; 
        $(location).attr('href',url);

  });

     $('table#datatable_users tbody').on( 'click', 'a.editar', function () {
       var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;
       
       var url = "usuarios?cargar=editar&login="+login; 
        $(location).attr('href',url);

  });

     $('table#datatable_users tbody').on( 'click', 'a.reporte', function () {
       var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;
       
       var url = "usuarios?cargar=reporte&login="+login; 
        $(location).attr('href',url);

  });

     $('table#datatable_users tbody').on( 'click', 'a.oficina', function () {
       var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;

        var url = "usuarios?cargar=regiones&login="+login; 
        $(location).attr('href',url);

  });

     $('table#datatable_users tbody').on( 'click', 'a.region', function () {
       var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;

    $(".modal-header").css("color", "black" );
    $(".modal-title").text("Asignación de Regiones");   
    $('#modalCRUD').modal('show');  
    $("select#oficinareg").empty();
    setTimeout(function(){ 
     selectsucursales();
     //ocultarperfil();  
   });  
}); 

  $('table#datatable_users tbody').on( 'click', 'button.Reiniciar', function () {
      var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;

            alertify.confirm("&#191;Desea Reiniciar la clave del usuario: "+login+"?", function (e) {
          if (e) {
          $.post('view/usuarios/reiniciar.php',{postusuario:login},
          function(data)
          {
          alertify.success(data);
          });
          } else {
          alertify.error("No se ha reiniciado la clave al usuario "+login);
          return false;
          }
          })
          
          return false;

   }); 

    $('table#datatable_users tbody').on( 'click', 'button.Desbloquear', function () {
      var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;
       var estatus = T.estatus;
       //alert(estatus);

        alertify.confirm("&#191;Desea Desbloquear este usuario: "+login+"?", function (e) {
          if (e) {
            $.post('view/usuarios/desbloquear.php',{postusuario:login},
              function(data)
              {
                alertify.success(data);
                setTimeout(function(){ window.location.reload(0); }, 2000); 
              });
          } else {
            alertify.error("No se ha desbloqueado el usuario "+login);
            return false;
          }
        })
          return false;
   }); 

    $('table#datatable_users tbody').on( 'click', 'input.radio', function () {
      //alert('algo');
       var table=$('table#datatable_users').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = T.login;
       var actividad = T.actividad;

       alertify.confirm("&#191;Desea inactivar el siguiente usuario: "+login+"?", function (e) {
    if (e) {
      $.post('view/usuarios/editaractividad.php',{postusuario:login},
        function(data)
        {
          alertify.success(data);
          setTimeout(function(){ window.location.reload(0);},2000); 
        });
    } else {
      alertify.error("Se ha cancelado el proceso");
      setTimeout(function(){ window.location.reload(0); }); 
      return false;
    }
      })
      return false;
      });


       }



    function selectsucursales() {

        var region=$('input#region').val();
        var login=$('input#login').val();
        var existe =[];
    // $.getJSON("view/usuarios/selectsucursalasig.php",{region:region, login:login},function(datos){
    //     if(datos != 0){
    //         $.each(datos, function (K, V) {         
    //             existe.push(parseInt(V['idsuc']));
    //         });   
    //     }
    // });

     var region=$('input#region').val();
       $.getJSON("view/usuarios/selectRegionAdic.php",function(datos){
         if(datos != 0){

        var $SELECT = $("select#oficinareg").select2({
                placeholder: 'Seleccione',
                allowClear: true,
            });
               
        $.each(datos, function (K, V) {
            var cregion =parseInt(V['cregion']);
            if (existe.includes(cregion)) {

                $SELECT.append($('<option>', { 
                    value: V['cregion'],
                    text: V['dregion'],
                    selected:true
                }));
            }else{

                $SELECT.append($('<option>', { 
                    value: V['dregion'],
                    text: V['dregion']
                }));
            }


        });
      }  
    });


  } 


    $('#formRegionesAdic').submit(function(e){ 
    e.preventDefault();
      parametros = $("form#formRegionesAdic").serialize();
      $.ajax({
          url : "view/usuarios/guardarSucursales.php",
          type : "POST",
          data : parametros,
          success: function(data){

          if(data == 0){
            alertify.success('Las Regiones han sido eliminadas!..');
            tableusuarios.ajax.reload(null, false);
            setTimeout(function(){ window.location.reload(0); }, 3000); 
              
          }else if (data != 1){
            alertify.success('Las regiones han sido asignadas correctamente!..');
            tableusuarios.ajax.reload(null, false);
            setTimeout(function(){ window.location.reload(0); }, 3000); 
              
          } else {

              alertify.error('Error!. La Region no fue asignada.');
          }
          }
      })
    });

</script>




