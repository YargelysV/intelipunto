<?php
//session_start();

if (isset($_SESSION["session_user"]))
{
	$login=$_REQUEST["login"];

	$controlador =new controladorUsuario();
  $resultado= $controlador->sp_selectReporteSuc($login);


?>
<input type="hidden" value="<?php echo $login;?>" name="usuario"  id="usuario" />
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header" align="center">Sucursales Asignadas: <?php echo $login?> </h3>
                    </div>

					 <div class="col-lg-12">
                    

                    	<table class="table table-bordered table-striped"   id="datatable_modulos" style="width: 100%"> 
		                   <thead>
		                      <tr>   
                          <th>Nº</th>   
                          <th>Región</th>                    
                          <th>Sucursal</th>                    
		                                         
		                      </tr>
		                   </thead>
		                </table>		
						
					</div>
				</div> 
			</div>
         <div class="col-md-4 col-md-offset-5" >
    <input type="button" class="btn btn-success"   name="volver" value="Volver" onClick="window.location.href='usuarios'"/>
  </div>
		</div>

	<?php
}
	else{
	header("Location:../salir");
	}


?>


<script type="text/javascript">
	
 function dtCore() {

  var usuario=document.getElementById('usuario').value;

    $('#datatable_modulos').DataTable({
   
    "ajax": {
    "url": "view/usuarios/selectReporteSuc.php",
          "type": "POST",
          "data": {"login":usuario}
         
    },
    "columns": [
    {"data":"sec", className: "text-center"},
    {"data":"region", className: "text-center"},
    {"data":"sucursal", className: "text-center"},
   

    ],
        "order" : [[0, "asc"]],

      "scrollX": false,
      "scrollY": false,
      "info":     false,
      "scrollCollapse": false,

      language:{
        "sProcessing":     "Procesando...",
         "sLengthMenu":     "Mostrar _MENU_ registros",
         "sZeroRecords":    "No se encontraron resultados",
         "sEmptyTable":     "Ningún dato disponible en esta tabla",
         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix":    "",
         "sSearch":         "Buscar:",
         "Print":           "Imprimir",
         "sUrl":            "",
         "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
         "oPaginate": {
           "sFirst":    "Primero",
           "sLast":     "Último",
           "sNext":     "Siguiente",
           "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }

    });

    }

</script>

