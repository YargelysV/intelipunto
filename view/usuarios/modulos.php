<?php
//session_start();
$usuario_agg=$_SESSION["session_user"];
$pass=$_SESSION["session_var"];

if (isset($_SESSION["session_user"]))
{
	$login=$_REQUEST["login"];
	$acceso=$_REQUEST["acceso"];

	$controlador =new controladorUsuario();
    $resultado= $controlador->listarmodulos($login,$acceso);


?>
<input type="hidden" value="<?php echo $usuario_agg;?>" name="usuarioagg"  id="usuarioagg" />
<input type="hidden" value="<?php echo $login;?>" name="usuario"  id="usuario" />
<input type="hidden" value="<?php echo $acceso;?>" name="usuario"  id="acceso" />
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><i class="fa fa-user fa-fw"></i>Permisos Usuario: <?php echo $login?> </h1>
                    </div>

					 <div class="col-lg-12">
                    

                    	<table class="table table-bordered table-striped"   id="datatable_modulos" style="width: 100%"> 
		                   <thead>
		                      <tr>   
                          <th>Nombre del M&oacute;dulo</th>   
                          <th>codmodulo</th>                    
		                      <th>Estatus</th>                      
                          <th>Tipo de Acceso</th>                      
		                      </tr>
		                   </thead>
		                </table>		
						
					</div>
				</div> 
			</div>

		</div>



    <div align="center" class="col-md-4 col-md-offset-5" >
		<input type="button" class="btn btn-success"   name="volver" value="Volver" onClick="window.location.href='usuarios'"/>
	</div>
	<br><br>
	<?php
}
	else{
	header("Location:../salir");
	}


?>


<script type="text/javascript">
	
 function dtCore() {


  var usuario=document.getElementById('usuario').value;
  var acceso=document.getElementById('acceso').value;


    $('#datatable_modulos').DataTable({
   
    "ajax": {
    "url": "view/usuarios/selectModulos.php",
          "type": "POST",
          "data": {"login":usuario, "acceso":acceso}
         
    },
    "columns": [
    {"data":"nmodulo", className: "text-center"},
    {"data":"codmodulo", className: "text-center"},
    {"data":"status",
        render : function ( data, type, full, meta ) {
          if (usuario==usuario && full.codmodulo == "GE"){
              return '<input type="checkbox" class="check-buttons" checked value="1" disabled="disabled">';

          } else {
            if (data == 1 || data == "1" || data === "1") {
              return '<input type="checkbox" class="check-buttons" checked value="1">';
            }else 
              return '<input type="checkbox" class="check-buttons" value="0">';
          }
          
        } 
      },
    {"data":"codtipo", 
      render : function ( data, type, full, meta ) {
          if (usuario==usuario && full.codmodulo == "GE"){
              return '<form><div class="form-group"><label>Lectura <input type="radio" class="radio-buttons" name="permiso" value="L" disabled="disabled"></label><label> &nbsp;Escritura <input type="radio" class="radio-buttons" name="permiso" checked value="W" disabled="disabled"></label></div></form>';
          } else {
    
            if (data == "W" || data === "W") {
              return '<form><div class="form-group"><label>Lectura <input class="radio-buttons" type="radio" name="permiso" value="L"></label><label> &nbsp;Escritura <input class="radio-buttons" type="radio" name="permiso" checked value="W"></label></div></form>';
            }else if (data == "L" || data === "L"){
              return '<form><div class="form-group"><label>Lectura <input type="radio" class="radio-buttons" name="permiso" checked value="L"></label><label> &nbsp;Escritura <input type="radio" class="radio-buttons" name="permiso" value="W"></label></div></form>';
            }else
              return "";
          } 
        } 

    }

    ],
        "order" : [[0, "asc"]],

         "columnDefs": [
           {
                 "targets": [1],
                 "visible": false,
             }
         ],
      "scrollX": false,
      "scrollY": false,
      "info":     false,
      "scrollCollapse": false,

      language:{
        "sProcessing":     "Procesando...",
         "sLengthMenu":     "Mostrar _MENU_ registros",
         "sZeroRecords":    "No se encontraron resultados",
         "sEmptyTable":     "Ningún dato disponible en esta tabla",
         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix":    "",
         "sSearch":         "Buscar:",
         "Print":           "Imprimir",
         "sUrl":            "",
         "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
         "oPaginate": {
           "sFirst":    "Primero",
           "sLast":     "Último",
           "sNext":     "Siguiente",
           "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }

    });

    $('#datatable_modulos tbody').on( 'click', 'input.check-buttons', function () {
      //alert('algo');
       var table=$('#datatable_modulos').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = $("#usuario").val();
       var acceso = $("#acceso").val();
       var codmodulo = T.codmodulo;
       var usuarioadm = $("#usuarioagg").val();
       var status = T.status;
       var checks=$(this).val();
      //alert(status);

      if (checks) {
        
        var ruta="view/usuarios/editarestatusmodulo.php?r=1&login="+login+"&acceso="+acceso+"&codmodulo="+codmodulo+"&usuario="+usuarioadm+"&status="+status;
        $.ajax({
            url : ruta,
            type : "POST",
           
            success: function(data){

                if(data == 1  || data == '1'){
                    var table=$('#datatable_modulos').DataTable();
                    table.ajax.reload();    

                }else{                   
                    alertify.error('Error el módulo no fue asignado');
                }
            }
        })
        
        } else {
      
        return false;
        }
      });

        $('table#datatable_modulos tbody').on( 'click', 'input.radio-buttons', function () {
      //alert('algo');
       var table=$('table#datatable_modulos').DataTable();
       var T =  table.row($(this).parents("tr")).data();
       var login = $("#usuario").val();
       var acceso = $("#acceso").val();
       var codmodulo = T.codmodulo;
       var codtipo = T.codtipo;
       var status = T.status;
       var usuarioadm = $("#usuarioagg").val();

      // alert(codtipo);
       var ruta="view/usuarios/guardartipoacceso.php?r=1&login="+login+"&acceso="+acceso+"&codmodulo="+codmodulo+"&usuario="+usuarioadm+"&status="+status+"&codtipo="+codtipo;
        $.ajax({
            url : ruta,
            type : "POST",
            //data : {idLote:idLote,IdEquipo:IdEquipo},
            success: function(data){

                if(data == 1  || data == '1'){
                    

                    if (codtipo=='W') {
                    var table=$('#datatable_modulos').DataTable();
                    table.ajax.reload();    
                     
                    } else {

                     // alertify.success('El tipo de acceso a cambiado a Escritura'); 
                    var table=$('#datatable_modulos').DataTable();
                    table.ajax.reload();    
                    }

                }else{
                    alertify.error();
                }
            }
        })         
      });

  }


</script>

