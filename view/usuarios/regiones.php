<?php
//session_start();

$pass=$_SESSION["session_var"];
$usuario_acceso=$_SESSION["session_user"];

if (isset($_SESSION["session_user"]))
{
	$login=$_REQUEST["login"];

	$controlador =new controladorUsuario();
    $resultado= $controlador->splistaregion();


?>


<input type="hidden" value="<?php echo $login;?>" name="usuario"  id="usuario" />
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><i class="fa fa-user fa-fw"></i>Permisos Usuario: <?php echo $login?> </h1>
                    </div>

					<div class="col-lg-12">
            <table class="table table-bordered table-striped" id="datatable_regiones" style="width: 100%"> 
		                   <thead>
		                      <tr>
		                      <th>Nº</th>                      
                              <th>Regiones</th>                                      
                              <th>Accion</th>                                      
		                      <th>codregion</th> 
                              </tr>
		                   </thead>
		                </table>		
						
					</div>
				</div> 
			</div>

		</div>



    <div align="center" class="col-md-4 col-md-offset-5" >
		<input type="button" class="btn btn-success"   name="volver" value="Volver" onClick="window.location.href='usuarios'"/>
	</div>
	<br><br>
	<?php
}
	else{
	header("Location:../salir");
	}


?>


    <div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header" align="center">
              <h4 class="modal-title" id="exampleModalLabel" ></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
            </div> 
           
           <form id="formRegiones" method="post" >
                <div class="col-lg-12">  
                    <br>
                <input type="hidden" name="usuario_acceso" value="<?php echo $usuario_acceso; ?>">
                <input type="hidden" name="login" id="login" value="<?php echo $login; ?>">
                <input type="hidden" name="region" id="region">
                  <label>Sucursal</label>
                     <select class="form control" name="sucursal[]" id="sucursal"  multiple="multiple" style="width:180px;padding-left:15px;color:#000000" >
                     </select>               
                </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnGuardarSuc" class="btn btn-primary">Guardar</button>
              </div>    
          </form>
            </div>

      </div>
  </div>

<script type="text/javascript">
	
 function dtCore() {

  var usuario=document.getElementById('usuario').value;

    tablaregiones=$('#datatable_regiones').DataTable({
   
    "ajax": {
    "url": "view/usuarios/selectRegiones.php",
          "type": "POST",
          "data": {"login":usuario}         
    },
    "columns": [
    {"data":"sec", className: "text-center"},
    {"data":"dregion", className: "text-center"},
    {"defaultContent":"<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='fa fa-edit'></i></button></div></div>"},
    {"data":"cregion", className: "text-center"}

    ],
    "columnDefs": [
           {
                 "targets": [3],
                 "visible": false,
             }
         ],

      "scrollX": false,
      "scrollY": false,
      "info":     false,
      "scrollCollapse": false,

      language:{
        "sProcessing":     "Procesando...",
         "sLengthMenu":     "Mostrar _MENU_ registros",
         "sZeroRecords":    "No se encontraron resultados",
         "sEmptyTable":     "Ningún dato disponible en esta tabla",
         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix":    "",
         "sSearch":         "Buscar:",
         "Print":           "Imprimir",
         "sUrl":            "",
         "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
         "oPaginate": {
           "sFirst":    "Primero",
           "sLast":     "Último",
           "sNext":     "Siguiente",
           "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }

    });

    $(document).on("click", ".btnEditar", function(){           
    opcion = 3;//editar
    fila = $(this).closest("tr");         
    cregion = parseInt(fila.find('td:eq(0)').text()); //capturo el ID  
    dregion = fila.find('td:eq(1)').text();             

    $("#region").val(cregion);
    $("#descreg").val(dregion);
    $(".modal-header").css("color", "black" );
    $(".modal-title").text("Asignación de Sucursal");   
    $('#modalCRUD').modal('show');  
    $("select#sucursal").empty();
    setTimeout(function(){ 
     selectsucursales();
     //ocultarperfil();  
   });  
});
  }

    function selectsucursales() {

        var region=$('input#region').val();
        var login=$('input#login').val();
        var existe =[];
    $.getJSON("view/usuarios/selectsucursalasig.php",{region:region, login:login},function(datos){
        if(datos != 0){
            $.each(datos, function (K, V) {         
                existe.push(parseInt(V['idsuc']));
            });   
        }
    });

     var region=$('input#region').val();
       $.getJSON("view/usuarios/selectsucursal.php",{region:region},function(datos){
         if(datos != 0){

        var $SELECT = $("select#sucursal").select2({
                placeholder: 'Seleccione',
                allowClear: true,
            });
               
        $.each(datos, function (K, V) {
            var id =parseInt(V['id']);
            if (existe.includes(id)) {

                $SELECT.append($('<option>', { 
                    value: V['id'],
                    text: V['reg'],
                    selected:true
                }));
            }else{

                $SELECT.append($('<option>', { 
                    value: V['id'],
                    text: V['reg']
                }));
            }


        });
      }  
    });


  } 

  
    $('#formRegiones').submit(function(e){ 
      e.preventDefault();
        parametros = $("form#formRegiones").serialize();
        $.ajax({
            url : "view/usuarios/guardarSucursales.php",
            type : "POST",
            data : parametros,
            success: function(data){

            if(data == 0){
              alertify.success('Las sucursales han sido eliminadas!..');
              tablaregiones.ajax.reload(null, false);
              setTimeout(function(){ window.location.reload(0); }, 3000); 
                
            }else if (data != 1){
              alertify.success('Las sucursales han sido asignadas correctamente!..');
              tablaregiones.ajax.reload(null, false);
              setTimeout(function(){ window.location.reload(0); }, 3000); 
                
            } else {

                alertify.error('Error!. La sucursal no fue asignada.');
            }
            }
        })
    });



</script>



