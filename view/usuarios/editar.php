<?php

	if (isset($_GET['login'])){
		$controlador =new controladorUsuario();
		$tipodoc=$controlador->tipodocumento();
		$tipousuario=$controlador->tipousuario();

		$controladorarea =new controladorUsuario();
		$tipoarea=$controladorarea->spareainteligensa();

		$controlador_region =new controladorUsuario();
		$tiporegion=$controlador_region->spregionesinteligensa();		

$_POST['tipodoc'] = isset($_POST['tipodoc']) ? $_POST['tipodoc'] : "";
$_POST['coddocumento'] = isset($_POST['coddocumento']) ? $_POST['coddocumento'] : "";
$_POST['clave'] = isset($_POST['clave']) ? $_POST['clave']  : "";

$fechanac=date('d-m-Y');
		$row=$controlador->ver($_GET['login']);
	}else
	{
		header('Location:../salir.php');
	}

	if ((isset($_POST['enviar']))&& (($_POST['enviar'])=='Editar')){
		

		$r=$controlador->editar(trim($_POST['tipodoc']), trim($_POST['coddocumento']), trim($_GET['login']), trim($_POST['clave']), trim($_POST['nombres']),trim($_POST['apellidos']), trim($fechanac), trim($_POST['correo']), trim($_POST['codtipousuario']), trim($_POST['activo']),  trim($_POST['cargo']),  trim($_POST['area']),  trim($_POST['region']), $_SESSION["session_user"]);

		//echo $r;
		if($r){
			if ($_POST['estatus']==0)
			{
			$controladortiempo =new controladorUsuario();
			$tiempo=$controladortiempo->spactualizarmotivo($_GET['login']);
			}

			if($_POST['activo']==0)
			{
			$controladormotivo =new controladorUsuario();
			$motivo=$controladormotivo->spinsertarmotivo($_GET['login'],$_POST['motivos'],$_SESSION["session_user"]);
			}
		echo "<script>alertify.success('Usuario editado con &Eacute;xito.');</script>";
		$controlador =new controladorUsuario();
		$row=$controlador->ver($_GET['login']);
		}
		else{
		echo "<script>alertify.error('No ha realizado ning&uacute;n cambio.');</script>";
		}

	}
?>
<input type="text" id="idregio" value="<?php echo $row['idregion'] ?>">
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><i class="fa fa-user fa-fw"></i>Editar Usuario</h1>
                    </div>

                     <div class="col-lg-12">
                      <div class="row">
                        <div class="panel panel-yellow" align="center">
						   <div class="panel-body">
                            <div class="table-responsive">


	<form action="" method="post" name="editarusuario">
								<table border="0" align="center">
									<tbody>
									<tr align="center">
										<td align="center">Tipo Documento</td>
										<td align="center">Documento de Identificac&oacute;n</td>
									</tr>
									<tr align="center">
										<td align="center">	<input  class="inputcentrado"  style="color: #000000" type="text" name="tipodoc"  value="<?php echo $row['tipodoc'];?>"   disabled="disabled"  /></td>
										<td align="center"><input  class="inputcentrado"  style="color: #000000" type="number" name="coddocumento"  value="<?php echo $row['coddocumento'];?>"   disabled="disabled"   /></td>
									</tr>


									<tr align="center">
										<td align="center">Usuario</td>
										<td align="center">Clave</td>
									</tr>
									<tr align="center">
										<td align="center">	<input  class="inputcentrado"  style="color: #000000" type="text" name="login"  value="<?php echo $row['usuario'];?>"  disabled="disabled" 	 /></td>
										<td align="center"><input  class="inputcentrado"  style="color: #000000" type="password" name="clave"  value="<?php echo '**********';?>" disabled="disabled" /></td>
									</tr>

									<tr align="center">
										<td align="center">Nombres</td>
										<td align="center">Apellidos</td>
									</tr>
									<tr align="center">
										<td align="center">	<input  class="inputcentrado"  style="text-transform:uppercase; color:#000000;" onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" name="nombres" maxlength="50"  value="<?php echo $row['nombres'];?>" required  onkeypress="return soloLetras(event);"/></td>
										<td align="center"><input  class="inputcentrado"  style="text-transform:uppercase; color:#000000;" onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" name="apellidos" maxlength="50"  value="<?php echo $row['apellidos'];?>" required onkeypress="return soloLetras(event);" /></td>
									</tr>

									<tr align="center">
										<td align="center">Correo</td>
										<td align="center">Tipo de Usuario</td>
									</tr>
									<tr align="center">
										<td align="center"><input  class="inputcentrado"  style="color: #000000" type="email" name="correo"  maxlength="50" value="<?php echo $row['correo'];?>" required /></td>
										<td align="center">
										<select name="codtipousuario" style="color:#000000" >
											<?php
											while ($row1=pg_fetch_array($tipousuario)):

											?>
											<option value="<?php echo $row1['codtipousuario']?>" <?php
											if ($row1['codtipousuario']==$row['codtipousuario'])
											{
											echo "selected";
											}?>><?php
											echo $row1['tipousuario']
										    ?></option>

											<?php
											endwhile;
											?>
											</select>
										</td>
									</tr>

									<tr align="center">
										<td align="center">Cargo</td>
										<td align="center">&Aacute;rea</td>
									</tr>
									<tr align="center">
										<td align="center">	<input  class="inputcentrado"  style="text-transform:uppercase; color:#000000;" onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" name="cargo" maxlength="50"  value="<?php echo $row['cargo'];?>" required /></td>
										<td align="center">
										<select name="area" style="color:#000000" >
											<?php
											while ($row2=pg_fetch_array($tipoarea)):
											?>
											<option value="<?php echo $row2['codarea']?>" <?php
											if ($row2['codarea']==$row['codarea'])
											{
											echo "selected";
											}?>><?php
											echo $row2[	'narea']
										    ?></option>

											<?php
											endwhile;
											?>
											</select>
										</td>

									</tr>

									<tr align="center">
										<td align="center">Estatus</td>
										<td align="center">Región</td>
									</tr>
									<tr align="center">								
										<td align="center">
										<?php
										if ($row['activo']==2){?>
										<input  class="inputcentrado"  style="text-transform:uppercase; color:#000000;" value="BLOQUEADO" readonly />
										<input type="hidden" name="activo" value="2" />

										<?php
										}
										elseif ($row['activo']==1){?>
										<font color="#000000">Activo: <input type="radio" name="activo" value="1" checked="checked" onclick="motivoocultar()" />
										Inactivo: <input type="radio" name="activo" value="0"  onclick="motivomostrar()" /></font>

										<input type="hidden" name="estatus" value="1" />

										<?php } else {?>
										<font color="#000000">Activo: <input type="radio" name="activo" value="1" onclick="motivoocultar()"/>
										Inactivo: <input type="radio" name="activo" value="0"  checked="checked"  onclick="motivomostrar()"/></font>
										<input type="hidden" name="estatus" value="0" />

								<div id="motivo">
								<table border="0" align="center">
									<tr align="center">
										<td align="center">Motivo</td>
										<td align="center" width="50%"></td>
									</tr>
										<tr align="center">
										
										<td align="center"><input  class="inputcentrado"  style="color: #000000"  type="text" name="motivos" id="motivos" maxlength="60"  value="<?php echo $row['motivo'];?>" disabled="disabled"/></td>
										<td align="center" width="50%"></td>
									</tr>
								</table>
								</div>

										<?php } ?>
										</td>
									<td align="center">
										<select name="region" id="region" style="color:#000000" >
											<?php
											while ($row3=pg_fetch_array($tiporegion)):
											?>
											<option value="<?php echo $row3['codregion']?>" <?php
											if ($row3['codregion']==$row['idregion'])
											{
											echo "selected";
											}?>><?php
											echo $row3[	'descripcionregion']
										    ?></option>

											<?php
											endwhile;
											?>
											</select>
										</td>

									</tr>
									</tbody>
								</table>

							<div id="motivo" style="display:none">
								<table border="0" align="center">
									<tr align="center">
										<td align="center">Motivo</td>
										<td align="center" width="50%"></td>
										
									</tr>
										<tr align="center">
										<td align="center"><input  class="inputcentrado"  style="color: #000000"  type="text" name="motivos" id="motivos" maxlength="60"  value="<?php echo $row['motivo'];?>"/></td>
										<td align="center" width="50%"></td>
										
									</tr>
								</table>
							</div>

									<br/>

									<tr align="center">
										<td colspan="2" align="center">
										<input type="submit" class="btn btn-primary"  name="enviar" value="Editar" />
										<input type="button" class="btn"  name="volver" value="Volver" onClick="window.location.href='usuarios.php'"/>										</td>
									</tr>
									 </form>




							</div>
						  </div>
						</div>
                      </div>
                    </div>

               <!-- /.row -->
               </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->
        </div>
