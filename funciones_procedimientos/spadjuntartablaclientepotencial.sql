﻿-- Function: spadjuntartablaclientepotencial()

-- DROP FUNCTION spadjuntartablaclientepotencial();

CREATE OR REPLACE FUNCTION spadjuntartablaclientepotencial()
  RETURNS boolean AS
$BODY$
	DECLARE usuariocarga CHARACTER VARYING;
	Begin
--select * from clie_tblclientepotencial
--select * from clie_tbltransferencia
		insert into clie_tblclientepotencial
		(fecharecepcion, codigobanco, tipopos,  tipocliente, nafiliacion, cantterminalesasig, razonsocial, 
		coddocumento, acteconomica, rlegal,correorl, telf1, telf2, direccion_instalacion, estado, municipio, parroquia, razoncomercial, agencia,
		usuario,idate, namearchivo, sizearchivo, ejecutivo, ejecutivocall,cbancoasig)
		select fecharecepcion, codigobanco,tipopos,upper(tipocliente)as tipocliente, nafiliacion, cantterminalesasig, 
		upper(razonsocial)as razonsocial, upper(coddocumento)as coddocumento, upper(acteconomica)as acteconomica, upper(rlegal)as rlegal, upper(correorl)as correorl, telf1, telf2, 
		upper(direccion_instalacion)as direccion_instalacion,upper(estado)as estado,upper(municipio)as  municipio,upper(parroquia) as parroquia, upper(razoncomercial)as razoncomercial, agencia,
		upper(usuario) as usuario,idate, upper(namearchivo) as namearchivo, sizearchivo, ejecutivo, ejecutivocall,cbancoasig
		from clie_tbltransferencia;

		usuariocarga:=(SELECT USUARIO FROM clie_tblclientepotencial WHERE NAFILIACION!='' AND IDSTATUS='0'  LIMIT  1);
		
		update clie_tblclientepotencial 
		set idstatus='3', fechaenviocomercializacion=now(), fecharecepcomercializacion=now(), usuariorecibe=usuariocarga, usuarioenvio=usuariocarga
		where NAFILIACION!='' AND IDSTATUS='0';
	
		
		return true;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spadjuntartablaclientepotencial()
  OWNER TO postgres;
