﻿
CREATE FUNCTION spverificatecnicorep(codtecnico integer, tipodoc character varying, documento character varying)
  RETURNS SETOF tbltecnicos AS
$BODY$

Begin
	return 	QUERY SELECT * FROM tbltecnicos where codtecnicos=codtecnico or coddocumento=tipodoc||'-'||documento;
	
	

End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
