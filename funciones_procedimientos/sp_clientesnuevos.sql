﻿-- Function: sp_clientesnuevos(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_clientesnuevos(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_clientesnuevos(inombre character varying, iapellido character varying, codbanco character varying, irif character varying, itlf character varying, bancointeres character varying, iusuario character varying, pros_razonsocial character varying, pros_tipopos character varying, pros_cantidadpos character varying, pros_correo character varying, pros_direccion character varying, pros_mediocontacto character varying, pros_tipocliente character varying, banco2 character varying)
  RETURNS boolean AS
$BODY$
		
DECLARE strcodbanco character varying;
DECLARE codigobank integer;
	Begin 

		if(codbanco<>'')then

		codigobank=codbanco::integer;
		strcodbanco=codigobank::character varying;

		end if;
			
		INSERT INTO tbl_prospectos (nombre,apellido,codigobanco,coddocumento, telf1,bancointeres,idate,usuario, razonsocial, tipopos, cantterminalesasig, correorl, direccion_instalacion, mediocontacto, tipocliente,fecharecepcion,cbancoasig )

		values(inombre , iapellido , strcodbanco , irif , itlf , bancointeres , now(), iusuario , pros_razonsocial,pros_tipopos , 
				case when pros_cantidadpos='' then  null else   pros_cantidadpos::integer end, 
		pros_correo , pros_direccion , pros_mediocontacto , pros_tipocliente, now(),banco2 );
		return true;
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_clientesnuevos(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
