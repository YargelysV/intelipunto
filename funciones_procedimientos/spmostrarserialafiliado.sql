﻿-- Function: spmostrarserialafiliado(character varying, character varying, character varying)

-- DROP FUNCTION spmostrarserialafiliado(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION spmostrarserialafiliado(IN serial character varying, IN afiliado character varying, IN cliente character varying)
  RETURNS TABLE(secuencial bigint, nafiliado character varying, serialpo character varying, nombrearchivo character varying, fecharecepalma date, tecnico integer, fechagest date, estatus integer, fechainst date, fechacarga timestamp with time zone, usuario character varying, estatusinst character varying, ntecnico text, terminal character varying, observacion character varying) AS
$BODY$
	
Begin

return query 	--select * from tblgestionserviciotecnico


			select row_number()  OVER(order by st.fechacarga),
			st.nroafiliado, st.serialpos, st.nombrearchivo, st.fecharecepalmacen, st.codtecnico, st.fechagestion, st.estatus,
			st.fechainstalacion, st.fechacarga, st.usuariocarga, pos.descestatus, t.nombres||' '||t.apellidos  as nombres, inv.numterminal, st.observacion
			from tblgestionserviciotecnico st 
			inner join tblestatusinspos pos
			on pos.codestatus=st.estatus
			inner join tbltecnicos t
			on t.codtecnicos=st.codtecnico
			INNER JOIN tblinventariopos inv
			on inv.serialpos=st.serialpos
			where st.serialpos=serial and st.nroafiliado=afiliado and st.id_cliente::integer=cliente::integer
			order by row_number()  OVER(order by st.fechacarga);
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrarserialafiliado(character varying, character varying, character varying)
  OWNER TO postgres;
