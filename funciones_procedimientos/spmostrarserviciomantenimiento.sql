﻿-- Function: spmostrarserviciomantenimiento(character varying, character varying)

-- DROP FUNCTION spmostrarserviciomantenimiento(character varying, character varying);

CREATE OR REPLACE FUNCTION spmostrarserviciomantenimiento(IN nafiliado character varying, IN nroterminal character varying)
  RETURNS TABLE(fechacarga text, nafiliacion character varying, razonsocial character varying, documento character varying, nterminal character varying, serialpos character varying, tipopos character varying, marcapos character varying, ibp character varying, codformapago integer,
   ntarjeta character varying, fechavenc date, ncuenta character varying, bancoorigen integer, nameformapago character varying, codtarjeta integer, codseguridad character varying, nombres character varying, coddocumento character varying, nbancoorigen character varying) AS
$BODY$
	Begin -- select * from tblrecaudacionmant

				return query select  
				to_char(cp.idate::date,'DD/MM/YYYY') as fechacarga,
				cp.nafiliacion, cp.razonsocial, cp.coddocumento, i.numterminal, i.serialpos, cp.tipopos,
				mar.marcapos, bc.ibp, f.formapago, f.ntarjeta,f.fechavenc, f.ncuenta,  
				f.banco, p.nameformapago, f.codtarjeta,  f.codseguridad,f.nombres, f.coddocumento,
				(select tblbanco.ibp from tblbanco where tblbanco.codigobanco::integer=f.banco::integer) as nbancoorigen
				FROM clie_tblclientepotencial as cp
				inner join tblbanco bc on cp.codigobanco::integer=bc.codigobanco::integer
				full outer join tblinventariopos i
				on i.nroafiliacion=cp.nafiliacion
				full outer join tblserialpos s
				on s.serial=i.serialpos
				full outer join tblrecaudacionmant f
				on f.nafiliacion=cp.nafiliacion
				full outer join tblformapago p
				on p.codformapago=f.formapago
				full outer join tbltipotarjeta t
				on t.codtarjeta=f.codtarjeta
				full outer join tblmarcapos mar
				on mar.codtipo=cp.codmarcapos
				full outer join tblgestionserviciotecnico gs
				on gs.nroafiliado=cp.nafiliacion and gs.estatus='4' 
				WHERE cp.codestatus in ('1','2') and 
				i.nroafiliacion is not null and cp.mantenimiento='1' and 
				cp.nafiliacion=nafiliado or i.numterminal=nroterminal
				
				union -- select * from tblrecaudacionmant
				
				SELECT 
				to_char(m.fecha::date,'DD/MM/YYYY') as fechacarga, m.nroafiliacion,  m.razonsocial, m.documento, m.numterminal, m.serialpos, tp.tipopos,
				mar.marcapos, bc.ibp, f.formapago, f.ntarjeta,f.fechavenc, f.ncuenta,  
				f.banco, p.nameformapago, f.codtarjeta,  f.codseguridad,f.nombres, f.coddocumento,
				(select tblbanco.ibp from tblbanco where tblbanco.codigobanco::integer=f.banco::integer)  as nbancoorigen
				from tblmantenimientonuevo m
				inner join tbltipopos tp 
				on tp.codtipo=m.tipopos::integer
				full outer join tblmarcapos mar
				on mar.codtipo::integer=m.marcapos::integer
				inner join tblbanco bc 
				on m.codigobanco::integer=bc.codigobanco::integer 
				full outer join tblrecaudacionmant f
				on f.nafiliacion=m.nroafiliacion
				full outer join tblformapago p
				on p.codformapago=f.formapago
				full outer join tbltipotarjeta t
				on t.codtarjeta=f.codtarjeta
				where m.nroafiliacion=nafiliado or m.numterminal=nroterminal and m.nroafiliacion<>''
				ORDER BY nafiliacion;
				
--  select * from spmostrarserviciomantenimiento('A80','555689')  select * from tblmantenimientonuevo

						
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrarserviciomantenimiento(character varying, character varying)
  OWNER TO postgres;
