﻿-- Function: spbuscarclienteacuerdorif(character varying)

-- DROP FUNCTION spbuscarclienteacuerdorif(character varying);

CREATE OR REPLACE FUNCTION spbuscarclienteacuerdorif(IN irif character varying)
  RETURNS TABLE(correlativo bigint, enlaceafiliado text, fecharecepcion date, codigobanco integer, ibp character varying, tipopos character varying, cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, calleav character varying, localidad character varying, sector character varying, urbanizacion character varying, local character varying, estado character varying, codigopostal character varying, puntoref character varying, usuario character varying, idate date, namearchivo character varying, sizearchivo character varying, direccion text, tabladireccion character varying) AS
$BODY$
	
	Begin
			return query 
			SELECT row_number()  OVER(order by result.fecharecepcion),* from (	
				select 
				concat_ws('/', tr.coddocumento, tr.codigobanco::integer, tr.fecharecepcion::date,tr.namearchivo,irif,tr.nafiliacion) as enlace, 
				tr.fecharecepcion::date,
				tr.codigobanco::integer, b.ibp, tr.tipopos,  case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, d.d_localidad,
				d.d_sector, d.d_urbanizacion, d.d_nlocal, d.d_estado, d.d_codepostal, d.d_ptoref, tr.usuario, tr.idate::date, 
				tr.namearchivo, tr.sizearchivo,  
				d.d_calle_av||' '||d.d_localidad||' '||d.d_sector||' '||d.d_urbanizacion||' '||d.d_nlocal||' '||d.d_estado||' '||d.d_codepostal||' '||d.d_ptoref as direccion, 
				'NI'::character varying instalacion
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				inner join tbldirecciones d
				on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='4'
				where (tr.coddocumento LIKE '%'||irif||'%' or tr.nafiliacion LIKE '%'||irif||'%' or tr.razonsocial LIKE '%'||irif||'%')   
				union
				select 
				concat_ws('/', tr.coddocumento, tr.codigobanco::integer, tr.fecharecepcion::date,tr.namearchivo,irif,tr.nafiliacion) as enlace, 
				tr.fecharecepcion::date,
				tr.codigobanco::integer, b.ibp, tr.tipopos,  case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.calleav, tr.localidad,
				tr.sector, tr.urbanizacion, tr.local, tr.estado, tr.codigopostal, tr.puntoref, tr.usuario, tr.idate::date, 
				tr.namearchivo, tr.sizearchivo,
				tr.calleav||' '||tr.localidad||' '||tr.sector||' '||tr.urbanizacion||' '||tr.local||' '||tr.estado||' '||tr.codigopostal||' '||tr.puntoref as direccion, 'BI'::character varying instalacion
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				where tr.coddocumento||tr.fecharecepcion||tr.codigobanco||tr.namearchivo not in (select 
					tr.coddocumento||tr.fecharecepcion||tr.codigobanco||tr.namearchivo
					from clie_tblclientepotencial tr
					inner join tblbanco b on 
					b.codigobanco::integer=tr.codigobanco::integer
					inner join tbldirecciones d
					on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='4'
					where (tr.coddocumento LIKE '%'||irif||'%' or tr.nafiliacion LIKE '%'||irif||'%' or tr.razonsocial LIKE '%'||irif||'%'))
			      and (tr.coddocumento LIKE '%'||irif||'%' or tr.nafiliacion LIKE '%'||irif||'%' or tr.razonsocial LIKE '%'||irif||'%')
			)result  order by row_number()  OVER(order by result.fecharecepcion), result.coddocumento;         
	
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarclienteacuerdorif(character varying)
  OWNER TO postgres;
