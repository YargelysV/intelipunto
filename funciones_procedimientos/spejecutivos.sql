﻿-- Function: spejecutivos(numeric, integer, character varying, character varying, character varying, character varying, character varying, integer, character varying)

-- DROP FUNCTION spejecutivos(numeric, integer, character varying, character varying, character varying, character varying, character varying, integer, character varying);

CREATE OR REPLACE FUNCTION spejecutivos(operacion numeric, codigovendedor integer, loginvendedor character varying, tipodoc character varying, documento character varying, nombre character varying, apellido character varying, ejecutivo integer, login character varying)
  RETURNS boolean AS
$BODY$

	DECLARE result int;
	
	Begin

-- select * from adm_ejecutivo
			CASE operacion

				when 2 then
					INSERT INTO adm_ejecutivo
					values(codigovendedor, loginvendedor, tipodoc||'-'||documento, nombre, apellido, login, now(),ejecutivo);
					return true;

				when 3 then
						--select * from tblusuario

					result:=(select count(*) from tblusuario where coddocumento=documento);
						if (result>0) then
							update tblusuario set 
						nombres=       CASE 
								WHEN nombre IS NULL OR nombre = '' THEN nombres
								ELSE nombre END,
						apellidos=      CASE 
								WHEN apellido IS NULL OR apellido = '' THEN apellidos
								ELSE apellido END,
						login_user=login, idate=now()		
							 where coddocumento=documento;	
						end if;
						
					update adm_ejecutivo set 
					 
					  codvendedor = CASE 
							WHEN codigovendedor IS NULL OR codigovendedor = 0 THEN codvendedor
							ELSE codigovendedor END, 
					  nvendedor=    CASE 
							WHEN loginvendedor IS NULL OR loginvendedor = '' THEN nvendedor
							ELSE loginvendedor END,
					  nombres=      CASE 
							WHEN nombre IS NULL OR nombre = '' THEN nombres
							ELSE nombre END,
					 apellidos=     CASE 
							WHEN apellido IS NULL OR apellido = '' THEN apellidos
							ELSE apellido END,
					codejecutivos=   CASE 
							WHEN ejecutivo IS NULL OR ejecutivo = 0 THEN codejecutivos
							ELSE ejecutivo END,						
					usuario=login, idate=now()
					WHERE	substr(coddocumento,strpos(coddocumento,'-')+1)=documento;
					
					if ejecutivo=1 then 
					update tblbanco set coddocumento=0 WHERE substr(coddocumento,strpos(coddocumento,'-')+1)=documento;
					end if;
					
					return true;
				when 4 then
					Delete from adm_ejecutivo WHERE substr(coddocumento,strpos(coddocumento,'-')+1)=documento;
					return true;
				else 
				return false;
			end case;	
		End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spejecutivos(numeric, integer, character varying, character varying, character varying, character varying, character varying, integer, character varying)
  OWNER TO postgres;
