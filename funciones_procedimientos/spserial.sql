﻿--DROP FUNCTION spserial();

CREATE OR REPLACE FUNCTION spserial()
  RETURNS table (serial character varying) AS
$BODY$
	
	Begin
	return query SELECT serialpos FROM tblinventariopos where asignado='0' order by serialpos;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spserial()
  OWNER TO postgres;

--  select spserial()
