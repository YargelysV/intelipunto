﻿-- Function: spadjuntarserie(character varying, character varying)

-- DROP FUNCTION spadjuntarserie(character varying, character varying);

CREATE OR REPLACE FUNCTION spadjuntarserie(numserie character varying, tipopos character varying, usuario character varying)
  RETURNS boolean AS
$BODY$
	Begin
			
		INSERT INTO tblserialpos (serial,tipopos,fechacarga,usuariocarga)
		values(numserie,tipopos, now(),usuario);
		return true;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spadjuntarserie(character varying, character varying, character varying)
  OWNER TO postgres;

--select * from tblserialpos