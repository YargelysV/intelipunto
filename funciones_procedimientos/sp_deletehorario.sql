﻿
CREATE OR REPLACE FUNCTION sp_deletehorario(iid_cliente character varying, iprefijobanco character varying, ifechacarga character varying) RETURNS boolean AS
$BODY$
Begin

DELETE FROM tblhoras
WHERE id_cliente = iid_cliente and prefijo_banco = iprefijobanco and fecha_carga = ifechacarga ;
return true;

End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- select sp_deletehorario('435345345','102','2017-10-20')