﻿

-- DROP FUNCTION spadjuntarserie(character varying, character varying);

CREATE OR REPLACE FUNCTION spadjuntartxt(numserie character varying)
  RETURNS boolean AS
$BODY$
	
	Begin
			
		INSERT INTO txt_archivo (fila)
		values(numserie);
		return true;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spadjuntartxt(character varying)
  OWNER TO postgres;
