﻿-- Function: sp_verclienteseriales(integer, date, character varying)

-- DROP FUNCTION sp_verclienteseriales(integer, date, character varying);

CREATE OR REPLACE FUNCTION sp_verclienteseriales(IN banco integer, IN fecha date, IN namearchiv character varying)
  RETURNS TABLE(enlace text, correlativo bigint, fecharecepcion text, nafiliacion character varying, razonsocial character varying, documento character varying, tipopos character varying, cantterminalesasig integer, tipolinea character varying, ibp character varying, namearchivo character varying, estatus integer) AS
$BODY$
    
    Begin
       
                return query 
                select 
                i.serialpos||'/'||tr.nafiliacion||'/'||tr.namearchivo||'/'||tr.idate::date||'/'||i.numterminal||'/'||tr.id_consecutivo, 
                row_number()  OVER(order by tr.namearchivo), 
                 to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
                tr.nafiliacion,tr.razonsocial, tr.coddocumento, tr.tipopos,case when tr.cant_pos_confirm is null then tr.cantterminalesasig 
                else tr.cant_pos_confirm end, tr.tipolinea, b.ibp, tr.namearchivo ,tr.codestatus 
                from clie_tblclientepotencial tr
                inner join tblbanco b on 
                b.codigobanco::integer=tr.codigobanco::integer
                inner join tblinventariopos i on i.nroafiliacion=tr.nafiliacion
                where  tr.codigobanco::integer=banco and tr.fecharecepcion::date=fecha and tr.nafiliacion<>'' and tr.codestatus in('1','2') 
                and 
		 CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				  else tr.namearchivo=namearchiv END
           
           
                ORDER BY row_number()  OVER(order by tr.namearchivo);
 
        
    End;




    
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_verclienteseriales(integer, date, character varying)
  OWNER TO postgres;
