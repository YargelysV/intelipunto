﻿-- Function: speliminararchivocliente(integer, date, character, character varying)

-- DROP FUNCTION speliminararchivocliente(integer, date, character, character varying);

CREATE OR REPLACE FUNCTION speliminararchivocliente(banco integer, fecha date, tipousuario character, login character varying)
  RETURNS boolean AS
$BODY$

	DECLARE tiempo bigint;
	DECLARE existe bigint;
	
	Begin
	--select * from clie_tblclientepotencial
		existe:=(select count(*) from clie_auditoriaarchivoeliminar where  codigobanco::integer=banco::integer
			and fecharecepcion::date=fecha);
		
		if ((tipousuario='A')and (existe=0)) then
			insert into clie_auditoriaarchivoeliminar select * from clie_tblclientepotencial where codigobanco::integer=banco::integer
			and fecharecepcion::date=fecha;
			update clie_auditoriaarchivoeliminar set usuarioelimina=login where usuarioelimina is null;
			delete from clie_tblclientepotencial where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
			return true;
			
			else if ((tipousuario='A')and (existe!=0)) then	
				update clie_auditoriaarchivoeliminar set usuarioelimina=login, idateelimina=now(), usuariorestaura=null, idaterestaura=null  where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
				delete from clie_tblclientepotencial where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
				return true;
				
				else if (tipousuario!='A')  then
					tiempo:=(SELECT extract(days from (timestamp 'now()' - (idate)))as dif_dias  
					from clie_tblclientepotencial where codigobanco::integer=banco::integer and fecharecepcion::date=fecha
					group by dif_dias);

					if ((tiempo=0) and (existe=0)) then
						insert into clie_auditoriaarchivoeliminar select * from clie_tblclientepotencial 
						where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
						update clie_auditoriaarchivoeliminar set usuarioelimina=login where usuarioelimina is null;
						delete from clie_tblclientepotencial where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
						return true;
						else if ((tiempo=0) and (existe!=0)) then
							update clie_auditoriaarchivoeliminar set usuarioelimina=login, idateelimina=now(), usuariorestaura=null, idaterestaura=null  where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
							delete from clie_tblclientepotencial where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
							return true;	
						
							else if (tiempo!=0) then
								return false;
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;				
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION speliminararchivocliente(integer, date, character, character varying)
  OWNER TO postgres;
