﻿-- Function: spcomercializacionreportedetallado(character varying, date, date)

-- DROP FUNCTION spcomercializacionreportedetallado(character varying, date, date);

CREATE OR REPLACE FUNCTION spcomercializacionreportedetallado(IN icodigobanco character varying, IN fechaenvioin date, IN fecharecepcionin date)
  RETURNS TABLE(correlativo bigint, fechaenvio date, fecharecepcion date, estatusventa character varying, estatusafiliado character varying, 
  cbanco character varying, ibp character varying, cantidadpos integer, tipopv character varying, tipoline character varying, tipomodelo character varying,
   tipoclient character varying, razon character varying, documento character varying, afiliado character varying, formapago character varying,
   transferencia character varying) AS
$BODY$
	
	Begin
		return 	QUERY 
			SELECT row_number()  OVER(order by p.fechaenviocomercializacion desc) as correlativo, 
			p.fechaenviocomercializacion, p.fecharecepcomercializacion, s.descestatus as estatusventa, c.descestatus as estatusafiliado,
			p.codigobanco, b.ibp, case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end, p.tipopos, p.tipolinea, p.tipomodelonegocio, p.tipocliente, p.razonsocial, p.coddocumento,
			p.nafiliacion, f.nameformapago, fc.ntransferencia 
			FROM clie_tblclientepotencial p 
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			inner join tblestatussolpos s
			on s.codestatus=p.codestatus
			left outer join tblfacturacioncliente fc
			on fc.coddocumento=p.coddocumento and fc.clientebanco::integer=p.codigobanco::integer and fc.namearchivo=p.namearchivo
			full outer join tblformapago f
			on f.codformapago=fc.formapago
			inner join tblestatuscomercializacion c
			on c.idstatus=p.idstatus
			where  p.codigobanco::integer=icodigobanco::integer 
			and 	  
				CASE 
				WHEN fechaenvioin IS NULL  THEN p.fechaenviocomercializacion is null
				ELSE p.fechaenviocomercializacion=fechaenvioin END
			and 
				CASE 
				WHEN fecharecepcionin IS NULL  THEN p.fecharecepcomercializacion is null
				ELSE p.fecharecepcomercializacion=fecharecepcionin END
			and p.codestatus in ('1','2','4') 
			order by  p.idstatus, p.fechaenviocomercializacion;


		
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spcomercializacionreportedetallado(character varying, date, date)
  OWNER TO postgres;

--  SELECT * FROM clie_tblclientepotencial WHERE NAMEARCHIVO ='BANPLUS_05032018_1.csv' CODDOCUMENTO='17754688'