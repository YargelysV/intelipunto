﻿-- Function: sp_tipocuenta()

-- DROP FUNCTION sp_tipocuenta();

CREATE OR REPLACE FUNCTION sp_tipocuenta()
  RETURNS SETOF tbltipocuenta AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tbltipocuenta;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_tipocuenta()
  OWNER TO postgres;
