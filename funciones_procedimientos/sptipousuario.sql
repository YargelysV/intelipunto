﻿-- Function: sptipousuario()

-- DROP FUNCTION sptipousuario();

CREATE OR REPLACE FUNCTION sptipousuario()
  RETURNS SETOF tbltipousuario AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tbltipousuario order by idate ;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sptipousuario()
  OWNER TO postgres;
