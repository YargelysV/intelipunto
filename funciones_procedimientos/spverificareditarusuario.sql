﻿-- Function: spverificareditarusuario(character varying, character varying, character varying, date, character varying, character, character, character varying, integer)

-- DROP FUNCTION spverificareditarusuario(character varying, character varying, character varying, date, character varying, character, character, character varying, integer);

CREATE OR REPLACE FUNCTION spverificareditarusuario(login character varying, nombresn character varying, apellidosn character varying, fechanacn character varying, correon character varying, codtipousuarion character, activon character, cargon character varying, arean integer)
  RETURNS SETOF tblusuario AS
$BODY$
	
	
	Begin
		return 	QUERY SELECT * from tblUsuario where  Nombres=nombresn and Apellidos=apellidosn  and correo=correon and
							  codTipoUsuario=codtipousuarion and activo=activon and
							 usuario=login and  area=arean and cargo=cargon;
							
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverificareditarusuario(character varying, character varying, character varying, character varying, character varying, character, character, character varying, integer)
  OWNER TO postgres;
