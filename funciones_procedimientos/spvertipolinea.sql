﻿CREATE OR REPLACE FUNCTION spvertipolinea()
  RETURNS SETOF tblproveedorsincard AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblproveedorsincard;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spvertipolinea()
  OWNER TO postgres;
