﻿-- Function: sp_reportexdiasdetalles(character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_reportexdiasdetalles(character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_reportexdiasdetalles(IN banco character varying, IN usuarioeje character varying, 
IN tipoposdetalles character varying, IN fechainicio character varying, IN fechafinal character varying, IN tiporeporte character varying)
  RETURNS TABLE(correlativo bigint, nombrebanco character varying, razonsocial character varying, coddocumento character varying, afiliado character varying, 
  ejecutivogestion text, gestionador character varying, fechaedicion date, tipo character varying, modelo character varying, cantidad bigint, ventas_con_fondos bigint, fecha_venta date, facturado bigint, fechafactura date, numerofactura character varying, montousdtransf double precision, montousdefect double precision, montobs double precision, vendedorfinal text ) AS
$BODY$										--select * from sp_reportexdias('','JUVENAL','01-01-2019','') 
											--select * from sp_reportexdiasdetalles('114','CCASTRO','Dial UP','26-07-2019','26-07-2019','1')

		declare tipoposnodefinido character varying;			--select * from sp_reportexdias('', '0', '2017/10/17','2019/04/22','1')
										--select * from tblbanco
										--select * from clie_tblclientepotencial where  codigobanco='102' AND EJECUTIVO=''
										--select * from 	 where 5129
										--select * from tblfacturacioncliente where p_idafiliado='5133'
	Begin									--select * from sp_reportexdias('102','0','2018-07-10','2019-04-22')
										--select * from clie_tblclientepotencial
										--select * from tblgestioncliente where g_coddocumento='J-222222' 
			if (tiporeporte='1') then
		RETURN query

				select 	row_number () OVER (ORDER BY b.ibp),b.ibp, cp.razonsocial, cp.coddocumento,
				cp.nafiliacion, concat_ws(' ', u.nombres, u.apellidos) as ejecutivogestion, 
				case when cp.usuarioedita is null or cp.usuarioedita='' then 'SIN GESTIONAR' else cp.usuarioedita end, cp.fechaedita::date
					,cp.tipopos,case when mp.marcapos is null or mp.marcapos='' then 'SIN ASIGNAR' else mp.marcapos end
					,sum(cp.CANT_POS_CONFIRM) as cantidad,
					--case when cp.tipopos is null or cp.tipopos='' then 'Sin definir' else cp.tipopos end, 
					sum(case when cp.codestatus in (1) then cp.CANT_POS_CONFIRM else 0 end ) as ventasconfondos,
					gs.g_fechacontacto::date,
					sum(case when f.estatus in (1) then cp.CANT_POS_CONFIRM else 0 end) as facturado, 
					case when f.nfactura is null or f.nfactura='' then null else f.fecha_facturacion::date end ,f.nfactura
					, sum(pagos.transfmontodolar) as montousdtransf,sum(pagos.efectmontodolar) as montousdefec,
					sum(pagos.montobs::double precision) as montobss,venta.nombres||' '||venta.apellidos as vendedorfinal
					--((sum(case when cp.codestatus in (1) then cp.codestatus else 0 end)) - (sum(case when f.estatus in (1) then f.estatus else 0 end)) ) as sinfacturar
				from clie_tblclientepotencial cp
				left join tblbanco b on b.codigobanco::integer=cp.cbancoasig::integer
				left join tblmarcapos mp on mp.codtipo=cp.codmarcapos
				left join tblfacturacioncliente f on f.id_registro=cp.id_consecutivo
				left join tblusuario u on u.usuario=cp.ejecutivo
				left join (select * from tblgestioncliente where g_estatuspos='1') as gs on gs.g_idregistro=cp.id_consecutivo
				left join (select p.p_idafiliado,SUM(CASE WHEN p.p_formapago='2' THEN p.p_montousd::double precision ELSE 0 END) as transfmontodolar ,
						SUM(CASE WHEN p.p_formapago='3' THEN p.p_montousd::double precision ELSE 0 END) as efectmontodolar,
				          sum(case when p.p_fconversion='0' or p.p_fconversion='' then p.p_monto::double precision  else 0 end) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				on pagos.p_idafiliado=f.id_registro and f.estatus='1'
				left join tblusuario venta on concat_ws('',venta.tipodoc,'-',venta.coddocumento)=gs.g_codejecutivointeligensa::text
			where case when fechainicio!='' then cp.tipopos=tipoposdetalles and u.usuario=usuarioeje and cp.cbancoasig=banco and gs.g_fechacontacto::date BETWEEN fechainicio::date and fechafinal::date 
			else cp.tipopos=tipoposdetalles and u.usuario=usuarioeje and cp.cbancoasig=banco end
			
				
				
				group by    b.ibp, ejecutivogestion, cp.usuarioedita, cp.razonsocial, cp.nafiliacion, cp.coddocumento, f.nfactura,cp.tipopos, gs.g_fechacontacto::date,
				mp.marcapos,cp.fechaedita::date,f.fecha_facturacion::date,vendedorfinal
				--vendedor, 
				--cp.usuarioedita
				order by b.ibp;

			elsif (tiporeporte='2') then
				return  QUERY
				
					select 	row_number () OVER (ORDER BY b.ibp),b.ibp, cp.razonsocial, cp.coddocumento,
				cp.nafiliacion, concat_ws(' ', u.nombres, u.apellidos) as ejecutivogestion, 
				case when cp.usuarioedita is null or cp.usuarioedita='' then 'SIN GESTIONAR' else cp.usuarioedita end, cp.fechaedita::date
					,cp.tipopos,case when mp.marcapos is null or mp.marcapos='' then 'SIN ASIGNAR' else mp.marcapos end
					,sum(cp.CANT_POS_CONFIRM) as cantidad,
					--case when cp.tipopos is null or cp.tipopos='' then 'Sin definir' else cp.tipopos end, 
					sum(case when cp.codestatus in (1) then cp.CANT_POS_CONFIRM else 0 end ) as ventasconfondos,
					gs.g_fechacontacto::date,
					sum(case when f.estatus in (1) then cp.CANT_POS_CONFIRM else 0 end) as facturado, 
					case when f.nfactura is null or f.nfactura='' then null else f.fecha_facturacion::date end ,f.nfactura
					, sum(pagos.transfmontodolar) as montousdtransf,sum(pagos.efectmontodolar) as montousdefec,
					sum(pagos.montobs::double precision) as montobss,venta.nombres||' '||venta.apellidos as vendedorfinal
					--((sum(case when cp.codestatus in (1) then cp.codestatus else 0 end)) - (sum(case when f.estatus in (1) then f.estatus else 0 end)) ) as sinfacturar
				from clie_tblclientepotencial cp
				left join tblbanco b on b.codigobanco::integer=cp.cbancoasig::integer
				left join tblmarcapos mp on mp.codtipo=cp.codmarcapos
				left join (select * from tblgestioncliente where g_estatuspos='1') as gs on gs.g_idregistro=cp.id_consecutivo
				left join tblusuario u on u.usuario=cp.ejecutivo
				left join tblfacturacioncliente f on f.id_registro=cp.id_consecutivo
				left join (select p.p_idafiliado,SUM(CASE WHEN p.p_formapago='2' THEN p.p_montousd::double precision ELSE 0 END) as transfmontodolar ,
						SUM(CASE WHEN p.p_formapago='3' THEN p.p_montousd::double precision ELSE 0 END) as efectmontodolar,
				          sum(case when p.p_fconversion='0' or p.p_fconversion='' then p.p_monto::double precision  else 0 end) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				on pagos.p_idafiliado=f.id_registro and f.estatus is not null

				left join tblusuario venta on concat_ws('',venta.tipodoc,'-',venta.coddocumento)=gs.g_codejecutivointeligensa::text
				
			where case when fechainicio!='' then cp.tipopos=tipoposdetalles and CP.EJECUTIVO=usuarioeje and cp.cbancoasig=banco and f.fecha_facturacion::date BETWEEN fechainicio::date and fechafinal::date 
			else cp.tipopos=tipoposdetalles and CP.EJECUTIVO=usuarioeje and cp.cbancoasig=banco end
				
				
				group by    b.ibp, ejecutivogestion, cp.usuarioedita, cp.razonsocial, cp.nafiliacion, cp.coddocumento, f.nfactura,cp.tipopos, gs.g_fechacontacto::date,
				mp.marcapos,cp.fechaedita::date,f.fecha_facturacion::date, vendedorfinal
				--vendedor, 
				--cp.usuarioedita
				order by b.ibp;


		end if;
	
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_reportexdiasdetalles(character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;


/*
				select 	row_number () OVER (ORDER BY b.ibp),b.ibp, cp.razonsocial, cp.coddocumento,
				cp.nafiliacion, concat_ws(' ', u.nombres, u.apellidos) as ejecutivogestion, 
				case when cp.usuarioedita is null or cp.usuarioedita='' then 'SIN GESTIONAR' else cp.usuarioedita end, cp.fechaedita::date
					,cp.tipopos,case when mp.marcapos is null or mp.marcapos='' then 'SIN ASIGNAR' else mp.marcapos end
					,sum(cp.CANT_POS_CONFIRM) as cantidad,
					sum(case when cp.codestatus in (1) then cp.CANT_POS_CONFIRM else 0 end ) as ventasconfondos,
					gs.g_fechacontacto::date,
					sum(case when f.estatus in (1) then cp.CANT_POS_CONFIRM else 0 end) as facturado, 
					case when f.nfactura is null or f.nfactura='' then null else f.fecha_facturacion::date end ,f.nfactura
					, sum(pagos.transfmontodolar) as montousdtransf,sum(pagos.efectmontodolar) as montousdefec,
					sum(pagos.montobs::double precision) as montobss, venta.nombres||' '||venta.apellidos as vendedorfinal
				from clie_tblclientepotencial cp
				left join tblbanco b on b.codigobanco::integer=cp.cbancoasig::integer
				left join tblmarcapos mp on mp.codtipo=cp.codmarcapos
				left join tblfacturacioncliente f on f.id_registro=cp.id_consecutivo
				left join tblusuario u on u.usuario=cp.ejecutivo
				left join (select * from tblgestioncliente where g_estatuspos='1') as gs on gs.g_idregistro=cp.id_consecutivo
				left join (select p.p_idafiliado,SUM(CASE WHEN p.p_formapago='2' THEN p.p_montousd::double precision ELSE 0 END) as transfmontodolar ,
						SUM(CASE WHEN p.p_formapago='3' THEN p.p_montousd::double precision ELSE 0 END) as efectmontodolar,
				          sum(case when p.p_fconversion='0' or p.p_fconversion='' then p.p_monto::double precision  else 0 end) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				on pagos.p_idafiliado=f.id_registro and f.estatus='1'

				left join tblusuario venta on concat_ws('',venta.tipodoc,'-',venta.coddocumento)=gs.g_codejecutivointeligensa::text
		--	where 
			--case when fechainicio!='' then cp.tipopos=tipoposdetalles and u.usuario='AARAQUE' and cp.cbancoasig=banco and gs.g_fechacontacto::date BETWEEN fechainicio::date and fechafinal::date 
			--else
		---	 cp.tipopos='Dial UP' and u.usuario='JRICO' and cp.cbancoasig='156' --end
				
				group by    b.ibp, ejecutivogestion, cp.usuarioedita, cp.razonsocial, cp.nafiliacion, cp.coddocumento, f.nfactura,cp.tipopos, gs.g_fechacontacto::date,
				mp.marcapos,cp.fechaedita::date,f.fecha_facturacion::date, vendedorfinal
				order by b.ibp;

*/
/*
select count(*),g_idregistro from tblgestioncliente where g_estatuspos='1'
GROUP BY g_idregistro order by count(*) desc  5443

select * from tblgestioncliente where g_estatuspos='1' 

select * from tblgestioncliente where g_idregistro='19129'

select * from tblusuario
*/

--select * from sp_reportexdiasdetalles('156','JRICO','Dial UP','','','1')