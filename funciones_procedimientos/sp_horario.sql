﻿-- Function: sp_horario(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_horario(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_horario(id_cliente character varying, iprefijobanco character varying, ifechacarga character varying, ilunh1 character varying, ilunh2 character varying, imarh1 character varying, imarh2 character varying, imieh1 character varying, imieh2 character varying, ijueh1 character varying, ijueh2 character varying, ivieh1 character varying, ivieh2 character varying, isabh1 character varying, isabh2 character varying, idomh1 character varying, idomh2 character varying, ilunh1t character varying, ilunh2t character varying, imarh1t character varying, imarh2t character varying, imieh1t character varying, imieh2t character varying, ijueh1t character varying, ijueh2t character varying, ivieh1t character varying, ivieh2t character varying, isabh1t character varying, isabh2t character varying, idomh1t character varying, idomh2t character varying, ilunh1n character varying, ilunh2n character varying, imarh1n character varying, imarh2n character varying, imieh1n character varying, imieh2n character varying, ijueh1n character varying, ijueh2n character varying, ivieh1n character varying, ivieh2n character varying, isabh1n character varying, isabh2n character varying, idomh1n character varying, idomh2n character varying)
  RETURNS boolean AS
$BODY$
Begin



INSERT INTO tblhoras(
    id_cliente, prefijo_banco, fecha_carga, lunh1, lunh2, marh1, marh2, mieh1, mieh2, jueh1, jueh2, vieh1, 
    vieh2, sabh1, sabh2, domh1, domh2, lunh1t, lunh2t, marh1t, marh2t, mieh1t, mieh2t, jueh1t, jueh2t, vieh1t, 
    vieh2t, sabh1t, sabh2t, domh1t, domh2t, lunh1n, lunh2n, marh1n, marh2n, mieh1n, mieh2n, jueh1n, jueh2n, vieh1n, 
    vieh2n, sabh1n, sabh2n, domh1n, domh2n)
VALUES (id_cliente,iprefijobanco,ifechacarga, ilunh1, ilunh2, imarh1, imarh2, imieh1, imieh2,  ijueh1, ijueh2 , ivieh1, 
    ivieh2 , isabh1, isabh2, idomh1, idomh2, ilunh1t, ilunh2t, imarh1t, imarh2t, imieh1t, imieh2t,  ijueh1t, ijueh2t, ivieh1t, 
    ivieh2t, isabh1t, isabh2t, idomh1t, idomh2t, ilunh1n, ilunh2n, imarh1n, imarh2n, imieh1n, imieh2n,  ijueh1n, ijueh2n, ivieh1n, 
    ivieh2n, isabh1n, isabh2n, idomh1n, idomh2n);

	return true;

End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_horario(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
