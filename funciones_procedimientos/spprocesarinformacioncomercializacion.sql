﻿-- Function: spprocesarinformacioncomercializacion(numeric, character varying, character varying)

-- DROP FUNCTION spprocesarinformacioncomercializacion(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION spprocesarinformacioncomercializacion(operacion numeric, banco character varying, usuarioe character varying)
  RETURNS boolean AS
$BODY$

Begin
	 update clie_tblclientepotencial set 							  
	  idstatus='1', fechaenviocomercializacion=now()::date, usuarioenvio=usuarioe
	  where  codigobanco::integer=banco::integer
		and codestatus in ('1','2','4') and idstatus='0';
	return true;
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spprocesarinformacioncomercializacion(numeric, character varying, character varying)
  OWNER TO postgres;
