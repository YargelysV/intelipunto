﻿-- Table: clie_tblclientepotencial

-- DROP TABLE clie_tblclientepotencial;

CREATE TABLE clie_tblclientepotencial
(
  id_consecutivo integer NOT NULL DEFAULT nextval('tbl_clientespotenciales_id_seq'::regclass),
  fecharecepcion timestamp with time zone NOT NULL,
  codigobanco character varying(4) NOT NULL,
  tipopos character varying(50) NOT NULL,
  tipolinea character varying(50),
  tipomodelonegocio character varying(100),
  tipocliente character varying(100) NOT NULL,
  nafiliacion character varying(50),
  cantterminalesasig integer NOT NULL,
  razonsocial character varying(200) NOT NULL,
  coddocumento character varying(15) NOT NULL,
  acteconomica character varying(200) NOT NULL,
  rlegal character varying(200) NOT NULL,
  cirl character varying(15),
  rifrl character varying(15),
  correorl character varying(200) NOT NULL,
  telfrl character varying(20),
  telf1 character varying(20) NOT NULL,
  telf2 character varying(20),
  ncuentapos character varying(30),
  direccion_instalacion character varying(1000) NOT NULL,
  estado character varying(200) NOT NULL DEFAULT 0,
  municipio character varying(200) NOT NULL DEFAULT 0,
  parroquia character varying(200) NOT NULL DEFAULT 0,
  usuario character varying(50) NOT NULL,
  idate timestamp with time zone NOT NULL DEFAULT now(),
  namearchivo character varying(300) NOT NULL,
  sizearchivo character varying(50) NOT NULL,
  codestatus integer NOT NULL DEFAULT 3,
  razoncomercial character varying(200),
  nombre character varying(50),
  apellido character varying(200),
  instituto_organismo character varying(200),
  tlf_movil1 character varying(20),
  tlf_movil2 character varying(20),
  email_corporativo character varying(200),
  email_comercial character varying(200),
  email_personal character varying(200),
  ciclo_actividad character varying(200),
  flujo_caja character varying(200),
  rotacion_inventario character varying(200),
  dias_laborables character varying(15),
  horario_comercial character varying(20),
  codmarcapos integer,
  cant_pos_confirm integer,
  tipo_cuenta integer,
  idstatus integer NOT NULL DEFAULT 0,
  fechaenviocomercializacion date,
  fecharecepcomercializacion date,
  usuarioenvio character varying(50),
  usuariorecibe character varying(50),
  interes character varying(2),
  mantenimiento character varying(2),
  usuarioedita character varying(100),
  fechaedita timestamp with time zone,
  agencia character varying(100),
  activo integer NOT NULL DEFAULT 0,
  usuario_activo character varying(50),
  estatus_contacto integer NOT NULL DEFAULT 1,
  ejecutivo character varying,
  ejecutivocall character varying,
  cbancoasig character varying(4),
  CONSTRAINT pk_tbl_clientepotencial PRIMARY KEY (direccion_instalacion, namearchivo, codigobanco, coddocumento, tipopos),
  CONSTRAINT fkclietblclientpotencial_estatuspos FOREIGN KEY (codestatus)
      REFERENCES tblestatussolpos (codestatus) MATCH Unknown
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkclietblclientpotencial_usuario FOREIGN KEY (usuario)
      REFERENCES tblusuario (usuario) MATCH Unknown
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkclitblclientpotencial_tblstatuscomercializacion FOREIGN KEY (idstatus)
      REFERENCES tblestatuscomercializacion (idstatus) MATCH Unknown
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT id_consecutivo_clie_tblclientepotencial UNIQUE (id_consecutivo),
  CONSTRAINT id_unique_clietblclientepotencial UNIQUE (id_consecutivo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE clie_tblclientepotencial
  OWNER TO postgres;
COMMENT ON TABLE clie_tblclientepotencial
  IS 'Tabla de clientes potenciales enviados por el banco. ';

-- Trigger: cambiar_datos on clie_tblclientepotencial

-- DROP TRIGGER cambiar_datos ON clie_tblclientepotencial;

CREATE TRIGGER cambiar_datos
  AFTER INSERT OR UPDATE
  ON clie_tblclientepotencial
  FOR EACH ROW
  EXECUTE PROCEDURE insertar_datos();

