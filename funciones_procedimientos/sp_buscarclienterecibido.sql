﻿

CREATE OR REPLACE FUNCTION spbuscarclienterecibido(buscar character varying, inicio integer, nroreg integer, cliente integer)
  RETURNS TABLE(fecharecepcion date, codigobanco integer, ibp character varying, usuario character varying, fechacarga date, namearchivo character varying, sizearchivo character varying, cantidad bigint) AS
$BODY$
	
	Begin
		return 	QUERY 
			select p.fecharecepcion::date, p.codigobanco::integer, b.ibp, p.usuario, p.idate::date, p.namearchivo, p.sizearchivo, count(p.codigobanco) as cantidad 
			from clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			where  (p.fecharecepcion::character varying LIKE '%'||buscar||'%' or p.idate::character varying LIKE '%'||buscar||'%' or p.namearchivo LIKE '%'||buscar||'%' or p.sizearchivo LIKE '%'||buscar||'%')
			and p.codigobanco::integer=cliente::integer
			group by p.fecharecepcion::date, p.codigobanco::integer, b.ibp,  p.usuario, p.idate::date, p.namearchivo, p.sizearchivo
			ORDER BY p.idate::date
			LIMIT nroreg OFFSET inicio;
	--SELECT * FROM tblUsuario WHERE usuario LIKE '%'||buscar||'%' or nombres LIKE '%'||buscar||'%' or apellidos
	-- LIKE '%'||buscar||'%' or coddocumento LIKE '%'||buscar||'%' order by usuario LIMIT nroreg OFFSET inicio ;	
	End;
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarclienterecibido(character varying, integer, integer, integer)
  OWNER TO postgres;

 --select * from spbuscarclienterecibido('','0','100','102')