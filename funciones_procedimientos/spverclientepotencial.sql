﻿-- Function: spverclientepotencial(integer, timestamp with time zone, character varying)

-- DROP FUNCTION spverclientepotencial(integer, timestamp with time zone, character varying);

CREATE OR REPLACE FUNCTION spverclientepotencial(IN banco integer, IN fecha timestamp with time zone, IN namearchiv character varying)
  RETURNS TABLE(id_consecutivo integer, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying,
   cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, 
   coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, 
   rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying,
    ncuentapos character varying, direccioninstalacion character varying, est character varying, mun character varying,
     parr character varying, usuario character varying, idate text, namearchivo character varying, sizearchivo character varying, direccion text) AS
$BODY$
	
	Begin
	
	
				return query select tr.id_consecutivo, 
				to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.direccion_instalacion, e.estado,
				m.municipio, par.parroquia, tr.usuario,
				to_char(tr.idate::date,'DD/MM/YYYY'),
				tr.namearchivo, tr.sizearchivo,
				concat_ws(' ',  e.estado,m.municipio,par.parroquia ) as direccion
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				left join tblestados e 
				on e.id_estado=tr.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=tr.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=tr.parroquia::integer
				where tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha
				and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				         else tr.namearchivo=namearchiv END
				order by tr.namearchivo;
		--select * from clie_tblclientepotencial
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientepotencial(integer, timestamp with time zone, character varying)
  OWNER TO postgres;