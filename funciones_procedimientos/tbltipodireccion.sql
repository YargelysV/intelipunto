﻿-- Table: tbltipodireccion

-- DROP TABLE tbltipodireccion;

CREATE TABLE tbltipodireccion
(
  codtipo integer NOT NULL,
  desctipo character varying(50),
  CONSTRAINT tbltipodireccion_pkey PRIMARY KEY (codtipo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbltipodireccion
  OWNER TO postgres;
