﻿-- Function: sp_formapago()

-- DROP FUNCTION sp_formapago();

CREATE OR REPLACE FUNCTION sp_formapago()
  RETURNS SETOF tblformapago AS
$BODY$
	
	Begin
		return 	QUERY 
			select * from tblformapago;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_formapago()
  OWNER TO postgres;
