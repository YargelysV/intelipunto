﻿-- Function: sp_editarcaptacion(numeric, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_editarcaptacion(numeric, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_editarcaptacion(ioperacion numeric, iidregistro integer, inombre character varying, iapellido character varying, icoddocumento character varying, itlf1 character varying, irazonsocial character varying, ibancointeres character varying, icodigobanco character varying, icantposasignados integer, icorreorl character varying, imedio character varying, idireccion_instalacion character varying, itipocliente character varying)
  RETURNS boolean AS
$BODY$	
	Begin
CASE ioperacion 
	when 3 then
		update tbl_prospectos set   
		  nombre = case 
				when inombre is null or inombre = '' then nombre
				else inombre end, 
		  apellido  = case 
				when iapellido is null or iapellido = '' then apellido 
				else iapellido  end,
		 coddocumento= case 
				when icoddocumento is null or icoddocumento = '' then coddocumento
				else icoddocumento end,
		telf1= case 
				when itlf1 is null or itlf1= '' then telf1
				else itlf1 end,
		razonsocial   = case 
				when irazonsocial is null or irazonsocial = '' then razonsocial
				else irazonsocial end,
		bancointeres = case 
				when ibancointeres is null or ibancointeres = '' then bancointeres
				else ibancointeres end,
		cbancoasig = case 
				when icodigobanco is null or icodigobanco = '' then cbancoasig
				else icodigobanco end,
		codigobanco = case 
				when icodigobanco is null or icodigobanco = '' then codigobanco
				else icodigobanco end,

		cantterminalesasig = case 
				when icantposasignados is null or icantposasignados::integer = 0 then cantterminalesasig
				else icantposasignados::integer end,						
		correorl =  case 
				when icorreorl is null or icorreorl = '' then correorl
				else icorreorl end,
		mediocontacto =  case 
				when imedio is null or imedio = '' then mediocontacto
				else imedio end,
		direccion_instalacion =  case 
				when idireccion_instalacion is null or idireccion_instalacion = '' then direccion_instalacion
				else idireccion_instalacion end,
		tipocliente =  case 
				when itipocliente is null or itipocliente = '' then tipocliente
				else itipocliente end,																						
		idate=now()
		where id_consecutivo=iidregistro;
		return true;	     
end case;	
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_editarcaptacion(numeric, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
