﻿-- Function: sp_buscarclienteinstalar(character varying, character varying, character varying)

-- DROP FUNCTION sp_buscarclienteinstalar(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_buscarclienteinstalar(IN cliente character varying, IN fecha character varying, IN origen character varying)
  RETURNS TABLE(enlace text, correlativo bigint, fecharecepcion text, codigobanco integer, ibp character varying, usuario character varying, fechacarga text, namearchivo character varying, sizearchivo character varying, cantidad bigint, nafiliacion character varying) AS
$BODY$
	
	
	Begin
		return 	QUERY 
			select p.codigobanco::integer||'/'||p.fecharecepcion::date||'/'||p.namearchivo||'/'||origen  as enlace,
			row_number()  OVER(order by p.codigobanco::integer), to_char(p.fecharecepcion::date,'DD/MM/YYYY'), p.codigobanco::integer, b.ibp,
			p.usuario,to_char(p.idate::date,'DD/MM/YYYY'), p.namearchivo, p.sizearchivo, count(p.codigobanco), p.nafiliacion as cantidad
			from clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			inner join tblinventariopos i on i.nroafiliacion=p.nafiliacion
			WHERE 
			p.codestatus in ('1','2') and p.nafiliacion <>''
			AND
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			AND 
			    CASE WHEN fecha is NULL or fecha='' then p.fecharecepcion IS NOT null
				  else p.fecharecepcion::date=fecha::date END	
			group by (p.fecharecepcion::date), p.codigobanco::integer, b.ibp,  p.usuario, (p.idate::date), p.namearchivo, p.sizearchivo, p.nafiliacion
			ORDER BY p.idate::date;
	End;
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_buscarclienteinstalar(character varying, character varying, character varying)
  OWNER TO postgres;