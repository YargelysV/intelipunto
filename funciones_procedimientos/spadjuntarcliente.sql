﻿-- Function: spadjuntarcliente(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION spadjuntarcliente(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION spadjuntarcliente(tipoclient integer, customernam character varying, shortnam character varying, statementnam character varying, customerclas character varying, lookupbutto2 character varying, addresscod character varying, contactperso character varying, addres1 character varying, addres2 character varying, addres3 character varying, ciudad character varying, estado character varying, nzip character varying, countrycod character varying, pais character varying, telf1 character varying, telf2 character varying, telf3 character varying, nfax character varying, nupszone character varying, nshippingmethod character varying, ntaxscheduleid character varying, nshipcompletdocument character varying, nprimaryshiptoaddresscode character varying, nsalespersonid character varying, nlookupbutton6 character varying, nuserdefined1 character varying, nuserdefined2 character varying, ncomment1 character varying, ncomment2 character varying, ncustomerdiscount character varying, npaymenttermsid character varying, ndiscountgraceperiod character varying, nduedategraceperiod character varying, npricelevel character varying, naccountsbutton character varying, noptionsbutton character varying, ntaxregistrationnumber character varying, ncurrencyid character varying, nemailstatementstoaddress character varying, usuario character varying)
  RETURNS boolean AS
$BODY$
	
	Begin

		set client_encoding to 'latin1';
		
		INSERT INTO adm_archivoclientes (tipocliente, CustomerName, ShortName, StatementName, CustomerClass, LookupButton2,
		 AddressCode, ContactPerson, Address1, Address2, Address3, City, State, Zip, CountryCode, Country, Phone1, Phone2,
		 Phone3, Fax, UPSZone, ShippingMethod, TaxScheduleID, ShipCompleteDocument, PrimaryShiptoAddressCode, SalespersonID,
		 LookupButton6, UserDefined1, UserDefined2, Comment1, Comment2, CustomerDiscount, PaymentTermsID, DiscountGracePeriod,
		 DueDateGracePeriod, PriceLevel, AccountsButton, OptionsButton, TaxRegistrationNumber, CurrencyID, EmailStatementsToAddress,
		 login, idate)
			values(TipoClient, CustomerNam, ShortNam, StatementNam, CustomerClas, LookupButto2,
		 AddressCod, ContactPerso, Addres1, Addres2, Addres3, Ciudad,  Estado, nZip, CountryCod, Pais, Telf1, Telf2,
		 Telf3, nFax, nUPSZone, nShippingMethod,nTaxScheduleID, nShipCompletDocument, nPrimaryShiptoAddressCode, nSalespersonID,
		 nLookupButton6, nUserDefined1,	nUserDefined2, nComment1, nComment2, nCustomerDiscount, nPaymentTermsID, nDiscountGracePeriod, 
		 nDueDateGracePeriod, nPriceLevel, nAccountsButton, nOptionsButton, nTaxRegistrationNumber, nCurrencyID, nEmailStatementsToAddress, 
		 usuario, now());
		return true;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spadjuntarcliente(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
