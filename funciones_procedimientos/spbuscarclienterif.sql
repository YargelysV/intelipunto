﻿-- Function: spbuscarclienterif(character varying, character varying, bigint, character varying)

-- DROP FUNCTION spbuscarclienterif(character varying, character varying, bigint, character varying);

CREATE OR REPLACE FUNCTION spbuscarclienterif(IN irif character varying, IN tipousuario character varying, IN ejecutivoin bigint, IN login character varying)
  RETURNS TABLE(enlace text, correlativo bigint, coddocumento character varying, razon character varying, razoncomercial character varying, personacontacto text, tlf1 character varying, afiliacion character varying, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying, usuario character varying, fechacarga text, namearchivo character varying, sizearchivo character varying, cantidad integer, activo integer, usuarioactivo character varying, enlaceactivo text, direccion text, idestatus_contacto integer, desc_estatus_contacto character varying, usuarioedita character varying, ejecutivo character varying) AS
$BODY$
   
	Begin
		if (ejecutivoin>0) then	
	
			return 	QUERY 
			select concat_ws('/', p.id_consecutivo, p.coddocumento,p.codigobanco::integer,to_char(p.idate::date,'DD-MM-YYYY'),p.namearchivo,tipousuario, ejecutivoin, login) as enlace, 
			 row_number()  OVER(order by p.codigobanco::integer), p.coddocumento, p.razonsocial,p.razoncomercial,concat_ws(' ', p.nombre,p.apellido) as personacontacto,p.telf1, p.nafiliacion, to_char(p.fecharecepcion::date,'DD-MM-YYYY'),
			 p.codigobanco::integer, b.ibp,p.tipopos, p.usuario,to_char(p.idate::date,'DD-MM-YYYY'), p.namearchivo, p.sizearchivo, case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end, 
			 p.activo, p.usuario_activo, p.id_consecutivo||'/'||p.activo  as enlaceactivo, concat_ws(' ', p.direccion_instalacion,e.estado,m.municipio,par.parroquia) as direccion, p.estatus_contacto, ec.descripcion_estatus_contacto 
                         ,p.usuarioedita, p.ejecutivo
			 from clie_tblclientepotencial p
			        inner join tblbanco b 
			        on b.codigobanco::integer=p.codigobanco::integer
			        left join tblusuario u 
				on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
			        inner join tbl_estatus_contacto as ec
			        on ec.id_estatus_contacto=p.estatus_contacto
				left join tblestados e 
				on e.id_estado=p.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=p.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=p.parroquia::integer
			
			WHERE  (u.usuario=login OR p.ejecutivo=login) and (p.coddocumento LIKE '%'||irif||'%' or p.nafiliacion LIKE '%'||irif||'%' or p.razonsocial LIKE '%'||irif||'%')
			group by p.id_consecutivo, p.coddocumento,  p.razonsocial,p.razoncomercial,p.nombre,p.apellido, p.nafiliacion,to_char(p.fecharecepcion::date,'DD-MM-YYYY'), p.codigobanco::integer, b.ibp, p.tipopos, p.usuario,to_char(p.idate::date,'DD-MM-YYYY'), p.namearchivo,
			 p.sizearchivo,p.cantterminalesasig , p.cant_pos_confirm,
			 p.direccion_instalacion, p.activo, p.usuario_activo,e.estado,m.municipio,par.parroquia,p.estatus_contacto,ec.descripcion_estatus_contacto,p.usuarioedita,p.ejecutivo
			ORDER BY  p.codigobanco::integer, p.namearchivo;

			else if  (tipousuario='E' or tipousuario='O') then

			return 	QUERY 
			select concat_ws('/', p.id_consecutivo, p.coddocumento,p.codigobanco::integer,to_char(p.idate::date,'DD-MM-YYYY'),p.namearchivo,tipousuario, ejecutivoin, login) as enlace, 
			 row_number()  OVER(order by p.codigobanco::integer), p.coddocumento, p.razonsocial, p.razoncomercial,concat_ws(' ', p.nombre,p.apellido) as personacontacto,p.telf1 ,p.nafiliacion,to_char(p.fecharecepcion::date,'DD-MM-YYYY'),
			 p.codigobanco::integer, b.ibp,p.tipopos, p.usuario, to_char(p.idate::date,'DD-MM-YYYY'), p.namearchivo, p.sizearchivo, case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end, 
			 p.activo, p.usuario_activo, p.id_consecutivo||'/'||p.activo  as enlaceactivo, concat_ws(' ', p.direccion_instalacion,e.estado,m.municipio,par.parroquia ) as direccion, p.estatus_contacto, ec.descripcion_estatus_contacto 
                         ,p.usuarioedita, p.ejecutivo
			 from clie_tblclientepotencial p
			        inner join tblbanco b 
			        on b.codigobanco::integer=p.codigobanco::integer
			        inner join tbl_estatus_contacto as ec
			        on ec.id_estatus_contacto=p.estatus_contacto
				left join tblestados e 
				on e.id_estado=p.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=p.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=p.parroquia::integer
			
			WHERE  p.ejecutivo=login and (p.coddocumento LIKE '%'||irif||'%' or p.nafiliacion LIKE '%'||irif||'%' or p.razonsocial LIKE '%'||irif||'%')
			group by p.id_consecutivo, p.coddocumento,  p.razonsocial,p.razoncomercial,p.nombre,p.apellido,p.telf1, p.nafiliacion, to_char(p.fecharecepcion::date,'DD-MM-YYYY'), p.codigobanco::integer, b.ibp, p.tipopos, p.usuario, to_char(p.idate::date,'DD-MM-YYYY'),
			 p.namearchivo, p.sizearchivo,p.cantterminalesasig , p.cant_pos_confirm,
			 p.direccion_instalacion, p.activo, p.usuario_activo,e.estado,m.municipio,par.parroquia,p.estatus_contacto,ec.descripcion_estatus_contacto,p.usuarioedita,p.ejecutivo
			ORDER BY  p.codigobanco::integer, p.namearchivo;

			else

			return 	QUERY 
			select concat_ws('/', p.id_consecutivo, p.coddocumento,p.codigobanco::integer,to_char(p.idate::date,'DD-MM-YYYY'),p.namearchivo,tipousuario, ejecutivoin, login) as enlace, 
			 row_number()  OVER(order by p.codigobanco::integer), p.coddocumento, p.razonsocial,p.razoncomercial,concat_ws(' ', p.nombre,p.apellido) as personacontacto,p.telf1, p.nafiliacion,to_char(p.fecharecepcion::date,'DD-MM-YYYY'),
			 p.codigobanco::integer, b.ibp,p.tipopos, p.usuario, to_char(p.idate::date,'DD-MM-YYYY') , p.namearchivo, p.sizearchivo, case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end, 
			 p.activo, p.usuario_activo, p.id_consecutivo||'/'||p.activo  as enlaceactivo, concat_ws(' ', p.direccion_instalacion,e.estado,m.municipio,par.parroquia ) as direccion, p.estatus_contacto, ec.descripcion_estatus_contacto 
                         ,p.usuarioedita, p.ejecutivo
			 from clie_tblclientepotencial p
			        inner join tblbanco b 
			        on b.codigobanco::integer=p.codigobanco::integer
			        inner join tbl_estatus_contacto as ec
			        on ec.id_estatus_contacto=p.estatus_contacto
				left join tblestados e 
				on e.id_estado=p.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=p.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=p.parroquia::integer
			
			WHERE p.coddocumento LIKE '%'||irif||'%' or p.nafiliacion LIKE '%'||irif||'%' or p.razonsocial LIKE '%'||irif||'%'
			group by p.id_consecutivo, p.coddocumento,  p.razonsocial,p.razoncomercial,p.nombre,p.apellido,p.telf1, p.nafiliacion,to_char(p.fecharecepcion::date,'DD-MM-YYYY'), p.codigobanco::integer, b.ibp, p.tipopos, p.usuario,to_char(p.idate::date,'DD-MM-YYYY') , 
			p.namearchivo, p.sizearchivo,p.cantterminalesasig , p.cant_pos_confirm,
			 p.direccion_instalacion, p.activo, p.usuario_activo,e.estado,m.municipio,par.parroquia,p.estatus_contacto,ec.descripcion_estatus_contacto,p.usuarioedita,p.ejecutivo
			ORDER BY  p.codigobanco::integer, p.namearchivo;


			end if;
		end if;		
	End;
	
	
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarclienterif(character varying, character varying, bigint, character varying)
  OWNER TO postgres;
