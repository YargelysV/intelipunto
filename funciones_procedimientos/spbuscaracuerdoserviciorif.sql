﻿-- Function: spbuscaracuerdoserviciorif(character varying, integer, date, character varying, character varying)

-- DROP FUNCTION spbuscaracuerdoserviciorif(character varying, integer, date, character varying, character varying);

CREATE OR REPLACE FUNCTION spbuscaracuerdoserviciorif(IN documento character varying, IN banco integer, IN fecha date, IN narchivo character varying, IN afiliado character varying)
  RETURNS TABLE(correlativo bigint, fecharecepcion date, fechainstalacion character varying, serialpos character varying, codigobanco integer, promedio character varying, ibp character varying, tipopos character varying, cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, rif character varying, direccion text, tabladireccion character varying, registro integer) AS
$BODY$
	declare asignados integer;
	declare postotal integer;
	declare faltantes integer;

	Begin

			asignados:=(select count(*)::integer from tblgestionserviciotecnico where nroafiliado=afiliado and nombrearchivo=narchivo and estatus='4');
			postotal:=(select case when cant_pos_confirm is null or cant_pos_confirm='0' then cantterminalesasig::integer else cant_pos_confirm::integer end 
				   from clie_tblclientepotencial where coddocumento=documento and namearchivo=narchivo);
			faltantes:=(postotal-asignados);

			if (faltantes=postotal) then
				faltantes=faltantes-1;
			end if;
				
			return query 
			SELECT row_number()  OVER(order by result.promediodias),* from (	

					select 
						tr.fecharecepcion::date,g.fechainstalacion::CHARACTER VARYING, g.serialpos, tr.codigobanco::integer,
						case when (extract(days from ((g.fechainstalacion) - ( tr.fecharecepcion)))) is null then 'En proceso' else 
							   (extract(days from ((g.fechainstalacion) - ( tr.fecharecepcion))))::character varying end as promediodias,
						b.ibp, tr.tipopos,  
						case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
						tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, 
						d.d_calle_av||' '||d.d_localidad||' '||d.d_sector||' '||d.d_urbanizacion||' '||d.d_nlocal||' '||d.d_estado||' '||d.d_codepostal||' '||d.d_ptoref as direccion, 
						'NI'::character varying instalacion
						,1 AS REGISTRO
						from clie_tblclientepotencial tr
						inner join tblbanco b on 
						b.codigobanco::integer=tr.codigobanco::integer
						inner join tbldirecciones d
						on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='4'
						left join tblgestionserviciotecnico g
						on   g.nroafiliado=tr.nafiliacion and g.nombrearchivo=tr.namearchivo and g.estatus='4'
						where  tr.codigobanco::integer=banco and tr.fecharecepcion::date=fecha  and tr.coddocumento=documento and tr.namearchivo=narchivo

					UNION

					select 
						tr.fecharecepcion::date,g.fechainstalacion::CHARACTER VARYING, case when g.serialpos is null then 'Por asignar' else g.serialpos end , tr.codigobanco::integer,
						case when (extract(days from ((g.fechainstalacion) - ( tr.fecharecepcion)))) is null then 'En proceso' else 
							   (extract(days from ((g.fechainstalacion) - ( tr.fecharecepcion))))::character varying end as promediodias,
						b.ibp, tr.tipopos, 
						case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
						tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, 
						tr.calleav||' '||tr.localidad||' '||tr.sector||' '||tr.urbanizacion||' '||tr.local||' '||tr.estado||' '||tr.codigopostal||' '||tr.puntoref as direccion, 'BI'::character varying instalacion
						,1 AS REGISTRO
						from clie_tblclientepotencial tr
						inner join tblbanco b on 
						b.codigobanco::integer=tr.codigobanco::integer
						left join tblgestionserviciotecnico g
						on tr.nafiliacion=g.nroafiliado and tr.namearchivo=g.nombrearchivo and g.estatus='4'
						where   tr.codigobanco::integer=banco and tr.fecharecepcion::date=fecha  and tr.coddocumento=documento and tr.namearchivo=narchivo
						and tr.coddocumento||tr.fecharecepcion||tr.codigobanco||tr.namearchivo not in (select 
											tr.coddocumento||tr.fecharecepcion||tr.codigobanco||tr.namearchivo
											from clie_tblclientepotencial tr
											inner join tblbanco b on 
											b.codigobanco::integer=tr.codigobanco::integer
											inner join tbldirecciones d
											on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='4'
											where    tr.codigobanco::integer=banco and tr.fecharecepcion::date=fecha  and tr.coddocumento=documento and tr.namearchivo=namearchivo)
					UNION

					select 
						tr.fecharecepcion::date,''::CHARACTER VARYING,'Por Asignar', tr.codigobanco::integer,'En proceso'::character varying as promediodias,
						b.ibp, tr.tipopos,  
						case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
						tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, '' as direccion, 
						'FI'::character varying instalacion, generate_series(1,faltantes)
						from clie_tblclientepotencial tr
						inner join tblbanco b on 
						b.codigobanco::integer=tr.codigobanco::integer
						where  tr.codigobanco::integer=banco and tr.fecharecepcion::date=fecha  and tr.coddocumento=documento and tr.namearchivo=narchivo
			)result  order by row_number()  OVER(order by result.promediodias);         
	
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscaracuerdoserviciorif(character varying, integer, date, character varying, character varying)
  OWNER TO postgres;

--select * from spbuscaracuerdoserviciorif('17705021','102','2017-10-20','BANCOprueba_23102017_4.csv','16546544')
/*

select 
tr.fecharecepcion::date,g.fechainstalacion::CHARACTER VARYING, g.serialpos, tr.codigobanco::integer,
case when (extract(days from ((g.fechainstalacion) - ( tr.fecharecepcion)))) is null then 'En proceso' else 
(extract(days from ((g.fechainstalacion) - ( tr.fecharecepcion))))::character varying end as promediodias,
b.ibp, tr.tipopos,  
case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, 
d.d_calle_av||' '||d.d_localidad||' '||d.d_sector||' '||d.d_urbanizacion||' '||d.d_nlocal||' '||d.d_estado||' '||d.d_codepostal||' '||d.d_ptoref as direccion, 
'NI'::character varying instalacion
,1 AS REGISTRO
from clie_tblclientepotencial tr
inner join tblbanco b on 
b.codigobanco::integer=tr.codigobanco::integer
inner join tbldirecciones d
on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='4'
left join tblgestionserviciotecnico g
on   g.nroafiliado=tr.nafiliacion and g.nombrearchivo=tr.namearchivo and g.estatus='4'
where  tr.codigobanco::integer='102' and tr.fecharecepcion::date='2017-10-20'  and tr.coddocumento='17705021' and tr.namearchivo='BANCOprueba_23102017_4.csv'

select * from tblgestionserviciotecnico*/