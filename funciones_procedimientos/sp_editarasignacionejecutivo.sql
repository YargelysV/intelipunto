﻿-- Function: sp_editarasignacionejecutivo(numeric, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_editarasignacionejecutivo(numeric, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_editarasignacionejecutivo(ioperacion numeric, iejecutivo character varying, iidregistro integer, inombre character varying, iapellido character varying, icoddocumento character varying, itlf1 character varying, irazonsocial character varying, ibancointeres character varying, icodigobanco character varying, icantposasignados integer, icorreorl character varying, imedio character varying, idireccion_instalacion character varying, itipocliente character varying)
  RETURNS boolean AS
$BODY$

	declare existepros bigint;
	declare existecp bigint;
 
	Begin

CASE ioperacion 
	when 3 then

		existepros:=(SELECT count(*) FROM clie_tblclientepotencial where id_tblprospectos=iidregistro and codigobanco=icodigobanco); --verificar si es registro de prospecto

		existecp:=(SELECT count(*) FROM clie_tblclientepotencial where id_consecutivo=iidregistro and codigobanco=icodigobanco); -- verifica si existe en cliente potencial como registro 
	IF((existepros=0) and (existecp=0))THEN

		  update tbl_prospectos set 
		  ejecutivo= case
		  when iejecutivo is null or iejecutivo='' then ejecutivo
		  else iejecutivo end,
		  cbancoasig=icodigobanco
	          where id_consecutivo=iidregistro;	
	insert into clie_tblclientepotencial ( 
	  fecharecepcion ,
	  codigobanco,
	  tipopos ,
	  tipocliente ,
	  cantterminalesasig ,
	  razonsocial ,
	  coddocumento ,
	  rlegal ,
	   direccion_instalacion,
	  correorl ,
	  telf1 ,
	  usuario ,
	  nombre,
	  apellido,
	  namearchivo,
	   ejecutivo,
	   cbancoasig,
	   id_tblprospectos )
	select	 
	  tp.fecharecepcion ,
	  tp.codigobanco,
	  tp.tipopos ,
	  tp.tipocliente ,
	  tp.cantterminalesasig ,
	  tp.razonsocial ,
	  tp.coddocumento ,
	  tp.nombre||' '||apellido ,
	  tp.direccion_instalacion,
	  tp.correorl ,
	  tp.telf1 ,
	  tp.usuario ,
	  tp.nombre,
	  tp.apellido,
	  'CAPTACION'||'_'|| tp.id_consecutivo ,
	  tp.ejecutivo,
	  tp.cbancoasig,
	  tp.id_consecutivo
	  FROM tbl_prospectos as tp WHERE tp.id_consecutivo=iidregistro;
	return true;
	END IF; 
	
	IF((existepros<>0) and (existecp=0))THEN
		--existe en cliente potencial pero viene de prospecto actualizar ambas tablas
		update tbl_prospectos set 
		ejecutivo= case
		when iejecutivo is null or iejecutivo='' then ejecutivo
		else iejecutivo end
		where id_consecutivo=iidregistro;

		UPDATE  clie_tblclientepotencial SET ejecutivo=case
		when iejecutivo is null or iejecutivo='' then ejecutivo
		else iejecutivo end  
		where id_tblprospectos=iidregistro ;
	return true;
	END IF;	

	IF((existepros=0) and (existecp<>0))THEN
		--existe en cliente potencial pero no viene de prospecto actualizar solo cp
		UPDATE  clie_tblclientepotencial SET ejecutivo=case
		when iejecutivo is null or iejecutivo='' then ejecutivo
		else iejecutivo end  
		where id_consecutivo=iidregistro ;
		
	END IF;	
	return true;
	     
end case;	
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_editarasignacionejecutivo(numeric, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
