﻿-- Function: spserialpos(character varying)

-- DROP FUNCTION spserialpos(character varying);

CREATE OR REPLACE FUNCTION spserialpos(nafiliado character varying)
  RETURNS SETOF character varying AS
$BODY$
	Begin
		return query
		select serial as serialpo from  tblserialpos where estatus='0'
		union
		select serialpos from tblinventariopos where nroafiliacion=nafiliado;
	End;
  --select seriales as serialpos select * from spserialpos('1212')
  --select * from tblserialpos


$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spserialpos(character varying)
  OWNER TO postgres;
