﻿-- Function: spverbancosxejecutivo(character varying)

-- DROP FUNCTION spverbancosxejecutivo(character varying,character varying,character varying);

CREATE OR REPLACE FUNCTION spverbancosxejecutivo(IN icoddocumento character varying, IN iarea character varying, IN icodtipousuario character varying)
  RETURNS TABLE(codigobanco integer, ibp character varying) AS
$BODY$
	
	Begin
	 if  (iarea='1' or iarea='11') and (icodtipousuario='A') then 
			return query 
			select b.codigobanco::INTEGER, b.ibp from tblbanco b
			group by b.codigobanco::INTEGER, b.ibp;
	 else

			return query 
			select b.codigobanco::INTEGER, b.ibp from tblbanco b
			where coddocumento=icoddocumento
			group by b.codigobanco::INTEGER, b.ibp;
		
	 end if;
	
	End;

--  select * from tblbanco

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverbancosxejecutivo(character varying,character varying,character varying)
  OWNER TO postgres;

--select * from spverbancosxejecutivo('17705021','1','A')