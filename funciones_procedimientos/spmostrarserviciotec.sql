﻿-- Function: spmostrarserviciotec()

-- DROP FUNCTION spmostrarserviciotec();

CREATE OR REPLACE FUNCTION spmostrarserviciotec()
  RETURNS SETOF clie_tblclientepotencial AS
$BODY$

	Begin	
				return query 
								
				
select * from clie_tblclientepotencial;
				
		
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrarserviciotec()
  OWNER TO postgres;
