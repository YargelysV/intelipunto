﻿-- Function: spactualizarmotivo(character varying)

-- DROP FUNCTION spactualizarmotivo(character varying);

CREATE OR REPLACE FUNCTION sp_updatehorario(iid_cliente character varying, iprefijobanco character varying, ifechacarga character varying, ilunh1 character varying, ilunh2 character varying, imarh1 character varying, imarh2 character varying, imieh1 character varying, imieh2 character varying, ijueh1 character varying, ijueh2 character varying, ivieh1 character varying, ivieh2 character varying, isabh1 character varying, isabh2 character varying, idomh1 character varying, idomh2 character varying, ilunh1t character varying, ilunh2t character varying, imarh1t character varying, imarh2t character varying, imieh1t character varying, imieh2t character varying, ijueh1t character varying, ijueh2t character varying, ivieh1t character varying, ivieh2t character varying, isabh1t character varying, isabh2t character varying, idomh1t character varying, idomh2t character varying, ilunh1n character varying, ilunh2n character varying, imarh1n character varying, imarh2n character varying, imieh1n character varying, imieh2n character varying, ijueh1n character varying, ijueh2n character varying, ivieh1n character varying, ivieh2n character varying, isabh1n character varying, isabh2n character varying, idomh1n character varying, idomh2n character varying)
  
  RETURNS boolean AS
$BODY$

	Begin

 UPDATE tblhoras
   SET lunh1=ilunh1, lunh2=ilunh2, 
       marh1=imarh1, marh2=imarh2, mieh1=imieh1, mieh2=imieh2, jueh1=ijueh1, jueh2=ijueh2, vieh1=ivieh1, 
       vieh2=ivieh2, sabh1=isabh1, sabh2=isabh2, domh1=idomh1, domh2=idomh2, lunh1t=ilunh1t, lunh2t=ilunh2t, 
       marh1t=imarh1t, marh2t=imarh2t, mieh1t=imieh1t, mieh2t=imieh2t, jueh1t=ijueh1t, jueh2t=ijueh2t, vieh1t=ivieh1t, 
       vieh2t=ivieh2t, sabh1t=isabh1t, sabh2t=isabh2t, domh1t=idomh1t, domh2t=idomh2t, lunh1n=ilunh1n, lunh2n=ilunh2n, 
       marh1n=imarh1n, marh2n=imarh2n, mieh1n=imieh1n, mieh2n=imieh2n, jueh1n=ijueh1n, jueh2n=ijueh2n, vieh1n=ivieh1n, 
       vieh2n=ivieh2n, sabh1n=isabh1n, sabh2n=isabh2n, domh1n=idomh1n, domh2n=idomh2n
 WHERE id_cliente=iid_cliente and id_cliente=iid_cliente and fecha_carga=ifechacarga;
 
 return true;
					
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;






