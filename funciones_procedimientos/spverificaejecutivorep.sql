﻿-- Function: spverificaejecutivorep(integer, character varying)

-- DROP FUNCTION spverificaejecutivorep(integer, character varying);

CREATE OR REPLACE FUNCTION spverificaejecutivorep(codejecutivo integer, tipodoc character varying, documento character varying)
  RETURNS SETOF adm_ejecutivo AS
$BODY$
	
	
Begin
	return 	QUERY SELECT * FROM adm_ejecutivo where codvendedor=codejecutivo or coddocumento=tipodoc||'-'||documento;
	
	

End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverificaejecutivorep(integer, character varying, character varying)
  OWNER TO postgres;
