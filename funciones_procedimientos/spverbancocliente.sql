﻿-- Function: spverbancocliente()

-- DROP FUNCTION spverbancocliente();

CREATE OR REPLACE FUNCTION spverbancocliente()
  RETURNS TABLE(banco integer, nbanco character varying) AS
$BODY$
	Begin
		return query 
			select  p.codigobanco::integer, b.ibp  
			from clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			left join  tblfacturacioncliente t
			on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo
			where codestatus in('1','2')  and  t.nfactura is null
			group by  p.codigobanco::integer, b.ibp
			order by p.codigobanco::integer;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverbancocliente()
  OWNER TO postgres;
