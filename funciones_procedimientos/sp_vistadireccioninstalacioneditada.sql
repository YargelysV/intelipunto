﻿-- Function: sp_vistadireccioninstalacioneditada(character varying, character varying, character varying)

-- DROP FUNCTION sp_vistadireccioninstalacioneditada(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_vistadireccioninstalacioneditada(IN icoddocumento character varying, IN iprefijo character varying, IN inamearchivo character varying)
  RETURNS TABLE(d_coddocumento character varying, d_codetipo integer, d_nafiliacion character varying, 
  d_calle_av character varying, d_localidad character varying, d_sector character varying, d_urbanizacion character varying, 
  d_nlocal character varying, d_estado character varying, d_codepostal character varying, d_ptoref character varying, d_codigobanco character varying,
   d_fechacarga character varying, d_namearchivo character varying,d_usuariocarga character varying,d_fechaactualizacion timestamp with time zone, id_estado integer, estado character varying) AS
$BODY$
	
	Begin
		return 	QUERY 
			select * from tbldirecciones as td
		       inner join tblestados te
		on te.id_estado::integer=td.d_estado::integer
		where td.d_codetipo = 4 and td.d_coddocumento = icoddocumento and td.d_codigobanco=iprefijo and td.d_namearchivo=inamearchivo ;
			End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_vistadireccioninstalacioneditada(character varying, character varying, character varying)
  OWNER TO postgres;
