﻿-- Function: spmostrarmotivos(character varying)

-- DROP FUNCTION spmostrarmotivos(character varying);

CREATE OR REPLACE FUNCTION spmostrarmotivos(IN login character varying)
  RETURNS TABLE(idinactivo1 integer, idusuario1 character varying, motivo1 character varying, idate1 text, usuarioinactiva1 character varying, tiempoinactivo1 double precision) AS
$BODY$

	
	Begin
		return query 
		select  idinactivo,idusuario,motivo,
		(TO_CHAR(idate,'DD')||'-'||TO_CHAR(idate,'MM')||'-'||(extract( year from idate))::character varying)as fecha, 
		usuarioinactiva,tiempoinactivo from tblusuario_motivo where idusuario=login and tiempoinactivo is not null order by idate asc;
				
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrarmotivos(character varying)
  OWNER TO postgres;
