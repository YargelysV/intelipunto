﻿-- Function: sp_verpagos(integer)

-- DROP FUNCTION sp_verpagos(integer);

CREATE OR REPLACE FUNCTION sp_verpagos(IN id_afiliado integer)
  RETURNS TABLE(id integer, idafiliado integer, fechapago text, fechapagomostrar text, horapago character varying, bancoorigen character varying, bancodestino character varying, formapago integer, tipomoneda integer, factorconversion character varying, montousd double precision, monto double precision, referencia character varying, nombredepositante character varying, imagenpago text, observacioncomer text, observacionadm text, estatus integer, usuariocarga character varying, fechacarga text, usuarioconfirma character varying, fechaconfirmacion date, enlace text) AS
$BODY$

	Begin	
		return query select p_id ,p_idafiliado ,to_char(p_fechapago, 'YYYY/MM/DD'), to_char(p_fechapago, 'DD/MM/YYYY') AS FECHAMOSTRAR, p_horapago  ,p_bancoorigen  ,p_bancodestino ,p_formapago , p_tipomoneda ,p_fconversion, p_montousd, p_monto , p_referencia ,p_nombredepositante 
		,p_imagenpago ,p_observacion_comer ,p_observacion_adm,p_estatus ,p_usuariocarga  ,to_char(p_fechacarga, 'YYYY/MM/DD') ,p_usuarioconfirma  ,p_fechaconfirmacion,  concat_ws('/',p_referencia,p_idafiliado,p_estatus) as enlace  
                from tblpagos where p_idafiliado=id_afiliado
		ORDER BY p_id asc;
		
		-- select *  from sp_verpagos('16882')

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_verpagos(integer)
  OWNER TO postgres;

--select * from sp_verpagos('18588')