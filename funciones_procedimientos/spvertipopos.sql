﻿-- Function: spvertipopos()

-- DROP FUNCTION spvertipopos();

CREATE OR REPLACE FUNCTION spvertipopos()
  RETURNS SETOF tbltipopos AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tbltipopos;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spvertipopos()
  OWNER TO postgres;

  --select * from spvertipopos()