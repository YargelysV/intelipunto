﻿-- Function: sp_reportedetalladopos(date)

-- DROP FUNCTION sp_reportedetalladopos(date);

CREATE OR REPLACE FUNCTION sp_reportedetalladopos(IN fechac date)
  RETURNS TABLE(enlace text, correlativo bigint, total bigint, asignados bigint, porasignar bigint, marca character varying, tipo character varying, proveedor character varying) AS
$BODY$

--  select * from sp_reportedetalladopos('2018-03-16')
	Begin
		return query SELECT concat_ws('/', t.fechacompra::date, t.marcapos) as enlace, row_number()  OVER(order by t.marcapos) as correlativo,
			COUNT(*) AS TOTAL, A.ASIGNADOS, P.PORASIGNAR, t.marcapos, t.tipopos, t.proveedor FROM tblserialpos T
			full outer join (SELECT COUNT(*) AS ASIGNADOS, marcapos FROM tblserialpos WHERE  ESTATUS='1' and FECHACOMPRA::DATE=fechac::date    GROUP BY marcapos) A 
			ON A.marcapos=T.marcapos 
                        full outer join (SELECT COUNT(*) AS PORASIGNAR, marcapos FROM tblserialpos WHERE  ESTATUS='0' and FECHACOMPRA::DATE=fechac::date  GROUP BY marcapos) P 
			ON p.marcapos=T.marcapos 
			WHERE T.FECHACOMPRA::DATE=fechac::date
			GROUP BY T.FECHACOMPRA, A.ASIGNADOS, P.PORASIGNAR, T.MARCAPOS, t.tipopos, t.proveedor
			order by t.marcapos;

			--  select count (*) from tblserialpos where   marcapos='IWL 220' and fechacompra='2018-03-16' and marcapos='IWL 220'

	End;

--select * from tblserialpos where marcapos='ICT 220 PEM' AND FECHACOMPRA='2018-03-16' AND ESTATUS='1'
--  select * from tblinventariopos
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_reportedetalladopos(date)
  OWNER TO postgres;
