﻿-- Function: spmostrardetalleafiliado(character varying, character varying)

-- DROP FUNCTION spmostrardetalleafiliado(character varying, character varying);

CREATE OR REPLACE FUNCTION spmostrardetalleafiliado(IN inafiliado character varying, IN inamearchivo character varying)
  RETURNS TABLE(fecharecepcion date, codigobanco integer, ibp character varying, tipopos character varying, cant_pos_confirm integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, namearchivo character varying, nombre character varying, apellido character varying, institutoorganismo character varying, direccion text, marcapos character varying) AS
$BODY$

	Begin	
			return query 
						
			select 
				tr.fecharecepcion::date,tr.codigobanco::integer, b.ibp, tr.tipopos, 
				case when tr.cant_pos_confirm is null or tr.cant_pos_confirm='0' then tr.cantterminalesasig else tr.cant_pos_confirm end,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, 
				tr.namearchivo, tr.nombre, tr.apellido, tr.instituto_organismo,
				concat_ws(' ', e.estado,m.municipio,par.parroquia,d.d_calle_av,d.d_urbanizacion,d.d_nlocal,d.d_codepostal,d.d_ptoref) as direccion,
				mar.marcapos
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				full outer join tbldirecciones d
				on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='4' 
				full outer join tblmarcapos mar
				on mar.codtipo=tr.codmarcapos
				left join tblestados e 
				on e.id_estado=d.d_estado
				left join tblmunicipios m 
				on m.id_municipio=d.d_municipio
				left join tblparroquias par 
				on par.id_parroquia=d.d_parroquia
				where  tr.nafiliacion=inafiliado and tr.namearchivo=inamearchivo;  
			
	End;
--select * from tbldirecciones
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrardetalleafiliado(character varying, character varying)
  OWNER TO postgres;
