﻿-- Function: sp_eventscreate(text, character varying, character varying, character varying)

-- DROP FUNCTION sp_eventscreate(text, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_eventscreate(itexto text, iinicio character varying, ifin character varying, iidcliente character varying)
  RETURNS boolean AS
$BODY$
	Begin
			
		INSERT INTO tblevents (descripcion,inicio,fin,id_cliente)
		values(itexto,iinicio::timestamp without time zone, ifin::timestamp without time zone,iidcliente);
		return true;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_eventscreate(text, character varying, character varying, character varying)
  OWNER TO postgres;
