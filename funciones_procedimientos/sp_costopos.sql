﻿CREATE FUNCTION sp_costopos(IN imarcapos integer)
  RETURNS SETOF  tblventapos AS
$BODY$
	Begin
		return 	QUERY SELECT * FROM tblventapos where marcapos=imarcapos;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
