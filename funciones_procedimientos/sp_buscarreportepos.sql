﻿-- Function: sp_buscarreportepos(character varying, character varying)

-- DROP FUNCTION sp_buscarreportepos(character varying, character varying);

CREATE OR REPLACE FUNCTION sp_buscarreportepos(IN cliente character varying, IN fecha character varying, IN origen character varying)
  RETURNS TABLE(enlace text, correlativo bigint, fechainstalacion text, codigobanco integer, ibp character varying, usuario character varying, fechacarga text, namearchivo character varying, sizearchivo character varying, cantidad bigint, nafiliacion character varying) AS
$BODY$
	
	
	Begin
		return 	QUERY 
			select p.codigobanco::integer||'/'||g.fechainstalacion::date||'/'||origen||'/'||p.namearchivo  as enlace,
			row_number()  OVER(order by p.codigobanco::integer), to_char(g.fechainstalacion::date,'DD/MM/YYYY'), p.codigobanco::integer, b.ibp,
			p.usuario,to_char(p.idate::date,'DD/MM/YYYY'), p.namearchivo, p.sizearchivo, count(p.codigobanco), p.nafiliacion as cantidad
			from clie_tblclientepotencial p
			inner join tblgestionserviciotecnico g
			on g.nroafiliado=p.nafiliacion 
			left join  tblfacturacioncliente t
			on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			WHERE 
			p.codestatus in ('1','2') and  t.nfactura is not null
			AND
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			AND 
			    CASE WHEN fecha is NULL or fecha='' then g.fechainstalacion IS NOT null
				  else g.fechainstalacion::date=fecha::date END	
			group by (g.fechainstalacion
			::date), p.codigobanco::integer, b.ibp,  p.usuario, (p.idate::date), p.namearchivo, p.sizearchivo, p.nafiliacion
			ORDER BY p.idate::date;
	End;
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_buscarreportepos(character varying, character varying)
  OWNER TO postgres;
