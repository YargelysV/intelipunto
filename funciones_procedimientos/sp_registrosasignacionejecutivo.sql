﻿-- Function: sp_registrosasignacionejecutivo(character varying)

-- DROP FUNCTION sp_registrosasignacionejecutivo(character varying);

CREATE OR REPLACE FUNCTION sp_registrosasignacionejecutivo(IN icodigobanco character varying)
  RETURNS TABLE(correlativo bigint, oid_consecutivo integer, ofecharecep text, onombre character varying, oapellido character varying, ocodigobanco integer, obancoasig integer, otipopos character varying, otipolinea character varying, omodelonegocio character varying, otipocliente character varying, onafiliado character varying, ocantposasignados integer, orazonsocial character varying, ocoddocumento character varying, oacteconomica character varying, orepresentantelegal character varying, ocirepresentantelegal character varying, rifrlegal character varying, ocorreorepresentantelegal character varying, otlfrepresentantelegal character varying, otlf1 character varying, otlf2 character varying, oncuentapos character varying, odireccion_instalacion character varying, oestado character varying, omunicipio character varying, oparroquia character varying, ousuario character varying, ofecharegistro text, onamearchivo character varying, osizearchivo character varying, ocodestatus integer, orazoncomercial character varying, oinst_organismo character varying, otlfm1 character varying, otlfm2 character varying, oemailcorporativo character varying, oemailcomercial character varying, oemailpersonal character varying, ocodmarcapos integer, ocantposconfirm integer, otipocuenta integer, oidestatus integer, ofechaecomercializacion text, ousuarioenvio character varying, ousuariorecibe character varying, ointeres character varying, omantenimiento character varying, ousuarioedita character varying, ofecha_edit text, oagencia character varying, oestatuscontacto integer, oejecutivo character varying, obancointeres character varying, omediocontacto character varying) AS
$BODY$
	Begin	


 return query 
  select  row_number()  OVER(order by id_consecutivo::integer),* from (
   select   
   id_consecutivo, to_char(fecharecepcion, 'DD/MM/YYYY') as fecharecep, nombre  , apellido  , codigobanco::integer,
  case when cbancoasig='' or cbancoasig is null then 0 else cbancoasig::integer end , tipopos , tipolinea , tipomodelonegocio , tipocliente , 
  nafiliacion , cantterminalesasig , razonsocial , coddocumento , acteconomica , rlegal , cirl , 
  rifrl , correorl , telfrl ,telf1, telf2 , ncuentapos , direccion_instalacion , estado ,municipio , 
  parroquia , usuario ,to_char(idate, 'DD/MM/YYYY') as fecharegistro , namearchivo ,sizearchivo ,codestatus , razoncomercial ,
  instituto_organismo , tlf_movil1 , tlf_movil2 , email_corporativo , email_comercial  , email_personal ,
  codmarcapos , cant_pos_confirm ,tipo_cuenta ,idstatus ,to_char(fechaenviocomercializacion, 'DD/MM/YYYY') as fechaecomercialiacion  ,usuarioenvio ,usuariorecibe ,interes ,
  mantenimiento ,usuarioedita , to_char(fechaedita, 'DD/MM/YYYY') as fecha_edit   ,agencia ,estatus_contacto , ejecutivo ,bancointeres ,mediocontacto
  from tbl_prospectos where codigobanco::integer = icodigobanco::integer and cbancoasig<>'' and codigobanco<>'' 
UNION
  select id_consecutivo, to_char(fecharecepcion, 'DD/MM/YYYY') as fecharecep, nombre  , apellido  , codigobanco::integer,
  case when cbancoasig='' or cbancoasig is null then 0 else cbancoasig::integer end , tipopos , tipolinea , tipomodelonegocio , tipocliente , 
  nafiliacion , cantterminalesasig , razonsocial , coddocumento , acteconomica , rlegal , cirl , 
  rifrl , correorl , telfrl ,telf1, telf2 , ncuentapos , direccion_instalacion , estado ,municipio , 
  parroquia , usuario ,to_char(idate, 'DD/MM/YYYY') as fecharegistro , namearchivo ,sizearchivo ,codestatus , razoncomercial ,
  instituto_organismo , tlf_movil1 , tlf_movil2 , email_corporativo , email_comercial  , email_personal ,
  codmarcapos , cant_pos_confirm ,tipo_cuenta ,idstatus ,to_char(fechaenviocomercializacion, 'DD/MM/YYYY') as fechaecomercialiacion  ,usuarioenvio ,usuariorecibe ,interes ,
  mantenimiento ,usuarioedita , to_char(fechaedita, 'DD/MM/YYYY') as fecha_edit   ,agencia ,estatus_contacto , ejecutivo ,bancointeres ,mediocontacto
  from tbl_prospectos where cbancoasig::integer = icodigobanco::integer  and codigobanco::integer<>icodigobanco::integer and codigobanco<>'' and cbancoasig<>''
--  ORDER BY id_consecutivo asc
UNION
	select id_consecutivo, to_char(fecharecepcion, 'DD/MM/YYYY') as fecharecep, nombre, apellido,codigobanco::integer,
	cbancoasig::INTEGER, tipopos , tipolinea , tipomodelonegocio , tipocliente , 
	nafiliacion , cantterminalesasig , razonsocial , coddocumento , acteconomica , rlegal , cirl , 
	rifrl , correorl , telfrl ,telf1, telf2 , ncuentapos , direccion_instalacion , estado ,municipio , 
	parroquia , usuario ,to_char(idate, 'DD/MM/YYYY') as fecharegistro , namearchivo ,sizearchivo ,codestatus , razoncomercial ,
	instituto_organismo , tlf_movil1 , tlf_movil2 , email_corporativo , email_comercial  , email_personal ,
	codmarcapos , cant_pos_confirm ,tipo_cuenta ,idstatus ,to_char(fechaenviocomercializacion, 'DD/MM/YYYY') as fechaecomercialiacion  ,usuarioenvio ,usuariorecibe ,interes ,
	mantenimiento ,usuarioedita , to_char(fechaedita, 'DD/MM/YYYY') as fecha_edit   ,agencia ,estatus_contacto , ejecutivo ,'' ,'5'
	from clie_tblclientepotencial where cbancoasig::integer = icodigobanco::integer and codigobanco::integer<>icodigobanco::integer and id_tblprospectos is null
UNION
	select id_consecutivo, to_char(fecharecepcion, 'DD/MM/YYYY') as fecharecep, nombre, apellido,codigobanco::integer,
	cbancoasig::INTEGER, tipopos , tipolinea , tipomodelonegocio , tipocliente , 
	nafiliacion , cantterminalesasig , razonsocial , coddocumento , acteconomica , rlegal , cirl , 
	rifrl , correorl , telfrl ,telf1, telf2 , ncuentapos , direccion_instalacion , estado ,municipio , 
	parroquia , usuario ,to_char(idate, 'DD/MM/YYYY') as fecharegistro , namearchivo ,sizearchivo ,codestatus , razoncomercial ,
	instituto_organismo , tlf_movil1 , tlf_movil2 , email_corporativo , email_comercial  , email_personal ,
	codmarcapos , cant_pos_confirm ,tipo_cuenta ,idstatus ,to_char(fechaenviocomercializacion, 'DD/MM/YYYY') as fechaecomercialiacion  ,usuarioenvio ,usuariorecibe ,interes ,
	mantenimiento ,usuarioedita , to_char(fechaedita, 'DD/MM/YYYY') as fecha_edit   ,agencia ,estatus_contacto , ejecutivo ,'' ,'5'
	from clie_tblclientepotencial where codigobanco::integer = icodigobanco::integer and id_tblprospectos is null
ORDER BY id_consecutivo asc) as result;

 --  SELECT * FROM clie_tblclientepotencial WHERE CODIGOBANCO='108'
  
-- select * from tbl_prospectos 
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_registrosasignacionejecutivo(character varying)
  OWNER TO postgres;
