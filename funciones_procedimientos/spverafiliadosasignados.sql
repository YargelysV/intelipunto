﻿--drop FUNCTION spverafiliadosasignados()
CREATE OR REPLACE FUNCTION spverafiliadosasignados()
  RETURNS table (razon character varying, documento character varying, afiliado character varying, ibp character varying) AS
$BODY$

Begin
	 return query 
		SELECT  p.razonsocial, p.coddocumento, p.nafiliacion, b.ibp
			FROM clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer 
			where nafiliacion!=''; 
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spverafiliadosasignados()
  OWNER TO postgres;


--select * from spverafiliadosasignados()

select * from clie_tblclientepotencial