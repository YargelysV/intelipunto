﻿-- Function: sp_tipo_cliente()

-- DROP FUNCTION sp_tipo_cliente();

CREATE OR REPLACE FUNCTION sp_tipo_cliente()
  RETURNS TABLE(codigocliente integer, tipoclientecomer character varying) AS
$BODY$
	Begin
		return 	QUERY
		SELECT * FROM  tbltipocliente;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_tipo_cliente()
  OWNER TO postgres;
