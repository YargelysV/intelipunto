﻿-- Function: sp_comercializacionbuscar(numeric, character varying, character varying)

-- DROP FUNCTION sp_comercializacionbuscar(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_comercializacionbuscar(IN operacion numeric, IN banco character varying, IN fechac character varying)
  RETURNS TABLE(correlativo bigint, fechaenvio date, fecharecepcion timestamp with time zone, codigobanco character varying, tipopos character varying, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, municipio character varying, parroquia character varying, estado character varying, codestatus integer, ibp character varying, descestatus character varying, fechacarga date, canpos integer, modelonegocio character varying, tipocliente character varying, formapago character varying, transferencia character varying) AS
$BODY$
	Begin

	CASE operacion
	
	when 1 then
		return 	QUERY SELECT row_number()  OVER(order by p.coddocumento),CURRENT_DATE, p.fecharecepcion, p.codigobanco, p.tipopos, p.tipolinea, p.nafiliacion, 
		p.razonsocial, p.coddocumento, p.acteconomica, p.rlegal, p.cirl, p.rifrl, p.correorl, p.telfrl,
		p.telf1, p.telf2, p.ncuentapos, p.municipio, p.parroquia,p.estado, p.codestatus, b.ibp, s.descestatus,
		p.idate::date, case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end, p.tipomodelonegocio, p.tipocliente, f.nameformapago, p.ntransferencia 
		FROM clie_tblclientepotencial p 
		inner join tblbanco b 
		on b.codigobanco::integer=p.codigobanco::integer
		inner join tblestatussolpos s
		on s.codestatus=p.codestatus
		full outer join tblfacturacioncliente fc
		on fc.coddocumento=p.coddocumento and fc.clientebanco::integer=p.codigobanco::integer and fc.namearchivo=p.namearchivo
		full outer join tblformapago f
		on f.codformapago=fc.formapago
		where  p.codigobanco::integer=banco::integer;


       when 2 then
       return 	QUERY SELECT row_number()  OVER(order by p.coddocumento),CURRENT_DATE, p.fecharecepcion, p.codigobanco, p.tipopos, p.tipolinea, p.nafiliacion, 
		p.razonsocial, p.coddocumento, p.acteconomica, p.rlegal, p.cirl, p.rifrl, p.correorl, p.telfrl,
		p.telf1, p.telf2, p.ncuentapos, p.municipio, p.parroquia, p.estado, p.codestatus, b.ibp, s.descestatus,
		p.idate::date,  case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end, p.tipomodelonegocio, p.tipocliente, f.nameformapago, p.ntransferencia  
		FROM clie_tblclientepotencial p 
		inner join tblbanco b 
		on b.codigobanco::integer=p.codigobanco::integer
		inner join tblestatussolpos s
		on s.codestatus=p.codestatus
		full outer join tblfacturacioncliente fc
		on fc.coddocumento=p.coddocumento and fc.clientebanco::integer=p.codigobanco::integer and fc.namearchivo=p.namearchivo
		full outer join tblformapago f
		on f.codformapago=fc.formapago
		where  p.codigobanco::integer=banco::integer;
		
     when 3 then
       return 	QUERY SELECT row_number()  OVER(order by p.coddocumento),CURRENT_DATE,p.fecharecepcion, p.codigobanco, p.tipopos, p.tipolinea, p.nafiliacion, 
		p.razonsocial, p.coddocumento, p.acteconomica, p.rlegal, p.cirl, p.rifrl, p.correorl, p.telfrl,
		p.telf1, p.telf2, p.ncuentapos, p.municipio, p.parroquia,p.estado, p.codestatus, b.ibp, s.descestatus,
		p.idate::date, case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end,
		 p.tipomodelonegocio, p.tipocliente, f.nameformapago, fc.ntransferencia 
		FROM clie_tblclientepotencial p 
		inner join tblbanco b 
		on b.codigobanco::integer=p.codigobanco::integer
		inner join tblestatussolpos s
		on s.codestatus=p.codestatus
		full outer join tblfacturacioncliente fc
		on fc.coddocumento=p.coddocumento and fc.clientebanco::integer=p.codigobanco::integer and fc.namearchivo=p.namearchivo
		full outer join tblformapago f
		on f.codformapago=fc.formapago
		where  
		   CASE WHEN banco is NULL or banco='' then p.codigobanco!=''
				  else p.codigobanco::integer=banco::integer END
		--AND 
		 --  CASE WHEN fechac is NULL or fechac='' then p.fechaenviocomercializacion IS NOT null
	--			  else p.fechaenviocomercializacion::date=fechac::date END
				  
		
		and p.codestatus in ('1','2','4') and p.nafiliacion is null and (p.idstatus='0' or (p.idstatus='1' and fechaenviocomercializacion=now()::date))
		order by p.fecharecepcion, p.coddocumento;

		--select * from tblfacturacioncliente
		-- delete from clie_tblclientepotencial where codigobanco='175'
     
       else 
       return ;
       end case;
		
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_comercializacionbuscar(numeric, character varying, character varying)
  OWNER TO postgres;

--select * from sp_comercializacionbuscar('3','175','')