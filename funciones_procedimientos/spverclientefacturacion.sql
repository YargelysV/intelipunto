﻿-- Function: spverclientefacturacion(integer, date, character varying, character varying, character varying, bigint, character varying)

-- DROP FUNCTION spverclientefacturacion(integer, date, character varying, character varying, character varying, bigint, character varying);

CREATE OR REPLACE FUNCTION spverclientefacturacion(IN banco integer, IN fecha date, IN namearchiv character varying, IN masiva character varying, IN tipousuario character varying, IN ejecutivoin bigint, IN login character varying)
  RETURNS TABLE(enlaceafiliado text, correlativo bigint, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying, cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, calleav character varying, municipio character varying, parroquia character varying, urbanizacion character varying, local character varying, estado character varying, codigopostal character varying, puntoref character varying, usuario character varying, idate text, namearchivo character varying, sizearchivo character varying, direccion text, ejecutivo character varying, estatuspago integer) AS
$BODY$
	declare pagos bigint;
	Begin
	--pagos:=(SELECT * FROM tblpagos WHERE p_idafiliado='55');
	
	if (ejecutivoin>0) then	

		return query 
		--tr.coddocumento||'/'||tr.codigobanco::integer||'/'||tr.fecharecepcion::date||'/'||tr.namearchivo||'/'||''||'/'||masiva||'/'||tr.id_consecutivo, 
		select concat_ws('/',tr.coddocumento,tr.codigobanco::integer, tr.fecharecepcion::date,tr.namearchivo,'',tr.id_consecutivo,'A', '', 'JUNIORS') as enlaceafiliado, 		
		row_number()  OVER(order by tr.namearchivo), 
		to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
		tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
		tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
		tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, m.municipio, par.parroquia,
		 d.d_urbanizacion, d.d_nlocal, e.estado, d.d_codepostal, d.d_ptoref, tr.usuario, to_char(tr.idate::date,'DD/MM/YYYY'), 
		tr.namearchivo, tr.sizearchivo,  m.municipio||' '||par.parroquia||' '||e.estado as direccion, tr.ejecutivo, case when pago.pagos is null or pago.pagos = '0' then '2' else tf.estatus end 
		from clie_tblclientepotencial tr
		inner join tblbanco b on 
		b.codigobanco::integer=tr.codigobanco::integer
		left join tblusuario u 
		on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
		full outer join tbldirecciones d
		on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='1' and d.id_afiliado=tr.id_consecutivo
		left join tblestados e 
		on e.id_estado=d.d_estado
		left join tblmunicipios m 
		on m.id_municipio=d.d_municipio
		left join tblparroquias par 
		on par.id_parroquia=d.d_parroquia
		LEFT join tblfacturacioncliente tf
		on tf.id_registro=tr.id_consecutivo
		-- select * from tblfacturacioncliente
		-- select * from tblpagos
		LEFT join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
			   group by p_idafiliado) pago
			   on pago.p_idafiliado=tr.id_consecutivo
	--	on tp.p_idafiliado=tr.id_consecutivo
		where  tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha  
		and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
			 else tr.namearchivo=namearchiv END
		and tr.codestatus in ('1','2') and (u.usuario=login OR  tr.ejecutivo=login)
		
		ORDER BY row_number()  OVER(order by tr.namearchivo);

	else if  (tipousuario='E' or tipousuario='O') then

			return query 
		select concat_ws('/',tr.coddocumento,tr.codigobanco::integer, tr.fecharecepcion::date,tr.namearchivo,masiva,tr.id_consecutivo,tipousuario, ejecutivoin, login) as enlaceafiliado, 		
		row_number()  OVER(order by tr.namearchivo), 
		to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
		tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
		tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
		tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, m.municipio, par.parroquia,
		 d.d_urbanizacion, d.d_nlocal, e.estado, d.d_codepostal, d.d_ptoref, tr.usuario, to_char(tr.idate::date,'DD/MM/YYYY'), 
		tr.namearchivo, tr.sizearchivo,  m.municipio||' '||par.parroquia||' '||e.estado as direccion, tr.ejecutivo, case when pago.pagos is null or pago.pagos = '0' then '2' else tf.estatus end
		from clie_tblclientepotencial tr
		inner join tblbanco b on 
		b.codigobanco::integer=tr.codigobanco::integer
		full outer join tbldirecciones d
		on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='1' and d.id_afiliado=tr.id_consecutivo
		left join tblestados e 
		on e.id_estado=d.d_estado
		left join tblmunicipios m 
		on m.id_municipio=d.d_municipio
		left join tblparroquias par 
		on par.id_parroquia=d.d_parroquia
		LEFT join tblfacturacioncliente tf
		on tf.id_registro=tr.id_consecutivo
		-- select * from tblfacturacioncliente
		-- select * from tblpagos
		LEFT join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
			   group by p_idafiliado) pago
			   on pago.p_idafiliado=tr.id_consecutivo
			   
		where  tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha  
		and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
			 else tr.namearchivo=namearchiv END
		and tr.codestatus in ('1','2') and tr.ejecutivo=login
		
		ORDER BY row_number()  OVER(order by tr.namearchivo);

	
	else
			return query 
		--tr.coddocumento||'/'||tr.codigobanco::integer||'/'||tr.fecharecepcion::date||'/'||tr.namearchivo||'/'||''||'/'||masiva||'/'||tr.id_consecutivo, 
		select concat_ws('/',tr.coddocumento,tr.codigobanco::integer, tr.fecharecepcion::date,tr.namearchivo,masiva,tr.id_consecutivo,tipousuario, ejecutivoin, login) as enlaceafiliado, 		
		row_number()  OVER(order by tr.namearchivo), 
		to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
		tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
		tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
		tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, m.municipio, par.parroquia,
		 d.d_urbanizacion, d.d_nlocal, e.estado, d.d_codepostal, d.d_ptoref, tr.usuario, to_char(tr.idate::date,'DD/MM/YYYY'), 
		tr.namearchivo, tr.sizearchivo,  m.municipio||' '||par.parroquia||' '||e.estado as direccion, tr.ejecutivo, case when pago.pagos is null or pago.pagos = '0' then '2' else tf.estatus end
		from clie_tblclientepotencial tr
		inner join tblbanco b on 
		b.codigobanco::integer=tr.codigobanco::integer
		full outer join tbldirecciones d
		on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='1' and d.id_afiliado=tr.id_consecutivo
		left join tblestados e 
		on e.id_estado=d.d_estado
		left join tblmunicipios m 
		on m.id_municipio=d.d_municipio
		left join tblparroquias par 
		on par.id_parroquia=d.d_parroquia
		LEFT join tblfacturacioncliente tf
		on tf.id_registro=tr.id_consecutivo
		LEFT join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
			   group by p_idafiliado) pago
			   on pago.p_idafiliado=tr.id_consecutivo
			   
		where  tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha  
		and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
			 else tr.namearchivo=namearchiv END
		and tr.codestatus in ('1','2')
		
		ORDER BY row_number()  OVER(order by tr.namearchivo);


	end if;
		end if;	
		
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientefacturacion(integer, date, character varying, character varying, character varying, bigint, character varying)
  OWNER TO postgres;
