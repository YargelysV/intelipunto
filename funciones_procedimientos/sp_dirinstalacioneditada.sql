﻿
CREATE FUNCTION sp_dirinstalacioneditada(icoddocumento character varying,iprefijo character varying,ifechacarga character varying)
  RETURNS integer AS
$BODY$

	Begin
		return 	
		(select count(*) from tbldirecciones where d_coddocumento = icoddocumento and d_codigobanco = iprefijo and d_fechacarga=ifechacarga and d_codetipo =4)::int;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
