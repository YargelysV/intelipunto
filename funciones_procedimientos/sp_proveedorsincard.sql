﻿
CREATE FUNCTION sp_proveedorsincard(IN icodproveedor integer)
  RETURNS setof tblproveedorsincard AS
$BODY$

  DECLARE codetipo integer;
	
	Begin
	return	QUERY 
	SELECT * FROM tblproveedorsincard 
	where codproveedor=icodproveedor;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
