﻿-- Function: spverbancoseje(integer, character varying, bigint, character varying)

-- DROP FUNCTION spverbancoseje(integer, character varying, bigint, character varying);

CREATE OR REPLACE FUNCTION spverbancoseje(IN valor integer, IN tipousuario character varying, IN ejecutivo bigint, IN login character varying)
  RETURNS TABLE(codigobanco integer, ibp character varying) AS
$BODY$
	
	Begin

		CASE valor
			when 1 then

				if (ejecutivo>0) then
				
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				left join tblusuario u on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
				where u.usuario=login OR p.ejecutivo=login
				group by p.codigobanco::INTEGER, b.ibp;
				

				else if  (tipousuario='E' or tipousuario='O') then

					return query 
					select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
					where p.ejecutivo=login
					group by p.codigobanco::INTEGER, b.ibp;

				
				else
					return query 
					select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
					group by p.codigobanco::INTEGER, b.ibp;
				end if;
				end if;
				
			when 3 then
				
				if (ejecutivo>0) then
				
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				left join tblusuario u on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
				where u.usuario=login OR p.ejecutivo=login
				AND p.codestatus in ('1','2')
				group by p.codigobanco::INTEGER, b.ibp;
				
				else if  (tipousuario='E' or tipousuario='O') then

					return query 
					select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
					where p.codestatus in ('1','2') and p.ejecutivo=login
					group by p.codigobanco::INTEGER, b.ibp;	

				
				else
					return query 
					select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
					where p.codestatus in ('1','2')
					group by p.codigobanco::INTEGER, b.ibp;	
				end if;
				end if;	

			else
			return; 
		end case;
	End;


--  select * from clie_tblclientepotencial
--  select * from tblgestionserviciotecnico
--  select * from adm_ejecutivo

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverbancoseje(integer, character varying, bigint, character varying)
  OWNER TO postgres;
