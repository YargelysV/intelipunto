﻿-- Function: spestatusinspos()

-- DROP FUNCTION spestatusinspos();

CREATE OR REPLACE FUNCTION spestatusinspos()
  RETURNS SETOF tblestatusinspos AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblestatusinspos;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spestatusinspos()
  OWNER TO postgres;
