﻿-- Function: spvertipocliente()

-- DROP FUNCTION spvertipocliente();

CREATE OR REPLACE FUNCTION spvertipocliente()
  RETURNS SETOF tbltipocliente AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tbltipocliente;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spvertipocliente()
  OWNER TO postgres;
