﻿-- Function: sp_buscarinventariopos()

-- DROP FUNCTION sp_buscarinventariopos();

CREATE OR REPLACE FUNCTION sp_buscarinventariopos()
  RETURNS TABLE(enlace text, correlativo bigint, cantidad bigint, ifechacompra text, asignados bigint, porasignar bigint) AS
$BODY$

--  select * from sp_buscarinventariopos()
	Begin
		return query

			SELECT concat_ws('/', t.fechacompra::date) as enlace, row_number()  OVER(order by t.fechacompra::date) as correlativo,
			COUNT(*) AS TOTAL, to_char(T.FECHACOMPRA,'DD/MM/YYYY'), A.ASIGNADOS, P.PORASIGNAR FROM tblserialpos T
			left join (SELECT COUNT(*) AS ASIGNADOS, FECHACOMPRA FROM tblserialpos WHERE  ESTATUS='1' GROUP BY FECHACOMPRA) A 
			ON A.FECHACOMPRA=T.FECHACOMPRA 
			left join (SELECT COUNT(*) AS PORASIGNAR, FECHACOMPRA FROM tblserialpos WHERE  ESTATUS='0' GROUP BY FECHACOMPRA) P 
			ON P.FECHACOMPRA=T.FECHACOMPRA 
			GROUP BY T.FECHACOMPRA, A.ASIGNADOS, P.PORASIGNAR
			order by t.fechacompra::date;
			--select * from tblserialpos


	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_buscarinventariopos()
  OWNER TO postgres;
