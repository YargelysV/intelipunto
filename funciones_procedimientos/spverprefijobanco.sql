﻿-- Function: spverprefijobanco()

-- DROP FUNCTION spverprefijobanco();

CREATE OR REPLACE FUNCTION spverprefijobanco()
  RETURNS SETOF tblbanco AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblbanco;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverprefijobanco()
  OWNER TO postgres;