﻿-- Function: sp_eliminargestioncliente(character varying, integer)

-- DROP FUNCTION sp_eliminargestioncliente(character varying, integer);

CREATE OR REPLACE FUNCTION sp_eliminargestioncliente(inafiliacion character varying, ig_codegestion integer)
  RETURNS boolean AS
$BODY$

	DECLARE tiempo bigint;
	
	Begin

		tiempo:=(SELECT extract(days from (timestamp 'now()' - (g_fechacontacto)))as dif_dias  
			 from tblgestioncliente where g_nafiliado=inafiliacion 
			  group by dif_dias);
		if (tiempo=0) then
			delete from tblgestioncliente where g_nafiliado=inafiliacion and g_codegestion=ig_codegestion;
			return true;
			
		else
			return false;
		end if;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_eliminargestioncliente(character varying, integer)
  OWNER TO postgres;
