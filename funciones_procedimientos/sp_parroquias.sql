﻿-- Function: sp_parroquias(integer)

-- DROP FUNCTION sp_parroquias(integer);

CREATE OR REPLACE FUNCTION sp_parroquias(iid_municipio integer)
  RETURNS SETOF tblparroquias AS
$BODY$
	
	Begin		
	return 	QUERY SELECT * FROM tblparroquias where id_municipio=iid_municipio order by parroquia asc;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_parroquias(integer)
  OWNER TO postgres;
