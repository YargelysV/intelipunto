﻿-- Function: sp_gestion(numeric, integer, integer, integer, numeric, numeric, character varying)

-- DROP FUNCTION sp_gestion(numeric, integer, integer, integer, numeric, numeric, character varying);

CREATE OR REPLACE FUNCTION sp_gestion(operacion numeric, icorrelativo integer, imarcapos integer, icodtipo integer, ivalordolar numeric, ifactorconversion numeric, iusuario character varying)
  RETURNS boolean AS
$BODY$

declare secuencial integer;
declare existe integer;
declare existe1 integer;
declare ivalorbs numeric(15,2); 
declare lvalordolar numeric(15,2);
declare lfactorc numeric(15,2);
declare lmarcapos integer;
declare lcodtipo integer; 
	Begin		

                                 secuencial:=(select max(correlativo) from tblventapos);
                                 
			 	existe1:=(select count(*) from tblventapos where correlativo=icorrelativo);
				if(existe1>0)then
				insert into tblvalorhistorico
				select * from tblventapos
				where correlativo=icorrelativo;
					
			CASE operacion
			                           
                                -- delete from tblvalorhistorico
                                -- select * from tblvalorhistorico where codtipo=4
                               	
				when 2 then
						
				existe:=( select count(*) from tblventapos where marcapos=imarcapos and tipopos=icodtipo);
				secuencial:=(select max(correlativo) from tblventapos);
				if(existe>0)then
				return false;
				else
					ivalorbs:=(ivalordolar::numeric(15,2)*ifactorconversion::numeric(15,2)); 
					
					INSERT INTO tblventapos 
					values(imarcapos, icodtipo,ivalordolar, ifactorconversion, ivalorbs, now(), iusuario,secuencial+1);
--select * from tblventapos
--select * from tblvalorhistorico	
      				        insert into tblvalorhistorico
					values(imarcapos, icodtipo,ivalordolar, ifactorconversion, ivalorbs, now(), iusuario,secuencial+1);
					--select * from tblventapos
					--where correlativo=secuencial+1;

					return true;
					
					
				end if;	

				
				when 3 then
				
				
					if ((imarcapos!=0) or (icodtipo!=0)) then
					
						lmarcapos:=(case WHEN imarcapos=0 or imarcapos is NULL  then 
								       (select marcapos from tblventapos where correlativo=icorrelativo)
								       else imarcapos end);
								       
						lcodtipo:=(case WHEN icodtipo=0 or icodtipo is NULL  then 
								       (select tipopos from tblventapos where correlativo=icorrelativo)
								       else icodtipo end);
								       
						existe:=( select count(*) from tblventapos where 
							 marcapos=lmarcapos and 
							 tipopos=lcodtipo);
						if(existe>0)then
							return false;
						else
							
							 lvalordolar:=(case WHEN ivalordolar=0 or ivalordolar is NULL  then 
								       (select valordolar from tblventapos where correlativo=icorrelativo)
								       else ivalordolar end);
							 lfactorc:=(case WHEN ifactorconversion=0 or ifactorconversion is NULL  then 
								       (select factorconversion from tblventapos where correlativo=icorrelativo)
								       else ifactorconversion end);
							 ivalorbs:=(lvalordolar::numeric(15,2)*lfactorc::numeric(15,2));

							 update tblventapos set 
							 
							  marcapos=     CASE 
									WHEN imarcapos IS NULL OR imarcapos = 0 THEN marcapos
									ELSE imarcapos END,
							 tipopos=     CASE 
									WHEN icodtipo IS NULL OR icodtipo = 0 THEN tipopos
									ELSE icodtipo END,
							 valordolar=     CASE 
									WHEN ivalordolar IS NULL OR ivalordolar = 0 THEN valordolar
									ELSE ivalordolar END,
							factorconversion=     CASE 
									WHEN ifactorconversion IS NULL OR ifactorconversion = 0 THEN factorconversion
									ELSE ifactorconversion END,	
							valorbs= ivalorbs,
							fechaact=now(),usuario=iusuario
							WHERE correlativo=icorrelativo;
							return true;
						end if;	
					else
							lvalordolar:=(case WHEN ivalordolar=0 or ivalordolar is NULL  then 
								       (select valordolar from tblventapos where correlativo=icorrelativo)
								       else ivalordolar end);
							 lfactorc:=(case WHEN ifactorconversion=0 or ifactorconversion is NULL  then 
								       (select factorconversion from tblventapos where correlativo=icorrelativo)
								       else ifactorconversion end);
							 ivalorbs:=(lvalordolar::numeric(15,2)*lfactorc::numeric(15,2));

				 update tblventapos set 
							 
							  marcapos=     CASE 
									WHEN imarcapos IS NULL OR imarcapos = 0 THEN marcapos
									ELSE imarcapos END,
							 tipopos=     CASE 
									WHEN icodtipo IS NULL OR icodtipo = 0 THEN tipopos
									ELSE icodtipo END,
							 valordolar=     CASE 
									WHEN ivalordolar IS NULL OR ivalordolar = 0 THEN valordolar
									ELSE ivalordolar END,
							factorconversion=     CASE 
									WHEN ifactorconversion IS NULL OR ifactorconversion = 0 THEN factorconversion
									ELSE ifactorconversion END,	
							valorbs= ivalorbs,
							fechaact=now(),usuario=iusuario
							WHERE correlativo=icorrelativo;
							return true;
				end if;
					
				when 4 then
					Delete from tblventapos WHERE correlativo=icorrelativo;
					return true;
					
				else 
				return false;
			end case;
			return true;
				
				else
				return false;	
				 end if;
				
		End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_gestion(numeric, integer, integer, integer, numeric, numeric, character varying)
  OWNER TO postgres;
