﻿-- Function: sp_comerdatoscontacto(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer)

-- DROP FUNCTION sp_comerdatoscontacto(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer);

CREATE OR REPLACE FUNCTION sp_comerdatoscontacto(icoddocumento character varying, itlf_movil1 character varying, itlf_movil2 character varying, iemail_corporativo character varying, iemail_comercial character varying, iemail_personal character varying, 
inombrearchivo character varying, inombre character varying, iapellido character varying, isuario character varying, iinteres character varying, inafiliado character varying)
  RETURNS boolean AS
$BODY$

DECLARE result int;
--   select * from clie_tblclientepotencial where coddocumento='123456'

--  update  clie_tblclientepotencial set interes='Si' where coddocumento='123456' and namearchivo= 'BANCOInteligensa_25102017_1.csv'
--   select * from tblgestioncliente where g_estatuspos=3 g_coddocumento='123456' and g_nombrearchivo= 'BANCOInteligensa_25102017_1.csv'
--   select * from tblestatussolpos 
declare estatus integer;
declare estatusgestionno integer;
declare estatusgestionsi integer;
Begin
			
			update clie_tblclientepotencial set 							  
			  tlf_movil1=itlf_movil1, tlf_movil2=itlf_movil2,
			  email_corporativo=iemail_corporativo, email_comercial=iemail_comercial,email_personal=iemail_personal,nombre=inombre,apellido=iapellido,interes=iinteres,usuarioedita=isuario,fechaedita=now()
			where coddocumento=icoddocumento  and namearchivo=inombrearchivo;
		
                 estatus:=(select count(*) from tblgestioncliente  where g_coddocumento=icoddocumento and g_nombrearchivo=inombrearchivo);
                 estatusgestionno:=(select g_estatuspos from tblgestioncliente  where g_coddocumento=icoddocumento and g_nombrearchivo=inombrearchivo and g_estatuspos=4);
                 estatusgestionsi:=(select g_estatuspos from tblgestioncliente  where g_coddocumento=icoddocumento and g_nombrearchivo=inombrearchivo and g_estatuspos=3);
                if(iinteres='Si' and estatus=0 and estatusgestionsi<>3)then
		INSERT INTO tblgestioncliente(g_nafiliado,g_estatuspos,g_fechacontacto,g_observacion,g_coddocumento,g_nombrearchivo,g_usuariocarga,g_fechacarga)
	        values(inafiliado, 3 , now(), 'insert desde sistema interesado',icoddocumento,inombrearchivo,isuario,now());
	        return true;
	        end if;
	        if(iinteres='No' and estatus=0 and estatusgestionno<>4)then
	        INSERT INTO tblgestioncliente(g_nafiliado,g_estatuspos,g_fechacontacto,g_observacion,g_coddocumento,g_nombrearchivo,g_usuariocarga,g_fechacarga)
	        values(inafiliado, 4 , now(), 'no esta interesado insert',icoddocumento,inombrearchivo,isuario,now());
	        return true;
	        end if;

                if(iinteres='Si' and estatusgestionno=4)then
		update tblgestioncliente set g_nafiliado =inafiliado, g_usuariocarga=isuario, g_fechacarga=now(),g_estatuspos= 3, g_observacion='si esta interesado update'  where g_coddocumento=icoddocumento and g_nombrearchivo=inombrearchivo;
		return true;
	        end if; 
	        
	        if(iinteres='No' and estatusgestionsi=3)then
		update tblgestioncliente set g_nafiliado =inafiliado, g_usuariocarga=isuario, g_fechacarga=now(),g_estatuspos= 4, g_observacion='no esta interesado update' where g_coddocumento=icoddocumento and g_nombrearchivo=inombrearchivo;
		return true;
	        end if;        
	
	return true;	
		
			
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_comerdatoscontacto(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer)
  OWNER TO postgres;
