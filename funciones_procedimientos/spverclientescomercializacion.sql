﻿-- Function: spverclientescomercializacion()

-- DROP FUNCTION spverclientescomercializacion();

CREATE OR REPLACE FUNCTION spverclientescomercializacion()
  RETURNS TABLE(fechaenvio date, fecharecepcion date, estatusventa character varying, estatusafiliado character varying, cbanco character varying, ibp character varying, cantidadpos integer, tipopv character varying, tipoline character varying, tipomodelo character varying, tipoclient character varying, razon character varying, documento character varying, afiliado character varying, formapago character varying, transferencia character varying) AS
$BODY$

Begin
	 return query 
		SELECT  p.fechaenviocomercializacion, p.fecharecepcomercializacion, s.descestatus as estatusventa, c.descestatus as estatusafiliado,
			p.codigobanco, b.ibp, case when p.cant_pos_confirm is null then p.cantterminalesasig else p.cant_pos_confirm end, p.tipopos, p.tipolinea, p.tipomodelonegocio, p.tipocliente, p.razonsocial, p.coddocumento,
			p.nafiliacion, f.nameformapago, fc.ntransferencia 
			FROM clie_tblclientepotencial p 
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			inner join tblestatussolpos s
			on s.codestatus=p.codestatus
			full outer join tblfacturacioncliente fc
			on fc.coddocumento=p.coddocumento and fc.clientebanco::integer=p.codigobanco::integer and fc.namearchivo=p.namearchivo
			inner join tblformapago f
			on f.codformapago=fc.formapago
			inner join tblestatuscomercializacion c
			on c.idstatus=p.idstatus
			where p.codestatus in('1','2'); 
End;

--select * from tblestatussolpos
--select * from clie_tblclientepotencial where codigobanco='102'
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientescomercializacion()
  OWNER TO postgres;

select * from spverclientescomercializacion()