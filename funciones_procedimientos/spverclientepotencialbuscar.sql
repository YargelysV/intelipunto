﻿-- Function: spverclientepotencialbuscar(character varying, character varying, character varying, character varying, bigint, character varying, bigint, bigint)

-- DROP FUNCTION spverclientepotencialbuscar(character varying, character varying, character varying, character varying, bigint, character varying, bigint, bigint);

CREATE OR REPLACE FUNCTION spverclientepotencialbuscar(IN banco character varying, IN fecha character varying, IN namearchiv character varying, IN tipousuario character varying, IN ejecutivoin bigint, IN login character varying, IN coordinador bigint, IN coordinadordos bigint)
  RETURNS TABLE(enlaceafiliado text, correlativo bigint, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying, cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, razoncomercial character varying, personacontacto text, tlf1 character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, direccion_instalacion character varying, estado character varying, municipio character varying, parroquia character varying, usuario character varying, idate text, namearchivo character varying, sizearchivo character varying, direccion text, activo integer, usuarioactivo character varying, enlaceactivo text, idestatus_contacto integer, desc_estatus_contacto character varying, usuarioedita character varying, ejecutivo character varying, fechaedita text, bcoasignado character varying) AS
$BODY$
	
	Begin
--	select usuarioedita FROM clie_tblclientepotencial where nafiliacion='76161992'
--      select * from tbl_estatus_contacto
--      select * from spverclientepotencialbuscar('171','23-08-2019','','B','0','OGARCIA','1')
		
		if (ejecutivoin>0 or coordinador>0 or coordinadordos>0) then	
			return 	QUERY 
				select concat_ws('/', tr.id_consecutivo::integer, tr.coddocumento,tr.codigobanco::integer,tr.idate::date,tr.namearchivo,tipousuario, ejecutivoin, login, '', '', coordinador, coordinadordos) as enlaceafiliado, 
				row_number()  OVER(order by tr.razonsocial), 
				to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial,tr.razoncomercial,concat_ws(' ', tr.nombre,tr.apellido) as personacontacto,tr.telf1, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.direccion_instalacion, e.estado,
				m.municipio, par.parroquia, tr.usuario,  to_char(tr.idate::date,'DD/MM/YYYY'), tr.namearchivo, 
				tr.sizearchivo, 
				concat_ws(' ', tr.direccion_instalacion,e.estado,m.municipio,par.parroquia ) as direccion, tr.activo, tr.usuario_activo, tr.id_consecutivo||'/'||tr.activo  as enlaceactivo, tr.estatus_contacto, ec.descripcion_estatus_contacto
				,tr.usuarioedita, tr.ejecutivo, to_char(tr.fechaedita::date,'DD/MM/YYYY'), bco.ibp
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				left join tblbanco bco on 
				bco.codigobanco::integer=tr.cbancoasig::integer 
				left join tblusuario u 
				on b.coddocumento=u.tipodoc||'-'||u.coddocumento or b.coordinador1=u.tipodoc||'-'||u.coddocumento or b.coordinador2=u.tipodoc||'-'||u.coddocumento
				left join tblestados e 
				on e.id_estado=tr.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=tr.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=tr.parroquia::integer
				inner join tbl_estatus_contacto ec on
				ec.id_estatus_contacto=tr.estatus_contacto
				where 
				CASE WHEN banco is NULL or banco='' then tr.codigobanco!=''
				else tr.codigobanco=banco END
				and CASE WHEN fecha is NULL or fecha='' then tr.fecharecepcion is NOT NULL 
				else tr.fecharecepcion::date=fecha::date END
				and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				else tr.namearchivo=namearchiv END
				and (u.usuario=login OR tr.ejecutivo=login)
				group by tr.fecharecepcion,tr.codigobanco,b.ibp,tr.tipopos,tr.tipolinea,tr.nafiliacion, tr.razonsocial,tr.razoncomercial, personacontacto,tr.telf1,tr.coddocumento,
				tr.acteconomica, tr.rlegal, tr.tipolinea, tr.nafiliacion, tr.razonsocial,tr.razoncomercial,personacontacto,tr.telf1, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.direccion_instalacion, e.estado,
				m.municipio, par.parroquia, tr.usuario, tr.idate::date, tr.namearchivo, 
				tr.sizearchivo, direccion, tr.activo, tr.usuario_activo, enlaceactivo, tr.estatus_contacto, ec.descripcion_estatus_contacto
				,tr.usuarioedita, tr.ejecutivo,tr.fechaedita::date, bco.ibp
				ORDER BY row_number() OVER(order by tr.razonsocial);
				
			else if  (tipousuario='E' or tipousuario='O') then

				return 	QUERY 
				select concat_ws('/', tr.id_consecutivo::integer, tr.coddocumento,tr.codigobanco::integer,tr.idate::date,tr.namearchivo,tipousuario, ejecutivoin, login) as enlaceafiliado, 
				row_number()  OVER(order by tr.razonsocial), 
				to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial,tr.razoncomercial,concat_ws(' ', tr.nombre,tr.apellido) as personacontacto,tr.telf1, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.direccion_instalacion, e.estado,
				m.municipio, par.parroquia, tr.usuario,  to_char(tr.idate::date,'DD/MM/YYYY'), tr.namearchivo, 
				tr.sizearchivo, 
				concat_ws(' ', tr.direccion_instalacion,e.estado,m.municipio,par.parroquia ) as direccion, tr.activo, tr.usuario_activo, tr.id_consecutivo||'/'||tr.activo  as enlaceactivo, tr.estatus_contacto, ec.descripcion_estatus_contacto
				,tr.usuarioedita, tr.ejecutivo, to_char(tr.fechaedita::date,'DD/MM/YYYY'), bco.ibp
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				left join tblbanco bco on 
				bco.codigobanco::integer=tr.cbancoasig::integer 
				left join tblestados e 
				on e.id_estado=tr.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=tr.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=tr.parroquia::integer
				inner join tbl_estatus_contacto ec on
				ec.id_estatus_contacto=tr.estatus_contacto
				where 
				CASE WHEN banco is NULL or banco='' then tr.codigobanco!=''
				else tr.codigobanco=banco END
				and CASE WHEN fecha is NULL or fecha='' then tr.fecharecepcion is NOT NULL 
				else tr.fecharecepcion::date=fecha::date END
				and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				else tr.namearchivo=namearchiv END
				and tr.ejecutivo=login
				ORDER BY row_number() OVER(order by tr.razonsocial);

			else

			return 	QUERY 
				select concat_ws('/', tr.id_consecutivo::integer, tr.coddocumento,tr.codigobanco::integer,tr.idate::date,tr.namearchivo,tipousuario, ejecutivoin, login) as enlaceafiliado, 
				row_number()  OVER(order by tr.razonsocial), 
				to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial,tr.razoncomercial,concat_ws(' ', tr.nombre,tr.apellido) as personacontacto,tr.telf1, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.direccion_instalacion, e.estado,
				m.municipio, par.parroquia, tr.usuario,  to_char(tr.idate::date,'DD/MM/YYYY'), tr.namearchivo, 
				tr.sizearchivo, 
				concat_ws(' ', tr.direccion_instalacion,e.estado,m.municipio,par.parroquia ) as direccion, tr.activo, tr.usuario_activo, tr.id_consecutivo||'/'||tr.activo  as enlaceactivo, tr.estatus_contacto, ec.descripcion_estatus_contacto
				,tr.usuarioedita, tr.ejecutivo,to_char(tr.fechaedita::date,'DD/MM/YYYY'), bco.ibp
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				left join tblbanco bco on 
				bco.codigobanco::integer=tr.cbancoasig::integer 
				left join tblestados e 
				on e.id_estado=tr.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=tr.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=tr.parroquia::integer
				inner join tbl_estatus_contacto ec on
				ec.id_estatus_contacto=tr.estatus_contacto
				where 
				--CASE WHEN banco is NULL or banco='' then tr.codigobanco!=''
				--else tr.codigobanco=banco END
				tr.codigobanco=banco
				/*and CASE WHEN fecha is NULL or fecha='' then tr.fecharecepcion is NOT NULL 
				else tr.fecharecepcion::date=fecha::date END
				and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				else tr.namearchivo=namearchiv END*/
				ORDER BY row_number() OVER(order by tr.razonsocial);
			
			end if;
		end if;			
		
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientepotencialbuscar(character varying, character varying, character varying, character varying, bigint, character varying, bigint, bigint)
  OWNER TO postgres;


--select * from spverclientepotencialbuscar('116','','','A','0','JROJAS','0','0')


	/*select concat_ws('/', tr.id_consecutivo::integer, tr.coddocumento,tr.codigobanco::integer,tr.idate::date,tr.namearchivo,'A', '0', 'JROJAS') as enlaceafiliado, 
	row_number()  OVER(order by tr.razonsocial), 
	to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
	tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
	tr.tipolinea, tr.nafiliacion, tr.razonsocial,tr.razoncomercial,concat_ws(' ', tr.nombre,tr.apellido) as personacontacto,tr.telf1, tr.coddocumento, tr.acteconomica, tr.rlegal, 
	tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.direccion_instalacion, e.estado,
	m.municipio, par.parroquia, tr.usuario,  to_char(tr.idate::date,'DD/MM/YYYY'), tr.namearchivo, 
	tr.sizearchivo, 
	concat_ws(' ', tr.direccion_instalacion,e.estado,m.municipio,par.parroquia ) as direccion, tr.activo, tr.usuario_activo, tr.id_consecutivo||'/'||tr.activo  as enlaceactivo, tr.estatus_contacto, ec.descripcion_estatus_contacto
	,tr.usuarioedita, tr.ejecutivo,to_char(tr.fechaedita::date,'DD/MM/YYYY'), bco.ibp
	from clie_tblclientepotencial tr
	inner join tblbanco b on 
	b.codigobanco::integer=tr.codigobanco::integer
	left join tblbanco bco on 
	bco.codigobanco::integer=tr.cbancoasig::integer -- select * from clie_tblclientepotencial
	left join tblestados e 
	on e.id_estado=tr.estado::integer
	left join tblmunicipios m 
	on m.id_municipio=tr.municipio::integer
	left join tblparroquias par 
	on par.id_parroquia=tr.parroquia::integer
	inner join tbl_estatus_contacto ec on
	ec.id_estatus_contacto=tr.estatus_contacto
	where 
	tr.codigobanco='116'
	--CASE WHEN banco is NULL or banco='' then tr.codigobanco!=''
	--else tr.codigobanco=banco END
	and --CASE WHEN fecha is NULL or fecha='' then 
	tr.fecharecepcion is NOT NULL 
	--else tr.fecharecepcion::date=fecha::date END
	and-- CASE WHEN namearchiv is NULL or namearchiv='' then
	 tr.namearchivo!=''
	--else tr.namearchivo=namearchiv END
	ORDER BY row_number() OVER(order by tr.razonsocial);*/
