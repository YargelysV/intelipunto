﻿-- Function: speditarserviciomantenimiento(character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying)

-- DROP FUNCTION speditarserviciomantenimiento(character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying);

CREATE OR REPLACE FUNCTION speditarserviciomantenimiento(afiliacion character varying, ddocumento character varying, fpago integer, numtarjeta character varying, seguridad character varying, fechaven character varying, cuenta character varying, bancoo character varying,
 nombre character varying, icodtarjeta integer, fechareg character varying, login character varying)
  RETURNS boolean AS
$BODY$
	DECLARE existe bigint;
	
	Begin
--	select * from tblrecaudacionmant
--	select * from tblfacturacioncliente
		existe:=(select count(*) from tblrecaudacionmant where nafiliacion=afiliacion 
			 and documento=ddocumento);

		if(existe=0) then

			insert into tblrecaudacionmant 
			values (afiliacion,ddocumento,
				CASE 
				WHEN fpago IS NULL OR fpago = 0 THEN NULL 
				ELSE fpago END, 

				CASE
				WHEN numtarjeta IS NULL OR numtarjeta = '' THEN NULL 
				ELSE numtarjeta END,
				
				CASE
				WHEN icodtarjeta IS NULL OR icodtarjeta = 0 THEN NULL 
				ELSE icodtarjeta END,
								
				CASE 
				WHEN cuenta IS NULL OR cuenta = '' THEN NULL 
				ELSE cuenta END, 
				
				bancoo::integer, now(), login,
				CASE 
				
				WHEN fechaven IS NULL OR fechaven = '' THEN NULL 
				ELSE fechaven::DATE END);
			return true;
		else
		
			update tblrecaudacionmant set 	-- select * from tblrecaudacionmant 

			 formapago=	        CASE 
						WHEN fpago IS NULL OR fpago = 0 THEN formapago
						ELSE fpago END,	
			 ntarjeta=	        CASE 
						WHEN numtarjeta IS NULL OR numtarjeta = '' THEN ntarjeta
						ELSE numtarjeta END,			
			banco=	      		CASE 
						WHEN bancoo IS NULL OR bancoo = '' THEN banco
						ELSE bancoo::integer END,
			 
			codseguridad=	        CASE 
						WHEN seguridad IS NULL OR seguridad = '' THEN codseguridad 
						ELSE seguridad END,

			fechavenc=		CASE 
						WHEN fechaven IS NULL OR fechaven = '' THEN fechavenc 
						ELSE fechaven::DATE END,			

			ncuenta=	        CASE 
						WHEN cuenta IS NULL OR cuenta = '' THEN ncuenta 
						ELSE cuenta END,			
			
			fecharegistro= now(), usuario=login,	

			nombres=	        CASE 
						WHEN nombre IS NULL OR nombre = '' THEN nombres 
						ELSE nombre END,	

			codtarjeta=		CASE 
						WHEN icodtarjeta IS NULL OR icodtarjeta = 0 THEN codtarjeta 
						ELSE icodtarjeta END
				
				WHERE	nafiliacion=afiliacion and documento=ddocumento;
			return true;
		end if;
			
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION speditarserviciomantenimiento(character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying)
  OWNER TO postgres;
