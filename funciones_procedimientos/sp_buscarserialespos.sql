﻿-- Function: sp_buscarserialespos(date)

-- DROP FUNCTION sp_buscarserialespos(date);

CREATE OR REPLACE FUNCTION sp_buscarserialespos(IN fecha date)
  RETURNS TABLE(correlativo bigint, iproveedor character varying, ifechacompra text, imarcapos character varying, serialpos character varying, tipopv character varying, iactivofijo character varying, ibancoasignado integer, nombrebanco character varying, fechac text, usuario character varying) AS
$BODY$
	Begin
		return query 
			SELECT row_number()  OVER(order by serial) as correlativo,
			s.proveedor, to_char(s.fechacompra::date,'DD/MM/YYYY'), s.marcapos, s.serial, s.tipopos, s.activofijo, s.bancoasignado::integer, b.ibp, 
			to_char(s.fechacarga::date,'DD/MM/YYYY'), s.usuariocarga
			FROM tblserialpos as s
			full outer join tblbanco b on s.bancoasignado::integer=b.codigobanco::integer
			where fechacompra::date=fecha
			order by serial;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_buscarserialespos(date)
  OWNER TO postgres;

--SELECT * FROM sp_buscarserialespos('2018-03-16')
--select * from tblbanco