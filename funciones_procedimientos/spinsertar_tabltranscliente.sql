﻿-- Function: spinsertar_tabltranscliente(integer, timestamp with time zone, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, timestamp with time zone, character varying)

-- DROP FUNCTION spinsertar_tabltranscliente(integer, timestamp with time zone, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, timestamp with time zone);

CREATE OR REPLACE FUNCTION spinsertar_tabltranscliente(consecutivo integer, fecharecepcionarchivo timestamp with time zone, prefijobanco character varying, tipo character varying, tcliente character varying, cantter integer, nombreafiliado character varying, razoncome character varying, rif character varying, actividadeconomica character varying, nombrerepresentantelegal character varying, correoelectronico character varying, telefono1 character varying, telefono2 character varying, direccion character varying, destado character varying, dmunicipio character varying, dparroquia character varying, agencia character varying, afiliado character varying, login character varying, name character varying, size character varying, nejecutivo character varying, ejecutivocc character varying, fechacarga timestamp with time zone, bancoasignado character varying)
  RETURNS boolean AS
$BODY$
	declare tipoposs character varying;
	declare cantidadregistros bigint;
	Begin

	if (tipo='Fijo') THEN
	tipo='Dial UP';
	END IF;
	
	if (tipo='Inalambrico') THEN
	tipo='GPRS';
	END IF;

	if (tipo='') THEN
	tipo='';
	END IF;

	--select * from clie_tbltransferencia

		INSERT INTO clie_tbltransferencia (id_consecutivo,fecharecepcion, codigobanco, tipopos, tipocliente,
		nafiliacion, cantterminalesasig , razonsocial, razoncomercial, coddocumento, acteconomica, rlegal, correorl, telf1, telf2,
		direccion_instalacion, estado, municipio, parroquia, usuario, idate, namearchivo, sizearchivo, agencia, ejecutivo, ejecutivocall, cbancoasig)
		
		values(consecutivo, fecharecepcionarchivo, prefijobanco, tipo, upper(tcliente)  ,upper(afiliado), cantter,upper(nombreafiliado),upper(razoncome),
			upper(rif),upper(actividadEconomica),
			upper(nombrerepresentantelegal),upper(correoelectronico),
			telefono1,telefono2,upper(direccion),destado,dmunicipio,dparroquia,upper(login), fechacarga, upper(name), size,agencia, nejecutivo, ejecutivocc,bancoasignado);

		cantidadregistros:= (select count(*) from clie_tbltransferencia where direccion_instalacion=direccion and namearchivo=name and codigobanco::integer=prefijobanco::integer and coddocumento=rif );
                if (cantidadregistros>1) then
                return false;
                else	
		return true;
		end if;
			
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spinsertar_tabltranscliente(integer, timestamp with time zone, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, timestamp with time zone, character varying)
  OWNER TO postgres;

