﻿-- Function: spcomercializacionreporte(character varying, character varying)

-- DROP FUNCTION spcomercializacionreporte(character varying, character varying);

CREATE OR REPLACE FUNCTION spcomercializacionreporte(IN cliente character varying, IN fecha character varying)
  RETURNS TABLE(correlativo bigint, enlacereporte text, fechaenvio date, fecharecepcion date, estatusafiliado character varying, cbanco character varying, ibp character varying, registros bigint, cantidadpos bigint) AS
$BODY$
	
	Begin

		if (fecha!='') then
		
		return 	QUERY 
			SELECT row_number()  OVER(order by p.fechaenviocomercializacion::date desc) as correlativo, 
			concat_ws(',', p.codigobanco, p.fechaenviocomercializacion, p.fecharecepcomercializacion) as enlacereporte, 
			p.fechaenviocomercializacion::date, p.fecharecepcomercializacion, c.descestatus as estatusafiliado,
			p.codigobanco, b.ibp, count(p.coddocumento)as registros, sum(case when cant_pos_confirm is null or cant_pos_confirm='0' then cantterminalesasig else cant_pos_confirm end) as cantpos
			FROM clie_tblclientepotencial p 
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			inner join tblestatussolpos s
			on s.codestatus=p.codestatus
			left join  tblfacturacioncliente t
			on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo and t.id_afiliado=p.id_consecutivo
			full outer join tblformapago f
			on f.codformapago=t.formapago
			inner join tblestatuscomercializacion c
			on c.idstatus=p.idstatus
			where p.codestatus in ('1','2','4') 
			--and p.codigobanco::integer='175'
			AND
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			AND 
				p.fechaenviocomercializacion::date=fecha::date 	
			group by p.fechaenviocomercializacion, p.fecharecepcomercializacion, c.descestatus,
			p.codigobanco, b.ibp, p.idstatus
			order by  correlativo;
		else

		return 	QUERY 
			SELECT row_number()  OVER(order by p.fechaenviocomercializacion::date desc) as correlativo, 
			concat_ws(',', p.codigobanco, p.fechaenviocomercializacion, p.fecharecepcomercializacion) as enlacereporte, 
			p.fechaenviocomercializacion::date, p.fecharecepcomercializacion, c.descestatus as estatusafiliado,
			p.codigobanco, b.ibp, count(p.coddocumento)as registros, sum(case when cant_pos_confirm is null or cant_pos_confirm='0' then cantterminalesasig else cant_pos_confirm end) as cantpos
			FROM clie_tblclientepotencial p 
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			inner join tblestatussolpos s
			on s.codestatus=p.codestatus
			left join  tblfacturacioncliente t
			on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo and t.id_afiliado=p.id_consecutivo
			full outer join tblformapago f
			on f.codformapago=t.formapago
			inner join tblestatuscomercializacion c
			on c.idstatus=p.idstatus
			where p.codestatus in ('1','2','4') 
			--and p.codigobanco::integer='175'
			AND
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			group by p.fechaenviocomercializacion, p.fecharecepcomercializacion, c.descestatus,
			p.codigobanco, b.ibp, p.idstatus
			order by  correlativo;
		end if;	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spcomercializacionreporte(character varying, character varying)
  OWNER TO postgres;

 -- select * from spcomercializacionreporte('','')

--select * from clie_tblclientepotencial where fechaenviocomercializacion is not null