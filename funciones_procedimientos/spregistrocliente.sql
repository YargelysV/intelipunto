﻿-- Function: spregistrocliente(integer)

-- DROP FUNCTION spregistrocliente(integer);

CREATE OR REPLACE FUNCTION spregistrocliente(idregistro integer)
  RETURNS SETOF tbldirecciones AS
$BODY$
	
	Begin
		return 	QUERY 
			--select * from clie_tblclientepotencial 
			select * from tbldirecciones where d_codetipo='1' and id_afiliado=idregistro;
			
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spregistrocliente(integer)
  OWNER TO postgres;
