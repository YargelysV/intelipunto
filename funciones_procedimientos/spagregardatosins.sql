﻿
CREATE FUNCTION spagregardatosins(serial character varying, namearchivo character varying, fecharec date, coditecnico integer, fechages date, estatuspos integer, fechains date, usuariocarg character varying, nafiliacion character varying)
  RETURNS boolean AS
$BODY$
	Begin

		INSERT INTO tblgestionserviciotecnico (serialpos, nombrearchivo, fecharecepalmacen,codtecnico,fechagestion, estatus,fechainstalacion,fechacarga, usuariocarga, nroafiliado)
		values(serial, namearchivo, fecharec, coditecnico, fechages, estatuspos, fechains, now(), usuariocarg, nafiliacion);
		return true;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;