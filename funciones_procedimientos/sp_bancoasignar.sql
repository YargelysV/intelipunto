﻿-- Function: sp_bancoasignar()

-- DROP FUNCTION sp_bancoasignar();

CREATE OR REPLACE FUNCTION sp_bancoasignar()
  RETURNS SETOF tblbanco AS
$BODY$
	Begin
		return 	QUERY
		SELECT * FROM  tblbanco  ;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_bancoasignar()
  OWNER TO postgres;