﻿-- Function: spverejecutivos()

-- DROP FUNCTION spverejecutivos();

CREATE OR REPLACE FUNCTION spverejecutivos()
  RETURNS SETOF adm_ejecutivo AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM adm_ejecutivo where nvendedor is not null;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverejecutivos()
  OWNER TO postgres;
