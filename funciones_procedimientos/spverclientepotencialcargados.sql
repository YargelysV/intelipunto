﻿-- Function: spverclientepotencialcargados()

-- DROP FUNCTION spverclientepotencialcargados();

CREATE OR REPLACE FUNCTION spverclientepotencialcargados()
  RETURNS TABLE(fechaenvio date, fechacarga date, cantidadtotal bigint, archivo character varying) AS
$BODY$
	
	DECLARE banco integer;
	DECLARE fecha TIMESTAMP WITH TIME ZONE;
	
	Begin

	fecha:=(SELECT CAST(now() AS DATE) - CAST('30 days' AS INTERVAL));
	banco:=(select codigobanco::integer from clie_tbltransferencia limit 1);

	
	
				return query 
				select fecharecepcion::date, idate::date as fechacarga, count(codigobanco::integer) as cantidadtotal,  
				namearchivo
				from clie_tblclientepotencial
				where codigobanco::integer=banco and idate between fecha and now()
				group by  fecharecepcion,idate::date, codigobanco::integer, namearchivo
				order by idate::date;
		
	End;
	--select * from clie_tblclientepotencial

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientepotencialcargados()
  OWNER TO postgres;
