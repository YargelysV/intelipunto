﻿-- Function: mostrarserialafiliado(character varying, character varying, character varying)

-- DROP FUNCTION mostrarserialafiliado(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION mostrarserialafiliado(IN serial character varying,IN afiliado character varying, IN archivo character varying)
  RETURNS TABLE(secuencial bigint, nafiliado character varying, serialpo character varying, nombrearchivo character varying, fecharecepalma date,
  tecnico integer, fechagest date, estatus integer, fechainst date, fechacarga timestamp with time zone, usuario character varying, estatusinst character varying,
  ntencino text ) AS
$BODY$
	
Begin


return query 

			select row_number()  OVER(order by st.fechacarga),
			st.nroafiliado, st.serialpos, st.nombrearchivo, st.fecharecepalmacen, st.codtecnico, st.fechagestion, st.estatus,
			st.fechainstalacion, st.fechacarga, st.usuariocarga, pos.descestatus, t.nombres||' '||t.apellidos  as nombres
			from tblgestionserviciotecnico st 
			inner join tblestatusinspos pos
			on pos.codestatus=st.estatus
			inner join tbltecnicos t
			on t.codtecnicos=st.codtecnico
				where st.serialpos=serial and st.nroafiliado=afiliado and st.nombrearchivo=archivo
			order by row_number()  OVER(order by st.fechacarga);
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION mostrarserialafiliado(character varying, character varying, character varying)
  OWNER TO postgres;

--select * from mostrarserialafiliado('62647','16546544','BANCOprueba_23102017_4.csv')

--select * from tblgestionserviciotecnico