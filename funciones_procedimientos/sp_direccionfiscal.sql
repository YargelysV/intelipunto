﻿-- Function: sp_direccionfiscal(character varying, integer, character varying, integer, integer, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer)

-- DROP FUNCTION sp_direccionfiscal(character varying, integer, character varying, integer, integer, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer);

CREATE OR REPLACE FUNCTION sp_direccionfiscal(icoddocumento character varying, icodetipo integer, id_calle_av character varying, imunicipio integer, iparroquia integer, iurbanizacion character varying, inlocal character varying, iestado integer, icodepostal character varying, iptoref character varying, ifechacarga character varying, iprefijo character varying, inamearchivo character varying, iusuario character varying, idregistro integer)
  RETURNS boolean AS
$BODY$

DECLARE resultadoselect int;
Begin


			update clie_tblclientepotencial set 				  
			  usuarioedita=iusuario
			WHERE  id_consecutivo=idregistro ;

resultadoselect:=(select count(d_codetipo) from tbldirecciones where d_codetipo = icodetipo and d_coddocumento = icoddocumento and d_namearchivo= inamearchivo and d_codigobanco=iprefijo and id_afiliado=idregistro);

		if (resultadoselect <>0)then
			update tbldirecciones set d_calle_av=id_calle_av,d_municipio=imunicipio, d_parroquia=iparroquia, d_urbanizacion=iurbanizacion, d_nlocal=inlocal,
			 d_estado = iestado,d_codepostal=icodepostal, d_ptoref=iptoref,d_fechacarga= ifechacarga,d_codigobanco=iprefijo,d_namearchivo=inamearchivo,d_usuariocarga=iusuario
			where d_codetipo = icodetipo  and d_coddocumento = icoddocumento and d_codigobanco=iprefijo and d_namearchivo= inamearchivo and id_afiliado=idregistro;
			
			else
                        insert into tbldirecciones(d_coddocumento, d_codetipo, d_calle_av, d_municipio, d_parroquia, d_urbanizacion, d_nlocal, d_estado, d_codepostal, d_ptoref, d_fechacarga, d_codigobanco,d_namearchivo,d_usuariocarga,id_afiliado)
		        values (icoddocumento,icodetipo , id_calle_av  , imunicipio , iparroquia  , iurbanizacion  , inlocal  , iestado  , icodepostal  , iptoref,ifechacarga, iprefijo,inamearchivo,iusuario, idregistro);

	        end if;
	        return true;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_direccionfiscal(character varying, integer, character varying, integer, integer, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer)
  OWNER TO postgres;
