﻿-- Function: sp_buscartipousuario(character varying)

-- DROP FUNCTION sp_buscartipousuario(character varying);

CREATE OR REPLACE FUNCTION sp_buscartipousuario(IN usuarioeje character varying)
  RETURNS TABLE(tipousuario character, tipoejecutivo integer) AS
$BODY$

	Begin	--select * from adm_ejecutivo
			return query 
			--select * from tblusuario
			select u.codtipousuario, e.codejecutivos from tblusuario u
			inner join adm_ejecutivo e on e.nvendedor=u.usuario
			where u.usuario=usuarioeje;
			
			
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_buscartipousuario(character varying)
  OWNER TO postgres;
