﻿-- Function: spagregardatodsins(timestamp with time zone, timestamp with time zone, character varying, timestamp with time zone, character varying, character varying)

-- DROP FUNCTION spagregardatodsins(timestamp with time zone, timestamp with time zone, character varying, timestamp with time zone, character varying, character varying);

CREATE OR REPLACE FUNCTION spagregardatodsins(fechaent timestamp with time zone, fecharec timestamp with time zone, serial character varying, fechains timestamp with time zone, estatus character varying, nroafiliacion character varying)
  RETURNS boolean AS
$BODY$
	Begin

		
		INSERT INTO tblserviciotecnico (nafiliacion,nserialpos,codestatusins,fechaentccr,fecharecpos,fechainspos,fechamodst)
		values(nroafiliacion, serial, estatus, fechaent, fecharec, fechains, now());
		return true;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spagregardatodsins(timestamp with time zone, timestamp with time zone, character varying, timestamp with time zone, character varying, character varying)
  OWNER TO postgres;
