﻿-- Function: sp_caracteristicasdepago(character varying, integer, character varying, integer, character varying, character varying, character varying, numeric, integer, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying)

-- DROP FUNCTION sp_caracteristicasdepago(character varying, integer, character varying, integer, character varying, character varying, character varying, numeric, integer, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_caracteristicasdepago(icoddocumento character varying, bancocliente integer, inombrearchivo character varying, iformapago integer, referencia character varying, bancoo character varying, login character varying, icostopos numeric, icodtarjeta integer, ifechapago character varying, ihorapago character varying, factura character varying, fechafact character varying, inafiliacion character varying, icodestatus integer, iobservacion character varying, iejebanco character varying, iejeinteligensa character varying)
  RETURNS boolean AS
$BODY$
	DECLARE existe bigint;

	
	Begin
-- select * from tblfacturacioncliente where coddocumento='123456'
-- delete from tblfacturacioncliente where coddocumento='123456'
-- select * from tblventapos
		existe:=(select count(*) from tblfacturacioncliente where coddocumento=icoddocumento 
			 and clientebanco::INTEGER=bancocliente::INTEGER and namearchivo=inombrearchivo);

		if(existe=0) then

			insert into tblfacturacioncliente 
			values (icoddocumento, bancocliente::INTEGER, inombrearchivo, 
				CASE 
				WHEN fechafact IS NULL OR fechafact = '' THEN NULL 
				ELSE fechafact::DATE END, 
				CASE 
				WHEN factura IS NULL OR factura = '' THEN NULL 
				ELSE factura END, 
				iformapago,referencia,bancoo::integer, now(), login, icostopos,
				CASE 
				WHEN icodtarjeta IS NULL OR icodtarjeta = 0 THEN NULL 
				ELSE icodtarjeta END,
				CASE 
				WHEN ifechapago IS NULL OR ifechapago = '' THEN NULL 
				ELSE ifechapago::DATE END,
				CASE 
				WHEN ihorapago IS NULL OR ihorapago = '' THEN NULL 
				ELSE ihorapago END);
					-- select * from tblgestioncliente
			INSERT INTO tblgestioncliente(g_nafiliado,g_estatuspos,g_fechacontacto,g_observacion,g_coddocumento,g_nombrearchivo,g_usuariocarga,g_fechacarga,g_codejecutivobanco,g_codejecutivointeligensa)
			values(inafiliacion,icodestatus , now(), iobservacion,icoddocumento,inombrearchivo,login,now(),
				CASE 
				WHEN iejebanco IS NULL OR iejebanco = '' THEN NULL 
				ELSE iejebanco END,
				CASE 
				WHEN iejeinteligensa IS NULL OR iejeinteligensa = '' THEN NULL 
				ELSE iejeinteligensa END);
				
			return true;
		else
		
			update tblfacturacioncliente set 
			  fechafacturacion = 	CASE 
						WHEN fechafact IS NULL OR fechafact = '' THEN fechafacturacion 
						ELSE fechafact::DATE END, 
			  nfactura=	        CASE 
						WHEN factura IS NULL OR factura = '' THEN nfactura
						ELSE factura END,
			 formapago=	        iformapago,
			ntransferencia=	        CASE 
						WHEN referencia IS NULL OR referencia = '' THEN ntransferencia 
						ELSE referencia END,
			bancoorigen=	        CASE 
						WHEN bancoo IS NULL OR bancoo = '' THEN bancoorigen 
						ELSE bancoo::integer END,
			fecharegistro= now(), usuario=login,	

			costopos=	CASE 
					WHEN icostopos IS NULL OR icostopos = 0 THEN costopos 
					ELSE icostopos END,
			codtarjeta=     icodtarjeta ,
			fechapago=	CASE 
					WHEN ifechapago IS NULL OR ifechapago = '' THEN fechapago 
					ELSE ifechapago::DATE END,
			horapago=	CASE 
					WHEN ihorapago IS NULL OR ihorapago = '' THEN horapago 
					ELSE ihorapago END		
			 WHERE	coddocumento=icoddocumento and clientebanco::INTEGER=clientebanco::INTEGER and namearchivo=inombrearchivo;

			INSERT INTO tblgestioncliente(g_nafiliado,g_estatuspos,g_fechacontacto,g_observacion,g_coddocumento,g_nombrearchivo,g_usuariocarga,g_fechacarga,g_codejecutivobanco,g_codejecutivointeligensa)
			values(inafiliacion,icodestatus , now(), iobservacion,icoddocumento,inombrearchivo,login,now(),
				CASE 
				WHEN iejebanco IS NULL OR iejebanco = '' THEN NULL 
				ELSE iejebanco END,
				CASE 
				WHEN iejeinteligensa IS NULL OR iejeinteligensa = '' THEN NULL 
				ELSE iejeinteligensa END);
			return true;
		end if;
			
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_caracteristicasdepago(character varying, integer, character varying, integer, character varying, character varying, character varying, numeric, integer, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying)
  OWNER TO postgres;
