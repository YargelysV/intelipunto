﻿-- Function: sp_verafiliadocomercializacion(integer)

-- DROP FUNCTION sp_verafiliadocomercializacion(integer);

CREATE OR REPLACE FUNCTION sp_verafiliadocomercializacion(idregistro integer)
  RETURNS SETOF comercializacion AS
$BODY$
	
	Begin

		return 	QUERY 
			  select * from comercializacion where id_consecutivo=idregistro;
                       		   
	End;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_verafiliadocomercializacion(integer)
  OWNER TO postgres;
