﻿-- Function: spverificarmodulosactivos(character varying)

-- DROP FUNCTION spverificarmodulosactivos(character varying);

CREATE OR REPLACE FUNCTION spverificarmodulosactivos(IN login character varying)
  RETURNS TABLE(codmodulo character varying, codtipo character, codtipousuario character) AS
$BODY$
	
	Begin
		return 	QUERY 		
		select tblpermisosmodulos.codmodulo, tblpermisosmodulos.codtipo, tblusuario.codtipousuario from tblpermisosmodulos
		inner join tblmodulos on tblmodulos.codmodulo=tblpermisosmodulos.codmodulo
		inner join tblusuario on tblusuario.usuario=tblpermisosmodulos.usuario
		 where tblpermisosmodulos.usuario=login and tblpermisosmodulos.estatus='1' 
		 order by tblmodulos.serial ;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverificarmodulosactivos(character varying)
  OWNER TO postgres;

--select * from spverificarmodulosactivos('JROJAS')