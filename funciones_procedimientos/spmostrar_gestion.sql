﻿-- Function: spmostrar_gestion()

-- DROP FUNCTION spmostrar_gestion();

CREATE OR REPLACE FUNCTION spmostrar_gestion()
  RETURNS TABLE(secuencial integer, imarcapos integer, codtipo integer, ivalordolar numeric(15,2), ifactorconversion numeric(15,2), ivalorbs numeric(15,2), ifecha timestamp with time zone, iusuario character varying) AS
$BODY$

	Begin	
		return query select correlativo, marcapos, tipopos, valordolar, factorconversion,valorbs,fechaact, usuario from tblventapos 
		ORDER BY correlativo;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrar_gestion()
  OWNER TO postgres;
