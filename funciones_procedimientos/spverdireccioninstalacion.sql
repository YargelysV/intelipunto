﻿-- Function: spverdireccioninstalacion(integer)

-- DROP FUNCTION spverdireccioninstalacion(integer);

CREATE OR REPLACE FUNCTION spverdireccioninstalacion(idcliente integer)
  RETURNS integer AS
$BODY$

DECLARE valor integer;
DECLARE valor2 integer;

Begin


		valor:=(SELECT count (*) FROM tbldirecciones d
		where  d.d_codetipo= 4 and  d.id_afiliado=idcliente);
			
	if (valor = 0 )then

		valor2:=(SELECT cl.estado::integer FROM clie_tblclientepotencial cl
		where  cl.id_consecutivo=idcliente
		group by cl.estado);

			if (valor2 = 0)then
				return 0;
			else
				return 1;
			end if;
		
	else
		return 1;
		
	end if;
		
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spverdireccioninstalacion(integer)
  OWNER TO postgres;
