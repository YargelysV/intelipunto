﻿-- Function: sp_verclienteafiliado(integer, date, character varying)

-- DROP FUNCTION sp_verclienteafiliado(integer, date, character varying);

CREATE OR REPLACE FUNCTION sp_verclienteafiliado(IN banco integer, IN fecha date, IN namearchiv character varying)
  RETURNS TABLE(enlace text, correlativo bigint, fecharecepcion text, nafiliacion character varying, factura character varying, razonsocial character varying, documento character varying, tipopos character varying, cantterminalesasig integer, tipolinea character varying, ibp character varying, namearchivo character varying, estatus integer, direccion text, registro integer) AS
$BODY$
    
    Begin		
				--select * from sp_verclienteafiliado('116','6/2/2019','' )
					
									
    				
                return query 
--select * from clie_tblclientepotencial
--select * from tbldirecciones
                select 
                concat_ws('/', case when tr.nafiliacion is null then '' else tr.nafiliacion end,tr.namearchivo,tr.idate::date,tr.id_consecutivo,tr.coddocumento,tr.codigobanco),
              --  tr.nafiliacion||'/'||tr.namearchivo||'/'||tr.idate::date||'/'||tr.id_consecutivo||'/'||tr.coddocumento||'/'||tr.codigobanco,
                row_number()  OVER(order by tr.namearchivo), 
                 to_char(tr.fecharecepcion::date,'DD/MM/YYYY'),
                case when tr.nafiliacion is null then '' else tr.nafiliacion end,f.nfactura,tr.razonsocial, tr.coddocumento, tr.tipopos,
                case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end, tr.tipolinea, b.ibp, tr.namearchivo ,tr.codestatus,
		case when insta.direccion is null or  insta.direccion ='' then case WHEN tr.estado is null or tr.estado='0' then concat_ws(' ', e.estado,m.municipio,par.parroquia) else concat_ws(' ', tr.direccion_instalacion,e.estado,m.municipio,par.parroquia) end
		else insta.direccion  end , tr.id_consecutivo

		from clie_tblclientepotencial tr
                inner join tblbanco b on 
                b.codigobanco::integer=tr.codigobanco::integer	 
		left join (select concat_ws(' ', d.d_calle_av,d.d_urbanizacion,e.estado,m.municipio,par.parroquia) as direccion, d.id_afiliado
			   from tbldirecciones d 
			   left join tblestados e 
 			   on e.id_estado=d.d_estado
 			   left join tblmunicipios m 
			   on m.id_municipio=d.d_municipio
			   left join tblparroquias par 
			   on par.id_parroquia=d.d_parroquia 	
			   where d.d_codetipo='4' group by d.d_urbanizacion,d.d_calle_av, d.id_afiliado, e.estado, m.municipio,par.parroquia) insta
		on insta.id_afiliado=tr.id_consecutivo
		left join tblestados e 
		on e.id_estado=tr.estado::integer
		left join tblmunicipios m 
		on m.id_municipio=tr.municipio::integer
		left join tblparroquias par 
		on par.id_parroquia=tr.parroquia::integer 	
		inner join tblfacturacioncliente f on
		f.id_registro=tr.id_consecutivo
                where  tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha
                and tr.codestatus in('1','2') and f.nfactura!=''
                and CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
                         else tr.namearchivo=namearchiv END
	
                ORDER BY row_number()  OVER(order by tr.namearchivo);

		
    End;


    
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_verclienteafiliado(integer, date, character varying)
  OWNER TO postgres;

--166/25-07-2019
select * from sp_verclienteafiliado('166','25-07-2019','')