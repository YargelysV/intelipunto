﻿-- Function: spregistrosserviciotecnico(character varying, character varying)

-- DROP FUNCTION spregistrosserviciotecnico(character varying, character varying);

CREATE OR REPLACE FUNCTION spregistrosserviciotecnico(IN afiliado character varying, IN archivo character varying)
  RETURNS TABLE(nafiliado character varying, serial character varying, archivonombre character varying, fechaalmacen date, tecnico integer, fgestion date, status integer, finstalacion date,
  fcarga timestamp with time zone, usuario character varying, correlativo bigint) AS
$BODY$
	declare asignados bigint;
	declare postotal bigint;
	declare faltantes bigint;
	
	Begin
			asignados:=(select count(*) from tblgestionserviciotecnico where nroafiliado=afiliado and nombrearchivo=archivo);
			postotal:=(select cant_pos_confirm from clie_tblclientepotencial where nafiliacion=afiliado and namearchivo=archivo);
			faltantes:=(postotal-asignados);
			
			return query 
			select nroafiliado, serialpos, nombrearchivo, fecharecepalmacen, codtecnico, fechagestion, estatus, fechainstalacion, fechacarga, usuariocarga, 0
			from tblgestionserviciotecnico  where nroafiliado=afiliado and nombrearchivo=archivo
			union
			SELECT '' as nroafiliado,'' as seriapos,'' as nombrearchivo,null as fecharecepalmacen,0 as codtecnico,null as fechagestion,0 as estatus
			,null as fechainstalacion,null as fechacarga,'' as usuariocarga,  generate_series(1,faltantes) FROM  tblgestionserviciotecnico
			where nroafiliado=afiliado and nombrearchivo=archivo;
	
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spregistrosserviciotecnico(character varying, character varying)
  OWNER TO postgres;

  --select * from spregistrosserviciotecnico('16546544','BANCOprueba_23102017_4.csv')

  --select * from tblgestionserviciotecnico
