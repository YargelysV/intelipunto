﻿-- Function: sp_reportediarioestatusgeneral(integer)

-- DROP FUNCTION sp_reportediarioestatusgeneral(integer);

CREATE OR REPLACE FUNCTION sp_reportediarioestatusgeneral(IN banco integer)
  RETURNS TABLE(sec bigint, fecharecep date, codbanco character varying, nbanco character varying, ejec character varying, coddocumento character varying, consecutivo integer, razonsocial character varying, numafiliado character varying, correo character varying, tlf1 character varying, direccions text, cantidadpos integer, tipoposs character varying, marcapos character varying, fechacarga date, estatuscontac character varying, factura character varying, estatuscliente text, descequipo character varying, serialp character varying, serialsim character varying, tlf2 character varying, tlf3 character varying, tlf4 character varying, serialmifi character varying, fechainstalacion date) AS
$BODY$

	Begin		-- select * from sp_reportediarioestatusgeneral('0') order by serialp WHERE CONSECUTIVO='19887'
		
			-- select * from CLIE_TBLCLIENTEPOTENCIAL select * from tblinventariopos
			-- select *from tblgestionserviciotecnico where id_cliente='19507'

			return query 
			   
			select row_number () OVER (ORDER BY cp.codigobanco::integer, cp.estatus_contacto) as consecutivo,cp.fecharecepcion::date, cp.codigobanco, b.ibp,
		        cp.ejecutivo,  cp.coddocumento, cp.id_consecutivo, cp.razonsocial,cp.nafiliacion, cp.correorl, cp.telf1, concat_ws(' ', cp.direccion_instalacion,e.estado,m.municipio,par.parroquia) as direccion,	
			case when cp.cant_pos_confirm ='0' or cp.cant_pos_confirm is null then  cp.cantterminalesasig else cp.cant_pos_confirm end,
			cp.tipopos, mp.marcapos, cp.idate::date,ec.descripcion_estatus_contacto, fact.nfactura,
			(case when pago.pagos is null or pago.pagos = '0' then  'NO SE HAN CARGADO PAGOS' else 
			case when fact.fecha_facturacion is null then case when fact.estatus=0 then 'PAGOS SIN CONFIRMAR' ELSE 'SIN FACTURAR' end else 'FACTURADO' end end) as estatuscliente,
			case when inv.serialpos='' or inv.serialpos is null then 'Por Configurar POS' else est.desc_estatus end,inv.serialpos,inv.numsim, cp.telf2, CP.TLF_MOVIL1,
			CP.TLF_MOVIL2, inv.serialmifi, gestecn.fechainstalacion as fechainstalacion
			from clie_tblclientepotencial as cp
			inner join tblbanco as b
			on b.codigobanco::integer=cp.codigobanco::integer
			inner join tbl_estatus_contacto as ec
			on ec.id_estatus_contacto=cp.estatus_contacto
			left join tblmarcapos as mp on 
			mp.codtipo=cp.codmarcapos
			left join tblestados e 
			on e.id_estado=cp.estado::integer
			left join tblmunicipios m 
			on m.id_municipio=cp.municipio::integer
			left join tblparroquias par 
			on par.id_parroquia=cp.parroquia::integer 
			left join tblfacturacioncliente as fact
			on fact.id_registro=cp.id_consecutivo
			left join tblinventariopos as inv 
			on inv.id_cliente::integer=cp.id_consecutivo
			left join tblestatusinteliservices as est
			on est.idestatus=inv.idestatusinteliservices
			left join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
				group by p_idafiliado) pago
				on pago.p_idafiliado=cp.id_consecutivo
			left join (select tcn.fechainstalacion,tcn.id_cliente,tcn.serialpos from tblgestionserviciotecnico tcn where estatus in ('4','30','32')) gestecn
			on gestecn.id_cliente=cp.id_consecutivo::integer and gestecn.serialpos=inv.serialpos
		--	where fact.nfactura is null and inv.serialpos!=''
				
			where case when banco !='0' then cp.codigobanco::integer=banco::integer else cp.codigobanco::integer is not null end
			and (cp.tipopos!='MIFI' or cp.codmarcapos!=9)
				order by cp.codigobanco::integer, cp.estatus_contacto;
			
				-- where inv.serialpos!='' and fact.nfactura is null
				-- inner join tblfacturacioncliente as fac
				-- on fac.id_registro=
				-- select * from tblestatusinteliservices
				-- select * from tblpagos where p_idafiliado='15459'  order by p_idafiliado
				-- select * from tbldirecciones where id_afiliado='10343' () // todos 7300
				-- select * from tblinventariopos where id_cliente='12304' // seriales y sin card asignados
				-- select * from tblfacturacioncliente where id_registro='12427' // numero factura
				-- select * from clie_tblclientepotencial WHERE ID_CONSECUTIVO='10343'
					
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_reportediarioestatusgeneral(integer)
  OWNER TO postgres;
