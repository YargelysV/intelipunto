﻿-- Function: spbuscarusuario(character varying, integer, integer)

-- DROP FUNCTION spbuscarusuario(character varying, integer, integer);

CREATE OR REPLACE FUNCTION spusuario(login character varying)
  RETURNS SETOF tblusuario AS
$BODY$
	
	Begin
	return query SELECT * FROM tblUsuario WHERE usuario=login;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spusuario(character varying)
  OWNER TO postgres;

--select * from spusuario('JROJAS') 