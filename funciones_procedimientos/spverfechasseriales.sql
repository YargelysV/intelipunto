﻿CREATE OR REPLACE FUNCTION spverfechasseriales()
  RETURNS table(fecha date, total bigint) AS
$BODY$
	Begin
		return query 
			select fechacarga::date, count(serial) as total from  tblserialpos 
			group by fechacarga::date;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spverfechasseriales()
  OWNER TO postgres;

--select * from tblserialpos

--select * from spverfechasseriales()