﻿
-- Function: sp_verestado()

-- DROP FUNCTION sp_verestado();

CREATE OR REPLACE FUNCTION sp_estadoselected(IN iid_estado integer)
  RETURNS SETOF tblestados AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblestados where id_estado=iid_estado order by id_estado asc;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

