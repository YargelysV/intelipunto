﻿-- Function: sp_tienedireccionfiscal(character varying)

-- DROP FUNCTION sp_tienedireccionfiscal(character varying);

CREATE OR REPLACE FUNCTION sp_tienedireccionfiscal(icoddocumento character varying)
  RETURNS integer AS
$BODY$
	
	Begin
		return 	
		(select count(*) from tbldirecciones where d_coddocumento = icoddocumento and d_codetipo =1)::int;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_tienedireccionfiscal(character varying)
  OWNER TO postgres;
