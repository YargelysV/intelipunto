﻿-- Function: spverclienteserviciotecnico(integer, date, character varying)

-- DROP FUNCTION spverclienteserviciotecnico(integer, date, character varying);

CREATE OR REPLACE FUNCTION spverclienteserviciotecnico(IN banco integer, IN fecha date, IN namearchiv character varying)
  RETURNS TABLE(correlativo bigint, enlaceafiliado text, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying, 
  cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, 
  acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, 
  telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, estado character varying, 
  municipio character varying, parroquia character varying, usuario character varying, idate text, namearchivo character varying, 
  sizearchivo character varying, direccion text, tabladireccion character varying, marcapos character varying) AS
$BODY$
	
	Begin

			return query 
			SELECT row_number()  OVER(order by result.fecharecepcion),* from (	
				select 
				tr.coddocumento||'/'||tr.codigobanco::integer||'/'||tr.idate::date||'/'||tr.namearchivo as enlace, 
				to_char(tr.fecharecepcion::date,'DD/MM/YYYY') as fecharecepcion,
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, e.estado,m.municipio,
				par.parroquia, tr.usuario, to_char(tr.idate::date,'DD/MM/YYYY'), 
				tr.namearchivo, tr.sizearchivo,  
				concat_ws(' ', e.estado,m.municipio,par.parroquia,d.d_calle_av,d.d_urbanizacion,d.d_nlocal,d.d_codepostal,d.d_ptoref) as direccion,
				'NI'::character varying instalacion, mar.marcapos
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				inner join tbldirecciones d
				on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer 
					and  CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
					     else tr.namearchivo=namearchiv END and d.d_codetipo='4'
				full outer join tblmarcapos mar
				on mar.codtipo=tr.codmarcapos
				left join tblestados e 
				on e.id_estado=d.d_estado
				left join tblmunicipios m 
				on m.id_municipio=d.d_municipio
				left join tblparroquias par 
				on par.id_parroquia=d.d_parroquia
				where  tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha 
				and 
				  CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				  else tr.namearchivo=namearchiv END
				and tr.codestatus in ('1','2')      

				union
				select 
				tr.coddocumento||'/'||tr.codigobanco::integer||'/'||tr.idate::date||'/'||tr.namearchivo as enlacce, 
				to_char(tr.fecharecepcion::date,'DD/MM/YYYY') as fecharecepcion,
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, e.estado, m.municipio,
				par.parroquia, tr.usuario, to_char(tr.idate::date,'DD/MM/YYYY'),
				tr.namearchivo, tr.sizearchivo,
				concat_ws(' ', e.estado,m.municipio,par.parroquia,tr.direccion_instalacion) as direccion,
				'BI'::character varying instalacion,
				mar.marcapos
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				full outer join tblmarcapos mar
				on mar.codtipo=tr.codmarcapos
				left join tblestados e 
				on e.id_estado=tr.estado::integer
				left join tblmunicipios m 
				on m.id_municipio=tr.municipio::integer
				left join tblparroquias par 
				on par.id_parroquia=tr.parroquia::integer
				where  tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha  
				and  CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				     else tr.namearchivo=namearchiv END
				and tr.codestatus in ('1','2') 
				and tr.coddocumento||tr.fecharecepcion||tr.codigobanco||tr.namearchivo not in (select 
					tr.coddocumento||tr.fecharecepcion||tr.codigobanco||tr.namearchivo
					from clie_tblclientepotencial tr
					inner join tblbanco b on 
					b.codigobanco::integer=tr.codigobanco::integer
					inner join tbldirecciones d
					on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='4'
					where  tr.codigobanco::integer=banco::integer and tr.fecharecepcion::date=fecha 
					and
					  CASE WHEN namearchiv is NULL or namearchiv='' then tr.namearchivo!=''
				     	  else tr.namearchivo=namearchiv END
					and tr.codestatus in ('1','2') )
			)result  order by row_number()  OVER(order by result.fecharecepcion), result.coddocumento;         
	
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclienteserviciotecnico(integer, date, character varying)
  OWNER TO postgres;


--select * from spverclienteserviciotecnico('176','2017-10-07','BANCO_20022018_2.csv')