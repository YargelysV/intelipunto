﻿-- Function: spusuarios(numeric, character varying)

-- DROP FUNCTION spusuarios(numeric, character varying);

CREATE OR REPLACE FUNCTION spusuarios(IN operacion numeric, IN login character varying)
  RETURNS TABLE(tipodoc character varying, coddocumento character varying, usuario character varying, clave character varying, nombres character varying, apellidos character varying, fechanac date, correo character varying, codtipousuario character, activo character, idate timestamp without time zone, codarea integer, area character varying, cargo character varying, enlinea character, idinactivo integer, motivo character varying, tipousuario character varying, ejebanco bigint) AS
$BODY$
	--select * from tbltipousuario
	Begin
	case operacion
		when 1 then
		return 	QUERY 
			SELECT u.tipodoc, u.coddocumento, u.usuario, u.clave, u.nombres, u.apellidos, u.fechanac, u.correo, u.codtipousuario, 
			u.activo, u.idate,u.area, area.narea,  u.cargo, u.enlinea, u.idinactivo, m.motivo, tipo.tipousuario, count(b.codigobanco) AS ejebanco
			FROM tblUsuario u
			inner join tblareasinteligensa area
			on area.codarea=u.area
			full outer join tblusuario_motivo m 
			on m.idusuario=u.usuario and u.idinactivo=m.idinactivo
			inner join tbltipousuario tipo
			on tipo.codtipousuario=u.codtipousuario
			left join tblbanco b on b.coddocumento=u.tipodoc||'-'||u.coddocumento
			where u.usuario=login
			group by u.tipodoc, u.coddocumento, u.usuario, u.clave, u.nombres, u.apellidos, u.fechanac, u.correo, u.codtipousuario, 
			u.activo, u.idate,u.area, area.narea,  u.cargo, u.enlinea, u.idinactivo, m.motivo, tipo.tipousuario
			order by usuario;
			
		else
		return; 
	end case;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spusuarios(numeric, character varying)
  OWNER TO postgres;
