﻿-- Function: spbuscarclienterecibido(character varying, character varying, character varying, character varying, bigint, character varying)

-- DROP FUNCTION spbuscarclienterecibido(character varying, character varying, character varying, character varying, bigint, character varying);

CREATE OR REPLACE FUNCTION spbuscarclienterecibido(IN cliente character varying, IN fecha character varying, IN origen character varying, IN tipousuario character varying, IN ejecutivoin bigint, IN login character varying)
  RETURNS TABLE(enlace text, correlativo bigint, fecharecepcion text, codigobanco integer, ibp character varying, usuario character varying, fechacarga text, namearchivo character varying, sizearchivo character varying, cantidad bigint) AS
$BODY$
	
	Begin
	if (ejecutivoin>0) then	
	
		return 	QUERY 
			select concat_ws('/', p.codigobanco::integer, p.fecharecepcion::date,tipousuario, ejecutivoin, login, origen,p.namearchivo,cliente,fecha) as enlace, 
			row_number()  OVER(order by p.fecharecepcion::date), to_char(p.fecharecepcion::date,'DD/MM/YYYY'), p.codigobanco::integer, b.ibp,
			p.usuario, to_char(p.idate::date,'DD/MM/YYYY'), p.namearchivo, p.sizearchivo, count(p.codigobanco) as cantidad
			from clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			left join tblusuario u on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
			where (u.usuario=login OR p.ejecutivo=login)
			AND
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			AND 
			    CASE WHEN fecha is NULL or fecha='' then p.fecharecepcion IS NOT null
				  else p.fecharecepcion::date=fecha::date END			
				
			group by p.fecharecepcion::date, p.codigobanco::integer,p.namearchivo,b.ibp, p.usuario, p.idate::date,p.sizearchivo
			ORDER BY p.idate::date;
			
	else if  (tipousuario='E' or tipousuario='O') then

			return 	QUERY 
			select concat_ws('/', p.codigobanco::integer, p.fecharecepcion::date,tipousuario, ejecutivoin, login, origen,p.namearchivo,cliente,fecha) as enlace, 
			row_number()  OVER(order by p.fecharecepcion::date), to_char(p.fecharecepcion::date,'DD/MM/YYYY'), p.codigobanco::integer, b.ibp,
			p.usuario, to_char(p.idate::date,'DD/MM/YYYY'), p.namearchivo, p.sizearchivo, count(p.codigobanco) as cantidad
			from clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			WHERE p.ejecutivo=login
			AND 
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			AND 
			    CASE WHEN fecha is NULL or fecha='' then p.fecharecepcion IS NOT null
				  else p.fecharecepcion::date=fecha::date END			
				
			group by p.fecharecepcion::date, p.codigobanco::integer,p.namearchivo,b.ibp, p.usuario, p.idate::date,p.sizearchivo
			ORDER BY p.idate::date;

	else
			return 	QUERY 
			select concat_ws('/', p.codigobanco::integer, p.fecharecepcion::date,tipousuario, ejecutivoin, login, origen,p.namearchivo,cliente,fecha) as enlace, 
			row_number()  OVER(order by p.fecharecepcion::date), to_char(p.fecharecepcion::date,'DD/MM/YYYY'), p.codigobanco::integer, b.ibp,
			p.usuario, to_char(p.idate::date,'DD/MM/YYYY'), p.namearchivo, p.sizearchivo, count(p.codigobanco) as cantidad
			from clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			WHERE 
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			AND 
			    CASE WHEN fecha is NULL or fecha='' then p.fecharecepcion IS NOT null
				  else p.fecharecepcion::date=fecha::date END			
				
			group by p.fecharecepcion::date, p.codigobanco::integer,p.namearchivo,b.ibp, p.usuario, p.idate::date,p.sizearchivo
			ORDER BY p.idate::date;
	end if;
	end if;		
	End;
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarclienterecibido(character varying, character varying, character varying, character varying, bigint, character varying)
  OWNER TO postgres;

--select * from spbuscarclienterecibido('171','','','A','1','JPARRA')
--SELECT * FROM clie_tblclientepotencial