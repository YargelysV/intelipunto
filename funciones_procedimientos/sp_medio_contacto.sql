﻿-- Function: sp_medio_contacto()

-- DROP FUNCTION sp_medio_contacto();

CREATE OR REPLACE FUNCTION sp_medio_contacto()
  RETURNS TABLE(codigomedioc integer, mediocontacto character varying) AS
$BODY$
	Begin
		return 	QUERY
		SELECT * FROM  tbl_medio_contacto;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_medio_contacto()
  OWNER TO postgres;
