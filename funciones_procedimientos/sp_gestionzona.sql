﻿-- Function: sp_gestionzona(numeric, integer, integer, integer, integer, numeric, character varying)

-- DROP FUNCTION sp_gestionzona(numeric, integer, integer, integer, integer, numeric, character varying);

CREATE OR REPLACE FUNCTION sp_gestionzona(operacion numeric, iestados integer, imunicipios integer, iparroquias integer, itipozona integer, ivalor numeric, iusuario character varying)
  RETURNS boolean AS
$BODY$
	Begin
			CASE operacion

				when 2 then
					INSERT INTO  tblgestionmantenimiento 
					values(iestados, imunicipios, iparroquias, itipozona, ivalor, iusuario, now());
					return true;

				when 3 then
						
					update tblgestionmantenimiento set 
					 
					  tipozona = CASE 
							WHEN itipozona IS NULL OR itipozona = 0 THEN tipozona
							ELSE itipozona END, 
					  preciozona=      CASE 
							WHEN ivalor IS NULL OR ivalor = 0 THEN preciozona
							ELSE ivalor END,
											
					usuario=iusuario, fecha=now()
					WHERE	estados=iestados;
					return true;

			end case;	
		End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_gestionzona(numeric, integer, integer, integer, integer, numeric, character varying)
  OWNER TO postgres;
