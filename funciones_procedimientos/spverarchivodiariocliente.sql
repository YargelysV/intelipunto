﻿-- Function: spverarchivodiariocliente(character varying, character varying)

-- DROP FUNCTION spverarchivodiariocliente(character varying, character varying);


CREATE OR REPLACE FUNCTION spverarchivodiariocliente(IN cliente character varying, IN fecha character varying)
  RETURNS TABLE(tipocliente character varying, facturacion character varying, razon character varying, rif character varying, representantelegal character varying, banco integer, nbanco character varying, destipo character varying, calle character varying, municipio character varying, parroquia character varying, urbanizacion character varying, nlocal character varying, estado character varying, ptoref character varying, codepostal character varying, tlf1 character varying, tlf2 character varying, address1 text, address2 text, countrycode character varying, afiliado character varying, fechare text) AS
$BODY$
	Begin
	--select * from tbldirecciones
		return query 
			select '1'::character varying as tipocliente, 'FACTURACION'::character varying as facturacion, p.razonsocial,p.coddocumento, p.rlegal,  p.codigobanco::integer, b.ibp, p.tipocliente,
			 d.d_calle_av, m.municipio, par.parroquia, d.d_urbanizacion, d.d_nlocal, e.estado, d.d_ptoref, d.d_codepostal,
			 p.telf1, p.telf2, m.municipio||' '||par.parroquia||' '||d.d_calle_av||' '||d.d_urbanizacion as address1, 
			 d.d_nlocal||' '||d.d_ptoref as address2,
			 'VE'::character varying as countrycode, p.nafiliacion,(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
			from clie_tblclientepotencial p
			-- SELECT * FROM clie_tblclientepotencial
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			left join tbldirecciones d
			on d.id_afiliado=p.id_consecutivo 
			left join tbltipodireccion td
			on td.codtipo=d.d_codetipo
			left join  tblfacturacioncliente t
			-- SELECT * FROM tblfacturacioncliente
			on t.id_registro=p.id_consecutivo
			left join tblestados e 
			on e.id_estado=d.d_estado::integer
			left join tblmunicipios m 
			on m.id_municipio=d.d_municipio::integer
			left join tblparroquias par 
			on par.id_parroquia=d.d_parroquia::integer
			WHERE 
			
			     CASE WHEN cliente is NULL or cliente='' then p.codigobanco!=''
				  else p.codigobanco::integer=cliente::integer END
			AND 
			    CASE WHEN fecha is NULL or fecha='' then p.fecharecepcion IS NOT null
				  else p.fecharecepcion::date=fecha::date END		
			and p.codestatus in('1','2')   and t.nfactura is null 
			and d.d_codetipo='1'
			order by p.razonsocial;

--select * from tbldirecciones where d_coddocumento='6417175'
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverarchivodiariocliente(character varying, character varying)
  OWNER TO postgres;

--SELECT * FROM spverarchivodiariocliente('116','13-02-2019')