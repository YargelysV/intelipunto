﻿-- Function: speditaractividad(character varying)

-- DROP FUNCTION speditaractividad(character varying);

CREATE OR REPLACE FUNCTION speditaractividad(login character varying)
  RETURNS boolean AS
$BODY$
		
	Begin

		UPDATE tblusuario SET enlinea=0 WHERE usuario=login;

		return true;
		
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION speditaractividad(character varying)
  OWNER TO postgres;
