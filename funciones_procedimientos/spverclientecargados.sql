﻿-- Function: spverclientecargados()

-- DROP FUNCTION spverclientecargados();

CREATE OR REPLACE FUNCTION spverclientecargados(IN banco integer)
  RETURNS TABLE(fechaenvio date, fechacarga date, cantidadtotal bigint, nombrebanco character varying) AS
$BODY$
	
	DECLARE fecha TIMESTAMP WITH TIME ZONE;
	
	Begin

	fecha:=(SELECT CAST(now() AS DATE) - CAST('30 days' AS INTERVAL));
	
				return query 
				select p.fecharecepcion::date, p.idate::date as fechacarga, count(p.codigobanco::integer) as cantidadtotal,
				b.ibp 
				from clie_tblclientepotencial p 
				inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
				where p.codigobanco::integer=banco and p.idate between fecha and now()
				group by  p.fecharecepcion,p.idate::date, p.codigobanco::integer, b.ibp
				order by p.idate::date;
			--select * from tblbanco
			--select * from clie_tblclientepotencial
		
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientecargados(integer)
  OWNER TO postgres;

--select * from spverclientecargados('102')