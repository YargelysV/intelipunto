﻿-- Function: sp_verhorario(character varying, character varying, character varying)

-- DROP FUNCTION sp_verhorario(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_verhorario(iid_cliente character varying, iprefijobanco character varying, ifecha character varying)
  RETURNS setof tblhoras AS
$BODY$
	
	Begin
		return 	QUERY 
			select * from tblhoras where id_cliente=iid_cliente and prefijo_banco=iprefijobanco and fecha_carga=ifecha;
	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_verhorario(character varying, character varying, character varying)
  OWNER TO postgres;
--select sp_verhorario('543543','102','2017-10-20')
delete from tblhoras