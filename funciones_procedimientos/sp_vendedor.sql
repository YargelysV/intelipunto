﻿-- Function: spvervendedor()

-- DROP FUNCTION spvervendedor();

CREATE OR REPLACE FUNCTION spvervendedor()
  RETURNS SETOF adm_vendedor AS
$BODY$
	
	Begin
		return 	QUERY 
			select * from adm_vendedor;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spvervendedor()
  OWNER TO postgres;
