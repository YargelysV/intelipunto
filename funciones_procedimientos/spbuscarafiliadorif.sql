﻿-- Function: spbuscarafiliadorif(character varying)

-- DROP FUNCTION spbuscarafiliadorif(character varying);

CREATE OR REPLACE FUNCTION spbuscarafiliadorif(IN busqueda character varying)
  RETURNS TABLE(enlace text, correlativo bigint, nafiliacion character varying, razonsocial character varying, documento character varying, tipopos character varying, cantterminalesasig integer, tipolinea character varying, ibp character varying, namearchivo character varying, estatus integer, direccion text) AS
$BODY$
    Begin
        return     QUERY --select * from clie_tblclientepotencial
			SELECT concat_ws('/', cp.nafiliacion, cp.namearchivo,cp.fecharecepcion::date,cp.id_consecutivo, cp.coddocumento, cp.codigobanco,busqueda) as enlace, row_number()  OVER(order by correlativo), cp.nafiliacion, cp.razonsocial, cp.coddocumento, cp.tipopos,case when cp.cant_pos_confirm is null then cp.cantterminalesasig else cp.cant_pos_confirm end, 
			cp.tipolinea, bc.ibp, cp.namearchivo ,cp.codestatus, 

			case when insta.direccion is null or  insta.direccion ='' then case WHEN cp.estado='0' then concat_ws(' ', e.estado,m.municipio,par.parroquia) else concat_ws(' ', cp.direccion_instalacion,e.estado,m.municipio,par.parroquia) end else insta.direccion  end FROM clie_tblclientepotencial as cp
			inner join tblbanco bc on cp.codigobanco::integer=bc.codigobanco::integer
			left join (select concat_ws(' ', d.d_calle_av,d.d_urbanizacion,e.estado,m.municipio,par.parroquia) as direccion, d.id_afiliado
			   from tbldirecciones d 
			   left join tblestados e 
 			   on e.id_estado=d.d_estado
 			   left join tblmunicipios m 
			   on m.id_municipio=d.d_municipio
			   left join tblparroquias par 
			   on par.id_parroquia=d.d_parroquia 	
			   where d.d_codetipo='4' group by d.d_urbanizacion,d.d_calle_av, d.id_afiliado, e.estado, m.municipio,par.parroquia) insta
		on insta.id_afiliado=cp.id_consecutivo
		left join tblestados e 
		on e.id_estado=cp.estado::integer
		left join tblmunicipios m 
		on m.id_municipio=cp.municipio::integer
		left join tblparroquias par 
		on par.id_parroquia=cp.parroquia::integer
WHERE cp.codestatus in ('1','2') and 
(cp.nafiliacion LIKE '%'||busqueda||'%' or cp.razonsocial LIKE '%'||busqueda||'%' or cp.coddocumento LIKE '%'||busqueda||'%') and cp.nafiliacion<>''; 
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarafiliadorif(character varying)
  OWNER TO postgres;
