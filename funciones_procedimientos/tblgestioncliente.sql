﻿

CREATE TABLE tblgestioncliente
(
  g_codegestion integer NOT NULL DEFAULT nextval('codegestion_id'::regclass),
  g_nafiliado character varying(50),
  g_estatuspos integer,
  g_fechacontacto timestamp with time zone NOT NULL DEFAULT now(),
  g_observacion character varying(200),
  g_codejecutivo integer NOT NULL,
  g_coddocumento character varying(50) NOT NULL,
  CONSTRAINT pk_tblgestioncliente PRIMARY KEY (g_coddocumento, g_codegestion)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tblgestioncliente
  OWNER TO postgres;
COMMENT ON TABLE tblgestioncliente
  IS 'control de gestion de clientes, existe N estatus la gestion se debe cerrar cuando el estatus es: VENTA CON FONDO';
