﻿-- Function: sprestaurararchivocliente(integer, timestamp with time zone, character varying)

-- DROP FUNCTION sprestaurararchivocliente(integer, timestamp with time zone, character varying);

CREATE OR REPLACE FUNCTION sprestaurararchivocliente(banco integer, fecha timestamp with time zone, login character varying)
  RETURNS boolean AS
$BODY$

	--select * from clie_auditoriaarchivoeliminar
	Begin
			insert into 
					clie_tblclientepotencial (id_consecutivo, fecharecepcion, codigobanco, tipopos, tipolinea, tipomodelonegocio,
					tipocliente, nafiliacion, cantterminalesasig, razonsocial, coddocumento, acteconomica, rlegal, cirl, rifrl, correorl,
					telfrl, telf1, telf2, ncuentapos, direccion_instalacion, estado, municipio, parroquia ,
					usuario, idate, namearchivo, sizearchivo, codestatus, agencia, ejecutivo)
			select 
					id_consecutivo, fecharecepcion, codigobanco, tipopos, tipolinea, tipomodelonegocio,
					tipocliente, nafiliacion, cantterminalesasig, razonsocial, coddocumento, acteconomica, rlegal, cirl, rifrl, correorl,
					telfrl, telf1, telf2, ncuentapos, direccion_instalacion, estado, municipio, parroquia, 
					usuario, idate, namearchivo, sizearchivo, codestatus, agencia, ejecutivo
			from 
					clie_auditoriaarchivoeliminar where codigobanco::integer=banco::integer
					and fecharecepcion::date=fecha;

			update clie_tblclientepotencial set usuario=login 		
			where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;
			
			update clie_auditoriaarchivoeliminar set usuariorestaura=login, idaterestaura=now() 		
			where codigobanco::integer=banco::integer and fecharecepcion::date=fecha;

			return true;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sprestaurararchivocliente(integer, timestamp with time zone, character varying)
  OWNER TO postgres;
