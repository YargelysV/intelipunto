﻿-- Function: spverarchivocargado(character varying)

-- DROP FUNCTION spverarchivocargado(character varying);

CREATE OR REPLACE FUNCTION spverarchivocargado(name character varying)
  RETURNS SETOF clie_tblclientepotencial AS
$BODY$
	
	Begin
		
		return 	QUERY SELECT * FROM clie_tblclientepotencial where namearchivo=name;
	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverarchivocargado(character varying)
  OWNER TO postgres;
