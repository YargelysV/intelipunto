﻿-- Function: sp_verestado()

-- DROP FUNCTION sp_verestado();

CREATE OR REPLACE FUNCTION sp_verestado()
  RETURNS SETOF tblestados AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblestados order by id_estado asc;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_verestado()
  OWNER TO postgres;
