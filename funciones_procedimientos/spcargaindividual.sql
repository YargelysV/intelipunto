﻿-- Function: spcargaindividual(numeric, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION spcargaindividual(numeric, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION spcargaindividual(operacion numeric, nafiliacion character varying, irazonsocial character varying, cdocumento character varying, nterminal character varying,
 serial character varying, bancoorigen character varying, itipo character varying, imarcapos character varying, login character varying)
  RETURNS boolean AS
$BODY$
	Begin
			CASE operacion		--  select * from tblmantenimientonuevo

				when 2 then
					INSERT INTO tblmantenimientonuevo 
					values(nafiliacion, irazonsocial, cdocumento, nterminal, serial, bancoorigen, itipo, imarcapos, login, now());
					return true;

				
				when 3 then
						
					update tblmantenimientonuevo set 

					  nroafiliacion = CASE 
							WHEN nafiliacion IS NULL OR nafiliacion = '' THEN nroafiliacion
							ELSE nafiliacion END, 
					 
					  razonsocial = CASE 
							WHEN irazonsocial IS NULL OR irazonsocial = '' THEN razonsocial
							ELSE irazonsocial END, 

					  documento = CASE 
							WHEN cdocumento IS NULL OR cdocumento = '' THEN documento
							ELSE cdocumento END, 

					numterminal = CASE 
							WHEN nterminal IS NULL OR nterminal = '' THEN numterminal
							ELSE nterminal END, 		
									
					  serialpos=      CASE 
							WHEN serial IS NULL OR serial = '' THEN serialpos
							ELSE serial END,
							
					 codigobanco=     CASE 
							WHEN bancoorigen IS NULL OR bancoorigen = '' THEN codigobanco
							ELSE bancoorigen END,
					tipopos=     CASE 
							WHEN itipo IS NULL OR itipo = '' THEN tipopos
							ELSE itipo END,	
					marcapos=     CASE 
							WHEN imarcapos IS NULL OR imarcapos = '' THEN marcapos
							ELSE imarcapos END,							
					usuario=login
					WHERE	numterminal=nterminal;
					return true;	
					-- select * from spcargaindividual ('2','2004','comerciosss','12121225','202','j859oooo5','151','2','2','YQUIJADA')

				end case;	
		End;
--select * from tblmantenimientonuevo
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spcargaindividual(numeric, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
