﻿-- Function: sp_insertargestion(character varying, integer, integer, character varying, character varying, character varying)

-- DROP FUNCTION sp_insertargestion(character varying, integer, integer, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_insertargestion(inafiliado character varying, icodejecutivo integer, iestatuspos integer, iobservacion character varying, icoddocumento character varying, inombrearchivo character varying, isuario character varying)
  RETURNS boolean AS
$BODY$
	--   SELECT * FROM clie_tblclientepotencial
	--    select * from tblgestioncliente
	--    select * from  tbldirecciones
	
	Begin
					INSERT INTO tblgestioncliente(g_nafiliado,g_codejecutivo,g_estatuspos,g_fechacontacto,g_observacion,g_coddocumento,g_nombrearchivo)
					values(inafiliado, icodejecutivo, iestatuspos, now(), iobservacion,icoddocumento,inombrearchivo, d_usuariocarga=iusuario,d_fechaactualizacion=now());

					UPDATE clie_tblclientepotencial SET codestatus=iestatuspos, usuarioedita=iusuario,fechaedita=now() where coddocumento=icoddocumento and namearchivo=inombrearchivo;
					return true;
					
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_insertargestion(character varying, integer, integer, character varying, character varying, character varying)
  OWNER TO postgres;
