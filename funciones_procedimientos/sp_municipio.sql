﻿-- Function: sp_municipio(integer)

-- DROP FUNCTION sp_municipio(integer);

CREATE OR REPLACE FUNCTION sp_municipio(iid_estado integer)
  RETURNS SETOF tblmunicipios AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblmunicipios where id_estado=iid_estado order by municipio asc;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_municipio(integer)
  OWNER TO postgres;
