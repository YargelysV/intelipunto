﻿-- Function: spusuarioia(numeric, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character, integer, character varying, integer, character varying)

-- DROP FUNCTION spusuarioia(numeric, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character, integer, character varying, integer, character varying);

CREATE OR REPLACE FUNCTION spusuarioia(operacion numeric, tipodocn character varying, coddocumenton character varying, login character varying, claven character varying, nombresn character varying, apellidosn character varying, fechanacn character varying, correon character varying, codtipousuarion character, activon integer, cargon character varying, arean integer, usuarion character varying)
  RETURNS boolean AS
$BODY$
	
	DECLARE result int;
	DECLARE resultvendedor int; 
	DECLARE codvendedornew int; 
	--select * from adm_ejecutivo
	--select * from tblUsuario
	Begin		

			CASE operacion
				when 3 then

					result:=(select count(codvendedor) from adm_ejecutivo where nvendedor=login);
					codvendedornew:=(select (max(codvendedor)+1) from adm_ejecutivo);

					if (result>0) then
						INSERT INTO tblUsuario (tipodoc, coddocumento, usuario, clave, nombres, apellidos, fechanac, correo, codtipousuario, activo, idate, area, cargo, login_user)
						values(tipodocn, coddocumenton, login, claven, nombresn, apellidosn, now(), correon, codtipousuarion, activon, now(), arean, cargon, usuarion);
						return true;
						
					else if ((result=0) and ((codtipousuarion='O') OR (codtipousuarion='E'))) then
							INSERT INTO tblUsuario (tipodoc, coddocumento, usuario, clave, nombres, apellidos, fechanac, correo, codtipousuario, activo, idate, area, cargo, login_user)
							values(tipodocn, coddocumenton, login, claven, nombresn, apellidosn, now(), correon, codtipousuarion, activon, now(), arean, cargon, usuarion);
							
							INSERT INTO adm_ejecutivo
							values (codvendedornew, login, tipodocn||'-'||coddocumenton, nombresn, apellidosn,usuarion, now(), 1);
							return true;	
						else if ((result=0) and ((codtipousuarion!='O') and (codtipousuarion!='E'))) then
							INSERT INTO tblUsuario (tipodoc, coddocumento, usuario, clave, nombres, apellidos, fechanac, correo, codtipousuario, activo, idate, area, cargo, login_user)
							values(tipodocn, coddocumenton, login, claven, nombresn, apellidosn, now(), correon, codtipousuarion, activon, now(), arean, cargon, usuarion);
							return true;
							end if;
						end if;

					end if;
				

				when 4 then
					result:=(select count(Usuario) from tblUsuario where usuario=login);
					resultvendedor:=(select count(codvendedor) from adm_ejecutivo where nvendedor=login);

					if (result>0) then
						
						if (codtipousuarion='L') then
							DELETE FROM tblpermisosmodulos WHERE usuario=login and codmodulo='GE';
						end if;
						
						if (resultvendedor>0) then 
							update adm_ejecutivo set 
							nombres=nombresn, apellidos=apellidosn, 
							idate=now(),
							usuario=usuarion
							WHERE	nvendedor=login;
						end if;

						update tblUsuario set 
						Nombres=nombresn, Apellidos=apellidosn, 
						correo=correon,
						codTipoUsuario=codtipousuarion, activo=activon, idate=now(),
						area=arean, cargo=cargon, idinactivo=null, login_user=usuarion
						WHERE	usuario=login;
						return true;
					else
					return false;
					end if;
	
				else 
				return false;
			end case;	
		End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spusuarioia(numeric, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character, integer, character varying, integer, character varying)
  OWNER TO postgres;
