﻿-- Function: sp_reportediarioxbanco(integer)

-- DROP FUNCTION sp_reportediarioxbanco(integer);

CREATE OR REPLACE FUNCTION sp_reportediarioxbanco(IN banco integer)
  RETURNS TABLE(correlativo bigint, ibp character varying, carteratotal bigint, modelo character varying, estatusventa bigint, estatusfacturacion integer, cantfacturados bigint, montousd numeric, montobs numeric) AS
$BODY$

DECLARE tipoposnodefinido character varying;		
DECLARE bancobsq integer;
DECLARE cant bigint;
	Begin		
		
	tipoposnodefinido='Sin definir';
	
			return query 
			
	                       -- select * from clie_tblclientepotencial where   codigobanco::integer=116
	                	-- select sum(cantterminalesasig) from clie_tblclientepotencial where codigobanco::integer=116 as carteratotal
	                	-- select count(tf.estatus ) where
			SELECT  row_number () OVER (ORDER BY b.ibp), b.ibp,  (select sum(cantterminalesasig) from clie_tblclientepotencial where codigobanco::integer=banco) as carteratotal ,case when tr.tipopos  is null or tr.tipopos ='' then tipoposnodefinido else tr.tipopos  end as modelo, 
		        count(tr.codestatus) as estatusventa,tf.estatus as estatusfacturacion, case when tf.estatus=1 then count(tf.estatus ) else 0 end as cantfacturados,
			sum(pagos.montodolar) as montousd, sum(pagos.montobs) as montobss  
			FROM clie_tblclientepotencial tr 
			inner join tblbanco b on 
			b.codigobanco::integer=tr.codigobanco::integer
			left join tblfacturacioncliente tf 
			on
			tr.id_consecutivo=tf.id_registro
			left join (select p.p_idafiliado,sum(p.p_montousd::integer) as montodolar ,
				          sum(case when p.p_fconversion='0' or p.p_fconversion='' then p.p_monto::integer  else 0 end) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				on pagos.p_idafiliado=tf.id_registro and tf.estatus='1'
		
			WHERE 
--                         tr.codigobanco::integer<>0 and tr.codestatus=1
--                        	group by  b.ibp, tr.codigobanco,tr.tipopos,tr.codestatus ,tf.estatus
-- 			order by tr.codigobanco desc;
			
			
			
			CASE
			WHEN banco<>0 THEN 
			 tr.codigobanco::integer=banco and tr.codestatus=1
		        ELSE
		   	  tr.codigobanco::integer<>0 and  tr.codestatus=1   			
		        END 
				group by  b.ibp, tr.codigobanco,tr.tipopos,tr.codestatus ,tf.estatus
				order by tr.codigobanco desc;
			-- order by tr.codigobanco,carteratotal,tr.tipopos desc;

			 --   select * from tblestatussolpos
	                 --   select * from tblgestioncliente
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_reportediarioxbanco(integer)
  OWNER TO postgres;
