﻿-- Function: sptecnicos(numeric, integer, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sptecnicos(numeric, integer, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sptecnicos(operacion numeric, codigotecnico integer, trabajo character varying, tipodoc character varying, documento character varying, nombre character varying, apellido character varying, login character varying)
  RETURNS boolean AS
$BODY$
	Begin
			CASE operacion

				when 2 then
					INSERT INTO tbltecnicos 
					values(codigotecnico, tipodoc||'-'||documento, nombre, apellido, trabajo, login, now());
					return true;

				when 3 then
						
					update tbltecnicos set 
					 
					  codtecnicos = CASE 
							WHEN codigotecnico IS NULL OR codigotecnico = 0 THEN codtecnicos
							ELSE codigotecnico END, 
					  nombres=      CASE 
							WHEN nombre IS NULL OR nombre = '' THEN nombres
							ELSE nombre END,
					 apellidos=     CASE 
							WHEN apellido IS NULL OR apellido = '' THEN apellidos
							ELSE apellido END,
					zonatrabajo=     CASE 
							WHEN trabajo IS NULL OR trabajo = '' THEN zonatrabajo
							ELSE trabajo END,						
					usuario=login, idate=now()
					WHERE	substr(coddocumento,strpos(coddocumento,'-')+1)=documento;
					return true;

				when 4 then
					Delete from tbltecnicos WHERE substr(coddocumento,strpos(coddocumento,'-')+1)=documento;
					return true;
					
				else 
				return false;
			end case;	
		End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sptecnicos(numeric, integer, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
