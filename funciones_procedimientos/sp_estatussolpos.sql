﻿-- Function: sp_estatussolpos()

-- DROP FUNCTION sp_estatussolpos();

CREATE OR REPLACE FUNCTION sp_estatussolpos(operacion numeric)
  RETURNS SETOF tblestatussolpos AS
$BODY$
	
	Begin
	CASE operacion

	when 1 then
		return 	QUERY SELECT * FROM tblestatussolpos where codestatus!=4;
        when 2 then
        return 	QUERY SELECT * FROM tblestatussolpos;

        else 
        return;
        
        end case;		
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_estatussolpos()
  OWNER TO postgres;
