﻿-- Function: sp_verestatusgestion(character varying, character varying)

-- DROP FUNCTION sp_verestatusgestion(character varying, character varying);

CREATE OR REPLACE FUNCTION sp_verestatusgestion(IN icoddocumento character varying, IN inamearchivo character varying)
  RETURNS TABLE(correlativo bigint, g_nafiliado character varying, g_estatupos integer, fechagestion text, g_observacion character varying, g_coddocumento character varying, g_nombrearchivo character varying, g_usuariocarga character varying, g_fechacarga character varying, g_codejecutivobanco character varying, g_codejecutivointeligensa character varying, codestatus integer, descestatus character varying, codvendedor1 integer, nvendedor1 character varying, coddocumento1 character varying, nombres1 character varying, apellidos1 character varying, ocodejecutivo1 integer, codvendedor2 integer, nvendedor2 character varying, coddocumento2 character varying, nombres2 character varying, apellidos2 character varying, ocodejecutivo2 integer) AS
$BODY$
	
	Begin	
		return 	QUERY 
			select  row_number()  OVER(order by tg.g_codegestion) ,tg.g_nafiliado,tg.g_estatuspos,to_char(tg.g_fechacontacto::date,'DD/MM/YYYY'),tg.g_observacion,tg.g_coddocumento,tg.g_nombrearchivo,tg.g_usuariocarga,
			 tg.g_fechacarga, tg.g_codejecutivobanco,tg.g_codejecutivointeligensa,te.codestatus,te.descestatus,
			tv.codvendedor, tv.nvendedor,tv.coddocumento,tv.nombres,tv.apellidos, tv.codejecutivos,ti.codvendedor, ti.nvendedor,ti.coddocumento,ti.nombres,ti.apellidos, ti.codejecutivos from tblgestioncliente tg
			inner join tblestatussolpos te 
			on te.codestatus=tg.g_estatuspos
			left join adm_ejecutivo tv
			on tv.coddocumento=tg.g_codejecutivobanco
			left join adm_ejecutivo ti
			on
			ti.coddocumento=tg.g_codejecutivointeligensa 
			-- where tg.g_coddocumento='123456' and tg.g_nombrearchivo='BANCOInteligensa_25102017_1.csv';
			where tg.g_coddocumento=icoddocumento and tg.g_nombrearchivo=inamearchivo
			ORDER BY tg.g_fechacontacto ASC;
			End;	

			-- select * from tblgestioncliente where g_coddocumento='123456'
-- 			delete from tblgestioncliente where g_coddocumento='123456'
-- 			select * from clie_tblclientepotencial where coddocumento='123456'
-- 			update clie_tblclientepotencial set interes='' where coddocumento='123456'
-- 			update 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_verestatusgestion(character varying, character varying)
  OWNER TO postgres;
