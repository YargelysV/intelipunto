﻿-- Function: sp_tipolinea(character varying)

-- DROP FUNCTION sp_tipolinea(character varying);

CREATE OR REPLACE FUNCTION sp_tipolinea(IN itipopos character varying)
  RETURNS TABLE(tpcodetipo integer, tptipopos character varying, trcodtipopos integer, trcodtipolinea integer) AS
$BODY$

  DECLARE codetipo integer;

	Begin
	return	QUERY 
	SELECT * FROM tbltipopos as tp
	inner join tblrelineapos tr
	on 
	tr.codtipopos=tp.codtipo 
	where tp.tipopos=itipopos;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_tipolinea(character varying)
  OWNER TO postgres;
