select extract(year from fecharecepcion), ibp,count(*) as cantidad
 from clie_tblclientepotencial c
inner join tblbanco b on b.codigobanco::integer= c.codigobanco::integer
 where extract(year from fecharecepcion)='2018'
 group by extract(year from fecharecepcion), ibp
 order by 1,2


--para trabajar con crosstab debe agregarse la siguiente extensión
--CREATE EXTENSION tablefunc;

 SELECT * FROM crosstab(
   'select extract(year from c.fecharecepcion)::text, b.ibp,count(*)::INTEGER as cantidad
 from clie_tblclientepotencial c
inner join tblbanco b on b.codigobanco::integer= c.codigobanco::integer
 group by extract(year from c.fecharecepcion), b.ibp  order by 1,2',
'select ibp::text from tblbanco order by 1'
   ) 
    AS ct ("año" text, 
"100% Banco, Banco Universal C.A." integer,
"Bancamica, Banco Micro financiero C.A." integer,
"Bancaribe C.A. Banco Universal" integer,
"Banco Activo, Banco Universal" integer,
"Banco Agrícola de Venezuela, C.A. Banco Universal" integer,
"Banco Bicentenario del Pueblo de la Clase Obrera, Mujer y Comunas B.U." integer,
"Banco Caroni C.A. Banco Universal" integer,
"Banco de Venezuela S.A.C.A. Banco Universal" integer,
"Banco del Tesoro, C.A. Banco Universal" integer,
"Banco Exterior C.A. Banco Universal" integer,
"Banco Nacional de Credito, C.A Banco Universal" integer,
"Banco Occidental de Descuento, Banco Universal C.A" integer,
"Banco Plaza, Banco Universal" integer,
"Bancrecer, S.A. Banco Micro financiero" integer,
"Banplus Banco Universal, C.A" integer,
"BBVA Provincial" integer,
"BFC Banco Fondo Común C.A. Banco Universal" integer,
"Citibank N.A." integer,
"Del Sur Banco Universal C.A." integer,
"Mi Banco, Banco Micro financiero C.A." integer,
"Novo Banco, S.A. Sucursal Venezuela Banco Universal" integer
)





