﻿-- Function: spverfechas(integer)

-- DROP FUNCTION spverfechas(integer);

CREATE OR REPLACE FUNCTION spverfechas(IN valor integer)
  RETURNS TABLE(fecha text, anio double precision, mesdia text) AS
$BODY$
	
	Begin

		CASE valor
			when 1 then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				group by fecha, p.fecharecepcion
				order by fecharecepcion;
			when 3 then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				where p.codestatus in ('1','2')
				group by fecha, p.fecharecepcion
				order by fecharecepcion;
				
			when 4 then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				group by fecha, p.fecharecepcion
				order by fecharecepcion;
				
			when 5 then
				return query 
				select  
				(TO_CHAR(p.fechaenviocomercializacion,'DD')||'-'||TO_CHAR(p.fechaenviocomercializacion,'MM')||'-'||(extract( year from p.fechaenviocomercializacion))::character varying) as fecha
				,extract( year from p.fechaenviocomercializacion) AS anho, TO_CHAR(p.fechaenviocomercializacion,'MM')||(TO_CHAR(p.fechaenviocomercializacion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				where p.codestatus in ('1','2','4') and  p.fechaenviocomercializacion is not null
				group by fecha, p.fechaenviocomercializacion
				order by p.fechaenviocomercializacion;
                      
			when 6 then
-- modulo facturacion
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from
					clie_tblclientepotencial p
				left join  tblfacturacioncliente t
					on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo 
				where 
			-- 	select * from tblfacturacioncliente
					p.codestatus in('1','2')  and  t.nfactura is null and t.ntransferencia<>'' 
				group by fecha, p.fecharecepcion
				order by fecharecepcion;

			when 7 then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				where p.codestatus in ('1','2') and p.nafiliacion<>''
				group by fecha, p.fecharecepcion
				order by fecharecepcion;

			when 8 then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from
					clie_tblclientepotencial p
				left join  tblfacturacioncliente t
					on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo
				where 
					p.codestatus in('1','2')  and  t.nfactura is not null
				group by fecha, p.fecharecepcion
				order by fecharecepcion;	

			when 9 then
				return query 
				select  
				(TO_CHAR(g.fechainstalacion,'DD')||'-'||TO_CHAR(g.fechainstalacion,'MM')||'-'||(extract( year from g.fechainstalacion))::character varying) as fecha
				,extract( year from g.fechainstalacion) AS anho, TO_CHAR(g.fechainstalacion,'MM')||(TO_CHAR(g.fechainstalacion,'DD')) as mes_dia
				from
				 clie_tblclientepotencial p
				inner join tblgestionserviciotecnico g 
				on g.nroafiliado=p.nafiliacion	
				left join  tblfacturacioncliente t
					on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo
				where 
					p.codestatus in('1','2') and g.estatus='4' and  t.nfactura is not null
				group by fecha, g.fechainstalacion
				order by g.fechainstalacion;

			when 12 then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				inner join tblinventariopos i on i.nroafiliacion=p.nafiliacion
				where p.codestatus in ('1','2') and p.nafiliacion<>''
				group by fecha, p.fecharecepcion
				order by fecharecepcion;
			else
			return; 
		end case;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverfechas(integer)
  OWNER TO postgres;