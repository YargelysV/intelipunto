﻿-- Function: sp_usuariologin(character varying)

-- DROP FUNCTION sp_usuariologin(character varying);

CREATE OR REPLACE FUNCTION sp_usuariologin(usuariologin character varying)
  RETURNS boolean AS
$BODY$
--select * from sp_usuariologin('JUNIORS') select * from tbllogin
declare existe_usuario int;
Begin
			--return query
			
			existe_usuario:=(select count(ejecutivo) from tbllogin where ejecutivo=usuariologin);
			--si existe_usuario es igual a 1 existe.
			--si existe_usuario es igual a 0 no existe.

			if (existe_usuario=0) then
			INSERT INTO tbllogin (ejecutivo, fecha1,conexiones)
			VALUES (usuariologin, now(),'1');
			return true;
				--VALUES ('JUVENAL', '11/05/2019','1');

			--UPDATE tbllogin SET fecha3=fecha2, fecha2=fecha1, fecha1='30/12/2020', conexiones=conexiones+1
			--WHERE ejecutivo = 'JUVENAL';
			elsif (existe_usuario=1) then
			UPDATE tbllogin SET fecha3=fecha2, fecha2=fecha1, fecha1=now(), conexiones=conexiones+1
			WHERE ejecutivo = usuariologin;
			return true;
end if;				
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_usuariologin(character varying)
  OWNER TO postgres;
