﻿-- Function: sp_ejecutivoxbanco(integer)

-- DROP FUNCTION sp_ejecutivoxbanco(integer);

CREATE OR REPLACE FUNCTION sp_ejecutivoxbanco(IN banco integer)
  RETURNS TABLE(codigobanco integer, ibp character varying, ejecutivo character varying) AS
$BODY$
	
	Begin						--  select * from sp_ejecutivoxbanco( '116')

		
			

			
				return query 
				
					select p.codigobanco::integer, b.ibp, p.ejecutivo
					from clie_tblclientepotencial p
					inner join tblbanco b 
					on b.codigobanco::integer=p.codigobanco::integer
					inner join adm_ejecutivo a 
					on a.nvendedor=p.ejecutivo
					where p.codigobanco::integer=banco::integer
					group by p.codigobanco::integer, b.ibp,p.ejecutivo
				
				order by p.ejecutivo;
		
			
		
	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_ejecutivoxbanco(integer)
  OWNER TO postgres;
