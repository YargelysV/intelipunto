﻿-- Function: spverfechaxbanco(integer, integer)

-- DROP FUNCTION spverfechaxbanco(integer, integer);

CREATE OR REPLACE FUNCTION spverfechaxbanco(IN valor integer, IN banco integer)
  RETURNS TABLE(codigobanco integer, ibp character varying, fecha text, anho double precision, mesdia text) AS
$BODY$
	
	Begin

		CASE valor
			when 1 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					where p.codigobanco::integer=banco::integer
					group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecha) as resul 
					group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;

			when 3 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					where p.codigobanco::integer=banco::integer
					and p.codestatus in ('1','2')
					group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecha) as resul 
					group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;
					
			when 4 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					where p.codigobanco::integer=banco::integer
					group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecha) as resul 
				group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;

			when 5 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fechaenviocomercializacion,'DD')||'-'||TO_CHAR(p.fechaenviocomercializacion,'MM')||'-'||(extract( year from p.fechaenviocomercializacion))::character varying) as fecha
					,extract( year from p.fechaenviocomercializacion) AS anho, TO_CHAR(p.fechaenviocomercializacion,'MM')||(TO_CHAR(p.fechaenviocomercializacion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					where p.codigobanco::integer=banco::integer
					and p.codestatus in ('1','2','4') and  p.fechaenviocomercializacion is not null
					group by p.codigobanco::integer, b.ibp, fecha, p.fechaenviocomercializacion
					order by fechaenviocomercializacion) as resul 
					group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;
					
			when 6 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select  p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from
						clie_tblclientepotencial p
					inner join tblbanco b 
						on b.codigobanco::integer=p.codigobanco::integer
					left join  tblfacturacioncliente t
						on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo
					where
						p.codigobanco::integer=banco::integer 
						and p.codestatus in('1','2')  and  t.nfactura is null
					group by  p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecharecepcion)as resul
				group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;	
			
			when 7 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					where p.codigobanco::integer=banco::integer
					and p.codestatus in ('1','2') and p.nafiliacion<>''
					group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecha) as resul 
					group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;

			when 8 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select  p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from
						clie_tblclientepotencial p
					inner join tblbanco b 
						on b.codigobanco::integer=p.codigobanco::integer
					left join  tblfacturacioncliente t
						on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo
					where
						p.codigobanco::integer=banco::integer 
						and p.codestatus in('1','2')  and  t.nfactura is not null
					group by  p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecharecepcion)as resul
				group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;	

			when 9 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select  p.codigobanco::integer, b.ibp, 
					(TO_CHAR(g.fechainstalacion,'DD')||'-'||TO_CHAR(g.fechainstalacion,'MM')||'-'||(extract( year from g.fechainstalacion))::character varying) as fecha
					,extract( year from g.fechainstalacion) AS anho, TO_CHAR(g.fechainstalacion,'MM')||(TO_CHAR(g.fechainstalacion,'DD')) as mes_dia
					from
						clie_tblclientepotencial p
					inner join tblbanco b 
						on b.codigobanco::integer=p.codigobanco::integer
					inner join tblgestionserviciotecnico g
						on g.nroafiliado=p.nafiliacion		
					left join  tblfacturacioncliente t
						on t.coddocumento=p.coddocumento and t.clientebanco::integer=p.codigobanco::integer and t.namearchivo=p.namearchivo
					where
						p.codigobanco::integer=banco::integer 
						and p.codestatus in('1','2') and g.estatus='4' and  t.nfactura is not null
					group by  p.codigobanco::integer, b.ibp, fecha, g.fechainstalacion
					order by fechainstalacion)as resul
				group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;	

			when 12 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					inner join tblinventariopos i on i.nroafiliacion=p.nafiliacion
					where p.codigobanco::integer=banco::integer
					and p.codestatus in ('1','2') and p.nafiliacion<>''
					group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecha) as resul 
					group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;	
			else
			return; 
		end case;
	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverfechaxbanco(integer, integer)
  OWNER TO postgres;