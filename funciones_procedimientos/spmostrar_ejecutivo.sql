﻿-- Function: spmostrarejecutivo()

-- DROP FUNCTION spmostrarejecutivo();

CREATE OR REPLACE FUNCTION spmostrarejecutivo()
  RETURNS TABLE(correlativo bigint, vendedor integer, aliasvendedor character varying, tipodoc text,
   documento text, nombre character varying, apellido character varying, usuer character varying, fecha timestamp with time zone) AS
$BODY$

	Begin	
		return query select row_number()  OVER(order by codvendedor), codvendedor, nvendedor, substr(coddocumento,1,strpos(coddocumento,'-') -1) as tipodoc,
             substr(coddocumento,strpos(coddocumento,'-')+1) as documento, nombres, apellidos,usuario,idate from adm_ejecutivo 
		ORDER BY codvendedor;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrarejecutivo()
  OWNER TO postgres;

