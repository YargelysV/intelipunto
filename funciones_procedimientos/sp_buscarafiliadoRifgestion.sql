﻿-- Function: sp_buscarafiliadorifgestion(character varying)

-- DROP FUNCTION sp_buscarafiliadorifgestion(character varying);

CREATE OR REPLACE FUNCTION sp_buscarafiliadorifgestion(IN busqueda character varying)
  RETURNS TABLE(enlace text, idcorrelativo bigint, nafiliacion character varying, razonsocial character varying, documento character varying, tipopos character varying, cantterminalesasig integer, tipolinea character varying, ibp character varying, namearchivo character varying, estatus integer, direccion text) AS
$BODY$					--select * from sp_buscarafiliadoRifgestion('860287283')    select * from tblinventariopos
    Begin
        return     QUERY --select * from clie_tblclientepotencial      select * from


						
			SELECT concat_ws('/', i.serialpos,cp.nafiliacion, cp.namearchivo,cp.fecharecepcion::date,i.numterminal,i.id_cliente ,busqueda) as enlace, row_number()  OVER(order by idcorrelativo), 
			cp.nafiliacion, cp.razonsocial, cp.coddocumento, cp.tipopos,case when cp.cant_pos_confirm is null then cp.cantterminalesasig else cp.cant_pos_confirm end, 
			cp.tipolinea, bc.ibp, cp.namearchivo ,cp.codestatus, 

			case when insta.direccion is null or  insta.direccion ='' then case WHEN cp.estado='0' then concat_ws(' ', e.estado,m.municipio,par.parroquia) else concat_ws(' ', cp.direccion_instalacion,e.estado,m.municipio,par.parroquia) end else insta.direccion  end FROM clie_tblclientepotencial as cp
			inner join tblbanco bc on cp.codigobanco::integer=bc.codigobanco::integer
			left join (select concat_ws(' ', d.d_calle_av,d.d_urbanizacion,e.estado,m.municipio,par.parroquia) as direccion, d.id_afiliado
			   from tbldirecciones d 
			   left join tblestados e 
 			   on e.id_estado=d.d_estado
 			   left join tblmunicipios m 
			   on m.id_municipio=d.d_municipio
			   left join tblparroquias par 
			   on par.id_parroquia=d.d_parroquia 
			   where d.d_codetipo='4' group by d.d_urbanizacion,d.d_calle_av, d.id_afiliado, e.estado, m.municipio,par.parroquia) insta
		on insta.id_afiliado=cp.id_consecutivo
		left join tblestados e 
		on e.id_estado=cp.estado::integer
		left join tblmunicipios m 
		on m.id_municipio=cp.municipio::integer
		left join tblparroquias par 
		on par.id_parroquia=cp.parroquia::integer
		full outer join tblinventariopos i on i.nroafiliacion=cp.nafiliacion  and i.id_cliente::integer=cp.id_consecutivo
WHERE cp.codestatus in ('1','2') and 
(cp.nafiliacion LIKE '%'||busqueda||'%' or cp.razonsocial LIKE '%'||busqueda||'%' or cp.coddocumento LIKE '%'||busqueda||'%') and cp.nafiliacion<>'';
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_buscarafiliadorifgestion(character varying)
  OWNER TO postgres;


--select * from sp_buscarafiliadorifgestion('860287283')

--select * from tblinventariopos where nroafiliacion='"860287283"'


/*		
			SELECT concat_ws('/', i.serialpos,cp.nafiliacion, cp.namearchivo,cp.fecharecepcion::date,busqueda,i.numterminal) as enlace, row_number()  
			OVER(order by idcorrelativo), 

select cp.nafiliacion, cp.razonsocial, cp.coddocumento, cp.tipopos,case when cp.cant_pos_confirm is null then cp.cantterminalesasig else cp.cant_pos_confirm end, 
cp.tipolinea, bc.ibp, cp.namearchivo ,cp.codestatus, 
case when insta.direccion is null or  insta.direccion ='' then case WHEN cp.estado='0' then concat_ws(' ', e.estado,m.municipio,par.parroquia) else concat_ws(' ', cp.direccion_instalacion,e.estado,m.municipio,par.parroquia) end else insta.direccion  end 
FROM clie_tblclientepotencial as cp
inner join tblbanco bc on cp.codigobanco::integer=bc.codigobanco::integer
	left join (select concat_ws(' ', d.d_calle_av,d.d_urbanizacion,e.estado,m.municipio,par.parroquia) as direccion, d.id_afiliado
	from tbldirecciones d 
	left join tblestados e 
	on e.id_estado=d.d_estado
	left join tblmunicipios m 
	on m.id_municipio=d.d_municipio
	left join tblparroquias par 
	on par.id_parroquia=d.d_parroquia 
	where d.d_codetipo='4' group by d.d_urbanizacion,d.d_calle_av, d.id_afiliado, e.estado, m.municipio,par.parroquia) insta
	on insta.id_afiliado=cp.id_consecutivo
left join tblestados e 
on e.id_estado=cp.estado::integer
left join tblmunicipios m 
on m.id_municipio=cp.municipio::integer
left join tblparroquias par 
on par.id_parroquia=cp.parroquia::integer
full outer join tblinventariopos i on i.nroafiliacion=cp.nafiliacion
WHERE cp.codestatus in ('1') and 
(cp.nafiliacion LIKE '%'||'860287283'||'%' or cp.razonsocial LIKE '%'||'860287283'||'%' or cp.coddocumento LIKE '%'||'860287283'||'%') and cp.nafiliacion<>''
group by cp.nafiliacion, cp.razonsocial, cp.coddocumento, cp.tipopos,cp.cant_pos_confirm,
cp.tipolinea, bc.ibp, cp.namearchivo ,cp.codestatus, i.serialpos, cp.fecharecepcion, i.numterminal,cp.cantterminalesasig,insta.direccion,cp.estado
,e.estado,m.municipio,par.parroquia,cp.direccion_instalacion;

select * from clie_tblclientepotencial cp 
inner join tblbanco bc on cp.codigobanco::integer=bc.codigobanco::integer
	left join (select concat_ws(' ', d.d_calle_av,d.d_urbanizacion,e.estado,m.municipio,par.parroquia) as direccion, d.id_afiliado
	from tbldirecciones d 
	left join tblestados e 
	on e.id_estado=d.d_estado
	left join tblmunicipios m 
	on m.id_municipio=d.d_municipio
	left join tblparroquias par 
	on par.id_parroquia=d.d_parroquia 
	where d.d_codetipo='4' group by d.d_urbanizacion,d.d_calle_av, d.id_afiliado, e.estado, m.municipio,par.parroquia) insta
	on insta.id_afiliado=cp.id_consecutivo
left join tblestados e 
on e.id_estado=cp.estado::integer
left join tblmunicipios m 
on m.id_municipio=cp.municipio::integer
left join tblparroquias par 
on par.id_parroquia=cp.parroquia::integer
inner join tblinventariopos i on i.nroafiliacion=cp.nafiliacion and i.id_cliente::integer=cp.id_consecutivo

 WHERE cp.codestatus in ('1') and 
(cp.nafiliacion LIKE '%'||'860287283'||'%' or cp.razonsocial LIKE '%'||'860287283'||'%' or cp.coddocumento LIKE '%'||'860287283'||'%') and cp.nafiliacion<>''

select * from tblinventariopos*/