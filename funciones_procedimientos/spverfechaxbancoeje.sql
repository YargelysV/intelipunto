﻿-- Function: spverfechaxbancoeje(integer, integer, character varying, bigint, character varying)

-- DROP FUNCTION spverfechaxbancoeje(integer, integer, character varying, bigint, character varying);

CREATE OR REPLACE FUNCTION spverfechaxbancoeje(IN valor integer, IN banco integer, IN tipousuario character varying, IN ejecutivo bigint, IN login character varying)
  RETURNS TABLE(codigobanco integer, ibp character varying, fecha text, anho double precision, mesdia text) AS
$BODY$
	
	Begin

		CASE valor
			when 1 then
			if (ejecutivo>0) then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					left join tblusuario u on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
					where u.usuario=login
					AND p.codigobanco::integer=banco::integer
					group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecha) as resul 
					group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;

			else if  (tipousuario='E' or tipousuario='O') then

				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
				select p.codigobanco::integer, b.ibp, 
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
				where p.codigobanco::integer=banco::integer and p.ejecutivo=login
				group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
				order by fecha) as resul 
				group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;
			
			else
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
					select p.codigobanco::integer, b.ibp, 
					(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
					,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
					from clie_tblclientepotencial p
					inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
					where p.codigobanco::integer=banco::integer
					group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
					order by fecha) as resul 
					group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;
			end if;
			end if;
		
			else
			return; 
		end case;
	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverfechaxbancoeje(integer, integer, character varying, bigint, character varying)
  OWNER TO postgres;
