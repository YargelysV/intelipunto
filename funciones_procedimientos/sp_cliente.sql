﻿-- Function: sp_cliente(character varying)

-- DROP FUNCTION sp_cliente(character varying);

CREATE OR REPLACE FUNCTION sp_cliente(rif character varying)
  RETURNS table(razon character varying) AS
$BODY$
	
	Begin
		return 	QUERY 
			select razonsocial from clie_tblclientepotencial where coddocumento=rif group by razonsocial;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_cliente(character varying)
  OWNER TO postgres;

--select * from sp_cliente('21545658')