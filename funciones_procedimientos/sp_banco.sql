﻿-- Function: sp_banco(character varying)

-- DROP FUNCTION sp_banco(character varying);

CREATE OR REPLACE FUNCTION sp_banco(icodigobanco character varying)
  RETURNS SETOF tblbanco AS
$BODY$
	
	Begin
		return 	QUERY 
			select * from tblbanco where codigobanco::integer=icodigobanco::integer;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_banco(character varying)
  OWNER TO postgres;
