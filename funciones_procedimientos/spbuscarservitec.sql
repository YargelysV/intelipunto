﻿-- Function: spbuscarservitec(character varying)

-- DROP FUNCTION spbuscarservitec(character varying);

CREATE OR REPLACE FUNCTION spbuscarservitec(IN busqueda character varying)
  RETURNS TABLE(nafiliacion character varying, enlace text, razonsocial character varying, documento character varying, 
  tipopos character varying, cantterminalesasig integer, tipolinea character varying, ibp character varying, 
  namearchivo character varying, estatus integer) AS
$BODY$
Begin
return query 
SELECT cp.nafiliacion, cp.nafiliacion||'/'||cp.namearchivo||'/'||busqueda as enlace, cp.razonsocial, cp.coddocumento, cp.tipopos,case when cp.cant_pos_confirm is null then cp.cantterminalesasig else cp.cant_pos_confirm end, 
cp.tipolinea, bc.ibp, cp.namearchivo ,cp.codestatus FROM clie_tblclientepotencial as cp
inner join tblbanco bc on cp.codigobanco::integer=bc.codigobanco::integer
WHERE cp.codestatus in ('1','2') and 
(cp.nafiliacion LIKE '%'||busqueda||'%' or cp.razonsocial LIKE '%'||busqueda||'%' or cp.coddocumento LIKE '%'||busqueda||'%') and cp.nafiliacion<>''; 
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarservitec(character varying)
  OWNER TO postgres;
