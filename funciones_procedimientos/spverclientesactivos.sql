﻿-- Function: spverclientesactivos()

-- DROP FUNCTION spverclientesactivos();

CREATE OR REPLACE FUNCTION spverclientesactivos()
  RETURNS TABLE(fechaenvio text, codbanco integer, rif character varying, direccion character varying, est character varying, mun character varying,
   parr character varying) AS
$BODY$
	
	Begin
		return 	QUERY SELECT (TO_CHAR(fecharecepcion,'DD')||'/'||TO_CHAR(fecharecepcion,'MM')||'/'||(extract( year from fecharecepcion))::character varying)as 
		fechaenvio, codigobanco::integer, coddocumento, direccion_instalacion, estado, municipio, parroquia 
		 FROM clie_tblclientepotencial;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientesactivos()
  OWNER TO postgres;

 -- select * from spverclientesactivos()
