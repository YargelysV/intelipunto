﻿-- Function: sp_comerdatosbasicos(character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_comerdatosbasicos(character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_comerdatosbasicos(icoddocumento character varying,  inombre character varying, iapellido character varying, iinstituto_organismo character varying,irazoncomercial_fantasia character varying )
  RETURNS boolean AS
$BODY$

DECLARE result int;


Begin
                          
		result:=(select count(coddocumento) from clie_tblclientepotencial where coddocumento=icoddocumento);
		if (result>0) then
			
			update clie_tblclientepotencial set 							  
			  nombre=inombre, apellido=iapellido, 
			  instituto_organismo=iinstituto_organismo,razoncomercial=irazoncomercial_fantasia
			WHERE	coddocumento=icoddocumento ;
			return true;
			else
			return false;

		end if;
		
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_comerdatosbasicos(character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
