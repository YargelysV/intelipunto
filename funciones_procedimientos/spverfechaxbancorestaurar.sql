﻿-- Function: spverfechaxbancorestaurar(integer, integer)

-- DROP FUNCTION spverfechaxbancorestaurar(integer, integer);

CREATE OR REPLACE FUNCTION spverfechaxbancorestaurar(IN valor integer, IN banco integer)
  RETURNS TABLE(codigobanco integer, ibp character varying, fecha text, anho double precision, mesdia text) AS
$BODY$
	
	Begin


		CASE valor
			when 1 then
				return query 
				select resul.codigobanco::integer, resul.ibp, resul.fecha, resul.anho, resul.mes_dia from (
				select p.codigobanco::integer, b.ibp, 
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_auditoriaarchivoeliminar p
				inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
				where p.codigobanco::integer=banco::integer
				and  p.usuariorestaura is null
				group by p.codigobanco::integer, b.ibp, fecha, p.fecharecepcion
				order by fecha) as resul 
				group by resul.codigobanco::integer, resul.ibp, resul.fecha,resul.anho, resul.mes_dia
				order by resul.anho, resul.mes_dia;
			
			else
			return; 
		end case;
	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverfechaxbancorestaurar(integer, integer)
  OWNER TO postgres;
