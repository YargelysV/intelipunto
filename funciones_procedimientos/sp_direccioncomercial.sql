﻿-- Function: sp_direccioncomercial(character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_direccioncomercial(character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_direccioncomercial(icoddocumento character varying, icodetipo integer, id_calle_av character varying, ilocalidad character varying, isector character varying, iurbanizacion character varying, inlocal character varying, iestado character varying, icodepostal character varying, 
iptoref character varying, ifechacarga character varying, iprefijo character varying)
  RETURNS boolean AS
$BODY$

DECLARE resultadoselect int;
Begin

-- CONSULTO SI EL CLIENTE TIENE REGISTRO CON DIRECCION  COMERCIAL
resultadoselect:=(select count(d_codetipo) from tbldirecciones where d_codetipo = 2 and d_coddocumento = icoddocumento and d_fechacarga= ifechacarga and d_codigobanco=iprefijo);

		if (resultadoselect <>0)then

			update tbldirecciones set d_calle_av=id_calle_av,d_localidad=ilocalidad, d_sector=isector, d_urbanizacion=iurbanizacion, d_nlocal=inlocal, d_estado = iestado,d_codepostal=icodepostal, d_ptoref=iptoref,d_fechacarga= ifechacarga,d_codigobanco=iprefijo  
			where d_codetipo = icodetipo  and d_coddocumento = icoddocumento and d_fechacarga= ifechacarga and d_codigobanco=iprefijo;
			else 
                        insert into tbldirecciones(d_coddocumento, d_codetipo, d_calle_av, d_localidad, d_sector, d_urbanizacion, d_nlocal, d_estado, d_codepostal, d_ptoref,d_fechacarga, d_codigobanco)
		        values (icoddocumento,icodetipo , id_calle_av  , ilocalidad , isector  , iurbanizacion  , inlocal  , iestado  , icodepostal  , iptoref,ifechacarga, iprefijo );
	       	    
	        end if;
	        return true;
end;


$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_direccioncomercial(character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
