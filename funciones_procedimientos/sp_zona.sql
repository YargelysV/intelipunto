﻿-- Function: sp_zona(integer)

-- DROP FUNCTION sp_zona(integer);

CREATE OR REPLACE FUNCTION sp_zona(perfil integer)
  RETURNS SETOF tbltipozona AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tbltipozona where cod_perfil=perfil and codzona!=0
		order by codzona;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_zona(integer)
  OWNER TO postgres;
