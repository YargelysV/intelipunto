﻿-- Function: sp_reportexdias(character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_reportexdias(character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_reportexdias(IN banco character varying, IN usuejecutivo character varying, IN fechainicio character varying, IN fechafinal character varying, IN tiporeporte character varying)
  RETURNS TABLE(correlativo bigint, nombrebanco character varying, vendedor text, cantidad bigint, tipopos character varying, ventas_con_fondos bigint, facturado bigint, montousd double precision, montobs double precision, gestionador character varying) AS
$BODY$										--select * from sp_reportexdias('','JUVENAL','01-01-2019','') 
											--select * from sp_reportexdias('','','2019-04-25','2019-04-25','1')

		declare tipoposnodefinido character varying;			--select * from sp_reportexdias('', '0', '2017/10/17','2019/04/22','1')
										--select * from tblbanco
										--select * from clie_tblclientepotencial where  codigobanco='102' AND EJECUTIVO=''
										--select * from 	 where 5129
										--select * from tblpagos where p_idafiliado='5133'
	Begin									--select * from sp_reportexdias('102','0','2018-07-10','2019-04-22')
										--select * from clie_tblclientepotencial


	tipoposnodefinido='Sin definir';

	if (fechainicio!='') then
		if (tiporeporte='1') then
		return  QUERY
				
				select 	row_number () OVER (ORDER BY b.ibp), b.ibp,concat_ws(' ', u.nombres, u.apellidos) as vendedor ,count(cp.id_consecutivo) as cantidad,
					case when cp.tipopos is null or cp.tipopos='' then tipoposnodefinido else cp.tipopos end, 
					sum(case when cp.codestatus in (1) then cp.codestatus else 0 end) as ventasconfondos,
					sum(case when f.estatus in (1) then f.estatus else 0 end) as facturado, sum(pagos.montodolar::double precision) as montousd,
					sum(pagos.montobs::double precision) as montobss, case when cp.usuarioedita is null or cp.usuarioedita='' then 'SIN GESTIONAR' else cp.usuarioedita end
					--((sum(case when cp.codestatus in (1) then cp.codestatus else 0 end)) - (sum(case when f.estatus in (1) then f.estatus else 0 end)) ) as sinfacturar
				from clie_tblclientepotencial cp
				left join tblbanco b on b.codigobanco::integer=cp.codigobanco::integer
				left join tblfacturacioncliente f on f.id_registro=cp.id_consecutivo
				left join tblusuario u on u.usuario=cp.ejecutivo
				left join (select p.p_idafiliado,sum(p.p_montousd::double precision) as montodolar,
				          sum(case when p.p_fconversion='0' or p.p_fconversion='' then p.p_monto::double precision  else 0 end) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				on pagos.p_idafiliado=f.id_registro and f.estatus='1'
				
				--left join (select p.p_idafiliado,sum(p.p_montousd::integer) as montodolar ,
				 --         sum(p.p_monto::integer) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				--on pagos.p_idafiliado=cp.id_consecutivo 
				where 
					case 
					when banco='' or banco='0' then cp.codigobanco!='' else cp.codigobanco=banco
					end

					and cp.fechaedita::date BETWEEN fechainicio::date and fechafinal::date and 

					case 
					when usuejecutivo = '' or usuejecutivo='0' then cp.ejecutivo!='' else cp.ejecutivo=usuejecutivo 
					end
				group by  cp.tipopos,b.ibp, vendedor, cp.usuarioedita
				order by b.ibp ;

			elsif (tiporeporte='2') then
				return  QUERY
				
					select 	row_number () OVER (ORDER BY b.ibp), b.ibp,concat_ws(' ', u.nombres, u.apellidos) as vendedor ,count(cp.id_consecutivo) as cantidad,
						case when cp.tipopos is null or cp.tipopos='' then tipoposnodefinido else cp.tipopos end, 
						sum(case when cp.codestatus in (1) then cp.codestatus else 0 end) as ventasconfondos,
						sum(case when f.estatus in (1) then f.estatus else 0 end) as facturado, sum(pagos.montodolar::double precision) as montousd,
						sum(pagos.montobs::double precision) as montobss, case when cp.usuarioedita is null or cp.usuarioedita='' then 'SIN GESTIONAR' else cp.usuarioedita end
						--((sum(case when cp.codestatus in (1) then cp.codestatus else 0 end)) - (sum(case when f.estatus in (1) then f.estatus else 0 end)) ) as sinfacturar
					from clie_tblclientepotencial cp
					left join tblbanco b on b.codigobanco::integer=cp.codigobanco::integer
					left join tblfacturacioncliente f on f.id_registro=cp.id_consecutivo
					left join tblusuario u on u.usuario=cp.ejecutivo
					left join (select p.p_idafiliado,sum(p.p_montousd::double precision) as montodolar,
						  sum(case when p.p_fconversion='0' or p.p_fconversion='' then p.p_monto::double precision  else 0 end) as montobs from tblpagos p group by p.p_idafiliado) pagos 
					on pagos.p_idafiliado=f.id_registro and f.estatus='1'
					
					--left join (select p.p_idafiliado,sum(p.p_montousd::integer) as montodolar ,
					 --         sum(p.p_monto::integer) as montobs from tblpagos p group by p.p_idafiliado) pagos 
					--on pagos.p_idafiliado=cp.id_consecutivo 
					where 
						case 
						when banco='' or banco='0' then cp.codigobanco!='' else cp.codigobanco=banco
						end

						and f.fecha_facturacion::date BETWEEN fechainicio::date and fechafinal::date and 

						case 
						when usuejecutivo = '' or usuejecutivo='0' then cp.ejecutivo!='' else cp.ejecutivo=usuejecutivo 
						end
					group by  cp.tipopos,b.ibp, vendedor, cp.usuarioedita  
					order by b.ibp ;

		end if;

		else 
		return  QUERY
			select 	row_number () OVER (ORDER BY b.ibp), b.ibp,concat_ws(' ', u.nombres, u.apellidos) as vendedor ,count(cp.id_consecutivo) as cantidad,
					case when cp.tipopos is null or cp.tipopos='' then tipoposnodefinido else cp.tipopos end, 
					sum(case when cp.codestatus in (1) then cp.codestatus else 0 end) as ventasconfondos,
					sum(case when f.estatus in (1) then f.estatus else 0 end) as facturado, sum(pagos.montodolar::double precision) as montousd,
					sum(pagos.montobs::double precision) as montobss, case when cp.usuarioedita is null or cp.usuarioedita='' then 'SIN GESTIONAR' else cp.usuarioedita end
					--((sum(case when cp.codestatus in (1) then cp.codestatus else 0 end)) - (sum(case when f.estatus in (1) then f.estatus else 0 end)) ) as sinfacturar
				from clie_tblclientepotencial cp
				left join tblbanco b on b.codigobanco::integer=cp.codigobanco::integer
				left join tblfacturacioncliente f on f.id_registro=cp.id_consecutivo
				left join tblusuario u on u.usuario=cp.ejecutivo
				left join (select p.p_idafiliado,sum(p.p_montousd::double precision) as montodolar,
				          sum(case when p.p_fconversion='0' or p.p_fconversion='' then p.p_monto::double precision  else 0 end) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				on pagos.p_idafiliado=f.id_registro and f.estatus='1'
				
				--left join (select p.p_idafiliado,sum(p.p_montousd::integer) as montodolar ,
				 --         sum(p.p_monto::integer) as montobs from tblpagos p group by p.p_idafiliado) pagos 
				--on pagos.p_idafiliado=cp.id_consecutivo 
				where 
					case 
					when banco='' or banco='0' then cp.codigobanco!='' else cp.codigobanco=banco
					end

					and 

					case 
					when usuejecutivo = '' or usuejecutivo='0' then cp.ejecutivo!='' else cp.ejecutivo=usuejecutivo 
					end
				group by  cp.tipopos,b.ibp, vendedor, cp.usuarioedita  
				order by b.ibp ;
			
			-- select * from tblusuario
	end if;
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_reportexdias(character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
