﻿-- Function: sp_reportediarioestatus(integer)

-- DROP FUNCTION sp_reportediarioresultadogestion(integer);

CREATE OR REPLACE FUNCTION sp_reportediarioresultadogestion(IN banco integer)
  RETURNS TABLE(sec bigint, enlace text, cliente character varying, afiliado character varying, cbanco character varying, ibpbanco character varying, 
  serial character varying, terminal character varying, sim character varying, estatus character varying, codestatus integer, razonsocial character varying) AS
$BODY$


	Begin		-- select * from sp_reportediarioestatus('0')
		
	
	
			return query 
			
	             		select row_number () OVER (ORDER BY c.codigobanco::integer, g.idestatusinteliservices) as consecutivo,
	             		concat_ws('/', g.nroafiliacion,'','',g.id_cliente,'',c.codigobanco,g.serialpos, case when g.numterminal is null then '' else g.numterminal end,'',banco) as enlace,
	             		g.id_cliente,g.nroafiliacion, c.codigobanco, b.ibp, g.serialpos, g.numterminal, g.numsim, 
				servi.desc_estatus, g.idestatusinteliservices, c.razonsocial
				from tblinventariopos g
				left join tblestatusinteliservices servi
				on servi.idestatus=g.idestatusinteliservices
				left join clie_tblclientepotencial c
				on c.id_consecutivo::integer=g.id_cliente::integer
				left join tblbanco b
				on b.codigobanco::integer=c.codigobanco::integer
				where case when banco !='0' then c.codigobanco::integer=banco::integer else g.id_cliente is not null end
				order by c.codigobanco::integer, g.idestatusinteliservices;

				--select count(serialpos), serialpos from tblinventariopos 
				--group by serialpos order by serialpos


							
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_reportediarioresultadogestion(integer)
  OWNER TO postgres;


--select * from sp_reportediarioresultadogestion('0')