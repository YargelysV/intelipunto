﻿-- Function: spverdatosinstapos(character varying)

-- DROP FUNCTION spverdatosinstapos(character varying);

CREATE OR REPLACE FUNCTION spverdatosinstapos(nroafiliacion character varying)
  RETURNS SETOF tblserviciotecnico AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblserviciotecnico where nafiliacion=nroafiliacion;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverdatosinstapos(character varying)
  OWNER TO postgres;
