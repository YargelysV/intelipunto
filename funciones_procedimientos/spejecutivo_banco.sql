﻿-- Function: spejecutivo_banco(character varying)

-- DROP FUNCTION spejecutivo_banco(character varying);


CREATE OR REPLACE FUNCTION spejecutivo_banco(IN cdocumento character varying)
  RETURNS TABLE(correlativo bigint, vendedor integer, nombre character varying, apellido character varying, documento character varying, nombrebanco character varying, banco integer) AS
$BODY$
	
	Begin
		return 	QUERY 
			SELECT row_number()  OVER(order by e.coddocumento asc) as correlativo, 
			e.codvendedor, e.nombres, e.apellidos,e.coddocumento, b.ibp, b.codigobanco::integer
			FROM adm_ejecutivo e
			left join tblbanco b
			on b.codvendedor=e.codvendedor
		        where e.coddocumento=cdocumento;			
	End;

--  select * from spejecutivo_banco('V-21222222');
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spejecutivo_banco(character varying)
  OWNER TO postgres;
