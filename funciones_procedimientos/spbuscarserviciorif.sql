﻿-- Function: spbuscarserviciorif(character varying)

-- DROP FUNCTION spbuscarserviciorif(character varying);

CREATE OR REPLACE FUNCTION spbuscarserviciorif(IN busqueda character varying)
  RETURNS TABLE(enlace text, correlativo bigint, fechainstalacion text, nafiliacion character varying, razonsocial character varying,
   documento character varying, terminal character varying, serial character varying, tipopos character varying, 
   marcapos character varying, ibp character varying) AS
$BODY$
    Begin				-- select * from clie_tblclientepotencial	
					-- select *from tblinventariopos
					-- select * from tblmarcapos
        return     QUERY 
					SELECT concat_ws('/', cp.nafiliacion,i.numterminal) as enlace, row_number()  
				OVER(order by cp.nafiliacion), 
				to_char(gs.fechainstalacion::date,'DD/MM/YYYY') as fechainstalacion,
				cp.nafiliacion, cp.razonsocial, cp.coddocumento, i.numterminal, i.serialpos, cp.tipopos,
				mar.marcapos, bc.ibp
				FROM clie_tblclientepotencial as cp
				inner join tblbanco bc on cp.codigobanco::integer=bc.codigobanco::integer
				full outer join tblinventariopos i
				on i.nroafiliacion=cp.nafiliacion
				full outer join tblserialpos s
				on s.serial=i.serialpos
				full outer join tblmarcapos mar
				on mar.codtipo=cp.codmarcapos
				full outer join tblgestionserviciotecnico gs
				on gs.nroafiliado=cp.nafiliacion and gs.estatus='4' 
				WHERE cp.codestatus in ('1','2') and 
				i.nroafiliacion is not null and cp.mantenimiento='1' and 
				(cp.nafiliacion LIKE '%'||busqueda||'%' or cp.razonsocial LIKE '%'||busqueda||'%' or cp.coddocumento LIKE '%'||busqueda||'%') and cp.nafiliacion<>''
				
				union
				SELECT concat_ws('/', m.nroafiliacion,m.numterminal) as enlace, row_number()  
				OVER(order by m.nroafiliacion), 
				''::text as fechainstalacion, m.nroafiliacion,  m.razonsocial, m.documento, m.numterminal, m.serialpos, tp.tipopos,
				mar.marcapos, bc.ibp
				from tblmantenimientonuevo m
				inner join tbltipopos tp 
				on tp.codtipo=m.tipopos::integer
				full outer join tblmarcapos mar
				on mar.codtipo::integer=m.marcapos::integer
				inner join tblbanco bc 
				on m.codigobanco::integer=bc.codigobanco::integer 
				where (m.nroafiliacion LIKE '%'||busqueda||'%' or  m.razonsocial LIKE '%'||busqueda||'%' or m.documento LIKE '%'||busqueda||'%') and m.nroafiliacion<>''
				ORDER BY nafiliacion;


End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarserviciorif(character varying)
  OWNER TO postgres;


--	select * from spbuscarserviciorif('8')	