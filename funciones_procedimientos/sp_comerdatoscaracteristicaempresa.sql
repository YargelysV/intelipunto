﻿
-- Function: sp_comerdatoscaracteristicaempresa(character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_comerdatoscaracteristicaempresa(character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_comerdatoscaracteristicaempresa(icoddocumento character varying,inombrearchivo character varying,lun integer,mar integer, mie integer,jue integer,
vier integer,sab integer,dom integer,iusuario character varying,txtlunvie character varying, txtsabdom character varying, iacteconomica character varying)
  RETURNS boolean AS
$BODY$
DECLARE result bigint;
DECLARE result1 bigint;
Begin
    	        result:=(select count(coddocumento) from clie_tblclientepotencial where coddocumento=icoddocumento and namearchivo=inombrearchivo);
		if (result>0) then
			update clie_tblclientepotencial set 							  
			  acteconomica=iacteconomica
			WHERE coddocumento=icoddocumento and namearchivo=inombrearchivo ;

	        result1:=(select count(d_coddocumento) from tbldiashorario where coddocumento=icoddocumento and namearchivo=inombrearchivo);
		if (result1>0) then
                        UPDATE tbldiashorario SET 
                        lunes=lun,martes=mar,miercoles=mie,jueves=jue,viernes=vie,sabado=sab,domingo=dom,d_usuariocarga=iusuario,de_fechacarga=now(),observacion_lun_vier=txtlunvie,observacion_sab_dom=txtsabdom
                        WHERE d_coddocumento=icoddocumento and d_namearchivo=inombrearchivo;
                else
                INSERT INTO tbldiashorario(d_coddocumento, d_namearchivo, lunes, martes, miercoles, jueves, viernes, sabado, domingo, d_usuariocarga, d_fechacarga, obervacion_lun_vier, 
                obsevacion_sab_dom)
                VALUES (icoddocumento,inombrearchivo,lun,mar,mie,jue,vier,sab,dom ,iusuario,now(),txtlunvie,txtsabdom, iacteconomica);
                else 
                

                end if;
			return true;
			else
			return false;
               
		end if;
		
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_comerdatoscaracteristicaempresa(character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
--  select * from tbldiashorario