﻿-- Function: speliminar_archivoseries(date)

-- DROP FUNCTION speliminar_archivoseries(date);

CREATE OR REPLACE FUNCTION speliminar_archivoseries(fecha date)
  RETURNS boolean AS
$BODY$
Begin

	DELETE FROM tblserialpos
	WHERE fechacompra::date=fecha;
	return true;

End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION speliminar_archivoseries(date)
  OWNER TO postgres;
--select * from tblserialpos