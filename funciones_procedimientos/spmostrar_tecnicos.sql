﻿CREATE FUNCTION spmostrar_tecnicos()
  RETURNS TABLE(correlativo bigint, tecnico integer, tipodoc text, documento text, nombre character varying, apellido character varying, trabajo character varying, fecha timestamp with time zone) AS
$BODY$

	Begin	
		return query select row_number()  OVER(order by codtecnicos), codtecnicos, substr(coddocumento,1,strpos(coddocumento,'-') -1) as tipodoc,
             substr(coddocumento,strpos(coddocumento,'-')+1) as documento, nombres, apellidos,zonatrabajo,idate from tbltecnicos 
		ORDER BY codtecnicos;

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
