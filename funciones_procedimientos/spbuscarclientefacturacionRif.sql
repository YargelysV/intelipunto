﻿-- Function: spbuscarclientefacturacionrif(character varying, character varying, bigint, character varying)

-- DROP FUNCTION spbuscarclientefacturacionrif(character varying, character varying, bigint, character varying);

CREATE OR REPLACE FUNCTION spbuscarclientefacturacionrif(IN irif character varying, IN tipousuario character varying, IN ejecutivoin bigint, IN login character varying)
  RETURNS TABLE(enlaceafiliado text, correlativo bigint, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying, cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, calleav character varying, municipio character varying, parroquia character varying, urbanizacion character varying, local character varying, estado character varying, codigopostal character varying, puntoref character varying, usuario character varying, idate date, namearchivo character varying, sizearchivo character varying, direccion text, ejecutivo character varying, estatuspago integer) AS
$BODY$
    Begin


	if (ejecutivoin>0) then	
			return     QUERY 
			select concat_ws('/',tr.coddocumento,tr.codigobanco::integer,tr.fecharecepcion::date,tr.namearchivo,'',tr.id_consecutivo,tipousuario, ejecutivoin, login,irif) as enlace, 
			--select  tr.coddocumento||'/'||tr.codigobanco::integer||'/'||tr.fecharecepcion::date||'/'||tr.namearchivo||'/'||irif||'/'||'/'||tr.id_consecutivo, 
			row_number()  OVER(order by tr.namearchivo), 
			(TO_CHAR(tr.fecharecepcion,'DD')||'-'||TO_CHAR(tr.fecharecepcion,'MM')||'-'||(extract( year from tr.fecharecepcion))::character varying) as fecharecepcion,
			tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
			tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
			tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, m.municipio, par.parroquia,
			d.d_urbanizacion, d.d_nlocal, e.estado, d.d_codepostal, d.d_ptoref, tr.usuario, tr.idate::date, 
			tr.namearchivo, tr.sizearchivo,  concat_ws(' ', e.estado,m.municipio,par.parroquia) as direccion, tr.ejecutivo,
			case when pago.pagos is null or pago.pagos = '0' then '2' else tf.estatus end 
			from clie_tblclientepotencial tr
			inner join tblbanco b on 
			b.codigobanco::integer=tr.codigobanco::integer
			left join tblusuario u 
			on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
			full outer join tbldirecciones d
			on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='1' and d.id_afiliado=tr.id_consecutivo
			left join tblestados e 
			on e.id_estado=d.d_estado
			left join tblmunicipios m 
			on m.id_municipio=d.d_municipio
			left join tblparroquias par 
			on par.id_parroquia=d.d_parroquia
			LEFT join tblfacturacioncliente tf
			on tf.id_registro=tr.id_consecutivo
			LEFT join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
			   group by p_idafiliado) pago
			   on pago.p_idafiliado=tr.id_consecutivo
			
			WHERE tr.codestatus in ('1','2')  and (u.usuario=login OR tr.ejecutivo=login)  and (tr.coddocumento LIKE '%'||irif||'%' or tr.nafiliacion LIKE '%'||irif||'%' or tr.razonsocial LIKE '%'||irif||'%')
			ORDER BY row_number()  OVER(order by tr.namearchivo);

			else if  (tipousuario='E' or tipousuario='O') then

			return     QUERY 
			select concat_ws('/',tr.coddocumento,tr.codigobanco::integer,tr.fecharecepcion::date,tr.namearchivo,'',tr.id_consecutivo,tipousuario, ejecutivoin, login,irif) as enlace, 
			row_number()  OVER(order by tr.namearchivo), 
			(TO_CHAR(tr.fecharecepcion,'DD')||'-'||TO_CHAR(tr.fecharecepcion,'MM')||'-'||(extract( year from tr.fecharecepcion))::character varying) as fecharecepcion,
			tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
			tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
			tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, m.municipio, par.parroquia,
			d.d_urbanizacion, d.d_nlocal, e.estado, d.d_codepostal, d.d_ptoref, tr.usuario, tr.idate::date, 
			tr.namearchivo, tr.sizearchivo,  concat_ws(' ', e.estado,m.municipio,par.parroquia) as direccion, tr.ejecutivo,
			 case when pago.pagos is null or pago.pagos = '0' then '2' else tf.estatus end  
			from clie_tblclientepotencial tr
			inner join tblbanco b on 
			b.codigobanco::integer=tr.codigobanco::integer
			full outer join tbldirecciones d
			on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='1' and d.id_afiliado=tr.id_consecutivo
			left join tblestados e 
			on e.id_estado=d.d_estado
			left join tblmunicipios m 
			on m.id_municipio=d.d_municipio
			left join tblparroquias par 
			on par.id_parroquia=d.d_parroquia
			LEFT join tblfacturacioncliente tf
			on tf.id_registro=tr.id_consecutivo
			LEFT join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
			   group by p_idafiliado) pago
			   on pago.p_idafiliado=tr.id_consecutivo
			WHERE tr.codestatus in ('1','2') and  tr.ejecutivo=login  and (tr.coddocumento LIKE '%'||irif||'%' or tr.nafiliacion LIKE '%'||irif||'%' or tr.razonsocial LIKE '%'||irif||'%')
			ORDER BY row_number()  OVER(order by tr.namearchivo);
			
			else

			return     QUERY 
			select concat_ws('/',tr.coddocumento,tr.codigobanco::integer,tr.fecharecepcion::date,tr.namearchivo,'',tr.id_consecutivo,tipousuario, ejecutivoin, login,irif) as enlace, 
			row_number()  OVER(order by tr.namearchivo), 
			(TO_CHAR(tr.fecharecepcion,'DD')||'-'||TO_CHAR(tr.fecharecepcion,'MM')||'-'||(extract( year from tr.fecharecepcion))::character varying) as fecharecepcion,
			tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
			tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
			tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, m.municipio, par.parroquia,
			d.d_urbanizacion, d.d_nlocal, e.estado, d.d_codepostal, d.d_ptoref, tr.usuario, tr.idate::date, 
			tr.namearchivo, tr.sizearchivo,  concat_ws(' ', e.estado,m.municipio,par.parroquia) as direccion, tr.ejecutivo,
			 case when pago.pagos is null or pago.pagos = '0' then '2' else tf.estatus end 
			from clie_tblclientepotencial tr
			inner join tblbanco b on 
			b.codigobanco::integer=tr.codigobanco::integer
			full outer join tbldirecciones d
			on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='1' and d.id_afiliado=tr.id_consecutivo
			left join tblestados e 
			on e.id_estado=d.d_estado
			left join tblmunicipios m 
			on m.id_municipio=d.d_municipio
			left join tblparroquias par 
			on par.id_parroquia=d.d_parroquia
			LEFT join tblfacturacioncliente tf
			on tf.id_registro=tr.id_consecutivo
			LEFT join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
			   group by p_idafiliado) pago
			   on pago.p_idafiliado=tr.id_consecutivo
			   
			WHERE tr.codestatus in ('1','2')  and (tr.coddocumento LIKE '%'||irif||'%' or tr.nafiliacion LIKE '%'||irif||'%' or tr.razonsocial LIKE '%'||irif||'%')
			ORDER BY row_number()  OVER(order by tr.namearchivo);
			
			end if;
		end if;		
    End;
    
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarclientefacturacionrif(character varying, character varying, bigint, character varying)
  OWNER TO postgres;
