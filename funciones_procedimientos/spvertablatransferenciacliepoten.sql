﻿-- Function: spvertablatransferenciacliepoten()

-- DROP FUNCTION spvertablatransferenciacliepoten();

CREATE OR REPLACE FUNCTION spvertablatransferenciacliepoten()
  RETURNS TABLE(id_consecutivo integer, fecharecepcion text, codigobanco character varying, ibp character varying, tipopos character varying, tipomodelonegocio character varying, tipocliente character varying, afiliado character varying, cantidadterminales integer, tipolinea character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, direccion character varying, est character varying, mun character varying, parr character varying, agenci character varying, usuario character varying, idate timestamp with time zone, ejecutivo character varying) AS
$BODY$
--SELECT * FROM clie_tbltransferencia
	
	Begin
				return query 
				select tr.id_consecutivo, to_char(tr.fecharecepcion::date,'DD/MM/YYYY') , tr.codigobanco, b.ibp, tr.tipopos, tr.tipomodelonegocio, tr.tipocliente,
				tr.nafiliacion,tr.cantterminalesasig,
				tr.tipolinea, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.direccion_instalacion, tr.estado,
				tr.municipio, tr.parroquia,tr.agencia, tr.usuario, tr.idate, tr.ejecutivo
				from clie_tbltransferencia tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				order by tr.id_consecutivo;
				
	End;
	--select * from clie_tbltransferencia

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spvertablatransferenciacliepoten()
  OWNER TO postgres;
