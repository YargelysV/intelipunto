﻿-- Function: spverafiliados()

-- DROP FUNCTION spverafiliados();

CREATE OR REPLACE FUNCTION spverafiliados()
  RETURNS SETOF clie_tblclientepotencial AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM clie_tblclientepotencial;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverafiliados()
  OWNER TO postgres;