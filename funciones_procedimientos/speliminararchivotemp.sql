﻿-- Function: speliminararchivotemp(character varying)

-- DROP FUNCTION speliminararchivotemp(character varying);

CREATE OR REPLACE FUNCTION speliminararchivotemp(nombre character varying)
  RETURNS boolean AS
$BODY$
	
	DECLARE _filename VARCHAR;
	Begin

		_filename := 'C:/xampp/htdocs/Intelipunto/Descargas/a.txt';

		-- Delete temporary file
		EXECUTE format(
			    $$COPY (SELECT 1) TO PROGRAM 'rm %I'$$,
			    _filename
			  );
	return true;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION speliminararchivotemp(character varying)
  OWNER TO postgres;

--select * from speliminararchivotemp('102.Mac')