﻿-- Table: tbltipocuenta

-- DROP TABLE tbltipocuenta;

CREATE TABLE tbltipocuenta
(
  codetipo integer NOT NULL,
  desctipo character varying(50) NOT NULL,
  CONSTRAINT pk_tbltipocuenta PRIMARY KEY (codetipo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbltipocuenta
  OWNER TO postgres;
