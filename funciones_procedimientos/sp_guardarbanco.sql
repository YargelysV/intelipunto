﻿-- Function: sp_guardarbanco(integer, character varying, character varying)

-- DROP FUNCTION sp_guardarbanco(integer, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_guardarbanco(id_registro integer, codbanco character varying, usuarioin character varying)
  RETURNS boolean AS
$BODY$

Begin
	 --   select * from clie_tblclientepotencial
	 update clie_tblclientepotencial set 							  
	  cbancoasig=codbanco, usuarioedita=usuarioin, fechaedita=now()
	  where  id_consecutivo::integer=id_registro::integer;
	return true;
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_guardarbanco(integer, character varying, character varying)
  OWNER TO postgres;
