﻿-- Function: spasignarserialpos1(integer, character varying)

-- DROP FUNCTION spasignarserialpos1(integer, character varying);

CREATE OR REPLACE FUNCTION spasignarserialpos1(IN registro integer, IN busqueda character varying)
  RETURNS TABLE(secuencial bigint, enlace text, serialpo character varying, numtermx character varying, numsimx character varying, fechaen text, nroafiliaci character varying, namearchiv character varying, correlativoq character varying, correlativ bigint, fechacargasistem timestamp with time zone, numamexx character varying, estatus character varying) AS
$BODY$
declare asignados bigint;
declare postotal bigint;
declare faltantes bigint;

Begin
asignados:=(select count(*) from tblinventariopos where id_cliente::integer=registro::integer);
postotal:=(select case when cant_pos_confirm is null or cant_pos_confirm='0' then cantterminalesasig else cant_pos_confirm end
	   from clie_tblclientepotencial where id_consecutivo=registro);
faltantes:=(postotal-asignados);

return query 
SELECT row_number()  OVER(order by result.fechacarga),* from (
			select i.serialpos||'/'||i.nroafiliacion||'/'||'/'||busqueda||'/'||i.numterminal as enlace, i.serialpos, i.numterminal, i.numsim, to_char(i.fechaent,'DD/MM/YYYY'), i.nroafiliacion, i.namearchivo,correlativo,0, i.fechacarga, i.numamex, 
			case when i.serialpos is null or i.serialpos='' and s.desc_estatus is null then 'Por Configurar' else s.desc_estatus end
			from tblinventariopos i
			left join tblestatusinteliservices s on
			s.idestatus=i.idestatusinteliservices
			where id_cliente::integer=registro::integer
			union
			select ''||'/'||p.nafiliacion||'/'||'/'||busqueda||'/'||'' as enlace,'' as serialpos,'' as numterminal,'' as numsim,null as fechaent,p.nafiliacion, p.namearchivo,'' as correlativo, 
			 generate_series(1,faltantes), now(), '' as numamex, 'Por Configurar' as desc_estatus FROM  clie_tblclientepotencial p
			where p.id_consecutivo=registro
			)result  order by row_number()  OVER(order by result.fechacarga);
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spasignarserialpos1(integer, character varying)
  OWNER TO postgres;

SELECT * FROM spasignarserialpos1('18775','')