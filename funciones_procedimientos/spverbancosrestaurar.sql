﻿-- Function: spverbancosrestaurar(integer)

-- DROP FUNCTION spverbancosrestaurar(integer);

CREATE OR REPLACE FUNCTION spverbancosrestaurar(IN valor integer)
  RETURNS TABLE(codigobanco integer, ibp character varying) AS
$BODY$
	
	Begin

		CASE valor
			when 1 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_auditoriaarchivoeliminar p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				where usuariorestaura is null
				group by p.codigobanco::INTEGER, b.ibp;
			else
			return; 
		end case;
	
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverbancosrestaurar(integer)
  OWNER TO postgres;
