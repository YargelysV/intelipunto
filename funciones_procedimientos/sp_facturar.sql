﻿-- Function: sp_facturar(integer, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_facturar(integer, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_facturar(iidregistro integer, infactura character varying, ifechafacturacion character varying, icostopos character varying, iusuariofactura character varying)
  RETURNS boolean AS
$BODY$
	DECLARE existe bigint;
	DECLARE status integer;
	
	Begin
		existe:=(select count(*) from tblfacturacioncliente where id_registro=iidregistro);

		if(existe=0) then
               -- select * from tblfacturacioncliente
			insert into tblfacturacioncliente(id_registro, nfactura, fecha_facturacion,fecha_creacion_registro,costopos,usuario_carga_factura) 
			values (iidregistro, infactura, ifechafacturacion::date, now(),icostopos::numeric,iusuariofactura);
			
				
			return true;
		else
		
			update tblfacturacioncliente set 
			nfactura=	        CASE 
						WHEN infactura IS NULL OR infactura = '' THEN nfactura
						ELSE infactura END,
			fecha_facturacion=	CASE 
						WHEN ifechafacturacion IS NULL OR ifechafacturacion = '' THEN fecha_facturacion 
						ELSE ifechafacturacion::date END,
			fecha_creacion_registro= now(), 	

			usuario_carga_factura=	CASE 
					WHEN iusuariofactura IS NULL OR iusuariofactura = '' THEN usuario_carga_factura 
					ELSE iusuariofactura END,
			costopos=	CASE 
					WHEN icostopos IS NULL OR icostopos::numeric = 0 THEN costopos 
					ELSE icostopos::numeric END		
			 WHERE	id_registro=iidregistro;

			return true;
		end if;
			
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_facturar(integer, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
