﻿-- Function: sp_parroquiaselected(integer)

-- DROP FUNCTION sp_parroquiaselected(integer);

CREATE OR REPLACE FUNCTION sp_parroquiaselected(iid_parroquia integer)
  RETURNS SETOF tblparroquias AS
$BODY$
	
	Begin		
	return 	QUERY SELECT * FROM tblparroquias where id_parroquia=iid_parroquia order by parroquia asc;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_parroquiaselected(integer)
  OWNER TO postgres;
