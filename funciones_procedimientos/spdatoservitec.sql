﻿-- DROP FUNCTION spdatoservitec(character varying);
CREATE OR REPLACE FUNCTION spdatoservitec(IN nroafiliacion character varying)
  RETURNS TABLE(nafiliciacion character varying, coddocumento character varying, razonsocial character varying, razoncomercial_fantasia character varying, nombre character varying, apellido character varying, instituto_organismo character varying, calleav character varying, localidad character varying, sector character varying, urbanizacion character varying, local character varying, estado character varying, codigopostal character varying, puntoref character varying, telf1 character varying, telf2 character varying, tlf_movil1 character varying, tlf_movil2 character varying, email_corporativo character varying, email_comercial character varying, email_personal character varying, cantidad_pos integer, tipopos character varying, nserial character varying, codestatusins character varying, fechaentccr time with time zone, fecharecpos time with time zone, fechainspos time with time zone) AS
$BODY$
	
	Begin
	return 	QUERY 
			SELECT 
			clie_tblclientepotencial.nafiliacion, 
			clie_tblclientepotencial.coddocumento, 
			clie_tblclientepotencial.razonsocial, 
			clie_tblclientepotencial.razoncomercial,
			clie_tblclientepotencial.nombre, 
			clie_tblclientepotencial.apellido, 
			clie_tblclientepotencial.instituto_organismo,
			clie_tblclientepotencial.calleav, 
			clie_tblclientepotencial.localidad, 
			clie_tblclientepotencial.sector,
			clie_tblclientepotencial.urbanizacion, 
			clie_tblclientepotencial.local, 
			clie_tblclientepotencial.estado,
			clie_tblclientepotencial.codigopostal, 
			clie_tblclientepotencial.puntoref,
			clie_tblclientepotencial.telf1, 
			clie_tblclientepotencial.telf2, 
			clie_tblclientepotencial.tlf_movil1, 
			clie_tblclientepotencial.tlf_movil2,
			clie_tblclientepotencial.email_corporativo, 
			clie_tblclientepotencial.email_comercial,
			clie_tblclientepotencial.email_personal, 
			clie_tblclientepotencial.cant_pos_confirm,
			clie_tblclientepotencial.tipopos, 
			tblserviciotecnico.nserialpos,
			tblserviciotecnico.codestatusins,
			tblserviciotecnico.fechaentccr,
			tblserviciotecnico.fecharecpos,
			tblserviciotecnico.fechainspos
			FROM clie_tblclientepotencial 
			full outer join tblserviciotecnico on clie_tblclientepotencial.nafiliacion=tblserviciotecnico.nafiliacion
			where clie_tblclientepotencial.nafiliacion=nroafiliacion;
		
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spdatoservitec(character varying)
  OWNER TO postgres;

--select * from spdatoservitec('123456')

--select * from clie_tblclientepotencial
