﻿CREATE FUNCTION sp_marcapos()
  RETURNS SETOF  tblmarcapos AS
$BODY$
	Begin
		return 	QUERY SELECT * FROM tblmarcapos;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
