﻿-- Function: spverfechaseje(integer,character varying, bigint, character varying)

-- DROP FUNCTION spverfechaseje(integer,character varying, bigint, character varying);

CREATE OR REPLACE FUNCTION spverfechaseje(IN valor integer,IN tipousuario character varying, IN ejecutivo bigint, IN login character varying)
  RETURNS TABLE(fecha text, anio double precision, mesdia text) AS
$BODY$
	
	Begin

		CASE valor
			when 1 then
			if (ejecutivo>0) then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::integer=p.codigobanco::integer
				left join tblusuario u on b.coddocumento=u.tipodoc||'-'||u.coddocumento 
				where u.usuario=login
				group by fecha, p.fecharecepcion
				order by fecharecepcion;
				
			else if  (tipousuario='E' or tipousuario='O') then
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				where p.ejecutivo=login
				group by fecha, p.fecharecepcion
				order by fecharecepcion;
			else
				return query 
				select  
				(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
				,extract( year from p.fecharecepcion) AS anho, TO_CHAR(p.fecharecepcion,'MM')||(TO_CHAR(p.fecharecepcion,'DD')) as mes_dia
				from clie_tblclientepotencial p
				group by fecha, p.fecharecepcion
				order by fecharecepcion;
			end if;
			end if;
				
			else
			return; 
		end case;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverfechaseje(integer,character varying, bigint, character varying)
  OWNER TO postgres;

--select * from spverfechaseje('1','A','2','GERENTE')