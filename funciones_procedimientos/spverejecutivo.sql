﻿-- Function: spverejecutivo(integer)

-- DROP FUNCTION spverejecutivo(integer);

CREATE OR REPLACE FUNCTION spverejecutivo(IN valor integer)
  RETURNS TABLE(ejecutivo text, coddocumento character varying) AS
$BODY$
	
	Begin

		CASE valor
		
			when 10 then
				return query 
				select concat_ws (' ', a.nombres, a.apellidos)  as ejecutivo, g.g_codejecutivointeligensa from tblgestioncliente g
				inner join adm_ejecutivo a 
				on a.coddocumento=g.g_codejecutivointeligensa
				group by g.g_codejecutivointeligensa, ejecutivo;
			 
		         when 11 then
		return query 
                           select concat_ws (' ', u.nombres, u.apellidos)::text as coddocumento, u.usuario::character varying as ejecutivo   from tblusuario u;
                         
			else
			return; 
		end case;
	End;


--  select * from clie_tblclientepotencial
--  select * from tblgestioncliente
--  select * from adm_ejecutivo

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverejecutivo(integer)
  OWNER TO postgres;