﻿-- Function: spverclientecomercializacionbuscar(character varying, integer, integer, integer, timestamp with time zone)

-- DROP FUNCTION spverclientecomercializacionbuscar(character varying, integer, integer, integer, timestamp with time zone);

CREATE OR REPLACE FUNCTION spverclientecomercializacionbuscar(IN buscar character varying, IN inicio integer, IN nroreg integer, IN banco integer, IN fecha timestamp with time zone)
  RETURNS TABLE(id_consecutivo integer, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying, cantidadterminales integer, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, calleav character varying, localidad character varying, sector character varying, urbanizacion character varying, local character varying, estado character varying, codigopostal character varying, puntoref character varying, usuario character varying, idate date, namearchivo character varying, sizearchivo character varying) AS
$BODY$
	
	Begin
	
	
				return query select tr.id_consecutivo, 
				(TO_CHAR(tr.fecharecepcion,'DD')||'-'||TO_CHAR(tr.fecharecepcion,'MM')||'-'||(extract( year from tr.fecharecepcion))::character varying) as fecharecepcion,
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, tr.calleav, tr.localidad,
				tr.sector, tr.urbanizacion, tr.local, tr.estado, tr.codigopostal, tr.puntoref, tr.usuario, tr.idate::date, tr.namearchivo, 
				tr.sizearchivo
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				where (tr.fecharecepcion::character varying LIKE '%'||buscar||'%' or tr.razonsocial::character varying LIKE '%'||buscar||'%' or tr.coddocumento LIKE '%'||buscar||'%' or tr.namearchivo LIKE '%'||buscar||'%')
				and tr.codigobanco::integer=banco::integer and tr.idate::date=fecha
				ORDER BY tr.coddocumento
				LIMIT nroreg OFFSET inicio ;
		
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverclientecomercializacionbuscar(character varying, integer, integer, integer, timestamp with time zone)
  OWNER TO postgres;