﻿-- Function: sp_editadireccioninstalacion(character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_editadireccioninstalacion(character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_editadireccioninstalacion(icoddocumento character varying, icodetipo integer, id_calle_av character varying, ilocalidad character varying, isector character varying, iurbanizacion character varying, inlocal character varying, iestado character varying, icodepostal character varying, iptoref character varying, ifechacarga character varying, iprefijo character varying, inamearchivo character varying, iusuario character varying)
  RETURNS boolean AS
$BODY$

DECLARE resultadoselect int;
Begin


resultadoselect:=(select count(d_codetipo) from tbldirecciones where d_codetipo =4 and d_coddocumento = icoddocumento and d_codigobanco::integer=iprefijo::integer and d_namearchivo=inamearchivo);

		if (resultadoselect <>0)then

			update tbldirecciones set d_calle_av=id_calle_av,d_localidad=ilocalidad, d_sector=isector, d_urbanizacion=iurbanizacion, d_nlocal=inlocal,
			 d_estado = iestado,d_codepostal=icodepostal, d_ptoref=iptoref,d_fechacarga= ifechacarga,d_codigobanco=iprefijo,d_namearchivo=inamearchivo, d_usuariocarga=iusuario
			where d_codetipo = icodetipo and d_coddocumento = icoddocumento  and d_codigobanco=iprefijo and d_namearchivo=inamearchivo;
			 return true;
			else if(resultadoselect = 0)then
                        insert into tbldirecciones(d_coddocumento, d_codetipo, d_calle_av, d_localidad, d_sector, d_urbanizacion, d_nlocal, d_estado, d_codepostal, d_ptoref, d_fechacarga, d_codigobanco, d_namearchivo,d_usuariocarga)
		        values (icoddocumento,icodetipo, id_calle_av  , ilocalidad , isector  , iurbanizacion  , inlocal  , iestado  , icodepostal  , iptoref, ifechacarga, iprefijo,inamearchivo, iusuario );
                        return true;
                        
                        else 
                       
                        return false;
                        
	                end if;
	       end if;
	        return false;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_editadireccioninstalacion(character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
