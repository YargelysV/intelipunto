﻿-- Function: speditarclientefacturacion(character varying, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION speditarclientefacturacion(character varying, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION speditarclientefacturacion(documento character varying, bancocliente integer, nombrearchivo character varying,
 icostopos  numeric, fechafact character varying, factura character varying, fpago integer, icodtarjeta integer, 
 referencia character varying, bancoo character varying, ifechapago character varying, ihorapago character varying, login character varying)
  RETURNS boolean AS
$BODY$
	DECLARE existe bigint;
	
	Begin
--select * from tblfacturacioncliente
		existe:=(select count(*) from tblfacturacioncliente where coddocumento=documento 
			 and clientebanco::INTEGER=bancocliente::INTEGER and namearchivo=nombrearchivo);

		if(existe=0) then

			insert into tblfacturacioncliente 
			values (documento, bancocliente::INTEGER, nombrearchivo, 
				CASE 
				WHEN fechafact IS NULL OR fechafact = '' THEN NULL 
				ELSE fechafact::DATE END, 
				CASE 
				WHEN factura IS NULL OR factura = '' THEN NULL 
				ELSE factura END, 
				CASE 
				WHEN fpago IS NULL OR fpago = 0 THEN NULL 
				ELSE fpago END, 
				referencia, bancoo::integer, now(), login, icostopos,
				CASE 
				WHEN icodtarjeta IS NULL OR icodtarjeta = 0 THEN NULL 
				ELSE icodtarjeta END,
				CASE 
				WHEN ifechapago IS NULL OR ifechapago = '' THEN NULL 
				ELSE ifechapago::DATE END,
				CASE 
				WHEN ihorapago IS NULL OR ihorapago = '' THEN NULL 
				ELSE ihorapago END);
			return true;
		else
		
			update tblfacturacioncliente set 
			  fechafacturacion = 	CASE 
						WHEN fechafact IS NULL OR fechafact = '' THEN fechafacturacion 
						ELSE fechafact::DATE END, 
			  nfactura=	        CASE 
						WHEN factura IS NULL OR factura = '' THEN nfactura
						ELSE factura END,
			 formapago=	        CASE 
						WHEN fpago IS NULL OR fpago = 0 THEN formapago
						ELSE fpago END,
			ntransferencia=	        CASE 
						WHEN referencia IS NULL OR referencia = '' THEN ntransferencia 
						ELSE referencia END,
			bancoorigen=	        CASE 
						WHEN bancoo IS NULL OR bancoo = '' THEN bancoorigen 
						ELSE bancoo::integer END,
			fecharegistro= now(), usuario=login,	

			costopos=	CASE 
					WHEN icostopos IS NULL OR icostopos = 0 THEN costopos 
					ELSE icostopos END,
			codtarjeta=	CASE 
					WHEN icodtarjeta IS NULL OR icodtarjeta = 0 THEN codtarjeta 
					ELSE icodtarjeta END,
			fechapago=	CASE 
					WHEN ifechapago IS NULL OR ifechapago = '' THEN fechapago 
					ELSE ifechapago::DATE END,
			horapago=	CASE 
					WHEN ihorapago IS NULL OR ihorapago = '' THEN horapago 
					ELSE ihorapago END		
			 WHERE	coddocumento=documento and clientebanco::INTEGER=bancocliente::INTEGER and namearchivo=nombrearchivo;
			return true;
		end if;
			
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION speditarclientefacturacion(character varying, integer, character varying, numeric, character varying, character varying, integer, integer, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
