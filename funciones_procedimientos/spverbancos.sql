﻿-- Function: spverbancos(integer)

-- DROP FUNCTION spverbancos(integer);

CREATE OR REPLACE FUNCTION spverbancos(IN valor integer)
  RETURNS TABLE(codigobanco integer, ibp character varying) AS
$BODY$
	
	Begin

		CASE valor
			when 1 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				group by p.codigobanco::INTEGER, b.ibp;
			when 3 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				inner join tblfacturacioncliente f on f.id_registro=p.id_consecutivo
				inner join tblgestionserviciotecnico g on g.id_cliente::integer=p.id_consecutivo
				where p.codestatus in ('1','2') and f.nfactura!='' and g.estatus!='4' and p.nafiliacion<>''
				group by p.codigobanco::INTEGER, b.ibp;	
			when 4 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				group by p.codigobanco::INTEGER, b.ibp;	
			when 5 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				where p.codestatus in ('1','2','4')
				group by p.codigobanco::INTEGER, b.ibp;		
			when 6 then
			-- facturacioncliente
			-- select * from tblfacturacioncliente
				return query 
				select  p.codigobanco::integer, b.ibp  
				from clie_tblclientepotencial p
				inner join tblbanco b 
				on b.codigobanco::integer=p.codigobanco::integer
				left join  tblfacturacioncliente t
				on t.id_registro=p.id_consecutivo
				left join tbldirecciones d
				on d.id_afiliado=p.id_consecutivo
				where codestatus in('1','2')  and  t.nfactura is null and d.d_codetipo='1'
				group by  p.codigobanco::integer, b.ibp
				order by p.codigobanco::integer;
			when 7 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				inner join tblfacturacioncliente f on f.id_registro=p.id_consecutivo
				where p.codestatus in ('1','2') and f.nfactura!=''
				--and p.nafiliacion<>'' 
				group by p.codigobanco::INTEGER, b.ibp;
			when 8 then
			--SELECT * FROM clie_tblclientepotencial where id_consecutivo='10608'
				return query 
				select  p.codigobanco::integer, b.ibp  
				from clie_tblclientepotencial p
				inner join tblbanco b 
				on b.codigobanco::integer=p.codigobanco::integer
				left join  tblfacturacioncliente t
				on t.id_registro=p.id_consecutivo
				where codestatus in('1','2')  and  t.nfactura is not null
				group by  p.codigobanco::integer, b.ibp
				order by p.codigobanco::integer;

			when 9 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				inner join tblgestionserviciotecnico g on
				g.nroafiliado=p.nafiliacion
				where p.estatus_contacto in ('3','4') and g.estatus='4'
				group by p.codigobanco::INTEGER, b.ibp;	

			when 10 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				group by p.codigobanco::INTEGER, b.ibp;	

			when 11 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				group by p.codigobanco::INTEGER, b.ibp;	
				
		        when 12 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				--inner join tblinventariopos i on i.id_cliente::integer=p.id_consecutivo
				inner join tblfacturacioncliente f on f.id_registro=p.id_consecutivo
				inner join tblgestionserviciotecnico g on g.id_cliente::integer=p.id_consecutivo
				where p.codestatus in ('1','2') and f.nfactura!='' and p.nafiliacion<>'' and g.serialpos!=''
				--and i.serialpos!=''
				group by p.codigobanco::INTEGER, b.ibp;
				
                        when 13 then -- select * from tblfacturacioncliente
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				group by p.codigobanco::INTEGER, b.ibp;
			--administracion  --reporte general
			when 15 then
				return query 
				select p.codigobanco::INTEGER, b.ibp from clie_tblclientepotencial p
				inner join tblbanco b on b.codigobanco::INTEGER=p.codigobanco::INTEGER
				where p.codestatus in ('1','2')
				group by p.codigobanco::INTEGER, b.ibp;		
                       
			else
			return; 
		end case;
	End;


--  select * from clie_tblclientepotencial
--  select * from tblgestionserviciotecnico
--  select * from adm_ejecutivo

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spverbancos(integer)
  OWNER TO postgres;
