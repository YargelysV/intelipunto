﻿-- Function: spvervendedor(character varying)

-- DROP FUNCTION spvervendedor(character varying);

CREATE OR REPLACE FUNCTION spvervendedor(icodigobanco character varying)
  RETURNS TABLE (codvendedor integer,nvendedor character varying,coddocumento character varying,nombres character varying,apellidos character varying) AS
$BODY$
	
	Begin
		return 	QUERY 
		        	
			select e.codvendedor,e.nvendedor,e.coddocumento,e.nombres,e.apellidos from adm_ejecutivo as e
			inner join tblbanco as b
			on 
		        b.codvendedor = e.codvendedor
			where b.codigobanco::integer=icodigobanco::integer and b.codvendedor=e.codvendedor;
			
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spvervendedor(character varying)
  OWNER TO postgres;

  -- select * from tblbanco where codvendedor =1 and codigobanco='0102'
		        	-- select * from tblbanco

		        	 select * from spvervendedor('0115')
