﻿-- Function: sp_comercializacionvereditar(character varying)

-- DROP FUNCTION sp_comercializacionvereditar(character varying);

CREATE OR REPLACE FUNCTION sp_comercializacionvereditar(IN afiliado character varying)
  RETURNS TABLE(fecharecepcion timestamp with time zone, codigobanco character varying, tipopos character varying, tipolinea character varying, nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, calleav character varying, localidad character varying, sector character varying, urbanizacion character varying, local character varying, estado character varying, codigopostal character varying, puntoref character varying, codestatus integer, ibp character varying, descestatus character varying, fecha text) AS
$BODY$
	
	Begin
		return 	QUERY SELECT p.fecharecepcion, p.codigobanco, p.tipopos, p.tipolinea, p.nafiliacion, 
		p.razonsocial, p.coddocumento, p.acteconomica, p.rlegal, p.cirl, p.rifrl, p.correorl, p.telfrl,
		p.telf1, p.telf2, p.ncuentapos, p.calleav, p.localidad, p.sector, p.urbanizacion, p.local, 
		p.estado, p.codigopostal, p.puntoref, p.codestatus, b.ibp, s.descestatus,
		(TO_CHAR(p.fecharecepcion,'DD')||'-'||TO_CHAR(p.fecharecepcion,'MM')||'-'||(extract( year from p.fecharecepcion))::character varying) as fecha
		FROM clie_tblclientepotencial p 
		inner join tblbanco b 
		on b.codigobanco=p.codigobanco
		inner join tblestatussolpos s
		on s.codestatus=p.codestatus
		where  p.nafiliacion=afiliado;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_comercializacionvereditar(character varying)
  OWNER TO postgres;
