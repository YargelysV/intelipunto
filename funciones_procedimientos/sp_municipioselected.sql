﻿

CREATE OR REPLACE FUNCTION sp_municipioselected(iid_municipio integer)
  RETURNS SETOF tblmunicipios AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblmunicipios where id_municipio=iid_municipio order by municipio asc;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

