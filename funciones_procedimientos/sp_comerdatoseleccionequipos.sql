﻿-- Function: sp_comerdatoseleccionequipos(character varying, character varying, integer, integer, integer, integer, character varying)

-- DROP FUNCTION sp_comerdatoseleccionequipos(character varying, character varying, integer, integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION sp_comerdatoseleccionequipos(icoddocumento character varying, imodelo character varying, icantidad_pos integer, isenal_inalambrica integer, itipo_cuenta integer, iforma_pago integer, inombrearchivo character varying)
  RETURNS boolean AS
$BODY$

DECLARE result int;
 
Begin
                      
		result:=(select count(coddocumento) from clie_tblclientepotencial where coddocumento=icoddocumento and namearchivo=inombrearchivo);
		if (result>0) then
			
			update clie_tblclientepotencial set 							  
			  modelo=imodelo, cant_pos_confirm=icantidad_pos,
			  senal_inalambrica=isenal_inalambrica, tipo_cuenta=itipo_cuenta, forma_pago=iforma_pago
			  where coddocumento=icoddocumento and namearchivo=inombrearchivo;
			return true;
			else
			return false;
               
		end if;
		
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_comerdatoseleccionequipos(character varying, character varying, integer, integer, integer, integer, character varying)
  OWNER TO postgres;
