﻿-- Function: spbuscaracuerdoservicioBFgrafica(character varying, integer, date, character varying)

-- DROP FUNCTION spbuscaracuerdoservicioBFgrafica(integer, date);

CREATE OR REPLACE FUNCTION spbuscaracuerdoservicioBFgrafica(IN banco integer, IN fecha date)
  RETURNS TABLE(correlativo bigint, registros bigint, promedios character varying) AS
$BODY$
	
	Begin
			return query 
			SELECT row_number()  OVER(order by result.promedio desc),* from (	
				select count(tr.fecharecepcion) as registros, case when prom.promedio is null then 'En proceso' else prom.promedio::character varying end
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				left join (select  case when AVG(extract(days from ((g.fechainstalacion) - ( t.fecharecepcion)))) is null then '0' else 
					   AVG(extract(days from ((g.fechainstalacion) - ( t.fecharecepcion))))::double precision end as promedio,g.nroafiliado, g.nombrearchivo
					   from tblgestionserviciotecnico g 
					   inner join clie_tblclientepotencial t 
					   on t.nafiliacion=g.nroafiliado and t.namearchivo=g.nombrearchivo
					   group by g.nroafiliado, g.nombrearchivo) prom
				on   prom.nroafiliado=tr.nafiliacion and prom.nombrearchivo=tr.namearchivo
				where  tr.codigobanco::integer=banco and tr.fecharecepcion::date=fecha
				group by prom.promedio
			)result  order by row_number()  OVER(order by result.promedio desc);         
	
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscaracuerdoservicioBFgrafica(integer, date)
  OWNER TO postgres;

--select * from spbuscaracuerdoservicioBFgrafica('102','20-10-2017')


