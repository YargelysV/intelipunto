﻿-- Function: spmostrarclientefacturacion(integer)

-- DROP FUNCTION spmostrarclientefacturacion(integer);

CREATE OR REPLACE FUNCTION spmostrarclientefacturacion(IN idregistro integer)
  RETURNS TABLE(idafiliado integer, fecharecepcion text, codigobanco integer, ibp character varying, tipopos character varying, cantidadterminales integer, tipolinea character varying, 
  nafiliacion character varying, razonsocial character varying, coddocumento character varying, acteconomica character varying, rlegal character varying, cirl character varying, rifrl character varying, 
  correorl character varying, telfrl character varying, telf1 character varying, telf2 character varying, ncuentapos character varying, calleav character varying, municipio character varying, 
  parroquia character varying, urbanizacion character varying, local character varying, estado character varying, codigopostal character varying, puntoref character varying, usuario character varying, 
  idate date, namearchivo character varying, sizearchivo character varying, direccion text, fechafacturacion date, nfactura character varying, costoposfacturado numeric, estatus integer, 
  marcapos character varying, factorconversion numeric, valordolar numeric) AS
$BODY$
	
	Begin

        -- SELECT * FROM spmostrarclientefacturacion('5191')
        -- J-54321-0/116/2019-02-13/BOD_13032019_03.CSV/65965524//5191
	-- select * from tblfacturacioncliente
	-- select * from tblformapago
				return query 
				select 
				tr.id_consecutivo,(TO_CHAR(tr.fecharecepcion,'DD')||'-'||TO_CHAR(tr.fecharecepcion,'MM')||'-'||(extract( year from tr.fecharecepcion))::character varying) as fecharecepcion,
				tr.codigobanco::integer, b.ibp, tr.tipopos, case when tr.cant_pos_confirm is null then tr.cantterminalesasig else tr.cant_pos_confirm end ,
				tr.tipolinea, tr.nafiliacion, tr.razonsocial, tr.coddocumento, tr.acteconomica, tr.rlegal, 
				tr.cirl, tr.rifrl, tr.correorl, tr.telfrl, tr.telf1, tr.telf2, tr.ncuentapos, d.d_calle_av, 
				m.municipio, par.parroquia, d.d_urbanizacion, d.d_nlocal, e.estado, d.d_codepostal, d.d_ptoref, tr.usuario, tr.idate::date, 
				tr.namearchivo, tr.sizearchivo, 
				concat_ws(' ', e.estado,m.municipio,par.parroquia) as direccion, f.fecha_facturacion::DATE,
				f.nfactura,case when f.costopos is null or f.costopos=0 then v.valorbs else f.costopos end,case when pago.pagos is null or pago.pagos = '0' then '2' else f.estatus end, marc.marcapos,v.factorconversion, v.valordolar
				from clie_tblclientepotencial tr
				inner join tblbanco b on 
				b.codigobanco::integer=tr.codigobanco::integer
				full outer join tblfacturacioncliente f
				on  f.id_registro=tr.id_consecutivo
		
				full outer join tbldirecciones d  
				on d.d_coddocumento=tr.coddocumento and d.d_codigobanco::integer=tr.codigobanco::integer and d.d_namearchivo=tr.namearchivo and d.d_codetipo='1' and d.id_afiliado=tr.id_consecutivo
				full outer join tblmarcapos marc
				on marc.codtipo=tr.codmarcapos 
				full outer join tbltipopos pos
				on pos.tipopos=tr.tipopos
				full outer join tblventapos v
				on v.marcapos=tr.codmarcapos and v.tipopos=pos.codtipo
				left join tblestados e 
				on e.id_estado=d.d_estado
				left join tblmunicipios m 
				on m.id_municipio=d.d_municipio
				left join tblparroquias par 
				on par.id_parroquia=d.d_parroquia
				LEFT join (SELECT count(p_idafiliado)as pagos, p_idafiliado FROM tblpagos 
				 group by p_idafiliado) pago
				 on pago.p_idafiliado=tr.id_consecutivo
				where   tr.id_consecutivo=idregistro
				and tr.codestatus in ('1','2');
						
	End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spmostrarclientefacturacion (integer)
  OWNER TO postgres;
--ELECT * FROM spmostrarclientefacturacion(5133)
