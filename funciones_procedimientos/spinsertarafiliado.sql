﻿-- Function: spinsertarafiliado(integer, date, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION spinsertarafiliado(integer, date, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION spinsertarafiliado(consecutivo integer, fenvio date, cliente character varying, prefijobanco character varying, tipopv character varying, cantpv integer, tipoline character varying, modelonegocio character varying, tipcliente character varying, razon character varying, rif character varying, formapag character varying, transferencia character varying, afiliado character varying, usuariocarg character varying)
  RETURNS boolean AS
$BODY$

Begin
	 update clie_tblclientepotencial set 							  
	  nafiliacion=afiliado,  idstatus='2', fecharecepcomercializacion=now()::date, usuariorecibe=usuariocarg
	  where  fechaenviocomercializacion=fenvio and codigobanco::integer=prefijobanco::integer and tipopos=tipopv and cant_pos_confirm=cantpv
	  and tipolinea=tipoline and tipomodelonegocio=modelonegocio and tipocliente=tipcliente and coddocumento=rif
	  and codestatus in ('1','2') and idstatus='1';
	return true;
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spinsertarafiliado(integer, date, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
