﻿-- Function: spasignarserialpos(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION spasignarserialpos(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION spasignarserialpos(secuencial character varying,ifechaentrega character varying,  numeroterminal character varying, 
serialposasignado character varying, numerosim character varying,  inroafiliacion character varying, inamearchivo character varying, 
usuario character varying)
  RETURNS integer AS
$BODY$

declare existeserial bigint;
declare existeinventario bigint;
declare existeafiliadocorrelativo bigint;
declare existeafiliado bigint;
	Begin

		existeserial:= (select count(*)  from tblserialpos where serial=serialposasignado); 							
                existeinventario:= (select count(serialpos)  from tblinventariopos where serialpos=serialposasignado and nroafiliacion!=inroafiliacion); 

		if(existeserial=0)then
                return 0;
		end if;
                
                if (existeserial<>0 and existeinventario=0)then
			existeafiliadocorrelativo:= (select correlativo::bigint  from tblinventariopos where serialpos=serialposasignado and nroafiliacion=inroafiliacion); 

			existeafiliado:= (select count(serialpos)  from tblinventariopos where serialpos=serialposasignado and nroafiliacion=inroafiliacion); 

			if (existeafiliadocorrelativo!=secuencial::bigint and secuencial!='0')then
				return 0;
			end if;
			
			if(existeafiliado!=0)then
				update tblinventariopos 
				set  numterminal = CASE 
							WHEN numeroterminal IS NULL OR numeroterminal = '' THEN numterminal
							ELSE numeroterminal END, 
					  numsim=    CASE 
							WHEN numerosim IS NULL OR numerosim = '' THEN numsim
							ELSE numerosim END,
					  fechaent=      CASE 
							WHEN ifechaentrega IS NULL OR ifechaentrega = '' THEN fechaent
							ELSE ifechaentrega::date END,
					usuariocarga=usuario, fechacarga=now()
				where nroafiliacion=inroafiliacion and namearchivo = inamearchivo and serialpos=serialposasignado;
				return 1;

					
			end if;
			if (existeafiliado=0) then
				 INSERT INTO tblinventariopos(serialpos, numterminal, numsim, fechaent, nroafiliacion, namearchivo, correlativo, usuariocarga, fechacarga)
				 VALUES (serialposasignado, numeroterminal, numerosim,ifechaentrega::date ,inroafiliacion, inamearchivo,secuencial,usuario, now());

				 UPDATE tblserialpos SET estatus='1' where serial=serialposasignado;
				 return 1;
			end if;
		end if;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spasignarserialpos(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;

--'62647'

--select * from tblinventariopos
--DELETE FROM tblinventariopos WHERE SERIALPOS='62648'
--select * from tblserialpos where serial='62649'
--UPDATE tblserialpos SET ESTATUS='0' WHERE serial='62648'

--select * from spasignarserialpos('0','2018-03-01','435345','62648','','16546544','BANCOprueba_23102017_4.csv','JROJAS')

--select * from spasignarserialpos('0','','','62647','434325','16546544','BANCOprueba_23102017_4.csv','JROJAS')

--select * from spasignarserialpos('0','2018-03-01','','62648','','16546544','BANCOprueba_23102017_4.csv','JROJAS')