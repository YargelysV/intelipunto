﻿-- Function: spvertipodemodelo()

-- DROP FUNCTION spvertipodemodelo();

CREATE OR REPLACE FUNCTION spvertipodemodelo()
  RETURNS SETOF tblmodelonegocio AS
$BODY$
	
	Begin
		return 	QUERY SELECT * FROM tblmodelonegocio;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spvertipodemodelo()
  OWNER TO postgres;
