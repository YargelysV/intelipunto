﻿-- Function: spbuscarclienter(integer)

-- DROP FUNCTION spbuscarclienter(integer);

CREATE OR REPLACE FUNCTION spbuscarclienter(IN cliente integer)
  RETURNS TABLE(fecharecepcion date, codigobanco integer, ibp character varying, usuario character varying, fechacarga date,
   namearchivo character varying, sizearchivo character varying, cantidad bigint) AS
$BODY$
	
	Begin
		return 	QUERY 
			select p.fecharecepcion::date, p.codigobanco::integer, b.ibp, p.usuario, p.idate::date, p.namearchivo, p.sizearchivo, 
			count(p.codigobanco) as cantidad
			from clie_tblclientepotencial p
			inner join tblbanco b 
			on b.codigobanco::integer=p.codigobanco::integer
			where  p.codigobanco::integer=cliente::integer
			group by p.fecharecepcion::date, p.codigobanco::integer, b.ibp,  p.usuario, p.idate::date, p.namearchivo, p.sizearchivo
			order by p.idate::date;
	End;
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarclienter(integer)
  OWNER TO postgres;

