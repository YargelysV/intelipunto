﻿-- Function: sp_indicainteres(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION sp_indicainteres(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_indicainteres(icoddocumento character varying, inombrearchivo character varying,idireccion character varying, isuario character varying, iinteres character varying, inafiliado character varying, iejebanco character varying, iejeinteligensa character varying)
  RETURNS boolean AS
$BODY$

DECLARE result int;
Begin			
			
                if(iinteres='Si')then
		INSERT INTO tblgestioncliente(g_nafiliado,g_estatuspos,g_fechacontacto,g_observacion,g_coddocumento,g_nombrearchivo,g_usuariocarga,g_fechacarga,g_codejecutivobanco,g_codejecutivointeligensa,direccion)
	        values(inafiliado, 3 , now(), 'Si esta interesado',icoddocumento,inombrearchivo,isuario,now(),
		CASE 
	        WHEN iejebanco IS NULL OR iejebanco = '' THEN NULL 
		ELSE iejebanco END,
                CASE 
	        WHEN iejeinteligensa IS NULL OR iejeinteligensa = '' THEN NULL 
		ELSE iejeinteligensa END,
		idireccion);
	        return true;
	        end if;
	        if(iinteres='No')then
	        INSERT INTO tblgestioncliente(g_nafiliado,g_estatuspos,g_fechacontacto,g_observacion,g_coddocumento,g_nombrearchivo,g_usuariocarga,g_fechacarga,g_codejecutivobanco,g_codejecutivointeligensa,direccion)
	        values(inafiliado, 4 , now(), 'No esta interesado',icoddocumento,inombrearchivo,isuario,now(),
		CASE 
	        WHEN iejebanco IS NULL OR iejebanco = '' THEN NULL 
		ELSE iejebanco END,
	        CASE 
	        WHEN iejeinteligensa IS NULL OR iejeinteligensa = '' THEN NULL 
		ELSE iejeinteligensa END,
		idireccion);
	        return true;
	        end if;
	return false;			
			
End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_indicainteres(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO postgres;
