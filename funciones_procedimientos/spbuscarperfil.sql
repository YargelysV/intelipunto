﻿-- Function: spbuscarperfil(integer)

-- DROP FUNCTION spbuscarperfil(integer);

CREATE OR REPLACE FUNCTION spbuscarperfil(IN perfil integer)
  RETURNS TABLE(correlativo bigint, iestados integer, imunicipios integer, iparroquias integer, itipozona integer, desczona character varying, codperfil integer, preciozona numeric, iusuario character varying, ifecha timestamp with time zone, perfiles text) AS
$BODY$
	
	Begin
		return 	QUERY 
		       SELECT row_number()  OVER(order by t.estados,t.municipios,t.parroquias asc) as correlativo,
			t.estados, t.municipios, t.parroquias, t.tipozona, 
			b.desczona, 
			t.cod_perfil, 
			b.preciozona, 
			t.usuario, now(),
			concat_ws('/', t.estados,t.municipios,t.parroquias,t.cod_perfil) as perfiles
			from tblgestionmantenimiento as t
			inner join tbltipozona as b
			on b.codzona = t.tipozona and b.cod_perfil::integer=perfil::integer
			WHERE t.cod_perfil::integer=perfil::integer
			order by row_number() OVER(order by correlativo), t.tipozona;
	End;


	

-- select * from spbuscarperfil(2)
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION spbuscarperfil(integer)
OWNER TO postgres;
