﻿-- Function: sp_crudpago(numeric, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, character varying, character varying, text, character varying)

-- DROP FUNCTION sp_crudpago(numeric, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, character varying, character varying, text, character varying);

CREATE OR REPLACE FUNCTION sp_crudpago(operacion numeric, iidafiliado integer, ifechapago character varying, ihorapago character varying, ibancoorigen character varying, ibancodestino character varying, iformapago integer, imoneda integer, imonto character varying, ireferencia character varying, idepositante character varying, icapture character varying, iobservacion text, iusuariocarga character varying)
  RETURNS boolean AS
$BODY$

      DECLARE existe bigint;
	Begin
			CASE operacion

				when 2 then
					INSERT INTO tblpagos(p_idafiliado, p_bancoorigen,p_bancodestino,p_formapago,p_tipomoneda,p_monto,p_referencia,p_nombredepositante,p_fechapago,p_horapago,p_imagenpago,p_observacion,p_usuariocarga,p_fechacarga )
					values(                iidafiliado,ibancoorigen ,ibancodestino ,iformapago ,imoneda     ,imonto ,ireferencia ,idepositante,ifechapago::date,ihorapago,icapture,iobservacion, iusuariocarga,now());
									
					existe:=(SELECT count(*)FROM tblfacturacioncliente WHERE id_registro=iidafiliado);
					IF (existe>0)then 	
					update  tblfacturacioncliente set estatus=0 where id_registro=iidafiliado;
					else
                                        -- select * from tblfacturacioncliente
                                        insert into tblfacturacioncliente(id_registro,fecha_creacion_registro,usuario_actualiza_estatus,fecha_act_estatus)
                                        VALUES(iidafiliado,now(),iusuariocarga,now());
					end if;
					return true;

				when 3 then
						
					update tblpagos set 
					 -- select * from tblpagos
					  p_fechapago = case 
							when ifechapago is null or ifechapago = '' then p_fechapago
							else ifechapago::date end, 
					  p_horapago  = case 
							when ihorapago is null or ihorapago = '' then p_horapago
							else ihorapago end,
					 p_bancoorigen= case 
							when ibancoorigen is null or ibancoorigen = '0' then p_bancoorigen
							else ibancoorigen end,
					p_bancodestino= case 
							when ibancodestino is null or ibancodestino = '0' then p_bancodestino
							else ibancodestino end,
					p_formapago   = case 
							when iformapago is null or iformapago = 0 then p_formapago
							else iformapago end,
					p_tipomoneda  = case 
							when imoneda is null or imoneda = 0 then p_tipomoneda
							else imoneda end,
					p_monto = case 
							when imonto is null or imonto = '' then p_monto
							else imonto end,
					p_referencia = case 
							when ireferencia is null or ireferencia = '' then p_referencia
							else ireferencia end,						
				 p_nombredepositante =  case 
							when idepositante is null or idepositante = '' then p_nombredepositante
							else idepositante end,
					p_imagenpago =  case 
							when icapture is null or icapture = '' then p_imagenpago
							else icapture end,
					p_observacion =  case 
							when iobservacion is null or iobservacion = '' then p_observacion
							else iobservacion end,																				
					p_usuariocarga=iusuariocarga, p_fechacarga=now()
					where p_idafiliado=iidafiliado and p_referencia=ireferencia;
					return true;
					--                select * from tblpagos where p_idafiliado=5133 and p_referencia='00006'
					
				else 
				return false;
			end case;	
		End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_crudpago(numeric, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, character varying, character varying, text, character varying)
  OWNER TO postgres;
