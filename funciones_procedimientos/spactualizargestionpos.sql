﻿-- Function: spactualizargestionpos(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION spactualizargestionpos(character varying,character varying, character varying, character varying, character varying, integer, character varying, integer, character varying);

CREATE OR REPLACE FUNCTION spactualizargestionpos(operacion numeric,serialposasignado character varying,inroafiliacion character varying,  inamearchivo character varying , 
usuario character varying, fecharecepalma character varying,  tecnico integer, fechagest character varying, estatusins integer,
fechainst character varying, fechac character varying)
  RETURNS boolean AS
$BODY$

	Begin
--select * from tblgestionserviciotecnico
CASE operacion
	when 3 then

		update  tblgestionserviciotecnico
		set  
			 fecharecepalmacen=CASE 
					WHEN fecharecepalma IS NULL OR fecharecepalma = '' THEN fecharecepalmacen
					ELSE fecharecepalma::date END,

			  codtecnico = CASE 
					WHEN tecnico IS NULL OR tecnico = 0 THEN codtecnico
					ELSE tecnico END, 
			  fechagestion= CASE 
					WHEN fechagest IS NULL OR fechagest = '' THEN fechagestion
					ELSE fechagest::date END,
			 estatus = CASE 
					WHEN estatusins IS NULL OR estatusins = 0 THEN estatus
					ELSE estatusins END, 		
			  fechainstalacion=      CASE 
					WHEN fechainst IS NULL OR fechainst = '' THEN fechainstalacion
					ELSE fechainst::date END,
			usuariocarga=usuario
		where nroafiliado=inroafiliacion and nombrearchivo = inamearchivo and serialpos=serialposasignado and fechacarga=fechac::timestamp with time zone ;
		return true;
	when 2 then
		insert into tblgestionserviciotecnico
		values(inroafiliacion,serialposasignado,inamearchivo,fecharecepalma::date,tecnico,fechagest::date,estatusins,
		CASE 
		WHEN fechainst IS NULL OR fechainst = '' THEN NULL
		ELSE fechainst::date END, now(),usuario);
		return true;

	else 
		return false;
end case;	

	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION spactualizargestionpos(numeric,character varying, character varying, character varying, character varying, character varying, integer, character varying, integer, character varying,character varying)
  OWNER TO postgres;

--'62647'

--select * from spactualizargestionpos('3','62647','16546544','BANCOprueba_23102017_4.csv','JROJAS','2018-01-01',2,'2018-01-01',1,'')
--select * from tblgestionserviciotecnico

--delete from tblgestionserviciotecnico where fechainstalacion is null
