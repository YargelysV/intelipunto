﻿

CREATE FUNCTION sp_senalinalambrica()
  RETURNS SETOF tblsenalinalambrica AS
$BODY$
	
	Begin
		return 	QUERY 
			select * from tblsenalinalambrica ;
	End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_senalinalambrica()
  OWNER TO postgres;
