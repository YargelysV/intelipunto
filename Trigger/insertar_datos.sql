﻿-- Function: insertar_datos()

-- DROP FUNCTION insertar_datos();

CREATE OR REPLACE FUNCTION insertar_datos()
  RETURNS trigger AS
$BODY$ 
BEGIN
		
		UPDATE clie_tblclientepotencial AS tr
		SET estado=subquery.id_estado
		from (
			SELECT  es.id_estado, es.estado, tr1.id_consecutivo 
			from clie_tblclientepotencial tr1
			inner join tblestados es
			on upper(es.estado)=upper(tr1.estado)
			where substring(tr1.estado from '[a-zA-Z]+')!='' and upper(es.estado)=upper(tr1.estado)) as subquery
		WHERE upper(subquery.estado)=upper(tr.estado) and tr.id_consecutivo=subquery.id_consecutivo;

		UPDATE clie_tblclientepotencial SET estado='0' WHERE estado IS NULL  or estado='';

		UPDATE clie_tblclientepotencial AS tr
		SET municipio=subquery.id_municipio
		from (
			SELECT mu.id_municipio, mu.municipio, tr1.estado, tr1.id_consecutivo 
			from clie_tblclientepotencial tr1
			inner join tblmunicipios mu
		on upper(mu.municipio)=upper(tr1.municipio)  and mu.id_estado::integer=tr1.estado::integer
		where substring(tr1.municipio from '[a-zA-Z]+')!='' and upper(mu.municipio)=upper(tr1.municipio) and mu.id_estado::integer=tr1.estado::integer) as subquery
		WHERE upper(subquery.municipio)=upper(tr.municipio) and subquery.estado=tr.estado  and tr.id_consecutivo=subquery.id_consecutivo;

		UPDATE clie_tblclientepotencial SET municipio='0' WHERE municipio IS NULL  or municipio='';
		
		UPDATE clie_tblclientepotencial AS tr
		SET PARROQUIA=subquery.id_parroquia
		from (
			SELECT pa.id_parroquia, pa.parroquia,tr1.municipio, tr1.id_consecutivo 
			from clie_tblclientepotencial tr1
			inner join tblparroquias pa
		on upper(pa.parroquia)=upper(tr1.parroquia)  and pa.id_municipio::integer=tr1.municipio::integer
		where substring(tr1.parroquia from '[a-zA-Z]+')!='' and upper(pa.parroquia)=upper(tr1.parroquia) and pa.id_municipio::integer=tr1.municipio::integer) as subquery
		WHERE upper(subquery.parroquia)=upper(tr.parroquia) and subquery.municipio=tr.municipio  and tr.id_consecutivo=subquery.id_consecutivo;

		UPDATE clie_tblclientepotencial SET parroquia='0' WHERE parroquia IS NULL  or parroquia='';
	
		
RETURN OLD;
		end;	

		
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION insertar_datos()
  OWNER TO postgres;


		
