var DemoApp = angular.module('DemoApp', ['dx']);

DemoApp.service('informacion', ['$http',
  function($http) {
    this.promiseCache = null;
    this.getData = function(url) {
      return this.promiseCache || (this.promiseCache = $http.get(url));
    }
  }
]);

DemoApp.controller('DemoController',['$scope', 'informacion', function DemoController($scope,informacion) {
   informacion.getData('api/select')
    .then(function(datos) {
          $scope.datos = datos.data
  
            $scope.gridOptions = {
            dataSource:$scope.datos,
                    selection: {
                mode: "multiple"
            },
            "export": {
                enabled: true,
                fileName: "Clientes",
                allowExportSelectedData: true
            },
            groupPanel: {
                visible: true
            },
            columns: [
                {
                    dataField: "ibp",
                    caption: "Nombre de Banco",
                    width: 60
                }, "codigobanco",
                "tipopos", 
                "canpos",
                "tipolinea", {
                    dataField: "modelonegocio",
                    caption: "Posición",
                    width: 130
                }, {
                    dataField: "tipocliente",
                    width: 100
                }, {
                    dataField: "razonsocial",
                    dataType: "date",
                    width: 100
                }
            ]
        };
    },function (error){
      console.log(error, 'can not get data.');
  }); 
}]);