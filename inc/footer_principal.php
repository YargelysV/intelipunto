
   <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="js/passw/jquery.min.js"></script>
<script type="text/javascript" src="js/passw/strength.js"></script>
<script type="text/javascript" src="js/passw/js.js"></script>


<script>
$(document).ready(function($) {
  
$('#secretkey').strength({
            strengthClass: 'strength',
            strengthMeterClass: 'strength_meter',
            strengthButtonClass: 'button_strength',
            strengthButtonText: 'Mostrar Contraseña',
            strengthButtonTextToggle: 'Ocultar Contraseña'
        });

});
</script>

</body>
</html>
