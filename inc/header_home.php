<?php

///ini_set("session.use_only_cookies","1");


if (isset($_SESSION["session_user"])) {
  //VERIFICAR MODULOS ACTIVOS
  $var = $_SESSION["session_user"];

  require("controller/Controladormodulos.php");
  $controladormodulos = new controladorModulo();
  $modulos = $controladormodulos->spverificarmodulosactivos($_SESSION["session_user"]);

  $_SESSION["administracion"] = 0;
  $_SESSION["clientespotenciales"] = 0;
  $_SESSION["comercializacion"] = 0;
  $_SESSION["ejecutivo"] = 0;
  $_SESSION["serviciotecnico"] = 0;
  $_SESSION["superusuario"] = 0;
  $_SESSION["acuerdosservicios"] = 0;
  $_SESSION["reportegestion"] = 0;
  $_SESSION["gestionmantenimiento"] = 0;
  $_SESSION["serviciomantenimiento"] = 0;
  $_SESSION["inventariopos"] = 0;
  $_SESSION["reporteejecutivo"] = 0;
  $_SESSION["captacioncliente"] = 0;
  $_SESSION["encuesta"] = 0;
  $_SESSION["reporteencuesta"] = 0;
  $_SESSION["activarequipos"] = 0;
  $_SESSION["controlacceso"] = 0;
  $_SESSION["visitatecnica"] = 0;
  $_SESSION["reporteprofit"] = 0;
  $_SESSION["modulobanesco"] = 0;
  $_SESSION["riesgo_medido"] = 0;
  $_SESSION["cssBanesco"] = 0;
  $_SESSION["onCssBanesco"] = "";
  //para activar css banesco  default Off JuniorsQ
  if ($_SESSION["onCssBanesco"] == "banesco")
    $_SESSION["cssBanesco"] = 1;
  else
    $_SESSION["cssBanesco"] = 0;

  /*variables para validar  documentación correspondiente*/
  $_SESSION["doc_comercial"] = 0;
  $_SESSION["doc_administracion"] = 0;
  $_SESSION["doc_servicio_tecnico"] = 0;
  $_SESSION["doc_reporte_gestion"] = 0;
  $_SESSION["doc_tablas_mantenimiento"] = 0;
  $_SESSION["doc_ejecutivos_venta"] = 0;
  $_SESSION["doc_servicio_mantenimiento"] = 0;
  $_SESSION["doc_inventario"] = 0;
  $_SESSION["doc_reporte_ejecutivo"] = 0;
  $_SESSION["doc_cliente_potencial"] = 0;

  $_SESSION["codtipousuario"] = 0;
  $_SESSION["ejebanco"] = 0;
  $_SESSION["coordbanco"] = 0;
  $_SESSION["coordinadordos"] = 0;
  /*para validación en administrador de usuarios*/
  $usuario = $_SESSION["session_user"];
  require_once("controller/controlador.php");
  $controlador = new controladorUsuario();
  $dataUsser = $controlador->ver($usuario);
  $_SESSION["area_usuario"] = $dataUsser['codarea'];
  $_SESSION["coordbanco"] = $dataUsser['coordbanco'];
  $_SESSION["coordinadordos"] = $dataUsser['coordinadordos'];
  $_SESSION["coddocumento_usuario"] = $dataUsser['tipodoc'] . '-' . $dataUsser['coddocumento'];
  /*cambio estatus de actividad en registro*/
  $id_registro = isset($_SESSION["id_registro_session"]) ? $_SESSION["id_registro_session"] : "0";
  $activo = 0;
  $user = "";
  $ctrlcomer = new controladorModulo();
  $actregistro = $ctrlcomer->sp_actividad($id_registro, $activo, $user);
  $_SESSION["id_registro_session"] = "0";

  /*cambio estatus de actividad en registro de encuesta*/
  $idcliente = isset($_SESSION["id_registro_sessionenc"]) ? $_SESSION["id_registro_sessionenc"] : "0";
  // echo $idcliente;
  $activo = 0;
  $user = "";
  $actregistroenc = $ctrlcomer->sp_actividadencuesta($idcliente, $activo, $user);
  $_SESSION["id_registro_sessionenc"] = "0";


  $controladormodulosge = new controladorModulo();
  $modulosge = $controladormodulosge->spverificarmodulosactivos($_SESSION["session_user"]);
  $MODULO = 0;
  $modulo_adm = 0;
  $modulo_co = 0;
  while ($rowmodulosge = pg_fetch_array($modulosge)) {

    if ($rowmodulosge['codmodulo'] == 'GE') {
      $MODULO = 1;
    }

    if ($rowmodulosge['codmodulo'] == 'HR') {
      $_SESSION["habilitaregistro"] = $rowmodulosge['codtipo'];
      $_SESSION["superusuario"] = $rowmodulosge['codtipousuario'];
    }
    if ($rowmodulosge['codmodulo'] == 'ADM') {
      $modulo_adm = 2;
    }
  }
  $hoy = date("d-m-Y");

  //contador de notificaciones
  $ctrlmodel = new controladorModulo();
  $count_noti = $ctrlmodel->sp_countnotificaciones($modulo_adm);
  $dn = pg_fetch_assoc($count_noti);
  $cantidad_n = $dn['sp_countnotificaciones'];


}

  $ctrlPagos = new controladorModulo();
  $r=$ctrlPagos->sp_pagosnoconf();
  $countpagos = pg_num_rows($r);

  $ctrlrecompra = new controladorModulo();
  $recompra=$ctrlrecompra->sp_recompranoconf();
  $countrecompra = pg_num_rows($recompra);

  $ctrltasa = new controladorModulo();
  $tasaxdia=$ctrltasa->sp_tasacambio();
  $tasa = pg_fetch_assoc($tasaxdia);
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <title><?php echo $tituloPagina; ?></title>
  <?php require('funciones.php');
  include_once('model/conexion_15.php'); ?>

  <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <link rel="icon" href="img/Intelipunto.ico" type="image/x-icon">
  <link rel="shortcut icon" href="img/Intelipunto.ico" type="image/x-icon" />

<?php
if(isset($verificamoscomercializacion)){
?>
  <!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/themes/bootstrap.min.css"/>

<!-- 
    RTL version
-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/alertify.rtl.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/themes/default.rtl.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/themes/semantic.rtl.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.14.0/build/css/themes/bootstrap.rtl.min.css"/>

<?php
} else{
?>

<!--  -->
  <script src="alertify/lib/alertify.min.js"></script>
  <script type="text/javascript" src="js/jquery.js"></script>

  <link rel="stylesheet" href="alertify/themes/alertify.core.css" />
  <link rel="stylesheet" href="alertify/themes/alertify.default.css" id="toggleCSS" />

  <?php
} 
?>
  <!--angular -->

  <script src="js/angular.min.js" type="text/javascript"></script>
  <script src="js/angular-route.min.js"></script>
  <script defer src="js/material.min.js"></script>
  <script defer src="js/jszip.min.js"></script>
  <script defer src="js/dx.all.js"></script>
  <script defer src="js/dx.aspnet.data.js"></script>


  <!--busqueda -->
  <script src="js/jquery-ui-1.12.1/jquery-ui.js" type="text/javascript"></script>
  <script src="js/jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript"></script>
  <!--galeria -->
  <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

  <!-- Datatale css -->
  <link type="text/css" rel="stylesheet" href="DataTable/datatable.css">
  <link type="text/css" rel="stylesheet" href="DataTable/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="DataTable/dataTables.bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="DataTable/jquery.dataTables.min">
  <link type="text/css" rel="stylesheet" href="DataTable/buttons.dataTables.min1.css">
  <link type="text/css" rel="stylesheet" href="DataTable/bootstrap1.min">

  <!-- Custom CSS -->
  <link href="dist/css/sb-admin-2.css" rel="stylesheet">
  <link href="css/mibloque.css" rel="stylesheet">
  <link href="css/dx.spa.css" rel="stylesheet">
  <link href="css/dx.common.css" rel="stylesheet">
  <link href="css/dx.light.css" rel="stylesheet">


  <link type="text/css" rel="stylesheet" href="css/themes/calendar_white.css" />
  <link type="text/css" rel="stylesheet" href="css/themes/bubble_default.css" />

  <!-- helper libraries -->
  <script src="js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
  <!-- daypilot libraries -->
  <script src="js/daypilot/daypilot-all.min.js" type="text/javascript"></script>

  <!-- mascara numero de tlf -->
  <script src="js/jquery/jquery.min.js" type="text/javascript"></script>
  <script src="js/jquery/jquery1.mask.js" type="text/javascript"></script>

  <link href="libs/morris.css" rel="stylesheet">
  <script src="libs/morris.min.js" type="text/javascript"></script>
  <script src="js/datatable.js" type="text/javascript"></script>
   <!--React-->
    <script type="text/javascript" src="React/react.production.min.js" crossorigin></script>
    <script type="text/javascript" src="React/react-dom.production.min.js" crossorigin></script>
    <!-- DatePicker and dependencies-->
    <script type="text/javascript" src="React/date-object.min.js"></script>
    <script type="text/javascript" src="React/browser.min.js"></script>
    <script type="text/javascript" src="React/pickerbrowser.min.js"></script>

    <!-- Optional Plugin -->
    <script type="text/javascript" src="React/date_picker_header.browser.js"></script>



      <?php if ($pagina=="reportebanesco" || $pagina=="afiliadobanesco") {  ?>
          
        <script src="js/javascript/afiliadoBanesco.js" type="text/javascript"></script>

      <?php } else if ($pagina=="inventariobanesco") { ?>

        <script src="js/javascript/inventarioBanesco.js" type="text/javascript"></script>

       <?php } else if ($pagina=="reporteinventariobanesco" || $pagina=="reportebanescogeneral" || $pagina=="reingresoequipos") { ?>

        <script src="js/javascript/inventarioBanesco.js" type="text/javascript"></script>

         <?php } else if ($pagina=="equiposbanesco") { ?>

        <script src="js/javascript/confirmacionBanesco.js" type="text/javascript"></script>

        <?php } else if ($pagina=="desaf-desincorpBanesco") { ?>

        <script src="js/javascript/confirmacionBanesco.js" type="text/javascript"></script>

      <?php } else {?>

            <script src="js/header.js" type="text/javascript"></script>
            <script src="js/buscarusuario.js" type="text/javascript"></script>
            <script src="js/functions.js" type="text/javascript"></script>
            <script src="js/app.js" type="text/javascript"></script>

      <?php  } ?>  





  <script>
    function mascaras() {
      $('.phone').mask('(0000)-000-0000');
      $('.money').mask('#.##0,00', {
        reverse: true
      });


    }
  </script>

  <style type="text/css">
    .navbar-default .navbar-top-links .dropdown .badge {
      position: absolute;
      margin-left: 0.75rem;
      top: 0.5em;
      font-weight: 700;
      font-size: 0.9rem;
    }

    .badge-danger {
      color: #fff;
      background-color: #dc3545;
    }

    .badge {
      display: inline-block;
      padding: .30 .8em;
      font-size: 80%;
      font-weight: 700;
      line-height: 1;
      text-align: center;
      white-space: nowrap;
      vertical-align: baseline;
    }

    .imagen {
      margin: 0;
      display: inline-block;
      height: 25px;
      float: none;
    }

    #detalles{

background-color:#337AB7; 
height: 80px;
}

    /* Definición de la animación 
    @keyframes moverTexto {
      0% { transform: translateX(200px); }
      50% { transform: translateX(20px); }
      100% { transform: translateX(20px); }
    }*/

    /* Definición de la animación */
    @keyframes moverTexto {
      0% { opacity: 0; transform: translateX(100%); }
      25% { opacity: 1; }
      75% { opacity: 1; }
      100% { opacity: 0; transform: translateX(-100%); }
    }

    /* Aplicación de la animación al elemento */
    .texto-movimiento {
      white-space: nowrap; /* Evita el salto de línea */
      /*overflow: hidden;  Oculta el texto que se desborda */
      animation: moverTexto 10s linear infinite; /* 8s es la duración de la animación */
    }
    
    .navbar.navbar-default.navbar-static-top{
        margin-bottom: 0px;
      
      }
      
      .nombrecito{
       color:#818181;
      }

      #admins1{
        visibility: hidden;
      }

      /* aqui empieza lo importante */
    @media screen and (max-device-width:768px) {
    
      body {
        font-size: 35px;
        
      }
      
      .navbar.navbar-default.navbar-static-top{
        padding-top: 30px;
        background-color: #f0ad4e;
      
      }

      .navbar-default.sidebar{

        all: unset;
        display: flex;
      }

      .navbar-brand{
        font-size:100px; 
        font-weight:bolder;
      }

      .sidebar-nav.navbar-collapse{

        overflow: scroll;
        position: fixed; bottom: 0; left: 0; right: 0; width: 100%; background-color: #f1f1f1; padding: 10px; z-index: 1000;
      }
      
      #side-menu{
        display: flex;
        font-size:36px; 
      }
      .fa{
        font-size: 80px;
      }
      .campanita{
        visibility: hidden;

      }
      li{
        all: unset;
        width: auto;
        align-items: center;
        text-align: center;
        font-weight: bold;
        margin: 0 20px;
      }
      .arrow{
        visibility: hidden;

      }
      .textototote{
      font-size: 24px;
    }

      .collapse.in{

       display: flex;
      }
      .H_doc{

        visibility: hidden;
      }
      .texto-movimiento{
        visibility: hidden;
      }
      .dropdown-toggle{

        font-size: 20px;
      }
      .nombrecito{
        font-size: 30px;
        color:#fff;
      }

      .invisible-mobile{
        display: none;
      }

      .navbar-brand.tasabcv{

        color: #fff;
      }
      .cerrar{

        font-display: none;
        font-size: 1px;
      }
      #page-wrapper{
        margin-left: 0;

      }
      #usuarioss{

        color: #fff;
        margin-right: -5%;
      }
      #textogrande{

        font-size: 30px;
      }
      #admins1{
        visibility: visible;
      }

      .page-header{
        font-size: 40px;
      }

      #clientes{
        margin-top: 30px;
        width: 99%;
        font-size: 45px;
      }

      #fecha{
        margin-top: 30px;
        width: 99%;
        font-size: 45px;
      }

      .col-lg-5{
        font-size: 50px;

      }

      .panel-footer{
        height: auto;
        padding: 30px;
      }

      .btn {
        font-size: 40px;
      }

      .panel-heading{

        height: auto;
        padding: 30px;
        font-size: 60px;
        font-weight: bolder;
      }

      #headingEspecial{

        font-size: 50px;
      }

      .form-control{
        font-size: 50px;
        height: 98px;
      }

      #tableEspacio{
     
        margin-top: 20px;
        margin-bottom: 300px;
      }

      #navProblematico{
        width: 100%;
        height: 200px;
        margin-top: 30px;
        border-radius: 20px;
      }

      #textoHeader{

        font-size: 50px;
        height: auto;
        line-height: 40px;
      }

      #theRow{


        height: auto;
      }

      #detalles{

        width: 100%;
        height: auto;
        padding: 30px;
        margin-top: 30px;
        margin-bottom: 100px;

      }

      #marginAdBajo{

        margin-bottom: 30px;
      }

      #textoConEspacio{
        font-size: 40px;
        height: 150px;
        line-height: 60px;
        font-weight: bold;

      }

      #page-wrapper{

        margin-bottom: 200px;
      }

      #espacios1{

        line-height: 60px;
      }

      .container-fluid{

        margin-bottom: 200px;;
      }

      #busquedaEspecifica{

        margin-left: 5%;
        font-size: 40px;
      }


      /* aca acaba el media */
      /* no aceptes el cambio llamado patinaje, se carga multiples cambios de mobile a1 a2*/
    }
    
    
    }
  </style>
</head>

<body>


  <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" >
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <?php
        $_SESSION["codtipousuario"] = $dataUsser['codtipousuario'];
        $_SESSION["ejebanco"] = $dataUsser['ejebanco'];
        $_SESSION["coordbanco"] = $dataUsser['coordbanco'];
        $_SESSION["coordinadordos"] = $dataUsser['coordinadordos'];
        ?>

        <?php if ($_SESSION["contrasena"] != '1') { ?>
          <a href="home" id="navbarbrandlogo" class="navbar-brand tasabcv">Intelipunto</a>
          <br>
          <h4 class="texto-movimiento">Mesa de cambio BCV (USD): Bs. <?php echo $tasa['tasa_dolar']?> </h4>
        <?php } else { ?>
          <a class="navbar-brand">Intelipunto</a>
        <?php  } ?>
      </div>
      <!-- /.navbar-header -->

      <ul class="nav navbar-top-links navbar-right">
        <?php if ($modulo_adm == 2) {  ?>


          <li class="dropdown">
            <a class="dropdown-toggle hidden" data-toggle="dropdown" href="#" aria-expanded="false">
              <span class="badge badge-danger <?php if ($cantidad_n == 0) : echo "hidden";
                                              endif ?>"> <?php echo $cantidad_n; ?> </span><i class="fa fa-bell fa-fw" style="color:black;"></i>
            </a>

            <ul class="dropdown-menu dropdown-alerts">
              <?php
              $ctrlmodel = new controladorModulo();
              $datos_notificaciones = $ctrlmodel->sp_vernotificaciones($modulo_adm);
              $cont = 0;
              while ($r_data =  pg_fetch_array($datos_notificaciones)) {
                $cont++;
              ?>

                <li>
                  <a href="facturacionpos?cargar=buscarrif&var=<?php echo $r_data['n_afiliado']; ?>">
                    <div style="color:purple;">
                      <i class="fa fa-comment fa-fw"></i> <b><?php echo $r_data['descripcion']; ?> </b>- Af: <?php echo $r_data['n_afiliado']; ?>
                      <span class="pull-right text-muted small"><?php echo $r_data['fechacreada']; ?></span>
                    </div>
                  </a>
                </li>
                <?php if ($cont != $cantidad_n) : ?>
                  <li class="divider"></li>
                <?php endif ?>

              <?php  }
              ?> 
              <?php if ($cantidad_n == 0) { ?>

                <li>
                  <a href="#">
                    <div style="color:black;">
                      <i class="fa fa-comment fa-fw"></i> <b>No tiene Notificaciones </b>
                      <span class="pull-right text-muted small"><?php echo $hoy; ?></span>
                    </div>
                  </a>
                </li>

              <?php    } ?>
            </ul>
            <!-- /.dropdown-user -->
          </li>

        <?php   } ?>


        <li class="dropdown" >
          <?php if(($countpagos >= 1 || $countrecompra >= 1) && ($dataUsser['codtipousuario'] === 'A' || $dataUsser['codarea'] === '1')){?>
            <img class="campanita" width="70" height="70"  src="img/campana-de-notificacion.gif" >
          <?php } ?>

      </li>

        <li>
          <div class="invisible-mobile">
            <style>
              .doc {
                color: #818181;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 12px;
                font-weight: bold;
              }

              /* .fa.fa-file-pdf-o{
                color: red;
            }*/
              .H_doc:hover {
                background: #81818126;
              }
            </style>

            <?php if ($dataUsser['area'] === 'OPERACIONES') { ?>
              <a class="H_doc" href="documentacion" style="display:none;">
                <span class="fa fa-file-pdf-o" style="color: red;"> <span class="doc"> DOCUMENTACION </span>
                </span>
              </a>

            <?php } else if (($_SESSION["contrasena"] != '1') ||  ($_SESSION["area_usuario"])) { ?>


              <a class="H_doc" href="documentacion">
                <span class="fa fa-file-pdf-o" style="color: red;"> <span class="doc"> DOCUMENTACION </span>
                </span>
              </a>
            <?php } else { ?>
              <span class="fa fa-file-pdf-o" style="color: red;"> <span class="doc"> DOCUMENTACION </span>
              </span>
            <?php  } ?>

          </div>


      <li class="hidden">
        <a href="dashboard"><i style="color:#04B4AE;" class="fa fa-tasks fa-fw"> </i> Dashboard </a>

      </li>
      <li>




      </li>
      <!-- /.dropdown -->


      <li class="dropdown" id="usuarioss">
        <a class="dropdown-toggle" id="usuarioss" data-toggle="dropdown" href="#">
          <b class="nombrecito"><?php echo $var; ?></b>
          <i class="fa fa-user fa-fw" id="usuarioss"></i> <i class="fa fa-caret-down" id="usuarioss"></i>
        </a>

        <ul class="dropdown-menu dropdown-user" id="textogrande">

          <li><a href="password"><i class="fa fa-user fa-fw"></i> Cambiar Contraseña</a>
          </li>
          <?php if ($dataUsser['codtipousuario'] === 'A' && $dataUsser['codarea'] === '1') { ?>
            <?PHP if ($MODULO == 1) { ?>


              <?PHP if ($_SESSION["contrasena"] != '1') { ?>


                <li>
                  <a href="usuarios"><i class="fa fa-group fa-fw" alt="administrar cuentas">
                    </i> Administrar Usuarios</a>
                </li>

              <?PHP } else { ?>
                <li>
                  <a href="password"><i class="fa fa-group fa-fw" alt="administrar cuentas">
                    </i> Administrar Usuarios</a>
                </li>
              <?PHP } ?>
            <?PHP } ?>
          <?PHP } ?>
        </ul>

      </li>
      <li class="divider"></li>
      <li><a href="salir.php" class="cerrar"><i class="fa fa-sign-out fa-fw" id="usuarioss"></i> Cerrar Sesión</a>

      </li>

      <!-- /.dropdown-user -->
      </li>
      <!-- /.dropdown -->
      </ul>
      <!-- /.navbar-top-links -->


      <!-- /.navbar-static-side -->
    </nav>
    <div class="navbar-default sidebar" role="navigation">
      <div <?php if ($pagina == "password") {
              echo "class='hidden'";
            }  ?> class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <?php
            while ($rowmodulos = pg_fetch_array($modulos)) {

              if ($rowmodulos['codmodulo'] == 'ADM') {
                $_SESSION["administracion"] = $rowmodulos['codtipo'];
                $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                $_SESSION["doc_administracion"] = 1;
            ?>

          <li>
            <a href="#"><?php if($countpagos >= 1 || $countrecompra >= 1 ){?>
              <img  width="50" height="50"  src="img/notificaciones.gif" class="campanita">
            <?php }else{ ?>
              <i style="color:red;" class="fa fa-clipboard x3"></i>
            <?php }?>  
              <div id="notificacionpagos"  style="display:block;display:inline;"> <i style="color:#red;" class="fa fa-check-square-o x4" id="admins1"></i></div>Administración
             
            <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
             
          <li <?php if ($pagina == "facturacionpos") {
                    echo "class='active'";
                  } ?>>
            <a href="facturacionpos"><i style="color:red;" class="fa fa-files-o"></i>Facturación POS</a>
          </li>

         <!--  <li <?php if ($pagina == "facturacionmifi") {
                    echo "class='active'";
                  } ?>>
            <a href="facturacionmifi"><i style="color:red;" class="fa fa-file-text-o"></i> Facturación MIFI</a>
          </li> -->
          <li <?php if ($pagina == "pagosxconfirmar") {
                    echo "class='active'";
                  } ?>>
            <a href="pagosxconfirmar"><i style="color:red;" class="fa fa-check"></i>Aprobación de Pagos y Recompras<?php if($countpagos >= 1 || $countrecompra >= 1 ){?><img  width="35" height="35"  src="img/bombilla.gif" >
            <?php } ?></a>
          </li>
          <?php if (($dataUsser['area'] == 'SISTEMA') || ($dataUsser['area'] == 'BACKOFFICE')) { ?>
              <li>
                <a href="aprob_expediente"><i style="color:red;" class="fa fa-check-square-o iconcolorbanesco"></i> Aprobación de Expediente BackOffice</a>
              </li>
            <?php } ?>

             <?php if (($dataUsser['area'] == 'SISTEMA')) { ?>
              <li>
                <a href="verificacion_documentos"><i style="color:red;" class="fa fa-check-square-o iconcolorbanesco"></i> Verificación de Documentos</a>
              </li>
            <?php } ?>

           <?php if (($dataUsser['codarea'] == '1') || ($dataUsser['codarea'] == '6')) { ?>
              <li>
                <a href="desafiliacion"><i style="color:red;" class="fa fa-file-excel-o"></i> Desafiliacion Masiva</a>
              </li>
               <?php } ?>
          </ul>
         </li>
         

        <?php
                }



                if ($rowmodulos['codmodulo'] == 'CP') {
                  $_SESSION["clientespotenciales"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];

                  $_SESSION["doc_cliente_potencial"] = 1;
        ?>
        <li>
          <a href="#"><i style="color:black;" class="fa fa-cloud-upload icon-3x"></i> Clientes<span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li <?php if ($pagina == "clientes_potenciales") {
                  echo "class='active'";
                } ?>>
              <a href="clientesPotenciales"><i style="color:black;" class="fa fa-cloud-upload icon-3x"></i> Recepción Clientes</a>
            </li>
          </ul>
        </li>
        <?php
              }


              if ($dataUsser['codtipousuario'] == 'T' && $dataUsser['codarea'] == '9') {
                $_SESSION["comercializacion"] = $rowmodulos['codtipo'];
                $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                $_SESSION["doc_comercial"] = 1;
              } else {
                if ($rowmodulos['codmodulo'] == 'CO') {
                  $_SESSION["comercializacion"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_comercial"] = 1;
        ?>
          <li>
            <a href="#"><i style="color:#045FB4;" class="fa fa-phone-square x4"></i> Comercialización<span class="fa arrow"></span> </a>
            <ul class="nav nav-second-level">
              <li <?php if ($pagina == "gestion_Comercializacion") {
                    echo "class='active'";
                  } ?>>
                <a href="gestioncomercializacion"><i style="color:#045FB4;" class="fa fa-phone-square x4"></i> Gestión Comercial</a>
              </li>
            </ul>
          </li>
        <?php

                }
              }

/*              if ($rowmodulos['codmodulo'] == 'RP') {
                $_SESSION["reportegestion"] = $rowmodulos['codtipo'];
                $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                $_SESSION["doc_reporte_gestion"] = 1;

        ?>
        <li <?php if ($pagina == "reportegestion") {
                  echo "class='active'";
                } ?>> <a> <i title="" style="color:#A01010;" class="fa fa-list-alt"> </i> Reporte de Gestion <span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="reportegestion"> <i style="color:#A01010;" class="fa fa-list-alt"> </i> Asignacion de Afiliado</a>
            </li>
            <li>
              <a href="reporteadministracion"><i style="color:#A01010;" class="fa fa-clipboard x3"> </i> Administracion-Inventario-ERP</a>
            </li>

          </ul>
        </li>
        <?php
                }*/
                /*if ($rowmodulos['codmodulo'] == 'AB') {
                  $_SESSION["reportegestion"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_reporte_gestion"] = 1;

        ?>
        <li <?php if ($pagina == "reportebanesco") {
                  echo "class='active'";
                } ?>> <a> <img class="imagen" src="img/imagenbanesco.png">Afiliados Banesco <span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="reportebanesco"><i style="color:black;" class="fa fa-download icon-3x iconcolorbanesco"></i> Data de Afiliados</a>
            </li>
            <li>
              <a href="afiliadobanesco"> <i style="color:#5F9B36;" class="fa fa-list-alt icon-3x iconcolorbanesco"></i> Asignación de Afiliado</a>
            </li>


          </ul>
        </li>
        <?php
                }*/
                /*if ($rowmodulos['codmodulo'] == 'DD') {
                  $_SESSION["reportegestion"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_reporte_gestion"] = 1;

        ?>
        <li <?php if ($pagina == "desaf-desincorpBanesco") {
                  echo "class='active'";
                } ?>> <a><img class="imagen" src="img/imagenbanesco.png"> Desaf./Desincorporacion<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="desaf-desincorpBanesco"><i style="color:#3D9209;" class="fa fa-search x4 iconcolorbanesco"></i>Busqueda</a>
            </li>
          </ul>
        </li>
        <?php
                }*/
                  /*if ($rowmodulos['codmodulo'] == 'EB') {
                  $_SESSION["reportegestion"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_reporte_gestion"] = 1;

        ?>
        <li <?php if ($pagina == "equiposbanesco") {
                  echo "class='active'";
                } ?>> <a> <img class="imagen" src="img/imagenbanesco.png">Confirmación de Equipos <span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="equiposbanesco"><i style="color:black;" class="fa fa-check-square-o iconcolorbanesco"></i> Equipos inicializados</a>
            </li>

          </ul>
        </li>
        <?php
                }*/

                if ($rowmodulos['codmodulo'] == 'EJ') {
                  $_SESSION["ejecutivo"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_ejecutivos_venta"] = 1;

        ?>
        <li <?php if ($pagina == "Ejecutivo") {
                  echo "class='active'";
                } ?>><a> <i title="" style="color:orange;" class="fa fa-users x3"> </i> Ejecutivos de venta<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="gestionarEjecutivo"><i title="" style="color:orange;" class="fa fa-users x3"> </i>Plantilla Ejecutivos</a>
            </li>

            <li>
              <a href="ejecutivobanco"><i class="fa fa-bank" style="color:orange;"></i>Asignacion de bancos</a>
            </li>
          </ul>
        </li>
        <?php
                }

                if ($rowmodulos['codmodulo'] == 'SM') {
                  $_SESSION["serviciomantenimiento"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_servicio_mantenimiento"] = 1;
        ?>
        <li>
          <a href="#"><i style="color:black;" class="fa fa-tags"></i> Servicio de Domiciliación<span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li>
              <a href="serviciomantenimiento"><i style="color:black;" class="fa fa-tags"></i> Servicio de Domiciliación</a>
            </li>
            <li>
              <a href="reporteserviciodomiciliacion"><i style="color:black;" class="fa fa-tags"></i> Reporte de Domiciliación</a>
            </li>
          </ul>
        </li>
        <?php
                }


                if ($rowmodulos['codmodulo'] == 'VT') {
                  $_SESSION["visitatecnica"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];

        ?>
        <li>
          <a href="#"><i style="color:black;" class="fa fa-calendar-plus-o"></i> Visita Técnica<span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li <?php if ($pagina == "visitatecnica") {
                  echo "class='active'";
                } ?>>
              <a href="visitatecnica"><i style="color:black;" class="fa fa-calendar-plus-o"></i> Gestión Visita Técnica</a>
            </li>

            <li <?php if ($pagina == "visitatecnica") {
                  echo "class='active'";
                } ?>>
              <a href="reportevisitatecnica"><i style="color:black;" class="fa fa-list-alt"></i> Reporte de Visita Técnica</a>
            </li>
          </ul>
        </li>
        <?php
                }

                if ($rowmodulos['codmodulo'] == 'ST') {
                  $_SESSION["serviciotecnico"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_servicio_tecnico"] = 1;

        ?>
        <li <?php if ($pagina == "serviciotecnico") {
                  echo "class='active'";
                } ?>><a> <i title="" style="color:#01DFD7" class="fa fa-gear"> </i> Servicio Tecnico<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <?php if ($dataUsser['codtipousuario'] == 'A' && $dataUsser['codarea'] != '9' ||  ($dataUsser['codtipousuario'] == 'T' && $dataUsser['codarea'] == '9')) { ?>
              <li>
                <a href="tecnicos"><i style="color:#01DFD7;" class="fa fa-group"></i> Plantilla Técnica</a>
              </li>
            <?php } ?>

            <?php if (($dataUsser['codtipousuario'] == 'A' && $dataUsser['codarea'] != '9') || ($dataUsser['codtipousuario'] == 'T') || ($dataUsser['codtipousuario'] == 'W' && $dataUsser['codarea'] != '9')  || ($dataUsser['codtipousuario'] == 'B' && $dataUsser['codarea'] != '9')  || ($dataUsser['codtipousuario'] == 'E' && $dataUsser['codarea'] != '9')) { ?>
              <li>
                <a href="ConfiguracionPOS"><i title="" style="color:#01DFD7" class="fa fa-wrench"> </i> Configuracion POS</a>
              </li>
            <?php } ?>



            <?php if (($dataUsser['codtipousuario'] == 'T' && $dataUsser['codarea'] != '9') || ($dataUsser['codtipousuario'] == 'A' && $dataUsser['codarea'] != '9') || ($dataUsser['codtipousuario'] == 'T' && $dataUsser['codarea'] == '9') || ($dataUsser['codtipousuario'] == 'W' && $dataUsser['codarea'] != '9')) { ?>
              <li>
                <a href="gestionarServicoTecnico"><i title="" style="color:#01DFD7" class="fa fa-list-alt"> </i> Resultados de la Gestión (General)</a>
              </li>
            <?php } ?>

            <li <?php if ($pagina == "serviciotecnicomifi") {
                  echo "class='active'";
                } ?>>
              <a href="serviciotecnicomifi"><i style="color:#01DFD7" class="fa fa-gears"></i> Servicio Técnico MIFI</a>
            </li>
          </ul>
        </li>
        <?php
                }

                if ($rowmodulos['codmodulo'] == 'AE') {
                  $_SESSION["activarequipos"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
        ?>

        <li <?php if ($pagina == "activacionequipos") {
                  echo "class='active'";
                } ?>>
          <a href="#"><i style="color:#A1D544" class="fa fa-check-circle"></i> Activación de Equipos <span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <?php if (($dataUsser['area'] == 'SISTEMA') || ($dataUsser['area'] == 'ADMINISTRACIÓN')) { ?>
        </li>
      <?php } ?>
      <li>
        <a href="activacionequipos"><i style="color:#A1D544" class="fa fa-check-circle"></i>Activación de Equipos</a>
      </li>
      </li>
      </ul>

      <?php
                }
                if ($rowmodulos['codmodulo'] == 'AC') {
                  $_SESSION["acuerdosservicios"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
      ?>
      <li <?php if ($pagina == "acuerdosservicios") {
                  echo "class='active'";
                } ?>><a> <i title="" style="color:#B84320" class="fa fa-suitcase 3x"> </i> Acuerdos de Servicios<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="acuerdosservicios"><i title="" style="color:#B84320" class="fa fa-suitcase 3x"> </i> Verificación de Dias</a>
          </li>
        </ul>
      </li>
      <?php
                }
                if ($rowmodulos['codmodulo'] == 'IP') {
                  $_SESSION["inventariopos"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_inventario"] = 1;

      ?>
      <li <?php if ($pagina == "inventariopos") {
                  echo "class='active'";
                } ?>><a> <i title="" style="color:purple" class="fa fa-cubes"> </i> Inventario de Equipos <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li <?php if ($pagina == "Serie") {
                  echo "class='active'";
                } ?>>
            <a href="Serie"><i style="color:purple;" class="fa fa-fax"></i> Inventario POS</a>
          </li>
          <li>
            <a href="Simcards">Inventario Simcards</a>
          </li>
          <li>
            <a href="Mifi">Inventario Mifi</a>
          </li>
          <li>
            <a href="reporteinventario"><i title="" style="color:purple" class="fa fa-list-alt"> </i> Reporte de Inventario POS</a>
          </li>

        </ul>
      </li>
      <?php
                }
                if ($rowmodulos['codmodulo'] == 'GM') {
                  $_SESSION["gestionmantenimiento"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["doc_tablas_mantenimiento"] = 1;

      ?>
      <li <?php if ($pagina == "gestionmantenimiento") {
                  echo "class='active'";
                } ?>><a> <i title="" style="color:#04B431" class="fa fa-compass"> </i> Zonas de Mantenimiento <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
 
          <li>
            <a href="gestionmantenimiento"><i title="" style="color:#04B431" class="fa fa-dollar"> </i> Zonas de Mantenimiento</a>
          </li>
        </ul>
      </li>
      <?php
                }

                if ($rowmodulos['codmodulo'] == 'RE') {
                  $_SESSION["reporteejecutivo"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  $_SESSION["area_usuario"] = $dataUsser['codarea'];
                  $_SESSION["doc_reporte_ejecutivo"] = 1;
      ?>
      <li <?php if ($pagina == "reporteejecutivo") {
                  echo "class='active'";
                } ?>><a> <i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Ejecutivos <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">


          <li>
            <?php if ($usuario != 'SOFITASA') { ?>
              <a href="reporteestatusgeneral"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Estatus General</a>
            <?php } ?>
          </li>

        <?php if (($dataUsser['codtipousuario'] == 'A' && $dataUsser['codarea'] == '1')) { ?>
            <li>
              <a href="reporteconexiones"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Conexiones</a>
            </li>
          <?php  }  ?>

            <li>
              <a href="reportediario"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Diario Ejecutivo</a>
            </li>


          <li>

            <?php if ($dataUsser['codarea'] === '1' || $dataUsser['codarea'] === '6' || $usuario === 'AABELLO' || $usuario === 'ERODRIGUEZ' || $usuario === 'MSUAREZ' || $usuario === 'ECASTELLANOS') { ?>
              <a href="reportedomiciliaciongeneral"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Estatus Domiciliación</a>
            <?php } ?>
          </li>

          <li>
            <?php if ($dataUsser['codarea'] === '1' || $dataUsser['codarea'] === '6' || $usuario === 'MTORRES' || $dataUsser['codarea'] === '13' ) { ?>
              <a href="reporteequiposactivos"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte General Equipos Vendidos</a>
         
          </li>

          <li>
            <a href="reporteequiposinstalados"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte General Equipos Activos (Instalados)</a>
          </li>

          <li>
            <a href="reportepercance"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Percance de Equipos.</a>
          </li> 

      <?php if ($dataUsser['codarea'] == '1' || $dataUsser['codarea'] == '6' || $dataUsser['codarea'] == '15' || $dataUsser['codarea'] == '16') { ?>
  

          <li>
            <a href="reportefacturaestatus"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Estatus Pagos</a>
          </li> 
          
      <?php   } ?>

<!--           <li>
            <a href="reportesustituciones"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte de Sustitución de Equipos.</a>
          </li> -->
        <!--  <?php } elseif ($usuario=== 'MTORRES' || $usuario=== 'YMATERAN' || $usuario=== 'MLOPEZF'){ ?>
                  <li>
                   <a href="reporteequiposinstalados"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte General Equipos Activos (Instalados)</a>
                  </li> -->



          <?php  } ?>

          

          <li>
            <a href="reporteserviciotecnico_resultado"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Servicio Tecnico (Resultado de la Gestión General)</a>
          </li>



<!--             
            <li>
              <a href="reportevendedoresmensual"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Vendedores Mensual</a>
            </li> -->
<!--             <li>
              <a href="reporteClientesCobrarDomiciliacion"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Domiciliacion</a>
            </li> -->


<!--            <li <?php if ($pagina == "reportedegestionmifi") {
                  echo "class='active'";
                } ?>>
            <?php if ($usuario != 'SOFITASA' && $dataUsser['codtipousuario'] != 'C') { ?>
              <a href="reportedegestionmifi"><i style="color:#A01010" class="fa fa-group"> </i> Reporte de Servicio Técnico (Gestión MIFI) </a>
            <?php } ?>

          </li>  -->
          
<!--           <li <?php if ($pagina == "reportegeneraldemifi") {
                  echo "class='active'";
                } ?>>
            <?php if ($usuario != 'SOFITASA' && $dataUsser['codtipousuario'] != 'C') { ?>
              <a href="reportegeneraldemifi"><i style="color:#A01010" class="fa fa-group"> </i> Reporte General de registros de Mifi</a>
            <?php } ?>
          </li> -->

        </ul>

      <?php }  ?>

      <?php


              if ($rowmodulos['codmodulo'] == 'RF') {
                $_SESSION["reporteprofit"] = $rowmodulos['codtipo'];
                $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                //$_SESSION["doc_comercial"]=1;


      ?>
      <li <?php if ($pagina == "reporte_profit") {
                  echo "class='active'";
                } ?>>
        <a href="#"><i style="color:#A01010" class="fa fa-group"></i> Reporte Profit<span class="fa arrow"></span> </a>
        <ul class="nav nav-second-level">
          <li>
            <a href="reporteclientesprofit"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Clientes Profit</a>
          </li>
          <li>
            <a href="reporteplantillaprofit"><i title="" style="color:#A01010" class="fa fa-group"> </i> Reporte Plantilla Profit</a>
          </li>
        </ul>
      </li>

      <?php  } ?>
      <?php


                if ($rowmodulos['codmodulo'] == 'CC') {
                  $_SESSION["captacioncliente"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  //$_SESSION["doc_comercial"]=1;

      ?>
      <li <?php if ($pagina == "captacion_clientes") {
                  echo "class='active'";
                } ?>>
        <a href="#"><i style="color:#045FB4;" class="fa fa-bullhorn x4"></i> Captación de Clientes<span class="fa arrow"></span> </a>
        <ul class="nav nav-second-level">
          <li>
            <a href="captacioncliente"><i style="color:#045FB4;" class="fa fa-bullhorn x4"></i> Captación</a>
          </li>

          <?php if ($_SESSION["codtipousuario"] == "W"){ ?>
            <li>
              <a href="captacioncliente?cargar=aceptacioncoord"><i style="color:#045FB4;" class="fa fa-check-square-o x4"></i> Aprobación</a>
            </li>
          <?php } else{ ?>
            <li>
              <a href="captacioncliente?cargar=aceptacioncoord"><i style="color:#045FB4;" class="fa fa-check-square-o x4"></i> Aprobación</a>
            </li>
          <?php } ?>
          <li>
            <a href="captacioncliente?cargar=buscarcliente"><i style="color:#045FB4;" class="fa fa-hand-o-right x4"></i> Asignación</a>
          </li>
        </ul>
      </li>

      <?php  } ?>
      <?php


                if ($rowmodulos['codmodulo'] == 'EN') {
                  $_SESSION["encuesta"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  //$_SESSION["doc_comercial"]=1;
                  if ($dataUsser['codarea'] == '1' || $dataUsser['codarea'] == '10') {

      ?>
        <li <?php if ($pagina == "encuesta") {
                    echo "class='active'";
                  } ?>>
          <a href="#"><i style="color:#F77979;" class="fa fa-phone-square x4"></i> Encuestas<span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li>
              <a href="encuestaa"><i style="color:#F77979;" class="fa fa-phone-square x4"></i> Realizar Encuesta</a>
            </li>
            <li><a href="#"><i style="color:#F77979;" class="fa fa-user fa-fw x4"></i>Personas Referidas<span class="fa arrow"></span> </a>
              <ul class="nav nav-second-level">
                <li><a href="personasreferidas"><i style="color:#F77979;" class="fa fa-bullhorn x4"></i> Captación Personas Referidas</a>
                <li><a href="reportepersonasreferidas"><i style="color:#F77979;" class="fa fa-check-circle  x4"></i> Reporte Personas Referidas</a>
                </li>
              </ul>
            </li>
            <li><a href="#"><i style="color:#F77979;" class="fa fa-edit x4"></i> Reportes Encuesta<span class="fa arrow"></span> </a>
              <ul class="nav nav-second-level">
                <li><a href="reporteencuesta"><i style="color:#F77979;" class="fa fa-question-circle x4"></i> Reporte Por Preguntas</a>
                <li><a href="reporteestatusencuesta"><i style="color:#F77979;" class="fa fa-check-circle  x4"></i> Reporte Por Estatus</a>
                <li><a href="reporteregistrosencuesta"><i style="color:#F77979;" class="fa fa-user x4"></i> Reporte Por Encuestados</a>
                </li>
            </li>
          </ul>
        </li>
        </ul>
        </li>

      <?php }
              }

              if ($rowmodulos['codmodulo'] == 'CA') {
                $_SESSION["controlacceso"] = $rowmodulos['codtipo'];
                $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                //$_SESSION["doc_comercial"]=1;


      ?>
      <li <?php if ($pagina == "control_acceso") {
                  echo "class='active'";
                } ?>>
        <a href="#"><i style="color:#DA70D6;" class="fa fa-check-square-o x4"></i> Control de Accesos<span class="fa arrow"></span> </a>
        <ul class="nav nav-second-level">
          <li>
            <a href="asignarferiados"><i style="color:#DA70D6;" class="fa fa-calendar-plus-o x4"></i> Agregar Feriados</a>
          </li>
          <li>
            <a href="ControlAccesos"><i style="color:#DA70D6;" class="fa fa-plus-square"></i> Adjuntar Archivo</a>
          </li>
          <li>
            <a href="buscadorcontrolacceso"><i style="color:#DA70D6;" class="fa fa-search x4"></i> Búsqueda</a>
          </li>
        </ul>
      </li>

      <?php  }
                /*if ($rowmodulos['codmodulo'] == 'MB') {
                  //activo variable de session para validar estilos banesco   
                  $_SESSION["cssBanesco"] = 1;

                  $_SESSION["inventariobanesco"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  //$_SESSION["doc_comercial"]=1;
      ?>
      <li <?php if ($pagina == "inventariobanesco") {
                  echo "class='active'";
                } ?>>
        <a href="#"><i style="color:green;" class="fa fa-th-list x4 iconcolorbanesco"></i>Inventario Banesco<span class="fa arrow"></span> </a>
        <ul class="nav nav-second-level"> 

          <li>
            <a href="inventario_banesco"><i style="color:green;" class="fa fa-th-list x4 iconcolorbanesco"></i> Inventario</a>
          </li>

          <li>
            <a href="reingreso_equipos"><i style="color:blue;" class="fa fa-th-list x4 iconcolorbanesco iconcolorbanesco"></i> Reingreso Inventario</a>
          </li>

        </ul>
      </li>

      <?php  }*/
                /*if ($rowmodulos['codmodulo'] == 'RB') {
                  //activo variable de session para validar estilos banesco   
                  $_SESSION["cssBanesco"] = 1;

                  $_SESSION["modulobanesco"] = $rowmodulos['codtipo'];
                  $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];
                  //$_SESSION["doc_comercial"]=1;
      ?>
      <li <?php if ($pagina == "modulo_banesco") {
                  echo "class='active'";
                } ?>>
        <a href="#"><img class="imagen" src="img/imagenbanesco.png">Reportes Banesco<span class="fa arrow"></span> </a>
        <ul class="nav nav-second-level"> 

          <li>
            <a href="reporteinventariobanesco"><i title="" style="color:black" class="fa fa-group iconcolorbanesco"> </i> Reporte de Inventario</a>
          </li>
          <li>
            <a href="reportebanescogeneral"><i title="" style="color:black" class="fa fa-group iconcolorbanesco"> </i> Reporte General Clientes</a>
          </li>

        </ul>
      </li>*/


/*      if ($dataUsser['codarea'] == '1' || $dataUsser['codarea'] == '13' || $usuario == 'NBRAVO' || $usuario == 'MAPEREZ' || $usuario == 'SSALAZAR' || $dataUsser['codarea'] == '6' || 
      $usuario=="JGONZALEZ" || $usuario=="RRODRIGUEZ" || $usuario=="JHERNANDEZ" || $usuario=="GFUENTES" || $usuario=="MSUAREZ" || $usuario=="NVERGARA"){*/

      
        if ($rowmodulos['codmodulo'] == 'RM') {
            $_SESSION["riesgo_medido"] = $rowmodulos['codtipo'];
            $_SESSION["superusuario"] = $rowmodulos['codtipousuario'];

            //echo  $_SESSION["riesgo_medido"];
        ?>
      
      <li class="textototote" <?php if ($pagina == "riesgomedido") {
                  echo "class='active'";
                } ?>>


        <a href="#"><i style="color:green;" class="fa fa-usd x4"></i>Reportes Control de Ventas<span class="fa arrow"></span> </a>
        <ul class="nav nav-second-level"> 

          <?php if ($_SESSION["riesgo_medido"] == "W") { ?>


          <li>
            <a href="riesgomedido"><i style="color:green;" class="fa fa-line-chart x4"></i> Reporte Control de Ventas</a>
          </li>

 <?php }  ?>
    
          <?php  if ($dataUsser['codarea'] == '1' || $dataUsser['codarea'] == '6'){ ?>

          <li>
            <a href="Reportecargaparametros"><i style="color:green;" class="fa fa-line-chart x4"></i> Solicitud de Carga de Parametros</a>
          </li>
          <li>
            <a href="iniciopreciospos"><i style="color:green;" class="fa fa-usd x4"></i> Precios POS</a>
          </li>

          
        <?php } ?>
          <!--li>
            <a href="controlordenservicio"><i style="color:green;" class="fa fa-line-chart x4"></i> Control Orden de Servicio</a>
          </li-->
        </ul>
      </li>

      <?php } ?>




      <?php  }?>



      <li>
        <?php if ($_SESSION["codtipousuario"] == "U") { ?>
          <link href="dist/css/cssBanesco.css" rel="stylesheet">
        <?php } ?>
      </li>
      </div>
      <!-- /.sidebar-collapse -->
    </div>


    <!--==============================header=================================-->
