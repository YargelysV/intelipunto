<?php
ini_set("session.use_only_cookies","1");

if (isset($_SESSION["session_user"])){
  session_set_cookie_params(0, "/", $_SERVER["HTTP_HOST"], 0); 

        $usuario=$_SESSION["session_user"];
        $controlador =new controladorUsuario();
        $dataUsser=$controlador->ver($usuario);

        require("controller/Controladormodulos.php");
        $controladormodulos =new controladorModulo();
        $modulos=$controladormodulos->spverificarmodulosactivos($_SESSION["session_user"]);

        $_SESSION["superusuario"]=0;
        $_SESSION["administracion"]=0;
        $_SESSION["clientespotenciales"]=0;
        $_SESSION["comercializacion"]=0;

        $controladormodulosge =new controladorModulo();
        $modulosge=$controladormodulosge->spverificarmodulosactivos($_SESSION["session_user"]);
        $MODULO=0;
         while($rowmodulosge = pg_fetch_array($modulosge))
        {
            if ($rowmodulosge['codmodulo']=='GE')
            {
            $MODULO=1;
            }
        }

//si cambia de navegador o ip o de Host
    if($_SESSION['navUser'] != $_SERVER['HTTP_USER_AGENT'] or
    $_SESSION['IPuser'] != $_SERVER['REMOTE_ADDR'] or
    $_SESSION['hostUser'] != gethostbyaddr($_SERVER['REMOTE_ADDR'])){
     header("Location: salir.php");
    }
    //calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-n-j H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));
    //comparamos el tiempo transcurrido

 }
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Intelipunto. by Inteligensa ">
  <meta name="keywords" content="Inicio de sesion,intelipunto">
  <title>Iniciar Sesión | Intelipunto</title>


 <link rel="stylesheet" href="alertify/themes/alertify.core.css" />
  <link rel="stylesheet" href="alertify/themes/alertify.default.css" id="toggleCSS" />
  <!-- js -->

  <script src="alertify/lib/alertify.min.js"></script>

  <meta name="msapplication-TileColor" content="#00bcd4">

   <link rel="icon"  href="img/Intelipunto.ico" type="image/x-icon">
    <link rel="shortcut icon" href="img/Intelipunto.ico" type="image/x-icon" />

  <link href="materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="materialize/css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <!-- Custome CSS-->
  <link href="materialize/css/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <?php if ($pagina=="") {  ?>
  <link href="materialize/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">
 <?php } ?>
  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="materialize/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="materialize/css/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">


  <link type="text/css" rel="stylesheet" href="DataTable/datatable.css">
  <link type="text/css" rel="stylesheet" href="DataTable/jquery.dataTables.css">

  <link type="text/css" rel="stylesheet" href="DataTable/datatable.css">
  <link type="text/css" rel="stylesheet" href="DataTable/jquery.dataTables.css">
  

  

</head>


  <?php if ($pagina!="") {  ?>


<body class="loaded">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START HEADER -->
  <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">
                      <li><h1 class="logo-wrapper"><a href="#" class="brand-logo darken-1"><img src="img/materialize-logo.png" alt="materialize logo"></a> <span class="logo-text">Intelipunto</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi mdi-magnify"></i>


                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown"><img src="img/United-States.png" alt="USA"></a><ul id="translation-dropdown" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 64px; left: 1107px; opacity: 1; display: none;">
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><img src="img/United-States.png" alt="English">  <span class="language-select">English</span></a>
                      </li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><img src="./Forms Layouts _ Materialize - Material Design Admin Template_files/France.png" alt="French">  <span class="language-select">French</span></a>
                      </li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><img src="./Forms Layouts _ Materialize - Material Design Admin Template_files/China.png" alt="Chinese">  <span class="language-select">Chinese</span></a>
                      </li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><img src="./Forms Layouts _ Materialize - Material Design Admin Template_files/Germany.png" alt="German">  <span class="language-select">German</span></a>
                      </li>

                    </ul>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="account-child"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>

                        </a><ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                        </li>
                        <li><a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- translation-button -->

                    <!-- notifications-dropdown -->

                </div>
            </nav>
        </div>
        <!-- end header nav-->
  </header>
  <!-- END HEADER -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

      <!-- START LEFT SIDEBAR NAV-->
      <aside id="left-sidebar-nav">
        <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y" style="width: 240px;">
            <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s4 m4 l4">
                    <img src="img/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                </div>
                <div class="col col s8 m8 l8">

                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?php echo $dataUsser['nombres'];?><i class="mdi-navigation-arrow-drop-down right"></i></a><ul id="profile-dropdown" class="dropdown-content">
                        <li><a href="#"><i class="mdi-action-face-unlock"></i> Perfil</a>
                        </li>
                        <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                        </li>
                        <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                        </li>
                        <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Salir</a>
                        </li>
                    </ul>
                    <p class="user-roal"><?php echo $dataUsser['tipousuario'];?></p>
                </div>
            </div>
            </li>

            <?php
                while($rowmodulos = pg_fetch_array($modulos))
                {

                  if ($rowmodulos['codmodulo']=='ADM')
                  {
                    $_SESSION["administracion"]=$rowmodulos['codtipo'];
                    $_SESSION["superusuario"]=$rowmodulos['codtipousuario'];
                      ?>


            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Administración</a>
                        <div class="collapsible-body" style="">
                            <ul>
                            <?php
                                ?>
                                <li <?php if ($pagina=="recepcionData") {echo "class='active'";} ?>><a href="recepcionData">Recepción Data Series</a>
                                </li>
                                <li <?php if ($pagina=="preparacion_data") {echo "class='active'";} ?>> <a href="preparacionData">Preparación Data Series</a>
                                </li>

                                <li <?php if ($pagina=="recepcion_cliente") {echo "class='active'";} ?>>  <a href="recepcionCliente">Recepción Cliente</a>
                                </li>

                                <li <?php if ($pagina=="preparacion_cliente") {echo "class='active'";} ?>>   <a href="preparacionCliente">Preparación Cliente</a>
                                </li>


                            </ul>
                        </div>
                    </li>
                </ul>
            </li>

                 <?php
                  }




                   if ($rowmodulos['codmodulo']=='CP')
                  {
                    $_SESSION["clientespotenciales"]=$rowmodulos['codtipo'];
                     $_SESSION["superusuario"]=$rowmodulos['codtipousuario'];
                      ?>
           <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Clientes Potenciales</a>
                        <div class="collapsible-body" style="">
                            <ul>

                                <li <?php if ($pagina=="clientes_potenciales") {echo "class='active'";} ?>><a href="clientesPotenciales">Recepción de Clientes Potenciales</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
  <?php
                  }
                      } //end while
                     ?>

             <li class="bold" <?php if ($pagina=="gestion_Comercializacion") {echo "class='active'";} ?>><a href="gestioncomercializacion" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Comercialización</a>
            </li>
            <li class="bold"><a href="http://demo.geekslabs.com/materialize-v1.0/app-email.html" class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Mailbox <span class="new badge">4</span></a>
            </li>
            <li class="bold"><a href="http://demo.geekslabs.com/materialize-v1.0/app-calendar.html" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
            </li>
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-invert-colors"></i> CSS</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-typography.html">Typography</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-icons.html">Icons</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-animations.html">Animations</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-shadow.html">Shadow</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-media.html">Media</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-sass.html">Sass</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> UI Elements</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-alerts.html">Alerts</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-buttons.html">Buttons</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-badges.html">Badges</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-breadcrumbs.html">Breadcrumbs</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-collections.html">Collections</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-collapsibles.html">Collapsibles</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-tabs.html">Tabs</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-navbar.html">Navbar</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-pagination.html">Pagination</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-preloader.html">Preloader</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-toasts.html">Toasts</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-tooltip.html">Tooltip</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/ui-waves.html">Waves</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Advanced UI <span class="new badge"></span></a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-chips.html">Chips</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-cards.html">Cards</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-modals.html">Modals</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-media.html">Media</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-range-slider.html">Range Slider</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-sweetalert.html">SweetAlert</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-nestable.html">Shortable &amp; Nestable</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-translation.html">Language Translation</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/advanced-ui-highlight.html">Highlight</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a href="http://demo.geekslabs.com/materialize-v1.0/app-widget.html" class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> Widgets</a>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Tables</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/table-basic.html">Basic Tables</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/table-data.html">Data Tables</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/table-jsgrid.html">jsGrid</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/table-editable.html">Editable Table</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/table-floatThead.html">floatThead</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold active"><a class="collapsible-header  waves-effect waves-cyan active"><i class="mdi-editor-insert-comment"></i> Forms <span class="new badge"></span></a>
                        <div class="collapsible-body" style="display: block;">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/form-elements.html">Form Elements</a>
                                </li>
                                <li class="active"><a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html">Form Layouts</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/form-validation.html">Form Validations</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/form-masks.html">Form Masks</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/form-file-uploads.html">File Uploads</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-social-pages"></i> Pages</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/page-contact.html">Contact Page</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/page-todo.html">ToDos</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/page-blog-1.html">Blog Type 1</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/page-blog-2.html">Blog Type 2</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/page-404.html">404</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/page-500.html">500</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/page-blank.html">Blank</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> eCommers</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/eCommerce-products-page.html">Products Page</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/eCommerce-pricing.html">Pricing Table</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/eCommerce-invoice.html">Invoice</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-image"></i> Medias</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/media-gallary-page.html">Gallery Page</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/media-hover-effects.html">Image Hover Effects</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-account-circle"></i> User</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/user-profile-page.html">User Profile</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/user-login.html">Login</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/user-register.html">Register</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/user-forgot-password.html">Forgot Password</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/user-lock-screen.html">Lock Screen</a>
                                </li>

                            </ul>
                        </div>
                    </li>

                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Charts</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/charts-chartjs.html">Chart JS</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/charts-chartist.html">Chartist</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/charts-morris.html">Morris Charts</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/charts-xcharts.html">xCharts</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/charts-flotcharts.html">Flot Charts</a>
                                </li>
                                <li><a href="http://demo.geekslabs.com/materialize-v1.0/charts-sparklines.html">Sparkline Charts</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="li-hover"><div class="divider"></div></li>
            <li class="li-hover"><p class="ultra-small margin more-text">MORE</p></li>
            <li><a href="http://demo.geekslabs.com/materialize-v1.0/angular-ui.html"><i class="mdi-action-verified-user"></i> Angular UI  <span class="new badge"></span></a>
            </li>
            <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-grid.html"><i class="mdi-image-grid-on"></i> Grid</a>
            </li>
            <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-color.html"><i class="mdi-editor-format-color-fill"></i> Color</a>
            </li>
            <li><a href="http://demo.geekslabs.com/materialize-v1.0/css-helpers.html"><i class="mdi-communication-live-help"></i> Helpers</a>
            </li>
            <li><a href="http://demo.geekslabs.com/materialize-v1.0/changelogs.html"><i class="mdi-action-swap-vert-circle"></i> Changelogs</a>
            </li>
            <li class="li-hover"><div class="divider"></div></li>
            <li class="li-hover"><p class="ultra-small margin more-text">Daily Sales</p></li>
            <li class="li-hover">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="sample-chart-wrapper">
                            <div class="ct-chart ct-golden-section" id="ct2-chart"><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-line" style="width: 100%; height: 100%;"><g class="ct-labels"><foreignobject style="overflow: visible;" x="45" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">1</span></foreignobject><foreignobject style="overflow: visible;" x="66" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">2</span></foreignobject><foreignobject style="overflow: visible;" x="87" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">3</span></foreignobject><foreignobject style="overflow: visible;" x="108" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">4</span></foreignobject><foreignobject style="overflow: visible;" x="129" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">5</span></foreignobject><foreignobject style="overflow: visible;" x="150" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">6</span></foreignobject><foreignobject style="overflow: visible;" x="171" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">7</span></foreignobject><foreignobject style="overflow: visible;" x="192" y="104" width="21" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">8</span></foreignobject><foreignobject style="overflow: visible;" y="84" x="-5" height="20.88888888888889" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">0</span></foreignobject><foreignobject style="overflow: visible;" y="66.9090909090909" x="-5" height="20.88888888888889" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">2</span></foreignobject><foreignobject style="overflow: visible;" y="49.81818181818181" x="-5" height="20.88888888888889" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">4</span></foreignobject><foreignobject style="overflow: visible;" y="32.72727272727273" x="-5" height="20.88888888888889" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">6</span></foreignobject><foreignobject style="overflow: visible;" y="15.63636363636364" x="-5" height="20.88888888888889" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">8</span></foreignobject></g><g class="ct-grids"><line x1="45" x2="45" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line x1="66" x2="66" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line x1="87" x2="87" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line x1="108" x2="108" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line x1="129" x2="129" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line x1="150" x2="150" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line x1="171" x2="171" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line x1="192" x2="192" y1="5" y2="99" class="ct-grid ct-horizontal"></line><line y1="99" y2="99" x1="45" x2="213" class="ct-grid ct-vertical"></line><line y1="81.9090909090909" y2="81.9090909090909" x1="45" x2="213" class="ct-grid ct-vertical"></line><line y1="64.81818181818181" y2="64.81818181818181" x1="45" x2="213" class="ct-grid ct-vertical"></line><line y1="47.72727272727273" y2="47.72727272727273" x1="45" x2="213" class="ct-grid ct-vertical"></line><line y1="30.63636363636364" y2="30.63636363636364" x1="45" x2="213" class="ct-grid ct-vertical"></line></g><g class="ct-series ct-series-a"><path d="M45,99L45,56.273C48.5,50.576,59,24.939,66,22.091C73,19.242,80,37.758,87,39.182C94,40.606,101,27.788,108,30.636C115,33.485,122,49.152,129,56.273C136,63.394,143,73.364,150,73.364C157,73.364,164,57.697,171,56.273C178,54.848,188.5,63.394,192,64.818L192,99" class="ct-area" ct:values="5,9,7,8,5,3,5,4"></path><path d="M45,56.273C48.5,50.576,59,24.939,66,22.091C73,19.242,80,37.758,87,39.182C94,40.606,101,27.788,108,30.636C115,33.485,122,49.152,129,56.273C136,63.394,143,73.364,150,73.364C157,73.364,164,57.697,171,56.273C178,54.848,188.5,63.394,192,64.818" class="ct-line" ct:values="5,9,7,8,5,3,5,4"></path><line x1="45" y1="56.27272727272727" x2="45.01" y2="56.27272727272727" class="ct-point" ct:value="5"></line><line x1="66" y1="22.090909090909093" x2="66.01" y2="22.090909090909093" class="ct-point" ct:value="9"></line><line x1="87" y1="39.18181818181818" x2="87.01" y2="39.18181818181818" class="ct-point" ct:value="7"></line><line x1="108" y1="30.63636363636364" x2="108.01" y2="30.63636363636364" class="ct-point" ct:value="8"></line><line x1="129" y1="56.27272727272727" x2="129.01" y2="56.27272727272727" class="ct-point" ct:value="5"></line><line x1="150" y1="73.36363636363636" x2="150.01" y2="73.36363636363636" class="ct-point" ct:value="3"></line><line x1="171" y1="56.27272727272727" x2="171.01" y2="56.27272727272727" class="ct-point" ct:value="5"></line><line x1="192" y1="64.81818181818181" x2="192.01" y2="64.81818181818181" class="ct-point" ct:value="4"></line></g></svg></div>
                        </div>
                    </div>
                </div>
            </li>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 525px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 210px;"></div></div></ul>
        <a href="http://demo.geekslabs.com/materialize-v1.0/form-layouts.html#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
        </aside>
      <!-- END LEFT SIDEBAR NAV-->
 <?php } ?>